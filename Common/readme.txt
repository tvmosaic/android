How to use Common in projects
==============================

settings.gradle:
----------------
include ':app'
include ':dvblink_common'
project(':dvblink_common').projectDir = new File('../Common/dvblink_common')
include ':xstream'
project(':xstream').projectDir = new File('../Common/xstream')
include ':libvlc'
project(':libvlc').projectDir = new File('../Common/libvlc')

app/build.gradle:
-----------------
...
dependencies {
    compile fileTree(dir: 'libs', include: ['*.jar'])
	...
    compile project(':dvblink_common')
    compile project(':xstream')
    compile project(':libvlc')
}
...

