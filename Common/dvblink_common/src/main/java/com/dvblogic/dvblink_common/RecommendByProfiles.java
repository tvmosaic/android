package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("recommend_by_profiles")
public class RecommendByProfiles extends Serializer
{
	@XStreamAlias("start_time")
	public long StartTime;

	@XStreamAlias("end_time")
	public long EndTime;

	@XStreamAlias("extended_epg")
	public boolean ExtendedEpg;

	@XStreamAlias("profiles")
	public ProfileIDList Profiles;
}
