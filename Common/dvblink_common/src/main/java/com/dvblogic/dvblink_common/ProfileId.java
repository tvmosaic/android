package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("create_profile")
public class ProfileId extends Serializer
{
	@XStreamAlias("profile_id")
	public String ProfileId;
}
