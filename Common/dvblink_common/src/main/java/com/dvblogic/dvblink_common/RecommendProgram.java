package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("program")
public class RecommendProgram extends Serializer
{
	@XStreamAlias("id")
	public String ID;
	
	@XStreamAlias("rating")
	public float Rating;
	
	@XStreamAlias("score")
	public float Score;
	
	
}
