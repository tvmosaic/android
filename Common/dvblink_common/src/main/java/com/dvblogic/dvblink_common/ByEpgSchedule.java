package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

@XStreamConverter(value=ByEpgScheduleConverter.class)
@XStreamAlias("by_epg")
public class ByEpgSchedule extends Serializer
{
	@XStreamAlias("channel_id")
	public String	ChannelID;

	@XStreamAlias("program_id")
	public String	ProgramID;

	@XStreamAlias("repeat")
	public boolean	IsRepeat;

	@XStreamAlias("new_only")
	public boolean	NewOnly;

	@XStreamAlias("record_series_anytime")
	public boolean	RecordSeriesAnytime=true;

	@XStreamAlias("recordings_to_keep")
	public int	RecordingsToKeep;

	@XStreamAlias("start_before")
	public int	StartBefore = Schedule.startMarginAnyTime;

    @XStreamAlias("start_after")
    public int	StartAfter = Schedule.startMarginAnyTime;

    @XStreamAlias("day_mask")
    public int	DayMask = Schedule.dayMaskAnyDay;

	@XStreamConverter(value=ByEpgScheduleProgramConverter.class)
	@XStreamAlias("program")
	public Program	Program;

	public ByEpgSchedule() {
		RecordSeriesAnytime = true;
	}

	public ByEpgSchedule(String channel_id, String program_id) {
		this(channel_id, program_id, false);
	}

	public ByEpgSchedule(String channel_id, String program_id, boolean is_repeat) {
		ChannelID = channel_id;
		ProgramID = program_id;
		IsRepeat = is_repeat;
		RecordSeriesAnytime = true;
	}
}
