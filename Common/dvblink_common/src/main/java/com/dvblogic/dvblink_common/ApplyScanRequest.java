package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by andreyp on 15.11.2016.
 */
@XStreamAlias("apply_scan")
public class ApplyScanRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    public ApplyScanRequest(String id) {
        deviceID = id;
    }
}