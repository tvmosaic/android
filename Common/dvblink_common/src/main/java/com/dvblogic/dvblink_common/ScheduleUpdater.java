package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("update_schedule")
public class ScheduleUpdater extends Serializer
{
	@XStreamAlias("schedule_id")
	public String	ScheduleID;

	@XStreamAlias("new_only")
	public boolean	NewOnly;

	@XStreamAlias("record_series_anytime")
	public boolean	RecordSeriesAnytime;

	@XStreamAlias("recordings_to_keep")
	public int	RecordingsToKeep;

	@XStreamAlias("margine_before")
	public long  MargineBefore;

    @XStreamAlias("margine_after")
    public long  MargineAfter;

    @XStreamAlias("targets")
    public SendToTargetIDLIst Targets;

    @XStreamAlias("active")
    public boolean Active = true;

    @XStreamAlias("priority")
    public int Priority = Schedule.schedulePriorityNormal;

    @XStreamAlias("start_before")
    public int	StartBefore = Schedule.startMarginAnyTime;

    @XStreamAlias("start_after")
    public int	StartAfter = Schedule.startMarginAnyTime;

    @XStreamAlias("day_mask")
    public int	DayMask = Schedule.dayMaskAnyDay;

    public ScheduleUpdater() {
        MargineAfter = -1;
        MargineBefore = -1;
        RecordingsToKeep = 0;
        RecordSeriesAnytime = true;
        NewOnly = false;
    }
}
