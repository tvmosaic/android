package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("activate_product")
public class ActivateProductResponse extends Serializer {

	public enum Result {
		SUCCESS,
		NO_SERVER_CONNECTION,
		NO_ACTIVATIONS_AVAILABLE,
		ALREADY_ACTIVATED,
		INVALID_LOGIN,
		IN_PROGRESS,
		FILE_WRITE_ERROR,
		ERROR,
		INVALID_XML,
		INVALID_DATA,
		INVALID_COUPON,
		ALREADY_USED_COUPON,
		EMAIL_ALREADY_IN_USE,
		OTHER_PRODUCT_COUPON
	}

	@XStreamAlias("result")
	private String result;

	public Result getResult() {
		try {
			return Result.valueOf(this.result.toUpperCase());
		} catch (IllegalArgumentException exception) {
			return Result.ERROR;
		}
	}
}
