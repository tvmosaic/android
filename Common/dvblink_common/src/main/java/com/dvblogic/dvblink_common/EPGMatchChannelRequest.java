package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("channel")
public class EPGMatchChannelRequest extends Serializer {
	@XStreamAsAttribute
	@XStreamAlias("id")
	public String id;

	public EPGMatchChannelRequest(String id) {
		this.id = id;
	}
}
