package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("stream")
public class RequestStream extends Serializer
{
	public static final long	serialVersionUID	= 1L;

	public static final String	ANDROID_TYPE		= "rtp";
	public static final String	IPHONE_TYPE			= "hls";
	public static final String	WINPHONE_TYPE		= "asf";
	public static final String	RAW_HTTP_TYPE		= "raw_http";
	public static final String	RAW_UDP_TYPE		= "raw_udp";

	@XStreamAlias("channel_dvblink_id")
	public String					DVBLinkID;

	@XStreamAlias("physical_channel_id")
	public String				PhysicalChannelId;

	@XStreamAlias("source_id")
	public String				SourceName;

	@XStreamAlias("client_id")
	public String				ClientId;

	@XStreamAlias("stream_type")
	public String				StreamType;

	@XStreamAlias("transcoder")
	public Transcoder			Transcoder;

	@XStreamAlias("server_address")
	public String				ServerAddress;

	@XStreamAlias("client_address")
	public String				ClientAddress;

	@XStreamAlias("streaming_port")
	public long					StreamingPort;

	@XStreamAlias("duration")
	public int					Duration;

	/*public RequestStream(String server_address, String channel_id, String client_id, String stream_type) {
		this(server_address, channel_id, client_id, stream_type, null);
	}*/

	public RequestStream(String server_address, String physical_channel_id, String source_name, String client_id,
			String stream_type) {
		this(server_address, physical_channel_id, source_name, client_id, stream_type, null);
	}

	public RequestStream(String server_address, String channel_id, String client_id, String stream_type,
			Transcoder transcoder) {
		this();
		ServerAddress = server_address;
		DVBLinkID = channel_id;
		ClientId = client_id;
		StreamType = stream_type;
		Transcoder = transcoder;
	}

	public RequestStream(String server_address, String physical_channel_id, String source_name, String client_id,
			String stream_type, Transcoder transcoder) {
		this();
		ServerAddress = server_address;
		PhysicalChannelId = physical_channel_id;
		SourceName = source_name;
		ClientId = client_id;
		StreamType = stream_type;
		Transcoder = transcoder;
	}

	public RequestStream() {
		ServerAddress = "";
		DVBLinkID = "";
		ClientId = "";
		StreamType = "";
		Transcoder = new Transcoder();
		PhysicalChannelId = "";
		SourceName = "";
		ClientAddress = "";
		StreamingPort = 0;
		Duration = 0;
	}
}
