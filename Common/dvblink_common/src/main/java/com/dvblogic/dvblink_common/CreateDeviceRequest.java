package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("manual_device")
public class CreateDeviceRequest extends Serializer {
    @XStreamAlias("device_params")
    public ConciseParamMap deviceParams;

    public CreateDeviceRequest(ConciseParamMap deviceParams) {
        this.deviceParams = deviceParams;
    }
}
