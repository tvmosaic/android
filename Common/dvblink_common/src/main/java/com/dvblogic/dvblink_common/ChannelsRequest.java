package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("channels")
public class ChannelsRequest extends Serializer
{
    @XStreamAlias("favorite_id")
    public String		favoriteId;

    public ChannelsRequest() {
        favoriteId = "";
    }

    public ChannelsRequest(String fav_id) {
        favoriteId = fav_id;
    }
}
