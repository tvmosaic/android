package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.basic.IntConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


public class UpdaterGetStatusResponseConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(UpdaterGetStatusResponse.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        UpdaterGetStatusResponse response = new UpdaterGetStatusResponse();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();

            if (nodeName.equalsIgnoreCase("status")) {
                response.status = reader.getValue();
            }

            if (nodeName.equalsIgnoreCase("components")) {
                while (reader.hasMoreChildren()) {
                    reader.moveDown();
                    nodeName = reader.getNodeName();
                    if (nodeName.equalsIgnoreCase("component")) {
                        UpdaterGetStatusResponse.Component component = new UpdaterGetStatusResponse.Component();
                        while (reader.hasMoreChildren()) {
                            reader.moveDown();
                            nodeName = reader.getNodeName();
                            if (nodeName.equalsIgnoreCase("name")) {
                                component.name = reader.getValue();
                            }
                            if (nodeName.equalsIgnoreCase("changes")) {
                                component.changes = reader.getValue();
                            }
                            if (nodeName.equalsIgnoreCase("local")) {
                                component.local = reader.getValue();
                            }
                            if (nodeName.equalsIgnoreCase("remote")) {
                                component.remote = reader.getValue();
                            }
                            reader.moveUp();
                        }
                        response.components.add(component);
                    }
                    reader.moveUp();
                }
            }
            reader.moveUp();
        }
        return response;
    }
}
