package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("object")
public class PlaybackItemContainer extends Serializer
{
	@XStreamAlias("containers")
	public ContainerList	Containers;

	@XStreamAlias("items")
	public PlaybackItemList	Items;

	@XStreamAlias("actual_count")
	public int				ActualCount;

	@XStreamAlias("total_count")
	public int				TotalCount;
}
