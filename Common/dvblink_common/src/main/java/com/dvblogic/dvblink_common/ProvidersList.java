package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("get_providers")
public class ProvidersList extends Serializer
{
	@XStreamImplicit(itemFieldName = "provider")
	private ArrayList<Provider>	_list;

	public ProvidersList() {
		_list = new ArrayList<Provider>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Provider>();
		}
		return this;
	}

	public ArrayList<Provider> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Provider ch) {
		if (_list == null) {
			_list = new ArrayList<Provider>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Provider get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
