package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.util.ArrayList;

@XStreamConverter(ParamContainerConverter.class)
@XStreamAlias("container")
public class ParamContainer
{
	public String id;
	public String name;
	public String desc;
	public ArrayList<SelectableParam> selectable_params = new ArrayList<>();
    public ArrayList<EditableParam> editable_params = new ArrayList<>();
}
