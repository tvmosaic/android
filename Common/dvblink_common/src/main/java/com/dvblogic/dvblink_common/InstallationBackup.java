package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.io.IOException;

@XStreamAlias("binary_data")
public class InstallationBackup extends Serializer
{
	@XStreamAlias("data")
	public String	data;

	@XStreamAlias("mime")
	public String	mime;

    public byte[] getData() {
        try {
            return Base64.decode(data, Base64.URL_SAFE);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean setData(byte[] binary_data, String mimeStr) {
        mime = mimeStr;

        try {
            data = Base64.encodeBytes(binary_data, Base64.URL_SAFE);
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

}
