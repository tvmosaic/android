package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("recommendations")
public class RecommendationsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "channel")
	private ArrayList<Recommendation>	_list;

	public RecommendationsList() {
		_list = new ArrayList<Recommendation>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Recommendation>();
		}
		return this;
	}

	public ArrayList<Recommendation> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Recommendation ch) {
		if (_list == null) {
			_list = new ArrayList<Recommendation>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Recommendation get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
