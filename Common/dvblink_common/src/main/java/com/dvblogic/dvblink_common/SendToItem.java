package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Serg on 25-Mar-16.
 */

@XStreamAlias("item")
public class SendToItem extends Serializer
{
    @XStreamAlias("item_id")
    public String itemId;

    @XStreamAlias("object_id")
    public String objectId;

    @XStreamAlias("description")
    public String description;

    @XStreamAlias("created")
    public long created;

    @XStreamAlias("target")
    public String target;

    @XStreamAlias("completed")
    public long completed;

    @XStreamAlias("status")
    public int status;
}
