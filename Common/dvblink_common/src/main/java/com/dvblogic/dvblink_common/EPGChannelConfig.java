package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("channel")
public class EPGChannelConfig extends Serializer {
	@XStreamAsAttribute
	@XStreamAlias("id")
	public String id;

	@XStreamAlias("epg_source_id")
	public String epgSourceId;

	@XStreamAlias("epg_channel_id")
	public String epgChannelId;

	public EPGChannelConfig(String id, String epgSourceId, String epgChannelId) {
		this.id = id;
		this.epgSourceId = epgSourceId;
		this.epgChannelId = epgChannelId;
	}
}
