package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("by_pattern")
public class ByPatternSchedule extends Serializer
{
	public static final String channel_id_any = "";
    public static final int keep_all_recordings = 0;

	@XStreamAlias("channel_id")
	public String	ChannelID;

	@XStreamAlias("key_phrase")
	public String	KeyPhrase;

	@XStreamAlias("genre_mask")
	public long	GenreMask;

	@XStreamAlias("recordings_to_keep")
	public int	RecordingsToKeep;

    @XStreamAlias("start_before")
    public int	StartBefore = Schedule.startMarginAnyTime;

    @XStreamAlias("start_after")
    public int	StartAfter = Schedule.startMarginAnyTime;

    @XStreamAlias("day_mask")
    public int	DayMask = Schedule.dayMaskAnyDay;

	public ByPatternSchedule() {
		ChannelID = channel_id_any;
        KeyPhrase = "";
        GenreMask = 0;
        RecordingsToKeep = keep_all_recordings;
	}

	public ByPatternSchedule(String channel_id, String key_phrase, long	genre_mask, int	rec_to_keep) {
		ChannelID = channel_id;
        KeyPhrase = key_phrase;
        GenreMask = genre_mask;
        RecordingsToKeep = rec_to_keep;
	}

}
