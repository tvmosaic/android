package com.dvblogic.dvblink_common;

/**
 * Created by Serg on 01-Sep-16.
 */
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("timeshift_status")
public class TimeShiftStatusResponse extends Serializer
{
    //  maximum size of the timeshift buffer in bytes
    @XStreamAlias("max_buffer_length")
    public long max_buffer_length;

    // current size of the timeshift buffer in bytes (may be less than max_buffer_length while buffer is growing)
    @XStreamAlias("buffer_length")
    public long buffer_length;

    // current playback position within the timeshift buffer in bytes (range: between 0 and buffer_length)
    @XStreamAlias("cur_pos_bytes")
    public long cur_pos_bytes;

    // current duration of the timeshift buffer in seconds
    @XStreamAlias("buffer_duration")
    public long buffer_duration;

    // current playback position within the timeshift buffer in seconds (range: between 0 and buffer_duration)
    @XStreamAlias("cur_pos_sec")
    public long cur_pos_sec;

    public TimeShiftStatusResponse() {
        max_buffer_length = 4294967296L; // 4GB
        buffer_length = 0;
        buffer_duration = 0;
        cur_pos_bytes = 0;
        cur_pos_sec = 0;
    }
}
