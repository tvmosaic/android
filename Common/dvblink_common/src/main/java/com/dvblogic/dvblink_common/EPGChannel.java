package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("epg_channel")
public class EPGChannel extends Serializer {
	@XStreamAsAttribute
	@XStreamAlias("id")
	public String id;

	@XStreamAsAttribute
	@XStreamAlias("name")
	public String name;

	@XStreamAlias("logo")
	private String logo;

	@Override
	public String toString() {
		return name;
	}

	public EPGChannel(String id, String name) {
		this.id = id;
		this.name = name;
	}
}
