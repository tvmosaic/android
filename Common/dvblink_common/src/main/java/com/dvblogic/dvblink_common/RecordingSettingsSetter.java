package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("recording_settings")
public class RecordingSettingsSetter extends Serializer
{
	@XStreamAlias("before_margin")
	public int BeforeMargin;

	@XStreamAlias("after_margin")
	public int AfterMargin;

	@XStreamAlias("recording_path")
	public String	RecordingPath;
}
