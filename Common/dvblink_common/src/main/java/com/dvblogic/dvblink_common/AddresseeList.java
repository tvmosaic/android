package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("addressees")
public class AddresseeList extends Serializer
{
	@XStreamImplicit(itemFieldName = "addressee")
	private ArrayList<Addressee>	_list;

	public AddresseeList() {
		_list = new ArrayList<Addressee>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Addressee>();
		}
		return this;
	}

	public ArrayList<Addressee> list() {

		if (_list == null)
			_list = new ArrayList<>();
		return _list;
	}

	public void add(Addressee ch) {
		if (_list == null)
			_list = new ArrayList<Addressee>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Addressee get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
