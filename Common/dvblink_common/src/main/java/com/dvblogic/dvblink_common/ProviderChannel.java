package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ProviderChannel extends Serializer
{
	@XStreamAlias("channel_id")
	public String ChannelId;

	@XStreamAlias("channel_name")
	public String ChannelName;
}
