package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("favorite")
public class Favorite extends Serializer
{
	@XStreamAlias("id")
	public String	ID;

	@XStreamAlias("name")
	public String	Name;
	
	@XStreamAlias("channels")
	public FavoriteChannelIDList	channels;

	public Favorite() {
		channels = new FavoriteChannelIDList();
	}

	public boolean contains(String channel_id) {
		for (String id : channels.list()) {
			if (id.equals(channel_id))
				return true;
		}
		return false;
	}
}
