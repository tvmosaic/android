package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ScannersConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(ScannerList.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        ScannerList scannerList = new ScannerList();
        while (reader.hasMoreChildren()) {
            reader.moveDown();

            String nodeName;
            String attrID = reader.getAttribute("id");
            String attrName = reader.getAttribute("name");
            ScannerList.Standard standard = new ScannerList.Standard(attrID, attrName);
            scannerList.addStandard(standard);

            reader.moveDown();

            while (reader.hasMoreChildren()) {
                reader.moveDown();

                attrID = reader.getAttribute("id");
                attrName = reader.getAttribute("name");

                if (reader.getNodeName().equalsIgnoreCase("container")) {
                    standard.standardSettings = readContainerData(attrID, attrName, reader);
                } else {
                    ScannerList.Setting setting = new ScannerList.Setting(attrID, attrName);

                    while (reader.hasMoreChildren()) {
                        reader.moveDown();
                        nodeName = reader.getNodeName();
                        attrID = reader.getAttribute("id");
                        attrName = reader.getAttribute("name");
                        ScannerList.BaseInfo baseInfo = null;
                        if (nodeName.equalsIgnoreCase("value")) {
                            String attrDescription = reader.getAttribute("desc");
                            String attrCountry = reader.getAttribute("country");
                            baseInfo = new ScannerList.Value(attrID, attrName, attrDescription, attrCountry);
                        } else if (nodeName.equalsIgnoreCase("container")) {
                            baseInfo = readContainerData(attrID, attrName, reader);
                        }
                        if (baseInfo != null) {
                            setting.add(baseInfo);
                        }
                        reader.moveUp();
                    }
                    standard.add(setting);
                }
                reader.moveUp();
            }
            reader.moveUp();
            reader.moveUp();
        }
        return scannerList;
    }

    protected ScannerList.Container readContainerData(String id, String name, HierarchicalStreamReader reader) {
        String attrDescription = reader.getAttribute("desc");
        ScannerList.Container baseInfo = new ScannerList.Container(id, name, attrDescription);
        while(reader.hasMoreChildren()) {
            reader.moveDown();
            String ch_id = reader.getAttribute("id");
            String ch_name = reader.getAttribute("name");
            String nodeName = reader.getNodeName();
            if (nodeName.equalsIgnoreCase("edit")) {
                String ch_fmt = reader.getAttribute("format");
                baseInfo.add(new ScannerList.EditableField(ch_id, ch_name, ch_fmt, reader.getValue()));
            } else if (nodeName.equalsIgnoreCase("selection")) {
                ScannerList.SelectField select = new ScannerList.SelectField(ch_id, ch_name);
                while (reader.hasMoreChildren()) {
                    reader.moveDown();
                    nodeName = reader.getNodeName();
                    if (nodeName.equalsIgnoreCase("value")) {
                        select.add(new ScannerList.Field(
                                reader.getAttribute("id"), reader.getAttribute("name"))
                        );
                    }
                    reader.moveUp();
                }
                baseInfo.add(select);
            }
            reader.moveUp();
        }
        return baseInfo;
    }

}
