package com.dvblogic.dvblink_common;

public class ProtocolType
{
	public static final int	UNSUPPORTED_PROTOCOLS	= 0;
	public static final int	HTTP_TYPE				= 1;
	public static final int	UDP_TYPE				= 2;
	public static final int	RTSP_TYPE				= 4;
	public static final int	ASF_TYPE				= 8;
	public static final int	HLS_TYPE				= 16;
	public static final int	WEBM_TYPE				= 32;
	public static final int	ALL_SUPPORTED_PROTOCOLS	= 65535;
}
