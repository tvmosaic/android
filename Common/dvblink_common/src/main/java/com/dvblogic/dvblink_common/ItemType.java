package com.dvblogic.dvblink_common;

public class ItemType
{
	public static final int	ITEM_UNKNOWN		= -1;
	public static final int	ITEM_RECORDED_TV	= 0;
	public static final int	ITEM_VIDEO			= 1;
	public static final int	ITEM_AUDIO			= 2;
	public static final int	ITEM_IMAGE			= 3;
}
