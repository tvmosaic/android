package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("get_profiles")
public class ProfilesResponse extends Serializer
{
	@XStreamImplicit(itemFieldName = "provider")
	private ArrayList<ProviderProfilesList>	_list;

	public ProfilesResponse() {
		_list = new ArrayList<ProviderProfilesList>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<ProviderProfilesList>();
		}
		return this;
	}

	public ArrayList<ProviderProfilesList> list() {

		return _list;
	}

	public void add(ProviderProfilesList ch) {
		if (_list == null) {
			_list = new ArrayList<ProviderProfilesList>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public ProviderProfilesList get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}


