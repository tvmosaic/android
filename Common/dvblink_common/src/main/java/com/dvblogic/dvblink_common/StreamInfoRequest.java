package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("stream_info")
public class StreamInfoRequest extends Serializer
{
	@XStreamAlias("client_id")
	public String				ClientId;

	@XStreamAlias("server_address")
	public String				ServerAddress;

	@XStreamAlias("channels_dvblink_ids")
	public DVBLinkChannelIdList	DVBLinkChannelsIds;

	public StreamInfoRequest() {
		
	}
	
	public StreamInfoRequest(String server_address, String client_id, DVBLinkChannelIdList ids) {
		ServerAddress = server_address;
		ClientId = client_id;
		DVBLinkChannelsIds = ids;
	}
}
