package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("program")
public class RecommendationProgram extends Serializer
{
	@XStreamAlias("start_time")
	public long StartTime;
	
	@XStreamAlias("end_time")
	public long EndTime;
	
	@XStreamAlias("rating")
	public float Rating;
	
	@XStreamAlias("score")
	public float Score;
	
	@XStreamAlias("is_series")
	public boolean IsSeries;
	
	@XStreamAlias("event")
	public ProgramEvent Event;
}
