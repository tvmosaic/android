package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("epg_channels")
public class GetEPGChannelsRequest extends Serializer {
    @XStreamAlias("epg_source_id")
    public String epgSourceID;

    public GetEPGChannelsRequest(String epgSourceID) {
        this.epgSourceID = epgSourceID;
    }
}
