package com.dvblogic.dvblink_common;

public class TranscoderType
{
	public static final int	UNSUPPORTED_TRANSCODERS		= 0;
	public static final int	WMV_TYPE					= 1;
	public static final int	WMA_TYPE					= 2;
	public static final int	H264_TYPE					= 4;
	public static final int	AAC_TYPE					= 8;
	public static final int	RAW_TYPE					= 16;
	public static final int	ALL_SUPPORTED_TRANSCODERS	= 65535;
}
