package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("invisible_channels")
public class SetChannelsVisibilityRequest extends InvisibleChannelsList
{
}
