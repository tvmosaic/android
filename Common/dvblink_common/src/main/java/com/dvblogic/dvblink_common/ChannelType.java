package com.dvblogic.dvblink_common;

/*
 public enum ChannelType {
 RD_CHANNEL_TV (0), 
 RD_CHANNEL_RADIO (1), 
 RD_CHANNEL_OTHER (2);

 @SuppressWarnings("unused")
 private int type=0;
 private ChannelType(int _type) { this.type=_type; };
 }
 */

public class ChannelType
{
	public static final int	RD_CHANNEL_TV		= 0;
	public static final int	RD_CHANNEL_RADIO	= 1;
	public static final int	RD_CHANNEL_OTHER	= 2;

    public static final int	RD_CHANNEL_FTA	= 0;
	public static final int	RD_CHANNEL_ENCRYPTED	= 1;
}
