package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("channel")
public class EpgProgramList extends Serializer
{
	@XStreamAlias("channel_id")
	@XStreamAsAttribute
	public String ChannelId;
	
	@XStreamImplicit(itemFieldName = "program")
	private ArrayList<EpgProgram>	_list;

	public EpgProgramList() {
		_list = new ArrayList<EpgProgram>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<EpgProgram>();
		}
		return this;
	}

	public ArrayList<EpgProgram> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(EpgProgram ch) {
		if (_list == null) {
			_list = new ArrayList<EpgProgram>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public EpgProgram get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
