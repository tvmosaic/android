package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("object_remover")
public class ObjectRemover extends Serializer
{
	public ObjectRemover(String object_id) {
		ObjectID = object_id;
	}

	@XStreamAlias("object_id")
	public String	ObjectID;
}
