package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Serg on 25-Mar-16.
 */

@XStreamAlias("item")
public class SendToAddItem extends Serializer
{
    @XStreamAlias("object_id")
    public String objectId;

    @XStreamAlias("description")
    public String description;

    @XStreamAlias("target")
    public String target;
}
