package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("event")
public class ProgramEvent extends Serializer
{
	@XStreamAlias("id")
	@XStreamAsAttribute
	public String ID;

	@XStreamAlias("name")
	@XStreamAsAttribute
	public String Name;
	
	@XStreamAlias("short_desc")
	public String Description;
	
	@XStreamAlias("subname")
	public String Subname;
	
	@XStreamAlias("language")
	public String Language;
	
	@XStreamAlias("actors")
	public String Actors;
	
	@XStreamAlias("directors")
	public String Directors;
	
	@XStreamAlias("writers")
	public String Writers;
	
	@XStreamAlias("producers")
	public String Producers;
	
	@XStreamAlias("guests")
	public String Guests;
	
	@XStreamAlias("categories")
	public String Categories;
	
	@XStreamAlias("image")
	public String Image;
	
	@XStreamAlias("start_time")
	public long StartTime;
	
	@XStreamAlias("duration")
	public long Duration;
	
	@XStreamAlias("year")
	public long Year;
	
	@XStreamAlias("episode_num")
	public long EpisodeNumber;

	@XStreamAlias("season_num")
	public long SeasonNumber;

	@XStreamAlias("stars_num")
	public long StarsNumber;

	@XStreamAlias("starsmax_num")
	public long StarsMaxNumber;

	@XStreamAlias("hdtv")
	public boolean IsHdtv;

	@XStreamAlias("premiere")
	public boolean IsPremiere;

	@XStreamAlias("repeat")
	public boolean IsRepeat;

	@XStreamAlias("cat_action")
	public boolean IsAction;

	@XStreamAlias("cat_comedy")
	public boolean IsComedy;

	@XStreamAlias("cat_documentary")
	public boolean IsDocumentary;

	@XStreamAlias("cat_drama")
	public boolean IsDrama;

	@XStreamAlias("cat_educational")
	public boolean IsEducational;

	@XStreamAlias("cat_horror")
	public boolean IsHorror;

	@XStreamAlias("cat_kids")
	public boolean IsKids;

	@XStreamAlias("cat_movie")
	public boolean IsMovie;

	@XStreamAlias("cat_music")
	public boolean IsMusic;

	@XStreamAlias("cat_news")
	public boolean IsNews;

	@XStreamAlias("cat_reality")
	public boolean IsReality;

	@XStreamAlias("cat_romance")
	public boolean IsRomance;

	@XStreamAlias("cat_scifi")
	public boolean IsScifi;

	@XStreamAlias("cat_serial")
	public boolean IsSerial;

	@XStreamAlias("cat_soap")
	public boolean IsSoap;

	@XStreamAlias("cat_special")
	public boolean IsSpecial;

	@XStreamAlias("cat_sports")
	public boolean IsSports;

	@XStreamAlias("cat_thriller")
	public boolean IsThriller;

	@XStreamAlias("cat_adult")
	public boolean IsAdult;

	@XStreamAlias("cat_series")
	public boolean IsSeries;
}
