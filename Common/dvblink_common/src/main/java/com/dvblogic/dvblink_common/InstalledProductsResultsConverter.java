package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.basic.IntConverter;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


public class InstalledProductsResultsConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(InstalledProductsResults.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        InstalledProductsResults results = new InstalledProductsResults();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            if (!nodeName.equalsIgnoreCase("product")) {
                continue;
            }
            InstalledProductsResults.Product product = new InstalledProductsResults.Product();
            product.id = reader.getAttribute("id");

            while (reader.hasMoreChildren()) {
                reader.moveDown();
                nodeName = reader.getNodeName();
                if (nodeName.equalsIgnoreCase("name")) {
                    product.name = reader.getValue();
                } else if (nodeName.equalsIgnoreCase("version")) {
                    product.version = reader.getValue();
                } else if (nodeName.equalsIgnoreCase("build")) {
                    product.build = reader.getValue();
                } else if (nodeName.equalsIgnoreCase("fingerprint")) {
                    product.fingerprint = reader.getValue();
                } else if (nodeName.equalsIgnoreCase("license_state")) {
                    product.licenseState = InstalledProductsResults.LicenseState.valueOf(
                            reader.getValue().toUpperCase()
                    );
                } else if (nodeName.equalsIgnoreCase("trial_available")) {
                    product.trialAvailable = true;
                } else if (nodeName.equalsIgnoreCase("requires_registration")) {
                    product.requiresRegistration = true;
                } else if (nodeName.equalsIgnoreCase("requires_subscription")) {
                    product.requiresSubscription = true;
                } else if (nodeName.equalsIgnoreCase("requires_coupon")) {
                    product.requiresCoupon = true;
                } else if (nodeName.equalsIgnoreCase("activation_in_progress")) {
                    product.activationInProgress = true;
                } else if (nodeName.equalsIgnoreCase("days_left")) {
                    product.daysLeft = Integer.parseInt(reader.getValue());
                } else if (nodeName.equalsIgnoreCase("license_name")) {
                    product.licenseName = reader.getValue();
                } else if (nodeName.equalsIgnoreCase("license_key")) {
                    product.licenseKey = reader.getValue();
                } else if (nodeName.equalsIgnoreCase("machine_id")) {
                    product.machineId = reader.getValue();
                } else {
                    throw new RuntimeException("unknown installed products item");
                }
                reader.moveUp();
            }
            results.add(product);
            reader.moveUp();
        }
        return results;
    }
}
