package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Created by andreyp on 08.11.2016.
 */
public class ParamContainerConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(ParamContainer.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {

        ParamContainer container = (ParamContainer)value;

        writer.addAttribute("id", container.id);
        writer.addAttribute("name", container.name);
        writer.addAttribute("desc", container.desc);

        for (int i=0; i<container.editable_params.size(); i++) {
            EditableParam ep = container.editable_params.get(i);

            writer.startNode("edit");

            writer.addAttribute("id", ep.key);
            writer.addAttribute("name", ep.name);
            if (ep.format == EditableParam.EDITABLE_FORMAT_NUMBER)
                writer.addAttribute("format", "number");
            else
                writer.addAttribute("format", "string");
            writer.setValue(ep.value);

            writer.endNode();
        }

        for (int i=0; i<container.selectable_params.size(); i++) {
            SelectableParam sp = container.selectable_params.get(i);

            writer.startNode("selection");

            writer.addAttribute("id", sp.key);
            writer.addAttribute("name", sp.name);
            writer.setValue(String.format("%d", sp.selected));

            for (int j=0; j<sp.values.size(); j++) {
                SelectableParam.SelectableParamEntry pe = sp.values.get(j);
                writer.startNode("value");
                writer.addAttribute("id", pe.value);
                writer.addAttribute("name", pe.name);
                writer.endNode();
            }

            writer.endNode();
        }
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {

        ParamContainer container = new ParamContainer();

        container.id = reader.getAttribute("id");
        container.name = reader.getAttribute("name");
        container.desc = reader.getAttribute("desc");

        while (reader.hasMoreChildren()) {
            reader.moveDown();

            String nodeName = reader.getNodeName();
            if (nodeName.equals("edit")) {
                EditableParam editable = new EditableParam();
                editable.key = reader.getAttribute("id");
                editable.name = reader.getAttribute("name");
                String fmt = reader.getAttribute("format");
                if (fmt.equals("number"))
                    editable.format = EditableParam.EDITABLE_FORMAT_NUMBER;
                else
                    editable.format = EditableParam.EDITABLE_FORMAT_STRING;

                editable.value = reader.getValue();

                container.editable_params.add(editable);
            }

            if (nodeName.equals("selection")) {
                SelectableParam selectable = new SelectableParam();
                selectable.key = reader.getAttribute("id");
                selectable.name = reader.getAttribute("name");

                try {
                    selectable.selected = Integer.parseInt(reader.getValue());
                } catch (NumberFormatException e) {
                    selectable.selected = 0;
                }

                while (reader.hasMoreChildren()) {
                    reader.moveDown();
                    nodeName = reader.getNodeName();

                    if (nodeName.equals("value")) {
                        String value = reader.getAttribute("id");
                        String name = reader.getAttribute("name");
                        selectable.addParamEntry(value, name);
                    }
                    reader.moveUp();
                }
                container.selectable_params.add(selectable);
            }

            reader.moveUp();
        }
        return container;
    }
}
