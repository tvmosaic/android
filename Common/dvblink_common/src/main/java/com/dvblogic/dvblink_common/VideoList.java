package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("items")
public class VideoList extends Serializer
{
	@XStreamImplicit(itemFieldName = "video")
	private ArrayList<Video>	_list;

	public VideoList() {
		_list = new ArrayList<Video>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Video>();
		}
		return this;
	}

	public ArrayList<Video> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Video ch) {
		if (_list == null) {
			_list = new ArrayList<Video>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Video get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
