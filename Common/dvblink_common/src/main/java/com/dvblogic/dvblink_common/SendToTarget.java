package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Serg on 25-Mar-16.
 */
@XStreamAlias("target")
public class SendToTarget extends Serializer
{
    @XStreamAlias("id")
    public String id;

    @XStreamAlias("name")
    public String name;

    @XStreamAlias("delete_on_success")
    public boolean deleteOnSuccess;

    @XStreamAlias("frm_id")
    public String frmId;

    @XStreamAlias("frm_params")
    public String frmParams;

    @XStreamAlias("dst_id")
    public String dstId;

    @XStreamAlias("dst_params")
    public String dstParams;
}
