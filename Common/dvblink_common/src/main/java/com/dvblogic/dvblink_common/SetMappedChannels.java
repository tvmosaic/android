package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("set_mapped_channels")
public class SetMappedChannels extends Serializer
{
	@XStreamImplicit(itemFieldName = "provider")
	public ArrayList<MappedChannelList> list;
}
