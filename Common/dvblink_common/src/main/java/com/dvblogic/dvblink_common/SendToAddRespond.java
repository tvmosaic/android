package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Serg on 25-Mar-16.
 */
@XStreamAlias("send_to_add_item")
public class SendToAddRespond extends Serializer {
    @XStreamImplicit(itemFieldName = "item_id")
    private ArrayList<String> _list;

    public SendToAddRespond() {
        _list = new ArrayList<String>();
    }

    private Object readResolve() {
        if (_list == null) {
            _list = new ArrayList<String>();
        }
        return this;
    }

    public ArrayList<String> list() {

        return _list;
    }

    public void add(String ch) {
        if (_list == null)
            _list = new ArrayList<String>();
        _list.add(ch);
    }

    public int size() {
        return (_list == null ? 0 : _list.size());
    }

    public String get(int idx) {
        return (_list == null ? null : _list.get(idx));
    }

    public void remove(int idx) {
        if (_list != null)
            _list.remove(idx);
    }

    public void clear() {
        if (_list != null)
            _list.clear();
    }
}
