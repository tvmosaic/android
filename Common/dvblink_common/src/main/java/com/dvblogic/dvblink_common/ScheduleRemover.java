package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("remove_schedule")
public class ScheduleRemover extends Serializer
{
	@XStreamAlias("schedule_id")
	public String	ScheduleID;

	public ScheduleRemover() {
		
	}
	
	public ScheduleRemover(String schedule_id) {
		ScheduleID = schedule_id;
	}
}
