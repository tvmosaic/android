package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("streaming_caps")
public class ServerCapabilities extends Serializer
{
	@XStreamAlias("protocols")
	public int				ProtocolTypes;

	@XStreamAlias("transcoders")
	public int				TranscoderTypes;

	@XStreamAlias("pb_protocols")
	public int				PlaybackProtocolTypes;

	@XStreamAlias("pb_transcoders")
	public int				PlaybackTranscoderTypes;

	@XStreamAlias("addressees")
	public AddresseeList	Addressees;

	@XStreamAlias("can_record")
	public boolean	CanRecord;

	@XStreamAlias("supports_timeshift")
	public boolean	SupportsTimeShift;

	@XStreamAlias("timeshift_version")
	public int	TimeShiftVersion;

	@XStreamAlias("device_management")
	public boolean	DeviceManagement;


	public boolean IsSocialSupported() {
		boolean ret_val = false;

		if (Addressees != null) {
			for (int idx = 0; idx < Addressees.size(); idx++) {
				Addressee a = Addressees.get(idx);
				if (a.ID.equals(DataProvider.SOCIAL_MODULE_ADDRESSEE)) {
					ret_val = true;
					break;
				}
			}
		}

		return ret_val;
	}

	public boolean IsHttpSupported() {
		return (ProtocolTypes & ProtocolType.HTTP_TYPE) != 0;
	}

	public boolean IsUdpSupported() {
		return (ProtocolTypes & ProtocolType.UDP_TYPE) != 0;
	}

	public boolean IsRtspSupported() {
		return (ProtocolTypes & ProtocolType.RTSP_TYPE) != 0;
	}

	public boolean IsAsfSupported() {
		return (ProtocolTypes & ProtocolType.ASF_TYPE) != 0;
	}

	public boolean IsHlsSupported() {
		return (ProtocolTypes & ProtocolType.HLS_TYPE) != 0;
	}

	public boolean IsWebmSupported() {
		return (ProtocolTypes & ProtocolType.WEBM_TYPE) != 0;
	}

	// /
	public boolean IsPlaybackHttpSupported() {
		return (PlaybackProtocolTypes & ProtocolType.HTTP_TYPE) != 0;
	}

	public boolean IsPlaybackUdpSupported() {
		return (PlaybackProtocolTypes & ProtocolType.UDP_TYPE) != 0;
	}

	public boolean IsPlaybackRtspSupported() {
		return (PlaybackProtocolTypes & ProtocolType.RTSP_TYPE) != 0;
	}

	public boolean IsPlaybackAsfSupported() {
		return (PlaybackProtocolTypes & ProtocolType.ASF_TYPE) != 0;
	}

	public boolean IsPlaybackHlsSupported() {
		return (PlaybackProtocolTypes & ProtocolType.HLS_TYPE) != 0;
	}

	public boolean IsPlaybackWebmSupported() {
		return (PlaybackProtocolTypes & ProtocolType.WEBM_TYPE) != 0;
	}

	// /
	public boolean IsWmvSupported() {
		return (TranscoderTypes & TranscoderType.WMV_TYPE) != 0;
	}

	public boolean IsWmaSupported() {
		return (TranscoderTypes & TranscoderType.WMA_TYPE) != 0;
	}

	public boolean IsH264Supported() {
		return (TranscoderTypes & TranscoderType.H264_TYPE) != 0;
	}

	public boolean IsAacSupported() {
		return (TranscoderTypes & TranscoderType.AAC_TYPE) != 0;
	}

	public boolean IsRawSupported() {
		return (TranscoderTypes & TranscoderType.RAW_TYPE) != 0;
	}

	// /
	public boolean IsPlaybackWmvSupported() {
		return (PlaybackTranscoderTypes & TranscoderType.WMV_TYPE) != 0;
	}

	public boolean IsPlaybackWmaSupported() {
		return (PlaybackTranscoderTypes & TranscoderType.WMA_TYPE) != 0;
	}

	public boolean IsPlaybackH264Supported() {
		return (PlaybackTranscoderTypes & TranscoderType.H264_TYPE) != 0;
	}

	public boolean IsPlaybackAacSupported() {
		return (PlaybackTranscoderTypes & TranscoderType.AAC_TYPE) != 0;
	}

	public boolean IsPlaybackRawSupported() {
		return (PlaybackTranscoderTypes & TranscoderType.RAW_TYPE) != 0;
	}
}
