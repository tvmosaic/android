package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("recordings")
public class RecordingsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "recording")
	private ArrayList<Recording>	_list;

	public RecordingsList() {
		_list = new ArrayList<Recording>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Recording>();
		}
		return this;
	}

	public ArrayList<Recording> list() {

        if (_list == null)
            _list = new ArrayList<>();

		return _list;
	}

	public void add(Recording ch) {
		if (_list == null) {
			_list = new ArrayList<Recording>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Recording get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
