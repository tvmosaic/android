package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("epg_searcher")
public class ChannelIdWithProgramsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "channel_epg")
	private ArrayList<ChannelIdWithPrograms>	_list;

	public ChannelIdWithProgramsList() {
		_list = new ArrayList<ChannelIdWithPrograms>();
	}

	public ArrayList<ChannelIdWithPrograms> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(ChannelIdWithPrograms ch) {
		if (_list == null)
			_list = new ArrayList<ChannelIdWithPrograms>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public ChannelIdWithPrograms get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
