package com.dvblogic.dvblink_common;

/**
 * Created by Serg on 01-Sep-16.
 */

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("timeshift_seek")
public class TimeShiftSeek  extends Serializer
{
    @XStreamAlias("channel_handle")
    public long channel_handle;

    @XStreamAlias("type")
    public long type; // 0 – by bytes, 1 – by time

    // int64 mandatory, offset in bytes (for seek by bytes) or in seconds (for seek by time).
    // Offset may be negative value and is calculated from a position, given by whence parameter
    @XStreamAlias("offset")
    public long offset;

    // 0 – offset is calculated from the beginning of the timeshift buffer,
    // 1 – offset is calculated from the current playback position,
    // 2 – offset is calculated from the end of the timeshift buffer
    @XStreamAlias("whence")
    public long whence;
}
