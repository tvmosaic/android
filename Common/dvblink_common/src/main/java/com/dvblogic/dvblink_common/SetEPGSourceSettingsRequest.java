package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("epg_source_settings")
public class SetEPGSourceSettingsRequest extends Serializer {

    @XStreamAlias("settings")
    public ConciseParamMap epgSourceSettings;

    public SetEPGSourceSettingsRequest(ConciseParamMap settings) {
        this.epgSourceSettings = settings;
    }
}
