package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by andreyp on 08.11.2016.
 */
@XStreamAlias("get_scanners")
public class ScannersRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    public ScannersRequest(String id) {
        deviceID = id;
    }
}
