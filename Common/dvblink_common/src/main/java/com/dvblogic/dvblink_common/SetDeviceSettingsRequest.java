package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("device_settings")
public class SetDeviceSettingsRequest extends Serializer {

    @XStreamAlias("device")
    public String deviceID;

    @XStreamAlias("settings")
    public ConciseParamMap deviceParams;

    public SetDeviceSettingsRequest(String id, ConciseParamMap deviceParams) {
        this.deviceID = id;
        this.deviceParams = deviceParams;
    }
}
