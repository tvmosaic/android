package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamConverter(ChannelListConverter.class)
@XStreamAlias("channels")
public class ChannelsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "channel")
	private ArrayList<Channel>	_list;

	public ChannelsList() {
		_list = new ArrayList<Channel>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Channel>();
		}
		return this;
	}

	public ArrayList<Channel> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Channel ch) {
		if (_list == null)
			_list = new ArrayList<Channel>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Channel get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
