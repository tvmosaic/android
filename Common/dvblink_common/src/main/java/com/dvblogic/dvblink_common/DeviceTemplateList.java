package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamConverter(DeviceTemplateListConverter.class)
@XStreamAlias("device_templates")
public class DeviceTemplateList  extends Serializer {
    public ParamContainer template_container = new ParamContainer();
}


