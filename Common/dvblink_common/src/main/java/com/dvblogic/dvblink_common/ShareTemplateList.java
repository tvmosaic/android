package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("templates")
public class ShareTemplateList extends Serializer
{
	@XStreamImplicit(itemFieldName = "template")
	private ArrayList<ShareTemplate>	_list;

	public ShareTemplateList() {
		_list = new ArrayList<ShareTemplate>();
	}

	public ArrayList<ShareTemplate> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(ShareTemplate ch) {
		if (_list == null) {
			_list = new ArrayList<ShareTemplate>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public ShareTemplate get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
