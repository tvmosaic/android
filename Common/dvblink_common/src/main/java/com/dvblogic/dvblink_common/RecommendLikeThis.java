package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("recommend_like_this")
public class RecommendLikeThis extends Serializer
{
	@XStreamAlias("dvblink_channel_id")
	public String	DVBLinkChannelId;

	@XStreamAlias("start_time")
	public long		StartTime;

	@XStreamAlias("end_time")
	public long		EndTime;

	@XStreamAlias("recommendations_from")
	public long		RecommendetionsFrom;

	@XStreamAlias("recommendations_to")
	public long		RecommendetionsTo;

	@XStreamAlias("extended_epg")
	public boolean	ExtendedEpg;
}
