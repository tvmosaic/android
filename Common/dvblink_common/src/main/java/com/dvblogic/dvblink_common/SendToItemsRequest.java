package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Serg on 25-Mar-16.
 */

@XStreamAlias("send_to_get_items")
public class SendToItemsRequest extends Serializer
{
    @XStreamAlias("type")
    public int type;

    public SendToItemsRequest() {

    }
    public SendToItemsRequest(int _type) {
        type = _type;
    }
}
