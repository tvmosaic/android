package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;


@XStreamAlias("epg_channels_config")
public class GetEPGChannelConfigRequest extends Serializer {
    @XStreamImplicit(itemFieldName = "channel")
    private ArrayList<EPGChannel> _list;

    public GetEPGChannelConfigRequest() {
        _list = new ArrayList<>();
    }

    private Object readResolve() {
        if (_list == null) {
            _list = new ArrayList<>();
        }
        return this;
    }

    public ArrayList<EPGChannel> list() {

        return _list;
    }

    public void add(EPGChannel ch) {
        if (_list == null)
            _list = new ArrayList<>();
        _list.add(ch);
    }

    public int size() {
        return (_list == null ? 0 : _list.size());
    }

    public EPGChannel get(int idx) {
        return (_list == null ? null : _list.get(idx));
    }

    public void remove(int idx) {
        if (_list != null)
            _list.remove(idx);
    }

    public void clear() {
        if (_list != null)
            _list.clear();
    }
}
