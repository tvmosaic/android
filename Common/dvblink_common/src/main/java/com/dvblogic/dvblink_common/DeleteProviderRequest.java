package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("drop_provider")
public class DeleteProviderRequest extends Serializer {
    @XStreamAlias("device_id")
    public String deviceID;

    @XStreamAlias("provider_id")
    public String providerID;
	
    public DeleteProviderRequest(String deviceID_, String providerID_) {
        deviceID = deviceID_;
		providerID = providerID_;
    }
}
