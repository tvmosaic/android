package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamConverter(ScannedChannelsListConverter.class)
@XStreamAlias("providers")
public class ScannedChannelsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "provider")
	private ArrayList<Headend>	_headends;

	public ScannedChannelsList() {
		_headends = new ArrayList<Headend>();
	}

	private Object readResolve() {
		if (_headends == null) {
			_headends = new ArrayList<Headend>();
		}
		return this;
	}

	public ArrayList<Headend> list() {
		if (_headends == null)
			_headends = new ArrayList<>();

		return _headends;
	}

	public void add(Headend h) {
		if (_headends == null) {
			_headends = new ArrayList<Headend>();
		}
		_headends.add(h);
	}

	public int size() {
		return (_headends == null ? 0 : _headends.size());
	}

	public Headend get(int idx) {
		return (_headends == null ? null : _headends.get(idx));
	}

	public void remove(int idx) {
		if (_headends != null)
			_headends.remove(idx);
	}

	public void clear() {
		if (_headends != null)
			_headends.clear();
	}
}
