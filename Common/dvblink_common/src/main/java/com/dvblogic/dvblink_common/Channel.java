package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("channel")
public class Channel extends Serializer
{
	public Channel() {
        Encrypted = 0;
	}

	@XStreamAlias("channel_id")
	public String	ID;

	@XStreamAlias("channel_dvblink_id")
	public String		DVBLinkID;

	@XStreamAlias("channel_name")
	public String	Name;

	@XStreamAlias("channel_number")
	public int		Number;

	@XStreamAlias("channel_subnumber")
	public int		Subnumber;

	@XStreamAlias("channel_type")
	// public ChannelType Type;
	public int		Type;
	
	@XStreamAlias("channel_child_lock")
	public boolean	ChannelChildLock;

    @XStreamAlias("channel_logo")
    public String	ChannelLogo;

    @XStreamAlias("channel_encrypted")
    public int	Encrypted;
}
