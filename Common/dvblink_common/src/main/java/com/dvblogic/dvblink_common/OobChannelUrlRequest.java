package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("get_oob_channel_url")
public class OobChannelUrlRequest extends Serializer {
	
	public static int OobChannelUrlFormatRaw = 0;
	public static int OobChannelUrlFormatChromcast = 1;
	
    @XStreamAlias("channel_id")
    public String channelID;

    @XStreamAlias("format")
    public int format;
	
    public OobChannelUrlRequest(String channelID_, int format_) {
        channelID = channelID_;
		format = format_;
    }
}
