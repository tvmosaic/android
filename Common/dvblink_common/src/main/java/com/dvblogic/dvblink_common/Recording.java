package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("recording")
public class Recording extends Serializer
{
	@XStreamAlias("recording_id")
	public String	RecordingID;

	@XStreamAlias("schedule_id")
	public String	ScheduleID;

	@XStreamAlias("channel_id")
	public String	ChannelID;

	@XStreamAlias("is_active")
	public boolean	IsActive;

	@XStreamAlias("is_conflict")
	public boolean	IsConflict;

	@XStreamAlias("program")
	public Program	Program;
}
