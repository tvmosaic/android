package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.util.ArrayList;

/**
 * Created by andreyp on 14.11.2016.
 */
@XStreamConverter(NetworkListConverter.class)
@XStreamAlias("networks")
public class NetworkList {

    private ArrayList<Setting> settings_ = new ArrayList<Setting>();
    public ArrayList<Setting> list() {
        return settings_;
    }

    public void add(Setting setting) {
        settings_.add(setting);
    }

    static public class Setting {
        public String id_;
        public String name_;

        Setting(String id, String name) {
            id_ = id;
            name_ = name;
        }

        @Override
        public String toString() {
			if (name_.length() == 0)
				return id_;
			
            return name_ + "(" + id_ + ")";
        }
    }
}
