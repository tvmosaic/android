package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Serg on 22-Apr-16.
 */
@XStreamAlias("server_info")
public class ServerVersion extends Serializer
{
    public transient static int	UDPPort	= 65432;

    @XStreamAlias("install_id")
    public String				InstallId;

    @XStreamAlias("server_id")
    public String				ServerId;

    @XStreamAlias("version")
    public String				Version;

    @XStreamAlias("build")
    public int					Build;

    public ServerVersion() {
    }

    public boolean available(Character ch) {
        return Version.charAt(0) >= ch;
    }
}
