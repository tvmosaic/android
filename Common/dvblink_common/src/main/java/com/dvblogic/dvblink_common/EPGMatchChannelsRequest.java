package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("match_info")
public class EPGMatchChannelsRequest extends Serializer {

	@XStreamAlias("epg_source_id")
	public String epgSourceId;

	@XStreamAlias("channels")
	public EPGMatchChannelListRequest channels = new EPGMatchChannelListRequest();
}
