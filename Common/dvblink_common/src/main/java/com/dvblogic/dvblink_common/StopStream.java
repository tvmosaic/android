package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("stop_stream")
public class StopStream extends Serializer
{
	@XStreamAlias("channel_handle")
	public long		ChannelHandle;

	@XStreamAlias("client_id")
	public String	ClientId;

	public StopStream() {
	}

	public StopStream(long channel_handle) {
		ChannelHandle = channel_handle;
	}

	public StopStream(String client_id) {
		ClientId = client_id;
	}
}
