package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("parental_status")
public class ParentalStatus extends Serializer
{
	@XStreamAlias("is_enabled")
	public boolean	IsEnabled;
}
