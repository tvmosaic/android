package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("invisible_channels")
public class InvisibleChannelsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "channel")
	private ArrayList<InvisibleChannel>	_list;

	public InvisibleChannelsList() {
		_list = new ArrayList<InvisibleChannel>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<InvisibleChannel>();
		}
		return this;
	}

	public ArrayList<InvisibleChannel> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(InvisibleChannel ch) {
		if (_list == null)
			_list = new ArrayList<InvisibleChannel>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public InvisibleChannel get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
