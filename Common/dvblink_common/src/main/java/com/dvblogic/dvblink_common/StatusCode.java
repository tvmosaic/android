package com.dvblogic.dvblink_common;

public enum StatusCode {
	STATUS_OK(0), 
	STATUS_ERROR(1000), 
	STATUS_INVALID_DATA(1001), 
	STATUS_INVALID_PARAM(1002), 
	STATUS_NOT_IMPLEMENTED(1003), 
	STATUS_SOCKET_ERROR(1004), 
	STATUS_MC_NOT_RUNNING(1005), 
	STATUS_NO_DEFAULT_RECORDER(1006), 
	STATUS_BUFFER_TOO_SMALL(1007), 
	STATUS_MCE_CONNECTION_ERROR(1008), 
	STATUS_ADRESSEE_NOT_FOUND(1009), 
	STATUS_TIME_OUT(1010), 
	STATUS_HANDLER_NOT_REGISTERED(1011),
	STATUS_PRODUCT_NOT_ACTIVATED(1012),
	STATUS_NO_FREE_TUNER(1013),
	STATUS_CONNECTION_ERROR(2000),
	STATUS_UNAUTHORISED(2001);

	private int	code;

	StatusCode(int _code) {
		this.code = _code;
	}

	static public boolean equals(StatusCode ecode, int icode) {
		return (ecode.code == icode);
	}

	static public boolean success(int icode) {
		return equals(STATUS_OK, icode);
	}

	static public int toInt(StatusCode ecode) {
		return ecode.code;
	}

	static public StatusCode fromInt(int icode) {
		if (equals(STATUS_OK, icode))
			return STATUS_OK;
		else if (equals(STATUS_INVALID_DATA, icode))
			return STATUS_INVALID_DATA;
		else if (equals(STATUS_INVALID_PARAM, icode))
			return STATUS_INVALID_PARAM;
		else if (equals(STATUS_NOT_IMPLEMENTED, icode))
			return STATUS_NOT_IMPLEMENTED;
		else if (equals(STATUS_SOCKET_ERROR, icode))
			return STATUS_SOCKET_ERROR;
		else if (equals(STATUS_MC_NOT_RUNNING, icode))
			return STATUS_MC_NOT_RUNNING;
		else if (equals(STATUS_NO_DEFAULT_RECORDER, icode))
			return STATUS_NO_DEFAULT_RECORDER;
		else if (equals(STATUS_BUFFER_TOO_SMALL, icode))
			return STATUS_BUFFER_TOO_SMALL;
		else if (equals(STATUS_MCE_CONNECTION_ERROR, icode))
			return STATUS_MCE_CONNECTION_ERROR;
		else if (equals(STATUS_ADRESSEE_NOT_FOUND, icode))
			return STATUS_ADRESSEE_NOT_FOUND;
		else if (equals(STATUS_MCE_CONNECTION_ERROR, icode))
			return STATUS_MCE_CONNECTION_ERROR;
		else if (equals(STATUS_TIME_OUT, icode))
			return STATUS_TIME_OUT;
		else if (equals(STATUS_HANDLER_NOT_REGISTERED, icode))
			return STATUS_HANDLER_NOT_REGISTERED;
		else if (equals(STATUS_PRODUCT_NOT_ACTIVATED, icode))
			return STATUS_PRODUCT_NOT_ACTIVATED;
		else if (equals(STATUS_NO_FREE_TUNER, icode))
			return STATUS_NO_FREE_TUNER;
		else if (equals(STATUS_UNAUTHORISED, icode))
			return STATUS_UNAUTHORISED;
		return STATUS_ERROR;
	}
}
