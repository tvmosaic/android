package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import com.thoughtworks.xstream.mapper.Mapper;

import java.lang.reflect.Field;

public class PlaybackItemListConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(PlaybackItemList.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        PlaybackItemList pbItemList = new PlaybackItemList();

        while (reader.hasMoreChildren()) {
            reader.moveDown();

            String name = reader.getNodeName();
            String val = reader.getValue();

            if (name.equals("video")) {
                VideoItem vi = null;
                try {
                    vi = (VideoItem) Serializer.deserialize(reader, VideoItem.class);
                } catch (Exception e) {vi = null;}

                if (vi != null)
                    pbItemList.add(vi);
            } else
            if (name.equals("recorded_tv")) {
                RecordedTV rtvi = null;
                try {
                    rtvi = (RecordedTV) Serializer.deserialize(reader, RecordedTV.class);
                } catch (Exception e) {rtvi = null;}

                if (rtvi != null)
                    pbItemList.add(rtvi);
            }

            reader.moveUp();
        }
        return pbItemList;
    }
}
