package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("server_settings_list")
public class ServerSettingsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "ServerSettings")
	private ArrayList<ServerSettings>	_list;

	public ServerSettingsList() {
		_list = new ArrayList<ServerSettings>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<ServerSettings>();
		}
		return this;
	}

	public ArrayList<ServerSettings> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(ServerSettings ch) {
		if (_list == null)
			_list = new ArrayList<ServerSettings>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public ServerSettings get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
	
	public void edit(ServerSettings _ss, boolean new_server, String current_server_id) {
		if (new_server) {
			add(_ss);
		} else {
			for (ServerSettings ss : list()) {
				if (ss.serverId.equals(current_server_id)) {
					ss.serverAddress = _ss.serverAddress;
					ss.serverPort = _ss.serverPort;
					ss.password = _ss.password;
					ss.serverName = _ss.serverName;
					ss.transcodingSupported = _ss.transcodingSupported;
					ss.userName = _ss.userName;
					ss.useHttps = _ss.useHttps;
					return;
				}
			}
		}
	}

	public void save(Settings set) {
		String xml = Serializer.serialize(this);
		set.setString("server_settings_list", xml);
	}
	
	public static ServerSettingsList load(Settings set) {
		String xml = set.getString("server_settings_list");
		return (ServerSettingsList)Serializer.deserialize(xml, ServerSettingsList.class);  
	}
}
