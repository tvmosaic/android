package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Serg on 25-Mar-16.
 */

@XStreamAlias("get_targets")
public class SendToTargetsRespond extends Serializer {
    @XStreamImplicit(itemFieldName = "target")
    private ArrayList<SendToTarget> _list;

    public SendToTargetsRespond() {
        _list = new ArrayList<SendToTarget>();
    }

    private Object readResolve() {
        if (_list == null) {
            _list = new ArrayList<SendToTarget>();
        }
        return this;
    }

    public ArrayList<SendToTarget> list() {

        return _list;
    }

    public void add(SendToTarget ch) {
        if (_list == null)
            _list = new ArrayList<SendToTarget>();
        _list.add(ch);
    }

    public int size() {
        return (_list == null ? 0 : _list.size());
    }

    public SendToTarget get(int idx) {
        return (_list == null ? null : _list.get(idx));
    }

    public void remove(int idx) {
        if (_list != null)
            _list.remove(idx);
    }

    public void clear() {
        if (_list != null)
            _list.clear();
    }

    public SendToTarget getById(String target_id) {
        SendToTarget res = null;
        if (_list != null) {
            for (int i=0; i<_list.size(); i++) {
                if (_list.get(i).id.equalsIgnoreCase(target_id)) {
                    res = _list.get(i);
                    break;
                }
            }
        }
        return res;
    }
}
