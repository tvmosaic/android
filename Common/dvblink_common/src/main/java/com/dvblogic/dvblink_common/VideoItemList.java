package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("items")
public class VideoItemList extends Serializer
{
	@XStreamImplicit(itemFieldName = "video")
	private ArrayList<VideoItem>	_list;

	public VideoItemList() {
		_list = new ArrayList<VideoItem>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<VideoItem>();
		}
		return this;
	}

	public ArrayList<VideoItem> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}


	public void add(VideoItem ch) {
		if (_list == null) {
			_list = new ArrayList<VideoItem>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public VideoItem get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
