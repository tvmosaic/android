package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamConverter(ProgramListConverter.class)
@XStreamAlias("dvblink_epg")
public class ProgramsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "program")
	private ArrayList<Program>	_list;

	public ProgramsList() {
		_list = new ArrayList<Program>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Program>();
		}
		return this;
	}

	public ArrayList<Program> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Program ch) {
		if (_list == null) {
			_list = new ArrayList<Program>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Program get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
