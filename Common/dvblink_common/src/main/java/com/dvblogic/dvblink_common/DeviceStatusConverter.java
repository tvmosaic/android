package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


/**
 * Created by andreyp on 14.11.2016.
 */
public class DeviceStatusConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(DeviceStatus.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {

        DeviceStatus deviceStatus = new DeviceStatus();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            if (nodeName.equalsIgnoreCase("status")) {
                deviceStatus.status = reader.getValue();
            }
            if (nodeName.equalsIgnoreCase("scanning")) {
                while (reader.hasMoreChildren()) {
                    reader.moveDown();
                    nodeName = reader.getNodeName();
                    if (nodeName.equalsIgnoreCase("progress")) {
                        deviceStatus.progress = Integer.parseInt(reader.getValue());
                    }
                    if (nodeName.equalsIgnoreCase("channels")) {
                        deviceStatus.channels = Integer.parseInt(reader.getValue());
                    }
                    if (nodeName.equalsIgnoreCase("transponders")) {
                        while (reader.hasMoreChildren()) {
                            reader.moveDown();
                            nodeName = reader.getNodeName();
                            if (nodeName.equalsIgnoreCase("transponder")) {
                                deviceStatus.transponders_.add(new DeviceStatus.Transponder(
                                        reader.getAttribute("id"), reader.getValue()
                                ));
                            }
                            reader.moveUp();
                        }
                    }
                    reader.moveUp();
                }
            }
            reader.moveUp();
        }
        return deviceStatus;
    }
}
