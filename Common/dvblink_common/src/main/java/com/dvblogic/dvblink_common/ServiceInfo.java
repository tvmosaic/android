package com.dvblogic.dvblink_common;

import android.annotation.SuppressLint;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("server_info")
public class ServiceInfo extends Serializer
{
	@XStreamAlias("server_name")
	public String				ServiceName;

	@XStreamAlias("server_address")
	public String				ServiceAdress;

	@XStreamAlias("server_id")
	public String				ServiceId;

	@XStreamAlias("base_streaming_port")
	public int					BaseStreamPort;

	public ServiceInfo() {
	}

	public ServiceInfo(String serviceName, String serviceAdress, String serviceId, int serviceBasePort,
			int baseStreamPort) {
		ServiceName = serviceName;
		ServiceAdress = serviceAdress;
		ServiceId = serviceId;
		BaseStreamPort = baseStreamPort;
	}

	@SuppressLint("DefaultLocale")
	public String string() {
		return String.format("Address: %s, sream port: %d, id: %s, name: %s", ServiceAdress,
				BaseStreamPort, ServiceId, ServiceName);
	}

	public String getHash() {
        return String.format("%s:%d", ServiceAdress, BaseStreamPort);
    }

    public boolean Equals(ServiceInfo serviceInfo) {
        return serviceInfo.ServiceAdress.equals(ServiceAdress)
                && serviceInfo.ServiceName.equals(ServiceName)
                && serviceInfo.ServiceId.equals(ServiceId)
                && serviceInfo.BaseStreamPort == BaseStreamPort;
    }
}
