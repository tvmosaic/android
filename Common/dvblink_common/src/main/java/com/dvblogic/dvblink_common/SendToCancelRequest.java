package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by Serg on 25-Mar-16.
 */

@XStreamAlias("send_to_cancel_item")
public class SendToCancelRequest extends Serializer
{
    @XStreamAlias("item_id")
   public String itemId;

    public SendToCancelRequest(String id) {
        itemId = id;
    }

    public SendToCancelRequest() {
        itemId = "";
    }
}
