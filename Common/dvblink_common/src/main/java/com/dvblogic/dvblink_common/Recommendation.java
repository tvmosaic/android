package com.dvblogic.dvblink_common;


import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("channel")
public class Recommendation extends Serializer
{
	@XStreamAlias("dvblink_channel_id")
	@XStreamAsAttribute
	public String		DVBLinkID;

	@XStreamImplicit(itemFieldName = "program")
	public ArrayList<RecommendationProgram> Programs;
}
