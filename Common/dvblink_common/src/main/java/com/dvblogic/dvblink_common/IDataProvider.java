package com.dvblogic.dvblink_common;

public interface IDataProvider
{
	void DataProcessing(String command, Object result_data);
	void ErrorProcessing(String command, StatusCode error);
}
