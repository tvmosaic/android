package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.ReflectionConverter;
import com.thoughtworks.xstream.converters.reflection.ReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.mapper.Mapper;

import java.lang.reflect.Field;


public class ByEpgScheduleConverter extends ReflectionConverter 
{
	public ByEpgScheduleConverter(Mapper mapper, ReflectionProvider reflectionProvider) {
		super(mapper, reflectionProvider);
	}

	@Override
	public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {
		ByEpgSchedule sch = new ByEpgSchedule();
		boolean found = false;
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			String name = reader.getNodeName();
			String val = reader.getValue();
			if (name.equals("channel_id"))
				sch.ChannelID =val;
			else if (name.equals("program_id"))
				sch.ProgramID =val; 
			else if (name.equals("repeat"))
				sch.IsRepeat = val.isEmpty() || Boolean.parseBoolean(val);
			else if (name.equals("new_only"))
				sch.NewOnly = val.isEmpty() || Boolean.parseBoolean(val);
			else if (name.equals("record_series_anytime")) {
				sch.RecordSeriesAnytime = val.isEmpty() || Boolean.parseBoolean(val);
				found = true;
			} else if (name.equals("recordings_to_keep")) 
				sch.RecordingsToKeep = Integer.valueOf(val);
			else if (name.equals("start_before"))
				sch.StartBefore = Integer.valueOf(val);
			else if (name.equals("start_after"))
				sch.StartAfter = Integer.valueOf(val);
			else if (name.equals("day_mask"))
				sch.DayMask = Integer.valueOf(val);
			else if (name.equals("program")) {
				try {
                    Field fl = ByEpgSchedule.class.getDeclaredField("Program");
                    if (fl != null) {
                        fl.setAccessible(true);
                        sch.Program = (Program) super.unmarshallField(context, sch.Program, Program.class, fl);
                    }
				} catch (NoSuchFieldException e) {
					e.printStackTrace();
				}
			}
			reader.moveUp();
		}

		if (!found)
			sch.RecordSeriesAnytime = true;
		return sch;
	}

    @SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(Class clazz) {
        return clazz == ByEpgSchedule.class;
	}
}
