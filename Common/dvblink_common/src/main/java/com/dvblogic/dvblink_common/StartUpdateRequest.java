package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("updater_start_update")
public class StartUpdateRequest extends Serializer {}
