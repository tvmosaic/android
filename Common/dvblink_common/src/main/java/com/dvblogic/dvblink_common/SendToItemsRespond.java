package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Serg on 25-Mar-16.
 */

@XStreamAlias("send_to_get_items")
public class SendToItemsRespond extends Serializer {
    @XStreamImplicit(itemFieldName = "item")
    private ArrayList<SendToItem> _list;

    public SendToItemsRespond() {
        _list = new ArrayList<SendToItem>();
    }

    private Object readResolve() {
        if (_list == null) {
            _list = new ArrayList<SendToItem>();
        }
        return this;
    }

    public ArrayList<SendToItem> list() {

        return _list;
    }

    public void add(SendToItem ch) {
        if (_list == null)
            _list = new ArrayList<SendToItem>();
        _list.add(ch);
    }

    public int size() {
        return (_list == null ? 0 : _list.size());
    }

    public SendToItem get(int idx) {
        return (_list == null ? null : _list.get(idx));
    }

    public void remove(int idx) {
        if (_list != null)
            _list.remove(idx);
    }

    public void clear() {
        if (_list != null)
            _list.clear();
    }
}
