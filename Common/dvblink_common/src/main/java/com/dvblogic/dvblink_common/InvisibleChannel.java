package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("channel")
public class InvisibleChannel extends Serializer {
	@XStreamAsAttribute
	@XStreamAlias("id")
	public String id;
}
