package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("get_epg")
public class EpgRequest extends Serializer
{
	@XStreamAlias("start_time")
	public long		StartTime;

	@XStreamAlias("end_time")
	public long		EndTime;

	@XStreamAlias("channels")	
	public ChannelIDList Channels;
	
	public EpgRequest() {
		Channels = new ChannelIDList();
	}
}
