package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("provider")
public class MappedChannelList extends Serializer
{
	@XStreamAlias("provider_id")
	@XStreamAsAttribute
	public String ProviderId;
	
	@XStreamImplicit(itemFieldName = "channel")
	private ArrayList<MappedChannel>	_list;

	public MappedChannelList() {
		_list = new ArrayList<MappedChannel>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<MappedChannel>();
		}
		return this;
	}

	public ArrayList<MappedChannel> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(MappedChannel ch) {
		if (_list == null) {
			_list = new ArrayList<MappedChannel>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public MappedChannel get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
