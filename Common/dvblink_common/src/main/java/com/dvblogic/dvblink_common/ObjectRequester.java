package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("object_requester")
public class ObjectRequester extends Serializer
{
	public static final String	OBJECT_ROOT_ID			= "";
	public static final int		OBJECT_COUNT_ALL		= -1;
	public static final String	DLRECORDER_SOURCE_ID	= "8F94B459-EFC0-4D91-9B29-EC3D72E92677";
	public static final String	DLRECORDER_RECORDINGS_BY_NAME_ID	= "E44367A7-6293-4492-8C07-0E551195B99F";
	public static final String	DLRECORDER_RECORDINGS_BY_DATE_ID	= "F6F08949-2A07-4074-9E9D-423D877270BB";
	public static final String	DLRECORDER_RECORDINGS_BY_GENRE_ID	= "CE482DD8-BC5E-47c3-9072-2554B968F27C";
	public static final String	DLRECORDER_RECORDINGS_BY_SERIES_ID	= "0E03FEB8-BD8F-46e7-B3EF-34F6890FB458";

	public ObjectRequester() {
	}
	
	public ObjectRequester(String object_id, String server_address) {
		ObjectID = object_id;
		ServerAddress = server_address;
		ObjectType = com.dvblogic.dvblink_common.ObjectType.OBJECT_UNKNOWN;
		ItemType = com.dvblogic.dvblink_common.ItemType.ITEM_UNKNOWN;
		RequestedCount = OBJECT_COUNT_ALL;
		IsChildrenRequest = true;
		StartPosition = 0;
	}

	@XStreamAlias("object_id")
	public String	ObjectID;

	@XStreamAlias("server_address")
	public String	ServerAddress;

	// ObjectType
	@XStreamAlias("object_type")
	public int		ObjectType;

	@XStreamAlias("item_type")
	public int		ItemType;

	@XStreamAlias("start_position")
	public int		StartPosition;

	@XStreamAlias("requested_count")
	public int		RequestedCount;

	@XStreamAlias("children_request")
	public boolean	IsChildrenRequest;
}
