package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("get_mapped_channels")
public class MappedChannelsResponse extends Serializer
{
	@XStreamImplicit(itemFieldName = "provider")
	private ArrayList<MappedChannelList> _list;

	public MappedChannelsResponse() {
		_list = new ArrayList<MappedChannelList>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<MappedChannelList>();
		}
		return this;
	}

	public ArrayList<MappedChannelList> list() {

		return _list;
	}

	public void add(MappedChannelList ch) {
		if (_list == null) {
			_list = new ArrayList<MappedChannelList>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public MappedChannelList get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
