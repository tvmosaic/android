package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.util.ArrayList;

@XStreamConverter(DeviceStatusConverter.class)
@XStreamAlias("device_status")
public class DeviceStatus {
    public String status;
    public Integer progress;
    public Integer channels;

    ArrayList<Transponder> transponders_ = new ArrayList<Transponder>();

    public ArrayList<Transponder> list() {
        return transponders_;
    }

    public static final String STATUS_IDLE = "idle";
    public static final String STATUS_STREAMING = "streaming";
    public static final String STATUS_SCANNING = "scanning";
    public static final String STATUS_SCAN_FINISHED = "scan_finished";
    public static final String STATUS_NETWORKS_SCANNED = "networks_scanned";

    static public class Transponder {
        public String name;
        public String id;

        public Transponder(String id_, String name_) {
            name = name_;
            id = id_;
        }

        @Override
        public String toString() {
            return name;
        }
    }
}
