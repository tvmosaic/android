package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("streaming_caps")
public class StreamingCapabilities extends Serializer
{
	@XStreamAlias("protocols")
	public int	ProtocolTypes;

	@XStreamAlias("transcoders")
	public int	TranscoderTypes;

	public boolean IsHttpSupported() {
		return (ProtocolTypes & ProtocolType.HTTP_TYPE) != 0;
	}

	public boolean IsUdpSupported() {
		return (ProtocolTypes & ProtocolType.UDP_TYPE) != 0;
	}

	public boolean IsRtspSupported() {
		return (ProtocolTypes & ProtocolType.RTSP_TYPE) != 0;
	}

	public boolean IsAsfSupported() {
		return (ProtocolTypes & ProtocolType.ASF_TYPE) != 0;
	}

	public boolean IsHlsSupported() {
		return (ProtocolTypes & ProtocolType.HLS_TYPE) != 0;
	}

	public boolean IsWebmSupported() {
		return (ProtocolTypes & ProtocolType.WEBM_TYPE) != 0;
	}

	public boolean IsWmvSupported() {
		return (TranscoderTypes & TranscoderType.WMV_TYPE) != 0;
	}

	public boolean IsWmaSupported() {
		return (TranscoderTypes & TranscoderType.WMA_TYPE) != 0;
	}

	public boolean IsH264Supported() {
		return (TranscoderTypes & TranscoderType.H264_TYPE) != 0;
	}

	public boolean IsAacSupported() {
		return (TranscoderTypes & TranscoderType.AAC_TYPE) != 0;
	}

	public boolean IsRawSupported() {
		return (TranscoderTypes & TranscoderType.RAW_TYPE) != 0;
	}
}
