package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("set_resume_info")
public class SetResumeInfoRequest extends Serializer {
    @XStreamAlias("object_id")
    public String objectID;

    @XStreamAlias("pos")
    public int pos;
	
    public SetResumeInfoRequest(String objectID_, int pos_) {
        objectID = objectID_;
		pos = pos_;
    }
}
