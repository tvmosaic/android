package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by andreyp on 15.11.2016.
 */
@XStreamAlias("cancel_scan")
public class CancelScanRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    public CancelScanRequest(String id) {
        deviceID = id;
    }
}