package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * Created by andreyp on 14.11.2016.
 */
@XStreamAlias("get_networks")
public class NetworksRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    public NetworksRequest(String id) {
        deviceID = id;
    }
}
