package com.dvblogic.dvblink_common;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.ViewConfiguration;
//import android.text.format.DateFormat;

public class Utils
{
	public static final TimeZone	utcTZ	= TimeZone.getTimeZone("UTC");
    protected static final Date conversion_date_obj_ = new Date();

	public static long UtcToLocal(long utc_sec) {
		int gmtOffset = TimeZone.getDefault().getOffset(utc_sec*1000);
		return utc_sec + gmtOffset / 1000;
	}

	public static long CurrentUtc() {
        return System.currentTimeMillis() / 1000;
	}

    /*
	public static long CurrentLocal() {
        long cur_utc = System.currentTimeMillis() / 1000;
        return UtcToLocal(cur_utc);
	}
	*/

	public static String ToLongTimeString(Context context, Date dt) {
		// SimpleDateFormat sdf = new SimpleDateFormat("HH:mm",
		// Locale.getDefault());
		// String ret = sdf.format(dt);
		boolean twentyFourHourStyle = android.text.format.DateFormat.is24HourFormat(context);
		SimpleDateFormat sf = (SimpleDateFormat) SimpleDateFormat.getTimeInstance(SimpleDateFormat.MEDIUM,
				Locale.getDefault());
		String pattern = ((SimpleDateFormat) sf).toPattern();
		if (twentyFourHourStyle) {
			pattern = pattern.replace("a",  "");
			sf = new SimpleDateFormat(pattern.replace("h", "H"), Locale.getDefault());
		} else {
			//pattern.concat(" a");
			sf = new SimpleDateFormat(pattern.replace("H", "h"), Locale.getDefault());
		}

		return sf.format(dt);
	}

	public static String UtcToShortTimeString(Context context, long utc_sec) {

		boolean twentyFourHourStyle = android.text.format.DateFormat.is24HourFormat(context);
		SimpleDateFormat sf = (SimpleDateFormat) SimpleDateFormat.getTimeInstance(SimpleDateFormat.SHORT,
				Locale.getDefault());
		String pattern = ((SimpleDateFormat) sf).toPattern();
		if (twentyFourHourStyle) {
			pattern = pattern.replace("a",  "");
			sf = new SimpleDateFormat(pattern.replace("h", "H"), Locale.getDefault());
		} else {
			//pattern.concat(" a");
			sf = new SimpleDateFormat(pattern.replace("H", "h"), Locale.getDefault());
		}
        synchronized (conversion_date_obj_) {
            conversion_date_obj_.setTime(utc_sec * 1000);
            return sf.format(conversion_date_obj_);
        }
	}

	public static String UtcToMediumDateString(Context context, long utc_sec) {

        SimpleDateFormat sf = (SimpleDateFormat) SimpleDateFormat.getDateInstance(SimpleDateFormat.MEDIUM,
                Locale.getDefault());

        synchronized (conversion_date_obj_) {
            conversion_date_obj_.setTime(utc_sec * 1000);
            return sf.format(conversion_date_obj_);
        }

	}

	public static String UtcToShortDateString(Context context, long utc_sec) {

        synchronized (conversion_date_obj_) {
            conversion_date_obj_.setTime(utc_sec * 1000);
            java.text.DateFormat df = android.text.format.DateFormat.getDateFormat(context);
            if (df instanceof SimpleDateFormat) {
                String pattern = ((SimpleDateFormat) df).toPattern();
                SimpleDateFormat sf = new SimpleDateFormat(pattern, Locale.getDefault());
                return sf.format(conversion_date_obj_);
            } else {
                SimpleDateFormat sf = (SimpleDateFormat) SimpleDateFormat.getDateInstance(SimpleDateFormat.SHORT,
                        Locale.getDefault());
                sf.setNumberFormat(NumberFormat.getNumberInstance(Locale.getDefault()));
                return sf.format(conversion_date_obj_);
            }
        }
	}

	public static String UtcToShortestDateString(Context context, long utc_sec) {
		java.text.DateFormat df = android.text.format.DateFormat.getDateFormat(context);
		String pattern = ((SimpleDateFormat) df).toPattern();
		char[] array = pattern.toCharArray();
		char separator = '.';
        for (char anArray : array) {
            separator = anArray;
            if (!Character.isLetterOrDigit(separator)) {
                break;
            }
        }
		char[] order = android.text.format.DateFormat.getDateFormatOrder(context);
		if (order[0] == 'y') {
			pattern = String.format("%c%c%c%c%c", order[1], order[1], separator, order[2], order[2]);
		} else {
			pattern = String.format("%c%c%c%c%c", order[0], order[0], separator, order[1], order[1]);
		}
		SimpleDateFormat sf = new SimpleDateFormat(pattern, Locale.getDefault());

        synchronized (conversion_date_obj_) {
            conversion_date_obj_.setTime(utc_sec * 1000);
            return sf.format(conversion_date_obj_);
        }
	}

	public static void trace(String str) {
		//SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss:SSS", Locale.getDefault());
		//System.out.format("%s> %s\n", sdf.format(new Date()), str);
	}

    public static boolean isToday(long utc_sec) {
        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTimeInMillis(utc_sec * 1000);
        int day = calendar.get(Calendar.DAY_OF_YEAR);

        calendar.setTimeInMillis(Utils.CurrentUtc() * 1000);
        int day_now = calendar.get(Calendar.DAY_OF_YEAR);

        return (day == day_now);
    }

    private static String formatChannelNumber(int num, int subnum) {
        if (num < 1) {
            return "";
        }
        Locale locale = Locale.getDefault();
        return (
                subnum > 0 ?
                        String.format(locale, "%d.%d", num, subnum) :
                        String.format(locale, "%d", num)
        );
    }

	public static String formatChannelNumber(ScannedChannel channel) {
        return formatChannelNumber(channel.number, channel.subnumber);
	}

	public static String formatChannelNumber(Channel channel) {
        return formatChannelNumber(channel.Number, channel.Subnumber);
	}

	public static void sortChannels(ChannelsList channels, Settings.EChannelSortMode mode) {
		if (mode == Settings.EChannelSortMode.CH_SORT_MODE_NAME) {
			// sort channels alphabetically
			Collections.sort(channels.list(), new Comparator<Channel>() {
				public int compare(Channel ch1, Channel ch2) {
					return ch1.Name.compareToIgnoreCase(ch2.Name);
				}
			});
		} else {
			// sort channels numerically
			Collections.sort(channels.list(), new Comparator<Channel>() {
				public int compare(Channel ch1, Channel ch2) {
					if (ch1.Number <= 0 && ch2.Number <= 0) {
						return 0;
					}
					if (ch1.Number <= 0) {
						return 1;
					}
					if (ch2.Number <= 0) {
						return -1;
					}
					return ((ch1.Number * 100 + ch1.Subnumber) - (ch2.Number * 100 + ch2.Subnumber));
				}
			});
		}
	}

    /**
     * Determines if the device uses navigation controls as the primary navigation from a number of factors.
     * @param context Application Context
     * @return True if the device uses navigation controls, false otherwise.
     */
    public static boolean usesRemoteControl(Context context) {
        Configuration configuration = context.getResources().getConfiguration();
        if (configuration.navigation == Configuration.NAVIGATION_NONAV) {
            return false;
        } else if (configuration.touchscreen == Configuration.TOUCHSCREEN_FINGER) {
            return false;
        } else if (configuration.navigation == Configuration.NAVIGATION_DPAD) {
            return true;
        } else if (configuration.touchscreen == Configuration.TOUCHSCREEN_NOTOUCH) {
            return true;
        } else if (configuration.touchscreen == Configuration.TOUCHSCREEN_UNDEFINED) {
            return true;
        } else if (configuration.navigationHidden == Configuration.NAVIGATIONHIDDEN_YES) {
            return true;
        } else if (configuration.uiMode == Configuration.UI_MODE_TYPE_TELEVISION) {
            return true;
        }
        return false;
    }

public static boolean usesNavBar(Context context) {
    Resources resources = context.getResources();
    int id = resources.getIdentifier("config_showNavigationBar", "bool", "android");
    if (id > 0) {
        return resources.getBoolean(id);
    } else {    // Check for keys
        boolean hasMenuKey = ViewConfiguration.get(context).hasPermanentMenuKey();
        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        return !hasMenuKey && !hasBackKey;
    }
}
/*

boolean hasSoftKey = ViewConfiguration.get(context).hasPermanentMenuKey();

public boolean isNavigationBarAvailable(){

        boolean hasBackKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_BACK);
        boolean hasHomeKey = KeyCharacterMap.deviceHasKey(KeyEvent.KEYCODE_HOME);

        return (!(hasBackKey && hasHomeKey));
    }

  */
}
