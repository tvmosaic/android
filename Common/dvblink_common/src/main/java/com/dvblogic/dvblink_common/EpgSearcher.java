package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("epg_searcher")
public class EpgSearcher extends Serializer
{
	public final transient long	EPG_INVALID_TIME	= -1;
	public final transient int	COUNT_ALL_RESULTS	= -1;

	@XStreamAlias("channels_ids")
	public ChannelIDList		ChannelsIDs;

	@XStreamAlias("genre_mask")
	public long 				genreMask;
	
	@XStreamAlias("program_id")
	public String				ProgramID;

	@XStreamAlias("keywords")
	public String				Keyword;

	@XStreamAlias("start_time")
	public long					StartTime;

	@XStreamAlias("end_time")
	public long					EndTime;

	@XStreamAlias("epg_short")
	public boolean				IsEpgShort;

	@XStreamAlias("episode_num")
	public int					EpisodeNum;

	@XStreamAlias("season_num")
	public int					SeasonNum;

	@XStreamAlias("requested_count")
	public int					RequestedCount;
	

	public EpgSearcher() {
		ChannelsIDs = new ChannelIDList();
		IsEpgShort = false;
		StartTime = EPG_INVALID_TIME;
		EndTime = EPG_INVALID_TIME;
		ProgramID = "";
		Keyword = "";
		EpisodeNum = 0;
		SeasonNum = 0;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(boolean is_epg_short, long start_time, long end_time) {
		this();
		IsEpgShort = is_epg_short;
		StartTime = start_time;
		EndTime = end_time;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(String keywords) {
		this();
		Keyword = keywords;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(String keywords, boolean is_epg_short, long start_time, long end_time) {
		this();
		Keyword = keywords;
		IsEpgShort = is_epg_short;
		StartTime = start_time;
		EndTime = end_time;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(ChannelIDList channels_ids) {
		this();
		ChannelsIDs = channels_ids;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(ChannelIDList channels_ids, boolean is_epg_short, long start_time, long end_time) {
		this();
		ChannelsIDs = channels_ids;
		IsEpgShort = is_epg_short;
		StartTime = start_time;
		EndTime = end_time;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(ChannelIDList channels_ids, String keywords) {
		this();
		ChannelsIDs = channels_ids;
		Keyword = keywords;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(ChannelIDList channels_ids, String keywords, long genre_flags) {
		this();
		ChannelsIDs = channels_ids;
		Keyword = keywords;
		genreMask = genre_flags;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(ChannelIDList channels_ids, String keywords, boolean is_epg_short, long start_time, long end_time) {
		this();
		ChannelsIDs = channels_ids;
		Keyword = keywords;
		IsEpgShort = is_epg_short;
		StartTime = start_time;
		EndTime = end_time;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}

	public EpgSearcher(String channel_id, String program_id) {
		this();
		ChannelsIDs = new ChannelIDList(channel_id);
		ProgramID = program_id;
		genreMask = 0;
		RequestedCount = COUNT_ALL_RESULTS;
	}
}
