package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.util.ArrayList;
import java.util.HashMap;

@XStreamConverter(EPGMatchResultsConverter.class)
@XStreamAlias("match_info")
public class EPGMatchResults extends HashMap<String, EPGMatchResults.EPGChannelsMatch> {

    static public class EPGChannelsMatch {
        public EPGChannels exact;
        public EPGChannels partial;
        public EPGChannelsMatch() {
            exact = new EPGChannels();
            partial = new EPGChannels();
        }
    };

    static public class EPGChannels extends ArrayList<EPGChannel> {

    }

    static public class EPGChannel {
        public String epgSourceId;
        public String epgChannelId;
    }
}
