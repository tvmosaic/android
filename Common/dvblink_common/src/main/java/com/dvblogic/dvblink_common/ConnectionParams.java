package com.dvblogic.dvblink_common;

public class ConnectionParams
{
	public String	address;
	public int		port;
	public String	user;
	public String	password;
}
