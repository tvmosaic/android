package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("recorded_tv")
public class RecordedTV extends Item
{
	public RecordedTV() {
		State = RecordedTVState.RTVS_COMPLETED;
	}

	@XStreamAlias("channel_id")
	public String		ChannelId;

	@XStreamAlias("channel_name")
	public String		ChannelName;

	@XStreamAlias("channel_number")
	public int			ChannelNumber;

	@XStreamAlias("channel_subnumber")
	public int			ChannelSubnumber;

	@XStreamAlias("state")
	public int			State;

	@XStreamAlias("schedule_id")
	public String		ScheduleId;

	@XStreamAlias("schedule_name")
	public String		ScheduleName;

	@XStreamAlias("schedule_series")
	public boolean		ScheduleSeries;

	@XStreamAlias("video_info")
	public VideoInfo	VideoInfo;
}
