package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.Map;

public class ConciseParamMapConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(ConciseParamMap.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {

        ConciseParamMap param_map = (ConciseParamMap)value;

        for (Map.Entry<String, String> entry : param_map.parameters.entrySet()) {

            writer.startNode("param");
            writer.addAttribute("id", entry.getKey());
            writer.setValue(entry.getValue());
            writer.endNode();
        }

    }

    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        ConciseParamMap param_map = new ConciseParamMap();

        while (reader.hasMoreChildren()) {
            reader.moveDown();

            param_map.parameters.put(reader.getAttribute("id"), reader.getValue());

            reader.moveUp();
        }
        return param_map;
    }
}
