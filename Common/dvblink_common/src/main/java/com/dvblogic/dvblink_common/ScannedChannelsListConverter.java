package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Created by andreyp on 08.11.2016.
 */
public class ScannedChannelsListConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(ScannedChannelsList.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        ScannedChannelsList headendList = new ScannedChannelsList();
        while (reader.hasMoreChildren()) {
            reader.moveDown();

            Headend hd = new Headend();

            hd.id = reader.getAttribute("id");
            hd.name = reader.getAttribute("name");
            hd.desc = reader.getAttribute("desc");

            while (reader.hasMoreChildren()) {
                reader.moveDown();
                Transponder tr = new Transponder();

                tr.id = reader.getAttribute("id");
                tr.name = reader.getAttribute("name");
                if (tr.name == null)
                    tr.name = "";

                while (reader.hasMoreChildren()) {
                    reader.moveDown();

                    ScannedChannel ch = new ScannedChannel();

                    while (reader.hasMoreChildren()) {
                        reader.moveDown();

                        String nodeName = reader.getNodeName();

                        if (nodeName.equals("channel_id"))
                            ch.id = reader.getValue();
                        else if (nodeName.equals("channel_name"))
                            ch.name = reader.getValue();
                        else if (nodeName.equals("channel_origin"))
                            ch.origin = reader.getValue();
                        else if (nodeName.equals("channel_encrypted"))
                            ch.encrypted = reader.getValue().equalsIgnoreCase("true");
                        else if (nodeName.equals("channel_number")) {
                            try {
                                ch.number = Integer.parseInt(reader.getValue());
                            } catch (NumberFormatException e) {
                                ch.number = -1;
                            }
                        }
                        else if (nodeName.equals("channel_subnumber")) {
                            try {
                                ch.subnumber = Integer.parseInt(reader.getValue());
                            } catch (NumberFormatException e) {
                                ch.subnumber = -1;
                            }
                        }
                        else if (nodeName.equals("channel_type")) {
                            try {
                                ch.type = Integer.parseInt(reader.getValue());
                            } catch (NumberFormatException e) {
                                ch.type = ChannelType.RD_CHANNEL_TV;
                            }
                        }

                        reader.moveUp();
                    }

                    tr.channels.add(ch);

                    reader.moveUp();
                }

                hd.transponders.add(tr);

                reader.moveUp();
            }

            headendList.add(hd);

            reader.moveUp();
        }
        return headendList;
    }
}
