package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("container")
public class Container extends Serializer
{
	public static final int	OBJECT_COUNT_UNKNOWN	= -1;

	@XStreamAlias("object_id")
	public String			ObjectID;

	@XStreamAlias("parent_id")
	public String			ParentID;

	@XStreamAlias("source_id")
	public String			SourceID;

	@XStreamAlias("name")
	public String			Name;

	@XStreamAlias("description")
	public String			Description;

	@XStreamAlias("logo")
	public String			Logotype;

	// ContainerType
	@XStreamAlias("container_type")
	public int				Type;

	// ItemType
	@XStreamAlias("content_type")
	public int				ContentType;

	@XStreamAlias("total_count")
	public int				TotalCount;

	public Container() {
		ObjectID = "";
		ParentID = "";
		SourceID = "";
		Name = "";
		Description = "";
		Logotype = "";
		Type = ContainerType.CONTAINER_UNKNOWN;
		ContentType = ItemType.ITEM_UNKNOWN;
		TotalCount = 0;
	}
}
