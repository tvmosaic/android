package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("stream")
public class ResponseStream extends Serializer
{
	@XStreamAlias("channel_handle")
	public long		ChannelHandle;

	@XStreamAlias("url")
	public String	Url;
}
