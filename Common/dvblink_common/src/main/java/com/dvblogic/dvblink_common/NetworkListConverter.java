package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Created by andreyp on 14.11.2016.
 */
public class NetworkListConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(NetworkList.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {

        NetworkList networkList =  new NetworkList();
        reader.moveDown();
        String nodeName = reader.getNodeName();
        if (!nodeName.equalsIgnoreCase("settings")) {
            return null;
        }
        reader.moveDown();
        nodeName = reader.getNodeName();
        if (!nodeName.equalsIgnoreCase("setting")) {
            return null;
        }
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            nodeName = reader.getNodeName();
            if (!nodeName.equalsIgnoreCase("value")) {
                return null;
            }
            networkList.add(new NetworkList.Setting(
                    reader.getAttribute("id"), reader.getAttribute("name")
            ));
            reader.moveUp();
        }

        return networkList;
    }
}
