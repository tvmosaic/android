package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("activate_product_trial")
public class ActivateProductTrialRequest extends Serializer {
    @XStreamAlias("id")
    public String id;

    public ActivateProductTrialRequest(String id) {
        this.id = id;
    }
}
