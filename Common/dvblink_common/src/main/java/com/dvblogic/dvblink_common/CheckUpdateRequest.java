package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("updater_check_update")
public class CheckUpdateRequest extends Serializer {}
