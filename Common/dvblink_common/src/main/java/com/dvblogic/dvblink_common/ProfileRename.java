package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("change_profile_name")
public class ProfileRename extends Serializer
{
	@XStreamAlias("profile_id")
	public String ProfileId;

	@XStreamAlias("profile_name")
	public String ProfileName;

}
