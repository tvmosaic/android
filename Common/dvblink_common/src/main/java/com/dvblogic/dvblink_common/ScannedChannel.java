package com.dvblogic.dvblink_common;

public class ScannedChannel {
	public String id;
	public String name;
	public String origin;
	public boolean encrypted = false;
    public int number = -1;
    public int subnumber = -1;
    public int type = ChannelType.RD_CHANNEL_TV;
}
