package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("epg_sources")
public class EPGSourceList extends Serializer
{
	@XStreamImplicit(itemFieldName = "source")
	private ArrayList<EPGSource>	_list;

	public EPGSourceList() {
		_list = new ArrayList<>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<>();
		}
		return this;
	}

	public ArrayList<EPGSource> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(EPGSource ch) {
		if (_list == null)
			_list = new ArrayList<>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public EPGSource get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
