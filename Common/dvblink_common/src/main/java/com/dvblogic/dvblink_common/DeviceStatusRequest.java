package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("get_device_status")
public class DeviceStatusRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    public DeviceStatusRequest(String deviceID_) {
        deviceID = deviceID_;
    }
}
