package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("response")
public class Response extends Serializer
{
	@XStreamAlias("status_code")
	public int		Status;

	@XStreamAlias("xml_result")
	public String	Result;
}
