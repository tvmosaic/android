package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("channel")
public class StreamInfo extends Serializer
{
	@XStreamAlias("channel_dvblink_id")
	public String		DVBLinkChannelId;

	@XStreamAlias("url")
	public String	Url;
}
