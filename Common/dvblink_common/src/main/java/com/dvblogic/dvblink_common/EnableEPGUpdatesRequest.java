package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("enable_epg_updates")
public class EnableEPGUpdatesRequest extends Serializer
{
    public EnableEPGUpdatesRequest(Boolean enabled) {
        this.enabled = enabled;
    }

    @XStreamAlias("enabled")
    public Boolean			enabled;
}
