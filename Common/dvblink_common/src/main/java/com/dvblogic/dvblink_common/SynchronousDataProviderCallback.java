package com.dvblogic.dvblink_common;

public class SynchronousDataProviderCallback implements IDataProvider {

    public Object result_ = null;
    public StatusCode result_code_ = StatusCode.STATUS_OK;

    public void DataProcessing(String command, Object result_data) {
        result_ = result_data;
        result_code_ = StatusCode.STATUS_OK;
    }

    public void ErrorProcessing(String command, StatusCode error) {
        result_ = null;
        result_code_ = error;
    }
}
