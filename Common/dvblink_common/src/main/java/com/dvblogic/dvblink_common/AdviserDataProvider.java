package com.dvblogic.dvblink_common;

import java.util.Locale;


@SuppressWarnings("WeakerAccess")
public class AdviserDataProvider extends DataProvider
{
	public static final String  CREATE_PROFILE_CMD	= "create_profile";	
	public static final String	GET_PROFILES_CMD	    = "get_profiles";	
	public static final String	REMOVE_PROFILE_CMD    = "delete_profile";
	public static final String	RENAME_PROFILE_CMD	    = "rename_profile";	
	public static final String	GET_PROVIDERS_CMD	    = "get_providers";	
	public static final String	GET_PROVIDER_CHANNELS_CMD    = "get_provider_channels";
	public static final String	GET_MAPPED_CHANNELS_CMD	    = "get_mapped_channels";	
	public static final String	SET_MAPPED_CHANNELS_CMD	    = "set_mapped_channels";	
	public static final String	GET_EPG_CMD    = "get_epg";
	public static final String	RATE_PROGRAM_CMD	    = "rate_program";	
	public static final String	RECOMMEND_BY_PROFILES_CMD    = "recommend_by_profiles";
	public static final String	RECOMMEND_LIKE_THIS_CMD    = "recommend_like_this";
    public static final String	EPG_SOURCE_REFRESH_CMD    = "epg_source_refresh";
    public static final String	EPG_SOURCE_GET_SETTINGS_CMD    = "epg_source_get_settings";
    public static final String	EPG_SOURCE_SET_SETTINGS_CMD    = "epg_source_set_settings";
	public static final String	BACKUP_INSTALLATION_SETTINGS_CMD    = "do_installation_backup";
	public static final String	RESTORE_INSTALLATION_SETTINGS_CMD    = "do_installation_restore";

	public static final String ADVISER_ADDRESSEE = "284cb518-d074-456d-949c-1423f9023e0d";
	public static final String SERVER_ADDRESSEE = "10befee1-10dd-404c-8873-8f78336161e3";

	public AdviserDataProvider(ServerSettings ss, IDataProvider data_provider) throws Exception {
		super(ss, data_provider);
	}

	public void CreateProfile(ProfileCreator request) {
		adviserStartGetData(CREATE_PROFILE_CMD, request);
	}

	public void GetProfiles(ProfilesRequest request) {
		adviserStartGetData(GET_PROFILES_CMD, request);
	}

	public void RemoveProfile(ProfileDelete request) {
		adviserStartGetData(REMOVE_PROFILE_CMD, request);
	}

	public void RenameProfile(ProfileRename request) {
		adviserStartGetData(RENAME_PROFILE_CMD, request);
	}

	public void GetProviders(ProvidersRequest request) {
		adviserStartGetData(GET_PROVIDERS_CMD, request);
	}

	public void GetProviderChannels(ProviderChannelsRequest request) {
		adviserStartGetData(GET_PROVIDER_CHANNELS_CMD, request);
	}

	public void GetMappedChannels(MappedChannelsRequest request) {
		adviserStartGetData(GET_MAPPED_CHANNELS_CMD, request);
	}

	public void SetMappedChannels(SetMappedChannels request) {
		adviserStartGetData(SET_MAPPED_CHANNELS_CMD, request);
	}

	public void GetEpg(EpgRequest request) {
		adviserStartGetData(GET_EPG_CMD, request);
	}

	public void RateProgram(RateProgram request) {
		adviserStartGetData(RATE_PROGRAM_CMD, request);
	}

	public void Recommend(RecommendByProfiles request) {
		adviserStartGetData(RECOMMEND_BY_PROFILES_CMD, request);
	}

	public void Recommend(RecommendLikeThis request) {
		adviserStartGetData(RECOMMEND_LIKE_THIS_CMD, request);
	}

    public void RefreshEpgSource(String epg_source_id) {
        lastXmlCommand = EPG_SOURCE_REFRESH_CMD;
        XmlCommand xmlcmd = new XmlCommand(epg_source_id, lastXmlCommand, "");

        super.StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
    }

    public void GetEPGSourceSettings(String epg_source_id) {
        lastXmlCommand = EPG_SOURCE_GET_SETTINGS_CMD;
        XmlCommand xmlcmd = new XmlCommand(epg_source_id, lastXmlCommand, "");

        super.StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
    }

    public void SetEPGSourceSettings(String epg_source_id, ConciseParamMap parameters) {
        lastXmlCommand = EPG_SOURCE_SET_SETTINGS_CMD;
        String xml_string = GetRequestString(lastXmlCommand, new SetEPGSourceSettingsRequest(parameters));
        XmlCommand xmlcmd = new XmlCommand(epg_source_id, lastXmlCommand, xml_string);

        super.StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
    }

	public void BackupInstallationSettings() {
		lastXmlCommand = BACKUP_INSTALLATION_SETTINGS_CMD;
		XmlCommand xmlcmd = new XmlCommand(SERVER_ADDRESSEE, lastXmlCommand, "");

		super.StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
	}

    public void RestoreInstallationSettings(InstallationBackup ib) {
        lastXmlCommand = RESTORE_INSTALLATION_SETTINGS_CMD;
        String xml_string = GetRequestString(lastXmlCommand, ib);
        XmlCommand xmlcmd = new XmlCommand(SERVER_ADDRESSEE, lastXmlCommand, xml_string);

        super.StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
    }


    protected void GetResponseObject(String command, Response response) {
		
		response_status = StatusCode.STATUS_OK;
		response_object = null;

		if (response.Result != null) {
			int pos = response.Result.indexOf("<?xml");
			if (pos > 0)
				response.Result = response.Result.substring(pos);
		}

		XmlCommandResponse xcr;
		if (command.equals(EXECUTE_COMMAND_CMD)) {
			xcr = (XmlCommandResponse) Deserialize(XmlCommandResponse.class, response.Result);
			if (xcr == null) {
				response_object = "";
				response_status = StatusCode.STATUS_INVALID_DATA;
				return;
			}
		}
		else
		{
			super.GetResponseObject(command, response);
			return;
		}
		
		try {
			String cmd = lastXmlCommand.toLowerCase(Locale.ENGLISH);
			if (cmd.equals(GET_PROVIDERS_CMD) && StatusCode.equals(StatusCode.STATUS_ADRESSEE_NOT_FOUND, response.Status)) {
				response_object = new ProvidersList();
				response_status = StatusCode.STATUS_ADRESSEE_NOT_FOUND;
				return;
			}
			if (xcr.Result.equals("1") || xcr.Result.equals("success")) {
				//System.out.format("<----- %s:%n%s%n=====%n", command, response.Result);

				if (cmd.equals(CREATE_PROFILE_CMD)) {
					response_object = Deserialize(ProfileId.class, xcr.Params);
				} else if (cmd.equals(GET_PROFILES_CMD)) {
					response_object = Deserialize(ProfilesResponse.class, xcr.Params);
				} else if (cmd.equals(GET_PROVIDERS_CMD)) {
					response_object = Deserialize(ProvidersList.class, xcr.Params);
				} else if (cmd.equals(GET_PROVIDER_CHANNELS_CMD)) {
					response_object = Deserialize(ProviderChannelsList.class, xcr.Params);
				} else if (cmd.equals(GET_MAPPED_CHANNELS_CMD)) {
					response_object = Deserialize(MappedChannelsResponse.class, xcr.Params);
				} else if (cmd.equals(GET_EPG_CMD)) {
					response_object = Deserialize(EpgResponse.class, xcr.Params);
                } else if (cmd.equals(EPG_SOURCE_GET_SETTINGS_CMD)) {
                    response_object = Deserialize(EPGSourceSettings.class, xcr.Params);
                } else if (cmd.equals(BACKUP_INSTALLATION_SETTINGS_CMD)) {
                    response_object = Deserialize(InstallationBackup.class, xcr.Params);
				} else if (cmd.equals(RECOMMEND_BY_PROFILES_CMD) || cmd.equals(RECOMMEND_LIKE_THIS_CMD)) {
					response_object = Deserialize(RecommendationsList.class, xcr.Params);
				} else if (cmd.equals(REMOVE_PROFILE_CMD) || cmd.equals(RENAME_PROFILE_CMD)
						|| cmd.equals(SET_MAPPED_CHANNELS_CMD) || cmd.equals(RATE_PROGRAM_CMD )
                        || cmd.equals(EPG_SOURCE_REFRESH_CMD) || cmd.equals(EPG_SOURCE_SET_SETTINGS_CMD)
                        || cmd.equals(RESTORE_INSTALLATION_SETTINGS_CMD)) {
					response_object = "";
				} else {
					super.GetResponseObject(command, response);
				}
			} else {
				response_status = StatusCode.fromInt(1000);
				//System.out.format("<----- %s:%nstatus code %d%n=====%n", command, response.Status);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			response_status = StatusCode.STATUS_INVALID_DATA;
		}
	}

	protected void adviserStartGetData(String command, Object param) {

		lastXmlCommand = command;
		String xml_string = GetRequestString(lastXmlCommand, param);
		XmlCommand xmlcmd = new XmlCommand(ADVISER_ADDRESSEE, lastXmlCommand, xml_string);

		super.StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
	}

	@Override
	public void DataProcessing(String command, Object result_data) {
		// System.out.format("DataProvider::DataProcessing(%s)%n", command);
		if (data_provider_ == null)
			return;
		try {
			//System.out.println("<----- DataProvider.DataProcessing() before GetResponseObject");
			GetResponseObject(command, (Response) result_data);
			//System.out.println("===== DataProvider.DataProcessing() after GetResponseObject");

			if (response_status == StatusCode.STATUS_OK || response_status == StatusCode.STATUS_ADRESSEE_NOT_FOUND) {
				//System.out.println("<----- DataProvider.DataProcessing()");
				if (command.equals(EXECUTE_COMMAND_CMD))
					command = lastXmlCommand;
				data_provider_.DataProcessing(command, response_object);
				//System.out.println("===== DataProvider.DataProcessing()");
			} else {
				if (command.equals(EXECUTE_COMMAND_CMD))
					command = lastXmlCommand;
				ErrorProcessing(command, response_status);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
 	}

}
