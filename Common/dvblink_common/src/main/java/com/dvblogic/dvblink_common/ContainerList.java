package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("containers")
public class ContainerList extends Serializer
{
	@XStreamImplicit(itemFieldName = "container")
	private ArrayList<Container>	_list;

	public ContainerList() {
		_list = new ArrayList<Container>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Container>();
		}
		return this;
	}

	public ArrayList<Container> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Container cont) {
		if (_list == null) {
			_list = new ArrayList<Container>();
		}
		_list.add(cont);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Container get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
