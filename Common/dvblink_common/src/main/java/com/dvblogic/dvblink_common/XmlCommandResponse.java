package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("xml_response")
public class XmlCommandResponse extends Serializer
{
	public static transient final String	XML_CMD_RESULT_SUCCESS	= "success";
	public static transient final String	XML_CMD_RESULT_FAIL		= "fail";

	@XStreamAlias("result")
	public String							Result;

	@XStreamAlias("param")
	public String							Params;

	public boolean IsSuccess() {
		return Result.equalsIgnoreCase("success");
	}

	public XmlCommandResponse() {	
		Result = "";
		Params = "";
	}
	
	public XmlCommandResponse(String result, String parameters) {
		Result = result;
		Params = parameters;
	}
}
