package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Created by andreyp on 08.11.2016.
 */
public class FavoritesListConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(FavoritesList.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        FavoritesList favList = new FavoritesList();

        while (reader.hasMoreChildren()) {
            reader.moveDown();

            Favorite fav = new Favorite();

            while (reader.hasMoreChildren()) {
                reader.moveDown();

                String nodeName = reader.getNodeName();

                if (nodeName.equals("id"))
                    fav.ID = reader.getValue();
                else if (nodeName.equals("name"))
                    fav.Name = reader.getValue();
                else if (nodeName.equals("channels")) {
                    while (reader.hasMoreChildren()) {
                        reader.moveDown();

                        nodeName = reader.getNodeName();

                        if (nodeName.equals("channel")) {
                            String id = reader.getValue();
                            fav.channels.add(id);
                        }

                        reader.moveUp();
                    }
                }

                reader.moveUp();
            }

            favList.add(fav);

            reader.moveUp();
        }
        return favList;
    }
}
