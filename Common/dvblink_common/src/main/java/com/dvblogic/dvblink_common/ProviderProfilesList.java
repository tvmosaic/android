package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("provider")
public class ProviderProfilesList extends Serializer
{
	@XStreamAlias("provider_id")
	@XStreamAsAttribute
	public String ProviderId;
	
	@XStreamImplicit(itemFieldName = "profile")
	private ArrayList<Profile>	_list;

	public ProviderProfilesList() {
		_list = new ArrayList<Profile>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Profile>();
		}
		return this;
	}

	public ArrayList<Profile> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Profile ch) {
		if (_list == null) {
			_list = new ArrayList<Profile>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Profile get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
