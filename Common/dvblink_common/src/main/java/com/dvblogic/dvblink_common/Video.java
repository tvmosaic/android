package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("video")
public class Video extends Item
{
	@XStreamAlias("video_info")
	public VideoInfo	VideoInfo;
}
