package com.dvblogic.dvblink_common;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class DataProvider implements IDataProvider
{
	public static final String	GET_CHANNELS_CMD		= "get_channels";
	public static final String	PLAY_CHANNEL_CMD		= "play_channel";
	public static final String	STOP_CHANNEL_CMD		= "stop_channel";
	public static final String	SEARCH_EPG_CMD			= "search_epg";
	public static final String	GET_SCHEDULES_CMD       = "get_schedules";
	public static final String	ADD_SCHEDULE_CMD		= "add_schedule";
	public static final String	REMOVE_SCHEDULE_CMD		= "remove_schedule";
	public static final String	UPDATE_SCHEDULE_CMD		= "update_schedule";
	public static final String	GET_RECORDINGS_CMD		= "get_recordings";
	public static final String	REMOVE_RECORDING_CMD	= "remove_recording";
	public static final String	SET_PARENTAL_LOCK_CMD	= "set_parental_lock";
	public static final String	GET_PARENTAL_STATUS_CMD	= "get_parental_status";
	public static final String	GET_OBJECT_CMD			= "get_object";
    public static final String	SEARCH_OBJECTS_CMD	    = "search_objects";
	public static final String	REMOVE_OBJECT_CMD		= "remove_object";
	public static final String	STOP_RECORDING_CMD		= "stop_recording";
	public static final String	GET_STREAM_INFO_CMD		= "get_stream_info";
	public static final String	GET_STREAMING_CAPS_CMD	= "get_streaming_capabilities";
	public static final String	EXECUTE_COMMAND_CMD		= "execute_command";
	public static final String	GET_RECORDING_SETTINGS	= "get_recording_settings";
	public static final String	SET_RECORDING_SETTINGS	= "set_recording_settings";
	public static final String	GET_FAVORITES_CMD   	= "get_favorites";
    public static final String	FORCE_EPG_UPDATE_CMD   	= "force_epg_update";
    public static final String	ENABLE_EPG_UPDATES_CMD  = "enable_epg_updates";
	public static final String	SET_RESUME_INFO_CMD  	= "set_object_resume_info";
	public static final String	GET_RESUME_INFO_CMD  	= "get_object_resume_info";
	public static final String	GET_OOB_CHANNEL_URL_CMD = "get_oob_channel_url";

	public static final String	SOCIAL_MODULE_ADDRESSEE	= "a8b42f8e-6a08-418a-8917-e767f766d576";

	public static final String	GET_SHARE_TEMPLATES_CMD	= "get_templates";
    public static final String	SEND_TO_GET_TARGETS_CMD    = "get_targets";
    public static final String	SEND_TO_GET_ITEMS_CMD    = "send_to_get_items";
    public static final String	SEND_TO_ADD_ITEM_CMD    = "send_to_add_item";
    public static final String	SEND_TO_CANCEL_ITEM_CMD    = "send_to_cancel_item";

    public static final String	GET_SERVER_INFO_CMD    = "get_server_info";
    public static final String	TIMESHIFT_GET_STATS    = "timeshift_get_stats";
    public static final String	TIMESHIFT_SEEK    = "timeshift_seek";

	// protected ExecutorService executor = Executors.newCachedThreadPool();
	protected ExecutorService	executor				= null;									// Executors.newSingleThreadExecutor();
	protected HttpDataProvider	http_data_provider_		= null;
	protected IDataProvider		data_provider_;
	protected Object			response_object			= null;
	protected StatusCode		response_status;
	protected URI				serverURI				= null;
	protected String			lastXmlCommand			= "";
	protected String			language = "*";
    protected Boolean           synchronous_mode_       = false;

	protected DataProvider(String server, int port, boolean use_https, String user_name, String password, IDataProvider data_provider)
			throws Exception {
		data_provider_ = data_provider;
		try {
			// serverURI = new URI("http", String.format("%s:%s", user_name,
			// password), server, port, "/mobile/", null,
			// null);
			serverURI = new URI(use_https ? "https" : "http", "", server, port, "/mobile/", null, null);
			http_data_provider_ = new HttpDataProvider(serverURI, user_name, password, this);
		} catch (URISyntaxException e) {
			e.printStackTrace();
			throw new Exception();
		}
	}

    public DataProvider(ServerSettings ss, IDataProvider data_provider) throws Exception {
        this(ss.serverAddress, ss.serverPort, ss.useHttps, ss.userName, ss.password, data_provider);
    }

    public DataProvider(URI serverUri, String user_name, String password, IDataProvider data_provider) throws Exception {
        data_provider_ = data_provider;
        try {
            serverURI = serverUri;
            http_data_provider_ = new HttpDataProvider(serverURI, user_name, password, this);
        } catch (URISyntaxException e) {
            e.printStackTrace();
            throw new Exception();
        }
    }

    public void setSynchronousMode(Boolean enable) {
        synchronous_mode_ = enable;
    }

    public void waitHttp() {
		try {
			executor.awaitTermination(60, TimeUnit.SECONDS);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void setLanguage(String lang) {
		language = lang.toLowerCase();
		if (http_data_provider_ != null)
			http_data_provider_.setLanguage(language);
	}

	public Object responseObject() {
		return response_object;
	}

	public StatusCode lastError() {
		return response_status;
	}

	public void GetChannels(ChannelsRequest request) {
		StartGetData(GET_CHANNELS_CMD, request);
	}

    public void ForceEPGUpdate(ForceEPGUpdateRequest request) {
        StartGetData(FORCE_EPG_UPDATE_CMD, request);
    }

    public void EnableEPGUpdates(EnableEPGUpdatesRequest request) {
        StartGetData(ENABLE_EPG_UPDATES_CMD, request);
    }

	public void SetResumeInfo(SetResumeInfoRequest request) {
		StartGetData(SET_RESUME_INFO_CMD, request);
	}

    public void GetResumeInfo(GetResumeInfoRequest request) {
        StartGetData(GET_RESUME_INFO_CMD, request);
    }

	public void GetOobChannelUrl(OobChannelUrlRequest request) {
		StartGetData(GET_OOB_CHANNEL_URL_CMD, request);
	}

	public void SearchEpg(EpgSearcher request) {
		StartGetData(SEARCH_EPG_CMD, request);
	}

	public void GetRecordings(RecordingsRequest request) {
		StartGetData(GET_RECORDINGS_CMD, request);
	}

	public void GetRecordingSettings(RecordingSettingsRequest request) {
		StartGetData(GET_RECORDING_SETTINGS, request);
	}

	public void SetRecordingSettings(RecordingSettingsSetter request) {
		StartGetData(SET_RECORDING_SETTINGS, request);
	}

	public void PlayChannel(RequestStream request) {
		StartGetData(PLAY_CHANNEL_CMD, request);
	}

	public void StopChannel(StopStream request) {
		StartGetData(STOP_CHANNEL_CMD, request);
	}

	public void GetSchedules(SchedulesRequest request) {
		StartGetData(GET_SCHEDULES_CMD, request);
	}

	public void AddSchedule(Schedule request) {
		StartGetData(ADD_SCHEDULE_CMD, request);
	}

	public void RemoveSchedule(ScheduleRemover request) {
		StartGetData(REMOVE_SCHEDULE_CMD, request);
	}

	public void UpdateSchedule(ScheduleUpdater request) {
		StartGetData(UPDATE_SCHEDULE_CMD, request);
	}

	public void RemoveRecording(RecordingRemover request) {
		StartGetData(REMOVE_RECORDING_CMD, request);
	}

	public void GetObjectAsXml(ObjectRequester request) {
		StartGetData(GET_OBJECT_CMD, request);
	}

	public void SearchObjects(ObjectSearcher request) {
		StartGetData(SEARCH_OBJECTS_CMD, request);
	}

	public Object GetObject(Class<?> cls, String xml_string) {
		Object response_object = null;

		try {
			response_object = Deserialize(cls, xml_string);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

		return response_object;
	}

	public void RemoveObject(ObjectRemover request) {
		StartGetData(REMOVE_OBJECT_CMD, request);
	}

	public void StopRecording(StopRecording request) {
		StartGetData(STOP_RECORDING_CMD, request);
	}

	public void GetStreamInfo(StreamInfoRequest request) {
		StartGetData(GET_STREAM_INFO_CMD, request);
	}

	public void GetStreamingCapabilities() {
		ServerCapsRequest request = new ServerCapsRequest();
		StartGetData(GET_STREAMING_CAPS_CMD, request);
	}

	public void ExecuteXmlCommand(XmlCommand xml_command) {
		lastXmlCommand = xml_command.Command;
		StartGetData(EXECUTE_COMMAND_CMD, xml_command);
	}

	public void GetShareTemplates() {
		GetShareTemplatesRequest req = new GetShareTemplatesRequest();
		String params_string = Serialize(req);

		XmlCommand xml_command = new XmlCommand(SOCIAL_MODULE_ADDRESSEE, GET_SHARE_TEMPLATES_CMD, params_string);
		ExecuteXmlCommand(xml_command);
	}

    public void SendToGetTargets(SendToTargetsRequest request) {
        socialStartGetData(SEND_TO_GET_TARGETS_CMD, request);
    }

    public void SendToGetItems(SendToItemsRequest request) {
        socialStartGetData(SEND_TO_GET_ITEMS_CMD, request);
    }

    public void SendToAddItem(SendToAddRequest request) {
        socialStartGetData(SEND_TO_ADD_ITEM_CMD, request);
    }

    public void SendToCancelItem(SendToCancelRequest request) {
        socialStartGetData(SEND_TO_CANCEL_ITEM_CMD, request);
    }

    public void GetVersion(ServerVersionRequest request) {
        StartGetData(GET_SERVER_INFO_CMD, request);
    }

    public void GetFavorites(FavoritesRequest request) {
		StartGetData(GET_FAVORITES_CMD, request);
	}

	public void Cancel() {
		CancelGetData();
	}

	public void Request(String command, Object param) {
		StartGetData(command, param);
	}

    protected void StartGetData(String command, Object param) {
        String xml_string = GetRequestString(command, param);
        if (xml_string != null) {

            // System.out.format("-----> %s:%n%s%n=====%n", command,
            // xml_string);

            if (!synchronous_mode_) {
                try {
                    Utils.trace("StartGetData start");
                    http_data_provider_.setData(command, xml_string);
                    executor = Executors.newSingleThreadExecutor();
                    executor.submit(http_data_provider_);
                    executor.shutdown();
                    Utils.trace("StartGetData end");
                } catch (Exception e) {
                    ProtectedErrorProcessing(command, StatusCode.STATUS_CONNECTION_ERROR);
                    e.printStackTrace();
                }
            } else {
                http_data_provider_.setData(command, xml_string);
                http_data_provider_.run();
            }
        } else {
            ProtectedErrorProcessing(command, StatusCode.STATUS_INVALID_DATA);
            System.out.format("-----> %s:%nSTATUS_INVALID_DATA%n=====%n", command);
        }
    }

    protected void socialStartGetData(String command, Object param) {

        lastXmlCommand = command;
        String xml_string = GetRequestString(lastXmlCommand, param);
        XmlCommand xmlcmd = new XmlCommand(SOCIAL_MODULE_ADDRESSEE, lastXmlCommand, xml_string);

        StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
    }

	protected void CancelGetData() {
	}

	protected String GetRequestString(String command, Object request_object) {
		try {
			return Serializer.serialize(request_object);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return null;
	}

	protected void GetResponseObject(String command, Response response) {
		response_status = StatusCode.STATUS_OK;
		response_object = null;

		try {
			response_object = response.Result;

			String cmd = command.toLowerCase(Locale.ENGLISH);
			if (StatusCode.equals(StatusCode.STATUS_OK, response.Status)) {
				// System.out.format("<----- %s:%n%s%n=====%n", command,
				// response.Result);

				if (cmd.equals(GET_CHANNELS_CMD )) {
					response_object = Deserialize(ChannelsList.class, response.Result);
				} else if (cmd.equals(PLAY_CHANNEL_CMD )) {
					response_object = Deserialize(ResponseStream.class, response.Result);
				} else if (cmd.equals(SEARCH_EPG_CMD )) {
					response_object = Deserialize(ChannelIdWithProgramsList.class, response.Result);
				} else if (cmd.equals(GET_SCHEDULES_CMD )) {
					response_object = Deserialize(SchedulesList.class, response.Result);
				} else if (cmd.equals(GET_RECORDINGS_CMD )) {
					response_object = Deserialize(RecordingsList.class, response.Result);
				} else if (cmd.equals(GET_OBJECT_CMD )) {
					response_object = response.Result;
                } else if (cmd.equals(SEARCH_OBJECTS_CMD)) {
                    response_object = response.Result;
				} else if (cmd.equals(GET_STREAM_INFO_CMD )) {
					response_object = Deserialize(StreamInfoList.class, response.Result);
				} else if (cmd.equals(GET_STREAMING_CAPS_CMD )) {
					response_object = Deserialize(ServerCapabilities.class, response.Result);
                } else if (cmd.equals(GET_RESUME_INFO_CMD )) {
                    response_object = Deserialize(ResumeInfo.class, response.Result);
				} else if (cmd.equals(GET_OOB_CHANNEL_URL_CMD)) {
					response_object = Deserialize(OobChannelUrl.class, response.Result);
				} else if (cmd.equals(GET_RECORDING_SETTINGS )) {
					response_object = Deserialize(RecordingSettingsResponse.class, response.Result);
					// System.out.println(response.Result);
				} else if (cmd.equals(EXECUTE_COMMAND_CMD )) {
					response_object = Deserialize(XmlCommandResponse.class, response.Result);
				} else if (cmd.equals(ADD_SCHEDULE_CMD ) || cmd.equals(REMOVE_SCHEDULE_CMD )
                        || cmd.equals(REMOVE_RECORDING_CMD )
						|| cmd.equals(STOP_CHANNEL_CMD ) || cmd.equals(REMOVE_OBJECT_CMD )
                        || cmd.equals(STOP_RECORDING_CMD ) || cmd.equals(TIMESHIFT_SEEK)
						|| cmd.equals(SET_RECORDING_SETTINGS ) || cmd.equals(UPDATE_SCHEDULE_CMD )
                        || cmd.equals(SET_RESUME_INFO_CMD) ) {
					response_object = "";
				} else if (cmd.equals(GET_FAVORITES_CMD )) {
					response_object = Deserialize(FavoritesList.class, response.Result);
                } else if (cmd.equals(GET_SERVER_INFO_CMD )) {
                    response_object = Deserialize(ServerVersion.class, response.Result);
                } else if (cmd.equals(TIMESHIFT_GET_STATS )) {
                    response_object = Deserialize(TimeShiftStatusResponse.class, response.Result);
                } else if (cmd.equals(FORCE_EPG_UPDATE_CMD)) {
                    response_object = true;
                } else if (cmd.equals(ENABLE_EPG_UPDATES_CMD)) {
                    response_object = true;
				} else {
					response_status = StatusCode.STATUS_NOT_IMPLEMENTED;
				}
			} else {
				response_status = StatusCode.fromInt(response.Status);
				System.out.format("<----- %s:%nstatus code %d%n=====%n", command, response.Status);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			response_status = StatusCode.STATUS_INVALID_DATA;
		}
	}

	protected String Serialize(Object data) {
		return Serializer.serialize(data);
	}

	public Object Deserialize(Class<?> cls, String xml_string) {
		return Serializer.deserialize(xml_string, cls);
	}

	// {
	/*
	 * protected void OnCommandProcessed(Object sender,
	 * HttpDataProviderEventArgs args) { if (null != data_provider_) {
	 * StatusCode status = args.Status; Object result_object;
	 * 
	 * if (StatusCode.STATUS_OK == status && (StatusCode.STATUS_OK == (status =
	 * GetResponseObject(args.Command, args.Result, out result_object)))) {
	 * ProtectedDataProcessing(args.Command, result_object); } else {
	 * ProtectedErrorProcessing(args.Command, status); } } }
	 * 
	 * 
	 * protected void ProtectedDataProcessing(String command, Object
	 * result_data) { if (data_provider_ == null) return;
	 * 
	 * try { data_provider_.DataProcessing(command, result_data); } catch
	 * (Exception e ) { } }
	 */
	protected void ProtectedErrorProcessing(String command, StatusCode error) {
		response_status = error;

		if (data_provider_ == null)
			return;

		try {
			data_provider_.ErrorProcessing(command, error);
		} catch (Exception e) {
		}
	}

	@Override
	public void DataProcessing(String command, Object result_data) {
		// System.out.format("DataProvider::DataProcessing(%s)%n", command);
		if (data_provider_ == null)
			return;
		try {
			// System.out.println("<----- DataProvider.DataProcessing() before GetResponseObject");
			Utils.trace("   DataProvider.DataProcessing GetResponseObject() start");
			GetResponseObject(command, (Response) result_data);
			Utils.trace("   DataProvider.DataProcessing GetResponseObject() end");
			// System.out.println("===== DataProvider.DataProcessing() after GetResponseObject");

			if (response_status == StatusCode.STATUS_OK) {
				// System.out.println("<----- DataProvider.DataProcessing()");
				Utils.trace("   data_provider_.DataProcessing start");
				data_provider_.DataProcessing(command, response_object);
				Utils.trace("   data_provider_.DataProcessing start");
				// System.out.println("===== DataProvider.DataProcessing()");
			} else {
				ErrorProcessing(command, response_status);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Override
	public void ErrorProcessing(String command, StatusCode error) {
		// System.out.format("DataProvider::ErrorProcessing(%s) %d%n", command,
		// StatusCode.toInt(error));
		response_status = error;
		if (data_provider_ == null)
			return;
		try {
			data_provider_.ErrorProcessing(command, response_status);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
