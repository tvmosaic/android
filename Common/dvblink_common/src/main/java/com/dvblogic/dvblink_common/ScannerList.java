package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

@XStreamConverter(ScannersConverter.class)
@XStreamAlias("scanners")
public class ScannerList {

    ArrayList<Standard> standards_ =  new ArrayList<Standard>();;
    public void addStandard(Standard standard) {
        standards_.add(standard);
    }
    public ArrayList<Standard> list() {
        return standards_;
    }

    static public class BaseInfo<T> implements Serializable  {
        private ArrayList<T> list_  =  new ArrayList<T>();
        public String id_;
        public String name_;

        public BaseInfo(String id, String name) {
            name_ = name;
            id_ = id;
        }
        public void add(T setting) {
            list_.add(setting);
        }
        public ArrayList<T> list() {
            if (list_ == null)
                list_ = new ArrayList<>();
            return list_;
        }

        @Override
        public String toString() {
            return name_;
        }

        public void sortAll() {
            //sort children
            for (T item: list_) {
                if (item instanceof BaseInfo) {
                    ((BaseInfo) item).sortAll();
                }
            }
            //sort itself
            if (list_.size() > 0)
                sort();
        }

        protected void sort() {
            Collections.sort(list_, new Comparator<T>() {
                public int compare(T item1, T item2) {

                    if (item1 instanceof Container)
                        return -1;

                    if (item2 instanceof Container)
                        return 1;

                    if (item1 instanceof BaseInfo && item2 instanceof BaseInfo) {
                        BaseInfo bi1 = (BaseInfo)item1;
                        BaseInfo bi2 = (BaseInfo)item2;
                        return bi1.name_.compareToIgnoreCase(bi2.name_);
                    }

                    return -1;
                }
            });

        }

    }

    static public class Standard extends BaseInfo<Setting> {
        public Standard(String id, String name) {
            super(id, name);
        }
        public Container standardSettings = null;
    }

    static public class Setting extends BaseInfo<BaseInfo> {
        public Setting(String id, String name) {
            super(id, name);
        }
    }

    static public class Value extends BaseInfo {
        public final String description_;
        public final String country_;

        public Value(String id, String name, String description, String country) {
            super(id, name);
            description_ = description;
            country_ = country;
        }
    }
    static public class Container extends BaseInfo<Field> {
        public final String description_;
        public Container(String id, String name, String description) {
            super(id, name);
            description_ = description == null ? "" : description;
        }
    }

    static public class Field implements Serializable {
        public String id_;
        public String name_;

        public Field(String id, String name) {
            name_ = name;
            id_ = id;
        }

        @Override
        public String toString() {
            return name_;
        }
    }

    static public class EditableField extends Field {
        public String valueFormat;
        public String defaultValue_;
        public EditableField(String id, String name, String format, String defaultValue) {
            super(id, name);
            valueFormat = format;
            defaultValue_ = defaultValue;
        }
    }

    static public class SelectField extends Field {
        private ArrayList<Field> options_ = new ArrayList<Field>();
        public SelectField(String id, String name) {
            super(id, name);
        }
        public void add(Field selectFieldOption) {
            options_.add(selectFieldOption);
        }
        public ArrayList<Field> list() {
            return options_;
        }
    }
}
