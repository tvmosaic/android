package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;


public class EPGMatchResultsConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(EPGMatchResults.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        EPGMatchResults results = new EPGMatchResults();
        while (reader.hasMoreChildren()) {
            reader.moveDown();
            String nodeName = reader.getNodeName();
            if (!nodeName.equalsIgnoreCase("channel")) {
                continue;
            }

            String channelId = reader.getAttribute("id");
            EPGMatchResults.EPGChannelsMatch epgChannels = new EPGMatchResults.EPGChannelsMatch();
            while (reader.hasMoreChildren()) {
                reader.moveDown();
                nodeName = reader.getNodeName();
                if (nodeName.equalsIgnoreCase("exact") || nodeName.equalsIgnoreCase("partial")) {
                    while (reader.hasMoreChildren()) {
                        reader.moveDown();
                        String nestedNodeName = reader.getNodeName();
                        if (nestedNodeName.equalsIgnoreCase("epg_channel")) {
                            String epgSourceId = reader.getAttribute("epg_source_id");
                            String epgChannelId = reader.getAttribute("epg_channel_id");
                            EPGMatchResults.EPGChannel epgChannel = new EPGMatchResults.EPGChannel();
                            epgChannel.epgSourceId = epgSourceId;
                            epgChannel.epgChannelId = epgChannelId;

                            EPGMatchResults.EPGChannels epgChannelList = nodeName.equalsIgnoreCase("exact") ?
                                    epgChannels.exact : epgChannels.partial;
                            epgChannelList.add(epgChannel);
                        }
                        reader.moveUp();
                    }

                }
                reader.moveUp();
            }
            results.put(channelId, epgChannels);
            reader.moveUp();
        }
        return results;
    }
}
