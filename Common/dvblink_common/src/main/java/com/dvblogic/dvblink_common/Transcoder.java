package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("transcoder")
public class Transcoder extends Serializer
{
	public static final long	serialVersionUID	= 1L;
    public static final long    invalidTranscoderValue = 0;
    public static final String  transcoderAudioTrackAny = "";

	@XStreamAlias("height")
	public long					Height;

	@XStreamAlias("width")
	public long					Width;

	@XStreamAlias("bitrate")
	public long					Bitrate;

	@XStreamAlias("audio_track")
	public String				AudioTrack;

	@XStreamAlias("scale")
	public long				VideoScale;

	public Transcoder(long height, long width, long bitrate, String audio_track, long scale) {
		Height = height;
		Width = width;
		Bitrate = bitrate;
		AudioTrack = audio_track;
        VideoScale = scale;
	}

	public Transcoder() {
		Height = invalidTranscoderValue;
		Width = invalidTranscoderValue;
		Bitrate = invalidTranscoderValue;
        VideoScale = invalidTranscoderValue;
		AudioTrack = transcoderAudioTrackAny;
	}
}
