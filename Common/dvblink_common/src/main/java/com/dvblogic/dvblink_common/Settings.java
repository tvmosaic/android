package com.dvblogic.dvblink_common;

import java.util.ArrayList;

public abstract class Settings
{
	public enum EChannelSortMode {
		CH_SORT_MODE_NAME(0), CH_SORT_MODE_NUMBER(1);

		@SuppressWarnings("unused")
		private int	mode;

		EChannelSortMode(int _mode) {
			mode = _mode;
		}
	}

	public static final int				START_FIRST_GUIDE				= 100;
	public static final int				START_FIRST_TVRECORDS			= 101;
	public static final int				START_FIRST_MEDIA_LIBRARY		= 102;

	private ArrayList<String>				_AudioTracks;

	public static final String				LANGUAGE_DEFAULT				= "English";
	public static final boolean				TRANSCODING_ENABLED_DEFAULT		= false;
	public static final String				AUDIO_TRACK_DEFAULT				= "eng";
	public static final long				VIDEO_SCALING_DEFAULT			= 1;
	public static final long				BITRATE_DEFAULT				    = 512;
	public static final EChannelSortMode	CH_SORT_MODE_DEFAULT			= EChannelSortMode.CH_SORT_MODE_NAME;
	public static final int				    DEINTERLACE_MODE_DEFAULT		= 1;
	public static final boolean				AUDIO_PASSTHROUGH_DEFAULT		= false;
	public static final int				    REFRESH_RATE_DEFAULT			= -1;
	public static final boolean				TIMESHIFT_SUPPORTED_DEFAULT			= false;
	public static final boolean				TIMESHIFT_ENABLED_DEFAULT			= true;
	public static final boolean				PLAY_LAST_CHANNEL_ON_START_DEFAULT	= false;
	public static final boolean				SHOW_SERVER_SELECTION_DEFAULT	= false;
	public static final long				CHROMECAST_BITRATE_KBS_DEFAULT		= 4096;
	public static final int 				START_FIRST_DEFAULT				= START_FIRST_GUIDE;

	public static final String				ENG_AUDIO_TRACK					= "eng";
	public static final String				RUS_AUDIO_TRACK					= "rus";
	public static final String				UKR_AUDIO_TRACK					= "ukr";
	public static final String				BUL_AUDIO_TRACK					= "bul";
	public static final String				CES_AUDIO_TRACK					= "ces";
	public static final String				DAN_AUDIO_TRACK					= "dan";
	public static final String				DEU_AUDIO_TRACK					= "deu";
	public static final String				FIN_AUDIO_TRACK					= "fin";
	public static final String				FRA_AUDIO_TRACK					= "fra";
	public static final String				ELL_AUDIO_TRACK					= "ell";
	public static final String				HRV_AUDIO_TRACK					= "hrv";
	public static final String				HUN_AUDIO_TRACK					= "hun";
	public static final String				ITA_AUDIO_TRACK					= "ita";
	public static final String				NOR_AUDIO_TRACK					= "nor";
	public static final String				POL_AUDIO_TRACK					= "pol";
	public static final String				SLK_AUDIO_TRACK					= "slk";
	public static final String				SLV_AUDIO_TRACK					= "slv";
	public static final String				SPA_AUDIO_TRACK					= "spa";
	public static final String				SWE_AUDIO_TRACK					= "swe";
	public static final String				TUR_AUDIO_TRACK					= "tur";
	public static final String				NLD_AUDIO_TRACK					= "nld";

	public static final String				LANGUAGE_KEY					= "language";
	public static final String				AUDIO_TRACK_KEY					= "audio_track";
	public static final String				BITRATE_KEY					    = "bitrate";
	public static final String				VIDEO_SCALING_KEY				= "video_scaling";
	public static final String				CH_SORT_MODE_KEY				= "channel_sort_mode";
	public static final String				TRANSCODING_ENABLED_KEY			= "transcoding_enabled";
    public static final String				SHOW_CHANNEL_NUMBER_KEY			= "show_channel_number";
	public static final String				USE_PROGRAM_COLORS_KEY			= "use_program_colors";
	public static final String				AUDIO_PASSTHROUGH_KEY			= "audio_passthrough";
	public static final String				DEINTERLACE_MODE_KEY			= "deinterlacing_mode";
	public static final String				REFRESH_RATE_KEY				= "refresh_rate";
	public static final String				TIMESHIFT_SUPPORTED_KEY		    = "time_shift_supported";
	public static final String				TIMESHIFT_ENABLED_KEY		    = "time_shift_enabled";
	public static final String 				PLAY_LAST_CHANNEL_ON_START 		= "play_last_channel_on_start";
	public static final String 				PLAY_LAST_CHANNEL_ID 			= "play_last_channel_id";
    public static final String				TVRECORDS_SORT_KEY				= "tvrecords_sort_mode";
	public static final String				SHOW_SERVER_SELECTION_KEY	    = "show_server_selection";
	public static final String				CHROMECAST_BITRATE_KBS_KEY	    = "chromecast_bitrate";
	public static final String				START_FIRST_ACTIVITY_KEY	    = "start_first_activity";


	public Settings() {
		_AudioTracks = new ArrayList<String>();
		_AudioTracks.add(BUL_AUDIO_TRACK);
		_AudioTracks.add(CES_AUDIO_TRACK);
		_AudioTracks.add(DAN_AUDIO_TRACK);
		_AudioTracks.add(DEU_AUDIO_TRACK);
		_AudioTracks.add(ELL_AUDIO_TRACK);
		_AudioTracks.add(ENG_AUDIO_TRACK);
		_AudioTracks.add(FIN_AUDIO_TRACK);
		_AudioTracks.add(FRA_AUDIO_TRACK);
		_AudioTracks.add(HRV_AUDIO_TRACK);
		_AudioTracks.add(HUN_AUDIO_TRACK);
		_AudioTracks.add(ITA_AUDIO_TRACK);
		_AudioTracks.add(NLD_AUDIO_TRACK);
		_AudioTracks.add(NOR_AUDIO_TRACK);
		_AudioTracks.add(POL_AUDIO_TRACK);
		_AudioTracks.add(RUS_AUDIO_TRACK);
		_AudioTracks.add(SLK_AUDIO_TRACK);
		_AudioTracks.add(SLV_AUDIO_TRACK);
		_AudioTracks.add(SPA_AUDIO_TRACK);
		_AudioTracks.add(SWE_AUDIO_TRACK);
		_AudioTracks.add(TUR_AUDIO_TRACK);
		_AudioTracks.add(UKR_AUDIO_TRACK);

	}

	public void Save() {
		SaveSettings();
	}

	public void setString(String key, String value) {
		AddOrUpdateValue(key, value);		
	}
	
	public String getString(String key) {
		return GetValueOrDefault(key, "");
	}
	
	public void setValue(String key, Object value) {
		AddOrUpdateValue(key, value);
	}
	
	public <T> T getValue(String key, T default_value) {
		return GetValueOrDefault(key, default_value);
	}
		
	public void setAudioTracks(ArrayList<String> list) {
		_AudioTracks = list;
	}

	public ArrayList<String> getAudioTracks() {
		return _AudioTracks;
	}

	public EChannelSortMode getChannelSortMode() {
		return GetValueOrDefault(CH_SORT_MODE_KEY, CH_SORT_MODE_DEFAULT);
	}

    public String getTVRecordsSort() {
        return getString(TVRECORDS_SORT_KEY);
    }
    public void setTVRecordsSort(String sortID) {
        setString(TVRECORDS_SORT_KEY, sortID);
    }

	public void setChannelSortMode(EChannelSortMode value) {
		AddOrUpdateValue(CH_SORT_MODE_KEY, value);
	}

	public String getLanguage() {
		return GetValueOrDefault(LANGUAGE_KEY, LANGUAGE_DEFAULT);
	}

	public void setLanguage(String value) {
		AddOrUpdateValue(LANGUAGE_KEY, value);
	}

	public ServerSettings getServerSettings() {
		
		String xml = getString("server_settings");
		ServerSettings ss = (ServerSettings) Serializer.deserialize(xml, ServerSettings.class);
		if (ss == null) {
			ss = new ServerSettings();
		}
		return ss;
	}

	public void setServerSettings(ServerSettings ss) {
		
		String xml = Serializer.serialize(ss);  
		setString("server_settings", xml);
	}

	public ServerSettingsList getServerSettingsList() {
		ServerSettingsList ssl = ServerSettingsList.load(this);
		return (ssl == null ? new ServerSettingsList() : ssl); 
	}
	
	public void setServerSettingsList(ServerSettingsList  ssl) {
		ssl.save(this); 
	}
	
	protected abstract void SaveSettings();

	protected abstract <T> boolean AddOrUpdateValue(String key, T value);

	protected abstract <T> T GetValueOrDefault(String key, T default_value);
}
