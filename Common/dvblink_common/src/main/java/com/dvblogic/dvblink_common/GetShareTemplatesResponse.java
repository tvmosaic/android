package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("get_templates_out_param")
public class GetShareTemplatesResponse extends Serializer
{
	@XStreamAlias("templates")
	public ShareTemplateList	Templates;
}
