package com.dvblogic.dvblink_common;

public class ContainerType
{
	public static final int	CONTAINER_UNKNOWN			= -1;
	public static final int	CONTAINER_SOURCE			= 0;
	public static final int	CONTAINER_TYPE				= 1;
	public static final int	CONTAINER_CATEGORY			= 2;
	public static final int	CONTAINER_CATEGORY_SORT		= 3;
	public static final int	CONTAINER_CATEGORY_GROUP	= 4;
	public static final int	CONTAINER_GROUP				= 5;

}
