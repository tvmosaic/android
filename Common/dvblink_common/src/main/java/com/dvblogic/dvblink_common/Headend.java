package com.dvblogic.dvblink_common;

import java.util.ArrayList;

public class Headend {
	public String id;
	public String name;
	public String desc;
	public ArrayList<Transponder> transponders = new ArrayList<>();
}
