package com.dvblogic.dvblink_common;

import android.os.Bundle;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

@XStreamConverter(StartScanConverter.class)
@XStreamAlias("start_scan")
public class StartScanRequest extends Serializer {

    public static String providerKey = "provider";
    public static String networkKey = "network";
    public static String deviceKey = "device";

    public String deviceID;
    public Bundle params;

    public StartScanRequest(String deviceID_, Bundle params_) {
        deviceID = deviceID_;
        params = params_;
    }
}
