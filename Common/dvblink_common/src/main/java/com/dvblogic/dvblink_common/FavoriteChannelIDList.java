package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("channels")
public class FavoriteChannelIDList extends Serializer
{

	@XStreamImplicit(itemFieldName = "channel")
	private ArrayList<String>	_list;

	public FavoriteChannelIDList() {
		_list = new ArrayList<String>();
	}

	public FavoriteChannelIDList(String id) {
		this();
		this.add(id);
	}

	public FavoriteChannelIDList(ArrayList<String> ids) {
		this();
		this._list.addAll(ids);
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<String>();
		}
		return this;
	}

	public ArrayList<String> list() {
		if (_list == null)
			_list = new ArrayList<>();
		return _list;
	}

	public void add(String ch) {
		if (_list == null) 
			_list = new ArrayList<String>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 :_list.size());
	}

	public String get(int idx) {
		return (_list == null ? null :_list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
