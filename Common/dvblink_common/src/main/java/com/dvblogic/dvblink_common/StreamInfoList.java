package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("stream_info")
public class StreamInfoList extends Serializer
{
	@XStreamImplicit(itemFieldName = "channel")
	private ArrayList<StreamInfo>	_list;

	public StreamInfoList() {
		_list = new ArrayList<StreamInfo>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<StreamInfo>();
		}
		return this;
	}

	public ArrayList<StreamInfo> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(StreamInfo ch) {
		if (_list == null) {
			_list = new ArrayList<StreamInfo>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public StreamInfo get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
