package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("manual_device")
public class DeleteDeviceRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    public DeleteDeviceRequest(String deviceID_) {
        deviceID = deviceID_;
    }
}
