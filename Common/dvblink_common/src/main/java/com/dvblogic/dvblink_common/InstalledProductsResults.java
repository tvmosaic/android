package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.util.ArrayList;


@XStreamConverter(InstalledProductsResultsConverter.class)
@XStreamAlias("installed_products")
public class InstalledProductsResults extends ArrayList<InstalledProductsResults.Product> {
    public static class Product {
        public String id;
        public String name;
        public String version;
        public String build;
        public String fingerprint;
        public LicenseState licenseState;
        public Boolean trialAvailable = false;
        public Boolean requiresRegistration = false;
        public Boolean requiresSubscription = false;
        public Boolean requiresCoupon = false;
        public Boolean activationInProgress = false;
        public int daysLeft = 0;
        public String licenseName = "";
        public String licenseKey = "";
        public String machineId = "";
    }

    public enum LicenseState {
        FREE,
        WRONG_FP,
        TRIAL,
        REGISTERED,
        EXPIRED,
        NO_LICENSE_FILE,
        NO_SUBSCRIPTION,
        SUBSCRIBED,
        SUBSCRIPTION_EXPIRED,
        SUBSCRIPTION_WRONG_FP,
        NO_COUPON,
        COUPON_WRONG_FP
    }


}
