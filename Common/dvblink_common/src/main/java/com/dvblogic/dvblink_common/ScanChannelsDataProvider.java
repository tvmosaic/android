package com.dvblogic.dvblink_common;

import java.util.Locale;


@SuppressWarnings("WeakerAccess")
public class ScanChannelsDataProvider extends AdviserDataProvider
{
	private static final String	GET_DEVICES_CMD		= "get_devices";
	private static final String	GET_DEVICE_TEMPLATES_CMD		= "get_device_templates";
	private static final String	CREATE_DEVICE_CMD		= "create_manual_device";
	private static final String	DELETE_DEVICE_CMD		= "delete_manual_device";
    private static final String	GET_DEVICE_SETTINGS_CMD		= "get_device_settings";
    private static final String	SET_DEVICE_SETTINGS_CMD		= "set_device_settings";
	private static final String	GET_SCANNERS_CMD	= "get_scanners";
	private static final String	GET_NETWORKS_CMD	= "get_networks";
	private static final String	START_SCAN_CMD		= "start_scan";
	private static final String	APPLY_SCAN_CMD		= "apply_scan";
	private static final String	CANCEL_SCAN_CMD		= "cancel_scan";
	private static final String	DEVICE_STATUS_CMD	= "get_device_status";
    private static final String	DELETE_PROVIDER_CMD	= "drop_provider_on_device";
    private static final String	RESCAN_PROVIDER_CMD		= "rescan_provider";
	private static final String	GET_RESCAN_SETTINGS_CMD		= "get_rescan_settings";

	private static final String	GET_ALL_CHANNELS_CMD	= "get_all_channels";
	private static final String	GET_SCANNED_CHANNELS_CMD	= "get_scanned_channels";
	private static final String	GET_CHANNELS_VISIBILITY_CMD	= "get_channels_visibility";
	private static final String	SET_CHANNELS_VISIBILITY_CMD	= "set_channels_visibility";
	private static final String	REPAIR_DB_CMD				= "repair_database";

	private static final String	GET_EPG_SOURCES_CMD	= "get_epg_sources";
	private static final String	GET_EPG_CHANNELS_CMD	= "get_epg_channels";
	private static final String	GET_EPG_CHANNEL_CONFIG_CMD	= "get_epg_channel_config";
	private static final String	SET_EPG_CHANNEL_CONFIG_CMD	= "set_epg_channel_config";
	private static final String	MATCH_EPG_CHANNELS_CMD	= "match_epg_channels";

	private static final String	GET_INSTALLED_PRODUCTS_CMD	= "get_installed_products";
	private static final String	ACTIVATE_PRODUCT_CMD	= "activate_product";
	private static final String	ACTIVATE_PRODUCT_TRIAL_CMD	= "activate_product_trial";

	private static final String	CHECK_UPDATE_CMD	= "updater_check_update";
	private static final String	GET_UPDATE_STATUS_CMD	= "updater_get_status";
	private static final String	START_UPDATE_CMD	= "updater_start_update";


	public ScanChannelsDataProvider(ServerSettings ss, IDataProvider data_provider) throws Exception {
		super(ss, data_provider);
	}

	public void GetDevices(DevicesRequest request) {
		StartGetData(GET_DEVICES_CMD, request);
	}

    public void GetDeviceTemplates(DeviceTemplatesRequest request) {
        StartGetData(GET_DEVICE_TEMPLATES_CMD, request);
    }

    public void GetDeviceSettings(GetDeviceSettingsRequest request) {
        StartGetData(GET_DEVICE_SETTINGS_CMD, request);
    }

    public void SetDeviceSettings(SetDeviceSettingsRequest request) {
        StartGetData(SET_DEVICE_SETTINGS_CMD, request);
    }

	public void CreateDevice(CreateDeviceRequest request) {
		StartGetData(CREATE_DEVICE_CMD, request);
	}

	public void DeleteDevice(DeleteDeviceRequest request) {
		StartGetData(DELETE_DEVICE_CMD, request);
	}

    public void DeleteProvider(DeleteProviderRequest request) {
        StartGetData(DELETE_PROVIDER_CMD, request);
    }

    public void RescanProvider(RescanProviderRequest request) {
        StartGetData(RESCAN_PROVIDER_CMD, request);
    }

	public void GetRescanSettings(RescanSettingsRequest request) {
		StartGetData(GET_RESCAN_SETTINGS_CMD, request);
	}

	public void GetScanners(ScannersRequest request) {
		StartGetData(GET_SCANNERS_CMD, request);
	}

	public void StartScan(StartScanRequest request) {
		StartGetData(START_SCAN_CMD, request);
	}

	public void DeviceStatus(DeviceStatusRequest request) {
		StartGetData(DEVICE_STATUS_CMD, request);
	}

	public void GetNetworks(NetworksRequest request) {
		StartGetData(GET_NETWORKS_CMD, request);
	}

	public void ApplyScan(ApplyScanRequest request) {
		StartGetData(APPLY_SCAN_CMD, request);
	}
	public void CancelScan(CancelScanRequest request) {
		StartGetData(CANCEL_SCAN_CMD, request);
	}

	public void GetAllChannels(ChannelsRequest request) {
		StartGetData(GET_ALL_CHANNELS_CMD, request);
	}

	public void GetScannedChannels(ScannedChannelsRequest request) {
		StartGetData(GET_SCANNED_CHANNELS_CMD, request);
	}

	public void GetChannelsVisibility(GetChannelsVisibilityRequest request) {
		StartGetData(GET_CHANNELS_VISIBILITY_CMD, request);
	}
	public void SetChannelsVisibility(SetChannelsVisibilityRequest request) {
		StartGetData(SET_CHANNELS_VISIBILITY_CMD, request);
	}

	public void RepairDB() {
		RepairDBRequest request = new RepairDBRequest();
		StartGetData(REPAIR_DB_CMD, request);
	}

	public void GetEPGSources() {
		StartGetData(GET_EPG_SOURCES_CMD, new GetEPGSourcesRequest());
	}
	public void GetEPGChannels(GetEPGChannelsRequest request) {
		StartGetData(GET_EPG_CHANNELS_CMD, request);
	}
	public void GetEPGChannelConfig(GetEPGChannelConfigRequest request) {
		StartGetData(GET_EPG_CHANNEL_CONFIG_CMD, request);
	}
	public void SetEPGChannelConfig(EPGChannelConfigList request) {
		StartGetData(SET_EPG_CHANNEL_CONFIG_CMD, request);
	}
	public void MatchEPGChannels(EPGMatchChannelsRequest request) {
		StartGetData(MATCH_EPG_CHANNELS_CMD, request);
	}

	public void GetInstalledProducts() {
		GetInstalledProductsRequest request = new GetInstalledProductsRequest();
		StartGetData(GET_INSTALLED_PRODUCTS_CMD, request);
	}
	public void ActivateProduct(ActivateProductRequest request) {
		StartGetData(ACTIVATE_PRODUCT_CMD, request);
	}
	public void ActivateProductTrial(ActivateProductTrialRequest request) {
		StartGetData(ACTIVATE_PRODUCT_TRIAL_CMD, request);
	}

	public void CheckUpdate() {
		StartGetData(CHECK_UPDATE_CMD, new CheckUpdateRequest());
	}
	public void GetUpdateStatus() {
		StartGetData(GET_UPDATE_STATUS_CMD, new UpdateStatusRequest());
	}
	public void StartUpdate() {
		StartGetData(START_UPDATE_CMD, new StartUpdateRequest());
	}


	protected void GetResponseObject(String command, Response response) {
		response_status = StatusCode.STATUS_OK;
		response_object = null;

		try {
			response_object = response.Result;
			String cmd = command.toLowerCase(Locale.ENGLISH);
			if (StatusCode.equals(StatusCode.STATUS_OK, response.Status)) {
				if (cmd.equals(GET_DEVICES_CMD)) {
					response_object = Deserialize(DeviceList.class, response.Result);
				} else if (cmd.equals(GET_DEVICE_TEMPLATES_CMD)) {
					response_object = Deserialize(DeviceTemplateList.class, response.Result);
                } else if (cmd.equals(GET_DEVICE_SETTINGS_CMD)) {
                    response_object = Deserialize(DeviceSettings.class, response.Result);
                } else if (cmd.equals(GET_RESCAN_SETTINGS_CMD)) {
                    response_object = Deserialize(RescanSettings.class, response.Result);
                } else if (cmd.equals(SET_DEVICE_SETTINGS_CMD)) {
                    response_object = true;
                } else if (cmd.equals(CREATE_DEVICE_CMD)) {
                    response_object = true;
                } else if (cmd.equals(DELETE_DEVICE_CMD)) {
                    response_object = true;
                } else if (cmd.equals(DELETE_PROVIDER_CMD)) {
                    response_object = true;
                } else if (cmd.equals(RESCAN_PROVIDER_CMD)) {
                    response_object = true;
				} else if (cmd.equals(GET_SCANNERS_CMD)) {
					response_object = Deserialize(ScannerList.class, response.Result);
				} else if (cmd.equals(START_SCAN_CMD)) {
                    response_object = Deserialize(StartScanResponse.class, response.Result);
				} else if (cmd.equals(DEVICE_STATUS_CMD)) {
					response_object = Deserialize(DeviceStatus.class, response.Result);
				} else if (cmd.equals(GET_NETWORKS_CMD)) {
					response_object = Deserialize(NetworkList.class, response.Result);
				} else if (cmd.equals(APPLY_SCAN_CMD)) {
					response_object = true;
				} else if (cmd.equals(CANCEL_SCAN_CMD)) {
					response_object = true;
				} else if (cmd.equals(GET_ALL_CHANNELS_CMD)) {
					response_object = Deserialize(ChannelsList.class, response.Result);
				} else if (cmd.equals(GET_SCANNED_CHANNELS_CMD)) {
					response_object = Deserialize(ScannedChannelsList.class, response.Result);
				} else if (cmd.equals(GET_CHANNELS_VISIBILITY_CMD)) {
					response_object = Deserialize(InvisibleChannelsList.class, response.Result);
				} else if (cmd.equals(SET_CHANNELS_VISIBILITY_CMD)) {
					response_object = true;
				} else if (cmd.equals(REPAIR_DB_CMD)) {
					response_object = true;
				} else if (cmd.equals(GET_EPG_SOURCES_CMD)) {
					response_object = Deserialize(EPGSourceList.class, response.Result);
				} else if (cmd.equals(GET_EPG_CHANNELS_CMD)) {
					response_object = Deserialize(EPGChannelList.class, response.Result);
				} else if (cmd.equals(GET_EPG_CHANNEL_CONFIG_CMD)) {
					response_object = Deserialize(EPGChannelConfigList.class, response.Result);
				} else if (cmd.equals(SET_EPG_CHANNEL_CONFIG_CMD)) {
					response_object = true;
				} else if (cmd.equals(MATCH_EPG_CHANNELS_CMD)) {
					response_object = Deserialize(EPGMatchResults.class, response.Result);
				} else if (cmd.equals(GET_INSTALLED_PRODUCTS_CMD)) {
					response_object = Deserialize(InstalledProductsResults.class, response.Result);
				} else if (cmd.equals(ACTIVATE_PRODUCT_CMD)) {
					response_object = Deserialize(ActivateProductResponse.class, response.Result);
				} else if (cmd.equals(ACTIVATE_PRODUCT_TRIAL_CMD)) {
					response_object = Deserialize(ActivateProductResponse.class, response.Result);
				} else if (cmd.equals(CHECK_UPDATE_CMD)) {
					response_object = true;
				} else if (cmd.equals(GET_UPDATE_STATUS_CMD)) {
					response_object = Deserialize(UpdaterGetStatusResponse.class, response.Result);
				} else if (cmd.equals(START_UPDATE_CMD)) {
					response_object = true;
				}
				else {
					super.GetResponseObject(command, response);
				}
			} else {
				response_status = StatusCode.fromInt(response.Status);
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			response_status = StatusCode.STATUS_INVALID_DATA;
		}
	}

	protected void adviserStartGetData(String command, Object param) {
		lastXmlCommand = command;
		String xml_string = GetRequestString(lastXmlCommand, param);
		XmlCommand xmlcmd = new XmlCommand(ADVISER_ADDRESSEE, lastXmlCommand, xml_string);
		super.StartGetData(EXECUTE_COMMAND_CMD, xmlcmd);
	}

}
