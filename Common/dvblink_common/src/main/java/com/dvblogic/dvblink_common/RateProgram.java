package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rate_program")
public class RateProgram extends Serializer
{
	@XStreamAlias("profile_id")
	public String	ProfileId;

	@XStreamAlias("dvblink_channel_id")
	public String		DVBLinkChannelId;

	@XStreamAlias("start_time")
	public long		StartTime;

	@XStreamAlias("end_time")
	public long		EndTime;

	@XStreamAlias("rating")
	public float	Rating;
}
