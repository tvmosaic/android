package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("object_searcher")
public class ObjectSearcher extends Serializer
{
	public ObjectSearcher() {
	}
	
	public ObjectSearcher(String object_id, String server_address, String searchString) {
		ObjectID = object_id;
		ServerAddress = server_address;
		ObjectType = com.dvblogic.dvblink_common.ObjectType.OBJECT_UNKNOWN;
		ItemType = com.dvblogic.dvblink_common.ItemType.ITEM_UNKNOWN;
		SearchString = searchString;
	}

	@XStreamAlias("object_id")
	public String	ObjectID;

	@XStreamAlias("server_address")
	public String	ServerAddress;

	// ObjectType
	@XStreamAlias("object_type")
	public int		ObjectType;

	@XStreamAlias("item_type")
	public int		ItemType;

	@XStreamAlias("search_string")
	public String		SearchString;
}
