package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("get_resume_info")
public class GetResumeInfoRequest extends Serializer {
    @XStreamAlias("object_id")
    public String objectID;

    public GetResumeInfoRequest(String objectID_) {
        objectID = objectID_;
    }
}
