package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("xml_cmd")
public class XmlCommand extends Serializer
{
	@XStreamAlias("cmd")
	public String	Command;

	@XStreamAlias("param")
	public String	Params;

	@XStreamAlias("addressee")
	public String	Addressee;

	public XmlCommand(String addressee, String command, String parameters) {
		Command = command;
		Params = parameters;
		Addressee = addressee;
	}
}
