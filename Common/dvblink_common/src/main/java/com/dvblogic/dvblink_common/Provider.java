package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("provider")
public class Provider extends Serializer
{
	@XStreamAlias("provider_id")
	public String ProviderId;

	@XStreamAlias("provider_name")
	public String ProviderName;

	@XStreamAlias("provider_desc")
	public String ProviderDesc;
}
