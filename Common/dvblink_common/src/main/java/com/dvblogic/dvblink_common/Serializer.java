package com.dvblogic.dvblink_common;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Scanner;

import android.util.Log;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.reflection.AbstractReflectionConverter.DuplicateFieldException;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.xml.XmlFriendlyNameCoder;
import com.thoughtworks.xstream.io.xml.XppDriver;
import com.thoughtworks.xstream.mapper.CannotResolveClassException;
import com.thoughtworks.xstream.mapper.MapperWrapper;

public class Serializer
{
	private static XStream xstream_ = null;

    private static XStream get_xsetram_instance() {

		if (xstream_ == null) {
			XmlFriendlyNameCoder replacer = new XmlFriendlyNameCoder("ddd", "_");
			xstream_ = new XStream(new XppDriver(replacer)) {
				protected MapperWrapper wrapMapper(MapperWrapper next) {
					return new MapperWrapper(next) {
						@SuppressWarnings("rawtypes")
						public boolean shouldSerializeMember(Class definedIn, String fieldName) {
							try {
								return definedIn != Object.class || realClass(fieldName) != null;
							} catch (CannotResolveClassException cnrce) {
								cnrce.printStackTrace();
								return false;
							}
						}
					};
				}
			};
			xstream_.registerConverter(new BooleanConverter());
		}

		return xstream_;
	}

	public static String serialize(Object obj) {
		synchronized (get_xsetram_instance()) {
			try {
				Class<?> cls = obj.getClass();
				get_xsetram_instance().processAnnotations(cls);
				String xml = get_xsetram_instance().toXML(obj);
				return "<?xml version='1.0' encoding='utf-8'?>\n" + xml;
			} catch (DuplicateFieldException df) {
				System.out.println(df.getMessage());
			} catch (Exception e) {
				System.out.println(e.getMessage());
			}
		}
		return null;
	}

	public static Object deserialize(String xml, Class<?> cls) {
		synchronized (get_xsetram_instance()) {
			try {
				xstream_.processAnnotations(cls);
				return xstream_.fromXML(xml);
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("Serializer", "deserialize error", e);
				return null;
			}
		}
	}

	public static Object deserialize(HierarchicalStreamReader reader, Class<?> cls) {
		synchronized (get_xsetram_instance()) {
			try {
				xstream_.processAnnotations(cls);
				return xstream_.unmarshal(reader);
			} catch (Exception e) {
				e.printStackTrace();
				Log.e("Serializer", "deserialize error", e);
				return null;
			}
		}
	}

	public static String stringFromFile(String fileName) {
		FileInputStream fis = null;
		Scanner scanner = null;
		try {
			fis = new FileInputStream(fileName);
			scanner = new Scanner(fis, "UTF-8");
            return scanner.useDelimiter("\\A").next();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} finally {
			try {
				fis.close();
			} catch (Exception e) {
			}
			try {
				scanner.close();
			} catch (Exception e) {
			}
		}
		return null;
	}

	public static String stringFromStream(InputStream fis) {
		Scanner scanner = null;
		try {
			scanner = new Scanner(fis, "UTF-8");
            return scanner.useDelimiter("\\A").next();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				scanner.close();
			} catch (Exception e) {
			}
		}
		return null;
	}
}
