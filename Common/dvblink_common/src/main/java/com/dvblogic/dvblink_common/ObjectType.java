package com.dvblogic.dvblink_common;

public class ObjectType
{
	public static final int	OBJECT_UNKNOWN		= -1;
	public static final int	OBJECT_CONTAINER	= 0;
	public static final int	OBJECT_ITEM			= 1;
}
