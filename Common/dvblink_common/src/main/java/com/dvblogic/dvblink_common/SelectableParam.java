package com.dvblogic.dvblink_common;

import java.util.ArrayList;

public class SelectableParam
{

	public class SelectableParamEntry {
		public String value;
		public String name;
	}

	public SelectableParam() {
		selected = 0;
	}

	public String key;
	public String name;
	public ArrayList<SelectableParamEntry> values = new ArrayList<>();
	public int selected;

    public void addParamEntry(String value, String name) {
        SelectableParamEntry entry = new SelectableParamEntry();
        entry.value = value;
        entry.name = name;
        values.add(entry);
    }
}
