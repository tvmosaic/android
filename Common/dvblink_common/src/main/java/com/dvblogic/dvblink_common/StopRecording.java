package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("stop_recording")
public class StopRecording extends Serializer
{
	@XStreamAlias("object_id")
	public String	ObjectID;

	public StopRecording() {
	}

	public StopRecording(String object_id) {
		ObjectID = object_id;
	}

}
