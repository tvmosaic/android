package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("resume_info")
public class ResumeInfo extends Serializer {
    @XStreamAlias("pos")
    public int pos;
	
    public ResumeInfo() {
		pos = 0;
    }
}
