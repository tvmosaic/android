package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("start_scan")
public class StartScanResponse extends Serializer
{
    public static final int RESULT_ERROR = 0;
    public static final int RESULT_STARTED = 1;
    public static final int RESULT_JOINED = 2;

    @XStreamAlias("scan_start_result")
    public int result;
}
