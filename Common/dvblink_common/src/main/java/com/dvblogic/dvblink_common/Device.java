package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import java.util.ArrayList;

@XStreamAlias("device")
public class Device extends Serializer
{
    public final static String missing_state_ = "missing";
    public final static String active_state_ = "active";
    public final static String new_state_ = "new";

    @XStreamAlias("id")
    public String ID;

    @XStreamAlias("name")
    public String Name;

    @XStreamAlias("state")
    public String State;

    @XStreamAlias("status")
    public String Status;

    @XStreamAlias("standards")
    public Integer Standards;

    @XStreamAlias("has_settings")
    public Boolean hasSettings = false;

    @XStreamAlias("can_be_deleted")
    public Boolean canBeDeleted = false;

    @XStreamAlias("providers")
    public ArrayList<Provider> Providers;

    public boolean isActive() {
        return State.equalsIgnoreCase(active_state_) || State.equalsIgnoreCase(new_state_);
    }

    public boolean isMissing() {
        return State.equalsIgnoreCase(missing_state_);
    }

    public boolean isScanning() {
        return Status.equalsIgnoreCase(DeviceStatus.STATUS_SCANNING) || Status.equalsIgnoreCase(DeviceStatus.STATUS_SCAN_FINISHED) || Status.equalsIgnoreCase(DeviceStatus.STATUS_NETWORKS_SCANNED);
    }
}
