package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("get_provider_channels")
public class ProviderChannelsRequest extends Serializer
{
	@XStreamAlias("provider_id")
	public String ProviderId;
}
