package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

@XStreamAlias("source")
public class EPGSource extends Serializer {
	public static String DEFAULT_EPG_SOURCE_ID = "e96f83f5-79ad-4ea4-af2b-6682f233ad1a";

	@XStreamAsAttribute
	@XStreamAlias("id")
	public String id;

	@XStreamAsAttribute
	@XStreamAlias("name")
	public String name;

	@XStreamAsAttribute
	@XStreamAlias("settings")
	public boolean has_settings;

	@XStreamAsAttribute
	@XStreamAlias("is_default")
	public boolean is_default;

	public EPGSource(String id, String name, boolean settings) {
		this.id = id;
		this.name = name;
        this.has_settings = settings;
        this.is_default = id.equalsIgnoreCase(DEFAULT_EPG_SOURCE_ID);
	}

	public EPGSource() {
		this(DEFAULT_EPG_SOURCE_ID, "_Default_", false);
	}

	public boolean isDefault() {
		return is_default;
	}

	public boolean equals_id(String id) {
        return this.id.equalsIgnoreCase(id);
    }
}
