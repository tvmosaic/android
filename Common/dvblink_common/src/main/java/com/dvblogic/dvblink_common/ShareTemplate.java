package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("template")
public class ShareTemplate extends Serializer
{
	@XStreamAlias("id")
	public String	ID;

	@XStreamAlias("default")
	public boolean	DefaultText;

	@XStreamAlias("built_in")
	public boolean	BuiltIn;

	@XStreamAlias("text")
	public String	Text;

	@XStreamAlias("name")
	public String	Name;
}
