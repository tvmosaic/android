package com.dvblogic.dvblink_common;

/**
 * Created by Serg on 01-Sep-16.
 */
import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("timeshift_status")
public class TimeShiftStatusRequest extends Serializer
{
    @XStreamAlias("channel_handle")
    public long channel_handle;
}
