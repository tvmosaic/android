package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("schedules")
public class SchedulesList extends Serializer
{
	@XStreamImplicit(itemFieldName = "schedule")
	private ArrayList<Schedule>	_list;

	public SchedulesList() {
		_list = new ArrayList<Schedule>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Schedule>();
		}
		return this;
	}

	public ArrayList<Schedule> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Schedule ch) {
		if (_list == null) {
			_list = new ArrayList<Schedule>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Schedule get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
