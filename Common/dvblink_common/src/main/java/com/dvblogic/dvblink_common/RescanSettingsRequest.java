package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rescan_provider_settings")
public class RescanSettingsRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    @XStreamAlias("provider_id")
    public String providerID;
	
    public RescanSettingsRequest(String deviceID_, String providerID_) {
        deviceID = deviceID_;
		providerID = providerID_;
    }
}
