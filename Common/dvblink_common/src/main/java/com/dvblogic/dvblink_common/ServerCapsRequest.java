package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("streaming_caps")
public class ServerCapsRequest extends Serializer
{
	@XStreamAlias("addressees")
	public AddresseeList	Addressees;

	public ServerCapsRequest() {
		AddresseeList addressees = new AddresseeList();
		Addressee a = new Addressee();
		a.ID = DataProvider.SOCIAL_MODULE_ADDRESSEE;
		addressees.add(a);

		Addressees = addressees;
	}
}
