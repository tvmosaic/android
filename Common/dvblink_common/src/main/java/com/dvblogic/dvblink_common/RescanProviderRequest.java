package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("rescan_provider")
public class RescanProviderRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    @XStreamAlias("provider_id")
    public String providerID;

    @XStreamAlias("settings")
    public ConciseParamMap parameters;

    public RescanProviderRequest(String deviceID_, String providerID_, ConciseParamMap parameters_) {
        deviceID = deviceID_;
		providerID = providerID_;
        parameters = parameters_;
    }
}
