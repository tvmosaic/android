package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.util.HashMap;
import java.util.Map;

@XStreamConverter(ConciseParamMapConverter.class)
@XStreamAlias("concise_param_map")
public class ConciseParamMap
{
	public HashMap<String, String> parameters = new HashMap<>();
}
