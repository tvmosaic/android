package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("create_profile")
public class ProfileCreator extends Serializer
{
	@XStreamAlias("profile_name")
	public String	Name;

	@XStreamAlias("channel_id")
	public String	DVBLinkID;

	@XStreamAlias("start_time")
	public long		StartTime;

	@XStreamAlias("end_time")
	public long		EndTime;
}
