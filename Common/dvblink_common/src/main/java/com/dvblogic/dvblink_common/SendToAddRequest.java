package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

import java.util.ArrayList;

/**
 * Created by Serg on 25-Mar-16.
 */
@XStreamAlias("send_to_add_item")
public class SendToAddRequest extends Serializer {
    @XStreamImplicit(itemFieldName = "item")
    private ArrayList<SendToAddItem> _list;

    public SendToAddRequest() {
        _list = new ArrayList<SendToAddItem>();
    }

    private Object readResolve() {
        if (_list == null) {
            _list = new ArrayList<SendToAddItem>();
        }
        return this;
    }

    public ArrayList<SendToAddItem> list() {

        return _list;
    }

    public void add(SendToAddItem ch) {
        if (_list == null)
            _list = new ArrayList<SendToAddItem>();
        _list.add(ch);
    }

    public int size() {
        return (_list == null ? 0 : _list.size());
    }

    public SendToAddItem get(int idx) {
        return (_list == null ? null : _list.get(idx));
    }

    public void remove(int idx) {
        if (_list != null)
            _list.remove(idx);
    }

    public void clear() {
        if (_list != null)
            _list.clear();
    }
}
