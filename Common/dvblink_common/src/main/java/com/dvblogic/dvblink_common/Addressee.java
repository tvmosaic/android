package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("addressee")
public class Addressee extends Serializer
{
	@XStreamAlias("id")
	public String	ID;

	public Addressee() {
	}

	public Addressee(String id) {
		ID = id;
	}
}
