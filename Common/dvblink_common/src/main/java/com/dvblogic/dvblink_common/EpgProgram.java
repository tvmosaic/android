package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("program")
public class EpgProgram extends Serializer
{
	@XStreamAlias("program_id")
	public String	ProgramId;

	@XStreamAlias("program_name")
	public String	ProgramName;

	@XStreamAlias("start_time")
	public long		StartTime;

	@XStreamAlias("end_time")
	public long		EndTime;
}
