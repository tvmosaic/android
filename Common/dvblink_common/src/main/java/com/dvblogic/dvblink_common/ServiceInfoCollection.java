package com.dvblogic.dvblink_common;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class ServiceInfoCollection
{
	private static ServiceInfoCollection	inst			= null;
	private LinkedHashMap<String, ServiceInfo>	serviceInfos	= new LinkedHashMap<String, ServiceInfo>();

	public static ServiceInfoCollection Instance() {
		if (inst == null)
			inst = new ServiceInfoCollection();
		return inst;
	}

	protected ServiceInfoCollection() {
	}

	public ServiceInfo getServiceInfo(String id) {
		ServiceInfo info;
		synchronized (this) {
			info = serviceInfos.containsKey(id) ? serviceInfos.get(id) : null;
		}
		return info;
	}

	public void setServiceInfo(String id, ServiceInfo value) {
		synchronized (this) {
			serviceInfos.put(id, value);
		}
	}

	public ServiceInfo[] GetServiceInfos() {
		ServiceInfo[] arr;
		synchronized (this) {
			arr = (ServiceInfo[]) serviceInfos.values().toArray(new ServiceInfo[serviceInfos.size()]);
		}
		return arr;
	}

	public ArrayList<ServiceInfo> GetServiceInfosAsList() {
        ArrayList<ServiceInfo> arr;
		synchronized (this) {
			arr = new ArrayList<ServiceInfo>(serviceInfos.values());
		}
		return arr;
	}

	public void Clear() {
		synchronized (this) {
			serviceInfos.clear();
		}
	}
}
