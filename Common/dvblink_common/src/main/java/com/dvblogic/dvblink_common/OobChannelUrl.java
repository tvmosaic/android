package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("oob_channel_url")
public class OobChannelUrl extends Serializer
{
	@XStreamAlias("url")
	public String url;

	@XStreamAlias("mime")
	public String mime;
}
