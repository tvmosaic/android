package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("get_provider_channels")
public class ProviderChannelsList extends Serializer
{
	@XStreamImplicit(itemFieldName = "channel")
	private ArrayList<ProviderChannel>	_list;

	public ProviderChannelsList() {
		_list = new ArrayList<ProviderChannel>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<ProviderChannel>();
		}
		return this;
	}

	public ArrayList<ProviderChannel> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(ProviderChannel ch) {
		if (_list == null) {
			_list = new ArrayList<ProviderChannel>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public ProviderChannel get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
