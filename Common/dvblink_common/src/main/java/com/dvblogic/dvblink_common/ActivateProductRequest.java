package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("activate_product")
public class ActivateProductRequest extends Serializer {
    @XStreamAlias("id")
    public String id;

    @XStreamAlias("email")
    public String email;

    @XStreamAlias("serial")
    public String serial;

    @XStreamAlias("user_name")
    public String username;

    public ActivateProductRequest(String id, String email, String serial, String username) {
        this.id = id;
        this.email = email;
        this.serial = serial;
        this.username = username;
    }
}
