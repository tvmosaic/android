package com.dvblogic.dvblink_common;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

//import android.util.Base64;

public class HttpDataProvider implements Runnable
{
	/*
	 * protected class HttpDataProviderEventArgs// : EventArgs { public String
	 * Result; public String Command; public StatusCode Status; }
	 */
    protected static final String	HTTP_CONTENT_TYPE				= "application/x-www-form-urlencoded";
    protected static final String	HTTP_COMMAND_QUERY_STRING		= "command";
    protected static final String	HTTP_XML_PARAM_QUERY_STRING		= "xml_param";
    protected static final String	HTTP_COUNT_PARAM_QUERY_STRING	= "count";

    protected static int			request_count					= 0;
	protected HttpPost			request							= null;
	protected DefaultHttpClient	httpClient						= null;
    protected IDataProvider		dataProvider;
    protected String				command;
    protected String				xmlString;
	protected String language = "*";

	/*
	public HttpDataProvider(URI _serverURI, IDataProvider _dataProvider) throws Exception {
		serverURI = _serverURI;
		dataProvider = _dataProvider;

		httpClient = new DefaultHttpClient();
		request = new HttpPost(serverURI);
		request.setHeader("Authorization",getB64Auth("", ""));
		if (httpClient == null || request == null) {
			throw new Exception();
		}
	}
	*/
	public HttpDataProvider(URI _serverURI, String name, String password, IDataProvider _dataProvider) throws Exception {
		dataProvider = _dataProvider;

        boolean trust_all = false;

        if (trust_all) {
            httpClient = getTrustAllHttpClient();
        } else {
            DefaultHttpClient client = new DefaultHttpClient();
            ClientConnectionManager mgr = client.getConnectionManager();
            HttpParams params = client.getParams();

            httpClient = new DefaultHttpClient(new ThreadSafeClientConnManager(params,
                    mgr.getSchemeRegistry()), params);
        }

        if (httpClient != null) {
            final HttpParams httpParameters = httpClient.getParams();

            HttpConnectionParams.setConnectionTimeout(httpParameters, 7 * 1000); //value 7 results in 15 seconds timeout (?), whichis ok
//            HttpConnectionParams.setSoTimeout(httpParameters, 5 * 1000);
        }

		request = new HttpPost(_serverURI);
		request.setHeader("Authorization",getB64Auth(name, password));
		if (httpClient == null || request == null) {
			throw new Exception();
		}
	}

    protected DefaultHttpClient getTrustAllHttpClient() {
        try {
            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);

            TrustAllSSLSocketFactory sf = new TrustAllSSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);

            return new DefaultHttpClient(ccm, params);
        } catch (Exception e) {
            return new DefaultHttpClient();
        }
    }

	public void setLanguage(String lang) {
		language = lang;
	}

	private String getB64Auth(String login, String pass) {
		String source = login + ":" + pass;
		String ret="";
		try {
			ret = "Basic " + 
			Base64.encodeBytes(source.getBytes(), Base64.URL_SAFE);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return ret;
	}

	public void setData(String _command, String _xml_String) {
		command = _command;
		xmlString = _xml_String;
	}

	public void getData(String command, String xml_String) {
		HttpResponse response;
		try {
			Utils.trace("======================= getData(" + command + ") start");
			List<NameValuePair> nvps = new ArrayList<NameValuePair>();
			nvps.add(new BasicNameValuePair(HTTP_COMMAND_QUERY_STRING, command));
			nvps.add(new BasicNameValuePair(HTTP_XML_PARAM_QUERY_STRING, xml_String));
			nvps.add(new BasicNameValuePair(HTTP_COUNT_PARAM_QUERY_STRING, String.format("%d", ++request_count)));

			request.setEntity(new UrlEncodedFormEntity(nvps, HTTP.UTF_8));
			request.setHeader("Content-Type", HTTP_CONTENT_TYPE);
			request.setHeader("Accept-Language", language);

			Utils.trace("   execute start");
			response = httpClient.execute(request);
			Utils.trace("   execute end");

			//check if authorization is needed
            if (response.getStatusLine().getStatusCode() == 401) {
                dataProvider.ErrorProcessing(command, StatusCode.STATUS_UNAUTHORISED);
            } else {

                Utils.trace("   read start");
                InputStream content = response.getEntity().getContent();
                ByteArrayOutputStream result = new ByteArrayOutputStream();
                byte[] buffer = new byte[2048];
                int length;
                while ((length = content.read(buffer)) != -1) {
                    result.write(buffer, 0, length);
                }
                // Change in the future: StandardCharsets.UTF_8.name() if > JDK 7 and API > 19
                String strResponse = result.toString("UTF-8");

                Utils.trace("   read end");

                content.close();
                response.getEntity().consumeContent();

                Utils.trace("   deserialize start");
                Response res = (Response) Serializer.deserialize(strResponse, Response.class);
                Utils.trace("   deserialize end");

                if (res != null) {
                    try {
                        Utils.trace("   dataProvider.DataProcessing start");
                        dataProvider.DataProcessing(command, res);
                        Utils.trace("   dataProvider.DataProcessing end");
                    } catch (Exception e) {
                        System.out.println("HttpDataProvider.getData() EXCEPTION ");
                        e.printStackTrace();
                    }
                    return;
                } else {
                    dataProvider.ErrorProcessing(command, StatusCode.STATUS_INVALID_DATA);

                    System.out.println("Can't resolve class Response");
                    System.out.format("%d: %s%n", response.getStatusLine().getStatusCode(), response.getStatusLine()
                            .getReasonPhrase());
                    System.out.println(strResponse);
                }
            }
		} catch (ClientProtocolException e1) {
			dataProvider.ErrorProcessing(command, StatusCode.STATUS_INVALID_PARAM);

			System.out.print("=> ");
			System.out.println(e1.getMessage());
			// e1.printStackTrace();
		} catch (IOException e1) {
			dataProvider.ErrorProcessing(command, StatusCode.STATUS_CONNECTION_ERROR);
			// e1.printStackTrace();
		}
		Utils.trace("======================= getData(" + command + ") finish");
	}

	@Override
	public void run() {
		// System.out.println("run start");

		this.getData(command, xmlString);

		// System.out.println("run finish");
	}
}
