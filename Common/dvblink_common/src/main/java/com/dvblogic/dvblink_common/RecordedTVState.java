package com.dvblogic.dvblink_common;

/*
 public enum RecordedTVState
 {
 RTVS_IN_PROGRESS (-1),
 RTVS_ERROR (0),
 RTVS_FORCED_TO_COMPLETION (1),
 RTVS_COMPLETED (2),
 RTVS_NEW_UNK (3);

 private int state;
 private RecordedTVState(int _state) { state=_state; }
 public int toInteger() { return state; }
 public static RecordedTVState fromInteger(int v) 
 { 
 switch(v)
 {
 case -1: return RTVS_IN_PROGRESS; 
 case 0: return RTVS_ERROR; 
 case 1: return RTVS_FORCED_TO_COMPLETION; 
 case 2: return RTVS_COMPLETED; 
 case 3: return RTVS_NEW_UNK; 
 default:
 break;
 }
 return RTVS_ERROR;
 }
 }
 */

public class RecordedTVState
{
	public static final int	RTVS_IN_PROGRESS			= 0;
	public static final int	RTVS_ERROR					= 1;
	public static final int	RTVS_FORCED_TO_COMPLETION	= 2;
	public static final int	RTVS_COMPLETED				= 3;
}
