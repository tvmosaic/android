package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("epg_source_settings")
public class EPGSourceSettings extends Serializer {
	
	@XStreamAlias("container")
    public ParamContainer settings;
}


