package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("device_settings")
public class GetDeviceSettingsRequest extends Serializer {
    @XStreamAlias("device")
    public String deviceID;

    public GetDeviceSettingsRequest(String deviceID_) {
        deviceID = deviceID_;
    }
}
