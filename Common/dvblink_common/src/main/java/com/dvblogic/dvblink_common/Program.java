package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("program")
public class Program extends VideoInfo
{
	public Program() {
        super();

        IsSeries = false;
        IsRecord = false;
        IsRepeatRecord = false;
        IsRecordConflict = false;
    }

	@XStreamAlias("program_id")
	public String	ID;

	@XStreamAlias("is_series")
	public boolean	IsSeries;

	@XStreamAlias("is_record")
	public boolean	IsRecord;

    @XStreamAlias("is_repeat_record")
    public boolean	IsRepeatRecord;

    @XStreamAlias("is_record_conflict")
    public boolean	IsRecordConflict;

}
