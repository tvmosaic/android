package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.basic.AbstractSingleValueConverter;

public class BooleanConverter extends AbstractSingleValueConverter
{
	public BooleanConverter() {
	}

	@SuppressWarnings("rawtypes")
	@Override
	public boolean canConvert(final Class type) {
		return type.equals(boolean.class) || type.equals(Boolean.class);
	}

	public Object fromString(final String str) {

		return (str.equalsIgnoreCase("false") || str.equalsIgnoreCase("0") || str.equalsIgnoreCase("no")) ? Boolean.FALSE
				: Boolean.TRUE;
	}

	public String toString(final Object obj) {
		final Boolean value = (Boolean) obj;
		return obj == null ? null : value.booleanValue() ? "true" : "false";
	}
}
