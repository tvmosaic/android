package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("manual")
public class ManualSchedule extends Serializer
{
	public static final int	DAY_MASK_ONCE	= 0;
	public static final int	DAY_MASK_SUN	= 1;
	public static final int	DAY_MASK_MON	= 2;
	public static final int	DAY_MASK_TUE	= 4;
	public static final int	DAY_MASK_WED	= 8;
	public static final int	DAY_MASK_THU	= 16;
	public static final int	DAY_MASK_FRI	= 32;
	public static final int	DAY_MASK_SAT	= 64;
	public static final int	DAY_MASK_DAILY	= DAY_MASK_SUN | DAY_MASK_MON | DAY_MASK_TUE | DAY_MASK_WED | DAY_MASK_THU | DAY_MASK_FRI | DAY_MASK_SAT;

	@XStreamAlias("channel_id")
	public String			ChannelID;

	@XStreamAlias("title")
	public String			Title;

	@XStreamAlias("start_time")
	public long				StartTime;

	@XStreamAlias("duration")
	public int				Duration;

	@XStreamAlias("day_mask")
	public int				DayMask;

	@XStreamAlias("recordings_to_keep")
	public int	RecordingsToKeep;

	public ManualSchedule() {
	}
	
	public ManualSchedule(String channel_id, String title, long start_time, int duration, int day_mask, int recordings_to_keep) {
		ChannelID = channel_id;
		Title = title;
		StartTime = start_time;
		Duration = duration;
		DayMask = day_mask;
		RecordingsToKeep = recordings_to_keep;
	}
	
	public ManualSchedule(String channel_id, String title, long start_time, int duration, int day_mask) {
		ChannelID = channel_id;
		Title = title;
		StartTime = start_time;
		Duration = duration;
		DayMask = day_mask;
		RecordingsToKeep = 0;
	}
}
