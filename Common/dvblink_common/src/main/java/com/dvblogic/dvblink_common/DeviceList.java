package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("devices")
public class DeviceList extends Serializer {
    @XStreamImplicit(itemFieldName = "device")
    private ArrayList<Device> _list;

    public DeviceList() {
        _list = new ArrayList<Device>();
    }

    private Object readResolve() {
        if (_list == null) {
            _list = new ArrayList<Device>();
        }
        return this;
    }

    public ArrayList<Device> list() {
        if (_list == null)
            _list = new ArrayList<>();

        return _list;
    }

    public void add(Device dev) {
        if (_list == null)
            _list = new ArrayList<Device>();
        _list.add(dev);
    }

    public void insert(Device dev, int idx) {
        if (_list == null)
            _list = new ArrayList<Device>();
        _list.add(idx, dev);
    }

    public int size() {
        return (_list == null ? 0 : _list.size());
    }

    public Device get(int idx) {
        return (_list == null ? null : _list.get(idx));
    }

    public void remove(int idx) {
        if (_list != null)
            _list.remove(idx);
    }

    public void clear() {
        if (_list != null)
            _list.clear();
    }
}


