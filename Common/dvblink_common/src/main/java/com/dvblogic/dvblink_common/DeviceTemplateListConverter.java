package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

/**
 * Created by andreyp on 08.11.2016.
 */
public class DeviceTemplateListConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(DeviceTemplateList.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader, UnmarshallingContext context) {

        DeviceTemplateList templates = null;

        ParamContainerConverter conv = new ParamContainerConverter();

        while (reader.hasMoreChildren()) {
            reader.moveDown();

            if (reader.getNodeName().equals("container")) {
                templates = new DeviceTemplateList();
                templates.template_container = (ParamContainer) conv.unmarshal(reader, context);
            }

            reader.moveUp();
        }
        return templates;
    }
}
