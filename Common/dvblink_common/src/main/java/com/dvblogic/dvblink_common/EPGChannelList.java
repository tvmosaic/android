package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("epg_channels")
public class EPGChannelList extends Serializer
{
	@XStreamImplicit(itemFieldName = "epg_channel")
	private ArrayList<EPGChannel>	_list;

	public EPGChannelList() {
		_list = new ArrayList<>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<>();
		}
		return this;
	}

	public ArrayList<EPGChannel> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(EPGChannel ch) {
		if (_list == null)
			_list = new ArrayList<>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public EPGChannel get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
