package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("item")
public class Item extends Serializer
{
	@XStreamAlias("object_id")
	public String	ObjectID;

	@XStreamAlias("parent_id")
	public String	ParentID;

	@XStreamAlias("url")
	public String	Url;

	@XStreamAlias("thumbnail")
	public String	Thumbnail;

	@XStreamAlias("can_be_deleted")
	public boolean	CanBeDeleted;

	@XStreamAlias("size")
	public long		Size;

	@XStreamAlias("creation_time")
	public long		CreationTime;
}
