package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("parental_lock")
public class ParentalLock extends Serializer
{
	@XStreamAlias("client_id")
	public String	ClientId;

	@XStreamAlias("code")
	public String	LockCode;

	@XStreamAlias("is_enable")
	public boolean	IsEnable;

	public ParentalLock() {		
	}
	
	public ParentalLock(String client_id) {
		IsEnable = true;
	}
}
