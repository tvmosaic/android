package com.dvblogic.dvblink_common;

import android.os.Bundle;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class StartScanConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(StartScanRequest.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
        StartScanRequest request = (StartScanRequest) value;

        Bundle parameters = request.params == null ? new Bundle() : (Bundle)request.params.clone();
        parameters.putString(StartScanRequest.deviceKey, request.deviceID);

        for (String key : parameters.keySet()) {
            writer.startNode(key);
            writer.setValue(parameters.getString(key, ""));
            writer.endNode();
        }
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        return null;
    }
}
