package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("get_epg")
public class EpgResponse extends Serializer
{
	@XStreamImplicit(itemFieldName = "cnannel")
	private ArrayList<EpgProgramList>	_list;

	public EpgResponse() {
		_list = new ArrayList<EpgProgramList>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<EpgProgramList>();
		}
		return this;
	}

	public ArrayList<EpgProgramList> list() {

		return _list;
	}

	public void add(EpgProgramList ch) {
		if (_list == null) {
			_list = new ArrayList<EpgProgramList>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public EpgProgramList get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
