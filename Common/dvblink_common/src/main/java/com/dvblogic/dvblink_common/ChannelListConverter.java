package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ChannelListConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(ChannelsList.class);
    }

    protected void createNodeWithValue(HierarchicalStreamWriter writer, String nodeName, Object value) {
        writer.startNode(nodeName);
        try {
            writer.setValue(value.toString());
        } catch(Exception e) {}
        writer.endNode();
    }

    public void marshal(Object value, HierarchicalStreamWriter writer,
                        MarshallingContext context) {
        ChannelsList chList = (ChannelsList)value;

        for (int i=0; i<chList.size(); i++) {
            writer.startNode("channel");
            createNodeWithValue(writer, "channel_id", chList.get(i).ID);
            createNodeWithValue(writer, "channel_dvblink_id", chList.get(i).DVBLinkID);
            createNodeWithValue(writer, "channel_name", chList.get(i).Name);
            createNodeWithValue(writer, "channel_logo", chList.get(i).ChannelLogo);
            if (chList.get(i).Number > 0)
                createNodeWithValue(writer, "channel_number", chList.get(i).Number);
            if (chList.get(i).Number > 0)
                createNodeWithValue(writer, "channel_subnumber", chList.get(i).Subnumber);
            createNodeWithValue(writer, "channel_type", chList.get(i).Type);
            createNodeWithValue(writer, "channel_encrypted", chList.get(i).Encrypted);
            writer.endNode();
        }
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        ChannelsList chList = new ChannelsList();

        while (reader.hasMoreChildren()) {
            reader.moveDown();

            Channel ch = new Channel();

            while (reader.hasMoreChildren()) {
                reader.moveDown();

                String nodeName = reader.getNodeName();

                if (nodeName.equals("channel_id"))
                    ch.ID = reader.getValue();
                else if (nodeName.equals("channel_dvblink_id"))
                    ch.DVBLinkID = reader.getValue();
                else if (nodeName.equals("channel_name"))
                    ch.Name = reader.getValue();
                else if (nodeName.equals("channel_logo"))
                    ch.ChannelLogo = reader.getValue();
                else if (nodeName.equals("channel_number")) {
                    try {
                        ch.Number = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        ch.Number = -1;
                    }
                }
                else if (nodeName.equals("channel_subnumber")) {
                    try {
                        ch.Subnumber = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        ch.Subnumber = -1;
                    }
                }
                else if (nodeName.equals("channel_type")) {
                    try {
                        ch.Type = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        ch.Type = ChannelType.RD_CHANNEL_TV;
                    }
                }
                else if (nodeName.equals("channel_encrypted")) {
                    try {
                        ch.Encrypted = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        ch.Encrypted = ChannelType.RD_CHANNEL_FTA;
                    }
                }

                reader.moveUp();
            }

            chList.add(ch);

            reader.moveUp();
        }
        return chList;
    }
}
