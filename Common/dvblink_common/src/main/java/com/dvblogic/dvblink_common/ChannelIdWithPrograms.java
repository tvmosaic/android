package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("channel_epg")
public class ChannelIdWithPrograms extends Serializer
{
	@XStreamAlias("channel_id")
	public String		ChannelId;

	@XStreamAlias("dvblink_epg")
	public ProgramsList	Programs;
}
