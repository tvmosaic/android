package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("video_info")
public class VideoInfo extends Serializer
{
    public VideoInfo() {
        super();

        Duration = 0;
        Year = 0;
        EpisodeNum = 0;
        SeasonNum = 0;
        StarsNum = 0;
        StarsMaxNum = 0;
        IsHdtv = false;
        IsPremiere = false;
        IsRepeat = false;
        IsAction = false;
        IsComedy = false;
        IsDocumentary = false;
        IsDrama = false;
        IsEducational = false;
        IsHorror = false;
        IsKids = false;
        IsMovie = false;
        IsMusic = false;
        IsNews = false;
        IsReality = false;
        IsRomance = false;
        IsScifi = false;
        IsSerial = false;
        IsSoap = false;
        IsSpecial = false;
        IsSports = false;
        IsThriller = false;
        IsAdult = false;
    }

	@XStreamAlias("name")
	public String	Title;

	@XStreamAlias("short_desc")
	public String	ShortDesc;

	@XStreamAlias("start_time")
	public long		StartTime;

	@XStreamAlias("duration")
	public int		Duration;

	@XStreamAlias("subname")
	public String	Subname;

	@XStreamAlias("language")
	public String	Language;

	@XStreamAlias("actors")
	public String	Actors;

	@XStreamAlias("directors")
	public String	Directors;

	@XStreamAlias("writers")
	public String	Writers;

	@XStreamAlias("producers")
	public String	Producers;

	@XStreamAlias("guests")
	public String	Guests;

	@XStreamAlias("categories")
	public String	Keywords;

	@XStreamAlias("image")
	public String	Image;

	@XStreamAlias("year")
	public int		Year;

	@XStreamAlias("episode_num")
	public int		EpisodeNum;

	@XStreamAlias("season_num")
	public int		SeasonNum;

	@XStreamAlias("stars_num")
	public int		StarsNum;

	@XStreamAlias("starsmax_num")
	public int		StarsMaxNum;

	@XStreamAlias("hdtv")
	public boolean	IsHdtv;

	@XStreamAlias("premiere")
	public boolean	IsPremiere;

	@XStreamAlias("repeat")
	public boolean	IsRepeat;

	@XStreamAlias("cat_action")
	public boolean	IsAction;

	@XStreamAlias("cat_comedy")
	public boolean	IsComedy;

	@XStreamAlias("cat_documentary")
	public boolean	IsDocumentary;

	@XStreamAlias("cat_drama")
	public boolean	IsDrama;

	@XStreamAlias("cat_educational")
	public boolean	IsEducational;

	@XStreamAlias("cat_horror")
	public boolean	IsHorror;

	@XStreamAlias("cat_kids")
	public boolean	IsKids;

	@XStreamAlias("cat_movie")
	public boolean	IsMovie;

	@XStreamAlias("cat_music")
	public boolean	IsMusic;

	@XStreamAlias("cat_news")
	public boolean	IsNews;

	@XStreamAlias("cat_reality")
	public boolean	IsReality;

	@XStreamAlias("cat_romance")
	public boolean	IsRomance;

	@XStreamAlias("cat_scifi")
	public boolean	IsScifi;

	@XStreamAlias("cat_serial")
	public boolean	IsSerial;

	@XStreamAlias("cat_soap")
	public boolean	IsSoap;

	@XStreamAlias("cat_special")
	public boolean	IsSpecial;

	@XStreamAlias("cat_sports")
	public boolean	IsSports;

	@XStreamAlias("cat_thriller")
	public boolean	IsThriller;

	@XStreamAlias("cat_adult")
	public boolean	IsAdult;
}
