package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;

import java.util.ArrayList;

@XStreamConverter(UpdaterGetStatusResponseConverter.class)
@XStreamAlias("updater_status")
public class UpdaterGetStatusResponse extends Serializer {
	public enum Status {
		UP_TO_DATE,
		NEEDS_UPDATE,
		UPDATE_IN_PROGRESS
	}

	static public class Component {
		public String name;
		public String changes;
		public String local;
		public String remote;

		public static final String UNKNOWN = "0";
	}

	@XStreamAlias("status")
	public String status;

	public ArrayList <Component> components = new ArrayList<>();

	public Status getStatus() {
		try {
			return Status.valueOf(this.status.toUpperCase());
		} catch (IllegalArgumentException exception) {
			return Status.UP_TO_DATE;
		}
	}
}
