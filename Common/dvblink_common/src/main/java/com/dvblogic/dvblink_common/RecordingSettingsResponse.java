package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("recording_settings")
public class RecordingSettingsResponse extends Serializer
{
	@XStreamAlias("recording_path")
	public String	RecordingPath;

	@XStreamAlias("before_margin")
	public int BeforeMargin;

	@XStreamAlias("after_margin")
	public int AfterMargin;

	@XStreamAlias("total_space")
	public long TotalSpace; // KB

	@XStreamAlias("avail_space")
	public long	AvailSpace; // KB
	
	@XStreamAlias("check_deleted")
	public boolean	CheckDeleted; 

	@XStreamAlias("ds_auto_mode")
	public boolean	DsAutoMode; 

	@XStreamAlias("ds_man_value")
	public long	DsManValue; 

	@XStreamAlias("auto_delete")
	public boolean	AutoDelete;

	@XStreamAlias("new_only_algo_type")
	public int	NewOnlyAlgoType;

	@XStreamAlias("filename_pattern")
	public String	filenamePattern;

}
