package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamConverter(FavoritesListConverter.class)
@XStreamAlias("favorites")
public class FavoritesList extends Serializer
{
	@XStreamImplicit(itemFieldName = "favorite")
	private ArrayList<Favorite>	_list;

	public FavoritesList() {
		_list = new ArrayList<Favorite>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Favorite>();
		}
		return this;
	}

	public ArrayList<Favorite> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(Favorite ch) {
		if (_list == null)
			_list = new ArrayList<Favorite>();
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Favorite get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
