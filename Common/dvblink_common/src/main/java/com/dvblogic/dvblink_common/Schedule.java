package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("schedule")
public class Schedule extends Serializer
{
    public static final int schedulePriorityLow = -1;
    public static final int schedulePriorityNormal = 0;
    public static final int schedulePriorityHigh = 1;

    public static final int startMarginAnyTime = -1;
    public static final int dayMaskAnyDay = 0;

	@XStreamAlias("schedule_id")
	public String			ScheduleID;

	@XStreamAlias("user_param")
	public String			UserParam;

	@XStreamAlias("force_add")
	public boolean			IsForceAdd;
	
	@XStreamAlias("margine_before")
	public long  MargineBefore = -1;

	@XStreamAlias("margine_after")
	public long  MargineAfter = -1;

    @XStreamAlias("targets")
    public SendToTargetIDLIst Targets;

	@XStreamAlias("by_epg")
	public ByEpgSchedule	ByEpg;

	@XStreamAlias("manual")
	public ManualSchedule	Manual;

	@XStreamAlias("by_pattern")
	public ByPatternSchedule ByPattern;

    @XStreamAlias("active")
    public boolean Active = true;

    @XStreamAlias("priority")
    public Integer Priority = schedulePriorityNormal;

	public Schedule() {
	}

	public Schedule(ByEpgSchedule by_epg) {
		this(by_epg, null, false);
	}

	public Schedule(ByEpgSchedule by_epg, String user_param) {
		this(by_epg, user_param, false);
	}

	public Schedule(ByEpgSchedule by_epg, String user_param, boolean is_force_add) {
		ByEpg = by_epg;
		UserParam = user_param;
		IsForceAdd = is_force_add;
	}

	public Schedule(ManualSchedule manual) {
		this(manual, null, false);
	}

	public Schedule(ManualSchedule manual, String user_param) {
		this(manual, user_param, false);
	}

	public Schedule(ManualSchedule manual, String user_param, boolean is_force_add) {
		Manual = manual;
		UserParam = user_param;
		IsForceAdd = is_force_add;
	}

    public Schedule(ByPatternSchedule by_pattern) {
        this(by_pattern, null, false);
    }

    public Schedule(ByPatternSchedule by_pattern, String user_param) {
        this(by_pattern, user_param, false);
    }

    public Schedule(ByPatternSchedule by_pattern, String user_param, boolean is_force_add) {
        ByPattern = by_pattern;
        UserParam = user_param;
        IsForceAdd = is_force_add;
    }

    public boolean isV2Schedule() {
        return Priority != null;
    }

    public boolean isEpg() {
		return ByEpg != null && !ByEpg.ChannelID.isEmpty();
	}
	
	public boolean isManual() {
		return Manual != null && !Manual.ChannelID.isEmpty();
	}
	
	public boolean isPatern() {
		return ByPattern != null && (!isEpg() && !isManual());
	}

   public int recordingsToKeep() {
       if (isEpg())
           return ByEpg.RecordingsToKeep;
       else if (isManual())
           return Manual.RecordingsToKeep;
       else if (isPatern())
           return ByPattern.RecordingsToKeep;
       return 0;
   }

    public void setRecordingsToKeep(int num) {
        if (isEpg())
            ByEpg.RecordingsToKeep = num;
        else if (isManual())
            Manual.RecordingsToKeep = num;
        else if (isPatern())
            ByPattern.RecordingsToKeep = num;
    }

	public String channelID() {
		if (isEpg())
			return ByEpg.ChannelID;
		else if (isManual())
			return Manual.ChannelID;
		else if (isPatern())
			return ByPattern.ChannelID;
		return "";
	}

	public boolean isRepeat() {
		if (isEpg())
			return ByEpg.IsRepeat;
		else if (isManual())
			return (Manual.DayMask != 0);
		else if (isPatern())
			return true; //repeating by its nature
		return false;
	}

    public int getDayMask() {
        if (isEpg())
            return ByEpg.DayMask;
        else if (isManual())
            return Manual.DayMask;
        else if (isPatern())
            return ByPattern.DayMask;
        return dayMaskAnyDay;
    }

    public void setDayMask(int daymask) {
        if (isEpg())
            ByEpg.DayMask = daymask;
        else if (isManual())
            Manual.DayMask = daymask;
        else if (isPatern())
            ByPattern.DayMask = daymask;
    }

    public int getStartBefore() {
        if (isEpg())
            return ByEpg.StartBefore;
        else if (isPatern())
            return ByPattern.StartBefore;
        return startMarginAnyTime;
    }

    public void setStartBefore(int t) {
        if (isEpg())
            ByEpg.StartBefore = t;
        else if (isPatern())
            ByPattern.StartBefore = t;
    }

    public int getStartAfter() {
        if (isEpg())
            return ByEpg.StartAfter;
        else if (isPatern())
            return ByPattern.StartAfter;
        return startMarginAnyTime;
    }

    public void setStartAfter(int t) {
        if (isEpg())
            ByEpg.StartAfter = t;
        else if (isPatern())
            ByPattern.StartAfter = t;
    }

}
