package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("settings")
public class RescanSettings extends Serializer {
	
	@XStreamAlias("container")
    public ParamContainer settings;
}


