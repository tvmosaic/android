package com.dvblogic.dvblink_common;

public class EditableParam
{
	public static final int	EDITABLE_FORMAT_STRING		= 0;
	public static final int	EDITABLE_FORMAT_NUMBER	= 1;

	public String key;
	public String name;
	public String value;
	public int format;
}
