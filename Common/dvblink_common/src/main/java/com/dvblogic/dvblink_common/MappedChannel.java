package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("channel")
public class MappedChannel extends Serializer
{
	@XStreamAlias("dvblink_channel_id")
	public String		DVBLinkID;

	@XStreamAlias("channel_id")
	public String	RemoteChannelId;


}
