package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamAlias("services")
public class ServiceInfoList extends Serializer
{
	@XStreamImplicit(itemFieldName = "ServiceInfo")
	private ArrayList<ServiceInfo>	_list;

	public ServiceInfoList() {
		_list = new ArrayList<ServiceInfo>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<ServiceInfo>();
		}
		return this;
	}

	public ArrayList<ServiceInfo> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}

	public void add(ServiceInfo ch) {
		if (_list == null) {
			_list = new ArrayList<ServiceInfo>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public ServiceInfo get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
