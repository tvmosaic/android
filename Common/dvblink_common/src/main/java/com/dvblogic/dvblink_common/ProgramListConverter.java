package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

public class ProgramListConverter implements Converter {
    public boolean canConvert(Class clazz) {
        return clazz.equals(ProgramsList.class);
    }

    public void marshal(Object value, HierarchicalStreamWriter writer, MarshallingContext context) {
    }

    public Object unmarshal(HierarchicalStreamReader reader,
                            UnmarshallingContext context) {
        ProgramsList prgList = new ProgramsList();

        while (reader.hasMoreChildren()) {
            reader.moveDown();

            Program prg = new Program();

            while (reader.hasMoreChildren()) {
                reader.moveDown();

                String nodeName = reader.getNodeName();

                if (nodeName.equals("program_id"))
                    prg.ID = reader.getValue();
                else if (nodeName.equals("is_series")) {
                    try {
                        prg.IsSeries = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsSeries = false;
                    }
                }
                else if (nodeName.equals("is_record")) {
                    try {
                        prg.IsRecord = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsRecord = false;
                    }
                }
                else if (nodeName.equals("is_repeat_record")) {
                    try {
                        prg.IsRepeatRecord = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsRepeatRecord = false;
                    }
                }
                else if (nodeName.equals("is_record_conflict")) {
                    try {
                        prg.IsRecordConflict = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsAdult = false;
                    }
                }
                else if (nodeName.equals("name"))
                    prg.Title = reader.getValue();
                else if (nodeName.equals("short_desc"))
                    prg.ShortDesc = reader.getValue();
                else if (nodeName.equals("start_time"))
                    try {
                        prg.StartTime = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.StartTime = 0;
                    }
                else if (nodeName.equals("duration")) {
                    try {
                        prg.Duration = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.Duration = 0;
                    }
                }
                else if (nodeName.equals("subname")) {
                    prg.Subname = reader.getValue();
                }
                else if (nodeName.equals("language")) {
                    prg.Language = reader.getValue();
                }
                else if (nodeName.equals("actors")) {
                    prg.Actors = reader.getValue();
                }
                else if (nodeName.equals("directors")) {
                    prg.Directors = reader.getValue();
                }
                else if (nodeName.equals("writers")) {
                    prg.Writers = reader.getValue();
                }
                else if (nodeName.equals("producers")) {
                    prg.Producers = reader.getValue();
                }
                else if (nodeName.equals("guests")) {
                    prg.Guests = reader.getValue();
                }
                else if (nodeName.equals("categories")) {
                    prg.Keywords = reader.getValue();
                }
                else if (nodeName.equals("image")) {
                    prg.Image = reader.getValue();
                }
                else if (nodeName.equals("year")) {
                    try {
                        prg.Year = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.Year = 0;
                    }
                }
                else if (nodeName.equals("episode_num")) {
                    try {
                        prg.EpisodeNum = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.EpisodeNum = 0;
                    }
                }
                else if (nodeName.equals("season_num")) {
                    try {
                        prg.SeasonNum = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.SeasonNum = 0;
                    }
                }
                else if (nodeName.equals("stars_num")) {
                    try {
                        prg.StarsNum = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.StarsNum = 0;
                    }
                }
                else if (nodeName.equals("starsmax_num")) {
                    try {
                        prg.StarsMaxNum = Integer.parseInt(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.StarsMaxNum = 0;
                    }
                }
                else if (nodeName.equals("hdtv")) {
                    try {
                        prg.IsHdtv = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsHdtv = false;
                    }
                }
                else if (nodeName.equals("premiere")) {
                    try {
                        prg.IsPremiere = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsPremiere = false;
                    }
                }
                else if (nodeName.equals("repeat")) {
                    try {
                        prg.IsRepeat = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsRepeat = false;
                    }
                }
                else if (nodeName.equals("cat_action")) {
                    try {
                        prg.IsAction = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsAction = false;
                    }
                }
                else if (nodeName.equals("cat_comedy")) {
                    try {
                        prg.IsComedy = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsComedy = false;
                    }
                }
                else if (nodeName.equals("cat_documentary")) {
                    try {
                        prg.IsDocumentary = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsDocumentary = false;
                    }
                }
                else if (nodeName.equals("cat_drama")) {
                    try {
                        prg.IsDrama = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsDrama = false;
                    }
                }
                else if (nodeName.equals("cat_educational")) {
                    try {
                        prg.IsEducational = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsEducational = false;
                    }
                }
                else if (nodeName.equals("cat_horror")) {
                    try {
                        prg.IsHorror = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsHorror = false;
                    }
                }
                else if (nodeName.equals("cat_kids")) {
                    try {
                        prg.IsKids = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsKids = false;
                    }
                }
                else if (nodeName.equals("cat_movie")) {
                    try {
                        prg.IsMovie = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsMovie = false;
                    }
                }
                else if (nodeName.equals("cat_music")) {
                    try {
                        prg.IsMusic = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsMusic = false;
                    }
                }
                else if (nodeName.equals("cat_news")) {
                    try {
                        prg.IsNews = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsNews = false;
                    }
                }
                else if (nodeName.equals("cat_reality")) {
                    try {
                        prg.IsReality = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsReality = false;
                    }
                }
                else if (nodeName.equals("cat_romance")) {
                    try {
                        prg.IsRomance = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsRomance = false;
                    }
                }
                else if (nodeName.equals("cat_scifi")) {
                    try {
                        prg.IsScifi = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsScifi = false;
                    }
                }
                else if (nodeName.equals("cat_serial")) {
                    try {
                        prg.IsSerial = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsSerial = false;
                    }
                }
                else if (nodeName.equals("cat_soap")) {
                    try {
                        prg.IsSoap = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsSoap = false;
                    }
                }
                else if (nodeName.equals("cat_special")) {
                    try {
                        prg.IsSpecial = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsSpecial = false;
                    }
                }
                else if (nodeName.equals("cat_sports")) {
                    try {
                        prg.IsSports = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsSports = false;
                    }
                }
                else if (nodeName.equals("cat_thriller")) {
                    try {
                        prg.IsThriller = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsThriller = false;
                    }
                }
                else if (nodeName.equals("cat_adult")) {
                    try {
                        prg.IsAdult = Boolean.parseBoolean(reader.getValue());
                    } catch (NumberFormatException e) {
                        prg.IsAdult = false;
                    }
                }

                reader.moveUp();
            }

            prgList.add(prg);

            reader.moveUp();
        }
        return prgList;
    }
}
