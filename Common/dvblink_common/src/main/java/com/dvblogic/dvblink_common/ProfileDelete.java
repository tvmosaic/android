package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("delete_profile")
public class ProfileDelete extends Serializer
{
	@XStreamAlias("profile_id")
	public String ProfileId;
}
