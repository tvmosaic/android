package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("server_settings")
public class ServerSettings extends Serializer
{
	public static final String				USER_NAME_DEFAULT				= "";
	public static final String				PASSWORD_DEFAULT				= "";
	public static final String				SERVER_ADDRESS_DEFAULT			= "";
	public static final String				SERVER_ID_DEFAULT				= "";
	public static final String				SERVER_NAME_DEFAULT				= "";
    public static final boolean				TRANSCODING_SUPPORTED_DEFAULT	= false;
    public static final int				    SERVER_PORT_DEFAULT				= 0;
	public static final boolean				USE_HTTPS_DEFAULT	= false;

	@XStreamAlias("server_name")
	public String	serverName;

	@XStreamAlias("server_address")
	public String	serverAddress;

	@XStreamAlias("server_id")
	public String	serverId;

	@XStreamAlias("base_streaming_port")
	public int		serverPort;

	@XStreamAlias("user_name")
	public String	userName;

	@XStreamAlias("password")
	public String	password;

	@XStreamAlias("transcoding_supported")
	public boolean	transcodingSupported;

	@XStreamAlias("use_https")
	public boolean	useHttps;

	public ServerSettings() {
		serverName = SERVER_NAME_DEFAULT;
		serverId = SERVER_ID_DEFAULT;
		serverAddress = SERVER_ADDRESS_DEFAULT;
		serverPort = SERVER_PORT_DEFAULT;
		userName = USER_NAME_DEFAULT;
		password = PASSWORD_DEFAULT;
		transcodingSupported = TRANSCODING_SUPPORTED_DEFAULT;
		useHttps = USE_HTTPS_DEFAULT;
	}

}
