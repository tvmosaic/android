package com.dvblogic.dvblink_common;

import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("remove_recording")
public class RecordingRemover extends Serializer
{
	@XStreamAlias("recording_id")
	public String	RecordingID;

	public RecordingRemover() {
	}
	
	public RecordingRemover(String recording_id) {
		RecordingID = recording_id;
	}
}
