package com.dvblogic.dvblink_common;


import com.thoughtworks.xstream.annotations.XStreamAlias;

@XStreamAlias("profile")
public class Profile extends Serializer
{
	@XStreamAlias("profile_id")
	public String ProfileId;

	@XStreamAlias("profile_name")
	public String ProfileName;
	
	public Profile() {
	}

	public Profile(String id, String name) {
		ProfileId = id;
		ProfileName = name;
	}
}
