package com.dvblogic.dvblink_common;

import java.util.ArrayList;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamConverter;
import com.thoughtworks.xstream.annotations.XStreamImplicit;

@XStreamConverter(PlaybackItemListConverter.class)
@XStreamAlias("items")
public class PlaybackItemList extends Serializer
{
	private ArrayList<Item>	_list;

	public PlaybackItemList() {
		_list = new ArrayList<Item>();
	}

	private Object readResolve() {
		if (_list == null) {
			_list = new ArrayList<Item>();
		}
		return this;
	}

	public ArrayList<Item> list() {
		if (_list == null)
			_list = new ArrayList<>();

		return _list;
	}


	public void add(Item ch) {
		if (_list == null) {
			_list = new ArrayList<Item>();
		}
		_list.add(ch);
	}

	public int size() {
		return (_list == null ? 0 : _list.size());
	}

	public Item get(int idx) {
		return (_list == null ? null : _list.get(idx));
	}

	public void remove(int idx) {
		if (_list != null)
			_list.remove(idx);
	}

	public void clear() {
		if (_list != null)
			_list.clear();
	}
}
