package com.dvblogic.tvmosaic;

import android.content.Context;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.widget.SeekBar;

public class CustomSeekBar extends SeekBar {

	private int keyCode;
	private boolean shortPress = false;

	private View.OnKeyListener shortKeyPress;
	private View.OnKeyListener longKeyPress;

	public CustomSeekBar(Context context) {
		super(context);
	}

	public CustomSeekBar(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public CustomSeekBar(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	@Override
	public boolean onKeyLongPress(int keyCode, KeyEvent event) {
		if (keyCode == this.keyCode) {
			shortPress = false;
			if (longKeyPress != null) {
				longKeyPress.onKey(this, keyCode, event);
			}
			return true;
		}
		return super.onKeyLongPress(keyCode, event);
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() == KeyEvent.ACTION_DOWN) {
			event.startTracking();
			if (event.getRepeatCount() == 0){
				shortPress = true;
				this.keyCode = keyCode;
			}
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

	@Override
	public boolean onKeyUp(int keyCode, KeyEvent event) {
		if (keyCode == this.keyCode) {
			if (shortPress) {
				if (shortKeyPress != null) {
					shortKeyPress.onKey(this, keyCode, event);
				}
			}
			shortPress = false;
			return true;
		}
		return super.onKeyUp(keyCode, event);
	}

	public void setOnShortPress(OnKeyListener onKeyListener) {
		this.shortKeyPress = onKeyListener;
	}

	public void setOnLongPress(OnKeyListener onKeyListener) {
		this.longKeyPress = onKeyListener;
	}
}
