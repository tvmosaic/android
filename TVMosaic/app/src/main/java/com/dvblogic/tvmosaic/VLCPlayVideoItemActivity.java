package com.dvblogic.tvmosaic;

import android.content.ComponentName;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.dvblogic.dvblink_common.*;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.*;

import TVMosaic.TVMosaic.R;

public class VLCPlayVideoItemActivity extends BaseActivity implements IVLCVout.OnNewVideoLayoutListener
{
    private static final String TAG = "Player";
    private static final long HIDE_PANEL_DELAY = 5000;
    private static final int INFO_PANEL_ALPHA_ENABLED = 255;
    private static final int INFO_PANEL_ALPHA_DISABLED = 50;
    private static final int REWIND_DURATION_SECONDS = -15;
    private static final int FORWARD_DURATION_SECONDS = 30;
    private static final int TIME_SHIFT_STATS_TIMER_INTERVAL = 500;
    private static final int SEEK_MAX_VALUE = 1000;
    private static final int MOVE_THROTTLE_TIME = 650;

    private class ItemAdapter extends BaseAdapter {
        boolean _audio_adapter = true;

        private ItemAdapter(boolean audio_adapter) {
            super();
            _audio_adapter = audio_adapter;
        }

        @SuppressWarnings("unchecked")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            try {
                if (row == null) {
                    row = LinearLayout.inflate(_context, R.layout.player_menu_row, null);
                }
                MediaPlayer.TrackDescription track = (MediaPlayer.TrackDescription) getItem(position);
                if (track == null)
                    return row;

                String text = track.name;
                TextView label = (TextView) row.findViewById(R.id.vlc_menu_row_text);
                label.setText(text);

                ImageView icon = (ImageView) row.findViewById(R.id.vlc_menu_row_icon);
                if (_audio_adapter) {
                    icon.setImageResource(
                            selected_audio == position ?
                                    R.drawable.tvm_checkbox_checked_icon_active :
                                    R.drawable.tvm_checkbox_unchecked_icon_active
                    );
                } else {
                    icon.setImageResource(
                            selected_sub == position ?
                                    R.drawable.tvm_checkbox_checked_icon_active :
                                    R.drawable.tvm_checkbox_unchecked_icon_active
                    );
                }
            } catch (Exception e) {
                Log.e(TAG, "unknown", e);
            }
            return row;
        }

        @Override
        public int getCount() {
            if (_audio_adapter)
                return (mAudioTracks == null ? 0 : mAudioTracks.length);
            else
                return (mSubTracks == null ? 0 : mSubTracks.length);
        }

        @Override
        public Object getItem(int position) {
            if (_audio_adapter)
                return (mAudioTracks == null ? null : mAudioTracks[position]);
            else
                return (mSubTracks == null ? null : mSubTracks[position]);
        }

        @SuppressWarnings("unchecked")
        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    private class HidePanelTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hidePanels();
                }
            });

        }
    }
    private static int stream_invalid_idx = -1;
    private static int stream_disabled_id = -1;

    private final int state_ready = 0;
    private final int state_seek = 1;

    private boolean is_seeking_ = false;
    private MediaPlayer mMediaPlayer = null;
    private Media mMedia = null;
    private LibVLC libvlc = null;
    private SurfaceView mSurfaceView = null;
    private SurfaceView mSurfaceSubtitles = null;
    private FrameLayout mVideoSurfaceFrame = null;
    private View.OnLayoutChangeListener mOnLayoutChangeListener = null;
    private final Handler mHandler = new Handler();
    private MediaPlayer.TrackDescription[] mAudioTracks = null;
    private MediaPlayer.TrackDescription[] mSubTracks = null;
    private int selected_audio = stream_invalid_idx;
    private int selected_sub = stream_invalid_idx;

    private Boolean _started = false;
    private int state_ = state_ready;
    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener;
    private ComponentName mediaButtonReceiver;
    private AudioManager audioManager;

    private CustomTextView programName;
    private CustomTextView programTime;
    private Timer hideInfoPanelTimer;
    private LinearLayout infoPanel;
    private TextView currentTime;
    private CustomSeekBar programSeek;

    private GridView subtitleList;
    private GridView audioTrackList;
    private LinearLayout paramPanel;

    private boolean infoPanelShowing = false;
    private boolean infoPanelHiding = false;

    private boolean paramPanelShowing = false;
    private boolean paramPanelHiding = false;

    private int mVideoHeight = 0;
    private int mVideoWidth = 0;
    private int mVideoVisibleHeight = 0;
    private int mVideoVisibleWidth = 0;
    private int mVideoSarNum = 0;
    private int mVideoSarDen = 0;

    private VideoItem video_item_ = null;
    private Timer getStatsTimer;
    private int seek_position_ = -1;
    private int new_resume_position_ = 0;

    private Timer moveThrottleTimer;

    private Queue<Pair<Integer, Object>> queue = new LinkedList<>();
    private boolean ignoreWindowFocusChange = false;
    private Toast toast;

    private void startHideInfoPanelTimer() {

        if (!Utils.usesRemoteControl(this))
            return;

        stopHideInfoPanelTimer();
        hideInfoPanelTimer = new Timer();
        hideInfoPanelTimer.schedule(
                new HidePanelTimerTask(), HIDE_PANEL_DELAY
        );
    }
    private void stopHideInfoPanelTimer() {

        if (!Utils.usesRemoteControl(this))
            return;

        if (hideInfoPanelTimer != null) {
            hideInfoPanelTimer.cancel();
        }
    }

    private void showInfoPanel() {
        if (!infoPanelHidden() || infoPanelShowing) {
            return;
        }
        infoPanelShowing = true;
        startHideInfoPanelTimer();
        showCurrentProgram();
        UpdatePlayStats();
        startStatsTimer();

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_in_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                infoPanel.setVisibility(View.VISIBLE);
                infoPanelShowing = false;
                programSeek.requestFocus();
            }
        });
        infoPanel.startAnimation(animation);
    }

    private void hideInfoPanel() {
        if (infoPanelHidden() || infoPanelHiding) {
            return;
        }
        infoPanelHiding = true;
        stopHideInfoPanelTimer();
        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_out_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                infoPanel.setVisibility(View.INVISIBLE);
                infoPanelHiding = false;
            }
        });
        infoPanel.startAnimation(animation);
        stopStatsTimer();
    }

    private boolean infoPanelHidden() {
        return infoPanel.getVisibility() == View.INVISIBLE;
    }

    private boolean paramPanelHidden() {
        return paramPanel.getVisibility() != View.VISIBLE;
    }

    private void showParamPanel() {

        if (!paramPanelHidden() || paramPanelShowing) {
            return;
        }
        paramPanelShowing = true;

        stopHideInfoPanelTimer();
        paramPanel.setVisibility(View.VISIBLE);

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_in_top);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                paramPanelShowing = false;
                paramPanel.requestFocus();
            }
        });
        paramPanel.startAnimation(animation);
    }

    private void hideParamPanel() {
        if (paramPanelHidden() || paramPanelHiding) {
            return;
        }
        paramPanelHiding = true;

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_out_top);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                paramPanel.setVisibility(View.GONE);
                paramPanelHiding = false;
            }
        });
        paramPanel.startAnimation(animation);
    }

    private void enableInfoPanel(boolean enabled) {

        int alphaCurrentTimeElement = enabled ? INFO_PANEL_ALPHA_ENABLED : INFO_PANEL_ALPHA_DISABLED;
        currentTime.setTextColor(currentTime.getTextColors().withAlpha(alphaCurrentTimeElement));
        programName.setTextColor(programName.getTextColors().withAlpha(alphaCurrentTimeElement));
        programTime.setTextColor(programTime.getTextColors().withAlpha(alphaCurrentTimeElement));
    }

    private void showCurrentProgram() {
        String str = video_item_.VideoInfo.Title;
        if (video_item_.VideoInfo.Subname != null && !video_item_.VideoInfo.Subname.isEmpty()) {
            str += " - " + video_item_.VideoInfo.Subname;
        }
        programName.setText(str);
    }

    String getTime(long time_sec) {
        String str;
        long h = time_sec / 3600;
        if (h == 0) {
            long m = time_sec / 60;
            long s = (time_sec - m*60) % 60;
            str = String.format("%d:%02d", m, s);
        } else {
            long m = (time_sec - h*3600) / 60;
            long s = (time_sec - h*3600) % 60;
            str = String.format("%d:%02d:%02d", h, m, s);
        }
        return str;
    }

    private void updateCurrentPlaybackPosition(int seconds) {
        currentTime.setText(getTime(seconds));
    }

    private void prepareInputParams() {
        try {
            String xml_video_item = getIntent().getStringExtra("video_item");
            video_item_ = (VideoItem) Serializer.deserialize(xml_video_item, VideoItem.class);
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    private void preparePlayerViews() {
        mVideoSurfaceFrame = (FrameLayout) findViewById(R.id.video_frame);

        mSurfaceView = (SurfaceView) findViewById(R.id.surface_view);
        mSurfaceSubtitles = (SurfaceView) findViewById(R.id.surface_subtitles_view);

        mSurfaceSubtitles.setZOrderMediaOverlay(true);
        mSurfaceSubtitles.getHolder().setFormat(PixelFormat.TRANSLUCENT);

        mSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    boolean paramAreaTouch = event.getY() < (v.getHeight() / 2);
                    boolean infoAreaTouch = event.getY() > (v.getHeight() / 2);

                    if (paramAreaTouch) {
                        if (!infoPanelHidden()) {
                            hideInfoPanel();
                        }
                        if (paramPanelHidden() && infoPanelHidden()) {
                            showParamPanel();
                        }
                    }

                    if (infoAreaTouch) {
                        if (!paramPanelHidden()) {
                            hideParamPanel();
                        }
                        if (infoPanelHidden() && paramPanelHidden()) {
                            showInfoPanel();
                        }
                    }
                }
                return true;
            }
        });
    }

    private void prepareInfoPanel() {
        LayoutInflater inflater = getLayoutInflater();

        programName = (CustomTextView) findViewById(R.id.program_name);
        programTime = (CustomTextView) findViewById(R.id.program_time);

        programSeek.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                enableInfoPanel(focus);
                if (focus) {
                    hideParamPanel();

                    startHideInfoPanelTimer();
                }
            }
        });

        infoPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!paramPanelHidden()) {
                    hideParamPanel();
                    infoPanel.requestFocus();
                }
            }
        });
    }

    private void setRefreshRate() {
        try {
            float _refresh_rate = settings_.getValue(Settings.REFRESH_RATE_KEY, Settings.REFRESH_RATE_DEFAULT);
            if (_refresh_rate > 0) {
                Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
                WindowManager.LayoutParams params = getWindow().getAttributes();
                if (Build.VERSION.SDK_INT >= 23) {
                    Display.Mode[] modes = display.getSupportedModes();
                    for (int idx = 0; idx < modes.length; idx++) {
                        Display.Mode mode = modes[idx];
                        if (mode.getRefreshRate() == _refresh_rate) {
                            params.preferredDisplayModeId = mode.getModeId();
                            getWindow().setAttributes(params);
                        }
                    }
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    params.preferredRefreshRate = _refresh_rate;
                    getWindow().setAttributes(params);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    protected int get_idx_for_id(MediaPlayer.TrackDescription[] tracks, int id) {
        int ret_val = stream_invalid_idx;

        if (tracks != null) {
            for (int i = 0; i < tracks.length; i++) {
                if (tracks[i].id == id) {
                    ret_val = i;
                    break;
                }
            }
        }

        return ret_val;
    }

    protected int get_first_enabled_track_idx(MediaPlayer.TrackDescription[] tracks) {
        int ret_val = stream_invalid_idx;

        if (tracks != null) {
            for (int i = 0; i < tracks.length; i++) {
                if (tracks[i].id != stream_disabled_id) {
                    ret_val = i;
                    break;
                }
            }
        }

        return ret_val;
    }

    private void fillMenu() {
        mAudioTracks = mMediaPlayer.getAudioTracks();
        if (selected_audio == stream_invalid_idx) {
            selected_audio = get_first_enabled_track_idx(mAudioTracks);
            if (selected_audio != -1)
                mMediaPlayer.setAudioTrack(mAudioTracks[selected_audio].id);
        } else {
            if (mAudioTracks != null)
                mMediaPlayer.setAudioTrack(mAudioTracks[selected_audio].id);
        }

        mSubTracks = mMediaPlayer.getSpuTracks();
        if (selected_sub == stream_invalid_idx) {
            selected_sub = get_idx_for_id(mSubTracks, stream_disabled_id);
            if (selected_sub != -1)
                mMediaPlayer.setSpuTrack(mSubTracks[selected_sub].id);
        } else {
            if (mSubTracks != null)
                mMediaPlayer.setSpuTrack(mSubTracks[selected_sub].id);
        }

        if (mSubTracks != null) {
            subtitleList.setAdapter(new ItemAdapter(false));
            subtitleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_sub = position;
                    mMediaPlayer.setSpuTrack(mSubTracks[position].id);
                    subtitleList.invalidateViews();
                }
            });
        }

        if (mAudioTracks != null) {
            audioTrackList.setAdapter(new ItemAdapter(true));
            audioTrackList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_audio = position;
                    mMediaPlayer.setAudioTrack(mAudioTracks[position].id);
                    audioTrackList.invalidateViews();
                }
            });
            audioTrackList.setSelection(selected_audio);
        }
    }

    private void rewind() {
        move(REWIND_DURATION_SECONDS);
    }

    private void forward() {
        move(FORWARD_DURATION_SECONDS);
    }

    private void move(int duration) {
        stopStatsTimer(); // Останавка обновления стат. информации c сервера (меняет cur_pos_sec).

        if (moveThrottleTimer != null) {
            moveThrottleTimer.cancel();
            moveThrottleTimer = null;
        }

        //set initial position
        if (seek_position_ == -1) {
            seek_position_ = (int) (mMediaPlayer.getTime() / 1000);
        }

        int ll = (int)mMediaPlayer.getLength() / 1000;
        int position = 0;
        position = Math.min(Math.max(seek_position_ + duration, 0), ll);

        seek_position_ = position;
        updateCurrentPlaybackPosition(seek_position_);
        programSeek.setProgress(calcPerN(position, ll, SEEK_MAX_VALUE));

        moveThrottleTimer = new Timer();
        moveThrottleTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        queue.add(new Pair<Integer, Object>(state_seek, seek_position_));
                        seek_position_ = -1;
                        startStatsTimer();
                    }
                });
            }
        }, MOVE_THROTTLE_TIME);
    }

    private int calcPerN(long current, long max, long n) {
        return (int)((float) current / max * n);
    }

    // region VLC Player
    protected void createPlayer() {

        // Create LibVLC
        ArrayList<String> options = new ArrayList<>();
        options.add("-vv"); // verbosity

        int deinterlace = settings_.getValue(Settings.DEINTERLACE_MODE_KEY, Settings.DEINTERLACE_MODE_DEFAULT);

        if (deinterlace == 1) {
            options.add("--video-filter");
            options.add("deinterlace");

//            options.add("--deinterlace-mode");
//            String algo = settings_.getValue("deinterlacing_algo", "linear");
//            options.add(algo);

            options.add("--deinterlace");
            options.add("-1");
        } else if (deinterlace == 0) {
            options.add("--deinterlace");
            options.add("0");
        }

        libvlc = new LibVLC(this, options);

        // Create media player
        mMediaPlayer = new MediaPlayer(libvlc);
        mMediaPlayer.setAudioOutput("android_audiotrack");

        boolean audio_passthrough = settings_.getValue(Settings.AUDIO_PASSTHROUGH_KEY, Settings.AUDIO_PASSTHROUGH_DEFAULT);
        if (audio_passthrough) {
            mMediaPlayer.setAudioOutputDevice("hdmi");
        } else {
            mMediaPlayer.setAudioOutputDevice("stereo");
        }

        // Set up video output
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        vout.setVideoView(mSurfaceView);

        if (mSurfaceSubtitles.getVisibility() != View.GONE)
            vout.setSubtitlesView(mSurfaceSubtitles);

        vout.attachViews(this);
    }

    private void stopPlayback() {
        if (mMediaPlayer != null && mMediaPlayer.getMedia() != null) {

            try {
                mMediaPlayer.setEventListener(null);

                if (mOnLayoutChangeListener != null) {
                    mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
                    mOnLayoutChangeListener = null;
                }
                mMediaPlayer.stop();

                mMediaPlayer.setMedia(null);
            } catch (Exception e) {}
        }
    }

    protected void invalidateTracksInfo() {
        mAudioTracks = null;
        mSubTracks = null;
    }

    private void startPlayback() {
        String url = video_item_.Url;

        startPlayer(url);
    }

    private boolean startPlayer(String url) {

        stopPlayback();

        state_ = state_ready;
        synchronized (_started) {
            _started = false;
        }

        try {

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            long audio_delay = Integer.parseInt(sp.getString("audio_delay", "0")) * 1000; //in microseconds
            boolean passtrough = sp.getBoolean(Settings.AUDIO_PASSTHROUGH_KEY, Settings.AUDIO_PASSTHROUGH_DEFAULT);

            Uri uri = Uri.parse(url);
            mMedia = new Media(libvlc, uri);
            mMedia.setHWDecoderEnabled(true, true);
            mMedia.addOption(":network-caching=1000");

            int resume_position = getIntent().getIntExtra("resume_position", 0);
            if (resume_position > 0) {
                String start_time_option = String.format(":start-time=%d", resume_position);
                mMedia.addOption(start_time_option);
            }

//            mMedia.parse();
            fillMenu();
            mMediaPlayer.setMedia(mMedia);

            mMedia.release();

            final Runnable mRunnable = new Runnable() {
                @Override
                public void run() {
                    updateVideoSurfaces();
                }
            };

            mMediaPlayer.setEventListener(new MediaPlayer.EventListener() {
                @Override
                public void onEvent(MediaPlayer.Event event) {
                    switch (event.type) {
                        case MediaPlayer.Event.TimeChanged:
                            synchronized (_started) {
                                if (!_started) {
                                    _started = true;
                                }
                            }
                            if (mAudioTracks == null) {
                                mMedia.parse();
                                fillMenu();
                            }
                            //playback started - reset seek variable
                            is_seeking_ = false;
                            break;
                        case MediaPlayer.Event.EndReached:
                            releasePlayer();
                            finish();
                            break;
                        case MediaPlayer.Event.Opening:
                            synchronized (_started) {
                                _started = false;
                            }
                            break;
                        case MediaPlayer.Event.Paused:
                            break;
                        case MediaPlayer.Event.Playing:
                            mHandler.removeCallbacks(mRunnable);
                            mHandler.post(mRunnable);
                            break;
                    }
                }
            });

            //reset selected streams info
            selected_audio = stream_invalid_idx;
            selected_sub = stream_invalid_idx;

            invalidateTracksInfo();

            mMediaPlayer.play();

            if (mOnLayoutChangeListener == null) {
                mOnLayoutChangeListener = new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right,
                                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (left != oldLeft || top != oldTop || right != oldRight || bottom != oldBottom) {
                            mHandler.removeCallbacks(mRunnable);
                            mHandler.post(mRunnable);
                        }
                    }
                };
            }
            mVideoSurfaceFrame.addOnLayoutChangeListener(mOnLayoutChangeListener);

            if (passtrough && audio_delay != 0)
                mMediaPlayer.setAudioDelay(audio_delay);

            mMediaPlayer.setVolume(100);
        } catch (Exception e) {
            Toast.makeText(this, "Error creating player!", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    private void releasePlayer() {
        if (libvlc == null)
            return;
        mMediaPlayer.setEventListener(null);
        if (mOnLayoutChangeListener != null) {
            mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
            mOnLayoutChangeListener = null;
        }
        if (mMediaPlayer.isPlaying())
            mMediaPlayer.stop();
        mMediaPlayer.setMedia(null);
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        try {
            vout.detachViews();
        } catch (Exception e) {
            Log.e(TAG, "releasePlayer", e);
        }

        mMediaPlayer.release();
        mMediaPlayer = null;

        libvlc.release();
        libvlc = null;
    }

    private void changeMediaPlayerLayout() {
        /* Change the video placement using the MediaPlayer API */
        //assume best fit algorithm for now
        mMediaPlayer.setAspectRatio(null);
        mMediaPlayer.setScale(0);
    }

    private void updateVideoSurfaces() {
        int sw = getWindow().getDecorView().getWidth();
        int sh = getWindow().getDecorView().getHeight();

        // sanity check
        if (sw * sh == 0) {
            Log.e(TAG, "Invalid surface size");
            return;
        }

        if (mMediaPlayer == null || mMediaPlayer.getVLCVout() == null) {
            return;
        }

        mMediaPlayer.getVLCVout().setWindowSize(sw, sh);

        ViewGroup.LayoutParams lp = mSurfaceView.getLayoutParams();
        if (mVideoWidth * mVideoHeight == 0) {
            /* Case of OpenGL vouts: handles the placement of the video using MediaPlayer API */
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mSurfaceView.setLayoutParams(lp);
            lp = mVideoSurfaceFrame.getLayoutParams();
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoSurfaceFrame.setLayoutParams(lp);
            changeMediaPlayerLayout();
            return;
        }

        if (lp.width == lp.height && lp.width == ViewGroup.LayoutParams.MATCH_PARENT) {
            /* We handle the placement of the video using Android View LayoutParams */
            mMediaPlayer.setAspectRatio(null);
            mMediaPlayer.setScale(0);
        }

        double dw = sw, dh = sh;
        final boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        if (sw > sh && isPortrait || sw < sh && !isPortrait) {
            dw = sh;
            dh = sw;
        }

        // compute the aspect ratio
        double ar, vw;
        if (mVideoSarDen == mVideoSarNum) {
            /* No indication about the density, assuming 1:1 */
            vw = mVideoVisibleWidth;
            ar = (double)mVideoVisibleWidth / (double)mVideoVisibleHeight;
        } else {
            /* Use the specified aspect ratio */
            vw = mVideoVisibleWidth * (double)mVideoSarNum / mVideoSarDen;
            ar = vw / mVideoVisibleHeight;
        }

        // compute the display aspect ratio
        double dar = dw / dh;

        //assume best fit algorithm
        if (dar < ar)
            dh = dw / ar;
        else
            dw = dh * ar;

        // set display size
        lp.width  = (int) Math.ceil(dw * mVideoWidth / mVideoVisibleWidth);
        lp.height = (int) Math.ceil(dh * mVideoHeight / mVideoVisibleHeight);
        mSurfaceView.setLayoutParams(lp);
        if (mSurfaceSubtitles != null)
            mSurfaceSubtitles.setLayoutParams(lp);

        // set frame size (crop if necessary)
        lp = mVideoSurfaceFrame.getLayoutParams();
        lp.width = (int) Math.floor(dw);
        lp.height = (int) Math.floor(dh);
        mVideoSurfaceFrame.setLayoutParams(lp);

        mSurfaceView.invalidate();
        if (mSurfaceSubtitles != null)
            mSurfaceSubtitles.invalidate();
    }

    @Override
    public void onNewVideoLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {

        mVideoWidth = width;
        mVideoHeight = height;
        mVideoVisibleWidth = visibleWidth;
        mVideoVisibleHeight = visibleHeight;
        mVideoSarNum = sarNum;
        mVideoSarDen = sarDen;
        updateVideoSurfaces();
    }

    // endregion

    // region View handler
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.play_video_item_activity);

        getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        toast = new Toast(_context);

        if (!Utils.usesRemoteControl(this)) {
            findViewById(R.id.help_line).setVisibility(View.GONE);

            View hbc = findViewById(R.id.hor_bar_container);
            hbc.setPadding(
                    (int)getResources().getDimension(R.dimen.play_live_progress_ll_padding_left_touch),
                    hbc.getPaddingTop(),
                    (int)getResources().getDimension(R.dimen.play_live_progress_ll_padding_right_touch),
                    hbc.getPaddingBottom());

            ImageButton ib = (ImageButton) findViewById(R.id.stop_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCurrentPlaybackPos();
                    finish();
                }
            });

            ib = (ImageButton) findViewById(R.id.pause_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageButton ib = (ImageButton)v;
                    if (togglePause()) {
                        ib.setImageResource(R.drawable.tvm_play_play);
                    } else {
                        ib.setImageResource(R.drawable.tvm_play_pause);
                    }
                }
            });

            ib = (ImageButton) findViewById(R.id.rewind_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnTouchListener(new View.OnTouchListener() {

                private Handler mHandler;

                @Override public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (mHandler != null)
                                return true;
                            //do once and start timer after 300 ms to repeat
                            rewind();
                            mHandler = new Handler();
                            mHandler.postDelayed(mAction, 300);
                            break;
                        case MotionEvent.ACTION_UP:
                            if (mHandler == null) return true;
                            mHandler.removeCallbacks(mAction);
                            mHandler = null;
                            break;
                    }
                    return false;
                }

                Runnable mAction = new Runnable() {
                    @Override public void run() {
                        rewind();
                        mHandler.postDelayed(this, 100);
                    }
                };

            });

            ib = (ImageButton) findViewById(R.id.forward_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnTouchListener(new View.OnTouchListener() {

                private Handler mHandler;

                @Override public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (mHandler != null)
                                return true;
                            //do once and start timer after 300 ms to repeat
                            forward();
                            mHandler = new Handler();
                            mHandler.postDelayed(mAction, 300);
                            break;
                        case MotionEvent.ACTION_UP:
                            if (mHandler == null) return true;
                            mHandler.removeCallbacks(mAction);
                            mHandler = null;
                            break;
                    }
                    return false;
                }

                Runnable mAction = new Runnable() {
                    @Override public void run() {
                        forward();
                        mHandler.postDelayed(this, 100);
                    }
                };

            });
        }


        TextView tv;
        tv = (TextView) findViewById(R.id.audio_title_);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_AUDIO));
        tv = (TextView) findViewById(R.id.subtitles_title_);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SUBTITLES));
        tv = (TextView) findViewById(R.id.help_line);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_PLAY_HELP_TEXT));

        paramPanel = (LinearLayout) findViewById(R.id.param_panel);
        subtitleList = (GridView) paramPanel.findViewById(R.id.subtitles_list_);
        audioTrackList = (GridView) paramPanel.findViewById(R.id.audio_list_);
        currentTime = (TextView) findViewById(R.id.current_time);
        infoPanel = (LinearLayout) findViewById(R.id.info_panel);
        programSeek = (CustomSeekBar) findViewById(R.id.program_seek);

        programSeek.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                int duration = (int) mMediaPlayer.getLength() / 1000;
                if (duration > 0) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                        //stop statistics timer with playback position updates
                        stopStatsTimer();
                        updateCurrentPlaybackPosition(getAbsProgressTimeFromPoint(motionEvent.getX(), duration));
                        programSeek.setProgress(getProgressFromPoint(motionEvent.getX()));
                    } else if (motionEvent.getAction() == MotionEvent.ACTION_MOVE) {
                        updateCurrentPlaybackPosition(getAbsProgressTimeFromPoint(motionEvent.getX(), duration));
                        programSeek.setProgress(getProgressFromPoint(motionEvent.getX()));
                    } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        updateCurrentPlaybackPosition(getAbsProgressTimeFromPoint(motionEvent.getX(), duration));
                        programSeek.setProgress(getProgressFromPoint(motionEvent.getX()));
                        //request seek and restart stats timer
                        queue.add(new Pair<Integer, Object>(state_seek, getAbsProgressTimeFromPoint(motionEvent.getX(), duration)));
                        startStatsTimer();
                    }
                }
                return true;
            }
        });

        programSeek.setMax(SEEK_MAX_VALUE);
        programSeek.setProgress(0);
        programSeek.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                startHideInfoPanelTimer();

                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                if (backPressHandle(keyCode, keyEvent)) {
                    return true;
                }

                if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {
                    rewind();
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                    forward();
                    return true;
                }
                if (Common.okPressed(keyCode)) {
                    togglePause();
                    return true;
                }
                return false;
            }
        });
        programSeek.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                enableInfoPanel(true);
                if (focus) {
                    startHideInfoPanelTimer();
                }
            }
        });
        programSeek.setOnLongPress(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    showParamPanel();
                    return true;
                }
                return false;
            }
        });

        prepareInputParams();
        preparePlayerViews();
        setRefreshRate();
        prepareInfoPanel();

        state_ = state_ready;
        activateProgressBarFullScreen(false);
    }

    protected int getProgressFromPoint(float x) {
        int width = programSeek.getWidth();
        return (int)(x / width * SEEK_MAX_VALUE);
    }

    protected int getAbsProgressTimeFromPoint(float x, int duration) {
        int width = programSeek.getWidth();
        return (int)(x / width * duration);
    }

    protected void getCurrentPlaybackPos() {
        try {
            if (mMediaPlayer != null) {
                new_resume_position_ = (int) (mMediaPlayer.getTime() / 1000);

                //if we are in the last 5 seconds - reset it to 0
                if (new_resume_position_ + 5 > mMediaPlayer.getLength())
                    new_resume_position_ = 0;
            }

        } catch (Exception e) {
            new_resume_position_ = 0;
        }
    }

    private boolean backPressHandle(int keyCode, KeyEvent keyEvent) {
        if (Common.backPressed(keyCode, keyEvent)) {
            if (infoPanelHidden() && paramPanelHidden()) {
                getCurrentPlaybackPos();
                finish();
            } else {
                hidePanels();
            }
            return true;
        }
        return false;
    }

    private boolean togglePause() {
        if (unPause()) {
            return false;
        }
        mMediaPlayer.pause();

        return true;
    }

    private boolean unPause() {
        if (!mMediaPlayer.isPlaying()) {
            invalidateTracksInfo();
            mMediaPlayer.play();
            return true;
        }
        return false;
    }

    private void startStatsTimer() {
        stopStatsTimer();
        getStatsTimer = new Timer();
        getStatsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        requestStats();
                    }
                });
            }
        }, 0, TIME_SHIFT_STATS_TIMER_INTERVAL);
    }

    private void stopStatsTimer() {
        if (getStatsTimer == null) {
            return;
        }
        getStatsTimer.cancel();
        getStatsTimer.purge();
        getStatsTimer = null;
    }

    private void UpdatePlayStats() {
        if (!is_seeking_) {
            int pos_seconds = 0;
            int percN = 0;
            int duration_minutes = 0;
            if (mMediaPlayer != null) {
                //for debug purposes
                float f = mMediaPlayer.getPosition();
                long l = mMediaPlayer.getTime();
                long totalTime = (long) (l / f);
                long ll = mMediaPlayer.getLength();
                long d = mMediaPlayer.getMedia().getDuration();
                Log.i(TAG, String.format("%f, %d, %d, %d, %d", f, l, ll, d, totalTime));

                int duration = (int)mMediaPlayer.getLength() / 1000;
                pos_seconds = (int) mMediaPlayer.getTime() / 1000;
                percN = duration > 0 ? calcPerN(pos_seconds, duration, SEEK_MAX_VALUE) : 0;

                duration_minutes = (int) (duration / 60.0);
            }
            programSeek.setProgress(percN);
            updateCurrentPlaybackPosition(pos_seconds);
            //update duration
            if (duration_minutes > 0) {
                String strDuration = String.format(Locale.getDefault(), "%s %d %s",
                        language_manager_.GetString(LocaleStrings.IDS_DURATION), duration_minutes,
                        language_manager_.GetString(LocaleStrings.IDS_MINUTE));

                programTime.setText(strDuration);
            }
        }
    }

    // region Server communication TimeShift
    private void requestStats() {
        // Log.i(TAG, "requestStats");
        synchronized (_started) {
            if (!_started) {
                return;
            }
        }
        if (state_ != state_ready) {
            return;
        }

        try {
            Pair<Integer, Object> pair = queue.poll();
            if (pair == null) {
                UpdatePlayStats();
            } else {
                switch (pair.first) {
                    case state_seek:
                        Pair<Integer, Object> pair1 = queue.peek();
                        while (pair1 != null && pair1.first == state_seek) {
                            pair = queue.poll();
                            pair1 = queue.peek();
                        }

                        int duration = (int)mMediaPlayer.getLength() / 1000;
                        float t = Float.parseFloat(pair.second.toString());
                        float pos = t / duration;
                        Log.i(TAG, String.format("SetPos: %f, %f", t, pos));
                        mMediaPlayer.setPosition(pos);
                        is_seeking_ = true;
//                        mMediaPlayer.setTime((int)pair.second * 1000);
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "requestStats", e);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus && !ignoreWindowFocusChange) {
            if (Utils.usesRemoteControl(this)) {
                showInfoPanel();
                infoPanel.requestFocus();
            }
            ignoreWindowFocusChange = false;
        }
    }

    @Override
    public void finish() {
        Intent i = getIntent();
        i.putExtra("resume_position", new_resume_position_);
        setResult(RESULT_FIRST_USER, i);

        super.finish();
    }

    @Override
    protected void onDestroy() {
        stopStatsTimer();
        // playback complete, give up audio focus
        audioManager.abandonAudioFocus(audioFocusChangeListener);
        //unregister media buttons
        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
        releasePlayer();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        getCurrentPlaybackPos();
        finish();
/*
        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
        if (mMediaPlayer != null)
            mMediaPlayer.pause();
*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        audioManager = (AudioManager) _context.getSystemService(AUDIO_SERVICE);
        audioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                switch (focusChange) {
                    case AudioManager.AUDIOFOCUS_GAIN:
                        // continue playback and raise volume (if it was previously lowered)
                        audioManager.registerMediaButtonEventReceiver(mediaButtonReceiver);
                        if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
                            invalidateTracksInfo();
                            mMediaPlayer.play();
                        }
                        break;

                    case AudioManager.AUDIOFOCUS_LOSS:
                        // stop playback, deregister buttons, clean up
                        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
                            mMediaPlayer.pause();
                        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
                        break;
                }
            }
        };
        int result = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mediaButtonReceiver = new ComponentName(getPackageName(), MediaButtonReceiver.class.getName());
            audioManager.registerMediaButtonEventReceiver(mediaButtonReceiver);
            // start playback
        }

        if (mMediaPlayer != null) {
            invalidateTracksInfo();
            mMediaPlayer.play();
        } else {
            //first start
            createPlayer();
            startPlayback();
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            if (!infoPanelShowing) {
                showParamPanel();
                return true;
            }
        }
        return super.onKeyLongPress(keyCode, event);
    }

    protected void hidePanels() {
        if (!paramPanelHidden()) {
            hideParamPanel();

            if (!infoPanelHidden())
                programSeek.requestFocus();
        }
        hideInfoPanel();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (backPressHandle(keyCode, event)) {
            return true;
        }

        boolean isDown = event.getAction() == KeyEvent.ACTION_DOWN;

        if (isDown) {
            if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN ||
                    keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                    keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
                return super.onKeyDown(keyCode, event);
            }
        }

        if (infoPanelHidden()) {
            showInfoPanel();
        }
        startHideInfoPanelTimer();

        if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            event.startTracking();
            return true;
        }

        if (isDown) {
            if (keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                forward();
                return true;
            }
            if (keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {
                rewind();
                return true;
            }
            if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
                togglePause();
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    // endregion

}


