package com.dvblogic.tvmosaic;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dvblogic.dvblink_common.Program;
import com.dvblogic.dvblink_common.Utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import TVMosaic.TVMosaic.R;
import com.dvblogic.dvblink_common.VideoInfo;

public class ProgramDetails extends RelativeLayout {

    Paint paint_;
    Rect rt_ = new Rect();

    protected VideoInfo program_;
    protected String channel_name_;
    protected LanguageManager		language_manager_;
    protected boolean is_empty_ = true;
    protected float score_ = 0;


    public ProgramDetails(Context context) {
        super(context);
   }

    public ProgramDetails(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgramDetails(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (paint_ == null)
            paint_ = new Paint();

        paint_.setStyle(Paint.Style.FILL_AND_STROKE);
        paint_.setColor(ContextCompat.getColor(getContext(), R.color.program_pane_bkg));

        int top_offs = (int)getContext().getResources().getDimension(R.dimen.program_pane_top_offs);
        int left_offs = (int)getContext().getResources().getDimension(R.dimen.guide_left_edge_offset);
        rt_.set(left_offs, top_offs, getWidth(), getHeight());
        paint_.setColor(ContextCompat.getColor(getContext(), R.color.channel_item_bkg_selected));
        canvas.drawRect(rt_, paint_);

    }

    public void setInfo(VideoInfo _program, String _channel_name, float _score, boolean _adviser_mode, boolean record_conflict,
                        LanguageManager _language_manager) {
        is_empty_ = false;
        language_manager_ = _language_manager;
        channel_name_ = _channel_name;
        score_ = _score;
        program_ = _program;

        update();
    }

    public void setInfo(VideoInfo _program, String _channel_name) {

        is_empty_ = true;
        channel_name_ = _channel_name;
        program_ = _program;

        update();
    }


    @SuppressLint("DefaultLocale")
    public void update() {

        if (is_empty_) {
            updateEmpty();
            return;
        }

        try {
            // title
            TextView tv = (TextView) findViewById(R.id.program_name);
            tv.setText(program_.Title == null ? "" : program_.Title);

            ImageView hd = (ImageView) findViewById(R.id.hd_icon);
            hd.setVisibility(program_.IsHdtv ? View.VISIBLE : View.INVISIBLE);

            ImageView repeat_premiere = (ImageView) findViewById(R.id.premiere_repeat_icon);
            if (program_.IsRepeat || program_.IsPremiere) {
                repeat_premiere.setImageResource(program_.IsRepeat ? R.drawable.tvm_icon_repeat : R.drawable.tvm_icon_premiere);
                repeat_premiere.setVisibility(View.VISIBLE);
            } else {
                repeat_premiere.setVisibility(View.INVISIBLE);
            }

            // episode
            String str = ((program_.Subname == null) ? "" : program_.Subname);

            String ES = "";
            if (program_.SeasonNum != 0)
                ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_SEASON), program_.SeasonNum);
            if (program_.EpisodeNum != 0) {
                if (program_.SeasonNum != 0)
                    ES += ", ";
                ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_EPISODE),
                        program_.EpisodeNum);
            }
            if (str.isEmpty()) {
                str += ES;
            } else {
                if (!ES.isEmpty())
                    str += "  |  " + ES;
            }

            tv = (TextView) findViewById(R.id.program_desc_line_1);
            tv.setText(str);

            // channel & time
            String strChannel = channel_name_;

            String strDate = null;
            if (Utils.isToday(program_.StartTime)) {
                strDate = language_manager_.GetString(LocaleStrings.IDS_TODAY);
            } else {
                strDate = Utils.UtcToMediumDateString(this.getContext(), program_.StartTime);
            }

            String strStart = Utils.UtcToShortTimeString(this.getContext(), program_.StartTime);
            String strEnd = Utils.UtcToShortTimeString(this.getContext(), program_.StartTime + program_.Duration);

            int duration_minutes = (int) (program_.Duration / 60.0);
            String strDuration = String.format(Locale.getDefault(), "%s %d %s",
                    language_manager_.GetString(LocaleStrings.IDS_DURATION), duration_minutes,
                    language_manager_.GetString(LocaleStrings.IDS_MINUTE));

            if (program_.StartTime > 0)
                //this is recorded tv - with channel and start/duration
                str = String.format("%s  |  %s  |  %s - %s  |  %s", strChannel, strDate, strStart, strEnd, strDuration);
            else if (program_.Duration > 0)
                //this is video with duration
                str = String.format("%s", strDuration);
            else
                //leave description string empty
                str = "";

            tv = (TextView) findViewById(R.id.program_desc_line_2);
            tv.setText(str);

            tv = (TextView) findViewById(R.id.program_long_description);
            tv.setText(program_.ShortDesc == null ? "" : program_.ShortDesc);

            invalidate();

        } catch (Exception e) {

        }
    }

    @SuppressLint("DefaultLocale")
    public void updateEmpty() {
        clear(program_.Title);
    }

    public void clear(String message) {
        try {
            TextView tv = (TextView) findViewById(R.id.program_name);
            tv.setText(message == null ? "" : message);

            tv = (TextView) findViewById(R.id.program_desc_line_1);
            tv.setText("");

            tv = (TextView) findViewById(R.id.program_desc_line_2);
            tv.setText("");

            tv = (TextView) findViewById(R.id.program_long_description);
            tv.setText("");

            ImageView iv = (ImageView) findViewById(R.id.hd_icon);
            iv.setVisibility(View.INVISIBLE);

            iv = (ImageView) findViewById(R.id.premiere_repeat_icon);
            iv.setVisibility(View.INVISIBLE);

        } catch (Exception e) {

        }
    }

    public void clear() {
        clear("");
    }
}

