package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import TVMosaic.TVMosaic.R;
import android.util.Log;
import android.view.*;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.dvblogic.dvblink_common.*;

import java.util.*;

public class EPGMappingActivity extends BaseActivity
{
	private static final String TAG = "EPGSetActivity";
	private static final int MANUAL_MAPPING_DIALOG = 1;
	private static final int AUTO_MAPPING_DIALOG = 2;
	private static final String SKIP_CHANNEL_ID = "a6c3fe00-c4a0-44bd-8daf-cf255c38abc0";
    private static final String RESET_CHANNEL_ID = "87d4976b-3142-4d37-84ff-fdcec4055401";

	private EPGSettingsProviderEx provider;
	private ListView mappedChannelList;
	private ViewFlipper flipper;
	private TextView dialogChannelNameTextView;
	private RelativeLayout dialogChannelView;
	private ProgressBar autoMappingProgressBar;

	private ListView epgChannelList;
	private HashMap<String, Channel> channels;
	private EPGSource epgSrc;
	private HashMap<String, MappedChannel> epgChannels;
	private HashMap<String, MappedChannel> partialEPGChannels = new HashMap<>();
	private Iterator<MappedChannel> autoMappingChannelIterator = null;
	private boolean autoMappingMode = false;
	private MappedChannel currentChannel = null;

	public class MappedChannel {
		public String id;
		public String name;
		public EPGSource epgSource;
		public EPGChannel epgChannel;
	}

	interface OnChannelClickListener {
		void onClick(MappedChannel mappedChannel, int position);
	}

	private class MappedChannelsAdapter extends BaseAdapter {

		private class MappedChannelClickListener implements View.OnClickListener {
			@Override
			public void onClick(View view) {
				int position = (int) view.getTag();
				MappedChannel mappedChannel = (MappedChannel) getItem(position);
				onChannelClickListener.onClick(mappedChannel, position);
			}
		}

		private LayoutInflater ltInflater;
		private ArrayList<MappedChannel> mappedChannels = new ArrayList<>();
		private MappedChannelClickListener mappedChannelClickListener = new MappedChannelClickListener();
		private OnChannelClickListener onChannelClickListener;

		MappedChannelsAdapter() {
			ltInflater = getLayoutInflater();
		}

		public ArrayList<EPGChannelConfig> getMappedChannels() {
			ArrayList<EPGChannelConfig> results = new ArrayList<>();
			for (MappedChannel mappedChannel: mappedChannels) {
				if (mappedChannel.epgSource.isDefault()) {
					continue;
				}
				results.add(new EPGChannelConfig(
						mappedChannel.id, mappedChannel.epgSource.id, mappedChannel.epgChannel.id
				));
			}
			Collections.sort(results, new Comparator<EPGChannelConfig>() {
				@Override
				public int compare(EPGChannelConfig o1, EPGChannelConfig o2) {
					return o1.id.compareToIgnoreCase(o2.id);
				}
			});
			return results;
		}

		public ArrayList<MappedChannel> getNotMappedChannels() {
			ArrayList<MappedChannel> results = new ArrayList<>();
			for (MappedChannel mappedChannel: mappedChannels) {
				if (!mappedChannel.epgSource.isDefault()) {
					continue;
				}
				results.add(mappedChannel);
			}
			return results;
		}

		public void setChannelMapping(String channelId, EPGSource epgSource, EPGChannel epgChannel) {
			MappedChannel mappedChannel = getMappingChannel(channelId);
			if (mappedChannel == null) {
				return;
			}
			mappedChannel.epgSource = epgSource;
			mappedChannel.epgChannel = epgChannel;
			notifyDataSetChanged();
			sort();
		}

		public void setOnChannelClickListener(OnChannelClickListener onChannelClickListener) {
			this.onChannelClickListener = onChannelClickListener;
		}

		@Override
		public int getCount() {
			return mappedChannels.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = ltInflater.inflate(R.layout.settings_link_layout, parent, false);
			}

			MappedChannel mappedChannel = (MappedChannel) getItem(position);
			TextView label;
			label = (TextView) view.findViewById(R.id.title_text);
			label.setText(mappedChannel.name);

			label = (TextView) view.findViewById(R.id.desc_text);
			label.setText(
					mappedChannel.epgSource.isDefault() ? language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_DEFAULT_SOURCE) :
							mappedChannel.epgChannel.name + " (" + mappedChannel.epgSource.name + ")"
			);

			view.setOnClickListener(mappedChannelClickListener);
			view.setTag(position);
			return view;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return mappedChannels.get(position);
		}

		public void add(MappedChannel mappedChannel) {
			mappedChannels.add(mappedChannel);
		}

		public void sort() {
			Collections.sort(mappedChannels, new Comparator<MappedChannel>() {
				@Override
				public int compare(MappedChannel mc1, MappedChannel mc2) {
					boolean mc1Default = mc1.epgSource.isDefault();
					boolean mc2Default = mc2.epgSource.isDefault();
					if (mc1Default && mc2Default) {
						return mc1.name.compareToIgnoreCase(mc2.name);
					}
					if (!mc1Default && mc2Default) {
						return 1;
					}
					if (mc1Default) {
						return -1;
					}
					return 0;
				}
			});
		}

		private MappedChannel getMappingChannel(String channelId) {
			for (MappedChannel mappedChannel: mappedChannels) {
				if (mappedChannel.id.equalsIgnoreCase(channelId)) {
					return mappedChannel;
				}
			}
			return null;
		}
	}

	private class EPGSettingsProviderEx extends EPGSettingsProvider {
		public EPGSettingsProviderEx() {
			super(EPGMappingActivity.this, serverSettings, language_manager_.getID());
		}

		@Override
		protected void onChannels(ChannelsList channels_) {
			channels = new HashMap<>();
			for (Channel channel: channels_.list()) {
				channels.put(channel.ID, channel);
			}

            //request EPG channels of this source
            provider.requestEPGChannels(epgSrc.id);
		}

		@Override
		protected void onEPGChannels(String epgSource, EPGChannelList epgChannelList) {
			epgChannels = new HashMap<>();
			for (EPGChannel epgChannel: epgChannelList.list()) {
				MappedChannel mappedChannel = new MappedChannel();
				mappedChannel.epgChannel = new EPGChannel(
						epgChannel.id, epgChannel.name
				);
				mappedChannel.epgSource = new EPGSource(epgSource, epgSrc.name, false);
				epgChannels.put(epgChannel.id, mappedChannel);
			}

            //show warning if there are no EPG channels
            if (epgChannelList.size() == 0)
                Toast.makeText(_context, language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SOURCE_CHANNELS_NOT_FOUND), Toast.LENGTH_LONG).show();

			EPGMappingActivity.this.onRequestEPGChannelConfig();
		}

		@Override
		protected void onGetEPGChannelConfig(EPGChannelConfigList epgChannelConfigList) {
			showMappedChannels(epgChannelConfigList);
		}

		@Override
		protected void onSetEPGChannelConfig() {
			setFlipperDirection(false);
			flipper.showPrevious();
		}

		@Override
		protected void onMatchEPGChannels(EPGMatchResults epgMatchResults) {
			String channelId = (String) epgMatchResults.keySet().toArray()[0];
			EPGMatchResults.EPGChannelsMatch EPGChannelsMatch = epgMatchResults.get(channelId);

			if (EPGChannelsMatch.partial.size() > 0 && EPGChannelsMatch.exact.size() < 1) {
				partialEPGChannels.clear();
				for (EPGMatchResults.EPGChannel epgChannel: EPGChannelsMatch.partial) {
					MappedChannel mappedChannel = new MappedChannel();
					mappedChannel.epgChannel = new EPGChannel(
							epgChannel.epgChannelId, epgChannels.get(epgChannel.epgChannelId).epgChannel.name
					);
					mappedChannel.epgSource = new EPGSource(epgChannel.epgSourceId, epgSrc.name, false);
					partialEPGChannels.put(epgChannel.epgChannelId, mappedChannel);
				}
				showChannelsSelector();
				return;
			}
			if (EPGChannelsMatch.exact.size() > 0) {
				EPGMatchResults.EPGChannel epgChannel = EPGChannelsMatch.exact.get(0);
				setMappingChannel(channelId, epgChannel.epgSourceId, epgChannel.epgChannelId);

			}
			requestAutoMappingNextChannel();
		}
	}

	public EPGMappingActivity() {
		show_exit_dialog = false;
	}

	private void resetChannelsToDefaultEPG() {
		activateProgressBar(true);
		for (String channelId: channels.keySet()) {
			setMappingChannel(channelId, EPGSource.DEFAULT_EPG_SOURCE_ID, channelId);
		}
		activateProgressBar(false);
	}

	private void showChannelsSelector() {
        if (currentChannel != null)
		    dialogChannelNameTextView.setText(currentChannel.name);

		setChannelListInvisible(false);
		ArrayList<HashMap<String, String>> epgChannelHashMap = new ArrayList<>();
		HashMap<String, MappedChannel> localEPGChannels = autoMappingMode ? partialEPGChannels : epgChannels;
		for (MappedChannel mappedChannel : localEPGChannels.values()) {
			HashMap<String, String> map = new HashMap<>();
			map.put("id", mappedChannel.epgChannel.id);
			map.put("name", mappedChannel.epgChannel.name);
			map.put("source", mappedChannel.epgSource.name);
			map.put("source_id", mappedChannel.epgSource.id);
			epgChannelHashMap.add(map);
		}
		if (autoMappingMode) {
			HashMap<String, String> skipChannelItem = new HashMap<>();
			skipChannelItem.put("id", SKIP_CHANNEL_ID);
			skipChannelItem.put("name", language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SKIP_CHANNEL));
			skipChannelItem.put("source", language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SKIP_CHANNEL_DESC));
			skipChannelItem.put("source_id", SKIP_CHANNEL_ID);
			epgChannelHashMap.add(skipChannelItem);
		} else {
            HashMap<String, String> resetChannelItem = new HashMap<>();
            resetChannelItem.put("id", RESET_CHANNEL_ID);
            resetChannelItem.put("name", language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_RESET_CHANNEL));
            resetChannelItem.put("source", language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_RESET_CHANNEL_DESC));
            resetChannelItem.put("source_id", RESET_CHANNEL_ID);
            epgChannelHashMap.add(resetChannelItem);
        }


		Collections.sort(epgChannelHashMap, new Comparator<HashMap<String, String>>() {
			@Override
			public int compare(HashMap<String, String> o1, HashMap<String, String> o2) {
				if (o1.get("id").equalsIgnoreCase(SKIP_CHANNEL_ID) || o1.get("id").equalsIgnoreCase(RESET_CHANNEL_ID)) {
					return -1;
				}
				if (o2.get("id").equalsIgnoreCase(SKIP_CHANNEL_ID) || o2.get("id").equalsIgnoreCase(RESET_CHANNEL_ID)) {
					return 1;
				}
				return o1.get("name").compareToIgnoreCase(o2.get("name"));
			}
		});
		SimpleAdapter adapter = new SimpleAdapter(
				this, epgChannelHashMap, R.layout.scan_list_item_2,
				new String[]{"name", "source"},
				new int[]{R.id.text1, R.id.text2}
		);
		epgChannelList.setAdapter(adapter);
		epgChannelList.setItemsCanFocus(true);

        if (!autoMappingMode && currentChannel != null) {
            //navigate to the closest item in the list

            class ChannelListScroller implements Runnable {
                int position_;
                ChannelListScroller(int pos) { position_ = pos; }
                public void run() {
                    epgChannelList.setSelection(position_);
                    epgChannelList.requestFocus();
                }
            }

            epgChannelList.clearFocus();
            epgChannelList.post(new ChannelListScroller(getClosestNameIndex(currentChannel.name, epgChannelHashMap)));
        } else {
            epgChannelList.requestFocus();
        }
	}

    private int getClosestNameIndex(String channelName, ArrayList<HashMap<String, String>> epgChannelHashMap) {
        int ret_val = 0;

        for (int i = 0; i < epgChannelHashMap.size(); i++) {
            HashMap<String, String> hm = epgChannelHashMap.get(i);
            if (hm.get("id").equalsIgnoreCase(SKIP_CHANNEL_ID) || hm.get("id").equalsIgnoreCase(RESET_CHANNEL_ID))
                continue;

            if (hm.get("name").compareToIgnoreCase(channelName) >= 0) {
                ret_val = i;
                break;
            }
        }

        return ret_val;
    }

	private void requestAutoMappingNextChannel() {
		if (autoMappingChannelIterator == null) {
			ArrayList<EPGMappingActivity.MappedChannel> mappedChannels = (
					(MappedChannelsAdapter) mappedChannelList.getAdapter()
			).getNotMappedChannels();
			autoMappingChannelIterator = mappedChannels.iterator();
			autoMappingProgressBar.setMax(mappedChannels.size());
			autoMappingProgressBar.setIndeterminate(false);
			autoMappingProgressBar.setProgress(0);
		}
		if (autoMappingChannelIterator.hasNext()) {
			autoMappingProgressBar.setProgress(autoMappingProgressBar.getProgress() + 1);
			MappedChannel mappedChannel = autoMappingChannelIterator.next();
			EPGChannelConfig epgChannelConfig = new EPGChannelConfig(
					mappedChannel.id, mappedChannel.epgSource.id, mappedChannel.epgChannel.id
			);
			ArrayList<String> channelsID = new ArrayList<>();
			channelsID.add(epgChannelConfig.id);
			currentChannel = mappedChannel;
			provider.requestMatchEPGChannels(epgSrc.id, channelsID);
		} else {
			dismissDialog(AUTO_MAPPING_DIALOG);
		}
	}

	private void showMappedChannels(EPGChannelConfigList epgChannelConfigList) {
		MappedChannelsAdapter mappedChannelsAdapter = new MappedChannelsAdapter();
		for (EPGChannelConfig epgChannelConfig: epgChannelConfigList.list()) {
			MappedChannel mappedChannel = new MappedChannel();
			mappedChannel.id = epgChannelConfig.id;

			if (!epgSrc.equals_id(epgChannelConfig.epgSourceId) &&
					!epgChannelConfig.epgSourceId.equalsIgnoreCase(EPGSource.DEFAULT_EPG_SOURCE_ID)) {
				continue;
			}
			if (!channels.containsKey(mappedChannel.id)) {
				continue;
			}

			mappedChannel.name = channels.get(mappedChannel.id).Name;
			mappedChannel.epgChannel = epgChannels.containsKey(epgChannelConfig.epgChannelId) ?
					epgChannels.get(epgChannelConfig.epgChannelId).epgChannel :
					new EPGChannel(epgChannelConfig.epgChannelId, "");
			mappedChannel.epgSource = epgSrc.equals_id(epgChannelConfig.epgSourceId) ?
                    epgSrc : new EPGSource();
			mappedChannelsAdapter.add(mappedChannel);
		}
		mappedChannelsAdapter.setOnChannelClickListener(new OnChannelClickListener() {
			@Override
			public void onClick(MappedChannel mappedChannel, int position) {
				currentChannel = mappedChannel;
				autoMappingMode = false;
				showDialog(MANUAL_MAPPING_DIALOG);
			}
		});
		mappedChannelsAdapter.sort();
		mappedChannelList.setAdapter(mappedChannelsAdapter);
		mappedChannelList.setItemsCanFocus(true);
	}

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.epg_mapping_activity);

		String epg_src_str = getIntent().getExtras().getString("epg_source");
		epgSrc = (EPGSource) Serializer.deserialize(epg_src_str, EPGSource.class);

		customView();

		View activity = findViewById(R.id.epg_mapping_activity);
		mappedChannelList = (ListView) activity.findViewById(R.id.epg_mapping_list);
		flipper = (ViewFlipper) findViewById(R.id.epg_mapping_flipper);

		provider = new EPGSettingsProviderEx();

		getWindow().getDecorView().post(new Runnable() {
			public void run() {
				provider.requestChannels();
			}
		});
	}

	private AdapterView.OnItemClickListener selectChannelClickListener = new AdapterView.OnItemClickListener() {
		@Override
		public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
			HashMap<String, String> params = (HashMap<String, String>) adapterView.getItemAtPosition(position);
			String epgChannelId = params.get("id");
			String epgSourceId = params.get("source_id");
			String channelId = currentChannel.id;

            if (epgChannelId.equalsIgnoreCase(RESET_CHANNEL_ID))
                epgSourceId = EPGSource.DEFAULT_EPG_SOURCE_ID;

			if (!epgChannelId.equalsIgnoreCase(SKIP_CHANNEL_ID)) {
				setMappingChannel(channelId, epgSourceId, epgChannelId);
			}
			currentChannel = null;
			if (autoMappingMode) {
				setChannelListInvisible(true);
				requestAutoMappingNextChannel();
			} else {
				dismissDialog(MANUAL_MAPPING_DIALOG);
			}
		}
	};

	@Override
	protected Dialog onCreateDialog(final int dialogID) {
		if (MANUAL_MAPPING_DIALOG == dialogID || AUTO_MAPPING_DIALOG == dialogID) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.NewSettingsProgressDialog);
			View dialog = getLayoutInflater()
					.inflate(R.layout.epg_mapping_dialog_layout, null);

			TextView tv;
			RelativeLayout rl;

			rl = (RelativeLayout) dialog.findViewById(R.id.epg_mapping_category);
			dialogChannelView = rl;
			tv = (TextView) rl.findViewById(R.id.category_text);
			tv.setText("");
			dialogChannelNameTextView = tv;
			tv = (TextView) rl.findViewById(R.id.category_desc);
			tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SELECT_CHANNEL));

			epgChannelList = (ListView) dialog.findViewById(R.id.epg_mapping_list);
			epgChannelList.setItemsCanFocus(true);
			epgChannelList.setOnItemClickListener(selectChannelClickListener);

			autoMappingProgressBar = (ProgressBar) dialog.findViewById(R.id.progress_bar);
			AlertDialog alertDialog = builder
					.setView(dialog)
					.setCancelable(true)
					.create();
			alertDialog.setCanceledOnTouchOutside(false);
			return alertDialog;
		}
		return null;
	}

	@Override
	public void onPrepareDialog(int dialogID, Dialog dialog) {
		if (MANUAL_MAPPING_DIALOG == dialogID) {
			setProgressBarInvisible(true);
			showChannelsSelector();
		}
		if (AUTO_MAPPING_DIALOG == dialogID) {
			setChannelListInvisible(true);
			setProgressBarInvisible(false);
		}
	}

	private void setMappingChannel(String channelId, String epgSourceId, String epgChannelId) {
		EPGChannel epgChannel = epgChannels.containsKey(epgChannelId) ?
				epgChannels.get(epgChannelId).epgChannel : new EPGChannel(epgChannelId, "");
		EPGSource epgSource = epgSrc.equals_id(epgSourceId) ? epgSrc : new EPGSource();
		((MappedChannelsAdapter) mappedChannelList.getAdapter()).setChannelMapping(
				channelId, epgSource, epgChannel
		);
	}

	private void customView() {
		TextView tv = (TextView)findViewById(R.id.head_bar);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MAPPING_CATEGORY));

		RelativeLayout rl = (RelativeLayout)findViewById(R.id.epg_action_category);
		tv = (TextView)rl.findViewById(R.id.category_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MAPPING));
		tv = (TextView)rl.findViewById(R.id.category_desc);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MAPPING_DESC));

		View autoMappingLink = findViewById(R.id.auto_mapping_link);
		tv = (TextView) autoMappingLink.findViewById(R.id.title_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_AUTO_MAPPING_LINK));
		tv = (TextView) autoMappingLink.findViewById(R.id.desc_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_AUTO_MAPPING_LINK_DESC));
		autoMappingLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
                showMappingPage(true);
			}
		});

		View manualMappingLink = findViewById(R.id.manual_mapping_link);
		tv = (TextView) manualMappingLink.findViewById(R.id.title_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MANUAL_MAPPING_LINK));
		tv = (TextView) manualMappingLink.findViewById(R.id.desc_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MANUAL_MAPPING_LINK_DESC));
		manualMappingLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
                showMappingPage(false);
			}
		});

		View resetLink = findViewById(R.id.reset_link);
		tv = (TextView) resetLink.findViewById(R.id.title_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_RESET_MAPPING_LINK));
		tv = (TextView) resetLink.findViewById(R.id.desc_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_RESET_MAPPING_LINK_DESC));
		resetLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
                AlertDialog.Builder ad = new AlertDialog.Builder(_context)
                    .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                    .setMessage(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_RESET_CONFIRM))
                    .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    resetChannelsToDefaultEPG();
                                }
                            })
                    .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            });
                ad.show();
			}
		});
	}

    protected void showMappingPage(boolean autoMapping) {
        setFlipperDirection(true);
        String categoryText = language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MANUAL_MAPPING_LINK);
        String categoryDesc = language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MANUAL_MAPPING_LINK_DESC);

        autoMappingMode = autoMapping;

        if (autoMappingMode) {
            autoMappingChannelIterator = null;
            showDialog(AUTO_MAPPING_DIALOG);
            requestAutoMappingNextChannel();
            categoryText = language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_AUTO_MAPPING_LINK);
            categoryDesc = language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_AUTO_MAPPING_LINK_DESC);
        }

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.epg_mapping_mapping_view);
        RelativeLayout category = (RelativeLayout) rl.findViewById(R.id.epg_mapping_category);
        TextView tv;

        if (Utils.usesRemoteControl(this)) {
            tv = (TextView) category.findViewById(R.id.category_text);
            tv.setText(categoryText);
            tv = (TextView) category.findViewById(R.id.category_desc);
            tv.setText(categoryDesc);
        } else {
            //do not show category on mobiles to save space for channels
            category.setVisibility(View.GONE);
        }

        flipper.setDisplayedChild(flipper.indexOfChild(rl));
    }

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() != KeyEvent.ACTION_DOWN) {
			return true;
		}

		if (keyCode == 58) {
			return false;
		}

		if (Common.backPressed(keyCode, event)) {
			autoMappingMode = false;
			View view = flipper.getCurrentView();
			int viewID = view.getId();

			if (viewID == R.id.epg_mapping_mapping_view) {
				saveMapping();
			} else {
				onBackPressed();
			}

			return true;
		}
		return false;
	}

	private void saveMapping() {
		ArrayList<EPGChannelConfig> channelsForSave = ((MappedChannelsAdapter) mappedChannelList.getAdapter())
				.getMappedChannels();
		// Можно сериализовать channelsForSave, сравнивать со старым значением и только тогда сохранять.
		provider.requestSaveEPGChannelConfig(channelsForSave);
	}

	private void requestEPGChannels() {
		if (epgChannels != null && epgChannels.size() > 0) {
			onRequestEPGChannelConfig();
			return;
		}
		provider.requestEPGChannels(epgSrc.id);
	}

	private void onRequestEPGChannelConfig() {
		ArrayList<String> channelsId = new ArrayList<>();
		for (Map.Entry<String, Channel> channel: channels.entrySet()) {
			channelsId.add(channel.getKey());
		}
		provider.requestEPGChannelConfig(channelsId);
	}

	protected void setFlipperDirection(Boolean forward) {
		flipper.setInAnimation(null);
		flipper.setOutAnimation(null);
		flipper.setInAnimation(AnimationUtils.loadAnimation(_context, forward ? R.anim.scan_in : R.anim.scan_in_rev));
		flipper.setOutAnimation(AnimationUtils.loadAnimation(_context, forward ? R.anim.scan_out : R.anim.scan_out_rev));
	}

	public void setProgressBarInvisible(boolean invisible) {
		int visibility = invisible ? View.GONE : View.VISIBLE;
		autoMappingProgressBar.setVisibility(visibility);
	}

	public void setChannelListInvisible(boolean invisible) {
		int visibility = invisible ? View.GONE : View.VISIBLE;
		dialogChannelView.setVisibility(visibility);
		epgChannelList.setVisibility(visibility);
	}
}
