package com.dvblogic.tvmosaic;

import com.dvblogic.dvblink_common.Profile;
import com.dvblogic.dvblink_common.RecommendationsList;

class ProfileRecommendation {
	public String providerId = null;
	public Profile profile;
	public RecommendationsList recommendationList;
}
