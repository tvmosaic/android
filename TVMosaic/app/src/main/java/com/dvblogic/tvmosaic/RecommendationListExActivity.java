package com.dvblogic.tvmosaic;

import android.content.Intent;
import android.os.Bundle;

import TVMosaic.TVMosaic.R;
import android.support.v4.content.ContextCompat;
import android.util.Pair;
import android.view.*;
import android.widget.*;
import com.dvblogic.dvblink_common.*;

import java.util.*;


public class RecommendationListExActivity extends BaseActivity implements OnBitmapLoadFinished {

	private static final String TAG = "RecomListEx";

	private static final int PROGRAM_ACTIVITY_REQUEST_CODE = 1;
	private static final String LIKE_THIS_PROFILE_ID = "56b83566-fdbf-4bbb-a786-fffc463fdd01";
	private static final int SECONDS_PER_DAY = 60 * 60 * 24;
	private static final int REQUESTED_COUNT = 50;

	private View root;
	private ListView programs;
	private ToggleButton profilesButton;
	private ToggleButton periodButton;
	private DropdownMenu profilesMenu;
	private DropdownMenu periodsMenu;

	private boolean needUpdateRecommendations = false;

	private ProgramDetails programDetailsPane;

	private BaseActivity _activity;

	private String selectedProfileID = "";
	private String likeThisChannelID = "";
	private long likeThisStartTime = -1;
	private long likeThisEndTime = -1;

	private MappedChannelsResponse mappedChannels = null;
	private ArrayList<ProfileRecommendation> profiles = new ArrayList<>();
	private RecommendationListAdviserProvider adviserProvider;
	private HashMap<String, Channel> channelByIDMap = new HashMap<>();
    private ChannelIDList channels_to_update_ = new ChannelIDList();

	public RecommendationListExActivity() {
		_activity = this;
	}

	private class SearchResultsAdapter extends BaseAdapter {
		private ArrayList<Pair<RecommendationProgram, Channel>> results = new ArrayList<>();
		private LayoutInflater ltInflater;
		private int positionLastShowedProgramInfo = -1;

		private View.OnClickListener resultItemClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showProgramInfo((int) view.getTag(), true);
			}
		};
		private View.OnFocusChangeListener resultItemFocusListener = new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean focus) {
				if (!focus) {
					return;
				}
				showProgramInfo((int) view.getTag(), false);
			}
		};
		private View.OnKeyListener resultItemKeyListener = new View.OnKeyListener() {
			@Override
			public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
				if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && Common.okPressed(keyEvent.getKeyCode())) {
					showProgramInfo((int) view.getTag(), true);
					return true;
				}
				return false;
			}
		};

		private void showProgramInfo(int position, boolean extend) {
			if (position == positionLastShowedProgramInfo && !extend) {
				return;
			}
			positionLastShowedProgramInfo = position;

			Pair<RecommendationProgram, Channel> programChannel = results.get(position);
			RecommendationProgram program = programChannel.first;

			Program pureProgram = convertProgram(program);
			if (programDetailsPane.getVisibility() == View.VISIBLE)
				programDetailsPane.setInfo(pureProgram, programChannel.second.Name, 0, false, false, language_manager_);

			if (extend) {
				startProgramActivity(pureProgram, programChannel.second, program);
			} else {
				setProgramImage(pureProgram);
			}
		}

		private Program convertProgram(RecommendationProgram program) {
			Program pureProgram = new Program();
			pureProgram.ID = program.Event.ID;
			pureProgram.Title = program.Event.Name;
			pureProgram.IsRepeat = program.Event.IsRepeat;
			pureProgram.IsPremiere = program.Event.IsPremiere;
			pureProgram.Subname = program.Event.Subname;
			pureProgram.SeasonNum = (int) program.Event.SeasonNumber;
			pureProgram.EpisodeNum = (int) program.Event.EpisodeNumber;
			pureProgram.StartTime = program.Event.StartTime;
			pureProgram.Duration = (int) program.Event.Duration;
			pureProgram.ShortDesc = program.Event.Description;
			pureProgram.Image = program.Event.Image;
			pureProgram.Directors = program.Event.Directors;
			pureProgram.Writers = program.Event.Writers;
			pureProgram.Actors = program.Event.Actors;
			pureProgram.Language = program.Event.Language;
			pureProgram.Producers = program.Event.Producers;
			pureProgram.Guests = program.Event.Guests;
			pureProgram.IsAction = program.Event.IsAction;
			pureProgram.Year = (int) program.Event.Year;
			pureProgram.IsComedy = program.Event.IsComedy;
			pureProgram.IsDocumentary = program.Event.IsDocumentary;
			pureProgram.IsDrama = program.Event.IsDrama;
			pureProgram.IsEducational = program.Event.IsEducational;
			pureProgram.IsHorror = program.Event.IsHorror;
			pureProgram.IsKids = program.Event.IsKids;
			pureProgram.IsMovie = program.Event.IsMovie;
			pureProgram.IsMusic = program.Event.IsMusic;
			pureProgram.IsNews = program.Event.IsNews;
			pureProgram.IsReality = program.Event.IsReality;
			pureProgram.IsRomance = program.Event.IsRomance;
			pureProgram.IsScifi = program.Event.IsScifi;
			pureProgram.IsSerial = program.Event.IsSerial;
			pureProgram.IsSoap = program.Event.IsSoap;
			pureProgram.IsSpecial = program.Event.IsSpecial;
			pureProgram.IsSports = program.Event.IsSports;
			pureProgram.IsThriller = program.Event.IsThriller;
			pureProgram.IsAdult = program.Event.IsAdult;
			pureProgram.IsSeries = program.Event.IsSeries;
			return pureProgram;
		}

		void startProgramActivity(Program program, Channel channel, RecommendationProgram recommendationProgram) {
			Intent i = new Intent(RecommendationListExActivity.this, ProgramActivity.class);
			i.putExtra("channel_id", channel.ID);
			i.putExtra("long_channel_id", channel.DVBLinkID);
			i.putExtra("channel_name", channel.Name);
			i.putExtra("program_id", program.ID);
			i.putExtra("social_supported", false);
			i.putExtra("enable_create_profile", false);
			i.putExtra("adviser_mode", true);
			i.putExtra("profile_id", selectedProfileID);
			float rating = 0;
			if (recommendationProgram != null) {
				if (recommendationProgram.Rating < 0) {
					rating = -1;
				} else if (recommendationProgram.Rating > 0) {
					rating = 1;
				}
			}
			i.putExtra("rating", rating);
			i.putExtra("score", (recommendationProgram != null) ? recommendationProgram.Score : 0.0f);
			i.putExtra("enable_like_dislike", !selectedProfileID.equals(LIKE_THIS_PROFILE_ID));

			startActivityForResult(i, PROGRAM_ACTIVITY_REQUEST_CODE);
		}

		public SearchResultsAdapter() {
			ltInflater = getLayoutInflater();
		}

		@Override
		public int getCount() {
			return results.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = ltInflater.inflate(R.layout.recommend_ex_item, parent, false);
			}
			Pair<RecommendationProgram, Channel> programChannel = results.get(position);

			TextView label;
			label = (TextView) view.findViewById(R.id.title);
			label.setText(programChannel.first.Event.Name);

            if (!Utils.usesRemoteControl(_context)) {
                String str = ((programChannel.first.Event.Subname == null) ? "" : programChannel.first.Event.Subname);
                String ES = "";
                if (programChannel.first.Event.SeasonNumber != 0)
                    ES += String.format("S%02d", programChannel.first.Event.SeasonNumber);

                if (programChannel.first.Event.EpisodeNumber != 0) {
                    ES += String.format("E%02d", programChannel.first.Event.EpisodeNumber);
                }
                if (str.isEmpty()) {
                    str += ES;
                } else {
                    if (!ES.isEmpty())
                        str += "  |  " + ES;
                }

                label = (TextView) view.findViewById(R.id.subtitle);
                label.setText(str);
                if (!str.isEmpty())
                    label.setVisibility(View.VISIBLE);
                else
                    label.setVisibility(View.GONE);
            }

			label = (TextView) view.findViewById(R.id.start_time);
			label.setText(formatStartTime(programChannel.first.StartTime));

			label = (TextView) view.findViewById(R.id.start_date);
			label.setText(formatStartDate(
					programChannel.first.StartTime, programChannel.first.StartTime + programChannel.first.Event.Duration
			));

			label = (TextView) view.findViewById(R.id.channel);
			label.setText(programChannel.second.Name);

			label = (TextView) view.findViewById(R.id.duration);
			label.setText(formatDuration(programChannel.first.Event.Duration));

			ImageView iv = (ImageView) view.findViewById(R.id.score);
            if (programChannel.first.Score < 2)
			    iv.setImageResource(R.drawable.tvm_icon_advise_3);
            else
            if (programChannel.first.Score >= 2 && programChannel.first.Score < 4)
                iv.setImageResource(R.drawable.tvm_icon_advise_2);
            else
                iv.setImageResource(R.drawable.tvm_icon_advise_1);

			view.setTag(position);
			view.setOnFocusChangeListener(resultItemFocusListener);
			view.setOnClickListener(resultItemClickListener);
			view.setOnKeyListener(resultItemKeyListener);

			return view;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return results.get(position);
		}

		public void addProgram(RecommendationProgram program, Channel channel) {
			results.add(new Pair<>(program, channel));
		}

		public void sort() {
			Collections.sort(results, new Comparator<Pair<RecommendationProgram, Channel>>() {
				@Override
				public int compare(Pair<RecommendationProgram, Channel> o1, Pair<RecommendationProgram, Channel> o2) {
					return (int) (o1.first.StartTime - o2.first.StartTime);
				}
			});
		}

		public void limit(int length) {
			results = new  ArrayList<>(results.subList(0, Math.min(length, results.size())));
		}
	}

	@Override
	public void loadFinished() {}

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		this.show_exit_dialog = false;
		setContentView(R.layout.recommendation_list_ex_activity);

        final int number_of_visible_rows = 5;
        final int row_height  = (int)this.getResources().getDimension(R.dimen.search_item_height);
        final int divider_width = (int)this.getResources().getDimension(R.dimen.search_divider_width);

        final int layout_height = Utils.usesRemoteControl(this) ? (row_height * number_of_visible_rows + divider_width * number_of_visible_rows) : ViewGroup.LayoutParams.MATCH_PARENT;
        final View lv = findViewById(R.id.programs);

        if (!Utils.usesRemoteControl(this)) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) lv.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, R.id.status_line);
            lv.setLayoutParams(params);
        }

        lv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lv.getLayoutParams().height = layout_height;
                lv.requestLayout();
                lv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

		Intent intent = getIntent();
		ChannelsList channels = (ChannelsList) Serializer.deserialize(
				intent.getStringExtra("channels"), ChannelsList.class
		);
		for (Channel channel: channels.list()) {
			channelByIDMap.put(channel.DVBLinkID, channel);
		}
		if (intent.hasExtra("profile_id")) {
			selectedProfileID = intent.getStringExtra("profile_id");
		} else {
			selectedProfileID = LIKE_THIS_PROFILE_ID;
			likeThisChannelID = intent.getStringExtra("like_this_dvblink_channel_id");
			likeThisStartTime = intent.getLongExtra("like_this_start_time", -1);
			likeThisEndTime = intent.getLongExtra("like_this_end_time", -1);
		}

		root = findViewById(R.id.recommendation_list_ex_layout);

		programs = (ListView) root.findViewById(R.id.programs);
		programDetailsPane = (ProgramDetails) root.findViewById(R.id.program_details_pane);
        if (!Utils.usesRemoteControl(this))
            programDetailsPane.setVisibility(View.GONE);

		profilesButton = (ToggleButton) root.findViewById(R.id.profiles_button);
		profilesButton.setText(language_manager_.GetString(LocaleStrings.IDS_PROFILES));
		profilesButton.setTextOn(language_manager_.GetString(LocaleStrings.IDS_PROFILES));
		profilesButton.setTextOff(language_manager_.GetString(LocaleStrings.IDS_PROFILES));
		periodButton = (ToggleButton) root.findViewById(R.id.period_button);
		periodButton.setText(language_manager_.GetString(LocaleStrings.IDS_PERIOD));
		periodButton.setTextOn(language_manager_.GetString(LocaleStrings.IDS_PERIOD));
		periodButton.setTextOff(language_manager_.GetString(LocaleStrings.IDS_PERIOD));
		ArrayList<Pair<String, String>> items = new ArrayList<>();
		items.add(new Pair<>(language_manager_.GetString(LocaleStrings.IDS_ONEDAY), Integer.toString(SECONDS_PER_DAY)));
		items.add(new Pair<>(language_manager_.GetString(LocaleStrings.IDS_TWODAYS), Integer.toString(SECONDS_PER_DAY * 2)));
		items.add(new Pair<>(language_manager_.GetString(LocaleStrings.IDS_FOURDAYS), Integer.toString(SECONDS_PER_DAY * 4)));
		items.add(new Pair<>(language_manager_.GetString(LocaleStrings.IDS_ONEWEEK), Integer.toString(SECONDS_PER_DAY * 7)));
		items.add(new Pair<>(language_manager_.GetString(LocaleStrings.IDS_TWOWEEKS), Integer.toString(SECONDS_PER_DAY * 14)));
		periodsMenu = new DropdownMenu(this, periodButton, items, true);
		periodsMenu.setOnItemToggle(new DropdownMenu.OnItemToggle() {
			@Override
			public void onItemsChanged(ArrayList<String> checkedItems, boolean self) {
				requestCurrentRecommendations();
				periodsMenu.hide();
			}
		});
		periodsMenu.toggleCheckbox(items.get(0).second);

		adviserProvider = new RecommendationListAdviserProvider();

		getWindow().getDecorView().post(new Runnable() {
			public void run() {
				adviserProvider.requestMappedChannels();
			}
		});
	}

	@Override
	public void onBackPressed() {
		Intent intent = getIntent();
		if (needUpdateRecommendations) {
			intent.putExtra("need_update_recommendations", true);
			intent.putExtra("profile_id", selectedProfileID);
		}
        finishWithResult(RESULT_OK, intent);
	}

    protected void finishWithResult(int resultCode, Intent data) {
        if (channels_to_update_.size() > 0) {
            if (data == null)
                data = new Intent();

            data.removeExtra("need_channel_update");

            String channels_xml = Serializer.serialize(channels_to_update_);
            data.putExtra("need_channel_update", channels_xml);
        }
        setResult(resultCode, data);
        finish();
    }

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            String channels_xml = data.getStringExtra("need_channel_update");
            if (channels_xml != null) {
                ChannelIDList channels = (ChannelIDList) Serializer.deserialize(
                        channels_xml,
                        ChannelIDList.class
                );
                for (int i=0; i<channels.size(); i++) {
                    if (!channels_to_update_.list().contains(channels.get(i)))
                        channels_to_update_.add(channels.get(i));
                }
            }
        }

		if (requestCode == PROGRAM_ACTIVITY_REQUEST_CODE) {
			if (resultCode == ProgramActivity.return_play && data != null) {
				finishWithResult(resultCode, data);
			}
			if (data != null && data.hasExtra("need_update_recommendations")) {
				needUpdateRecommendations = true;
				requestCurrentRecommendations();
			}
		}
	}

	private void clearResults() {
		programs.setAdapter(null);
		programDetailsPane.clear();
		setProgramImage(null);
	}

	private void setProgramImage(Program program) {
        if (programDetailsPane.getVisibility() == View.VISIBLE) {
            programDetailsPane.findViewById(R.id.description_image).setVisibility(View.INVISIBLE);
            if (program == null) {
                return;
            }
            boolean loadBitmap = (program.Image != null && !program.Image.isEmpty());
            if (loadBitmap) {
                new com.dvblogic.tvmosaic.BitmapLoader(
                        this, this, program.Image,
                        (ImageView) programDetailsPane.findViewById(R.id.description_image), null
                ).start();
            }
        }
	}

	private String formatStartDate(long startTime, long endTime) {
		long now = Utils.CurrentUtc();
		if (now >= startTime && now < endTime) {
			return language_manager_.GetString(LocaleStrings.IDS_NOW);
		}
		return (Utils.isToday(startTime) ?
				language_manager_.GetString(LocaleStrings.IDS_TODAY) :
				Utils.UtcToMediumDateString(_context, startTime));
	}

	private String formatStartTime(long startTime) {
		return Utils.UtcToShortTimeString(_context, startTime);
	}

	private String formatDuration(long duration) {
		return String.format(Locale.getDefault(), "%d %s",
				duration / 60, language_manager_.GetString(LocaleStrings.IDS_MINUTE)
		);
	}

	private void showResult(RecommendationsList recommendations) {
		if (recommendations.size() < 1) {
			Toast.makeText(
					_activity, language_manager_.GetString(LocaleStrings.IDS_PROGRAM_NOT_FOUND_MSG),
					Toast.LENGTH_SHORT
			).show();
			return;
		}

		SearchResultsAdapter resultsAdapter = new SearchResultsAdapter();
		for (Recommendation resultItem: recommendations.list()) {
			Channel channel = channelByIDMap.get(resultItem.DVBLinkID);
			if (channel == null) {
				continue;
			}
			for (RecommendationProgram program: resultItem.Programs) {
				if (program.Event.Duration < 1) {
					continue;
				}
				resultsAdapter.addProgram(program, channel);
			}
		}
		resultsAdapter.sort();
		resultsAdapter.limit(REQUESTED_COUNT);
		programs.setAdapter(resultsAdapter);
		programs.setItemsCanFocus(true);
		programs.requestFocus();
	}

	private class RecommendationListAdviserProvider extends AdviserProvider {
		public RecommendationListAdviserProvider() {
			super(_activity, serverSettings, language_manager_.getID());
		}

		@Override
		protected void onMappedChannels(MappedChannelsResponse mappedChannels_) {
			if (mappedChannels_.size() > 0) {
				// mappedChannels = mappedChannels_;
				requestProfiles();
			}
		}

		@Override
		protected void onProfiles(ProfilesResponse profilesResponse) {
			if (profiles == null) {
				profiles = new ArrayList<>();
			}
			profiles.clear();

			if (profilesResponse != null) {
				ProfileRecommendation recommendation;
				for (ProviderProfilesList providerProfiles : profilesResponse.list()) {
					for (Profile profile : providerProfiles.list()) {
						recommendation = new ProfileRecommendation();
						recommendation.providerId = providerProfiles.ProviderId;
						recommendation.profile = profile;
						profiles.add(recommendation);
					}
				}
				Collections.sort(profiles, new Comparator<ProfileRecommendation>() {
					@Override
					public int compare(ProfileRecommendation o1, ProfileRecommendation o2) {
						return o1.profile.ProfileName.compareToIgnoreCase(o2.profile.ProfileName);
					}
				});

				createProfilesMenu(true);
			}

			// createMenu();
		}

		@Override
		protected void onRecommendByProfiles(RecommendationsList recommendations) {
			showResult(recommendations);
		}

		@Override
		protected void onLikeThis(RecommendationsList recommendations) {
			showResult(recommendations);
		}
	}

	private void createProfilesMenu(boolean fired) {
		ArrayList<Pair<String, String>> items = new ArrayList<>();
		if (selectedProfileID.equals(LIKE_THIS_PROFILE_ID)) {
			items.add(new Pair<>(language_manager_.GetString(LocaleStrings.IDS_LIKE_THIS), LIKE_THIS_PROFILE_ID));
		}
		for (ProfileRecommendation profile: profiles) {
			items.add(new Pair<>(profile.profile.ProfileName, profile.profile.ProfileId));
		}
		if (profilesMenu == null) {
			profilesMenu = new DropdownMenu(this, profilesButton, items, true);
			profilesMenu.setOnItemToggle(new DropdownMenu.OnItemToggle() {
				@Override
				public void onItemsChanged(ArrayList<String> checkedProfiles, boolean self) {
					if (!self) {
						needUpdateRecommendations = true;
					}
					profilesMenu.hide();
					requestCurrentRecommendations();
				}
			});
		} else {
			profilesMenu.setItems(items);
		}
		profilesMenu.toggleCheckbox(selectedProfileID, fired);
	}

	private void requestCurrentRecommendations() {
		ArrayList<String> checkedProfiles = profilesMenu.getCheckedID();
		ArrayList<String> checkedPeriods = periodsMenu.getCheckedID();
		if (checkedProfiles.size() != 1 || checkedPeriods.size() != 1) {
			return;
		}

		clearResults();

		selectedProfileID = checkedProfiles.get(0);

		long period = Integer.valueOf(checkedPeriods.get(0));
		if (selectedProfileID.equals(LIKE_THIS_PROFILE_ID)) {
			RecommendLikeThis request = new RecommendLikeThis();
			request.DVBLinkChannelId = likeThisChannelID;
			request.StartTime = likeThisStartTime;
			request.EndTime = likeThisEndTime;
			request.RecommendetionsFrom = request.StartTime;
			request.RecommendetionsTo = request.RecommendetionsFrom + period;
			adviserProvider.requestLikeThis(request);
		} else {
			long start = Utils.CurrentUtc();
			long end = start + period;
			adviserProvider.requestRecommendByProfile(selectedProfileID, start, end);
			if (!likeThisChannelID.isEmpty()) {
				likeThisChannelID = "";
				likeThisStartTime = -1;
				likeThisEndTime = -1;
				createProfilesMenu(false);
			}
		}
	}
}