package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import TVMosaic.TVMosaic.R;

import com.dvblogic.dvblink_common.ConciseParamMap;
import com.dvblogic.dvblink_common.EditableParam;
import com.dvblogic.dvblink_common.ParamContainer;
import com.dvblogic.dvblink_common.Serializer;
import com.dvblogic.dvblink_common.ServerSettings;
import com.dvblogic.dvblink_common.ServerVersion;
import com.dvblogic.dvblink_common.ServerVersionRequest;
import com.dvblogic.dvblink_common.ServiceInfo;
import com.dvblogic.dvblink_common.ServiceInfoCollection;
import com.dvblogic.dvblink_common.StatusCode;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class ServerSelectionActivity extends BaseActivity {

    private static final String TAG = "ServerSelectionActivity";
    private ListView serverList;
    ServiceListenerThreadController	serviceListener;
    ArrayList<ServiceInfo> _service_info_collection = new ArrayList<ServiceInfo>();
    private ServerSelectionAdapter serverAdapter;
    Timer timer = null;
    final static int manual_params_activity_req_code      = 100;
    final static int auth_settings_activity = 101;
    ServerSettings previous_settings_ = null;

    private static final String auth_settings_container_id = "5e266697-226a-4fed-8e63-911839080cf7";
    private static final String auth_settings_user_key = "auth_settings_user_key";
    private static final String auth_settings_pswd_key = "auth_settings_pswd_key";

    public class ServerSelectionAdapter extends BaseAdapter
    {
        private LayoutInflater ltInflater;
        private ServerClickListener serverClickListener;

        private class ServerClickListener implements View.OnClickListener {
            @Override
            public void onClick(View view) {

                int childIdx = -1;
                String tag = (String)view.getTag();
                if (tag != null)
                    childIdx = Integer.parseInt(tag);

                if (childIdx != -1) {
                    ServiceInfo si = _service_info_collection.get(childIdx);
                    if (!si.ServiceId.equals(ServerManualParamsActivity.manualServerId)) {
                        ServerSettings ss = new ServerSettings();
                        ss.serverName = si.ServiceName;
                        ss.serverAddress = si.ServiceAdress;
                        ss.serverId = si.ServiceId;
                        ss.serverPort = si.BaseStreamPort;

                        ApplyNewSettings(ss);
                    } else {
                        //start manual server address/port entry
                        Intent intent = new Intent(_context, ServerManualParamsActivity.class);
                        startActivityForResult(intent, manual_params_activity_req_code);
                    }
                }
            }
        }

        ServerSelectionAdapter() {
            ltInflater = getLayoutInflater();
            serverClickListener = new ServerClickListener();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ltInflater.inflate(R.layout.settings_link_layout, parent, false);
            }

            ServiceInfo si = _service_info_collection.get(position);

            TextView label;
            //title
            label = (TextView) view.findViewById(R.id.title_text);
            label.setText(si.ServiceName);
            //description
            label = (TextView) view.findViewById(R.id.desc_text);
            if (!si.ServiceId.equals(ServerManualParamsActivity.manualServerId)) {
                Locale locale = Locale.getDefault();
                label.setText(String.format(locale, "%s:%d", si.ServiceAdress, si.BaseStreamPort));
            } else {
                label.setText(language_manager_.GetString(LocaleStrings.IDS_SERVER_ENTER_MANUAL_PARAMS_DESC));
            }

            view.setTag(String.format("%d", position));
            view.setOnClickListener(serverClickListener);

            return view;
        }

        @Override
        public int getCount() {
            return _service_info_collection == null ? 0 : _service_info_collection.size();
        }

        @Override
        public Object getItem(int position) {
            return _service_info_collection.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.server_selection_activity);

        TextView tv = (TextView)findViewById(R.id.head_bar);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_PAGE_TITLE));

        RelativeLayout rl = (RelativeLayout)findViewById(R.id.server_selection_category);
        tv = (TextView)rl.findViewById(R.id.category_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SERVER_SELECTION));
        tv = (TextView)rl.findViewById(R.id.category_desc);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SERVER_SELECTION_DESC));

        View activity = findViewById(R.id.server_selection_activity);
        serverList = (ListView) activity.findViewById(R.id.server_list);

        //add manual server entry

        ServiceInfo msi = new ServiceInfo();
        msi.ServiceName = language_manager_.GetString(LocaleStrings.IDS_SERVER_ENTER_MANUAL_PARAMS);
        msi.ServiceId = ServerManualParamsActivity.manualServerId;
        _service_info_collection.add(msi);

        serverAdapter = new ServerSelectionAdapter();
        serverList.setAdapter(serverAdapter);
        serverList.setItemsCanFocus(true);

        serviceListener = new ServiceListenerThreadController();

        //reset activity result
        setResult(RESULT_CANCELED, getIntent());

        //save current server settings
        previous_settings_ = settings_.getServerSettings();
    }

    @Override
    protected void onDestroy() {
        if (timer != null) {
            timer.cancel();
            timer.purge();
            timer = null;
        }
        super.onDestroy();
    }

    @Override
    protected void onStart() {
        super.onStart();
        serviceListener.start();

        timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                serviceListener_FoundServersEvent();
            }
        }, 0, 1000);
    }

    @Override
    protected void onPause() {
        super.onPause();
        serviceListener.stop();
        timer.cancel();
    }

    @Override
    protected void onStop() {
        super.onStop();
        serviceListener.stop();
        timer.cancel();
    }

    @Override
    protected void onResume() {
        super.onResume();
        serviceListener.start();

        timer = new Timer();
        timer.schedule(new TimerTask() {
            public void run() {
                serviceListener_FoundServersEvent();
            }
        }, 0, 1000);
    }

    protected boolean contains(ServiceInfo si) {
        boolean ret_val = false;
        for (int i=0; i<_service_info_collection.size(); i++) {
            if (_service_info_collection.get(i).getHash().equals(si.getHash())) {
                ret_val = true;
                break;
            }
        }
        return ret_val;
    }

    public void refreshView() {

        boolean _invalidate = false;
        ArrayList<ServiceInfo> sic = ServiceInfoCollection.Instance().GetServiceInfosAsList();

        for (int i=0; i< sic.size(); i++) {
            if (!contains(sic.get(i))) {
                _service_info_collection.add(sic.get(i));
                _invalidate = true;
            }
        }

        if (_invalidate) {
            serverAdapter.notifyDataSetChanged();
        }
    }

    void serviceListener_FoundServersEvent() {
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                refreshView();
            }
        });
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return true;
        }
        if (Common.backPressed(keyCode, event)) {
            finish();
            return false;
        }
        return false;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == manual_params_activity_req_code && resultCode != RESULT_CANCELED) {

            int port = 0;
            try {
                port = Integer.parseInt(data.getStringExtra("server_port"));
            } catch(NumberFormatException e) {port = 0;}

            if (port != 0) {
                ServerSettings ss = new ServerSettings();
                ss.serverName = data.getStringExtra("server_name");
                ss.serverAddress = data.getStringExtra("server_addr");
                ss.serverId = data.getStringExtra("server_id");
                ss.useHttps = data.getBooleanExtra("use_https", false);
                ss.serverPort = port;

                ApplyNewSettings(ss);
            }

        } else
        if (requestCode == auth_settings_activity) {
            if (resultCode == RESULT_OK) {
                String param_str = data.getExtras().getString("parameters");
                ConciseParamMap parameters = (ConciseParamMap) Serializer.deserialize(param_str, ConciseParamMap.class);

                if (parameters != null) {
                    ServerSettings ss = settings_.getServerSettings();
                    ss.userName = parameters.parameters.get(auth_settings_user_key);
                    ss.password = parameters.parameters.get(auth_settings_pswd_key);
                    ApplyNewSettings(ss);
                } else {
                    settings_.setServerSettings(previous_settings_);
                    ProcessSettingsChange();

                }
            } else {
                settings_.setServerSettings(previous_settings_);
                ProcessSettingsChange();
            }
        }
    }

    protected void ApplyNewSettings(ServerSettings ss) {

        settings_.setServerSettings(ss);
        ProcessSettingsChange();
        //ask for server info from a new server to check if authentication is needed
        activateProgressBar(true);
        data_provider_.GetVersion(new ServerVersionRequest());
    }

    protected void processConnectionSuccess() {
        activateProgressBar(false);

        setResult(RESULT_OK, getIntent());
        finish();
    }

    protected void processGeneralError() {
        activateProgressBar(false);

        AlertDialog.Builder ad = new AlertDialog.Builder(_context)
                .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                .setMessage(language_manager_.GetString(LocaleStrings.IDS_SERVER_CHECK_ERROR))
                .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                setResult(RESULT_OK, getIntent());
                                finish();
                            }
                        })
                .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (previous_settings_ != null) {
                                    settings_.setServerSettings(previous_settings_);
                                    ProcessSettingsChange();
                                }
                            }
                        });

        ad.show();
    }

    protected void processAuthenticationError() {
        activateProgressBar(false);

        Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_INCORRECT_LOGIN_INFO_MSG), Toast.LENGTH_LONG).show();

        //launch source selector
        ParamContainer auth_settings_container = new ParamContainer();
        auth_settings_container.id = auth_settings_container_id;
        auth_settings_container.name = language_manager_.GetString(LocaleStrings.IDS_SERVER_AUTH_SETTINGS);
        auth_settings_container.desc = "";

        EditableParam ep_user = new EditableParam();
        ep_user.key = auth_settings_user_key;
        ep_user.name = language_manager_.GetString(LocaleStrings.IDS_SERVER_AUTH_SETTINGS_USER);
        ep_user.value = "";
        ep_user.format = EditableParam.EDITABLE_FORMAT_STRING;
        auth_settings_container.editable_params.add(ep_user);

        EditableParam ep_pswd = new EditableParam();
        ep_pswd.key = auth_settings_pswd_key;
        ep_pswd.name = language_manager_.GetString(LocaleStrings.IDS_SERVER_AUTH_SETTINGS_PSWD);
        ep_pswd.value = "";
        ep_pswd.format = EditableParam.EDITABLE_FORMAT_STRING;
        auth_settings_container.editable_params.add(ep_pswd);

        Intent intent = new Intent(_context, ParamContainerActivity.class);
        String param = Serializer.serialize(auth_settings_container);
        intent.putExtra("container", param);
        startActivityForResult(intent, auth_settings_activity);
    }

    @Override
    public void DataProcessing(String command, Object result_data) {

//        if (result_data instanceof ServerVersion)
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                processConnectionSuccess();
            }
        });
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        if (error == StatusCode.STATUS_UNAUTHORISED) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    processAuthenticationError();
                }
            });
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    processGeneralError();
                }
            });
        }
    }

}
