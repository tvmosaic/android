package com.dvblogic.tvmosaic;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.lang.String;
import java.util.HashMap;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import com.thoughtworks.xstream.annotations.XStreamAsAttribute;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

@XStreamAlias("language")
public class LanguageInfo
{
	private static final String		LANGUAGE_ROOT_NODE	= "language";
	private static final String		ID_ATTRIBUTE		= "id";
	private static final String		NAME_ATTRIBUTE		= "name";
	private static final String		VALUE_ATTRIBUTE		= "value";

	@XStreamAsAttribute
	@XStreamAlias("id")
	public String					ID;

	@XStreamAsAttribute
	@XStreamAlias("name")
	public String					Name;

	protected transient String		path;

	public boolean					IsValid;
	public HashMap<String, String>	Strings;

	public LanguageInfo() {
		ID = "";
		Name = "";
		IsValid = false;
		Strings = new HashMap<String, String>();
	}

	public String getPath() {
		return path;
	}

	public void setPath(String _path) {
		path = _path;
	}

	/*
	 * public LanguageInfo(InputStream xml_file) { IsValid = false; Strings =
	 * new HashMap<String, String>();
	 * 
	 * XmlFriendlyReader reader = (XmlReader) XmlReader.createReader(xml_file);
	 * 
	 * try { reader.MoveToContent();
	 * 
	 * //this is "language element" if (LANGUAGE_ROOT_NODE == reader.Name) { ID
	 * = reader.GetAttribute(ID_ATTRIBUTE); Name =
	 * reader.GetAttribute(NAME_ATTRIBUTE); IsValid = true;
	 * 
	 * //skip to strings reader.Read(); reader.MoveToContent();
	 * 
	 * while (!reader.EOF) { if (reader.IsStartElement()) { if
	 * (reader.IsEmptyElement) { String value =
	 * reader.GetAttribute(VALUE_ATTRIBUTE); if (null != value) {
	 * Strings.put(key, value)[reader.Name] = value; } } } reader.Read(); } } }
	 * catch (Exception e) { System.out.println(e.getMessage()); }
	 * 
	 * }
	 */

	public LanguageInfo(InputStream xml_file, String language_path) {
		IsValid = false;
		if (xml_file == null)
			return;

		Strings = new HashMap<String, String>();
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser;
			saxParser = factory.newSAXParser();
			Reader reader = new InputStreamReader(xml_file, "UTF-8");
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
			LanguageInfoHandler handler = new LanguageInfoHandler(this);
			saxParser.parse(is, handler);
			path = language_path;
			IsValid = true;

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public LanguageInfo(String language_path) {
		IsValid = false;
		Strings = new HashMap<String, String>();
		try {
			InputStream xml_stream = null;
			;
			xml_stream = new FileInputStream(language_path);
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser;
			saxParser = factory.newSAXParser();
			Reader reader = new InputStreamReader(xml_stream, "UTF-8");
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
			LanguageInfoHandler handler = new LanguageInfoHandler(this);
			saxParser.parse(is, handler);
			path = language_path;
			IsValid = true;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			// System.out.format("OpenLanguageFile() exception%n");
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	protected class LanguageInfoHandler extends DefaultHandler
	{
		private LanguageInfo	info;

		public LanguageInfoHandler(LanguageInfo _info) {
			info = _info;
		}

		public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
			// System.out.println("Start: " + qName);
			if (qName.equals(LANGUAGE_ROOT_NODE)) {
				info.ID = attributes.getValue(ID_ATTRIBUTE);
				info.Name = attributes.getValue(NAME_ATTRIBUTE);
				info.IsValid = true;
			} else {
				Strings.put(qName, attributes.getValue(VALUE_ATTRIBUTE));
			}
		}

		public void endElement(String uri, String localName, String qName) throws SAXException {
		}
	}
}
