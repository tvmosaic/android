package com.dvblogic.tvmosaic;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicBlur;
import android.view.View;
import android.widget.ImageView;

import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import TVMosaic.TVMosaic.R;

/**
 * Created by Serg on 12-Apr-16.
 */
public class BitmapLoader extends Thread
{
    Activity activity_;
    String url_;
    ImageView thumb_view_;
    ImageView background_view_;
    Bitmap image_;
    OnBitmapLoadFinished finisher_;

    public BitmapLoader(Activity activity, OnBitmapLoadFinished finisher, String url, ImageView thumb_view, ImageView background_view) {
        activity_ = activity;
        finisher_ = finisher;
        url_ = url;
        thumb_view_ = thumb_view;
        background_view_ = background_view;
    }

    @Override
    public void run() {
        try {
            URL ulrn = new URL(url_);
            URLConnection uc = ulrn.openConnection();
            uc.setDoInput(true);
            if (uc != null) {
                InputStream is = uc.getInputStream();
                if (is != null) {
                    image_ = BitmapFactory.decodeStream(is);
                }
            }
        } catch (Exception e) {
            // System.out.println(e.getMessage());
            e.printStackTrace();
        }
        if (image_ != null)
            updateImage();
    }

    protected void updateImage() {
        activity_.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                updateImage(activity_, image_, thumb_view_, background_view_);
                if (finisher_ != null)
                    finisher_.loadFinished();
            }
        });
    }

    static public void updateImage(Activity _activity, Bitmap _image, ImageView _thumb_view, ImageView _background_view) {

        final Activity activity = _activity;
        final Bitmap image = _image;
        final ImageView thumb_view = _thumb_view;
        final ImageView background_view = _background_view;

        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Bitmap bmp = image;
                int wd = image.getWidth();
                int ht = image.getHeight();
                if (wd > 2048 || ht > 2048) {
                    double aspect = (double)wd / ht;
                    if (wd > ht) {
                        wd = 2048;
                        ht = (int)(wd / aspect);
                    } else {
                        ht = 2048;
                        wd = (int)(ht * aspect);
                    }
                    bmp = Bitmap.createScaledBitmap(image, wd, ht, true);
                }
                thumb_view.setImageBitmap(bmp);

                Point pt = new Point();
                activity.getWindowManager().getDefaultDisplay().getSize(pt);
                double rw = (pt.x / 3.0) / image.getWidth();
                if (rw < 1) {
                    thumb_view.setAdjustViewBounds(true);
                    thumb_view.setMaxWidth(pt.x / 3);
                }
                thumb_view.setVisibility(View.VISIBLE);

                if (background_view != null) {
                    if (wd > pt.x/3 || ht > pt.y/3) {
                        double aspect = (double)wd / ht;
                        if (wd > ht) {
                            wd = pt.x / 3;
                            ht = (int)(wd / aspect);
                        } else {
                            ht = pt.y / 3;
                            wd = (int)(ht * aspect);
                        }
                    } else {
                        wd = wd / 3;
                        ht = ht / 3;
                    }
                    Bitmap newBmp = Bitmap.createScaledBitmap(image, wd, ht, true);
                    blur(activity, newBmp);
                    background_view.setImageBitmap(newBmp);
                }
            }
        });
    }

    static public void blur(Activity activity, Bitmap bitmapOriginal) {
        RenderScript rs = RenderScript.create(activity);

        final Allocation input = Allocation.createFromBitmap(rs, bitmapOriginal); //use this constructor for best performance, because it uses USAGE_SHARED mode which reuses memory
        final Allocation output = Allocation.createTyped(rs, input.getType());
        final ScriptIntrinsicBlur script = ScriptIntrinsicBlur.create(rs, Element.U8_4(rs));
        script.setRadius(16f);
        script.setInput(input);
        script.forEach(output);
        output.copyTo(bitmapOriginal);
    }
}

