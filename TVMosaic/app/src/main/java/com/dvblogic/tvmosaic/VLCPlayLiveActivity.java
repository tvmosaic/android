package com.dvblogic.tvmosaic;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.dvblogic.dvblink_common.*;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;
import org.videolan.libvlc.MediaPlayer;

import java.util.*;

import TVMosaic.TVMosaic.R;

public class VLCPlayLiveActivity extends BaseActivity implements IVLCVout.OnNewVideoLayoutListener {
    private static final String TAG = "Player";
    private int VISIBLE_CHANNELS_COUNT = 5;
    private static final long HIDE_PANEL_DELAY = 5000;
    private static final long UPDATE_LIVE_CHANNEL_INFO_DELAY = 5000;
    private static final int INFO_PANEL_ALPHA_ENABLED = 255;
    private static final int INFO_PANEL_ALPHA_DISABLED = 50;
    private static final int REWIND_DURATION_SECONDS = -15;
    private static final int FORWARD_DURATION_SECONDS = 30;
    private static final int TIME_SHIFT_STATS_TIMER_INTERVAL = 1100;
    private static final int SEEK_MAX_VALUE = 1000;
    private static final int MOVE_THROTTLE_TIME = 650;

    private class ItemAdapter extends BaseAdapter {
        boolean _audio_adapter = true;

        private ItemAdapter(boolean audio_adapter) {
            super();
            _audio_adapter = audio_adapter;
        }

        @SuppressWarnings("unchecked")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            try {
                if (row == null) {
                    row = LinearLayout.inflate(_context, R.layout.player_menu_row, null);
                }
                MediaPlayer.TrackDescription track = (MediaPlayer.TrackDescription) getItem(position);
                if (track == null)
                    return row;

                String text = track.name;
                TextView label = (TextView) row.findViewById(R.id.vlc_menu_row_text);
                label.setText(text);

                ImageView icon = (ImageView) row.findViewById(R.id.vlc_menu_row_icon);
                if (_audio_adapter) {
                    icon.setImageResource(
                            selected_audio == position ?
                                    R.drawable.tvm_checkbox_checked_icon_active :
                                    R.drawable.tvm_checkbox_unchecked_icon_active
                    );
                } else {
                    icon.setImageResource(
                            selected_sub == position ?
                                    R.drawable.tvm_checkbox_checked_icon_active :
                                    R.drawable.tvm_checkbox_unchecked_icon_active
                    );
                }
            } catch (Exception e) {
                Log.e(TAG, "unknown", e);
            }
            return row;
        }

        @Override
        public int getCount() {
            if (_audio_adapter)
                return (mAudioTracks == null ? 0 : mAudioTracks.length);
            else
                return (mSubTracks == null ? 0 : mSubTracks.length);
        }

        @Override
        public Object getItem(int position) {
            if (_audio_adapter)
                return (mAudioTracks == null ? null : mAudioTracks[position]);
            else
                return (mSubTracks == null ? null : mSubTracks[position]);
        }

        @SuppressWarnings("unchecked")
        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    private class HidePanelTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hidePanels();
                }
            });

        }
    }

    private class UpdateLiveChannelInfoTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    showCurrentProgram(channel_for_panel);
                    updateCurrentTime();
                }
            });
        }
    }

    private static int stream_invalid_idx = -1;
    private static int stream_disabled_id = -1;

    private final int state_ready = 0;
    private final int state_exiting = 1;
    private final int state_stop_play = 2;
    private final int state_direct_url = 3;
    private final int state_timeshift_url = 4;
    private final int state_get_stats = 5;
    private final int state_seek = 6;
    private final int state_server_caps = 7;

    private GuideActivity parent_activity = GuideActivity.getInstance();
    private MediaPlayer mMediaPlayer = null;
    private Media mMedia = null;
    private LibVLC libvlc = null;
    private SurfaceView mSurfaceView = null;
    private SurfaceView mSurfaceSubtitles = null;
    private FrameLayout mVideoSurfaceFrame = null;
    private View.OnLayoutChangeListener mOnLayoutChangeListener = null;
    private final Handler mHandler = new Handler();
    private MediaPlayer.TrackDescription[] mAudioTracks = null;
    private MediaPlayer.TrackDescription[] mSubTracks = null;
    private int selected_audio = stream_invalid_idx;
    private int selected_sub = stream_invalid_idx;
    private boolean has_to_initialize_ = false;

    private Boolean _started = false;
    private ChannelsList channels = null;
    private int channel_for_panel = 0;
    private int preselected_channel_for_panel = -1;
    private int state_ = state_ready;
    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener;
    private ComponentName mediaButtonReceiver;
    private AudioManager audioManager;

    private RecyclerView channelsRecyclerView;
    private PlayerChannelsRecyclerViewAdapter _adapter;
    private TextSwitcher programName;
    private TextSwitcher programTime;
    private Timer hideInfoPanelTimer;
    private Timer updateLiveChannelInfoTimer;
    private LinearLayout infoPanel;
    private int currentChannelPlay;
    private TextView currentTime;
    private ProgressBar programProgress;
    private CustomSeekBar programSeek;

    private GridView subtitleList;
    private GridView audioTrackList;
    private LinearLayout paramPanel;

    private boolean infoPanelShowing = false;
    private boolean infoPanelHiding = false;

    private boolean paramPanelShowing = false;
    private boolean paramPanelHiding = false;

    private HashMap<String, Program> programsEx;

    private int mVideoHeight = 0;
    private int mVideoWidth = 0;
    private int mVideoVisibleHeight = 0;
    private int mVideoVisibleWidth = 0;
    private int mVideoSarNum = 0;
    private int mVideoSarDen = 0;

    private String dvblink_channel_id_ = null;
    private long play_channel_handle_ = -1;
    private boolean time_shifted_playback_ = false;
    private Timer getStatsTimer;
    private TimeShiftStatusResponse timeShiftStatus = new TimeShiftStatusResponse();

    private Timer moveThrottleTimer;

    private Queue<Pair<Integer, Object>> queue = new LinkedList<>();
    private boolean ignoreWindowFocusChange = false;
    private Toast toast;

    private void startHideInfoPanelTimer() {

        if (!Utils.usesRemoteControl(this))
            return;

        stopHideInfoPanelTimer();
        hideInfoPanelTimer = new Timer();
        hideInfoPanelTimer.schedule(
                new HidePanelTimerTask(), HIDE_PANEL_DELAY
        );
    }

    private void stopHideInfoPanelTimer() {

        if (!Utils.usesRemoteControl(this))
            return;

        if (hideInfoPanelTimer != null) {
            hideInfoPanelTimer.cancel();
        }
    }

    private void startUpdateLiveChannelInfoTimer() {
        stopUpdateLiveChannelInfoTimer();
        updateLiveChannelInfoTimer = new Timer();
        updateLiveChannelInfoTimer.schedule(
                new UpdateLiveChannelInfoTimerTask(),
                UPDATE_LIVE_CHANNEL_INFO_DELAY,
                UPDATE_LIVE_CHANNEL_INFO_DELAY
        );
    }

    private void stopUpdateLiveChannelInfoTimer() {
        if (updateLiveChannelInfoTimer != null) {
            updateLiveChannelInfoTimer.cancel();
        }
        updateLiveChannelInfoTimer = null;
    }

    private void showInfoPanel() {
        if (!infoPanelHidden() || infoPanelShowing) {
            return;
        }
        infoPanelShowing = true;
        preselected_channel_for_panel = -1;
        startHideInfoPanelTimer();
        _adapter.selectItem(channel_for_panel);
        showCurrentProgram(channel_for_panel);
        updateCurrentTime();
        if (time_shifted_playback_) {
            startStatsTimer();
        }
        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_in_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                infoPanel.setVisibility(View.VISIBLE);
                infoPanelShowing = false;
                if (time_shifted_playback_) {
                    programSeek.requestFocus();
                }
            }
        });
        infoPanel.startAnimation(animation);
        startUpdateLiveChannelInfoTimer();
        hideToast();
    }

    private void hideInfoPanel() {
        if (infoPanelHidden() || infoPanelHiding) {
            return;
        }
        infoPanelHiding = true;
        stopHideInfoPanelTimer();
        stopUpdateLiveChannelInfoTimer();
        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_out_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                infoPanel.setVisibility(View.INVISIBLE);
                channel_for_panel = currentChannelPlay;
                infoPanelHiding = false;
            }
        });
        infoPanel.startAnimation(animation);
        if (time_shifted_playback_) {
            stopStatsTimer();
        }
    }

    private boolean infoPanelHidden() {
        return infoPanel.getVisibility() == View.INVISIBLE;
    }

    private boolean paramPanelHidden() {
        return paramPanel.getVisibility() != View.VISIBLE;
    }

    private void showParamPanel() {

        if (!paramPanelHidden() || paramPanelShowing) {
            return;
        }
        paramPanelShowing = true;

        stopHideInfoPanelTimer();
        paramPanel.setVisibility(View.VISIBLE);

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_in_top);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                paramPanelShowing = false;
                paramPanel.requestFocus();
            }
        });
        paramPanel.startAnimation(animation);
    }

    private void hideParamPanel() {

        if (paramPanelHidden() || paramPanelHiding) {
            return;
        }
        paramPanelHiding = true;

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_out_top);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                paramPanel.setVisibility(View.GONE);
                paramPanelHiding = false;
            }
        });
        paramPanel.startAnimation(animation);
    }

    private void enableInfoPanel(boolean enabled) {
        enableInfoPanel(enabled, enabled);
    }

    private void enableInfoPanel(boolean enabled, boolean enabledTimeElement) {
        int alpha = enabled ? INFO_PANEL_ALPHA_ENABLED : INFO_PANEL_ALPHA_DISABLED;

        TextView textView;
        textView = (TextView) programName.getChildAt(0);
        textView.setTextColor(textView.getTextColors().withAlpha(alpha));
        textView = (TextView) programName.getChildAt(1);
        textView.setTextColor(textView.getTextColors().withAlpha(alpha));
        textView = (TextView) programTime.getChildAt(0);
        textView.setTextColor(textView.getTextColors().withAlpha(alpha));
        textView = (TextView) programTime.getChildAt(1);
        textView.setTextColor(textView.getTextColors().withAlpha(alpha));

        int alphaCurrentTimeElement = enabledTimeElement ? INFO_PANEL_ALPHA_ENABLED : INFO_PANEL_ALPHA_DISABLED;
        currentTime.setTextColor(currentTime.getTextColors().withAlpha(alphaCurrentTimeElement));

        channelsRecyclerView.post(new Runnable() {
            boolean enabled_;
            @Override
            public void run() {
                _adapter.setEnabled(enabled_);
            }
            public Runnable init(boolean enabled) {
                this.enabled_ = enabled;
                return(this);
            }
        }.init(enabled));

    }

    private void showCurrentProgram(int channelIndex) {
        Channel ch = channels.get(channelIndex);
        prepareProgramsEx(parent_activity.extractCurrentProgramsEx());
        if (programsEx.containsKey(ch.ID)) {
            Program prg = programsEx.get(ch.ID);
            if (!((TextView) programName.getCurrentView()).getText().toString().equals(prg.Title)) {
                programName.setText(prg.Title);
            }

            String time = String.format("%s - %s",
                    Utils.UtcToShortTimeString(_context, prg.StartTime),
                    Utils.UtcToShortTimeString(_context, prg.StartTime + prg.Duration));

            if (!((TextView) programTime.getCurrentView()).getText().toString().equals(time)) {
                programTime.setText(time);
            }

            programProgress.setMax(prg.Duration);
            programProgress.setProgress((int) (Utils.CurrentUtc() - prg.StartTime));
        } else {
            String no_program_text = language_manager_.GetString(LocaleStrings.IDS_NO_PROGINFO_AVAILABLE);

            if (!((TextView) programName.getCurrentView()).getText().toString().equals(no_program_text))
                programName.setText(no_program_text);

            programTime.setText("");
            programProgress.setMax(Integer.MAX_VALUE);
            programProgress.setProgress(Integer.MIN_VALUE);
        }
    }

    private void updateCurrentTime() {
        String time = getCurrentTime();
        currentTime.setText(time);
    }

    private String getCurrentTime() {
        long thumbTime = getThumbTime();
        if (!time_shifted_playback_ || thumbTime < 0) {
            thumbTime = Utils.CurrentUtc();
        }
        return Utils.UtcToShortTimeString(_context, thumbTime);
    }

    private void prepareInputParams() {
        try {

            dvblink_channel_id_ = getIntent().getStringExtra("dvblink_channel_id");

            channels = new ChannelsList();
            for (int idx = 0; idx < parent_activity.channel_container.getChildCount(); idx++) {
                ChannelItem item = (ChannelItem) parent_activity.channel_container.getChildAt(idx);
                if (item.getVisibility() != View.VISIBLE)
                    continue;
                channels.add(item.channel_);
                if (item.channel_.DVBLinkID.equals(dvblink_channel_id_))
                    channel_for_panel = channels.size() - 1;
            }
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    // #ToDo Control audio volume
    /*
    // Done By @Dima
    AudioManager audManager; //= (AudioManager) getSystemService(AUDIO_SERVICE);
    int volumeVal; // = audManager.getStreamVolume(AudioManager.STREAM_MUSIC);
    */

    private void preparePlayerViews() {
        mVideoSurfaceFrame = (FrameLayout) findViewById(R.id.video_frame);

        mSurfaceView = (SurfaceView) findViewById(R.id.surface_view);
        mSurfaceSubtitles = (SurfaceView) findViewById(R.id.surface_subtitles_view);

        mSurfaceSubtitles.setZOrderMediaOverlay(true);
        mSurfaceSubtitles.getHolder().setFormat(PixelFormat.TRANSLUCENT);


        mSurfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(final View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    boolean paramAreaTouch = event.getY() < (v.getHeight() / 2);
                    boolean infoAreaTouch = event.getY() > (v.getHeight() / 2);

                    /*
                    // Manipulation with Volume Up/Down clicking on VolumeTouchAreas
                    // to catch touch from left/right border of view

                      boolean paramAreaTouch = event.getY() < (v.getHeight() / 3);
                    boolean infoAreaTouch = event.getY() > (v.getHeight() - (v.getHeight() / 3));
                    audManager = (AudioManager) getSystemService(AUDIO_SERVICE);
                    volumeVal = audManager.getStreamVolume(AudioManager.STREAM_MUSIC);

                    boolean volumeDownAreaTouch = event.getY() > v.getHeight() / 3
                            && event.getY() < (v.getHeight() - v.getHeight() / 3)
                            && event.getX() < v.getWidth() / 5;

                    boolean volumeUpAreaTouch = event.getY() > v.getHeight() / 3
                            && event.getY() < (v.getHeight() - v.getHeight() / 3)
                            && event.getX() > (v.getWidth() - v.getWidth() / 5);

                    if (volumeUpAreaTouch)
                        volumeUp(volumeVal);

                    if (volumeDownAreaTouch)
                        volumeDown(volumeVal);
*/
                    if (paramAreaTouch) {
                        if (!infoPanelHidden()) {
                            hideInfoPanel();
                        }
                        if (paramPanelHidden() && infoPanelHidden()) {
                            showParamPanel();
                        }
                    }

                    if (infoAreaTouch) {
                        if (!paramPanelHidden()) {
                            hideParamPanel();
                        }
                        if (infoPanelHidden() && paramPanelHidden()) {
                            showInfoPanel();
                        }
                    }
                }
                return true;
            }
        });

    }

/*
//volumeUp/volumeDown methods
    private void volumeUp(int volValue) {
        Animation volumeBtnShow = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.volume_btn_show);
        final Animation volumeBtnHide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.volume_btn_hide);
        final Button btn_volUp = (Button) findViewById(R.id.btn_volUp);

        Timer btnVolGoneTimer = new Timer();
        if (volValue < 15) {
            volValue += 1;
        } else {
            volValue = 15;
        }

        btn_volUp.setText(String.valueOf(volValue));
        btn_volUp.setVisibility(View.VISIBLE);
        btn_volUp.startAnimation(volumeBtnShow);
        btnVolGoneTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btn_volUp.startAnimation(volumeBtnHide);
                        btn_volUp.setVisibility(View.GONE);
                    }
                });
            }
        }, 500);
        audManager.setStreamVolume(AudioManager.STREAM_MUSIC,volValue,0);
    }

    private void volumeDown(int volValue) {

        Animation volumeBtnShow = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.volume_btn_show);
        final Animation volumeBtnHide = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.volume_btn_hide);
        final Button btn_volDown = (Button) findViewById(R.id.btn_volDown);

        if (volValue > 0) {
            volValue -= 1;
        } else {
            volValue = 0;
        }
        Timer btnVolGoneTimer = new Timer();

        btn_volDown.setText(String.valueOf(volValue));
        btn_volDown.setVisibility(View.VISIBLE);
        btn_volDown.startAnimation(volumeBtnShow);
        btnVolGoneTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        btn_volDown.startAnimation(volumeBtnHide);
                        btn_volDown.setVisibility(View.GONE);
                    }
                });
            }
        }, 500);
        audManager.setStreamVolume(AudioManager.STREAM_MUSIC,volValue,0);
    }*/

    private void prepareInfoPanel() {
        channelsRecyclerView = (RecyclerView) findViewById(R.id.channel_list);
        channelsRecyclerView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                enableInfoPanel(focus);
                if (focus) {
                    hideParamPanel();
                    startHideInfoPanelTimer();
                }
            }
        });

        _adapter = new PlayerChannelsRecyclerViewAdapter(_context, channels, VISIBLE_CHANNELS_COUNT);
        channelsRecyclerView.setAdapter(_adapter);
        channelsRecyclerView.setScrollingTouchSlop(RecyclerView.TOUCH_SLOP_PAGING);
        _adapter.selectItem(channel_for_panel);
        _adapter.setSelectEventListener(new PlayerChannelsRecyclerViewAdapter.OnSelectEventListener() {

            @Override
            public void onSelect(int index) {
                channel_for_panel = index;
                showCurrentProgram(channel_for_panel);
            }

            @Override
            public void onTouchSelect(int index) {
                //this function is called only for touch-enabled devices
                if (!Utils.usesRemoteControl(_context)) {
                    channel_for_panel = index;
                    showCurrentProgram(channel_for_panel);
                    //if this is the second touch on this channel - change channel
                    if (preselected_channel_for_panel == channel_for_panel) {
                        changePlayChannel();
                    } else {
                        preselected_channel_for_panel = channel_for_panel;
                    }
                }
            }


        });
        currentChannelPlay = channel_for_panel;

        LayoutInflater inflater = getLayoutInflater();

        programName = (TextSwitcher) findViewById(R.id.program_name);
        programName.addView(inflater.inflate(R.layout.player_program_text, null, false));
        programName.addView(inflater.inflate(R.layout.player_program_text, null, false));

        programTime = (TextSwitcher) findViewById(R.id.program_time);
        programTime.addView(inflater.inflate(R.layout.player_program_text, null, false));
        programTime.addView(inflater.inflate(R.layout.player_program_text, null, false));

        updateCurrentTime();

        infoPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!paramPanelHidden()) {
                    hideParamPanel();
                    infoPanel.requestFocus();
                }
            }
        });
    }

    private void setRefreshRate() {
        try {
            float _refresh_rate = settings_.getValue(Settings.REFRESH_RATE_KEY, Settings.REFRESH_RATE_DEFAULT);
            if (_refresh_rate > 0) {
                Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
                WindowManager.LayoutParams params = getWindow().getAttributes();
                if (Build.VERSION.SDK_INT >= 23) {
                    Display.Mode[] modes = display.getSupportedModes();
                    for (int idx = 0; idx < modes.length; idx++) {
                        Display.Mode mode = modes[idx];
                        if (mode.getRefreshRate() == _refresh_rate) {
                            params.preferredDisplayModeId = mode.getModeId();
                            getWindow().setAttributes(params);
                        }
                    }
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    params.preferredRefreshRate = _refresh_rate;
                    getWindow().setAttributes(params);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    private void prepareProgramsEx(HashMap<String, Program> programs) {
        programsEx = programs;
    }

    protected int get_idx_for_id(MediaPlayer.TrackDescription[] tracks, int id) {
        int ret_val = stream_invalid_idx;

        if (tracks != null) {
            for (int i = 0; i < tracks.length; i++) {
                if (tracks[i].id == id) {
                    ret_val = i;
                    break;
                }
            }
        }

        return ret_val;
    }

    protected int get_first_enabled_track_idx(MediaPlayer.TrackDescription[] tracks) {
        int ret_val = stream_invalid_idx;

        if (tracks != null) {
            for (int i = 0; i < tracks.length; i++) {
                if (tracks[i].id != stream_disabled_id) {
                    ret_val = i;
                    break;
                }
            }
        }

        return ret_val;
    }

    private void fillMenu() {
        mAudioTracks = mMediaPlayer.getAudioTracks();
        if (selected_audio == stream_invalid_idx) {
            selected_audio = get_first_enabled_track_idx(mAudioTracks);
            if (selected_audio != -1)
                mMediaPlayer.setAudioTrack(mAudioTracks[selected_audio].id);
        } else {
            if (mAudioTracks != null)
                mMediaPlayer.setAudioTrack(mAudioTracks[selected_audio].id);
        }

        mSubTracks = mMediaPlayer.getSpuTracks();
        if (selected_sub == stream_invalid_idx) {
            selected_sub = get_idx_for_id(mSubTracks, stream_disabled_id);
            if (selected_sub != -1)
                mMediaPlayer.setSpuTrack(mSubTracks[selected_sub].id);
        } else {
            if (mSubTracks != null)
                mMediaPlayer.setSpuTrack(mSubTracks[selected_sub].id);
        }

        if (mSubTracks != null) {
            subtitleList.setAdapter(new ItemAdapter(false));
            subtitleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_sub = position;
                    mMediaPlayer.setSpuTrack(mSubTracks[position].id);
                    subtitleList.invalidateViews();
                }
            });
        }

        if (mAudioTracks != null) {
            audioTrackList.setAdapter(new ItemAdapter(true));
            audioTrackList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_audio = position;
                    mMediaPlayer.setAudioTrack(mAudioTracks[position].id);
                    audioTrackList.invalidateViews();
                }
            });
            audioTrackList.setSelection(selected_audio);
        }
    }

    private void rewind() {
        move(REWIND_DURATION_SECONDS);
    }

    private void forward() {
        move(FORWARD_DURATION_SECONDS);
    }

    private void move(int duration) {
        stopStatsTimer(); // Останавка обновления стат. информации c сервера (меняет cur_pos_sec).

        if (moveThrottleTimer != null) {
            moveThrottleTimer.cancel();
            moveThrottleTimer = null;
        }

        long position = 0;
        if (timeShiftStatus.buffer_duration > 0) {
            position = Math.min(
                    Math.max(timeShiftStatus.cur_pos_sec + duration, 0),
                    timeShiftStatus.buffer_duration
            );
        }

        timeShiftStatus.cur_pos_sec = position;
        updateCurrentTime();
        programSeek.setProgress(calcPerN(position, timeShiftStatus.buffer_duration, SEEK_MAX_VALUE));

        moveThrottleTimer = new Timer();
        moveThrottleTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        queue.add(new Pair<Integer, Object>(state_seek, timeShiftStatus.cur_pos_sec));
                        startStatsTimer();
                    }
                });
            }
        }, MOVE_THROTTLE_TIME);
    }

    private int calcPerN(long current, long max, long n) {
        return (int)((float) current / max * n);
    }

    private long getThumbTime() {
        if (timeShiftStatus == null) {
            return -1;
        }
        return getThumbTime(timeShiftStatus.cur_pos_sec);
    }

    private long getThumbTime(long pos) {
        return Utils.CurrentUtc() - timeShiftStatus.buffer_duration + pos;
    }

    private void changeChannel(int inc) {
        channel_for_panel += inc;
        if (channel_for_panel < 0) {
            channel_for_panel = channels.size() - 1;
        } else if (channel_for_panel >= channels.size()) {
            channel_for_panel = 0;
        }
        ignoreWindowFocusChange = true;

        Channel channel = channels.get(channel_for_panel);
        makeToast(channel.Name);

        changePlayChannel();
    }

    // region VLC Player
    protected void createPlayer() {

        // Create LibVLC
        ArrayList<String> options = new ArrayList<>();
        options.add("-vv"); // verbosity

        int deinterlace = settings_.getValue(Settings.DEINTERLACE_MODE_KEY, Settings.DEINTERLACE_MODE_DEFAULT);

        if (deinterlace == 1) {
            options.add("--video-filter");
            options.add("deinterlace");

//            options.add("--deinterlace-mode");
//            String algo = settings_.getValue("deinterlacing_algo", "linear");
//            options.add(algo);

            options.add("--deinterlace");
            options.add("-1");
        } else if (deinterlace == 0) {
            options.add("--deinterlace");
            options.add("0");
        }

        libvlc = new LibVLC(this, options);

        // Create media player
        mMediaPlayer = new MediaPlayer(libvlc);
        mMediaPlayer.setAudioOutput("android_audiotrack");

        boolean audio_passthrough = settings_.getValue(Settings.AUDIO_PASSTHROUGH_KEY, Settings.AUDIO_PASSTHROUGH_DEFAULT);
        if (audio_passthrough) {
            mMediaPlayer.setAudioOutputDevice("hdmi");
        } else {
            mMediaPlayer.setAudioOutputDevice("stereo");
        }
        // Set up video output
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        vout.setVideoView(mSurfaceView);

        if (mSurfaceSubtitles.getVisibility() != View.GONE)
            vout.setSubtitlesView(mSurfaceSubtitles);

        vout.attachViews(this);

    }

    void changePlayChannel() {

        try {
            GuideActivity guide = GuideActivity.getInstance();
            if (Utils.usesRemoteControl(this)) {
                if (guide.active_channel_index < 0) {
                    return;
                }
                guide.channel_container.getChildAt(guide.active_channel_index).setActivated(false);
                guide.active_channel_index = channel_for_panel;
                guide.channel_container.getChildAt(guide.active_channel_index).setActivated(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        dvblink_channel_id_ = channels.get(channel_for_panel).DVBLinkID;
        currentChannelPlay = channel_for_panel;

        requestStopPlay(false);
    }

    private void stopPlayback() {
        if (mMediaPlayer != null && mMediaPlayer.getMedia() != null) {

            try {
                mMediaPlayer.setEventListener(null);

                if (mOnLayoutChangeListener != null) {
                    mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
                    mOnLayoutChangeListener = null;
                }
                mMediaPlayer.stop();

                mMediaPlayer.setMedia(null);
            } catch (Exception e) {}
        }
    }

    protected void invalidateTracksInfo() {
        mAudioTracks = null;
        mSubTracks = null;
    }

    private boolean startPlayback(String channel_url) {

        stopPlayback();

        state_ = state_ready;
        synchronized (_started) {
            _started = false;
        }

        try {

            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
            long audio_delay = Integer.parseInt(sp.getString("audio_delay", "0")) * 1000; //in microseconds
            boolean passtrough = sp.getBoolean(Settings.AUDIO_PASSTHROUGH_KEY, Settings.AUDIO_PASSTHROUGH_DEFAULT);

            Uri uri = Uri.parse(channel_url);
            mMedia = new Media(libvlc, uri);
            mMedia.setHWDecoderEnabled(true, true);
            mMedia.addOption(":network-caching=1000");
            mMedia.parse();
            fillMenu();
            mMediaPlayer.setMedia(mMedia);

            mMedia.release();

            final Runnable mRunnable = new Runnable() {
                @Override
                public void run() {
                    updateVideoSurfaces();
                }
            };

            mMediaPlayer.setEventListener(new MediaPlayer.EventListener() {
                @Override
                public void onEvent(MediaPlayer.Event event) {
                    switch (event.type) {
                        case MediaPlayer.Event.TimeChanged:
                            synchronized (_started) {
                                if (!_started) {
                                    _started = true;
                                }
                            }
                            if (mAudioTracks == null) {
                                mMedia.parse();
                                fillMenu();
                            }
                            break;
                        case MediaPlayer.Event.EndReached:
                            releasePlayer();
                            finish();
                            break;
                        case MediaPlayer.Event.Opening:
                            synchronized (_started) {
                                _started = false;
                            }
                            Thread thread = new Thread(new Runnable() {
                                    public void run() {
                                        try {
                                            Thread.sleep(30000);
                                            boolean go_out = false;
                                            synchronized (_started) {
                                                go_out = _started == false;
                                            }
                                        } catch (Exception e) {
                                            Log.e(TAG, "unknown", e);
                                        }
                                    }
                            });
                            thread.start();
                            break;
                        case MediaPlayer.Event.Paused:
                            break;
                        case MediaPlayer.Event.Playing:
                            mHandler.removeCallbacks(mRunnable);
                            mHandler.post(mRunnable);
                            break;
                    }
                }
            });

            //reset selected streams info
            selected_audio = stream_invalid_idx;
            selected_sub = stream_invalid_idx;

            invalidateTracksInfo();

            mMediaPlayer.play();



            if (mOnLayoutChangeListener == null) {
                mOnLayoutChangeListener = new View.OnLayoutChangeListener() {
                    @Override
                    public void onLayoutChange(View v, int left, int top, int right,
                                               int bottom, int oldLeft, int oldTop, int oldRight, int oldBottom) {
                        if (left != oldLeft || top != oldTop || right != oldRight || bottom != oldBottom) {
                            mHandler.removeCallbacks(mRunnable);
                            mHandler.post(mRunnable);
                        }
                    }
                };
            }
            mVideoSurfaceFrame.addOnLayoutChangeListener(mOnLayoutChangeListener);

            if (passtrough && audio_delay != 0)
                mMediaPlayer.setAudioDelay(audio_delay);

            mMediaPlayer.setVolume(100);


        } catch (Exception e) {
            Toast.makeText(this, "Error creating player!", Toast.LENGTH_LONG).show();
        }
        return true;
    }

    private void releasePlayer() {
        if (libvlc == null)
            return;
        mMediaPlayer.setEventListener(null);
        if (mOnLayoutChangeListener != null) {
            mVideoSurfaceFrame.removeOnLayoutChangeListener(mOnLayoutChangeListener);
            mOnLayoutChangeListener = null;
        }
        if (mMediaPlayer.isPlaying())
            mMediaPlayer.stop();
        mMediaPlayer.setMedia(null);
        final IVLCVout vout = mMediaPlayer.getVLCVout();
        try {
            vout.detachViews();
        } catch (Exception e) {
            Log.e(TAG, "releasePlayer", e);
        }

        mMediaPlayer.release();
        mMediaPlayer = null;

        libvlc.release();
        libvlc = null;
    }

    private void changeMediaPlayerLayout() {
        /* Change the video placement using the MediaPlayer API */
        //assume best fit algorithm for now
        mMediaPlayer.setAspectRatio(null);
        mMediaPlayer.setScale(0);
    }

    private void updateVideoSurfaces() {
        int sw = getWindow().getDecorView().getWidth();
        int sh = getWindow().getDecorView().getHeight();

        // sanity check
        if (sw * sh == 0) {
            Log.e(TAG, "Invalid surface size");
            return;
        }

        if (mMediaPlayer == null || mMediaPlayer.getVLCVout() == null) {
            return;
        }

        mMediaPlayer.getVLCVout().setWindowSize(sw, sh);

        ViewGroup.LayoutParams lp = mSurfaceView.getLayoutParams();
        if (mVideoWidth * mVideoHeight == 0) {
            /* Case of OpenGL vouts: handles the placement of the video using MediaPlayer API */
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mSurfaceView.setLayoutParams(lp);
            lp = mVideoSurfaceFrame.getLayoutParams();
            lp.width  = ViewGroup.LayoutParams.MATCH_PARENT;
            lp.height = ViewGroup.LayoutParams.MATCH_PARENT;
            mVideoSurfaceFrame.setLayoutParams(lp);
            changeMediaPlayerLayout();
            return;
        }

        if (lp.width == lp.height && lp.width == ViewGroup.LayoutParams.MATCH_PARENT) {
            /* We handle the placement of the video using Android View LayoutParams */
            mMediaPlayer.setAspectRatio(null);
            mMediaPlayer.setScale(0);
        }

        double dw = sw, dh = sh;
        final boolean isPortrait = getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;

        if (sw > sh && isPortrait || sw < sh && !isPortrait) {
            dw = sh;
            dh = sw;
        }

        // compute the aspect ratio
        double ar, vw;
        if (mVideoSarDen == mVideoSarNum) {
            /* No indication about the density, assuming 1:1 */
            vw = mVideoVisibleWidth;
            ar = (double)mVideoVisibleWidth / (double)mVideoVisibleHeight;
        } else {
            /* Use the specified aspect ratio */
            vw = mVideoVisibleWidth * (double)mVideoSarNum / mVideoSarDen;
            ar = vw / mVideoVisibleHeight;
        }

        // compute the display aspect ratio
        double dar = dw / dh;

        //assume best fit algorithm
        if (dar < ar)
            dh = dw / ar;
        else
            dw = dh * ar;

        // set display size
        lp.width  = (int) Math.ceil(dw * mVideoWidth / mVideoVisibleWidth);
        lp.height = (int) Math.ceil(dh * mVideoHeight / mVideoVisibleHeight);
        mSurfaceView.setLayoutParams(lp);
        if (mSurfaceSubtitles != null)
            mSurfaceSubtitles.setLayoutParams(lp);

        // set frame size (crop if necessary)
        lp = mVideoSurfaceFrame.getLayoutParams();
        lp.width = (int) Math.floor(dw);
        lp.height = (int) Math.floor(dh);
        mVideoSurfaceFrame.setLayoutParams(lp);

        mSurfaceView.invalidate();
        if (mSurfaceSubtitles != null)
            mSurfaceSubtitles.invalidate();
    }

    @Override
    public void onNewVideoLayout(IVLCVout vlcVout, int width, int height, int visibleWidth, int visibleHeight, int sarNum, int sarDen) {

        mVideoWidth = width;
        mVideoHeight = height;
        mVideoVisibleWidth = visibleWidth;
        mVideoVisibleHeight = visibleHeight;
        mVideoSarNum = sarNum;
        mVideoSarDen = sarDen;
        updateVideoSurfaces();
    }

    // endregion

    // region View handler
    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.play_live_activity);

        int hide_navigation_flags = Utils.usesNavBar(_context) ? View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION : 0;

        getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        getWindow().getDecorView().setSystemUiVisibility(
                          View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                        | hide_navigation_flags);

        toast = new Toast(_context);

        state_ = state_ready;
        activateProgressBarFullScreen(false);

        //indicate that this is a first start
        has_to_initialize_ = true;
    }

    protected void setupPlayerViews() {

        if (!Utils.usesRemoteControl(this)) {
            findViewById(R.id.help_line).setVisibility(View.GONE);
            VISIBLE_CHANNELS_COUNT = 3;

            View hbc = findViewById(R.id.hor_bar_container);
            hbc.setPadding(
                    (int)getResources().getDimension(R.dimen.play_live_progress_ll_padding_left_touch),
                    hbc.getPaddingTop(),
                    (int)getResources().getDimension(R.dimen.play_live_progress_ll_padding_right_touch),
                    hbc.getPaddingBottom());

            ImageButton ib = (ImageButton) findViewById(R.id.stop_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    requestStopPlay(true);
                }
            });

            if (time_shifted_playback_) {
                ib = (ImageButton) findViewById(R.id.pause_button);
                ib.setVisibility(View.VISIBLE);
                ib.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ImageButton ib = (ImageButton)v;
                        if (togglePause()) {
                            ib.setImageResource(R.drawable.tvm_play_play);
                        } else {
                            ib.setImageResource(R.drawable.tvm_play_pause);
                        }
                    }
                });

                ib = (ImageButton) findViewById(R.id.rewind_button);
                ib.setVisibility(View.VISIBLE);
                ib.setOnTouchListener(new View.OnTouchListener() {

                    private Handler mHandler;

                    @Override public boolean onTouch(View v, MotionEvent event) {
                        switch(event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                if (mHandler != null)
                                    return true;
                                //do once and start timer after 300 ms to repeat
                                rewind();
                                mHandler = new Handler();
                                mHandler.postDelayed(mAction, 300);
                                break;
                            case MotionEvent.ACTION_UP:
                                if (mHandler == null) return true;
                                mHandler.removeCallbacks(mAction);
                                mHandler = null;
                                break;
                        }
                        return false;
                    }

                    Runnable mAction = new Runnable() {
                        @Override public void run() {
                            rewind();
                            mHandler.postDelayed(this, 100);
                        }
                    };

                });

                ib = (ImageButton) findViewById(R.id.forward_button);
                ib.setVisibility(View.VISIBLE);
                ib.setOnTouchListener(new View.OnTouchListener() {

                    private Handler mHandler;

                    @Override public boolean onTouch(View v, MotionEvent event) {
                        switch(event.getAction()) {
                            case MotionEvent.ACTION_DOWN:
                                if (mHandler != null)
                                    return true;
                                //do once and start timer after 300 ms to repeat
                                forward();
                                mHandler = new Handler();
                                mHandler.postDelayed(mAction, 300);
                                break;
                            case MotionEvent.ACTION_UP:
                                if (mHandler == null) return true;
                                mHandler.removeCallbacks(mAction);
                                mHandler = null;
                                break;
                        }
                        return false;
                    }

                    Runnable mAction = new Runnable() {
                        @Override public void run() {
                            forward();
                            mHandler.postDelayed(this, 100);
                        }
                    };

                });
            } else {
                RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams)ib.getLayoutParams();
                params.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
                ib.setLayoutParams(params);
            }
        }

        TextView tv;
        tv = (TextView) findViewById(R.id.audio_title_);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_AUDIO));
        tv = (TextView) findViewById(R.id.subtitles_title_);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SUBTITLES));
        tv = (TextView) findViewById(R.id.help_line);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_PLAY_HELP_TEXT));

        paramPanel = (LinearLayout) findViewById(R.id.param_panel);
        subtitleList = (GridView) paramPanel.findViewById(R.id.subtitles_list_);
        audioTrackList = (GridView) paramPanel.findViewById(R.id.audio_list_);
        currentTime = (TextView) findViewById(R.id.current_time);
        programProgress = (ProgressBar) findViewById(R.id.program_progress);
        infoPanel = (LinearLayout) findViewById(R.id.info_panel);
        programSeek = (CustomSeekBar) findViewById(R.id.program_seek);

        if (time_shifted_playback_) {
            programSeek.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    return true;
                }
            });
            programSeek.setMax(SEEK_MAX_VALUE);
            programSeek.setProgress(SEEK_MAX_VALUE);
            programSeek.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                    startHideInfoPanelTimer();

                    if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                        return false;
                    }

                    if (backPressHandle(keyCode, keyEvent)) {
                        return true;
                    }

                    if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {
                        rewind();
                        return true;
                    }
                    if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                        forward();
                        return true;
                    }
                    if (Common.okPressed(keyCode)) {
                        togglePause();
                        return true;
                    }
                    return false;
                }
            });
            programSeek.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean focus) {
                    enableInfoPanel(false, true);
                    if (focus) {
                        startHideInfoPanelTimer();
                    }
                }
            });
            programSeek.setOnShortPress(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                        channelsRecyclerView.requestFocus();
                        return true;
                    }
                    return false;
                }
            });
            programSeek.setOnLongPress(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                    if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                        showParamPanel();
                        return true;
                    }
                    return false;
                }
            });
        }

        programSeek.setVisibility(time_shifted_playback_ ? View.VISIBLE : View.GONE);
        programProgress.setVisibility(!time_shifted_playback_ ? View.VISIBLE : View.GONE);

        prepareInputParams();
        preparePlayerViews();
        setRefreshRate();
        prepareInfoPanel();
    }


    private boolean backPressHandle(int keyCode, KeyEvent keyEvent) {
        if (Common.backPressed(keyCode, keyEvent)) {
            if (infoPanelHidden() && paramPanelHidden()) {
                requestStopPlay(true);
            } else {
                hidePanels();
            }
            return true;
        }
        return false;
    }

    private boolean togglePause() {
        if (unPause()) {
            return false;
        }
        mMediaPlayer.pause();

        return true;
    }

    private boolean unPause() {
        if (!mMediaPlayer.isPlaying()) {
            invalidateTracksInfo();
            mMediaPlayer.play();
            return true;
        }
        return false;
    }

    private void startStatsTimer() {
        stopStatsTimer();
        getStatsTimer = new Timer();
        getStatsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        requestStats();
                    }
                });
            }
        }, 0, TIME_SHIFT_STATS_TIMER_INTERVAL);
    }

    private void stopStatsTimer() {
        if (getStatsTimer == null) {
            return;
        }
        getStatsTimer.cancel();
        getStatsTimer.purge();
        getStatsTimer = null;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus && !ignoreWindowFocusChange && channels != null && channels.size() > 0 && channel_for_panel >= 0 && channel_for_panel < channels.size()) {
            if (Utils.usesRemoteControl(this)) {
                showInfoPanel();
                infoPanel.requestFocus();
            }
            ignoreWindowFocusChange = false;
        }
    }

    @Override
    protected void onDestroy() {
        stopUpdateLiveChannelInfoTimer();
        stopStatsTimer();
        // playback complete, give up audio focus
        audioManager.abandonAudioFocus(audioFocusChangeListener);
        //unregister media buttons
        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
        releasePlayer();

        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
//        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
        requestStopPlay(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        audioManager = (AudioManager) _context.getSystemService(AUDIO_SERVICE);
        audioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                switch (focusChange) {
                    case AudioManager.AUDIOFOCUS_GAIN:
                        // continue playback and raise volume (if it was previously lowered)
                        audioManager.registerMediaButtonEventReceiver(mediaButtonReceiver);
                        if (mMediaPlayer != null && !mMediaPlayer.isPlaying()) {
                            invalidateTracksInfo();
                            mMediaPlayer.play();
                        }
                        break;

                    case AudioManager.AUDIOFOCUS_LOSS:
                        // stop playback, deregister buttons, clean up
                        if (mMediaPlayer != null && mMediaPlayer.isPlaying())
                            mMediaPlayer.pause();
                        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
                        break;
                }
            }
        };
        int result = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mediaButtonReceiver = new ComponentName(getPackageName(), MediaButtonReceiver.class.getName());
            audioManager.registerMediaButtonEventReceiver(mediaButtonReceiver);
            // start playback
        }

        if (has_to_initialize_) {
            requestServerCaps();
            has_to_initialize_ = false;
        } else {
            requestPlayChannel();
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            showParamPanel();
            return true;
        }
        return super.onKeyLongPress(keyCode, event);
    }

    protected void hidePanels() {
        if (!paramPanelHidden()) {
            hideParamPanel();

            if (!infoPanelHidden())
                channelsRecyclerView.requestFocus();
        }
        hideInfoPanel();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (backPressHandle(keyCode, event)) {
            return true;
        }

        boolean isDown = event.getAction() == KeyEvent.ACTION_DOWN;

        if (isDown) {
            if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN ||
                    keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                    keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
                return super.onKeyDown(keyCode, event);
            }
        }

        if (infoPanelHidden()) {
            if (isDown && (keyCode == KeyEvent.KEYCODE_DPAD_UP || keyCode == KeyEvent.KEYCODE_PAGE_UP || keyCode == KeyEvent.KEYCODE_MEDIA_NEXT || keyCode == KeyEvent.KEYCODE_CHANNEL_UP)) {
                changeChannel(1);
                return true;
            }
            if (isDown && (keyCode == KeyEvent.KEYCODE_DPAD_DOWN || keyCode == KeyEvent.KEYCODE_PAGE_DOWN || keyCode == KeyEvent.KEYCODE_MEDIA_PREVIOUS || keyCode == KeyEvent.KEYCODE_CHANNEL_DOWN)) {
                changeChannel(-1);
                return true;
            }
            showInfoPanel();
        }
        startHideInfoPanelTimer();

        if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            event.startTracking();
            return true;
        }

        if (!infoPanelHidden() && paramPanelHidden()) {
            if (isDown) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                    _adapter.prevItem();
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                    _adapter.nextItem();
                    return true;
                }
                if (Common.okPressed(keyCode)) {
                    changePlayChannel();
                    return true;
                }
            }
        }

        if (time_shifted_playback_) {
            if (isDown) {
                if (keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                    forward();
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {
                    rewind();
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
                    togglePause();
                    return true;
                }
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    // endregion

    // region Server communication
    public void DataProcessing(String command, Object result_data) {
        final Object response = result_data;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state_ == state_stop_play) {
                    processStopPlay(response, false);
                } else if (state_ == state_exiting) {
                    processStopPlay(response, true);
                } else if (state_ == state_server_caps) {
                    processServerCaps(response);
                } else if (state_ == state_timeshift_url) {
                    processStartPlay(response);
                } else if (state_ == state_direct_url) {
                    processDirectUrl(response);
                } else if (state_ == state_get_stats) {
                    processGetStats(response);
                } else if (state_ == state_seek) {
                    processSeek(response);
                }
            }
        });
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        final StatusCode error_ = error;

        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state_ == state_stop_play && error_ == StatusCode.STATUS_NOT_IMPLEMENTED) {
                    //server does not support stopchannel comand at the moment
                    //just start playback of the new channel
                    processStopPlay(null, false);
                } else
                if (state_ == state_exiting) {
                    finish();
                } else {
                    state_ = state_ready;
                    activateProgressBarFullScreen(false);

                    if (language_manager_ != null) {
                        String msg = String.format("%s", language_manager_.ErrorToString(error_));
                        Toast.makeText(_context, msg, Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    protected void requestServerCaps() {
        if (data_provider_ != null && state_ == state_ready) {
            state_ = state_server_caps;
            activateProgressBarFullScreen(true);

            data_provider_.GetStreamingCapabilities();
        }
    }

    protected void processServerCaps(Object response) {
        try {
            state_ = state_ready;
            activateProgressBarFullScreen(false);
            if (response != null && response instanceof ServerCapabilities) {
                ServerCapabilities streaming_caps = (ServerCapabilities) response;

                time_shifted_playback_ = streaming_caps.SupportsTimeShift && settings_.getValue(Settings.TIMESHIFT_ENABLED_KEY, Settings.TIMESHIFT_ENABLED_DEFAULT);

                setupPlayerViews();
                createPlayer();

                requestPlayChannel();
            }
        } catch (Exception e) {
        }
    }

    protected void requestPlayChannel() {

        if (data_provider_ != null && state_ == state_ready) {
            if (time_shifted_playback_) {

                String stream_type = serverSettings.transcodingSupported && settings_.GetValueOrDefault(Settings.TRANSCODING_ENABLED_KEY, Settings.TRANSCODING_ENABLED_DEFAULT) ?
                        "h264ts_http_timeshift" : "raw_http_timeshift";

                long scale_factor = settings_.GetValueOrDefault(Settings.VIDEO_SCALING_KEY, Settings.VIDEO_SCALING_DEFAULT);
                long bitrate = settings_.GetValueOrDefault(Settings.BITRATE_KEY, Settings.BITRATE_DEFAULT);

                //for now always attach width/height for compatibility with dvblink servers
                //tvmosaic server actually uses scale
                Point pt = new Point();
                getWindowManager().getDefaultDisplay().getSize(pt);
                int height = (int) (pt.y / scale_factor);
                int width = (int) (pt.x / scale_factor);

                Transcoder tr = new Transcoder(height, width, bitrate, Transcoder.transcoderAudioTrackAny, scale_factor);
                RequestStream rs = new RequestStream(serverSettings.serverAddress, dvblink_channel_id_, settings_.getClientID(),
                        stream_type, tr);

                state_ = state_timeshift_url;
                data_provider_.PlayChannel(rs);
            } else {
                // request direct stream url
                DVBLinkChannelIdList idlist = new DVBLinkChannelIdList();
                idlist.add(dvblink_channel_id_);
                StreamInfoRequest rs = new StreamInfoRequest(serverSettings.serverAddress, settings_.getClientID(),
                        idlist);

                state_ = state_direct_url;

                data_provider_.GetStreamInfo(rs);
            }
            activateProgressBarFullScreen(true);
        }
    }

    protected void processStartPlay(Object response) {
        state_ = state_ready;
        activateProgressBarFullScreen(false);

        try {
            if (response != null && response instanceof ResponseStream) {
                ResponseStream streamer_obj = (ResponseStream) response;
                play_channel_handle_ = streamer_obj.ChannelHandle;
                String played_url = streamer_obj.Url;
                startPlayback(played_url);
                setLastChannelID(dvblink_channel_id_);
            }
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    private void setLastChannelID(String channelID) {
        settings_.setValue(
                Settings.PLAY_LAST_CHANNEL_ID, channelID
        );
    }

    protected void processDirectUrl(Object response) {
        state_ = state_ready;
        activateProgressBarFullScreen(false);

        try {
            if (response != null && response instanceof  StreamInfoList) {
                StreamInfoList stream_info = (StreamInfoList) response;
                if (stream_info.size() > 0) {
                    play_channel_handle_ = -1;
                    String played_url = stream_info.get(0).Url;

                    if (serverSettings.transcodingSupported && settings_.GetValueOrDefault(Settings.TRANSCODING_ENABLED_KEY, Settings.TRANSCODING_ENABLED_DEFAULT)) {

                        long scale_factor = settings_.GetValueOrDefault(Settings.VIDEO_SCALING_KEY, Settings.VIDEO_SCALING_DEFAULT);
                        long bitrate = settings_.GetValueOrDefault(Settings.BITRATE_KEY, Settings.BITRATE_DEFAULT);

                        //for now always attach width/height for compatibility with dvblink servers
                        //tvmosaic server actually uses scale
                        Point pt = new Point();
                        getWindowManager().getDefaultDisplay().getSize(pt);
                        int height = (int) (pt.y / scale_factor);
                        int width = (int) (pt.x / scale_factor);

                        played_url += "&transcoder=h264ts&width=" + width + "&height=" + height + "&bitrate=" + bitrate + "&scale=" + scale_factor;
                    }
                    startPlayback(played_url);
                    setLastChannelID(dvblink_channel_id_);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    protected void requestStopPlay(boolean exit) {
        if (data_provider_ != null && state_ == state_ready) {

            //stop media player to free the http connection to server
            try {
                stopPlayback();
//                if (mMediaPlayer != null && !mMediaPlayer.isPlaying())
//                    mMediaPlayer.stop();
            } catch (Exception e) {}

            state_ = exit ? state_exiting : state_stop_play;
            activateProgressBarFullScreen(true);
            StopStream ss = play_channel_handle_ == -1 ?
                    new StopStream(settings_.getClientID()) :
                    new StopStream(play_channel_handle_);
            data_provider_.StopChannel(ss);
        }
    }

    private void makeToast(String message) {
        LinearLayout layout = (LinearLayout) LinearLayout.inflate(_context, R.layout.toast, null);
        TextView text = (TextView) layout.findViewById(R.id.text);
        text.setText(message);
        hideToast();
        toast = new Toast(_context);
        toast.setView(layout);
        toast.setGravity(Gravity.LEFT | Gravity.TOP,
                _context.getResources().getDimensionPixelSize(R.dimen.player_toast_left_offs),
                _context.getResources().getDimensionPixelSize(R.dimen.player_toast_top_offs));
        toast.setDuration(Toast.LENGTH_LONG);
        toast.show();
    }
    private void hideToast() {
        toast.cancel();
    }

    protected void processStopPlay(Object response, boolean exit) {
        try {
            if (response != null) {
                play_channel_handle_ = -1;
            }
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
        state_ = state_ready;
        activateProgressBarFullScreen(false);

        _started = false;

        if (exit) {
            finish();
        } else {
            requestPlayChannel();
        }
    }
    // endregion

    // region Server communication TimeShift
    private void requestStats() {
        // Log.i(TAG, "requestStats");
        synchronized (_started) {
            if (!_started) {
                return;
            }
        }
        if (state_ != state_ready) {
            return;
        }

        try {
            Pair<Integer, Object> pair = queue.poll();
            if (pair == null) {
                state_ = state_get_stats;

                TimeShiftStatusRequest ts = new TimeShiftStatusRequest();
                ts.channel_handle = play_channel_handle_;
                data_provider_.Request(DataProvider.TIMESHIFT_GET_STATS, ts);
            } else {
                switch (pair.first) {
                    case state_seek:
                        Pair<Integer, Object> pair1 = queue.peek();
                        while (pair1 != null && pair1.first == state_seek) {
                            pair = queue.poll();
                            pair1 = queue.peek();
                        }
                        requestSeek((long) pair.second);
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "requestStats", e);
        }
    }

    private void processGetStats(Object response) {
        try {
            if (response != null && response instanceof TimeShiftStatusResponse) {
                timeShiftStatus = (TimeShiftStatusResponse) response;
                state_ = state_ready;
                int percN = calcPerN(timeShiftStatus.cur_pos_bytes, timeShiftStatus.buffer_length, SEEK_MAX_VALUE);
                programSeek.setProgress(percN);
                updateCurrentTime();
            }
        } catch (Exception e) {
            Log.e(TAG, "processGetStats", e);
        }
    }

    private void processSeek(Object response) {
        state_ = state_ready;
        activateProgressBarFullScreen(false);
        invalidateTracksInfo();
        mMediaPlayer.play();
        requestStats();
    }

    private void requestSeek(long pos) {
        try {
            state_ = state_seek;
            mMediaPlayer.stop();
            TimeShiftSeek ts = new TimeShiftSeek();
            ts.channel_handle = play_channel_handle_;
            ts.type = 1;
            ts.offset = pos;
            ts.whence = 0;
            data_provider_.Request(DataProvider.TIMESHIFT_SEEK, ts);
        } catch (Exception e) {
            Log.e(TAG, "requestSeek", e);
        }
    }
    // endregion
}


