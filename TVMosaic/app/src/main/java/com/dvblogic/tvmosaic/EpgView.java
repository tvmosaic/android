package com.dvblogic.tvmosaic;

import TVMosaic.TVMosaic.R;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.LinearLayout;

public class EpgView extends LinearLayout  
{
	protected int	now_x_coord	= 100;
	Paint paint;
	Rect drawingRect = new Rect();
	int line_color;
	Context _context;
	
	public EpgView(Context context) {
		super(context);
		_context = context;
		prepare_vars();
	}

	public EpgView(Context context, AttributeSet attrs) {
		super(context, attrs);
		_context = context;
		prepare_vars();
	}

	public EpgView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		_context = context;
		prepare_vars();
	}

	void prepare_vars() {
		paint = new Paint();
		line_color = ContextCompat.getColor(getContext(), R.color.guide_bar_now_line);
		paint.setColor(line_color);
		paint.setStrokeWidth(_context.getResources().getDimension(R.dimen.timeline_now_width));
	}

	public void set_now_coord(int new_x_coord) {
		now_x_coord = new_x_coord;
		postInvalidate();
	}

	@Override
	public void draw(Canvas canvas) {
		super.draw(canvas);

		if (now_x_coord != -1) {
			paint.setColor(line_color);
			paint.setStrokeWidth(_context.getResources().getDimension(R.dimen.time_line_width));
			canvas.drawLine(now_x_coord, 0, now_x_coord, getHeight(), paint);
		}
	}
}
