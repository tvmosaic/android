package com.dvblogic.tvmosaic;

import android.app.Activity;
import android.os.Build;
import android.util.Log;

import com.dvblogic.dvblink_common.AdviserDataProvider;
import com.dvblogic.dvblink_common.ChannelIDList;
import com.dvblogic.dvblink_common.ChannelIdWithPrograms;
import com.dvblogic.dvblink_common.ChannelIdWithProgramsList;
import com.dvblogic.dvblink_common.EpgSearcher;
import com.dvblogic.dvblink_common.IDataProvider;
import com.dvblogic.dvblink_common.ServerSettings;
import com.dvblogic.dvblink_common.StatusCode;
import com.dvblogic.dvblink_common.Utils;
import java.sql.Timestamp;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;

public class BackgroundEPGUpdater implements IDataProvider {

    public static long invalid_time_value = -1;

    private Activity activity_;
    private  ServerSettings server_settings_;
    private LanguageManager	language_manager_;
    private  AdviserDataProvider data_provider_ = null;
    private  IBackgroundEPGUpdater host_;
    private int update_interval_sec_;
    private Thread updater_thread_ = null;
    private ChannelIdWithProgramsList epg_data_ = null;
    private HashMap<String, String> hi_prio_channels_ = null;
    private long requested_start_time_ = -1;
    private long requested_end_time_ = -1;
    private int currently_updated_channel_idx_ = -1;
    private boolean update_active_;
    private long next_update_time_ = -1;
    private final ArrayList<PendingUpdateObject> pending_updates_ = new ArrayList<>();
    private final ArrayList<ChannelIDList> pending_channels_ = new ArrayList<>();
    private boolean requesting_epg_;

    class PendingUpdateObject {
        public long start_time_ = -1;
        public long end_time_ = -1;
        ChannelIDList channels_ = null;
    }

    public BackgroundEPGUpdater(ServerSettings server_settings, LanguageManager	language_manager, IBackgroundEPGUpdater host, Activity activity, int update_interval_sec) {
        server_settings_ = server_settings;
        language_manager_ = language_manager;
        host_ = host;
        update_interval_sec_ = update_interval_sec;
        activity_ = activity;

        //initialize data provider
        try {
            data_provider_ = new AdviserDataProvider(server_settings_, this);
        } catch (Exception e) {
            data_provider_ = null;
        }
    }

    protected long get_next_update_time() {
        return Utils.CurrentUtc() + update_interval_sec_;
    }

    public void add_update(long start_time, long end_time, ChannelIDList channels) {

        synchronized (pending_updates_) {
            PendingUpdateObject pu = new PendingUpdateObject();
            pu.start_time_ = start_time;
            pu.end_time_ = end_time;
            pu.channels_ = channels;
            pending_updates_.add(pu);
        }
    }

    private int getPendingUpdatesCount() {
        synchronized (pending_updates_) {
            return pending_updates_.size();
        }
    }

    private int getPendingChannelsCount() {
        synchronized (pending_channels_) {
            return pending_channels_.size();
        }
    }

    private void notifyUpdateFinished() {
        activity_.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                host_.updateFinished();
            }
        });
    }

    public void start() {
        data_provider_.setLanguage(language_manager_.getID());

        //start update timer thread
        updater_thread_ = new Thread(new Runnable() {
            public void run() {

                requesting_epg_ = false;
                update_active_ = false;

                //first update is now
                next_update_time_ = Utils.CurrentUtc();//get_next_update_time();

                boolean exit_flag = false;
                while(!exit_flag) {
                    try {
                        Thread.sleep(20);

                        if (getPendingUpdatesCount() > 0 && next_update_time_ > 0) {
                            //there is a pending update - do it now
                            next_update_time_ = Utils.CurrentUtc();
                        }

                        if (next_update_time_ > 0 && next_update_time_ <= Utils.CurrentUtc()) {
                            //update time!
                            next_update_time_ = -1;
                            activity_.runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (data_provider_ != null) {

                                        if (getPendingUpdatesCount() > 0) {
                                            PendingUpdateObject pu;
                                            synchronized (pending_updates_) {
                                                pu = pending_updates_.get(0);
                                                pending_updates_.remove(0);
                                            }
                                            if (pu.channels_ == null)
                                                host_.getChannelsForEPGUpdate(pu.start_time_, pu.end_time_);
                                            else{
                                                getEpgForChannels(pu.channels_, null, pu.start_time_, pu.end_time_);
                                            }
                                        } else
                                            host_.getChannelsForEPGUpdate(invalid_time_value, invalid_time_value);
                                    }
                                }
                            });
                        }

                        if (epg_data_ != null) {
                            //check if update is going on in the background already
                            if (!update_active_) {
//                                currently_updated_channel_idx_ += 1;

                                if (currently_updated_channel_idx_ == epg_data_.size()) {
                                    //update is finished
                                    currently_updated_channel_idx_ = -1;
                                    epg_data_ = null;
                                    //schedule a new one if all channels have been processed
                                    if (getPendingChannelsCount() == 0)
                                        notifyUpdateFinished();
                                        next_update_time_ = get_next_update_time();

                                } else {
                                    update_active_ = true;
                                    activity_.runOnUiThread(new Runnable() {
                                        @Override
                                        public void run() {
                                            int update_channel_count = 5;
                                            for (int c=0; c<update_channel_count; c++) {
                                                currently_updated_channel_idx_ += 1;
                                                if (currently_updated_channel_idx_ < epg_data_.size())
                                                    host_.processEPGDataUpdate(epg_data_, currently_updated_channel_idx_, requested_start_time_, requested_end_time_);
                                                else
                                                    break;
                                            }
                                            update_active_ = false;
                                        }
                                    });
                                }
                            }
                        } else {
                            if (!requesting_epg_ && getPendingChannelsCount() > 0) {

                                requesting_epg_ = true;
                                //fetch epg for the next channel batch
                                ChannelIDList channels;
                                synchronized (pending_channels_) {
                                    channels = pending_channels_.get(0);
                                    pending_channels_.remove(0);
                                }

                                if (data_provider_ != null) {
                                    EpgSearcher es = new EpgSearcher(channels, true, requested_start_time_, requested_end_time_);
                                    data_provider_.SearchEpg(es);
                                }
                            }
                        }

                    } catch (Exception e) {
                        exit_flag = true;
                    }
                }

                notifyUpdateFinished();
            }
        });
        updater_thread_.start();
    }

    public void stop() {
        if (updater_thread_ != null) {
            updater_thread_.interrupt();

            updater_thread_ = null;

            synchronized (pending_updates_) {
                pending_updates_.clear();
            }

            synchronized (pending_channels_) {
                pending_channels_.clear();
            }
        }
    }

    public void getEpgForChannels(ChannelIDList channels, HashMap<String, String> hi_prio_channels, long start_time, long end_time) {
        if (channels != null && channels.size() > 0) {

            host_.updateStarted();

            requested_start_time_ = start_time;
            requested_end_time_ = end_time;
            hi_prio_channels_ = hi_prio_channels;

            int max_channels = 10;

            int api = Build.VERSION.SDK_INT;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                max_channels = 50;
            }

            //sort channels on priority
            if (channels.size() > max_channels && hi_prio_channels_ != null) {
                Collections.sort(channels.list(), new Comparator<String>() {
                    public int compare(String ch1, String ch2) {
                        if (hi_prio_channels_.containsKey(ch1) && hi_prio_channels_.containsKey(ch2)) {
                            return 0;
                        }
                        if (hi_prio_channels_.containsKey(ch1)) {
                            return -1;
                        }
                        if (hi_prio_channels_.containsKey(ch2)) {
                            return 1;
                        }
                        return ch1.compareToIgnoreCase(ch2);
                    }
                });
            }

            synchronized (pending_channels_) {
                if (max_channels > 0) {
                    //split channels into batches of 10 channels each
                    for (int i = 0; i < channels.size(); i++) {
                        int c = i % max_channels;
                        if (c == 0)
                            pending_channels_.add(new ChannelIDList());

                        pending_channels_.get(pending_channels_.size() - 1).add(channels.get(i));
                    }
                } else {
                    pending_channels_.add(channels);
                }
            }
        } else {
            //no update is needed this time
            //schedule a new one
            next_update_time_ = get_next_update_time();
        }
    }

    public void DataProcessing(String command, Object result_data) {

        if (result_data != null && result_data instanceof ChannelIdWithProgramsList) {
            epg_data_ = (ChannelIdWithProgramsList) result_data;

            //sort returned channels on priority
            if (hi_prio_channels_ != null && epg_data_.list() != null) {
                Collections.sort(epg_data_.list(), new Comparator<ChannelIdWithPrograms>() {
                    public int compare(ChannelIdWithPrograms ch1, ChannelIdWithPrograms ch2) {
                        if (hi_prio_channels_.containsKey(ch1.ChannelId) && hi_prio_channels_.containsKey(ch2.ChannelId)) {
                            return 0;
                        }
                        if (hi_prio_channels_.containsKey(ch1.ChannelId)) {
                            return -1;
                        }
                        if (hi_prio_channels_.containsKey(ch2.ChannelId)) {
                            return 1;
                        }
                        return ch1.ChannelId.compareToIgnoreCase(ch2.ChannelId);
                    }
                });
            }

            requesting_epg_ = false;
            result_data = null;
        }
    }

    public void ErrorProcessing(String command, StatusCode error) {
        notifyUpdateFinished();
        //schedule a new update
        next_update_time_ = get_next_update_time();
    }

}
