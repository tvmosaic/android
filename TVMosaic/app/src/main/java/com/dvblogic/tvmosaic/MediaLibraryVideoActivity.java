package com.dvblogic.tvmosaic;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;

import TVMosaic.TVMosaic.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.*;

public class MediaLibraryVideoActivity extends BaseActivity implements OnBitmapLoadFinished, View.OnFocusChangeListener
{
    private static final String TAG = "MediaVideoActivity";

    private VideoItem video_item_ = null;
    boolean load_bitmap  = false;

    public MediaLibraryVideoActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle bundle) {

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(bundle);
        show_exit_dialog = false;

        try {
            initializeSettings();

            setContentView(R.layout.media_library_video_activity);

            if (!Utils.usesRemoteControl(this)) {
                View prg_container = findViewById(R.id.program_details_container);
                prg_container.setPadding(
                        (int) (prg_container.getPaddingLeft() * 0.3),
                        (int) (prg_container.getPaddingTop() * 0.3),
                        (int) (prg_container.getPaddingRight() * 0.3),
                        (int) (prg_container.getPaddingBottom() * 0.3)
                );
            }

            ClearProgramInfo();

            try {
                String xml_video_item = getIntent().getStringExtra("video_item");
                video_item_ = (VideoItem) Serializer.deserialize(xml_video_item, VideoItem.class);
            } catch (Exception e) {
                Log.e(TAG, "Eror, deserializing videoitem", e);
                video_item_ = null;
            }

            updateProgramDetails();

            setResult(RESULT_CANCELED);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void customizeLayout() {

        findViewById(R.id.content_layout).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                    View playButton = findViewById(R.id.play_button);
                    playButton.requestFocus();
                    return true;
                }

                return false;
            }
        });

        View playButton = findViewById(R.id.play_button);
        ((TextView) playButton.findViewById(R.id.title)).setText(
                language_manager_.GetString(LocaleStrings.IDS_PLAY_BUTTON_TITLE)
        );
        ((ImageView) playButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_play);
        playButton.setOnClickListener(playClickListener);
        playButton.setVisibility(View.VISIBLE);
        playButton.setEnabled(true);

        playButton.requestFocus();
    }

    private void playCurrentRecord() {
        //finish with play record result
        if (video_item_ != null) {
            String xml_video_item = Serializer.serialize(video_item_);
            getIntent().putExtra("video_item", xml_video_item);
        }

        finishWithResult(RESULT_OK);
    }

    OnClickListener playClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            playCurrentRecord();
        }
    };

    private void finishWithResult(int result) {
        Intent intent = getIntent();
        setResult(result, intent);
        finish();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void ClearProgramInfo() {

        TextView tv = (TextView) findViewById(R.id.program_name);
        tv.setText("");

        ImageView hd = (ImageView) findViewById(R.id.hd_icon);
        hd.setVisibility(View.INVISIBLE);

        ImageView repeat_premiere = (ImageView) findViewById(R.id.premiere_repeat_icon);
        repeat_premiere.setVisibility(View.INVISIBLE);

        View genre_dot = findViewById(R.id.genre_dot);
        genre_dot.setVisibility(View.INVISIBLE);

        tv = (TextView) findViewById(R.id.program_desc_line_1);
        tv.setText("");

        tv = (TextView) findViewById(R.id.program_desc_line_2);
        tv.setText("");

        tv = (TextView) findViewById(R.id.program_desc_line_3);
        tv.setText("");

        tv = (TextView) findViewById(R.id.description_text);
        tv.setText("");

        ImageView iv = (ImageView) findViewById(R.id.description_image);
        iv.setVisibility(View.INVISIBLE);

        //hide buttons
        View playButton = findViewById(R.id.play_button);
        playButton.setVisibility(View.GONE);
    }

    protected String AddLineToString(String valstr, String res_id, String str_to_add) {
        if (str_to_add != null && !str_to_add.equals("")) {
            str_to_add = str_to_add.replace("/", ", ");
            if (!valstr.equals(""))
                valstr += "\n";
            valstr += String.format("<i>%s:</i> %s", language_manager_.GetString(res_id), str_to_add);
        }
        return valstr;
    }

    @Override
    public void loadFinished() {
        setProgramDescription();
    }

    protected void updateProgramDetails() {
        if (video_item_ != null) {
            // thumbnail
            ImageView iv = (ImageView) findViewById(R.id.description_image);
            load_bitmap = (video_item_.VideoInfo.Image != null && !video_item_.VideoInfo.Image.equals(""));
            if (load_bitmap) {
                new BitmapLoader(this, this, video_item_.VideoInfo.Image, (ImageView) findViewById(R.id.description_image), null).start();
            }

            setProgramInfo();
            setProgramDescription();

            customizeLayout();

        } else {
            ClearProgramInfo();
        }
    }

    protected void setProgramInfo() {
        TextView tv = (TextView) findViewById(R.id.program_name);
        tv.setText(video_item_ == null || video_item_.VideoInfo.Title == null ? language_manager_.GetString(LocaleStrings.IDS_NO_PROGINFO_AVAILABLE) : video_item_.VideoInfo.Title);

        if (video_item_ == null)
            return;

        ImageView hd = (ImageView) findViewById(R.id.hd_icon);
        hd.setVisibility(video_item_.VideoInfo.IsHdtv ? View.VISIBLE : View.INVISIBLE);

        ImageView repeat_premiere = (ImageView) findViewById(R.id.premiere_repeat_icon);
        if (video_item_.VideoInfo.IsRepeat || video_item_.VideoInfo.IsPremiere) {
            repeat_premiere.setImageResource(video_item_.VideoInfo.IsRepeat ? R.drawable.tvm_icon_repeat : R.drawable.tvm_icon_premiere);
            repeat_premiere.setVisibility(View.VISIBLE);
        } else {
            repeat_premiere.setVisibility(View.INVISIBLE);
        }

        View genre_dot = findViewById(R.id.genre_dot);
        if (Constants.HasGenreColor(video_item_.VideoInfo)) {
            genre_dot.setVisibility(View.VISIBLE);
            GradientDrawable bgShape = (GradientDrawable)genre_dot.getBackground();
            bgShape.setColor(ContextCompat.getColor(_context, Constants.GetColorFromGenre(video_item_.VideoInfo)));
        } else {
            genre_dot.setVisibility(View.INVISIBLE);
        }

        // episode
        String str = ((video_item_.VideoInfo.Subname == null) ? "" : video_item_.VideoInfo.Subname);

        String ES = "";
        if (video_item_.VideoInfo.SeasonNum != 0)
            ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_SEASON), video_item_.VideoInfo.SeasonNum);
        if (video_item_.VideoInfo.EpisodeNum != 0) {
            if (video_item_.VideoInfo.SeasonNum != 0)
                ES += ", ";
            ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_EPISODE),
                    video_item_.VideoInfo.EpisodeNum);
        }
        if (str.isEmpty()) {
            str += ES;
        } else {
            if (!ES.isEmpty())
                str += "  |  " + ES;
        }

        tv = (TextView) findViewById(R.id.program_desc_line_1);
        tv.setText(str);

        // second description line
        str = "";
        int duration_minutes = (int) (video_item_.VideoInfo.Duration / 60.0);
        if (duration_minutes > 0) {
            String strDuration = String.format(Locale.getDefault(), "%s %d %s",
                    language_manager_.GetString(LocaleStrings.IDS_DURATION), duration_minutes,
                    language_manager_.GetString(LocaleStrings.IDS_MINUTE));

            str = strDuration;
        }

        tv = (TextView) findViewById(R.id.program_desc_line_2);
        tv.setText(str);

        //categories
        str = "";
        if (video_item_.VideoInfo.Keywords != null && !video_item_.VideoInfo.Keywords.equals("")) {
            str = video_item_.VideoInfo.Keywords.replace("/", ", ");
        }
        tv = (TextView) findViewById(R.id.program_desc_line_3);
        tv.setText(str);

    }

    String[] sizeLabels;

    public static String formatSize(long bytes, String[] labels) {
        // labels => { B, K, M. G, T, P, E }
        if (labels.length != 7) {
            throw new RuntimeException("param error");
        }
        if (bytes < 1024) {
            return bytes + " " + labels[0];
        }
        int numberOfLeadingZeros = (63 - Long.numberOfLeadingZeros(bytes)) / 10;
        return String.format(
                Locale.getDefault(),
                "%.1f %sB",
                (double) bytes / (1L << (numberOfLeadingZeros * 10)),
                numberOfLeadingZeros < 1 ? "" : labels[numberOfLeadingZeros]
        );
    }


    protected void setProgramDescription() {

        TextView tv = (TextView) findViewById(R.id.description_text);

        String desc_str = new String();
        if (video_item_.VideoInfo.ShortDesc != null && !video_item_.VideoInfo.ShortDesc.equals(""))
            desc_str += video_item_.VideoInfo.ShortDesc;

        String ext_str = new String();
        ext_str += "\n";
        if (video_item_.VideoInfo.Actors != null && !video_item_.VideoInfo.Actors.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_ACTORS_HEADER, video_item_.VideoInfo.Actors);
            ext_str += "\n";
        }
        if (video_item_.VideoInfo.Guests != null && !video_item_.VideoInfo.Guests.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_GUESTS_HEADER, video_item_.VideoInfo.Guests);
            ext_str += "\n";
        }
        if (video_item_.VideoInfo.Directors != null && !video_item_.VideoInfo.Directors.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_DIRECTORS_HEADER, video_item_.VideoInfo.Directors);
            ext_str += "\n";
        }
        if (video_item_.VideoInfo.Producers != null && !video_item_.VideoInfo.Producers.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_PRODUCERS_HEADER, video_item_.VideoInfo.Producers);
            ext_str += "\n";
        }
        if (video_item_.VideoInfo.Writers != null && !video_item_.VideoInfo.Writers.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_WRITERS_HEADER, video_item_.VideoInfo.Writers);
            ext_str += "\n";
        }
        if (video_item_.VideoInfo.Year > 0) {
            String local = String.format(Locale.getDefault(), "%d", video_item_.VideoInfo.Year);
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_YEAR_HEADER, local);
        }

        final String desc_str_f = desc_str.toString().replace("\n", "<br />");
        final String ext_str_f = ext_str.toString().replace("\n", "<br />");

        ImageView iv = (ImageView) findViewById(R.id.description_image);
        if (load_bitmap) {
            iv.setVisibility(View.VISIBLE);
            Display display = getWindowManager().getDefaultDisplay();
            Point pt = new Point();
            display.getSize(pt);
            iv.measure(pt.x, pt.y);

            if (iv.getMeasuredWidth() == 0 || iv.getMeasuredHeight() == 0) {
                Spanned s = Html.fromHtml(desc_str_f + ext_str_f);
                tv.setText(s);
            } else {
                float margin = getResources().getDimension(R.dimen.program_activity_thumb_margin);
                final int wd = (int) (iv.getMeasuredWidth() + margin);
                float ht = iv.getMeasuredHeight() + margin;

                float text_spacing = tv.getPaint().getFontSpacing();
                final int lines = (int) (ht / text_spacing);

                final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tv.getLayoutParams();
                params.setMargins(wd, 0, 0, 0);
                tv.setText(Html.fromHtml(desc_str_f + ext_str_f));
                tv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @SuppressWarnings("deprecation")
                    @Override
                    public void onGlobalLayout() {

                        TextView tv = (TextView) findViewById(R.id.description_text);
                        int linesCount = tv.getLayout().getLineCount();

                        // restore the margin
                        params.setMargins(0, 0, 0, 0);
                        SpannableString spanS = new SpannableString(Html.fromHtml(desc_str_f + ext_str_f));

                        if (linesCount <= lines) {
                            spanS.setSpan(new InfoLeadingMarginSpan2(lines, wd), 0, spanS.length(), 0);
                            tv.setText(spanS);
                        } else {
                            // find the breakpoint where to break the String.
                            int breakpoint = tv.getLayout().getLineEnd(lines - 1);

                            Spannable s1 = new SpannableStringBuilder(spanS, 0, breakpoint);
                            s1.setSpan(new InfoLeadingMarginSpan2(lines, wd), 0, s1.length(), 0);
                            Spannable s2 = new SpannableStringBuilder(System.getProperty("line.separator"));
                            Spannable s3 = new SpannableStringBuilder(spanS, breakpoint, spanS.length());
                            // It is needed to set a zero-margin span on for the text under the image to prevent the space on the right!
                            s3.setSpan(new InfoLeadingMarginSpan2(0, 0), 0, s3.length(), 0);
                            tv.setText(TextUtils.concat(s1, s2, s3));

                            // Align the text with the image by removing the rule that the text is to the right of the image
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tv.getLayoutParams();
                            int[] rules = params.getRules();
                            rules[RelativeLayout.RIGHT_OF] = 0;

                        }

                        // remove the GlobalLayoutListener
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            tv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            tv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }

                    }
                });
            }
        }
        else {
            iv.setVisibility(View.GONE);
            Spanned s = Html.fromHtml(desc_str_f + ext_str_f);
            tv.setText(s);
        }
    }

    public void onFocusChange(View v, boolean hasFocus) {
        int i = v.getId();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return true;
        }
        if (Common.backPressed(keyCode, event)) {
            finishWithResult(RESULT_CANCELED);
            return false;
        }
        return false;
    }



}
