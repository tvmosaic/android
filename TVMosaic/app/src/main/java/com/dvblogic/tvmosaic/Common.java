package com.dvblogic.tvmosaic;

import TVMosaic.TVMosaic.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.LightingColorFilter;
import android.graphics.Paint;
import android.graphics.Point;
import android.util.TypedValue;
import android.view.Display;
import android.view.KeyEvent;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

public class Common
{
	static public int _calcDropDownWidth(TextView tv, String[] str, ImageView iv, Display display) {
		float wd = 0;
		if (tv == null || str == null)
			return (int) wd;
		Paint paint = tv.getPaint();
		for (String s : str) {
			if (s==null || s.length()==0)
				continue;
			float width = paint.measureText(s+'W');
			wd = Math.max(wd, width);
		}
		if (wd == 0)
			wd = 200;
		
		float margin = 200;//getResources().getDimension(R.dimen.child_margin);
		Point pt = new Point();
		display.getSize(pt);
		if (iv != null) {
			iv.measure(pt.x, pt.y);
			wd += iv.getMeasuredWidth() + margin;
		} else {
			wd += margin / 2;
		}
		if (wd > pt.x)
			wd = pt.x - 64;
		return (int) wd;
	}

	static public int old_calcDropDownWidth(TextView tv, String[] str) {
		float wd = 0;
		if (tv == null || str == null)
			return (int) wd;
		Paint paint = tv.getPaint();
		for (String s : str) {
			if (s==null || s.length()==0)
				continue;
			float width = paint.measureText(s+'W');
			wd = Math.max(wd, width);
		}
		if (wd == 0)
			wd = 200;
		return (int) wd;
	}

	static public int calcTextWidth(TextView tv, String[] str) {
		float wd = 0;
		if (tv == null || str == null)
			return (int) wd;
		Paint paint = tv.getPaint();
		for (String s : str) {
			if (s==null || s.length()==0)
				continue;
			float width = paint.measureText(s+'W');
			wd = Math.max(wd, width);
		}
		if (wd == 0)
			wd = 200;
		return (int) wd;
	}

	static public Bitmap changeBitmapColor(Bitmap sourceBitmap, ImageView image, int color) {

		Bitmap resultBitmap = Bitmap.createBitmap(sourceBitmap, 0, 0, sourceBitmap.getWidth() - 1,
				sourceBitmap.getHeight() - 1);
		Paint p = new Paint();
		ColorFilter filter = new LightingColorFilter(color, 1);
		p.setColorFilter(filter);
		image.setImageBitmap(resultBitmap);

		Canvas canvas = new Canvas(resultBitmap);
		canvas.drawBitmap(resultBitmap, 0, 0, p);
        return resultBitmap;
	}

	static public int getColorFromAttribute(final Activity activity, final int attr) {
		if (attr == 0)
			return 0;
		final TypedValue typedvalueattr = new TypedValue();
		activity.getTheme().resolveAttribute(attr, typedvalueattr, true);
		int colorId = typedvalueattr.resourceId; 
		
		return activity.getResources().getColor(colorId);
	}
	
	static public void customizeSpinner(LinearLayout layout, String label, int image_resource) {
		TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
		if (tv != null)
			tv.setText(label);
		ImageView iv = (ImageView) layout.findViewById(R.id.spinner_icon);
		iv.setImageResource(image_resource);
	}

    static public boolean okPressed(int keyCode) {
        return (keyCode == KeyEvent.KEYCODE_DPAD_CENTER
                || keyCode == KeyEvent.KEYCODE_ENTER);
    }

    static public boolean backPressed(int keyCode, KeyEvent event) {
        return (keyCode == KeyEvent.KEYCODE_BACK
                || keyCode == KeyEvent.KEYCODE_ESCAPE) && (event.getAction() == KeyEvent.ACTION_DOWN);
        /*
        return (keyCode == KeyEvent.KEYCODE_BACK
                || keyCode == KeyEvent.KEYCODE_ESCAPE
                || keyCode == KeyEvent.KEYCODE_MENU);
                */
    }

	public static boolean isGoogleApiAvailable(Context ctx) {
		GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
		int resultCode = apiAvailability.isGooglePlayServicesAvailable(ctx);
		boolean result = (resultCode == ConnectionResult.SUCCESS);
		return result;
	}


}
