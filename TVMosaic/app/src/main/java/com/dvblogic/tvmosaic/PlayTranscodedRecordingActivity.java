package com.dvblogic.tvmosaic;

import android.content.ComponentName;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Point;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.*;

import com.dvblogic.dvblink_common.*;
import com.google.android.exoplayer.AspectRatioFrameLayout;
import com.google.android.exoplayer.ExoPlayer;
import com.google.android.exoplayer.MediaFormat;
import com.google.android.exoplayer.demo.player.DemoPlayer;
import com.google.android.exoplayer.demo.player.HlsRendererBuilder;
import com.google.android.exoplayer.util.PlayerControl;

import java.util.*;

import TVMosaic.TVMosaic.R;

public class PlayTranscodedRecordingActivity extends BaseActivity implements SurfaceHolder.Callback, DemoPlayer.Listener
{
    private static final String TAG = "Player";
    private static final long HIDE_PANEL_DELAY = 5000;
    private static final int INFO_PANEL_ALPHA_ENABLED = 255;
    private static final int INFO_PANEL_ALPHA_DISABLED = 50;
    private static final int REWIND_DURATION_SECONDS = -15;
    private static final int FORWARD_DURATION_SECONDS = 30;
    private static final int TIME_SHIFT_STATS_TIMER_INTERVAL = 500;
    private static final int SEEK_MAX_VALUE = 1000;
    private static final int MOVE_THROTTLE_TIME = 650;

    private class ItemAdapter extends BaseAdapter {
        boolean _audio_adapter = true;

        private ItemAdapter(boolean audio_adapter) {
            super();
            _audio_adapter = audio_adapter;
        }

        @SuppressWarnings("unchecked")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            try {
                if (row == null) {
                    row = LinearLayout.inflate(_context, R.layout.player_menu_row, null);
                }

                MediaFormat track = (MediaFormat) getItem(position);
                if (track == null)
                    return row;


                String text = language_manager_.GetString(LocaleStrings.IDS_TRACK) + " " + position;
                TextView label = (TextView) row.findViewById(R.id.vlc_menu_row_text);
                label.setText(text);

                ImageView icon = (ImageView) row.findViewById(R.id.vlc_menu_row_icon);
                if (_audio_adapter) {
                    icon.setImageResource(
                            selected_audio == position ?
                                    R.drawable.tvm_checkbox_checked_icon_active :
                                    R.drawable.tvm_checkbox_unchecked_icon_active
                    );
                } else {
                    icon.setImageResource(
                            selected_sub == position ?
                                    R.drawable.tvm_checkbox_checked_icon_active :
                                    R.drawable.tvm_checkbox_unchecked_icon_active
                    );
                }
            } catch (Exception e) {
                Log.e(TAG, "unknown", e);
            }
            return row;
        }

        @Override
        public int getCount() {

            if (_audio_adapter)
                return mAudioTracks.size();
            else
                return mSubTracks.size();
        }

        @Override
        public Object getItem(int position) {
            if (_audio_adapter)
                return (mAudioTracks == null ? null : mAudioTracks.get(position));
            else
                return (mSubTracks == null ? null : mSubTracks.get(position));
        }

        @SuppressWarnings("unchecked")
        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    private class HidePanelTimerTask extends TimerTask {
        @Override
        public void run() {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    hidePanels();
                }
            });

        }
    }
    private static int stream_invalid_idx = -1;
    private static int stream_disabled_id = -1;

    private final int state_ready = 0;
    private final int state_seek = 6;

    DemoPlayer exoPlayer=null;
    private SurfaceView surfaceView;
    private AspectRatioFrameLayout videoFrame;
    PlayerControl playerControl = null;

    private ArrayList<MediaFormat> mAudioTracks = new ArrayList<>();
    private ArrayList<MediaFormat> mSubTracks = new ArrayList<>();
    private int selected_audio = stream_invalid_idx;
    private int selected_sub = stream_invalid_idx;

    private Boolean _started = false;
    private int state_ = state_ready;
    private AudioManager.OnAudioFocusChangeListener audioFocusChangeListener;
    private ComponentName mediaButtonReceiver;
    private AudioManager audioManager;

    private CustomTextView programName;
    private CustomTextView programTime;
    private Timer hideInfoPanelTimer;
    private LinearLayout infoPanel;
    private TextView currentTime;
    private CustomSeekBar programSeek;

    private GridView subtitleList;
    private GridView audioTrackList;
    private LinearLayout paramPanel;

    private boolean infoPanelShowing = false;
    private boolean infoPanelHiding = false;

    private boolean paramPanelShowing = false;
    private boolean paramPanelHiding = false;

    private RecordedTV recording_ = null;
    private Timer getStatsTimer;
    private int seek_position_ = -1;
    private int new_resume_position_ = 0;

    private Timer moveThrottleTimer;

    private Queue<Pair<Integer, Object>> queue = new LinkedList<>();
    private boolean ignoreWindowFocusChange = false;
    private Toast toast;

    private void startHideInfoPanelTimer() {

        if (!Utils.usesRemoteControl(this))
            return;

        stopHideInfoPanelTimer();
        hideInfoPanelTimer = new Timer();
        hideInfoPanelTimer.schedule(
                new HidePanelTimerTask(), HIDE_PANEL_DELAY
        );
    }
    private void stopHideInfoPanelTimer() {

        if (!Utils.usesRemoteControl(this))
            return;

        if (hideInfoPanelTimer != null) {
            hideInfoPanelTimer.cancel();
        }
    }

    private void showInfoPanel() {
        if (!infoPanelHidden() || infoPanelShowing) {
            return;
        }
        infoPanelShowing = true;
        startHideInfoPanelTimer();
        showCurrentProgram();
        UpdatePlayStats();
        startStatsTimer();

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_in_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                infoPanel.setVisibility(View.VISIBLE);
                infoPanelShowing = false;
                programSeek.requestFocus();
            }
        });
        infoPanel.startAnimation(animation);
    }

    private void hideInfoPanel() {
        if (infoPanelHidden() || infoPanelHiding) {
            return;
        }
        infoPanelHiding = true;
        stopHideInfoPanelTimer();
        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_out_bottom);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                infoPanel.setVisibility(View.INVISIBLE);
                infoPanelHiding = false;
            }
        });
        infoPanel.startAnimation(animation);
        stopStatsTimer();
    }

    private boolean infoPanelHidden() {
        return infoPanel.getVisibility() == View.INVISIBLE;
    }

    private boolean paramPanelHidden() {
        return paramPanel.getVisibility() != View.VISIBLE;
    }

    private void showParamPanel() {

        if (!paramPanelHidden() || paramPanelShowing) {
            return;
        }
        paramPanelShowing = true;

        stopHideInfoPanelTimer();
        paramPanel.setVisibility(View.VISIBLE);

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_in_top);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                paramPanelShowing = false;
                paramPanel.requestFocus();
            }
        });
        paramPanel.startAnimation(animation);
    }

    private void hideParamPanel() {
        if (paramPanelHidden() || paramPanelHiding) {
            return;
        }
        paramPanelHiding = true;

        Animation animation = AnimationUtils.loadAnimation(_context, R.anim.slide_out_top);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) { }
            @Override
            public void onAnimationRepeat(Animation animation) { }
            @Override
            public void onAnimationEnd(Animation animation) {
                paramPanel.setVisibility(View.GONE);
                paramPanelHiding = false;
            }
        });
        paramPanel.startAnimation(animation);
    }

    private void enableInfoPanel(boolean enabled) {

        int alphaCurrentTimeElement = enabled ? INFO_PANEL_ALPHA_ENABLED : INFO_PANEL_ALPHA_DISABLED;
        currentTime.setTextColor(currentTime.getTextColors().withAlpha(alphaCurrentTimeElement));
        programName.setTextColor(programName.getTextColors().withAlpha(alphaCurrentTimeElement));
        programTime.setTextColor(programTime.getTextColors().withAlpha(alphaCurrentTimeElement));
    }

    private void showCurrentProgram() {
        String str = recording_.VideoInfo.Title;
        if (recording_.VideoInfo.Subname != null && !recording_.VideoInfo.Subname.isEmpty()) {
            str += " - " + recording_.VideoInfo.Subname;
        }
        programName.setText(str);

        // channel & time
        String strChannel = recording_.ChannelName;

        String strDate = null;
        if (Utils.isToday(recording_.VideoInfo.StartTime)) {
            strDate = language_manager_.GetString(LocaleStrings.IDS_TODAY);
        } else {
            strDate = Utils.UtcToMediumDateString(_context, recording_.VideoInfo.StartTime);
        }

        String strStart = Utils.UtcToShortTimeString(_context, recording_.VideoInfo.StartTime);
        String strEnd = Utils.UtcToShortTimeString(_context, recording_.VideoInfo.StartTime + recording_.VideoInfo.Duration);

        int duration_minutes = (int) (recording_.VideoInfo.Duration / 60.0);
        String strDuration = String.format(Locale.getDefault(), "%s %d %s",
                language_manager_.GetString(LocaleStrings.IDS_DURATION), duration_minutes,
                language_manager_.GetString(LocaleStrings.IDS_MINUTE));

        str = String.format("%s  |  %s  |  %s - %s  |  %s", strChannel, strDate, strStart, strEnd, strDuration);

        programTime.setText(str);
    }

    String getTime(long time_sec) {
        String str;
        long h = time_sec / 3600;
        if (h == 0) {
            long m = time_sec / 60;
            long s = (time_sec - m*60) % 60;
            str = String.format("%d:%02d", m, s);
        } else {
            long m = (time_sec - h*3600) / 60;
            long s = (time_sec - h*3600) % 60;
            str = String.format("%d:%02d:%02d", h, m, s);
        }
        return str;
    }

    private void updateCurrentPlaybackPosition(int seconds) {
        currentTime.setText(getTime(seconds));
    }

    private void prepareInputParams() {
        try {
            String xml_recording = getIntent().getStringExtra("recording");
            recording_ = (RecordedTV) Serializer.deserialize(xml_recording, RecordedTV.class);
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    private void preparePlayerViews() {
        videoFrame = (AspectRatioFrameLayout) findViewById(R.id.video_frame);
        surfaceView = (SurfaceView) findViewById(R.id.surface_view);
//        surfaceView.getHolder().addCallback(this);

        surfaceView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN){
                    boolean paramAreaTouch = event.getY() < (v.getHeight() / 2);
                    boolean infoAreaTouch = event.getY() > (v.getHeight() / 2);

                    if (paramAreaTouch) {
                        if (!infoPanelHidden()) {
                            hideInfoPanel();
                        }
                        if (paramPanelHidden() && infoPanelHidden()) {
                            showParamPanel();
                        }
                    }

                    if (infoAreaTouch) {
                        if (!paramPanelHidden()) {
                            hideParamPanel();
                        }
                        if (infoPanelHidden() && paramPanelHidden()) {
                            showInfoPanel();
                        }
                    }
                }
                return true;
            }
        });
    }

    private void prepareInfoPanel() {
        LayoutInflater inflater = getLayoutInflater();

        programName = (CustomTextView) findViewById(R.id.program_name);
        programTime = (CustomTextView) findViewById(R.id.program_time);

        programSeek.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                enableInfoPanel(focus);
                if (focus) {
                    hideParamPanel();

                    startHideInfoPanelTimer();
                }
            }
        });

        infoPanel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!paramPanelHidden()) {
                    hideParamPanel();
                    infoPanel.requestFocus();
                }
            }
        });
    }

    private void setRefreshRate() {
        try {
            float _refresh_rate = settings_.getValue(Settings.REFRESH_RATE_KEY, Settings.REFRESH_RATE_DEFAULT);
            if (_refresh_rate > 0) {
                Display display = ((WindowManager) getSystemService(WINDOW_SERVICE)).getDefaultDisplay();
                WindowManager.LayoutParams params = getWindow().getAttributes();
                if (Build.VERSION.SDK_INT >= 23) {
                    Display.Mode[] modes = display.getSupportedModes();
                    for (int idx = 0; idx < modes.length; idx++) {
                        Display.Mode mode = modes[idx];
                        if (mode.getRefreshRate() == _refresh_rate) {
                            params.preferredDisplayModeId = mode.getModeId();
                            getWindow().setAttributes(params);
                        }
                    }
                } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    params.preferredRefreshRate = _refresh_rate;
                    getWindow().setAttributes(params);
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "unknown", e);
        }
    }

    private void fillMenu() {

        mAudioTracks.clear();
        mSubTracks.clear();

        int audio_tracks_num = exoPlayer.getTrackCount(DemoPlayer.TYPE_AUDIO);
        for (int i=0; i<audio_tracks_num; i++) {
            MediaFormat mf = exoPlayer.getTrackFormat(DemoPlayer.TYPE_AUDIO, i);
            mAudioTracks.add(mf);
        }
        if (selected_audio == stream_invalid_idx)
            selected_audio = exoPlayer.getSelectedTrack(DemoPlayer.TYPE_AUDIO);
        else
            exoPlayer.setSelectedTrack(DemoPlayer.TYPE_AUDIO, selected_audio);

        int sub_tracks_num = exoPlayer.getTrackCount(DemoPlayer.TYPE_TEXT);
        for (int i=0; i<sub_tracks_num; i++) {
            MediaFormat mf = exoPlayer.getTrackFormat(DemoPlayer.TYPE_TEXT, i);
            mSubTracks.add(mf);
        }
        if (selected_sub == stream_invalid_idx)
            selected_sub = exoPlayer.getSelectedTrack(DemoPlayer.TYPE_TEXT);
        else
            exoPlayer.setSelectedTrack(DemoPlayer.TYPE_TEXT, selected_sub);

        if (mSubTracks.size() > 0) {
            subtitleList.setAdapter(new ItemAdapter(false));
            subtitleList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_sub = position;
                    exoPlayer.setSelectedTrack(DemoPlayer.TYPE_TEXT, selected_sub);
                    subtitleList.invalidateViews();
                }
            });
            subtitleList.setSelection(selected_sub);
        }

        if (mAudioTracks.size() > 0) {
            audioTrackList.setAdapter(new ItemAdapter(true));
            audioTrackList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    selected_audio = position;
                    exoPlayer.setSelectedTrack(DemoPlayer.TYPE_AUDIO, selected_audio);
                    audioTrackList.invalidateViews();
                }
            });
            audioTrackList.setSelection(selected_audio);
        }
    }

    private void rewind() {
        move(REWIND_DURATION_SECONDS);
    }

    private void forward() {
        move(FORWARD_DURATION_SECONDS);
    }

    private void move(int duration) {
        stopStatsTimer(); // Останавка обновления стат. информации c сервера (меняет cur_pos_sec).

        if (moveThrottleTimer != null) {
            moveThrottleTimer.cancel();
            moveThrottleTimer = null;
        }

        //set initial position
        if (seek_position_ == -1) {
            seek_position_ = (int) (playerControl.getCurrentPosition() / 1000);
        }

        int position = 0;
        if (recording_.VideoInfo.Duration > 0) {
            position = Math.min(
                    Math.max(seek_position_ + duration, 0),
                    recording_.VideoInfo.Duration
            );
        }

        seek_position_ = position;
        updateCurrentPlaybackPosition(seek_position_);
        programSeek.setProgress(calcPerN(position, recording_.VideoInfo.Duration, SEEK_MAX_VALUE));

        moveThrottleTimer = new Timer();
        moveThrottleTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        queue.add(new Pair<Integer, Object>(state_seek, seek_position_));
                        seek_position_ = -1;
                        startStatsTimer();
                    }
                });
            }
        }, MOVE_THROTTLE_TIME);
    }

    private int calcPerN(long current, long max, long n) {
        return (int)((float) current / max * n);
    }

    private DemoPlayer.RendererBuilder getRendererBuilder(String url) {
        String userAgent = "TVMosaic";
        return new HlsRendererBuilder(this, userAgent, url);
    }

    private void startPlayback() {
        String url = recording_.Url;

        long scale_factor = settings_.GetValueOrDefault(Settings.VIDEO_SCALING_KEY, Settings.VIDEO_SCALING_DEFAULT);
        long bitrate = settings_.GetValueOrDefault(Settings.BITRATE_KEY, Settings.BITRATE_DEFAULT);

        //for now always attach width/height for compatibility with dvblink servers
        //tvmosaic server actually uses scale
        Point pt = new Point();
        getWindowManager().getDefaultDisplay().getSize(pt);
        int height = (int) (pt.y / scale_factor);
        int width = (int) (pt.x / scale_factor);

        url += "&transcoder=hls&client_id=" + settings_.getClientID() + "&width=" + width + "&height=" + height
                + "&bitrate=" + bitrate + "&scale=" + scale_factor;

        exoPlayer = new DemoPlayer(getRendererBuilder(url));
        exoPlayer.addListener(this);

        int resume_position = getIntent().getIntExtra("resume_position", 0);
        exoPlayer.seekTo(resume_position * 1000);
        exoPlayer.prepare();
        playerControl = exoPlayer.getPlayerControl();

//        timeListener = new PlayActivity.TimeListener();

        exoPlayer.setSurface(surfaceView.getHolder().getSurface());
        exoPlayer.setPlayWhenReady(true);
    }

    private void releasePlayer() {
        if (exoPlayer != null) {
            exoPlayer.release();
            exoPlayer = null;
        }
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.play_transcoded_recording_activity);

        getWindow().getDecorView().setBackgroundColor(Color.BLACK);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        toast = new Toast(_context);

        if (!Utils.usesRemoteControl(this)) {
            findViewById(R.id.help_line).setVisibility(View.GONE);

            View hbc = findViewById(R.id.hor_bar_container);
            hbc.setPadding(
                    (int)getResources().getDimension(R.dimen.play_live_progress_ll_padding_left_touch),
                    hbc.getPaddingTop(),
                    (int)getResources().getDimension(R.dimen.play_live_progress_ll_padding_right_touch),
                    hbc.getPaddingBottom());

            ImageButton ib = (ImageButton) findViewById(R.id.stop_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getCurrentPlaybackPos();
                    finish();
                }
            });

            ib = (ImageButton) findViewById(R.id.pause_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ImageButton ib = (ImageButton)v;
                    if (togglePause()) {
                        ib.setImageResource(R.drawable.tvm_play_play);
                    } else {
                        ib.setImageResource(R.drawable.tvm_play_pause);
                    }
                }
            });

            ib = (ImageButton) findViewById(R.id.rewind_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnTouchListener(new View.OnTouchListener() {

                private Handler mHandler;

                @Override public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (mHandler != null)
                                return true;
                            //do once and start timer after 300 ms to repeat
                            rewind();
                            mHandler = new Handler();
                            mHandler.postDelayed(mAction, 300);
                            break;
                        case MotionEvent.ACTION_UP:
                            if (mHandler == null) return true;
                            mHandler.removeCallbacks(mAction);
                            mHandler = null;
                            break;
                    }
                    return false;
                }

                Runnable mAction = new Runnable() {
                    @Override public void run() {
                        rewind();
                        mHandler.postDelayed(this, 100);
                    }
                };

            });

            ib = (ImageButton) findViewById(R.id.forward_button);
            ib.setVisibility(View.VISIBLE);
            ib.setOnTouchListener(new View.OnTouchListener() {

                private Handler mHandler;

                @Override public boolean onTouch(View v, MotionEvent event) {
                    switch(event.getAction()) {
                        case MotionEvent.ACTION_DOWN:
                            if (mHandler != null)
                                return true;
                            //do once and start timer after 300 ms to repeat
                            forward();
                            mHandler = new Handler();
                            mHandler.postDelayed(mAction, 300);
                            break;
                        case MotionEvent.ACTION_UP:
                            if (mHandler == null) return true;
                            mHandler.removeCallbacks(mAction);
                            mHandler = null;
                            break;
                    }
                    return false;
                }

                Runnable mAction = new Runnable() {
                    @Override public void run() {
                        forward();
                        mHandler.postDelayed(this, 100);
                    }
                };

            });
        }


        TextView tv;
        tv = (TextView) findViewById(R.id.audio_title_);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_AUDIO));
        tv = (TextView) findViewById(R.id.subtitles_title_);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SUBTITLES));
        tv = (TextView) findViewById(R.id.help_line);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_PLAY_HELP_TEXT));

        paramPanel = (LinearLayout) findViewById(R.id.param_panel);
        subtitleList = (GridView) paramPanel.findViewById(R.id.subtitles_list_);
        audioTrackList = (GridView) paramPanel.findViewById(R.id.audio_list_);
        currentTime = (TextView) findViewById(R.id.current_time);
        infoPanel = (LinearLayout) findViewById(R.id.info_panel);
        programSeek = (CustomSeekBar) findViewById(R.id.program_seek);

        programSeek.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                return true;
            }
        });
        programSeek.setMax(SEEK_MAX_VALUE);
        programSeek.setProgress(0);
        programSeek.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                startHideInfoPanelTimer();

                if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }

                if (backPressHandle(keyCode, keyEvent)) {
                    return true;
                }

                if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {
                    rewind();
                    return true;
                }
                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                    forward();
                    return true;
                }
                if (Common.okPressed(keyCode)) {
                    togglePause();
                    return true;
                }
                return false;
            }
        });
        programSeek.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                enableInfoPanel(true);
                if (focus) {
                    startHideInfoPanelTimer();
                }
            }
        });
        programSeek.setOnLongPress(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    showParamPanel();
                    return true;
                }
                return false;
            }
        });

        prepareInputParams();
        preparePlayerViews();
        setRefreshRate();
        prepareInfoPanel();

        state_ = state_ready;
        activateProgressBarFullScreen(false);
    }

    protected void getCurrentPlaybackPos() {
        try {
            if (playerControl != null) {
                new_resume_position_ = (int) playerControl.getCurrentPosition() / 1000;

                //if we are in the last 5 seconds - reset it to 0
                if (new_resume_position_ + 5 > recording_.VideoInfo.Duration)
                    new_resume_position_ = 0;
            }

        } catch (Exception e) {
            new_resume_position_ = 0;
        }
    }

    private boolean backPressHandle(int keyCode, KeyEvent keyEvent) {
        if (Common.backPressed(keyCode, keyEvent)) {
            if (infoPanelHidden() && paramPanelHidden()) {
                getCurrentPlaybackPos();
                finish();
            } else {
                hidePanels();
            }
            return true;
        }
        return false;
    }

    private boolean togglePause() {
        if (unPause()) {
            return false;
        }
        playerControl.pause();

        return true;
    }

    private boolean unPause() {
        if (!playerControl.isPlaying()) {
            playerControl.start();
            return true;
        }
        return false;
    }

    private void startStatsTimer() {
        stopStatsTimer();
        getStatsTimer = new Timer();
        getStatsTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        requestStats();
                    }
                });
            }
        }, 0, TIME_SHIFT_STATS_TIMER_INTERVAL);
    }

    private void stopStatsTimer() {
        if (getStatsTimer == null) {
            return;
        }
        getStatsTimer.cancel();
        getStatsTimer.purge();
        getStatsTimer = null;
    }

    private void UpdatePlayStats() {
        int pos_seconds = 0;
        int percN = 0;
        if (playerControl != null) {
            int duration = recording_.VideoInfo.Duration;

            pos_seconds = (int) playerControl.getCurrentPosition() / 1000;
            percN = duration > 0 ? calcPerN(pos_seconds, duration, SEEK_MAX_VALUE) : 0;
        }
        programSeek.setProgress(percN);
        updateCurrentPlaybackPosition(pos_seconds);
    }

    // region Server communication TimeShift
    private void requestStats() {
        // Log.i(TAG, "requestStats");
        synchronized (_started) {
            if (!_started) {
                return;
            }
        }
        if (state_ != state_ready) {
            return;
        }

        try {
            Pair<Integer, Object> pair = queue.poll();
            if (pair == null) {
                UpdatePlayStats();
            } else {
                switch (pair.first) {
                    case state_seek:
                        Pair<Integer, Object> pair1 = queue.peek();
                        while (pair1 != null && pair1.first == state_seek) {
                            pair = queue.poll();
                            pair1 = queue.peek();
                        }

                        float t = Float.parseFloat(pair.second.toString());
                        Log.i(TAG, String.format("SetPos: %f", t));
                        playerControl.seekTo((int)(t * 1000));
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(TAG, "requestStats", e);
        }
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        if (hasFocus && !ignoreWindowFocusChange) {
            if (Utils.usesRemoteControl(this)) {
                showInfoPanel();
                infoPanel.requestFocus();
            }
            ignoreWindowFocusChange = false;
        }
    }

    @Override
    public void finish() {
        Intent i = getIntent();
        i.putExtra("resume_position", new_resume_position_);
        setResult(RESULT_FIRST_USER, i);

        super.finish();
    }

    @Override
    protected void onDestroy() {
        stopStatsTimer();
        // playback complete, give up audio focus
        audioManager.abandonAudioFocus(audioFocusChangeListener);
        //unregister media buttons
        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
        releasePlayer();
        super.onDestroy();
    }

    @Override
    protected void onPause() {
        super.onPause();
        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
        if (playerControl != null)
            playerControl.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        audioManager = (AudioManager) _context.getSystemService(AUDIO_SERVICE);
        audioFocusChangeListener = new AudioManager.OnAudioFocusChangeListener() {
            @Override
            public void onAudioFocusChange(int focusChange) {
                switch (focusChange) {
                    case AudioManager.AUDIOFOCUS_GAIN:
                        // continue playback and raise volume (if it was previously lowered)
                        audioManager.registerMediaButtonEventReceiver(mediaButtonReceiver);
                        if (playerControl != null && !playerControl.isPlaying()) {
                            playerControl.start();
                        }
                        break;

                    case AudioManager.AUDIOFOCUS_LOSS:
                        // stop playback, deregister buttons, clean up
                        if (playerControl != null && playerControl.isPlaying())
                            playerControl.pause();
                        audioManager.unregisterMediaButtonEventReceiver(mediaButtonReceiver);
                        break;
                }
            }
        };
        int result = audioManager.requestAudioFocus(audioFocusChangeListener, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

        if (result == AudioManager.AUDIOFOCUS_REQUEST_GRANTED) {
            mediaButtonReceiver = new ComponentName(getPackageName(), MediaButtonReceiver.class.getName());
            audioManager.registerMediaButtonEventReceiver(mediaButtonReceiver);
            // start playback
        }

        if (playerControl != null) {
            playerControl.start();
        } else {
            startPlayback();
        }
    }

    @Override
    public boolean onKeyLongPress(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            if (!infoPanelShowing) {
                showParamPanel();
                return true;
            }
        }
        return super.onKeyLongPress(keyCode, event);
    }

    protected void hidePanels() {
        if (!paramPanelHidden()) {
            hideParamPanel();

            if (!infoPanelHidden())
                programSeek.requestFocus();
        }
        hideInfoPanel();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (backPressHandle(keyCode, event)) {
            return true;
        }

        boolean isDown = event.getAction() == KeyEvent.ACTION_DOWN;

        if (isDown) {
            if (keyCode == KeyEvent.KEYCODE_VOLUME_DOWN ||
                    keyCode == KeyEvent.KEYCODE_VOLUME_UP ||
                    keyCode == KeyEvent.KEYCODE_VOLUME_MUTE) {
                return super.onKeyDown(keyCode, event);
            }
        }

        if (infoPanelHidden()) {
            showInfoPanel();
        }
        startHideInfoPanelTimer();

        if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
            event.startTracking();
            return true;
        }

        if (isDown) {
            if (keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                forward();
                return true;
            }
            if (keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {
                rewind();
                return true;
            }
            if (keyCode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
                togglePause();
                return true;
            }
        }

        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {
        if (exoPlayer != null) {
            exoPlayer.setSurface(holder.getSurface());
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {
        // Do nothing.
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        if (exoPlayer != null) {
            exoPlayer.blockingClearSurface();
        }
    }

    // DemoPlayer.Listener implementation
    @Override
    public void onStateChanged(boolean playWhenReady, int playbackState) {
        if (playbackState == ExoPlayer.STATE_ENDED) {
            finish();
        }
        if (playbackState == ExoPlayer.STATE_READY) {
            fillMenu();
            _started = true;
        }
    }

    @Override
    public void onError(Exception e) {
        releasePlayer();
        finish();
    }

    @Override
    public void onVideoSizeChanged(int width, int height, int unappliedRotationDegrees,
                                   float pixelWidthAspectRatio) {
        videoFrame.setAspectRatio(
                height == 0 ? 1 : (width * pixelWidthAspectRatio) / height);
    }

}


