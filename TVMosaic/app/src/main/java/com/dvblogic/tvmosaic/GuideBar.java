package com.dvblogic.tvmosaic;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;

import TVMosaic.TVMosaic.R;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.TextUtils.TruncateAt;
import android.util.Pair;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.View.OnFocusChangeListener;
import android.view.animation.Animation;
import android.widget.LinearLayout;;import com.dvblogic.dvblink_common.Program;
import com.dvblogic.dvblink_common.ProgramsList;
import com.dvblogic.dvblink_common.Recommendation;
import com.dvblogic.dvblink_common.RecommendationProgram;
import com.dvblogic.dvblink_common.Utils;

public class GuideBar extends View implements OnKeyListener, OnFocusChangeListener {

    protected class GuideBarItem {
        public boolean is_empty_ = true;
        public int start_x_;
        public int end_x_;
        public String time_;
        public boolean is_conflict;
        public int color_;
        public boolean color_valid_;
        private boolean _highlighted = false;
        Program prg_;

        public GuideBarItem(Context context, Program prg, int start_x, int end_x) {
            color_valid_ = true;
            start_x_ = start_x;
            end_x_ = end_x;

            if (prg != null) {
                prg_ = prg;
                is_empty_ = false;

                color_valid_ = Constants.HasGenreColor(prg);
                color_ = ContextCompat.getColor(getContext(), Constants.GetColorFromGenre(prg));

                time_ = String.format("%s - %s",
                        Utils.UtcToShortTimeString(parent_activity_, prg.StartTime),
                        Utils.UtcToShortTimeString(parent_activity_, prg.StartTime + prg.Duration));
            } else {
                is_empty_ = true;
                long epg_start_time = TimeLineBuilder.startTime();
                long epg_end_time = TimeLineBuilder.endTime();
                prg_ = new Program();
                prg_.StartTime = epg_start_time;
                prg_.Duration = (int)(epg_end_time - epg_start_time);
                prg_.Title = parent_activity_.language_manager_.GetString(LocaleStrings.IDS_NO_PROGINFO_AVAILABLE);
            }
        }

        public GuideBarItem(Context context, long start_time, long end_time, int start_x, int end_x) {
            color_valid_ = true;
            is_empty_ = true;
            start_x_ = start_x;
            end_x_ = end_x;
            prg_ = new Program();
            prg_.StartTime = start_time;
            prg_.Duration = (int)(end_time - start_time);
            prg_.Title = parent_activity_.language_manager_.GetString(LocaleStrings.IDS_NO_PROGINFO_AVAILABLE);
        }

        public void setConflict(boolean conflict) {
            is_conflict = conflict;
        }
        public void highlight(boolean state) { _highlighted  = state; }
        public boolean highlighted() {
            return _highlighted;
        }
    }

    class FindProgramParams {
        public int idx;
        public GuideBarItem gbi;
    }

    class GetProgramsToDrawParams {
        public int idx;
        public int count;
    }

    final int fast_access_step = 10;

    float sep_width_;
    int genre_bar_width_;
    int top_bottom_offs_;
    int left_right_offs_;

    Hashtable<Integer, Integer> fast_access_map_;
    ArrayList<GuideBarItem> items_;
    public String channel_id_;
    Paint paint_ = new Paint();;
    Rect clip_rect_ = new Rect();
    Rect item_rect_ = new Rect();
    Rect draw_rect_ = new Rect();
    Rect guide_bar_rect_ = new Rect();
    TextPaint text_paint_ = new TextPaint();

    GuideActivity parent_activity_;
    Context context_;
    HashMap<String, RecommendationProgram> recommendation_map = new HashMap<String, RecommendationProgram>();

    public GuideBar(Context context, GuideActivity parent_activity, String channel_id, ProgramsList programs) {
        super(context);

        setOnFocusChangeListener(this);
        setOnKeyListener(this);
        setFocusable(true);
        setBackgroundColor(Color.TRANSPARENT);

        context_ = context;
        parent_activity_ = parent_activity;

        sep_width_ = getContext().getResources().getDimension(R.dimen.separator_width);
        genre_bar_width_ = getContext().getResources().getDimensionPixelSize(R.dimen.guide_genre_bar_width);
        top_bottom_offs_ = getContext().getResources().getDimensionPixelSize(R.dimen.channel_contents_top_bottom_offs);
        left_right_offs_ = getContext().getResources().getDimensionPixelSize(R.dimen.channel_contents_left_right_offs);

        channel_id_ = channel_id;

        items_ = new ArrayList<GuideBarItem>();
        fast_access_map_ = new Hashtable<Integer, Integer>();

        insertEpg(programs);
    }

    public GuideBar(Context context) {
        super(context);
        setOnFocusChangeListener(this);
        setOnKeyListener(this);
        setFocusable(true);
        setBackgroundColor(Color.TRANSPARENT);
    }

    @Override
    public boolean performClick() {
        return super.performClick();
    }

    public long GetBarStartTime() {
        return items_.size() == 0 ? -1 : items_.get(0).prg_.StartTime;
    }

    public long GetBarEndTime() {
        return items_.size() == 0 ? -1 : items_.get(items_.size() - 1).prg_.StartTime + items_.get(items_.size() - 1).prg_.Duration;
    }

    public GuideBarItem GetProgramForX(int x) {
        GuideBarItem ret_val = null;

        GetProgramsToDrawParams gpd = GetProgramsToDraw(x, x);
        int idx_start = gpd.idx;

        if (idx_start != -1) {
            GuideBarItem gbi = items_.get(idx_start);
            if (!gbi.is_empty_)
                ret_val = gbi;
        }

        return ret_val;
    }

    GetProgramsToDrawParams get_programs_to_draw_params_ = new GetProgramsToDrawParams();

    protected GetProgramsToDrawParams GetProgramsToDraw(int wnd_start, int wnd_end) // out parameters
    // applied
    {

        int idx_start = -1;
        int idx_count = 0;

        int s_idx = -1;
        int e_idx = -1;

        if (wnd_start <= items_.get(0).start_x_) {
            s_idx = 0;
        } else {
            int s = wnd_start - (wnd_start % fast_access_step);
            Integer tmp = fast_access_map_.get(s);
            if (tmp != null)
                s_idx = tmp;
        }

        if (wnd_end >= items_.get(items_.size() - 1).end_x_) {
            e_idx = items_.size() - 1;
        } else {
            int e = wnd_end - (wnd_end % fast_access_step) + fast_access_step;
            Integer tmp = fast_access_map_.get(e);
            if (tmp != null)
                e_idx = tmp;
        }

        if (s_idx != -1 && e_idx != -1) {
            idx_start = s_idx;
            idx_count = e_idx - s_idx + 1;
        }

        get_programs_to_draw_params_.idx = idx_start;
        get_programs_to_draw_params_.count = idx_count;
        return get_programs_to_draw_params_;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        getLocalVisibleRect(clip_rect_);
        GetProgramsToDrawParams gpd = GetProgramsToDraw(clip_rect_.left, clip_rect_.right);
        int idx_start = gpd.idx;
        int count = gpd.count;
        for (int i = idx_start; i < idx_start + count; i++) {
            GuideBarItem item = items_.get(i);
            if (item.is_empty_)
                DrawEmptyGuideItem(item, canvas);
            else
                DrawGuideItem(item, canvas);
        }
    }

    boolean isAdviser() {
        return parent_activity_.isAdviserMode();
    }

    protected void DrawEmptyGuideItem(GuideBarItem gbi, Canvas canvas) {
/*
        Rect parent_rect = new Rect();
        parent_activity_.epg_container.getLocalVisibleRect(parent_rect);
        if (!Rect.intersects(parent_rect, drawRect))
            return;

        Rect item_rect = new Rect(gbi.start_x_, drawRect.top, gbi.end_x_, drawRect.bottom);
        if (!Rect.intersects(parent_rect, item_rect))
            return;
*/

        item_rect_.set(gbi.start_x_, 0, gbi.end_x_, this.getHeight() - 1);

        paint_.setAntiAlias(false);

        //draw line around the item
        if (!isAdviser()) {
            paint_.setStyle(Paint.Style.STROKE);
            paint_.setStrokeWidth(sep_width_);
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_separator));

            canvas.drawLine(gbi.start_x_, 0, gbi.end_x_, 0, paint_); //top hor
            canvas.drawLine(gbi.end_x_, 0, gbi.end_x_, this.getHeight() - 1, paint_); //right ver
            canvas.drawLine(gbi.start_x_, this.getHeight() - 1, gbi.end_x_, this.getHeight() - 1, paint_); //bottom hor
            canvas.drawLine(gbi.start_x_, 0, gbi.start_x_, this.getHeight() - 1, paint_); //left ver
        }

        guide_bar_rect_.set(item_rect_.left + left_right_offs_, item_rect_.top + top_bottom_offs_, item_rect_.right - left_right_offs_, item_rect_.bottom - top_bottom_offs_);
        // set new clip bounds
        canvas.save();
        canvas.clipRect(guide_bar_rect_);

        //draw underlying guide bar
        paint_.setStyle(Style.FILL_AND_STROKE);
        if (gbi.highlighted())
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_bar_bkg_selected));
        else
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_bar_bkg));
        canvas.drawRect(guide_bar_rect_, paint_);

        getLocalVisibleRect(draw_rect_);
        int text_start = Math.max(guide_bar_rect_.left, draw_rect_.left) + genre_bar_width_ ;
        int text_end = guide_bar_rect_.right - genre_bar_width_;
        clip_rect_.set(text_start, guide_bar_rect_.top, text_end, guide_bar_rect_.bottom);
        canvas.clipRect(guide_bar_rect_);

        paint_.setAntiAlias(true);
        paint_.setTypeface(CustomFont.getInstance(getContext()).getTypeFace());
        paint_.setStyle(Style.FILL);
        paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_bar_no_epg_text));
        paint_.setTextSize(getContext().getResources().getDimensionPixelSize(R.dimen.guide_bar_text_size_mid));

        float avail = clip_rect_.width();
        text_paint_.set(paint_);
        String ellipsize = TextUtils.ellipsize(gbi.prg_.Title, text_paint_, avail, TruncateAt.END).toString();

        float text_spacing = paint_.getTextSize();
        canvas.drawText(ellipsize, clip_rect_.left, clip_rect_.bottom - (clip_rect_.height() - text_spacing) / 2, paint_);

        canvas.restore();

        if (isAdviser()) {
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.new_primary_translucent));
            canvas.drawRect(item_rect_, paint_);
        }

    }

    protected void DrawGuideItem(GuideBarItem gbi, Canvas canvas) {

        item_rect_.set(gbi.start_x_, 0, gbi.end_x_, this.getHeight() - 1);

        paint_.setAntiAlias(false);

        //draw line around the item
        if (!isAdviser()) {
            paint_.setStyle(Paint.Style.STROKE);
            paint_.setStrokeWidth(sep_width_);
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_separator));


            canvas.drawLine(gbi.start_x_, 0, gbi.end_x_, 0, paint_); //top hor
            canvas.drawLine(gbi.end_x_, 0, gbi.end_x_, this.getHeight() - 1, paint_); //right ver
            canvas.drawLine(gbi.start_x_, this.getHeight() - 1, gbi.end_x_, this.getHeight() - 1, paint_); //bottom hor
            canvas.drawLine(gbi.start_x_, 0, gbi.start_x_, this.getHeight() - 1, paint_); //left ver
        }

        guide_bar_rect_.set(item_rect_.left + left_right_offs_, item_rect_.top + top_bottom_offs_, item_rect_.right - left_right_offs_, item_rect_.bottom - top_bottom_offs_);
        // set new clip bounds
        canvas.save();
        canvas.clipRect(guide_bar_rect_);

        //draw underlying guide bar
        paint_.setStyle(Style.FILL_AND_STROKE);
        if (gbi.highlighted())
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_bar_bkg_selected));
        else
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_bar_bkg));
        canvas.drawRect(guide_bar_rect_, paint_);

        //draw line in the middle
        paint_.setStyle(Style.STROKE);
        paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_separator));
        canvas.drawLine(guide_bar_rect_.left, item_rect_.height() / 2, guide_bar_rect_.right, item_rect_.height() / 2, paint_);

        int genre_bar_width = 0;
        //draw genre bar
        if (gbi.color_valid_ && useProgramColors()) {
            draw_rect_.set(guide_bar_rect_.left, guide_bar_rect_.top, guide_bar_rect_.left + genre_bar_width_, guide_bar_rect_.bottom);
            paint_.setStyle(Style.FILL_AND_STROKE);
            paint_.setColor(gbi.color_);
            canvas.drawRect(draw_rect_, paint_);
            genre_bar_width = genre_bar_width_;
        }

        getLocalVisibleRect(draw_rect_);
        int text_start = Math.max(guide_bar_rect_.left, draw_rect_.left) + genre_bar_width_ + genre_bar_width;

        //inner part, where texts and icons are located
        clip_rect_.set(text_start, guide_bar_rect_.top, guide_bar_rect_.right - genre_bar_width_, guide_bar_rect_.bottom);
        canvas.clipRect(clip_rect_);

        //program name
        int text_color;
        float text_size;
        float extra_text_size;
        if (gbi.highlighted()) {
            text_color = ContextCompat.getColor(getContext(), R.color.guide_bar_text_selected);
            text_size = getContext().getResources().getDimensionPixelSize(R.dimen.guide_bar_text_size_large);
            extra_text_size = getContext().getResources().getDimensionPixelSize(R.dimen.guide_bar_text_size_mid);
        } else {
            text_color = ContextCompat.getColor(getContext(), R.color.guide_bar_text);
            text_size = getContext().getResources().getDimensionPixelSize(R.dimen.guide_bar_text_size_mid);
            extra_text_size = getContext().getResources().getDimensionPixelSize(R.dimen.guide_bar_text_size_small);
        }

        paint_.setAntiAlias(true);
        paint_.setTypeface(CustomFont.getInstance(getContext()).getTypeFace_Bold());
        paint_.setStyle(Style.FILL);
        paint_.setColor(text_color);
        paint_.setTextSize(text_size);

        if (gbi.prg_.Title != null) {
            text_paint_.set(paint_);
            float avail = clip_rect_.width();
            String ellipsize = TextUtils.ellipsize(gbi.prg_.Title, text_paint_, avail, TruncateAt.END).toString();

            float text_spacing = paint_.getTextSize();
            canvas.drawText(ellipsize, clip_rect_.left, clip_rect_.top + text_spacing + (clip_rect_.height() / 2 - text_spacing) / 2, paint_);
        }

        //time/episode/season

        String strTime = gbi.time_;
        String ES = "";
        if (gbi.prg_.EpisodeNum != 0)
            ES += String.format("E%d", gbi.prg_.EpisodeNum);
        if (gbi.prg_.SeasonNum != 0)
            ES += String.format("S%d", gbi.prg_.SeasonNum);
        if (!ES.isEmpty()) {
            strTime += " | " + ES;
        }

        paint_.setTypeface(CustomFont.getInstance(getContext()).getTypeFace());
        paint_.setTextSize(extra_text_size);

        int bmp_height = (int) (clip_rect_.height() * 0.25); //bitmap takes 50% of the half of the clip_rect
        //we assume here that all bimtaps are of the same size
        int bmp_width = (int) ((bmp_height / (double) parent_activity_.hd_bmp_.getHeight()) * parent_activity_.hd_bmp_.getWidth());

        int icon_sep_width = getContext().getResources().getDimensionPixelSize(R.dimen.guide_bar_bmp_separator_width);

        int wd_rec = (gbi.prg_.IsRecord) ? bmp_width : 0;
        int wd_hd = (gbi.prg_.IsHdtv) ? bmp_width : 0;

        float avail = clip_rect_.width() - wd_hd - wd_rec - (wd_hd != 0 && wd_rec != 0 ? icon_sep_width : 0);

        text_paint_.set(paint_);
        String ellipsize = TextUtils.ellipsize(strTime, text_paint_, avail, TruncateAt.END).toString();
        float text_spacing = paint_.getTextSize();
        canvas.drawText(ellipsize, clip_rect_.left, clip_rect_.top + clip_rect_.height() - (clip_rect_.height() / 2 - text_spacing) / 2, paint_);

        paint_.setAntiAlias(false);

        //draw bitmaps
        if (wd_hd > 0 || wd_rec > 0) {

            Bitmap bmp = null;
            if (wd_rec > 0) {
                if (gbi.prg_.IsRecordConflict)
                    bmp = parent_activity_.rec_conflict_bmp_;
                else
                    if (gbi.prg_.IsRecord && gbi.prg_.IsRepeatRecord)
                        bmp = parent_activity_.rec_series_bmp_;
                else
                if (gbi.prg_.IsRecord)
                    bmp = parent_activity_.rec_bmp_;
            }

            draw_rect_.set(clip_rect_.right - bmp_width, item_rect_.height() / 2 + (guide_bar_rect_.height() / 2 - bmp_height) / 2, clip_rect_.right, guide_bar_rect_.bottom - (guide_bar_rect_.height() / 2 - bmp_height) / 2);
            if (wd_rec > 0)
                canvas.drawBitmap(bmp, null, draw_rect_, paint_);
            else
                canvas.drawBitmap(parent_activity_.hd_bmp_, null, draw_rect_, paint_);

            if (wd_hd > 0 && wd_rec > 0) {
                //draw second bmp
                draw_rect_.set(draw_rect_.left - bmp_width - icon_sep_width, draw_rect_.top, draw_rect_.right - bmp_width - icon_sep_width, draw_rect_.bottom);
                canvas.drawBitmap(parent_activity_.hd_bmp_, null, draw_rect_, paint_);
            }
        }

        canvas.restore();
        if (isAdviser()) {
            RecommendationProgram recommendationProgram = null;
            if (recommendation_map.containsKey(gbi.prg_.ID)) {
                recommendationProgram = recommendation_map.get(gbi.prg_.ID);
            }

            if (recommendationProgram != null && recommendationProgram.Rating >= 0) {
                /*
                paint_.setStyle(Paint.Style.STROKE);
                paint_.setStrokeWidth(sep_width_);
                paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_separator));
                canvas.drawLine(gbi.end_x_-1, 0, gbi.end_x_-1, this.getHeight() - 1, paint_); //right vert
                canvas.drawText(
                        Float.toString(recommendationProgram.Score),
                        clip_rect_.right - 25,
                        clip_rect_.top + clip_rect_.height() - (clip_rect_.height() / 2 - text_spacing) / 2,
                        paint_
                );
                */
            } else {
                if (!gbi.highlighted()) {
                    item_rect_.set(gbi.start_x_-1, 0, gbi.end_x_ + 0, this.getHeight());
                    paint_.setColor(ContextCompat.getColor(getContext(), R.color.new_primary_translucent));
                    canvas.drawRect(item_rect_, paint_);
                }
            }
        }
    }

    public void clearRecommendations() {
        recommendation_map.clear();
        invalidate();
    }

    public void setRecommendations(Recommendation recommendation) {
        recommendation_map.clear();
        if (recommendation != null) {
            for (RecommendationProgram recommendationProgram : recommendation.Programs) {
                recommendation_map.put(recommendationProgram.Event.ID, recommendationProgram);
            }
        }
        this.invalidate();
    }

    protected boolean useProgramColors() {
        return parent_activity_.useProgramColors();
    }

    public void clearConflicts() {
        for (int idx = 0; idx < items_.size(); idx++) {
            GuideBarItem item = items_.get(idx);
            item.is_conflict = false;
        }
    }

    public void setConflict(String program_id, boolean conflict) {
        for (int idx = 0; idx < items_.size(); idx++) {
            GuideBarItem item = items_.get(idx);

            if (item.prg_.ID == null) {
                continue;
            }

            if (item.prg_.ID.equals(program_id)) {
                item.is_conflict = conflict;
                break;
            }
        }
    }

    final FindProgramParams find_program_params_ = new FindProgramParams();
    protected FindProgramParams findProgram(Program prg) {

        for (int idx = 0; idx < items_.size(); idx++) {
            GuideBarItem item = items_.get(idx);
            if (item.prg_.ID == null)
                return null;
            if (item.prg_.ID.equals(prg.ID)) {
                find_program_params_.idx = idx;
                find_program_params_.gbi = item;
                return find_program_params_;
            }
        }
        return null;
    }

    public void insertEpg(ProgramsList programs) {
        if (programs == null || programs.size() == 0) {
            long start_time = TimeLineBuilder.startTime();
            long end_time = TimeLineBuilder.endTime();
            // insert epg item
            int x1 = TimeLineBuilder.GridTimePxPosition(context_, start_time);
            int x2 = TimeLineBuilder.GridTimePxPosition(context_, end_time);
            GuideBarItem i = new GuideBarItem(context_, null, x1, x2);
            items_.add(i);
        } else {
            Collections.sort(programs.list(), new Comparator<Program>() {
                public int compare(Program prg1, Program prg2) {
                    return (int) (prg1.StartTime - prg2.StartTime);
                }
            });
            for (Program prg : programs.list()) {
                long start_time = prg.StartTime;
                long end_time = prg.StartTime + prg.Duration;
                // insert epg item
                int x1 = TimeLineBuilder.GridTimePxPosition(context_, start_time);
                int x2 = TimeLineBuilder.GridTimePxPosition(context_, end_time);
                GuideBarItem i = new GuideBarItem(context_, prg, x1, x2);
                items_.add(i);
            }
        }
        addEmptyItems();

        invalidate();
//        requestLayout();

        // build fast access map
        for (int prg_idx = 0; prg_idx < items_.size(); prg_idx++) {
            int start_idx = items_.get(prg_idx).start_x_ - (items_.get(prg_idx).start_x_ % fast_access_step);
            int end_idx = items_.get(prg_idx).end_x_ - (items_.get(prg_idx).end_x_ % fast_access_step);
            for (int i = start_idx; i <= end_idx; i += fast_access_step) {
                if (i >= items_.get(prg_idx).start_x_ && i <= items_.get(prg_idx).end_x_)
                    fast_access_map_.put(i, prg_idx);
            }
        }
    }

    public void addEpg(ProgramsList programs) {
        if (programs == null || programs.size() == 0)
            return;

        //remove all empty items
        for (int idx=0; idx<items_.size(); idx++) {
            GuideBarItem item = items_.get(idx);
            if (item.is_empty_) {
                items_.remove(idx);
                //restart
                idx = (idx - 1) < 0 ? 0 : idx - 1;
            }
        }

        for (Program prg : programs.list()) {
            long start_time = prg.StartTime;
            long end_time = prg.StartTime + prg.Duration;
            int x1 = 0;
            int x2 = 0;
            FindProgramParams fpp = findProgram(prg);
            if (fpp != null) {
                if (start_time != fpp.gbi.prg_.StartTime || end_time != (fpp.gbi.prg_.StartTime + fpp.gbi.prg_.Duration)) {
                    start_time = Math.min(start_time, fpp.gbi.prg_.StartTime);
                    end_time = Math.max(end_time, fpp.gbi.prg_.StartTime + fpp.gbi.prg_.Duration);
                    x1 = TimeLineBuilder.GridTimePxPosition(context_, start_time);
                    x2 = TimeLineBuilder.GridTimePxPosition(context_, end_time);
                    fpp.gbi.prg_.StartTime = start_time;
                    fpp.gbi.prg_.Duration = (int)(end_time - start_time);
                    fpp.gbi.start_x_ = x1;
                    fpp.gbi.end_x_ = x2;
                }
                fpp.gbi.prg_ = prg;
                items_.set(fpp.idx, fpp.gbi);
            } else {
                x1 = TimeLineBuilder.GridTimePxPosition(context_, start_time);
                x2 = TimeLineBuilder.GridTimePxPosition(context_, end_time);
                items_.add(new GuideBarItem(context_, prg, x1, x2));
            }
        }

        Collections.sort(items_, new Comparator<GuideBarItem>() {
            public int compare(GuideBarItem i1, GuideBarItem i2) {
                return (int) (i1.prg_.StartTime - i2.prg_.StartTime);
            }
        });

        addEmptyItems();

        invalidate();
//        requestLayout();

        // build fast access map
        fast_access_map_.clear();
        for (int prg_idx = 0; prg_idx < items_.size(); prg_idx++) {
            int start_idx = items_.get(prg_idx).start_x_ - (items_.get(prg_idx).start_x_ % fast_access_step);
            int end_idx = items_.get(prg_idx).end_x_ - (items_.get(prg_idx).end_x_ % fast_access_step);
            for (int i = start_idx; i <= end_idx; i += fast_access_step) {
                if (i >= items_.get(prg_idx).start_x_ && i <= items_.get(prg_idx).end_x_)
                    fast_access_map_.put(i, prg_idx);
            }
        }
    }

    public void refreshEpg(ProgramsList programs) {
        for (Program prg : programs.list()) {
            FindProgramParams fpp = findProgram(prg);
            if (fpp != null) {
                fpp.gbi.prg_.IsPremiere = prg.IsPremiere;
                fpp.gbi.prg_.IsRecord = prg.IsRecord;
                fpp.gbi.prg_.IsRepeatRecord = prg.IsRepeatRecord;
                fpp.gbi.prg_.IsRepeat = prg.IsRepeat;
            }
        }
        invalidate();
//        requestLayout();
    }

    protected void addEmptyItems() {

        ArrayList<GuideBarItem> empty_items = new ArrayList<GuideBarItem>();

        long epg_start_time = TimeLineBuilder.startTime();
        long epg_end_time = TimeLineBuilder.endTime();
        long start_time = 0;
        long end_time = 0;
        int x1 = 0;
        int x2 = 0;
        int count = items_.size();
        if (count == 0) {
            start_time = epg_start_time;
            end_time = epg_end_time;
            x1 = TimeLineBuilder.GridTimePxPosition(context_, start_time);
            x2 = TimeLineBuilder.GridTimePxPosition(context_, end_time);
            empty_items.add(new GuideBarItem(context_, start_time, end_time, x1, x2));
        } else if (count == 1) {
            GuideBarItem i = items_.get(0);
            if (i.prg_.StartTime > epg_start_time) {
                empty_items.add(new GuideBarItem(context_, epg_start_time, i.prg_.StartTime, 0, i.start_x_));
            }
            long item_end_time = i.prg_.StartTime + i.prg_.Duration;
            if (item_end_time < epg_end_time) {
                start_time = item_end_time;
                end_time = epg_end_time;
                x1 = i.end_x_;
                x2 = TimeLineBuilder.GridTimePxPosition(context_, end_time);
                empty_items.add(new GuideBarItem(context_, start_time, end_time, x1, x2));
            }
        } else {
            GuideBarItem i = items_.get(0);
            if (i.prg_.StartTime > epg_start_time) {
                empty_items.add(new GuideBarItem(context_, epg_start_time, i.prg_.StartTime, 0, i.start_x_));
            }
            for (int index = 0; index < items_.size() - 1; index++) {
                GuideBarItem i1 = items_.get(index);
                GuideBarItem i2 = items_.get(index + 1);
                if (i1.end_x_ != i2.start_x_) {
                    start_time = i1.prg_.StartTime + i1.prg_.Duration;
                    end_time = i2.prg_.StartTime;
                    x1 = i1.end_x_;
                    x2 = i2.start_x_;
                    empty_items.add(new GuideBarItem(context_, start_time, end_time, x1, x2));
                }
            }
            i = items_.get(items_.size() - 1);
            long item_end_time = i.prg_.StartTime + i.prg_.Duration;
            if (item_end_time < epg_end_time) {
                start_time = item_end_time;
                end_time = epg_end_time;
                x1 = i.end_x_;
                x2 = TimeLineBuilder.GridTimePxPosition(context_, end_time);
                empty_items.add(new GuideBarItem(context_, start_time, end_time, x1, x2));
            }
        }

        items_.addAll(empty_items);

        Collections.sort(items_, new Comparator<GuideBarItem>() {
            @Override
            public int compare(GuideBarItem lhs, GuideBarItem rhs) {
                return (lhs.start_x_ - rhs.start_x_);
            }
        });
    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        if (parent_activity_.state_ != GuideActivity.EGAS_READY)
            return true;

        if (event.getAction() == KeyEvent.ACTION_UP)
            return true;
        int old_idx = 0;
        int new_idx = 0;
        /*
        for (int idx = 0; idx < items_.size(); idx++) {
            if (items_.get(idx).highlighted()) {
                old_idx = idx;
                break;
            }
        }
*/

        old_idx =  parent_activity_.active_cell_index < items_.size() ? parent_activity_.active_cell_index : items_.size() - 1;

        Rect drawingRect = new Rect();
        getLocalVisibleRect(drawingRect);

        if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
            items_.get(old_idx).highlight(false);
            new_idx = old_idx + 1;

            if (new_idx >= items_.size()-1) {
                new_idx = items_.size() - 1;
                parent_activity_.active_cell_index = new_idx;
                items_.get(new_idx).highlight(true);
                parent_activity_.requestEpg();
                invalidate();
                return true;
            }

            GuideBarItem item = items_.get(new_idx);
            if (item.is_empty_) {
                parent_activity_.requestEpg();
            } else if (item.start_x_ >= drawingRect.right) {
                parent_activity_.guide_scroller.scrollTo(item.start_x_, 0);
                parent_activity_.time_scroller.scrollTo(item.start_x_, 0);
            }
            parent_activity_.active_cell_index = new_idx;
            item.highlight(true);
            parent_activity_.fillItemDetails(this, item);
            invalidate();
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
            items_.get(old_idx).highlight(false);
            new_idx = old_idx - 1;

            if (new_idx < 0) {
                parent_activity_.channel_container.requestFocus();
                return true;
            }
            GuideBarItem item = items_.get(new_idx);
            if (item.end_x_ <= drawingRect.left) {
                if (drawingRect.left < drawingRect.width()) {
                    parent_activity_.guide_scroller.scrollTo(0, 0);
                    parent_activity_.time_scroller.scrollTo(0, 0);
                } else {
                    parent_activity_.guide_scroller.scrollTo(item.start_x_, 0);
                    parent_activity_.time_scroller.scrollTo(item.start_x_, 0);
                }
            }

            parent_activity_.active_cell_index = new_idx;
            item.highlight(true);
            parent_activity_.fillItemDetails(this, item);
            invalidate();
            /*
            if (skip_focus) {
                parent_activity_.channel_container.requestFocus();
            }
            */
            return true;
        } else if (Common.okPressed(keyCode)) {
            GuideBarItem item = items_.get(old_idx);
            if (item != null) {
                parent_activity_.startProgramActivity(channel_id_, item,
                        recommendation_map.get(item.prg_.ID));
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {

            for (int idx = parent_activity_.active_channel_index - 1; idx >= 0; idx--) {
                parent_activity_.active_channel_index = idx;
                LinearLayout ll = (LinearLayout) parent_activity_.getChannelEpg(idx);
                View vv = ll.getChildAt(0);
                if (ll.getVisibility() != View.VISIBLE || vv.getVisibility() != View.VISIBLE)
                    continue;

                GuideBar bar = (GuideBar) vv;
                vv.requestFocus();
                int _idx = getItem(bar, old_idx);
                if (_idx == -1) {
                    parent_activity_.active_channel_index = idx;
                } else {
                    parent_activity_.active_cell_index = _idx;
                    GuideBarItem item = bar.items_.get(_idx);
                    item.highlight(true);
                    parent_activity_.fillItemDetails(bar, item);
                    bar.invalidate();
                    break;
                }
            }
            return true;
        } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
            int cnt = parent_activity_.epg_container.getChildCount();
            for (int idx = parent_activity_.active_channel_index + 1; idx < cnt; idx++) {
                parent_activity_.active_channel_index = idx;
                LinearLayout ll = (LinearLayout) parent_activity_.getChannelEpg(idx);
                View vv = ll.getChildAt(0);
                if (ll.getVisibility() != View.VISIBLE || vv.getVisibility() != View.VISIBLE)
                    continue;

                GuideBar bar = (GuideBar) vv;
                vv.requestFocus();
                int _idx = getItem(bar, old_idx);
                if (_idx == -1) {
                    parent_activity_.active_channel_index = idx;
                } else {
                    parent_activity_.active_cell_index = _idx;
                    GuideBarItem item = bar.items_.get(_idx);
                    item.highlight(true);
                    parent_activity_.fillItemDetails(bar, item);
                    bar.invalidate();
                    break;
                }
            }
            return true;
        } else if (Common.backPressed(keyCode, event) || keyCode == KeyEvent.KEYCODE_MENU) {
            parent_activity_._options_menu.show();
            return true;
        }

        return false;
    }

    protected int getItem(GuideBar new_guide_bar, int old_idx) {
        Rect drawingRect = new Rect();
        getLocalVisibleRect(drawingRect);

        int new_idx = -1;
        GuideBarItem item = items_.get(old_idx);
        int start = Math.max(item.start_x_, drawingRect.left);
        int end = Math.min(item.end_x_, drawingRect.right);
        int center = start + (end - start) / 2;
        int len = 0;
        for (int idx = 0; idx < new_guide_bar.items_.size(); idx++) {
            GuideBarItem it = new_guide_bar.items_.get(idx);
            if (center >= it.start_x_ && center <= it.end_x_) {
                new_idx = idx;
                break;
            }
        }
        return new_idx;
    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (!hasFocus) {
            for (int idx = 0; idx < items_.size(); idx++) {
                GuideBarItem item = items_.get(idx);
                item.highlight(false);
            }
        }
    }

    public GuideBarItem getItemForTime(long t) {

        GuideBarItem gbi = null;
        for (int idx = 0; idx < items_.size(); idx++) {
            GuideBar.GuideBarItem item = items_.get(idx);
            if (item.prg_.StartTime <= t && (item.prg_.StartTime + item.prg_.Duration) >= t) {
                gbi = item;
                break;
            }
        }
        return gbi;
    }
}
