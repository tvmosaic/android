package com.dvblogic.tvmosaic;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.view.KeyEvent;
import android.view.View;
import android.widget.NumberPicker;

import TVMosaic.TVMosaic.R;

public class CustomDatePickerDialog extends DatePickerDialog
{
    Context _context;
    DatePickerDialog dlg;
    int picker_color = Color.TRANSPARENT;
    boolean day_left = true;

	public CustomDatePickerDialog(Context context, OnDateSetListener listener, int year,
			int monthOfYear, int dayOfMonth) {
        super(context, listener, year, monthOfYear, dayOfMonth);
        //super(context, listener, year, monthOfYear, dayOfMonth);
        _context = context;
        dlg = this;
        day_left = getPickersOrder();

        try {
            int id = getDatePicker().getContext().getResources().getIdentifier("android:id/day", null, null);
            final NumberPicker dayPicker = (NumberPicker) getDatePicker().findViewById(id);
            if (dayPicker != null) {
                dayPicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus)
                            dayPicker.setBackgroundColor(_context.getResources().getColor(R.color.date_picker_focused));
                        else
                            dayPicker.setBackgroundColor(picker_color);
                    }
                });
            }
            id = getDatePicker().getContext().getResources().getIdentifier("android:id/month", null, null);
            final NumberPicker monthPicker = (NumberPicker) getDatePicker().findViewById(id);
            if (monthPicker != null) {
                monthPicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus)
                            monthPicker.setBackgroundColor(_context.getResources().getColor(R.color.date_picker_focused));
                        else
                            monthPicker.setBackgroundColor(picker_color);
                    }
                });
            }

            NumberPicker picker = day_left ? dayPicker : monthPicker;
            if (picker != null) {
                picker.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                            View btn = dlg.getButton(DialogInterface.BUTTON_POSITIVE);
                            if (btn != null)
                                btn.requestFocus();
                            return true;
                        }
                        return false;
                    }
                });
            }
            id = getDatePicker().getContext().getResources().getIdentifier("android:id/year", null, null);
            final NumberPicker yearPicker = (NumberPicker) getDatePicker().findViewById(id);
            if (yearPicker != null) {
                yearPicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus)
                            yearPicker.setBackgroundColor(_context.getResources().getColor(R.color.date_picker_focused));
                        else
                            yearPicker.setBackgroundColor(picker_color);
                    }
                });
                yearPicker.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                            View btn = dlg.getButton(DialogInterface.BUTTON_NEGATIVE);
                            if (btn != null)
                                btn.requestFocus();
                            return true;
                        }
                        return false;
                    }
                });
            }
        } catch (Exception e) {
           e.printStackTrace();
        }
	}

    boolean getPickersOrder() {
        char[] pattern = android.text.format.DateFormat.getDateFormatOrder(_context);
        return pattern[0] == 'd';
        /*
        ContentResolver cr = _context.getContentResolver();
        String pattern = System.getString(_context.getContentResolver(), System.DATE_FORMAT);
        return pattern.indexOf("d")<pattern.indexOf("M");
        */
        /*
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.MONTH, 3);
        c.set(Calendar.YEAR, 2000);
        SimpleDateFormat df = new SimpleDateFormat(); //called without pattern
        String str = df.format(c.getTime());
        int day_pos = str.indexOf("1");
        int month_pos = str.indexOf("3");
        return day_pos < month_pos;
        */
    }
}

