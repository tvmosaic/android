package com.dvblogic.tvmosaic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import TVMosaic.TVMosaic.R;
import android.widget.Toast;
import com.dvblogic.dvblink_common.Settings;
import com.dvblogic.dvblink_common.StatusCode;

public class AdvancedSettingsActivity extends BaseSettingsActivity {
    private static final String TAG = "AdvSettingsActivity";

    private enum AdvancedSettingsActivityState {
        STATE_NONE,
        STATE_REPAIR_DB,
        STATE_READY
    }

    private AdvancedSettingsActivityState state_ = AdvancedSettingsActivityState.STATE_NONE;

    @Override
    protected int getLayoutID() {
        return R.layout.advanced_settings_activity;
    }

    @Override
    protected void customizeList() {
        customizeTVGuideSettings();
        customizePlayerSettings();
        customizeMiscellaneousSettings();
        headBar.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVANCED_PAGE_TITLE));
        state_ = AdvancedSettingsActivityState.STATE_READY;
        super.customizeList();
    }

    @Override
    public void onDestroy() {
        if (data_provider_ != null) {
            data_provider_.Cancel();
        }
        data_provider_ = null;
        super.onDestroy();
    }

    private String getOnOffFromBoolean(boolean isOn) {
        return language_manager_.GetString(isOn ? LocaleStrings.IDS_ON : LocaleStrings.IDS_OFF);
    }

    private void customizeTVGuideSettings() {
        RelativeLayout guide_category = (RelativeLayout) findViewById(R.id.tv_guide_category);
        customizeCategory(guide_category, LocaleStrings.IDS_TV_GUIDE_SETTINGS, LocaleStrings.IDS_TV_GUIDE_SETTINGS_DESC);

        // show channels numbers
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.channels_number_spinner);
        TextView tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SHOW_CHANNEL_NUMBER));

        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SHOW_CHANNEL_NUMBER_DESC));

        boolean onoff = settings_.getValue(Settings.SHOW_CHANNEL_NUMBER_KEY, true);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(getOnOffFromBoolean(onoff));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean onoff = !settings_.getValue(Settings.SHOW_CHANNEL_NUMBER_KEY, true);
                settings_.setValue(Settings.SHOW_CHANNEL_NUMBER_KEY, onoff);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.channels_number_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                tv.setText(getOnOffFromBoolean(onoff));
            }
        });

        // use program colors
        layout = (RelativeLayout) findViewById(R.id.colors_spinner);
        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_USE_PROGRAM_COLORS));

        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_USE_PROGRAM_COLORS_DESC));

        onoff = settings_.getValue(Settings.USE_PROGRAM_COLORS_KEY, true);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(getOnOffFromBoolean(onoff));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean onoff = !settings_.getValue(Settings.USE_PROGRAM_COLORS_KEY, true);
                settings_.setValue(Settings.USE_PROGRAM_COLORS_KEY, onoff);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.colors_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                tv.setText(getOnOffFromBoolean(onoff));

            }
        });
    }

    private void customizePlayerSettings() {
        RelativeLayout guide_category = (RelativeLayout) findViewById(R.id.player_category);
        customizeCategory(guide_category, LocaleStrings.IDS_PLAYER_SETTINGS, LocaleStrings.IDS_PLAYER_SETTINGS_DESC);

        // audio passthrough
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.audio_passthrough_spinner);
        TextView tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_AUDIO_PASSTHROUGH));

        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_AUDIO_PASSTHROUGH_DESC));

        boolean onoff = settings_.getValue(Settings.AUDIO_PASSTHROUGH_KEY, Settings.AUDIO_PASSTHROUGH_DEFAULT);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(getOnOffFromBoolean(onoff));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean onoff = !settings_.getValue(Settings.AUDIO_PASSTHROUGH_KEY, Settings.AUDIO_PASSTHROUGH_DEFAULT);
                settings_.setValue(Settings.AUDIO_PASSTHROUGH_KEY, onoff);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.audio_passthrough_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                tv.setText(getOnOffFromBoolean(onoff));
            }
        });

        // timeshifting
        layout = (RelativeLayout) findViewById(R.id.timeshifting_spinner);
        if (settings_.getValue(Settings.TIMESHIFT_SUPPORTED_KEY, false)) {

            tv = (TextView) layout.findViewById(R.id.title_text);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_TIMESHIFTING));

            tv = (TextView) layout.findViewById(R.id.desc_text);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_TIMESHIFTING_DESC));

            onoff = settings_.getValue(Settings.TIMESHIFT_ENABLED_KEY, Settings.TIMESHIFT_ENABLED_DEFAULT);
            tv = (TextView) layout.findViewById(R.id.spinner_label);
            tv.setText(getOnOffFromBoolean(onoff));

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean onoff = !settings_.getValue(Settings.TIMESHIFT_ENABLED_KEY, Settings.TIMESHIFT_ENABLED_DEFAULT);
                    settings_.setValue(Settings.TIMESHIFT_ENABLED_KEY, onoff);
                    RelativeLayout layout = (RelativeLayout) findViewById(R.id.timeshifting_spinner);
                    TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                    tv.setText(getOnOffFromBoolean(onoff));
                }
            });
        } else {
            layout.setVisibility(View.GONE);
        }

        // deinterlacing
        layout = (RelativeLayout) findViewById(R.id.deinterlacing_spinner);

        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_DEINTERLACING));

        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_DEINTERLACING_DESC));

        int mode = settings_.getValue(Settings.DEINTERLACE_MODE_KEY, Settings.DEINTERLACE_MODE_DEFAULT);
        if (mode < 0 || mode > 2)
            mode = 2;

        final String[] strModes = {
                language_manager_.GetString(LocaleStrings.IDS_OFF),
                language_manager_.GetString(LocaleStrings.IDS_ON),
                language_manager_.GetString(LocaleStrings.IDS_AUTO)
        };

        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(strModes[mode]);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int mode = settings_.getValue(Settings.DEINTERLACE_MODE_KEY, Settings.DEINTERLACE_MODE_DEFAULT);
                if (mode < 0 || mode > 2)
                    mode = 2;
                switch (mode) {
                    case 0:
                        mode = 1;
                        break;
                    case 1:
                        mode = 2;
                        break;
                    case 2:
                        mode = 0;
                        break;
                }
                settings_.setValue(Settings.DEINTERLACE_MODE_KEY, mode);
                TextView tv = (TextView) v.findViewById(R.id.spinner_label);
                tv.setText(strModes[mode]);
            }
        });

        // refresh rate
        layout = (RelativeLayout) findViewById(R.id.refresh_rate_spinner);

        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_REFRESH_RATE));

        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_REFRESH_RATE_DESC));

        final String[] strRates = {language_manager_.GetString(LocaleStrings.IDS_DEFAULT), "50", "60"};

        int refresh_rate = settings_.getValue(Settings.REFRESH_RATE_KEY, Settings.REFRESH_RATE_DEFAULT);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(refresh_rate < 0 ? strRates[0] : (refresh_rate == 50 ? strRates[1] : strRates[2]));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int refresh_rate = settings_.getValue(Settings.REFRESH_RATE_KEY, Settings.REFRESH_RATE_DEFAULT);
                switch (refresh_rate) {
                    case Settings.REFRESH_RATE_DEFAULT:
                        refresh_rate = 50;
                        break;
                    case 50:
                        refresh_rate = 60;
                        break;
                    case 60:
                        refresh_rate = Settings.REFRESH_RATE_DEFAULT;
                        break;
                }
                settings_.setValue(Settings.REFRESH_RATE_KEY, refresh_rate);

                RelativeLayout layout = (RelativeLayout) findViewById(R.id.refresh_rate_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                if (refresh_rate == 50) {
                    tv.setText(strRates[1]);
                } else if (refresh_rate == 60) {
                    tv.setText(strRates[2]);
                } else {
                    tv.setText(strRates[0]);
                }
            }
        });


        // Play last channel on start-up
        layout = (RelativeLayout) findViewById(R.id.play_last_channel_spinner);
        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_PLAY_LAST_CHANNEL));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_PLAY_LAST_CHANNEL_DESC));
        onoff = settings_.getValue(Settings.PLAY_LAST_CHANNEL_ON_START, Settings.PLAY_LAST_CHANNEL_ON_START_DEFAULT);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(getOnOffFromBoolean(onoff));
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean onoff = !settings_.getValue(
                        Settings.PLAY_LAST_CHANNEL_ON_START, Settings.PLAY_LAST_CHANNEL_ON_START_DEFAULT
                );
                settings_.setValue(Settings.PLAY_LAST_CHANNEL_ON_START, onoff);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.play_last_channel_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                tv.setText(getOnOffFromBoolean(onoff));
            }
        });

    }

    private void customizeMiscellaneousSettings() {
        TextView tv;
        RelativeLayout layout;

        RelativeLayout category = (RelativeLayout) findViewById(R.id.miscellaneous_settings);

        category.setVisibility(View.GONE);
    }

    private void requestRepairDB() {
        Log.i(TAG, "requestRepairDB");
        try {
            if (data_provider_ != null && state_ == AdvancedSettingsActivityState.STATE_READY) {
                activateProgressBar(true);
                state_ = AdvancedSettingsActivityState.STATE_REPAIR_DB;
                data_provider_.RepairDB();
            }
        } catch (Exception e) {
            Log.e(TAG, "requestRepairDB", e);
        }
    }

    private void processRepairDB() {
        Log.i(TAG, "processRepairDB");
        state_ = AdvancedSettingsActivityState.STATE_READY;
        activateProgressBar(false);
        if (response_ != null && response_ instanceof Boolean && (Boolean) response_) {
            Toast.makeText(this,
                    language_manager_.GetString(LocaleStrings.IDS_SETTINGS_REPAIR_DB_SUCCESS),
                    Toast.LENGTH_SHORT
            ).show();
        }
    }

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state_ == AdvancedSettingsActivityState.STATE_REPAIR_DB) {
                    processRepairDB();
                }
            }
        });
    }

    public void ErrorProcessing(String command, StatusCode error) {
        http_error_ = error;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProcessDataProviderError();
            }
        });
    }

    public void ProcessDataProviderError() {
        state_ = AdvancedSettingsActivityState.STATE_READY;
        activateProgressBar(false);
        if (language_manager_ != null) {
            String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
    }
}
