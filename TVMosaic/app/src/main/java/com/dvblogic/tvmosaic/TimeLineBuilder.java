package com.dvblogic.tvmosaic;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Pair;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.dvblogic.dvblink_common.Utils;

import TVMosaic.TVMosaic.R;

public class TimeLineBuilder
{
	static public double seconds_per_pixel = 5.625 * 2;

	static private long epg_start_time;
	static private long epg_end_time;

	static public Pair<Long, Long> getBounds() {
		return new Pair<Long, Long>(epg_start_time, epg_end_time);
	}
	
	static public long startTime() {
		return epg_start_time;
	}
	
	static public long endTime() {
		return epg_end_time;
	}

	static public void InitTimeline(GuideActivity ga, long epg_start_, long epg_end_) {
		epg_start_time = epg_start_;
		epg_end_time = epg_end_;
	}

	public static int GridTimeToPxWidth(Context context, long start_time, long end_time) {
		return (int) ((end_time - start_time) / seconds_per_pixel * context.getResources().getDisplayMetrics().density);
	}

	public static int GridTimePxPosition(Context context, long end_time) {
		return (int) ((end_time - epg_start_time) / seconds_per_pixel * context.getResources().getDisplayMetrics().density);
	}

	public static long PixToGridTime(Context context, int pix) {
		return (long) ( epg_start_time + pix * seconds_per_pixel / context.getResources().getDisplayMetrics().density);
	}


	public static int GetNowOffset(Context context, long wnd_start_time) {
		long now = Utils.CurrentUtc();
		return GridTimeToPxWidth(context, wnd_start_time, now);
	}

}


