package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.DatePicker;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import com.dvblogic.dvblink_common.*;

import TVMosaic.TVMosaic.R;

import static android.text.InputType.TYPE_CLASS_TEXT;

public class ManualScheduleActivity extends BaseActivity implements ActionOptionsPane.ActionOptionsPaneEventListener
{
    private static final int state_ready      = 1;
    private static final int state_requesting_channels      = 2;
    private static final int state_requesting_add_schedule      = 3;

    private static final String TAG = "ManualScheduleActivity";

    int state_ = state_ready;
    private ActionOptionsPane choice_menu_;
    Calendar start_time_;
    Calendar end_time_;
    int day_mask_;
    String title_;
    String channel_id_;
    ChannelsList channels_;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.manual_schedule_activity);

        //set result to cancelled until there are changes
        setResult(RESULT_CANCELED);

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(language_manager_.GetString(LocaleStrings.IDS_ADD_SCHEDULE));

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                requestChannels();
            }
        });
    }

    protected void customizeActivity() {
        customizeProperty(R.id.schedule_title, language_manager_.GetString(LocaleStrings.IDS_TITLE),
                language_manager_.GetString(LocaleStrings.IDS_TITLE_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextInputDialog tid = new TextInputDialog(_context,
                                language_manager_.GetString(LocaleStrings.IDS_TITLE),
                                language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_OK),
                                language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL));

                        tid.setOnResultListener(new TextInputDialog.OnResultListener() {
                            @Override
                            public void onResult(boolean result, String text) {
                                if (result) {
                                    title_ = text;
                                    setPropertyCurrentValue(R.id.schedule_title, title_);
                                }
                            }
                        });
                        tid.showDialog(TYPE_CLASS_TEXT, title_);
                    }
                });
        setPropertyCurrentValue(R.id.schedule_title, title_);

        customizeProperty(R.id.schedule_channel, language_manager_.GetString(LocaleStrings.IDS_CHANNEL),
                language_manager_.GetString(LocaleStrings.IDS_CHANNEL_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_channels);
                    }
                });
        setPropertyCurrentValue(R.id.schedule_channel, getSelectedChannelName());

        customizeProperty(R.id.schedule_repeat, language_manager_.GetString(LocaleStrings.IDS_REPEAT),
                language_manager_.GetString(LocaleStrings.IDS_REPEAT_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_repeat);
                    }
                });
        setPropertyCurrentValue(R.id.schedule_repeat, getRepeatString(this, day_mask_));

        customizeProperty(R.id.schedule_date, language_manager_.GetString(LocaleStrings.IDS_DATE),
                language_manager_.GetString(LocaleStrings.IDS_DATE_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        int year = start_time_.get(Calendar.YEAR);
                        int month = start_time_.get(Calendar.MONTH);
                        int day = start_time_.get(Calendar.DAY_OF_MONTH);
                        String title = language_manager_.GetString(LocaleStrings.IDS_DATE);
                        CustomDatePickerDialog datePicker = new CustomDatePickerDialog(_context, new DatePickerDialog.OnDateSetListener() {
                            @Override
                            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                                start_time_.set(year, monthOfYear, dayOfMonth);
                                end_time_.set(year, monthOfYear, dayOfMonth);
                                setPropertyCurrentValue(R.id.schedule_date, getstartDateString());
                            }
                        }, year, month, day);
                        datePicker.setTitle(title);
                        datePicker.show();
                    }
                });
        setPropertyCurrentValue(R.id.schedule_date, getstartDateString());

        customizeProperty(R.id.schedule_start_time, language_manager_.GetString(LocaleStrings.IDS_START_TIME),
                language_manager_.GetString(LocaleStrings.IDS_START_TIME_DESC), startTimeOnClickListener);
        setPropertyCurrentValue(R.id.schedule_start_time, getTimeString(start_time_));

        customizeProperty(R.id.schedule_stop_time, language_manager_.GetString(LocaleStrings.IDS_END_TIME),
                language_manager_.GetString(LocaleStrings.IDS_END_TIME_DESC), onStopTimeClickListener);
        setPropertyCurrentValue(R.id.schedule_stop_time, getTimeString(end_time_));

        customizeProperty(R.id.add_schedule, language_manager_.GetString(LocaleStrings.IDS_ADD_NEW_RECORDING),
                language_manager_.GetString(LocaleStrings.IDS_ADD_NEW_RECORDING_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        requestAddSchedule();
                    }
                });

    }

    View.OnClickListener startTimeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int hour = start_time_.get(Calendar.HOUR_OF_DAY);
            int minute = start_time_.get(Calendar.MINUTE);
            boolean b24 = DateFormat.is24HourFormat(_context);

            if (Utils.usesRemoteControl(_context)) {
                CustomTimePickerDialog start_time_picker = new CustomTimePickerDialog(_context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        start_time_.set(Calendar.HOUR_OF_DAY, selectedHour);
                        start_time_.set(Calendar.MINUTE, selectedMinute);
                        setPropertyCurrentValue(R.id.schedule_start_time, getTimeString(start_time_));
                    }
                }, hour, minute, b24);
                start_time_picker.setTitle(language_manager_.GetString(LocaleStrings.IDS_START_TIME));
                start_time_picker.show();
            } else {
                final TimePicker timePicker = new TimePicker(_context);
                timePicker.setIs24HourView(b24);
                timePicker.setCurrentHour(hour);
                timePicker.setCurrentMinute(minute);

                final AlertDialog timePickerDialog = new AlertDialog.Builder(_context)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                start_time_.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                                start_time_.set(Calendar.MINUTE, timePicker.getCurrentMinute());
                                setPropertyCurrentValue(R.id.schedule_start_time, getTimeString(start_time_));
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setView(timePicker).show();
            }
        }
    };

    View.OnClickListener onStopTimeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int hour = end_time_.get(Calendar.HOUR_OF_DAY);
            int minute = end_time_.get(Calendar.MINUTE);
            boolean b24 = DateFormat.is24HourFormat(_context);

            if (Utils.usesRemoteControl(_context)) {
                CustomTimePickerDialog start_time_picker = new CustomTimePickerDialog(_context, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        end_time_.set(Calendar.HOUR_OF_DAY, selectedHour);
                        end_time_.set(Calendar.MINUTE, selectedMinute);
                        setPropertyCurrentValue(R.id.schedule_stop_time, getTimeString(end_time_));
                    }
                }, hour, minute, DateFormat.is24HourFormat(_context));
                start_time_picker.setTitle(language_manager_.GetString(LocaleStrings.IDS_END_TIME));
                start_time_picker.show();
            } else {
                final TimePicker timePicker = new TimePicker(_context);
                timePicker.setIs24HourView(b24);
                timePicker.setCurrentHour(hour);
                timePicker.setCurrentMinute(minute);

                final AlertDialog timePickerDialog = new AlertDialog.Builder(_context)
                        .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                end_time_.set(Calendar.HOUR_OF_DAY, timePicker.getCurrentHour());
                                end_time_.set(Calendar.MINUTE, timePicker.getCurrentMinute());
                                setPropertyCurrentValue(R.id.schedule_stop_time, getTimeString(end_time_));
                            }
                        })
                        .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .setView(timePicker).show();
            }
        }
    };


    protected String getSelectedChannelName() {
        String ret_val = "";
        for (int i=0; i<channels_.size() ;i++) {
            if (channel_id_.equals(channels_.get(i).ID)) {
                ret_val = channels_.get(i).Name;
                break;
            }
        }
        return ret_val;
    }

    static public int repeat_masks[] = {ManualSchedule.DAY_MASK_SUN, ManualSchedule.DAY_MASK_MON, ManualSchedule.DAY_MASK_TUE, ManualSchedule.DAY_MASK_WED, ManualSchedule.DAY_MASK_THU,
            ManualSchedule.DAY_MASK_FRI, ManualSchedule.DAY_MASK_SAT};

    static public String repeat_mask_names[] = {LocaleStrings.IDS_SUNDAY, LocaleStrings.IDS_MONDAY, LocaleStrings.IDS_TUESDAY, LocaleStrings.IDS_WEDNESDAY, LocaleStrings.IDS_THURSDAY,
            LocaleStrings.IDS_FRIDAY, LocaleStrings.IDS_SATURDAY};

    static public String buildRepeatDaysString(BaseActivity activity, int day_mask) {
        String ret_val = "";
        //build repeat string
        for (int i=0; i<repeat_masks.length; i++) {
            if ((day_mask & repeat_masks[i]) != 0) {
                if (ret_val.length() > 0)
                    ret_val += "/";
                ret_val += activity.language_manager_.GetString(repeat_mask_names[i]);
            }
        }
        return ret_val;
    }

    static public String getRepeatString(BaseActivity activity, int day_mask) {
        String ret_val = "";
        if (day_mask == ManualSchedule.DAY_MASK_ONCE) {
            ret_val = activity.language_manager_.GetString(LocaleStrings.IDS_REPEAT_ONCE);
        } else {
            if (day_mask == ManualSchedule.DAY_MASK_DAILY) {
                ret_val = activity.language_manager_.GetString(LocaleStrings.IDS_REPEAT_DAILY);
            } else {
                ret_val = buildRepeatDaysString(activity, day_mask);
            }
        }
        return ret_val;
    }

    protected String getstartDateString() {
        String ret_val = "";
        long t = start_time_.getTimeInMillis() / 1000;
        if (Utils.isToday(t)) {
            ret_val = language_manager_.GetString(LocaleStrings.IDS_TODAY);
        } else {
            ret_val = Utils.UtcToMediumDateString(_context, t);
        }
        return ret_val;
    }

    protected String getTimeString(Calendar c) {
        String ret_val = "";
        long t = c.getTimeInMillis() / 1000;
        ret_val = Utils.UtcToShortTimeString(_context, t);
        return ret_val;
    }

    protected void customizeProperty(int layoutID, String title, String desc, View.OnClickListener listener) {
        TextView tv;
        View lv = findViewById(R.id.manual_schedule_properties);
        RelativeLayout layout = (RelativeLayout) lv.findViewById(layoutID);
        layout.setVisibility(View.VISIBLE);

        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(title);
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(desc);

        layout.setOnClickListener(listener);
    }

    protected void setPropertyCurrentValue(int layoutID, String current_value) {
        TextView tv;
        View lv = findViewById(R.id.manual_schedule_properties);
        RelativeLayout layout = (RelativeLayout) lv.findViewById(layoutID);

        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(current_value);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    protected void requestChannels() {
        activateProgressBar(true);
        state_ = state_requesting_channels;

        data_provider_.GetChannels(new ChannelsRequest());
    }

    private void processChannels() {

        ChannelsList cl = (ChannelsList)response_;

        channels_ = cl;
        Settings.EChannelSortMode mode = settings_.getChannelSortMode();
        Utils.sortChannels(channels_, mode);

        state_ = state_ready;
        activateProgressBar(false);

        if (channels_.size() == 0) {
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_MANUAL_SCHEDULE_NO_CHANNELS), Toast.LENGTH_SHORT).show();

            //exit. No channels - no recordings
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    startActivity(new Intent(getApplicationContext(), GuideActivity.class));
                    finish();
                }
            }, 1 * 1000);
        } else {
            initializeActivity();
        }
    }

    protected void initializeActivity() {
        channel_id_ = channels_.get(0).ID;
        day_mask_ = 0;
        title_ = "New recording";

        start_time_ = Calendar.getInstance(Locale.getDefault());
        end_time_ = Calendar.getInstance(Locale.getDefault());
        end_time_.setTimeInMillis(start_time_.getTimeInMillis() + 3600000);

        customizeActivity();
    }

    private void requestAddSchedule() {
        long start_time = start_time_.getTimeInMillis() / 1000;
        long end_time = end_time_.getTimeInMillis() / 1000;
        long duration = end_time - start_time;
        if (duration <= 0) {
            start_time_.set(Calendar.HOUR_OF_DAY, 23);
            start_time_.set(Calendar.MINUTE, 59);
            long midnight = start_time_.getTimeInMillis() / 1000;
            long dt_sec = midnight + (end_time_.get(Calendar.HOUR_OF_DAY) * 60 + end_time_.get(Calendar.MINUTE)) * 60;
            duration = dt_sec - start_time;
        }

        activateProgressBar(true);
        state_ = state_requesting_add_schedule;

        ManualSchedule msch = new ManualSchedule(channel_id_, title_, start_time, (int)duration + 1, day_mask_);
        Schedule sch = new Schedule(msch);
        data_provider_.AddSchedule(sch);
    }

    private void processAddSchedule() {
        state_ = state_ready;
        activateProgressBar(false);

        //add channel for epg update and exit
        ChannelIDList channels_to_refresh = new ChannelIDList();
        channels_to_refresh.add(channel_id_);

        Intent intent = getIntent();
        String channels_xml = Serializer.serialize(channels_to_refresh);
        intent.putExtra("need_channel_update", channels_xml);
        setResult(RESULT_OK, intent);

        finish();
    }

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;

        if (state_ == state_requesting_channels) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processChannels();
                }
            });
        } else
        if (state_ == state_requesting_add_schedule) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processAddSchedule();
                }
            });
        }
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        http_error_ = error;
        runOnUiThread(new Runnable() {
            public void run() {
                ProcessDataProviderError();
            }
        });
    }

    protected void ProcessDataProviderError() {
        try {
            String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

            state_ = state_ready;
            activateProgressBar(false);
        } catch (Exception e) {
        }
    }

    static final int action_pane_mode_none = 0;
    static final int action_pane_mode_channels = 1;
    static final int action_pane_mode_repeat = 2;

    protected int action_pane_mode_ = action_pane_mode_none;

    protected void showActionMenu(int mode) {
        ActionOptionsPaneItems options = null;

        action_pane_mode_ = mode;
        switch (mode) {
            case action_pane_mode_channels:
                options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON);
                for (int i=0; i<channels_.size(); i++) {
                    String id = String.valueOf(i);
                    options.add_item(channels_.get(i).Name, id);

                    if (channel_id_.equals(channels_.get(i).ID)) {
                        options.set_selected_item_id(id);
                    }
                }
                break;
            case action_pane_mode_repeat:
                options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX, true);
                for (int i=0; i<repeat_masks.length; i++) {
                    String id = String.valueOf(i);
                    options.add_item(language_manager_.GetString(repeat_mask_names[i]), id);
                    if ((repeat_masks[i] & day_mask_) != 0) {
                        //all items are unchecked by default
                        options.toggle_selection(id);
                    }
                }
                break;
            default:
                break;
        }

        choice_menu_ = new ActionOptionsPane(this, this, options, 0, null);
        choice_menu_.show();
    }

    public void onActionPaneOption(int option_id, String selected_option) {

        int value_idx = Integer.parseInt(selected_option);

        switch (action_pane_mode_) {
            case action_pane_mode_channels:
                channel_id_ = channels_.get(value_idx).ID;
                setPropertyCurrentValue(R.id.schedule_channel, getSelectedChannelName());
                break;
            case action_pane_mode_repeat:
                day_mask_ = ManualSchedule.DAY_MASK_ONCE;
                ArrayList<String> selected_items = choice_menu_.get_multiselected_items_id();
                for (int i=0; i<selected_items.size(); i++) {
                    int idx = Integer.valueOf(selected_items.get(i));
                    day_mask_ |= repeat_masks[idx];
                }
                setPropertyCurrentValue(R.id.schedule_repeat, getRepeatString(this, day_mask_));
                break;
            default:
                break;
        }
    }
}
