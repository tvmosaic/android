package com.dvblogic.tvmosaic;

import TVMosaic.TVMosaic.R;
import android.content.Context;
import android.graphics.Typeface;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.dvblogic.dvblink_common.ChannelsList;


public class PlayerChannelsRecyclerViewAdapter extends RecyclerView.Adapter<PlayerChannelsRecyclerViewAdapter.ChannelViewHolder> {

    private boolean isEnabled = true;

    public class ChannelViewHolder extends RecyclerView.ViewHolder {

        CardView cv;
        TextView channelName;
        LinearLayout container;

        ChannelViewHolder(View itemView) {
            super(itemView);
            cv = (CardView)itemView.findViewById(R.id.channel_container);
            channelName = (TextView)itemView.findViewById(R.id.name);
            container = (LinearLayout) itemView.findViewById(R.id.container);
            container.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    notifyItemChanged(selectedPos);
                    int newSelectedPos = getLayoutPosition();
                    if (newSelectedPos != selectedPos) {
                        selectedPos = newSelectedPos;
                        notifyItemChanged(selectedPos);
                        recyclerView.smoothScrollToPosition(selectedPos);
                    }
                    if (selectEventListener != null) {
                        selectEventListener.onTouchSelect(selectedPos);
                    }
                }
            });

        }
    }


    private Context context;
    private RecyclerView recyclerView;
    private int selectedPos = 0;
    private int visibleItemsCount = 0;
    private ChannelsList channels;

    public PlayerChannelsRecyclerViewAdapter(Context context, ChannelsList channels, int visibleItemsCount){
        this.context = context;
        this.channels = channels;
        this.visibleItemsCount = visibleItemsCount % 2 == 0 ? visibleItemsCount + 1 : visibleItemsCount;
        this.selectedPos = Math.min((int) Math.floor(visibleItemsCount / 2), channels.size());
    }

    public void nextItem() {
        if (selectedPos + 1 > channels.size() - 1) {
            return;
        }
        changeItem(1);
    }

    public void prevItem() {
        if (selectedPos < 1) {
            return;
        }
        changeItem(-1);
    }

    public void selectItem(int index) {
        index = Math.min(Math.max(index, 0), channels.size()-1);
        selectedPos = index;
        notifyItemChanged(selectedPos);
        //call scrollToPosition and then smoothScrollToPosition to prevent huge channel scrolling at the beginning
        recyclerView.scrollToPosition(selectedPos);
        recyclerView.smoothScrollToPosition(selectedPos);
        // if (selectEventListener != null) {
        //     selectEventListener.onSelect(index);
        // }
    }

    private OnSelectEventListener selectEventListener;

    public interface OnSelectEventListener {
        void onSelect(int index);
        void onTouchSelect(int index);
    }

    public void setSelectEventListener(OnSelectEventListener selectEventListener) {
        this.selectEventListener = selectEventListener;
    }

    private void changeItem(int direction) {
        notifyItemChanged(direction > 0 ? selectedPos++ : selectedPos--);
        int index = Math.max(selectedPos, 0);
        notifyItemChanged(index);
        recyclerView.smoothScrollToPosition(index);
        if (selectEventListener != null) {
            selectEventListener.onSelect(index);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;

        CentredLayoutManager centredLayoutManager = new CentredLayoutManager(context);
        int itemWidth = (int) Math.floor(containerWidth(context) / visibleItemsCount) + context.getResources().getDimensionPixelOffset(R.dimen.play_live_channel_half_card_margin);
        int offset = (int) Math.floor(visibleItemsCount / 2) * itemWidth;
        centredLayoutManager.setCenteredItemOffset(offset);
        recyclerView.setLayoutManager(centredLayoutManager);

        // For circular
        // recyclerView.getLayoutManager().scrollToPosition(Integer.MAX_VALUE / 2);
    }

    @Override
    public ChannelViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.player_channel_layout, viewGroup, false);
        ChannelViewHolder channelViewHolder = new ChannelViewHolder(v);
        return channelViewHolder;
    }

    @Override
    public void onBindViewHolder(ChannelViewHolder channelViewHolder, int position) {
        // For circular
        // int position = Math.abs(inputPosition) % channels.size();

        boolean isSelected = selectedPos == position;
        channelViewHolder.channelName.setText(channels.get(position).Name);

        channelViewHolder.channelName.setTypeface(null, isSelected ? Typeface.BOLD : Typeface.NORMAL);
        channelViewHolder.channelName.setTextSize(
                TypedValue.COMPLEX_UNIT_PX, isSelected ?
                        context.getResources().getDimension(R.dimen.play_live_channel_text_selected) :
                        context.getResources().getDimension(R.dimen.play_live_channel_text)
        );
        int topPadding = isSelected ? context.getResources().getDimensionPixelOffset(R.dimen.play_live_channel_text_selected_padding) :
                context.getResources().getDimensionPixelOffset(R.dimen.play_live_channel_text_padding);
        channelViewHolder.channelName.setPadding(0, topPadding, 0, 0);
        channelViewHolder.container.setSelected(isSelected);
        channelViewHolder.container.setEnabled(isEnabled);
        int itemWidth = (int) Math.floor(containerWidth(context) / visibleItemsCount) - context.getResources().getDimensionPixelOffset(R.dimen.play_live_channel_card_margin);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(itemWidth, ViewGroup.LayoutParams.WRAP_CONTENT);
        channelViewHolder.container.setLayoutParams(params);
    }

    @Override
    public int getItemCount() {
        // For circular
        // return Integer.MAX_VALUE;
        return channels.size();
    }

    protected static int containerWidth(Context context) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.widthPixels;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
        notifyDataSetChanged();
    }
}
