package com.dvblogic.tvmosaic;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.ChannelIDList;
import com.dvblogic.dvblink_common.ConciseParamMap;
import com.dvblogic.dvblink_common.EditableParam;
import com.dvblogic.dvblink_common.ParamContainer;
import com.dvblogic.dvblink_common.Schedule;
import com.dvblogic.dvblink_common.SchedulesList;
import com.dvblogic.dvblink_common.SchedulesRequest;
import com.dvblogic.dvblink_common.SelectableParam;
import com.dvblogic.dvblink_common.Serializer;
import com.dvblogic.dvblink_common.StatusCode;

import java.util.ArrayList;

import TVMosaic.TVMosaic.R;

public class ParamContainerActivity extends BaseActivity implements ActionOptionsPane.ActionOptionsPaneEventListener
{
    private static final String TAG = "ParamContainerActivity";
    private static final String applyAndContinueItemId = "75e75ef9-dae9-45cc-98b9-cc4b07fae6a5";

    private class ResultsAdapter extends BaseAdapter {
        private ArrayList<Schedule> schedules = new ArrayList<>();

        private LayoutInflater ltInflater;

        private View.OnClickListener resultItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                processOnItemClick(view);
            }
        };
        private View.OnKeyListener resultItemKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && Common.okPressed(keyEvent.getKeyCode())) {
                    processOnItemClick(view);
                    return true;
                }
                return false;
            }
        };

        public ResultsAdapter() {
            ltInflater = getLayoutInflater();
        }

        protected int getParamsNumber() {
            return container_.editable_params.size() + container_.selectable_params.size() + 1;
        }

        protected Object getParameter(int position) {
            int selectable_num = container_.selectable_params.size();
            int editable_num = container_.editable_params.size();
            if (position >= 0 && position < selectable_num)
                return container_.selectable_params.get(position);
            else if (position >= selectable_num && position < selectable_num + editable_num)
                return container_.editable_params.get(position - selectable_num);

            return null;
        }

        @Override
        public int getCount() {
            return getParamsNumber();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ltInflater.inflate(R.layout.settings_spinner_layout, parent, false);
            }

            String first_line  = "";
            String second_line  = "";
            String id  = "";

            Object obj = getParameter(position);
            if (obj == null) {
                //this is a special "apply" entry. It is always last in the list
                first_line = language_manager_.GetString(LocaleStrings.IDS_PARAM_CONTAINER_APPLY);
                second_line = language_manager_.GetString(LocaleStrings.IDS_PARAM_CONTAINER_APPLY_DESC);
                id = applyAndContinueItemId;
            } else if (obj.getClass() == EditableParam.class) {
                EditableParam ep = (EditableParam)obj;
                first_line = ep.name;
                id = ep.key;
                setEditableValue(ep, view);
            } else if (obj.getClass() == SelectableParam.class) {
                SelectableParam sp = (SelectableParam)obj;
                first_line = sp.name;
                id = sp.key;
                setSelectableValue(sp, view);
            }

            TextView label;
            label = (TextView) view.findViewById(R.id.title_text);
            label.setText(first_line);

            label = (TextView) view.findViewById(R.id.desc_text);
            label.setText(second_line);

            view.setTag(R.string.param_container_id, id);
            view.setOnClickListener(resultItemClickListener);
            view.setOnKeyListener(resultItemKeyListener);

            return view;
        }

        protected void setSelectableValue(SelectableParam sp, View view) {
			TextView label = (TextView) view.findViewById(R.id.spinner_label);
			label.setText(sp.values.get(sp.selected).name);
        }

        protected void setEditableValue(EditableParam ep, View view) {
            TextView label = (TextView) view.findViewById(R.id.spinner_label);
            label.setText(ep.value);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return getParameter(position);
        }
    }

    private ListView results;
    private ResultsAdapter resultsAdapter;
    private ParamContainer container_;
    private ActionOptionsPane selection_menu_;
    private String edited_param_id_;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.param_container_activity);

        String container_str = getIntent().getExtras().getString("container");
        container_ = (ParamContainer) Serializer.deserialize(container_str, ParamContainer.class);

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(container_.name);

        View root = findViewById(R.id.param_container_layout);
        results = (ListView) root.findViewById(R.id.param_list);

        resultsAdapter = new ResultsAdapter();
        results.setAdapter(resultsAdapter);
        results.setItemsCanFocus(true);
    }

    @Override
    public void onBackPressed() {
        finish();
    }

    protected void applyAndExit() {

        ConciseParamMap paramMap = new ConciseParamMap();
        for (int i=0; i<container_.selectable_params.size(); i++) {
            SelectableParam sp = container_.selectable_params.get(i);
            paramMap.parameters.put(
                    sp.key,
                    sp.values.get(sp.selected).value
            );
        }
        for (int i=0; i<container_.editable_params.size(); i++) {
            EditableParam ep = container_.editable_params.get(i);
            paramMap.parameters.put(
                    ep.key,
                    ep.value
            );
        }

        Intent intent = getIntent();
        String param = Serializer.serialize(paramMap);
        intent.putExtra("parameters", param);
        setResult(RESULT_OK, intent);
        finish();
    }

    protected Object getParamFromId(String id) {
        for (int i=0; i<container_.selectable_params.size(); i++) {
            if (container_.selectable_params.get(i).key.equals(id))
                return container_.selectable_params.get(i);
        }
        for (int i=0; i<container_.editable_params.size(); i++) {
            if (container_.editable_params.get(i).key.equals(id))
                return container_.editable_params.get(i);
        }
        return null;
    }

    protected void processOnItemClick(View view) {
        String id  = (String)view.getTag(R.string.param_container_id);

        if (id.equals(applyAndContinueItemId)) {
            applyAndExit();
        } else {
            Object obj = getParamFromId(id);
            edited_param_id_ = id;
            if (obj.getClass() == EditableParam.class) {
                EditableParam ep = (EditableParam)obj;

                TextInputDialog tid = new TextInputDialog(_context,
                        ep.name,
                        language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_OK),
                        language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL));

                tid.setOnResultListener(new TextInputDialog.OnResultListener() {
                    @Override
                    public void onResult(boolean result, String text) {
                        if (result) {
                            Object obj = getParamFromId(edited_param_id_);
                            if (obj.getClass() == EditableParam.class) {
                                EditableParam ep = (EditableParam) obj;
                                ep.value = text;
                                resultsAdapter.notifyDataSetChanged();
                            }
                        }
                    }
                });

                int input_type = ep.format == EditableParam.EDITABLE_FORMAT_NUMBER ? InputType.TYPE_CLASS_NUMBER : InputType.TYPE_CLASS_TEXT;
                tid.showDialog(input_type, ep.value);

            } else if (obj.getClass() == SelectableParam.class) {
                SelectableParam sp = (SelectableParam)obj;

                ActionOptionsPaneItems options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON);
                for (int i=0; i<sp.values.size(); i++) {
                    String item_id = String.valueOf(i);
                    options.add_item(sp.values.get(i).name, item_id);

                    if (sp.selected == i) {
                        options.set_selected_item_id(item_id);
                    }
                }

                selection_menu_ = new ActionOptionsPane(this, this, options, 0, null);
                selection_menu_.show();
            }
        }
    }

    public void onActionPaneOption(int option_id, String selected_option) {
        int idx = Integer.parseInt(selected_option);

        Object obj = getParamFromId(edited_param_id_);
        if (obj.getClass() == SelectableParam.class) {
            SelectableParam sp = (SelectableParam) obj;
            sp.selected = idx;
            resultsAdapter.notifyDataSetChanged();
        }

        selection_menu_.hide();
    }

}
