package com.dvblogic.tvmosaic;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import TVMosaic.TVMosaic.R;

public class TextInputDialog {

    protected Context _context;
    protected String _desc;
    protected String _button_positive;
    protected String _button_cancel;
    Dialog _dialog;
    OnResultListener _on_result_listener = null;

    public TextInputDialog(Context context,
                           String description,
                           String button_positive,
                           String button_cancel) {
        _context = context;
        _desc = description;
        _button_positive = button_positive;
        _button_cancel = button_cancel;
        _dialog = new Dialog(_context);
        _dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        _dialog.setContentView(R.layout.text_input_dialog);
        final Window window = _dialog.getWindow();
        window.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        window.clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        window.setBackgroundDrawable(new ColorDrawable(ContextCompat.getColor(_context,R.color.new_primary_translucent)));
    }

    public interface OnResultListener {
        void onResult(boolean result, String text);
    }

    public void setOnResultListener(OnResultListener l) {
        _on_result_listener = l;
    }

    public void showDialog(int text_type, String defaultText) {

        //customize dialog buttons, text and input
        CustomTextView desc = (CustomTextView)_dialog.findViewById(R.id.description);
        desc.setText(_desc);

        EditText et = (EditText)_dialog.findViewById(R.id.input_line);
        if (defaultText != null)
            et.setText(defaultText);
        et.setInputType(text_type); //TYPE_CLASS_TEXT

        Button ok_button = (Button)_dialog.findViewById(R.id.button_ok);
        ok_button.setText(_button_positive);
        ok_button.setOnClickListener(okClickListener);

        Button cancel_button = (Button)_dialog.findViewById(R.id.button_cancel);
        cancel_button.setText(_button_cancel);
        cancel_button.setOnClickListener(cancelClickListener);

        _dialog.show();
    }

    View.OnClickListener okClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            EditText et = (EditText)_dialog.findViewById(R.id.input_line);
            if (_on_result_listener != null)
                _on_result_listener.onResult(true, et.getText().toString());
            _dialog.dismiss();
        }

    };

    View.OnClickListener cancelClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (_on_result_listener != null)
                _on_result_listener.onResult(false, null);
            _dialog.dismiss();
        }
    };


}
