package com.dvblogic.tvmosaic;

public class ServiceListenerThreadController
{

	private ServiceListener	worker;
	private Thread			thread;

	public ServiceListenerThreadController() {
	}

	public void start() {
		worker = new ServiceListener();
		thread = new Thread(worker);
		thread.start();
	}

	public void stop() {
		worker.disconnect();
		try {
			thread.join();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
