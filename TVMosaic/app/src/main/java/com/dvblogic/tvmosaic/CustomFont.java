package com.dvblogic.tvmosaic;

import android.content.Context;
import android.graphics.Typeface;

public class CustomFont
{
	private static CustomFont	instance;
	private static Typeface		typeface;
    private static Typeface		typeface_bold;
    private static Typeface		typeface_italic;
    private static Typeface		typeface_bold_italic;

	public static CustomFont getInstance(Context context) {
		synchronized (CustomFont.class) {
			if (instance == null) {
				instance = new CustomFont();
				typeface = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Regular.ttf");
                typeface_bold = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Bold.ttf");
                typeface_italic = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-Italic.ttf");
                typeface_bold_italic = Typeface.createFromAsset(context.getResources().getAssets(), "fonts/Roboto-BoldItalic.ttf");
			}
			return instance;
		}
	}

	public Typeface getTypeFace() {
		return typeface;
	}
    public Typeface getTypeFace_Bold() {
        return typeface_bold;
    }
    public Typeface getTypeFace_Italic() {
        return typeface_italic;
    }
    public Typeface getTypeFace_BoldItalic() {
        return typeface_bold_italic;
    }
}
