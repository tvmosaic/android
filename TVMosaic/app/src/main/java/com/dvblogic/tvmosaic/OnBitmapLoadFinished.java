package com.dvblogic.tvmosaic;

/**
 * Created by Serg on 12-Apr-16.
 */
public interface OnBitmapLoadFinished {
    void loadFinished();
}
