package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import TVMosaic.TVMosaic.R;

import android.os.IBinder;
import android.support.v7.app.MediaRouteButton;
import android.text.InputType;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.widget.*;
import com.dvblogic.dvblink_common.*;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;

import java.util.*;

import static com.dvblogic.tvmosaic.ActionOptionsPaneItems.empty_selected_option_id_;

public class MediaLibraryActivity extends BaseActivity implements ActionPane.ActionPaneEventListener, OnBitmapLoadFinished
{
    private static final String TAG = "MediaLibraryActivity";

    private static final int VIDEO_INFO_ACTIVITY_REQUEST_CODE = 100;
    private static final int PLAYBACK_ACTIVITY_REQUEST_CODE = 101;

    private class TVMSessionManagerListener implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionEnded(CastSession session, int error) {
            if (session == mCastSession) {
                mCastSession = null;
            }
        }

        @Override
        public void onSessionResumed(CastSession session, boolean wasSuspended) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarted(CastSession session, String sessionId) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarting(CastSession session) {
        }

        @Override
        public void onSessionStartFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionEnding(CastSession session) {
        }

        @Override
        public void onSessionResuming(CastSession session, String sessionId) {
        }

        @Override
        public void onSessionResumeFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionSuspended(CastSession session, int reason) {
        }
    }


    private class ResultsAdapter extends BaseAdapter {
        private ArrayList<Item> video_items = new ArrayList<>();

        private LayoutInflater ltInflater;
        private int positionLastShowedProgramInfo = -1;

        private View.OnClickListener resultItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgramInfo((int) view.getTag(), true);
            }
        };
        private View.OnFocusChangeListener resultItemFocusListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (!focus) {
                    return;
                }
                showProgramInfo((int) view.getTag(), false);
            }
        };
        private View.OnKeyListener resultItemKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && Common.okPressed(keyEvent.getKeyCode())) {
                    showProgramInfo((int) view.getTag(), true);
                    return true;
                }
                return false;
            }
        };

        private void showProgramInfo(int position, boolean extend) {
            if (position == positionLastShowedProgramInfo && !extend) {
                return;
            }
            positionLastShowedProgramInfo = position;

            if (isVideoItem(position)) {
                VideoItem vi = (VideoItem)video_items.get(position);

                if (programDetailsPane.getVisibility() == View.VISIBLE)
                    programDetailsPane.setInfo(vi.VideoInfo, "", 0, false, false, language_manager_);

                if (extend) {
                    startPlaybackActivity(vi);
                } else {
                    setProgramImage(vi);
                }
            }
        }

        public ResultsAdapter() {
            ltInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return video_items.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ltInflater.inflate(R.layout.playback_item, parent, false);
            }

            if (isVideoItem(position)) {
                VideoItem vi = (VideoItem) video_items.get(position);

                TextView label;
                label = (TextView) view.findViewById(R.id.title);
                if (vi.VideoInfo.Title != null) {
                    String text = vi.VideoInfo.Title;
                    label.setText(text);
                }
                label.setTextColor(getResources().getColor(R.color.new_text_primary_active));

                if (!Utils.usesRemoteControl(_context)) {
                    String str = ((vi.VideoInfo.Subname == null) ? "" : vi.VideoInfo.Subname);
                    String ES = "";
                    if (vi.VideoInfo.SeasonNum != 0)
                        ES += String.format("S%02d", vi.VideoInfo.SeasonNum);

                    if (vi.VideoInfo.EpisodeNum != 0) {
                        ES += String.format("E%02d", vi.VideoInfo.EpisodeNum);
                    }
                    if (str.isEmpty()) {
                        str += ES;
                    } else {
                        if (!ES.isEmpty())
                            str += "  |  " + ES;
                    }

                    label = (TextView) view.findViewById(R.id.subtitle);
                    label.setText(str);
                    label.setTextColor(getResources().getColor(R.color.new_text_primary_active));
                    if (!str.isEmpty())
                        label.setVisibility(View.VISIBLE);
                    else
                        label.setVisibility(View.GONE);
                }
            }
            view.setTag(position);
            view.setOnFocusChangeListener(resultItemFocusListener);
            view.setOnClickListener(resultItemClickListener);
            view.setOnKeyListener(resultItemKeyListener);

            return view;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return video_items.get(position);
        }

        public void setVideoItems(PlaybackItemList vi) {
            video_items = vi.list();
        }

        public boolean isVideoItem(int position) {
            return (getItem(position) instanceof VideoItem);
        }
    }

    private class MyActionPane extends ActionPane {

        public static final int invalid_source_index_ = -1;
        public int sources_item_index_;

        public MyActionPane(BaseActivity context, ActionPaneEventListener listener) {
            super(context, listener);
        }

        public void selectSource(String source_id){
            if (sources_item_index_ != invalid_source_index_) {
                ActionOptionsPaneItems options = get_options(sources_item_index_);
                if (options != null) {
                    options.set_selected_item_id(source_id);
                }
            }
        }

        public void selectFilter(String filter_id) {
            for (int i=0; i<items_.size(); i++) {
                if (items_.get(i).id_ == sources_item_index_)
                    continue;

                ActionOptionsPaneItems options = items_.get(i).options_;
                if (options != null) {
                    ActionOptionsPaneItems.ActionOptionsPaneItem item = options.find(filter_id);
                    if (item != null)
                        options.set_selected_item_id(filter_id);
                    else
                        options.set_selected_item_id(ActionOptionsPaneItems.empty_selected_option_id_);
                }
            }
        }

        protected void setSearchActive(boolean active) {
            for (int i=0; i<items_.size(); i++) {
                if (items_.get(i).id_ == media_library_search_pane_action_id_) {
                    if (active) {
                        items_.get(i).text_ = LocaleStrings.IDS_SEARCH_PROGRAMS_PAGE_TITLE;
                        items_.get(i).left_icon_ = R.drawable.tvm_search_icon_normal;
                        items_.get(i).left_icon_selected_ = R.drawable.tvm_search_icon_hover;
                    } else {
                        items_.get(i).text_ = LocaleStrings.IDS_RESET_SEARCH;
                        items_.get(i).left_icon_ = R.drawable.tvm_reset_icon_normal;
                        items_.get(i).left_icon_selected_ = R.drawable.tvm_reset_icon_hover;
                    }
                    break;
                }
            }
        }

        protected void add_extra_pane_items() {

            sources_item_index_ = invalid_source_index_;

            //refresh
            ActionPaneItem refresh_api = new ActionPaneItem();
            refresh_api.text_ = LocaleStrings.IDS_REFRESH;
            refresh_api.id_ = media_library_refresh_pane_action_id_;
            refresh_api.left_icon_ = R.drawable.tvm_refresh_icon_normal;
            refresh_api.left_icon_selected_ = R.drawable.tvm_refresh_icon_hover;
            items_.add(0, refresh_api);

            int apiId = ActionPane.user_id_;

            if (source_structure_ != null) {
                //search
                ActionPaneItem search_api = new ActionPaneItem();
                search_api.text_ = LocaleStrings.IDS_SEARCH_PROGRAMS_PAGE_TITLE;
                search_api.id_ = media_library_search_pane_action_id_;
                search_api.left_icon_ = R.drawable.tvm_search_icon_normal;
                search_api.left_icon_selected_ = R.drawable.tvm_search_icon_hover;
                items_.add(0, search_api);

                //categories
                if (source_structure_.categories.size() > 0) {
                    for (int i=source_structure_.categories.size()-1; i>=0; i--) {
                        ActionPaneItem api = new ActionPaneItem();
                        api.id_ = ++apiId;
                        api.raw_text_ = source_structure_.categories.get(i).category.Name;
                        api.left_icon_ = R.drawable.tvm_sort_icon_normal;
                        api.left_icon_selected_ = R.drawable.tvm_sort_icon_hover;
                        api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
                        api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
                        api.options_ = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON);
                        items_.add(0, api);

                        ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode select_mode = ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON;
                        ActionOptionsPaneItems sorting_options = new ActionOptionsPaneItems(select_mode);
                        for (Container cnt : source_structure_.categories.get(i).subcategories) {
                            sorting_options.add_item(cnt.Name, cnt.ObjectID);
                        }
                        sorting_options.set_selected_item_id(empty_selected_option_id_);
                        this.set_options(apiId, sorting_options);
                    }
                }
            }
            //sources
            if (sources_ != null && sources_.size() > 0) {
                ActionPaneItem src_api = new ActionPaneItem();
                src_api.id_ = ++apiId;
                src_api.text_ = LocaleStrings.IDS_MEDIA_LIBRARY_SOURCES;
                src_api.left_icon_ = R.drawable.tvm_sort_icon_normal;
                src_api.left_icon_selected_ = R.drawable.tvm_sort_icon_hover;
                src_api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
                src_api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
                src_api.options_ = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON);
                items_.add(0, src_api);

                ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode select_mode = ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON;
                ActionOptionsPaneItems sorting_options = new ActionOptionsPaneItems(select_mode);
                for (MediaLibraryProvider.MediaLibrarySource src: sources_) {
                    sorting_options.add_item(src.name, src.id);
                }
                sorting_options.set_selected_item_id(empty_selected_option_id_);
                this.set_options(apiId, sorting_options);

                sources_item_index_ = apiId;
            }

        }
    }

    private class MediaLibraryProviderEx extends MediaLibraryProvider {
        public MediaLibraryProviderEx() {
            super(MediaLibraryActivity.this);
        }

        @Override
        protected void onSources(ArrayList<MediaLibrarySource> sources) {
            processSources(sources);
        }

        @Override
        protected void onSourceStructure(MediaLibrarySourceStructure source_structure) {
            processSourceStructure(source_structure);
        }

        @Override
        protected void onItems(PlaybackItemContainer items) {
            processItems(items);
        }

        @Override
        protected void onItem(PlaybackItemContainer item)
        {
            processItem(item);
        }

        @Override
        protected void onSearchResults(PlaybackItemList items) {
            processSearchResults(items);
        }

        @Override
        protected void onError() {}

        @Override
        protected void onResumeInfo(ResumeInfo ri) {
            processResumeInfo(ri);
        }

    }

    private static int max_items_per_request_ = 200;

    private ListView results;
    private ProgramDetails programDetailsPane;
    private MyActionPane _options_menu;
    private MediaLibraryProviderEx provider;
    private ArrayList<MediaLibraryProvider.MediaLibrarySource> sources_ = null;
    private String current_source_id_ = null;
    private MediaLibraryProvider.MediaLibrarySourceStructure source_structure_ = null;
    private Container current_container_ = null;
    private int current_start_idx_ = 0;
    private StatusLine _status_line;
    private String last_search_text_ = "";
    private boolean search_mode_enabled_ = false;
    private String saved_container_id_ = null;
    private VideoItem pending_playback_vi_ = null;

    // Date picker image viewer used for Show menu
    ImageView showMenu;

    //cast controls and variables
    private CastContext mCastContext = null;
    private MediaRouteButton mMediaRouteButton = null;
    private CastSession mCastSession = null;
    private final SessionManagerListener<CastSession> mSessionManagerListener = new TVMSessionManagerListener();

    @Override
    public void loadFinished() {}

    public MediaLibraryActivity() {
        show_exit_dialog = false;
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
        }
    };

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.media_library_activity);

        getIntent().setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        //initialize cast context only on mobile devices and only if google apis are available
        if (!Utils.usesRemoteControl(this) && Common.isGoogleApiAvailable(this)) {
            try {
                mCastContext = CastContext.getSharedInstance(this);
            } catch (Exception e) {
                mCastContext = null;
            }
        }

        if (mCastContext != null) {
            mMediaRouteButton = (MediaRouteButton) findViewById(R.id.media_route_button);
            CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), mMediaRouteButton);

            mMediaRouteButton.setVisibility(View.VISIBLE);

            ViewStub miniControllerStub = (ViewStub) findViewById(R.id.cast_mini_controller);
            miniControllerStub.inflate();
        }

        //request more items per page on the mobile phones - it is easier and faster to scroll vertically than on remote control devices
        if (Utils.usesRemoteControl(this))
            max_items_per_request_ = 200;
        else
            max_items_per_request_ = 300;

        int number_of_visible_rows = 5;
        int row_height  = (int)this.getResources().getDimension(R.dimen.search_item_height);
        int divider_width = (int)this.getResources().getDimension(R.dimen.search_divider_width);

        final int layout_height = Utils.usesRemoteControl(this) ? (row_height * number_of_visible_rows + divider_width * number_of_visible_rows) : ViewGroup.LayoutParams.MATCH_PARENT;
        final View lv = findViewById(R.id.video_items);

        if (!Utils.usesRemoteControl(this)) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) lv.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, R.id.status_line);
            lv.setLayoutParams(params);
        }

        lv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lv.getLayoutParams().height = layout_height;
                lv.requestLayout();
                lv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        _status_line = new StatusLine(findViewById(R.id.status_line));

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(language_manager_.GetString(LocaleStrings.IDS_MEDIA_LIBRARY_PAGE_TITLE));

        View root = findViewById(R.id.media_library_layout);
        results = (ListView) root.findViewById(R.id.video_items);
        programDetailsPane = (ProgramDetails) root.findViewById(R.id.program_details_pane);
        if (!Utils.usesRemoteControl(this))
            programDetailsPane.setVisibility(View.GONE);

        updateScreenTitle();

        provider = new MediaLibraryProviderEx();

        //Show menu on  date picker click
        showMenu = (ImageView) findViewById(R.id.date_picker);
        showMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _options_menu.show();
            }
        });

        ImageView pageUp = (ImageView) findViewById(R.id.page_up);
        pageUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_up();
            }
        });

        ImageView pageDown = (ImageView) findViewById(R.id.page_down);
        pageDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                page_down();
            }
        });

        createMenu();

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                refresh();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        if (mCastContext != null) {
            mCastContext.getSessionManager().addSessionManagerListener(
                    mSessionManagerListener, CastSession.class);
            if (mCastSession == null) {
                mCastSession = CastContext.getSharedInstance(this).getSessionManager()
                        .getCurrentCastSession();
            }
            if (mCastSession != null) {
                RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
                if (remoteMediaClient != null)
                    remoteMediaClient.addListener(new TVMRemoteMediaClientListener(mCastSession));
            }

        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mCastContext != null) {
            mCastContext.getSessionManager().removeSessionManagerListener(mSessionManagerListener, CastSession.class);
        }
        super.onPause();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){

        if (current_container_ != null && !search_mode_enabled_) {
            if (keyCode == KeyEvent.KEYCODE_DPAD_LEFT || keyCode == KeyEvent.KEYCODE_MEDIA_REWIND) {
                page_down();
                return true;
            }
            if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_MEDIA_FAST_FORWARD) {
                page_up();
                return true;
            }
        }

        if (keyCode == KeyEvent.KEYCODE_MENU) {
            _options_menu.show();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }

    protected void page_up() {
        if (current_container_ != null) {
            if (current_start_idx_ + max_items_per_request_ <= current_container_.TotalCount) {
                current_start_idx_ += max_items_per_request_;

                //request first page of the items
                provider.requestItems(current_container_.ObjectID, current_start_idx_, max_items_per_request_);
            }
        }
    }

    protected void page_down() {
        if (current_container_ != null) {
            if (current_start_idx_ != 0) {
                current_start_idx_ -= max_items_per_request_;
                if (current_start_idx_ < 0)
                    current_start_idx_ = 0;

                //request first page of the items
                provider.requestItems(current_container_.ObjectID, current_start_idx_, max_items_per_request_);
            }
        }
    }

    protected void updateScreenTitle() {
        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);

        if (current_source_id_ == null) {
            v.setText(language_manager_.GetString(LocaleStrings.IDS_MEDIA_LIBRARY_PAGE_TITLE));
        } else if (search_mode_enabled_) {
            String str = String.format("%d - %d / %d | %s", 1, results.getAdapter().getCount(), results.getAdapter().getCount(), language_manager_.GetString(LocaleStrings.IDS_SEARCH_RESULTS_FOR) +": " + last_search_text_);
            v.setText(str);
        } else {
            String src_name = "";
            for (MediaLibraryProvider.MediaLibrarySource src : sources_) {
                if (src.id.equals(current_source_id_)) {
                    src_name = src.name;
                    break;
                }
            }
            if (current_container_ != null) {
                String str = String.format("%d - %d / %d | %s | %s", current_start_idx_ + 1, current_start_idx_ + results.getAdapter().getCount(), current_container_.TotalCount, current_container_.Name, src_name);
                v.setText(str);
            } else {
                v.setText(src_name);
            }
        }
    }

    @Override
    public void DataProcessing(String command, Object result_data) {
        provider.DataProcessing(command, result_data);
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        provider.DataProcessing(command, error);
    }

    @Override
    public void onBackPressed() {
        _options_menu.show();
    }

    protected void createMenu() {
        _options_menu = new MyActionPane(this, this);
    }

    public void onActionPaneAction(int action_id) {
        if (action_id == ActionPane.media_library_refresh_pane_action_id_) {
            _options_menu.hide();
            cancelSearchResults();
            refresh();
        } else
        if (action_id == ActionPane.goto_pane_action_id_) {
            TopLevelNavigationPane tln = new TopLevelNavigationPane(TopLevelNavigationPane.exclude_flags_media_library_activity, this);
            tln.show();
        } else
        if (ActionPane.media_library_search_pane_action_id_ == action_id) {
            _options_menu.hide();

            if (search_mode_enabled_) {
                cancelSearchResults();
                //change current source and request structure of the new source
                selectContainer(current_container_);

            } else {
                if (current_container_ != null) {
                    TextInputDialog tid = new TextInputDialog(_context,
                            language_manager_.GetString(LocaleStrings.IDS_SEARCH_FOR),
                            language_manager_.GetString(LocaleStrings.IDS_SEARCH_BUTTON),
                            language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL));

                    tid.setOnResultListener(new TextInputDialog.OnResultListener() {
                        @Override
                        public void onResult(boolean result, String text) {
                            if (result) {

                                int minimumSearchLength = 3;
                                if (text.length() < minimumSearchLength) {
                                    Toast.makeText(MediaLibraryActivity.this, String.format(language_manager_.GetString(LocaleStrings.IDS_SEARCH_KEYWORDS_ERROR_MSG), minimumSearchLength), Toast.LENGTH_SHORT).show();
                                } else {
                                    last_search_text_ = text;
                                    provider.searchObjects(current_container_.ObjectID, last_search_text_);
                                }
                            }
                        }
                    });

                    tid.showDialog(InputType.TYPE_CLASS_TEXT, last_search_text_);
                }
            }
        }
    }

    class SourceSelectionRunnable implements Runnable {

        String selected_option_;

        public SourceSelectionRunnable(String selected_option) {
            selected_option_ = selected_option;
        }

        @Override
        public void run() {

            cancelSearchResults();

            source_structure_ = null;
            current_container_ = null;

            results.setAdapter(null);
            programDetailsPane.clear();
            setProgramImage(null);

            //recreate menu with sources without structure
            createMenu();
            //change current source and request structure of the new source
            setCurrentSource(selected_option_);
        }
    }

    protected Container get_container_by_id(String id) {
        Container container = null;

        if (source_structure_ != null) {
            if (source_structure_.categories != null) {
                for (MediaLibraryProvider.MediaLibraryCategory category : source_structure_.categories) {
                    for (int i=0; i<category.subcategories.size(); i++) {
                        if (category.subcategories.get(i).ObjectID.equalsIgnoreCase(id)) {
                            container = category.subcategories.get(i);
                            break;
                        }
                    }
                    if (container != null)
                        break;
                }
            }
        }
        return container;
    }

    public void onActionPaneOption(int option_id, String selected_option) {

        _options_menu.hide();

        if (option_id == _options_menu.sources_item_index_) {
            runOnUiThread(new SourceSelectionRunnable(selected_option));

        } else {
            cancelSearchResults();

            //new filter is selected
            results.setAdapter(null);
            programDetailsPane.clear();
            setProgramImage(null);

            //change current source and request structure of the new source
            Container container = get_container_by_id(selected_option);
            selectContainer(container);
        }

    }

    private void clearData() {

        current_source_id_ = null;
        source_structure_ = null;
        sources_ = null;
        current_container_ = null;

        results.setAdapter(null);
        programDetailsPane.clear();
        setProgramImage(null);
    }

    private void setProgramImage(VideoItem vi) {
        if (programDetailsPane.getVisibility() == View.VISIBLE) {
            programDetailsPane.findViewById(R.id.description_image).setVisibility(View.INVISIBLE);
            if (vi == null) {
                return;
            }
            boolean loadBitmap = (vi.Thumbnail != null && !vi.Thumbnail.isEmpty());
            if (loadBitmap) {
                new com.dvblogic.tvmosaic.BitmapLoader(
                        this, this, vi.Thumbnail,
                        (ImageView) programDetailsPane.findViewById(R.id.description_image), null
                ).start();
            }
        }
    }

    protected class ResumeRunnable implements Runnable {
        String vi_obj_id_;
        int resume_pos_;

        ResumeRunnable(String vi_obj_id, int resume_pos) {
            vi_obj_id_ = vi_obj_id;
            resume_pos_ = resume_pos;
        }

        ResumeRunnable(String vi_obj_id) {
            vi_obj_id_ = vi_obj_id;
            resume_pos_ = -1;
        }

        public void run() {
            if (resume_pos_ != -1)
                provider.requestSetResumeInfo(vi_obj_id_, resume_pos_);
            else
                provider.requestResumeInfo(vi_obj_id_);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == VIDEO_INFO_ACTIVITY_REQUEST_CODE && resultCode != RESULT_CANCELED) {
            //start playback
            if (data != null) {
                String video_item_str = data.getStringExtra("video_item");
                if (video_item_str != null) {
                    VideoItem vi = null;
                    try {
                        vi = (VideoItem) Serializer.deserialize(video_item_str, VideoItem.class);
                    } catch (Exception e) {
                        vi = null;
                    }
                    if (vi != null) {
                        pending_playback_vi_ = vi;
                        getWindow().getDecorView().post(new ResumeRunnable(pending_playback_vi_.ObjectID));
                    }
                }
            }
        }
        if (requestCode == PLAYBACK_ACTIVITY_REQUEST_CODE) {
            if (data != null) {
                int resume_position = data.getIntExtra("resume_position", 0);

                getWindow().getDecorView().post(new ResumeRunnable(pending_playback_vi_.ObjectID, resume_position));
            }
        }
    }

    public void refresh() {
        clearData();

        provider.requestSources();
    }

    protected void processSources(ArrayList<MediaLibraryProvider.MediaLibrarySource> sources) {
        sources_ = sources;

        //recreate menu with sources
        createMenu();

        if (sources_ != null && sources_.size() > 0) {
            //restore saved playback source (if any)
            String saved_src_id = settings_.getValue(SettingsManager.LAST_PLAYBACK_SRC_ID_KEY, SettingsManager.LAST_PLAYBACK_SRC_ID_DEFAULT);
            saved_container_id_ = settings_.getValue(SettingsManager.LAST_PLAYBACK_CONTAINER_ID_KEY, SettingsManager.LAST_PLAYBACK_CONTAINER_ID_DEFAULT);
            String src_id = null;
            //check if it is present in the source list
            for (int i=0; i<sources.size(); i++) {
                if (sources.get(i).id.equalsIgnoreCase(saved_src_id)) {
                    src_id = saved_src_id;
                    break;
                }
            }

            if (src_id == null)
                src_id = sources.get(0).id;

            setCurrentSource(src_id);
        } else {
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_MEDIA_LIBRARY_EMPTY), Toast.LENGTH_SHORT).show();
        }
    }

    protected void setCurrentSource(String source_id) {
        current_source_id_ = source_id;
        current_start_idx_ = 0;
        settings_.setValue(SettingsManager.LAST_PLAYBACK_SRC_ID_KEY, source_id);

        _options_menu.selectSource(current_source_id_);
        updateScreenTitle();

        //request source structure
        provider.requestSourceStructure(current_source_id_);
    }

    protected void processSourceStructure(MediaLibraryProvider.MediaLibrarySourceStructure source_structure) {
        source_structure_ = source_structure;

        if (source_structure_ != null)
            source_structure_.sort();

        //update pane menu with new selected source and its structure
        createMenu();
        //select current source
        _options_menu.selectSource(current_source_id_);

        Container first_container = null;
        Container saved_container = null;

        if (source_structure_ != null) {
            //find first container with items
            if (source_structure_.categories != null) {
                for (MediaLibraryProvider.MediaLibraryCategory category : source_structure_.categories) {
                    for (int i=0; i<category.subcategories.size(); i++) {
                        if (first_container == null)
                            first_container = category.subcategories.get(i);

                        if (category.subcategories.get(i).ObjectID.equalsIgnoreCase(saved_container_id_))
                            saved_container = category.subcategories.get(i);;
                    }
                }
            }
        }

        Container container = saved_container != null ? saved_container : first_container;

        if (container == null)
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_MEDIA_LIBRARY_EMPTY), Toast.LENGTH_SHORT).show();
        else
            selectContainer(container);
    }

    protected void selectContainer(Container container) {
        current_container_ = container;
        current_start_idx_ = 0;

        settings_.setValue(SettingsManager.LAST_PLAYBACK_CONTAINER_ID_KEY, container.ObjectID);

        //update actionPane with a new selected item
        _options_menu.selectFilter(current_container_.ObjectID);

        //request first page of the items
        provider.requestItems(current_container_.ObjectID, current_start_idx_, max_items_per_request_);
    }

    protected void processItems(PlaybackItemContainer items) {

        ResultsAdapter resultsAdapter = new ResultsAdapter();
        if (items != null) {
            resultsAdapter.setVideoItems(items.Items);
            current_container_.TotalCount = items.TotalCount;
        }

        if (resultsAdapter.getCount() == 0)
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_MEDIA_LIBRARY_EMPTY), Toast.LENGTH_SHORT).show();

        results.setAdapter(resultsAdapter);
        results.setItemsCanFocus(true);

        //update title
        updateScreenTitle();
    }

    protected void cancelSearchResults() {
        if (search_mode_enabled_) {
            search_mode_enabled_ = false;
            _options_menu.setSearchActive(true);

            updateScreenTitle();

            results.setAdapter(null);
            programDetailsPane.clear();
            setProgramImage(null);
        }
    }

    protected void processSearchResults(PlaybackItemList items) {

        if (items != null && items.size() > 0) {
            search_mode_enabled_ = true;
            _options_menu.setSearchActive(false);

            ResultsAdapter resultsAdapter = new ResultsAdapter();
            resultsAdapter.setVideoItems(items);

            results.setAdapter(resultsAdapter);
            results.setItemsCanFocus(true);

            //update title
            updateScreenTitle();
        } else {
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_MEDIA_LIBRARY_EMPTY), Toast.LENGTH_SHORT).show();
        }
    }

    void processStartPlaybackActivity(VideoItem vi, int resume_pos) {
        if (vi != null) {
            if (mCastSession != null) {
                startChromcastPlayback(vi, resume_pos);
            } else {
                Intent i = new Intent();
                i.setClass(this, VLCPlayVideoItemActivity.class);
                String xml_video_item = Serializer.serialize(vi);
                i.putExtra("video_item", xml_video_item);
                i.putExtra("resume_position", resume_pos);
                startActivityForResult(i, PLAYBACK_ACTIVITY_REQUEST_CODE);
            }
        }
    }

    void processStartVideoItemActivity(VideoItem vi) {
        if (vi != null) {
            Intent i = new Intent();
            i.setClass(this, MediaLibraryVideoActivity.class);
            String xml_video_item = Serializer.serialize(vi);
            i.putExtra("video_item", xml_video_item);
            startActivityForResult(i, VIDEO_INFO_ACTIVITY_REQUEST_CODE);
        }
    }

    void processItem(PlaybackItemContainer item) {
        if (item != null && item.Items != null && item.Items.size() > 0) {
            if (item.Items.get(0) instanceof VideoItem) {
                VideoItem vi = (VideoItem)item.Items.get(0);
                processStartVideoItemActivity(vi);
            }
        }
    }

    void startPlaybackActivity(VideoItem vi) {
        provider.requestItem(vi.ObjectID);
    }

    protected class ResumeOnClickListener implements DialogInterface.OnClickListener {
        int resume_pos_ = 0;

        public ResumeOnClickListener(int resume_pos) {
            resume_pos_ = resume_pos;
        }

        public void onClick(DialogInterface dialog, int which) {
            processStartPlaybackActivity(pending_playback_vi_, resume_pos_);
        }
    }

    protected void processResumeInfo(ResumeInfo ri) {
        //do not ask about resume position for chromecast. We will not be able to update it back anyway
        if (mCastSession == null && ri.pos > 0) {
            new AlertDialog.Builder(_context)
                    .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_INFO))
                    .setMessage(
                            language_manager_.GetString(LocaleStrings.IDS_PLAY_RESUME_QUESTION))
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            processStartPlaybackActivity(pending_playback_vi_, 0);
                        }
                    })
                    .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
                            new ResumeOnClickListener(ri.pos))
                    .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    processStartPlaybackActivity(pending_playback_vi_, 0);
                                }
                            }).show();
        } else {
            processStartPlaybackActivity(pending_playback_vi_, 0);
        }
    }

    protected void startChromcastPlayback(VideoItem vi, int resume_pos) {
        RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
        if (remoteMediaClient != null) {

            MediaMetadata mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
            mediaMetadata.putString(MediaMetadata.KEY_TITLE, vi.VideoInfo.Title);
            if (vi.VideoInfo.Subname != null && !vi.VideoInfo.Subname.isEmpty())
                mediaMetadata.putString(MediaMetadata.KEY_SUBTITLE, vi.VideoInfo.Subname);
            if (vi.VideoInfo.Image != null && !vi.VideoInfo.Image.isEmpty())
                mediaMetadata.addImage(new WebImage(Uri.parse(vi.VideoInfo.Image)));
            if (vi.VideoInfo.SeasonNum > 0)
                mediaMetadata.putInt(MediaMetadata.KEY_SEASON_NUMBER, vi.VideoInfo.SeasonNum);
            if (vi.VideoInfo.EpisodeNum > 0)
                mediaMetadata.putInt(MediaMetadata.KEY_EPISODE_NUMBER, vi.VideoInfo.EpisodeNum);

            MediaInfo mi = new MediaInfo.Builder(vi.Url)
                    .setContentType("video/mpeg")
                    .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                    .setMetadata(mediaMetadata).build();

            remoteMediaClient.addListener(new TVMRemoteMediaClientListener(mCastSession));

            remoteMediaClient.load(mi, true, (long)resume_pos * 1000);
        }
    }


}
