package com.dvblogic.tvmosaic;

import android.annotation.SuppressLint;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedSet;
import java.util.TreeSet;

@SuppressLint("UseSparseArrays") 
public class Genres
{
	public static final long	RDGC_ANY			= 0x00000000;
	public static final long	RDGC_NEWS			= 0x00000001;
	public static final long	RDGC_KIDS			= 0x00000002;
	public static final long	RDGC_MOVIE			= 0x00000004;
	public static final long	RDGC_SPORT			= 0x00000008;
	public static final long	RDGC_DOCUMENTARY	= 0x00000010;
	public static final long	RDGC_ACTION			= 0x00000020;
	public static final long	RDGC_COMEDY			= 0x00000040;
	public static final long	RDGC_DRAMA			= 0x00000080;
	public static final long	RDGC_EDU			= 0x00000100;
	public static final long	RDGC_HORROR			= 0x00000200;
	public static final long	RDGC_MUSIC			= 0x00000400;
	public static final long	RDGC_REALITY		= 0x00000800;
	public static final long	RDGC_ROMANCE		= 0x00001000;
	public static final long	RDGC_SCIFI			= 0x00002000;
	public static final long	RDGC_SERIAL			= 0x00004000;
	public static final long	RDGC_SOAP			= 0x00008000;
	public static final long	RDGC_SPECIAL		= 0x00010000;
	public static final long	RDGC_THRILLER		= 0x00020000;
	public static final long	RDGC_ADULT			= 0x00040000;

	static LinkedHashMap<Long, String> sortedHashMap = new LinkedHashMap<Long, String>();

	public static void init(LanguageManager mng) {
		HashMap<Long, String> map = new HashMap<Long, String>();
		map.put(RDGC_NEWS, mng.GetString(LocaleStrings.IDS_GENRE_NEWS));
		map.put(RDGC_KIDS, mng.GetString(LocaleStrings.IDS_GENRE_KIDS));
		map.put(RDGC_MOVIE, mng.GetString(LocaleStrings.IDS_GENRE_MOVIE));
		map.put(RDGC_SPORT, mng.GetString(LocaleStrings.IDS_GENRE_SPORT));
		map.put(RDGC_DOCUMENTARY, mng.GetString(LocaleStrings.IDS_GENRE_DOCUMENTARY));
		map.put(RDGC_ACTION, mng.GetString(LocaleStrings.IDS_GENRE_ACTION));
		map.put(RDGC_COMEDY, mng.GetString(LocaleStrings.IDS_GENRE_COMEDY));
		map.put(RDGC_DRAMA, mng.GetString(LocaleStrings.IDS_GENRE_DRAMA));
		map.put(RDGC_EDU, mng.GetString(LocaleStrings.IDS_GENRE_EDU));
		map.put(RDGC_HORROR, mng.GetString(LocaleStrings.IDS_GENRE_HORROR));
		map.put(RDGC_MUSIC, mng.GetString(LocaleStrings.IDS_GENRE_MUSIC));
		map.put(RDGC_REALITY, mng.GetString(LocaleStrings.IDS_GENRE_REALITY));
		map.put(RDGC_ROMANCE, mng.GetString(LocaleStrings.IDS_GENRE_ROMANCE));
		map.put(RDGC_SCIFI, mng.GetString(LocaleStrings.IDS_GENRE_SCIFI));
		map.put(RDGC_SERIAL, mng.GetString(LocaleStrings.IDS_GENRE_SERIAL));
		map.put(RDGC_SOAP, mng.GetString(LocaleStrings.IDS_GENRE_SOAP));
		map.put(RDGC_SPECIAL, mng.GetString(LocaleStrings.IDS_GENRE_SPECIAL));
		map.put(RDGC_THRILLER, mng.GetString(LocaleStrings.IDS_GENRE_THRILLER));
		map.put(RDGC_ADULT, mng.GetString(LocaleStrings.IDS_GENRE_ADULT));

		final Set<Map.Entry<Long, String>> entry = map.entrySet();
		final Comparator<Map.Entry<Long, String>> comparator = new Comparator<Map.Entry<Long, String>>() {
			@Override
			public int compare(Map.Entry<Long, String> o1, Map.Entry<Long, String> o2) {
				String value1 = o1.getValue();
				String value2 = o2.getValue();
				return value1.compareTo(value2);
			}
		};
		final SortedSet<Map.Entry<Long, String>> sortedSet = new TreeSet<Entry<Long, String>>(comparator);
		sortedSet.addAll(entry);
		sortedHashMap = new LinkedHashMap<Long, String>();
		for (Map.Entry<Long, String> entry1 : sortedSet) {
			sortedHashMap.put(entry1.getKey(), entry1.getValue());
		}
	}

	public static String toString(long key) {
			return sortedHashMap.get(key);
	}
	
	public static long fromString(String name) {
		for (Map.Entry<Long, String> entry : sortedHashMap.entrySet()) {
			if (entry.getValue().equalsIgnoreCase(name)) {
				return entry.getKey();
			}
		}
		return RDGC_ANY;
	}
	
	public static String[] keywords() {
		return sortedHashMap.values().toArray(new String[sortedHashMap.values().size()]);
	}

	public static String maskToString(long mask) {
        String ret_val = new String();

        for (Map.Entry<Long, String> entry : sortedHashMap.entrySet()) {
            if ((mask & entry.getKey()) != 0) {
                if (!ret_val.isEmpty())
                    ret_val += "/";

                ret_val += entry.getValue();
            }
        }

        return ret_val;
    }
}
