package com.dvblogic.tvmosaic;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.text.util.Linkify;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dvblogic.dvblink_common.Utils;

import java.util.Calendar;

import TVMosaic.TVMosaic.R;

public class AboutActivity extends BaseActivity
{
	public AboutActivity() {
		show_exit_dialog = false;
	}

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);

		setContentView(R.layout.about_activity);

//		about_logo

		// set activity result to cancelled
		setResult(Activity.RESULT_CANCELED, this.getIntent());

		SetFields();
	}

	public void onBackPressed() {
		super.onBackPressed();
	}
	
	protected void SetFields() {
		TextView tv = null;

		PackageInfo pInfo = null;
		String versionName = "";
        int versionCode = 0;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			versionName = pInfo.versionName;
            versionCode = pInfo.versionCode;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		tv = (TextView) this.findViewById(R.id.about_title);
		tv.setText(R.string.app_name, TextView.BufferType.NORMAL);

		Calendar now = Calendar.getInstance();
		int year = now.get(Calendar.YEAR);

		String str = String.format("%s %s(%d)<br><br>Copyright (c) 2008 - %s DVBLogic<br>%s",
				language_manager_.GetString(LocaleStrings.IDS_VERSION), versionName, versionCode, String.valueOf(year),
				language_manager_.GetString(LocaleStrings.IDS_COPYRIGHT));

		tv = (TextView) this.findViewById(R.id.about_info);
		tv.setText(Html.fromHtml(str), TextView.BufferType.NORMAL);

		tv = (TextView) this.findViewById(R.id.about_link);
        if (!Utils.usesRemoteControl(this)) {
            //disable web link
            tv.setAutoLinkMask(Linkify.WEB_URLS);
            tv.setLinksClickable(true);
        }
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_WEB_LINK));
	}
}
