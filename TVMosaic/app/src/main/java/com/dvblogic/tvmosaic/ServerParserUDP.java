package com.dvblogic.tvmosaic;

import com.dvblogic.dvblink_common.ServiceInfo;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

public class ServerParserUDP
{
	public ServiceInfo serviceInfo	= null;

	public ServerParserUDP() {
	}

	public ServiceInfo getServiceInfo(String xml) {
		serviceInfo = new ServiceInfo();
		ServiceInfoHandler handler = null;
		try {
			handler = new ServiceInfoHandler(this);
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser saxParser;
			saxParser = factory.newSAXParser();
			InputStream istr = new ByteArrayInputStream(xml.getBytes());
			BufferedReader reader = new BufferedReader(new InputStreamReader(istr));
			InputSource is = new InputSource(reader);
			is.setEncoding("UTF-8");
			saxParser.parse(is, handler);
			return (handler.isValid() ? serviceInfo : null);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	protected class ServiceInfoHandler extends DefaultHandler
	{
		private ServiceInfo	info	= null;
		String				qName;
		boolean				valid	= false;

		public ServiceInfoHandler(ServerParserUDP serverParserUDP) {
			info = serverParserUDP.serviceInfo;
		}

		public boolean isValid() {
			return valid;
		}

		public void endDocument() {
			if (!valid)
				info = null;
		}

		public void startElement(String uri, String localName, String _qName, Attributes attributes)
				throws SAXException {
			qName = _qName;
			if (qName == "server_info")
				valid = true;
		}

		public void characters(char ch[], int start, int length) throws SAXException {

			String value = new String(ch, start, length).trim();
			if (value.length() == 0)
				return; // ignore white space
			if (qName.equals("server_name")) {
				info.ServiceName = value;
			} else if (qName.equals("server_id")) {
				info.ServiceId = value;
			} else if (qName.equals("server_address")) {
				info.ServiceAdress = value;
			} else if (qName.equals("base_streaming_port")) {
				info.BaseStreamPort = Integer.parseInt(value);
			}
		}

		public void endElement(String uri, String localName, String qName) throws SAXException {
		}

	}
}
