package com.dvblogic.tvmosaic;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;
import android.widget.RelativeLayout;

import TVMosaic.TVMosaic.R;


public class ProgramInfoButton extends RelativeLayout {

    public ProgramInfoButton(Context context) {
        super(context);
    }

    public ProgramInfoButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ProgramInfoButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    public void onFinishInflate () {
        super.onFinishInflate();
    }

    @Override
    public void setEnabled(boolean enabled) {
        super.setEnabled(enabled);

        if (enabled)
            findViewById(R.id.inactive_overlay).setVisibility(GONE);
        else
            findViewById(R.id.inactive_overlay).setVisibility(VISIBLE);
    }

    @Override
    protected void onFocusChanged(boolean gainFocus,int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction,previouslyFocusedRect);

        if (gainFocus) {
            setBackgroundColor(ContextCompat.getColor(getContext(), R.color.program_details_button_bkg_selected));
        } else {
            setBackgroundColor(Color.TRANSPARENT);
        }
    }

}
