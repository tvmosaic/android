package com.dvblogic.tvmosaic;

import com.dvblogic.dvblink_common.ServiceInfo;
import com.dvblogic.dvblink_common.ServiceInfoCollection;

import java.io.IOException;
import java.net.*;
import java.util.Enumeration;

public class ServiceListener implements Runnable
{

	private final int				kGroupPort		= 65332;//65432;
	private final byte				kSendByte		= (byte) 0xDC;
	private final String			serverAddress	= "255.255.255.255";

	DatagramSocket					socket;
	private boolean					isConnected;
	Enumeration<NetworkInterface>	interfaces;
	byte[]							sendData		= { kSendByte };

	public ServiceListener() {
		isConnected = false;
		try {
			interfaces = NetworkInterface.getNetworkInterfaces();
		} catch (SocketException e) {
			e.printStackTrace();
		}
	}

	public void disconnect() {
		socket.close();
		synchronized (this) {
			isConnected = false;
		}
	}

	@Override
	public void run() {
		ServiceInfoCollection.Instance().Clear();
		try {
			socket = new DatagramSocket();
			socket.setBroadcast(true);
			synchronized (this) {
				isConnected = true;
			}
			while (true) {
				synchronized (this) {
					if (!isConnected)
						break;
				}
				find();
				Thread.sleep(300);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void find() {
		try {
			try {
				DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length,
						InetAddress.getByName(serverAddress), kGroupPort);
				socket.send(sendPacket);
			} catch (Exception e) {
				e.printStackTrace();
			}

			// Broadcast the message over all the network interfaces
			while (interfaces.hasMoreElements()) {
				NetworkInterface networkInterface = (NetworkInterface) interfaces.nextElement();

				if (networkInterface.isLoopback() || !networkInterface.isUp()) {
					continue; // Don't want to broadcast to the loopback
								// interface
				}

				for (InterfaceAddress interfaceAddress : networkInterface.getInterfaceAddresses()) {
					InetAddress broadcast = interfaceAddress.getBroadcast();
					if (broadcast == null) {
						continue;
					}

					// Send the broadcast package!
					try {
						DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, broadcast, kGroupPort);
						socket.send(sendPacket);
					} catch (Exception e) {
						e.printStackTrace();
					}

					System.out.println(getClass().getName() + ">>> Request packet sent to: "
							+ broadcast.getHostAddress() + "; Interface: " + networkInterface.getDisplayName());

				}
			}

			// Wait for a response
			byte[] recvBuf = new byte[15000];
			DatagramPacket receivePacket = new DatagramPacket(recvBuf, recvBuf.length);
			socket.receive(receivePacket);

			// We have a response
			/*
			 * System.out.println(getClass().getName() +
			 * ">>> Broadcast response from server: " +
			 * receivePacket.getAddress().getHostAddress());
			 */
			String message = new String(receivePacket.getData()).trim();
			// System.out.println(message);
			ServerParserUDP parser = new ServerParserUDP();
			ServiceInfo serviceInfo = parser.getServiceInfo(message);

			if (serviceInfo != null) {
				ServiceInfo oldInfo = ServiceInfoCollection.Instance().getServiceInfo(serviceInfo.getHash());

				if (oldInfo == null || !serviceInfo.Equals(oldInfo)) {
					ServiceInfoCollection.Instance().setServiceInfo(serviceInfo.getHash(), serviceInfo);

					// System.out.format("%s: %s%n", serviceInfo.ServiceAdress,
					// serviceInfo.ServiceName);
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

}
