package com.dvblogic.tvmosaic;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.ServerSettings;

import TVMosaic.TVMosaic.R;

import static android.text.InputType.TYPE_CLASS_NUMBER;
import static android.text.InputType.TYPE_CLASS_TEXT;

public class ServerManualParamsActivity extends BaseActivity
{
    private static final String TAG = "ServerManualParamsActivity";
    public static final Integer defaultTVMosaicPort = 9270;
    public static final String manualServerId = "95b46b58-d28b-11e7-8941-cec278b6b50a";
    private String server_addr_ = null;
    private String server_port_ = defaultTVMosaicPort.toString();
    private String server_name_;
    private boolean use_https_ = false;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.manual_tv_server_params);

        //set result to cancelled until there are changes
        setResult(RESULT_CANCELED);

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_PAGE_DESC));

        server_name_ = language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_NAME);

        ServerSettings ss = settings_.getServerSettings();
        if (ss.serverId.equals(manualServerId)) {
            server_addr_ = ss.serverAddress;
            server_port_ = String.valueOf(ss.serverPort);
            use_https_ = ss.useHttps;
        }

        customizeActivity();
    }

    protected void customizeActivity() {

        customizeProperty(R.id.server_address,
                language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_ADDRESS),
                language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_ADDRESS_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextInputDialog tid = new TextInputDialog(_context,
                                language_manager_.GetString(LocaleStrings.IDS_PROMPT_NEW_SERVER_ADDRESS),
                                language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_OK),
                                language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL));

                        tid.setOnResultListener(new TextInputDialog.OnResultListener() {
                            @Override
                            public void onResult(boolean result, String text) {
                                if (result) {
                                    server_addr_ = text;
                                    setPropertyCurrentValue(R.id.server_address, server_addr_);
                                }
                            }
                        });

                        tid.showDialog(TYPE_CLASS_TEXT, server_addr_);
                    }
                }
        );
        setPropertyCurrentValue(R.id.server_address, server_addr_);

        customizeProperty(R.id.server_port,
                language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_PORT),
                language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_PORT_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        TextInputDialog tid = new TextInputDialog(_context,
                                language_manager_.GetString(LocaleStrings.IDS_PROMPT_NEW_SERVER_PORT),
                                language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_OK),
                                language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL));

                        tid.setOnResultListener(new TextInputDialog.OnResultListener() {
                            @Override
                            public void onResult(boolean result, String text) {
                                if (result) {
                                    server_port_ = text;
                                    setPropertyCurrentValue(R.id.server_port, server_port_);
                                }
                            }
                        });

                        tid.showDialog(TYPE_CLASS_NUMBER, server_port_);
                    }
                }
        );
        setPropertyCurrentValue(R.id.server_port, server_port_);

        // https enabled
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.use_https);
        TextView tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_USE_HTTPS));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_USE_HTTPS_DESC));

        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(language_manager_.GetString(use_https_ ? LocaleStrings.IDS_ON : LocaleStrings.IDS_OFF));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                use_https_ = !use_https_;
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.use_https);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                tv.setText(language_manager_.GetString(use_https_ ? LocaleStrings.IDS_ON : LocaleStrings.IDS_OFF));
            }
        });

        customizeProperty(R.id.confirm_server,
                language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_ADD),
                language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_ADD_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        //do basic checks: address not epmty and port can be converted to int

                        boolean port_ok = true;
                        try {
                            if (server_port_ != null)
                                Integer.parseInt(server_port_);
                            else
                                port_ok = false;
                        } catch(NumberFormatException e) {port_ok = false;}

                        if (!port_ok) {
                            Toast.makeText(_context,
                                    language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_PORT_INVALID),
                                    Toast.LENGTH_SHORT).show();
                        } else
                        if (server_addr_ == null || server_addr_.isEmpty()) {
                            Toast.makeText(_context,
                                    language_manager_.GetString(LocaleStrings.IDS_SERVER_MANUAL_ADDRESS_INVALID),
                                    Toast.LENGTH_SHORT).show();
                        } else {
                            Intent intent = getIntent();
                            intent.putExtra("server_addr", server_addr_);
                            intent.putExtra("server_port", server_port_);
                            intent.putExtra("use_https", use_https_);
                            intent.putExtra("server_name", server_name_);
                            intent.putExtra("server_id", manualServerId);
                            setResult(RESULT_OK, intent);
                            finish();
                        }
                    }
                }
        );
    }

    protected void customizeProperty(int layoutID, String title, String desc, View.OnClickListener listener) {
        TextView tv;
        View lv = findViewById(R.id.server_properties);
        RelativeLayout layout = (RelativeLayout) lv.findViewById(layoutID);
        layout.setVisibility(View.VISIBLE);

        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(title);
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(desc);

        layout.setOnClickListener(listener);
    }

    protected void setPropertyCurrentValue(int layoutID, String current_value) {
        TextView tv;
        View lv = findViewById(R.id.server_properties);
        RelativeLayout layout = (RelativeLayout) lv.findViewById(layoutID);

        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(current_value);
    }

    @Override
    public void onBackPressed() {
        finish();
    }


}
