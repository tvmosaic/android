package com.dvblogic.tvmosaic;

import java.security.InvalidParameterException;
import java.util.ArrayList;

/**
 * Created by Pavlo on 12-12-2016.
 */

public class ActionOptionsPaneItems {

    public enum ActionOptionsPaneItemsSelectMode {
        AOPI_SELECT_MODE_CHECK_BOX(0), AOPI_SELECT_MODE_RADIO_BUTTON(1);

        @SuppressWarnings("unused")
        private int	mode;

        ActionOptionsPaneItemsSelectMode(int _mode) {
            mode = _mode;
        }
    }

    public static final String empty_selected_option_id_ = "7f6bc210-8fc0-445f-9930-785b0eb5aaa1";

    private ArrayList<ActionOptionsPaneItem> items_ = new ArrayList<ActionOptionsPaneItem>();
    private String selected_item_id_;
    private ActionOptionsPaneItemsSelectMode select_mode_ = ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON;
    private boolean multiselect = false;
    private ArrayList<String> selected_items_id_ = new ArrayList<>();

    public class ActionOptionsPaneItem {
        public String text_;
        public String id_ = empty_selected_option_id_;
    }

    ActionOptionsPaneItems(ActionOptionsPaneItemsSelectMode select_mode) {
        select_mode_ = select_mode;
        selected_item_id_ = empty_selected_option_id_;
    }

    ActionOptionsPaneItems(ActionOptionsPaneItemsSelectMode select_mode, boolean multiselect) {
        this(select_mode);
        if (multiselect && select_mode != ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX) {
            throw new InvalidParameterException("multiselect requires checkbox mode");
        }
        this.multiselect = multiselect;
    }

    public void set_selected_item_id(String id) {
        selected_item_id_ = id;
    }

    public String get_selected_item_id() {
        return selected_item_id_;
    }

    public ActionOptionsPaneItem get(int position) {
        return items_.get(position);
    }

    public ActionOptionsPaneItem find(String id) {
        ActionOptionsPaneItem item = null;

        for (int i=0, l=items_.size(); i<l; ++i) {
            if (items_.get(i).id_.equals(id)) {
                item = items_.get(i);
                break;
            }
        }

        return item;
    }

    public void add_item(String text, String id) {
        ActionOptionsPaneItem item = new ActionOptionsPaneItem();
        item.text_ = text;
        item.id_ = id;
        items_.add(item);
    }

    public void remove_item(String id) {
        if (id.equals(selected_item_id_)) {
            selected_item_id_ = empty_selected_option_id_;
        }
        if (is_selected(id)) {
            toggle_selection(id);
        }
        for (int i=0, l=items_.size(); i<l; ++i) {
            if (items_.get(i).id_.equals(id)) {
                items_.remove(i);
                break;
            }
        }

    }

    public int size() {
        return items_.size();
    }

    public ActionOptionsPaneItemsSelectMode get_select_mode() {
        return select_mode_;
    }

    public void set_selected_items_id(ArrayList<String> ids) {
        selected_items_id_.clear();
        selected_items_id_.addAll(ids);
    }

    public ArrayList<String> get_selected_items_id() {
        return selected_items_id_;
    }

    public boolean is_selected(String id) {
        return selected_items_id_.contains(id);
    }

    public boolean toggle_selection(String id) {
        if (is_selected(id)) {
            selected_items_id_.remove(id);
            return false;
        }
        selected_items_id_.add(id);
        return true;
    }

    public boolean is_multiselect() {
        return multiselect;
    }
}
