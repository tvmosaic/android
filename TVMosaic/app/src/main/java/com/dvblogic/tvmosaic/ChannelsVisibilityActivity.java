package com.dvblogic.tvmosaic;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;

import TVMosaic.TVMosaic.R;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.dvblogic.dvblink_common.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Locale;

import static android.text.InputType.TYPE_CLASS_TEXT;
import static com.dvblogic.tvmosaic.ActionOptionsPaneItems.empty_selected_option_id_;

public class ChannelsVisibilityActivity extends BaseActivity implements ActionPane.ActionPaneEventListener {

    protected final String any_headend_id = "75c8c6b3-b3d5-458e-b3cc-d580ae6a64ab";
    protected final String any_transponder_id = "98814a47-1757-4e02-b62a-ed896c1b33d8";

    private class ScanChannelSortItem {

        public ScanChannelSortItem(String name, int idx) {
            channelName = name;
            channelIdx = idx;
        }

        public String channelName;
        public int channelIdx;
    }

    public class ChannelsVisibilityAdapter extends BaseAdapter {
		private ScannedChannelsList headends;
		private ArrayList<String> invisibleChannelsID = new ArrayList<>();
		private LayoutInflater ltInflater;
		private ChannelClickListener channelClickListener;
        private String headendID = any_headend_id;
        private String transponderID = any_transponder_id;
        private ArrayList<ScanChannelSortItem> sortedChannels = new ArrayList<>();
        private boolean searchActive = false;

		private class ChannelClickListener implements View.OnClickListener {
			@Override
			public void onClick(View view) {
				String channelID = getSpinnerChannelID(view);
				if (switchSpinner(view)) {
					invisibleChannelsID.remove(channelID);
				} else {
					if (!invisibleChannelsID.contains(channelID)) {
						invisibleChannelsID.add(channelID);
					}
				}
			}
		}

		ChannelsVisibilityAdapter(ScannedChannelsList headends) {
			this.headends = headends;
			ltInflater = getLayoutInflater();
			channelClickListener = new ChannelClickListener();

            prepareSortedView(null);
            searchActive = false;
		}

		@Override
		public int getCount() {
			return getChannelsNumber();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = ltInflater.inflate(R.layout.settings_spinner_layout, parent, false);
			}

			ScannedChannel channel = getChannelFromPosition(position);
			TextView label;
			//title
			label = (TextView) view.findViewById(R.id.title_text);
			String label_text = channel.name;
			String chnum_text = Utils.formatChannelNumber(channel);
			if (!chnum_text.isEmpty())
				label_text += "   (" + chnum_text + ")";
			label.setText(label_text);
			//description
			label = (TextView) view.findViewById(R.id.desc_text);
			Locale locale = Locale.getDefault();
			label.setText(String.format(locale, "%s | %s", getChannelTypeString(channel), getChannelEncryptionString(channel)));

			view.setOnClickListener(channelClickListener);
			view.setTag(R.string.channels_visibility_id, channel.id);

			switchSpinner(view, !invisibleChannelsID.contains(channel.id));
			return view;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return getChannelFromPosition(position);
		}

		private String getChannelTypeString(ScannedChannel ch) {
			String ret_val = "";

			if (ch.type == ChannelType.RD_CHANNEL_TV)
				ret_val = language_manager_.GetString(LocaleStrings.IDS_CHANNEL_TV);
			else if (ch.type == ChannelType.RD_CHANNEL_RADIO)
				ret_val = language_manager_.GetString(LocaleStrings.IDS_CHANNEL_RADIO);
			else if (ch.type == ChannelType.RD_CHANNEL_OTHER)
				ret_val = language_manager_.GetString(LocaleStrings.IDS_CHANNEL_DATA);

			return ret_val;
		}

		private String getChannelEncryptionString(ScannedChannel ch) {
			String ret_val = "";

			if (ch.encrypted)
				ret_val = language_manager_.GetString(LocaleStrings.IDS_CHANNEL_ENCRYPTED);
			else
				ret_val = language_manager_.GetString(LocaleStrings.IDS_CHANNEL_FTA);

			return ret_val;
		}

		private String getSpinnerChannelID(View spinner) {
			return (String) spinner.getTag(R.string.channels_visibility_id);
		}

		private boolean switchSpinner(View spinner, boolean isOn) {
			TextView label = (TextView) spinner.findViewById(R.id.spinner_label);
			label.setText(getOnOffFromBoolean(isOn));
			spinner.setTag(R.string.channels_visibility_value, isOn);
			return isOn;
		}

		private boolean switchSpinner(View spinner) {
			return switchSpinner(spinner, !spinnerIsOn(spinner));
		}

		private boolean spinnerIsOn(View spinner) {
			return (boolean) spinner.getTag(R.string.channels_visibility_value);
		}

		private String getOnOffFromBoolean(boolean isOn) {
			return language_manager_.GetString(isOn ? LocaleStrings.IDS_ON : LocaleStrings.IDS_OFF);
		}

		private ArrayList<String> getInvisibleChannels() {
			return invisibleChannelsID;
		}

		private void setInvisibleChannels(InvisibleChannelsList invisibleChannelsList) {
			invisibleChannelsID.clear();
			for (InvisibleChannel channel: invisibleChannelsList.list()) {
				invisibleChannelsID.add(channel.id);
			}
			this.notifyDataSetChanged();
		}

        protected void toggleAllChannels(boolean visible) {
            int count = getCount();
            for (int i=0; i<count; i++) {
                ScannedChannel ch = (ScannedChannel)getItem(i);
                if (visible) {
                    invisibleChannelsID.remove(ch.id);
                } else {
                    if (!invisibleChannelsID.contains(ch.id))
                        invisibleChannelsID.add(ch.id);
                }
            }
        }

        private void setHeadendAndTransponderID(String headend, String transponder) {
            headendID = headend;
            transponderID = transponder;

            //setting new headend and transponder resets search
            prepareSortedView(null);
            searchActive = false;
        }

        private void enableSearch(String s) {
            //apply search results
            prepareSortedView(s);
            searchActive = true;
        }

        private void resetSearch() {
            if (searchActive) {
                prepareSortedView(null);
                searchActive = false;
            }
        }

        private boolean isSearchActive() {
            return searchActive;
        }

        private void prepareSortedView(String searchString) {
            sortedChannels.clear();

            int chnum = int_getChannelsNumber();
            for (int i=0; i<chnum; i++) {
                ScannedChannel sch = int_getChannelFromPosition(i);
                if (searchString == null || sch.name.toLowerCase().contains(searchString.toLowerCase()))
                    sortedChannels.add(new ScanChannelSortItem(sch.name, i));
            }

            Collections.sort(sortedChannels, new Comparator<ScanChannelSortItem>() {
                public int compare(ScanChannelSortItem sch1, ScanChannelSortItem sch2) {
                    return sch1.channelName.compareToIgnoreCase(sch2.channelName);
                }
            });
        }


        private ScannedChannel getChannelFromPosition(int position) {
            return int_getChannelFromPosition(sortedChannels.get(position).channelIdx);
        }

        private int getChannelsNumber() {
            return sortedChannels.size();
        }

        private ScannedChannel int_getChannelFromPosition(int position) {
            int count = 0;

            for (int i = 0; i < headends.size(); i++) {
                Headend hd = headends.get(i);

                if (headendID.equalsIgnoreCase(any_headend_id) || hd.id.equalsIgnoreCase(headendID)) {
                    for (int j = 0; j < hd.transponders.size(); j++) {
                        Transponder tr = hd.transponders.get(j);

                        if (transponderID.equalsIgnoreCase(any_transponder_id) || tr.id.equalsIgnoreCase(transponderID)) {
                            if (position >= count && position < count + tr.channels.size()) {
                                for (int k = 0; k < tr.channels.size(); k++) {
                                    if (count == position) {
                                        return tr.channels.get(k);
                                    } else {
                                        count += 1;
                                    }
                                }
                            } else {
                                count += tr.channels.size();
                            }
                        }
                    }
                }
            }

            return null;
        }

        private int int_getChannelsNumber() {

            int count = 0;

            for (int i = 0; i < headends.size(); i++) {
                Headend hd = headends.get(i);

                if (headendID.equalsIgnoreCase(any_headend_id) || hd.id.equalsIgnoreCase(headendID)) {
                    for (int j = 0; j < hd.transponders.size(); j++) {
                        Transponder tr = hd.transponders.get(j);

                        if (transponderID.equalsIgnoreCase(any_transponder_id) || tr.id.equalsIgnoreCase(transponderID)) {
                            count += tr.channels.size();
                        }
                    }
                }
            }
            return count;
        }

	}

    private class MyActionPane extends ActionPane {

        public MyActionPane(BaseActivity context, ActionPaneEventListener listener) {
            super(context, listener);
        }

        protected void fill_standard_pane_items() {

            ActionPaneItem exit_and_save_api = new ActionPaneItem();
            exit_and_save_api.text_ = language_manager_.GetString(LocaleStrings.IDS_SAVE);
            exit_and_save_api.id_ = exit_and_save_pane_action_id_;
			exit_and_save_api.left_icon_ = R.drawable.tvm_save_icon_normal;
			exit_and_save_api.left_icon_selected_ = R.drawable.tvm_save_icon_hover;
            items_.add(exit_and_save_api);

            ActionPaneItem exit_api = new ActionPaneItem();
            exit_api.text_ = language_manager_.GetString(LocaleStrings.IDS_DO_NOT_SAVE);
            exit_api.id_ = exit_pane_action_id_;
            exit_api.left_icon_ = R.drawable.tvm_cancel_icon_normal;
            exit_api.left_icon_selected_ = R.drawable.tvm_cancel_icon_hover;
            items_.add(exit_api);
        }

        protected void add_extra_pane_items() {
            for (int i=0; i<headends_.size(); i++) {
                Headend hd = headends_.get(i);

                ActionPaneItem api = new ActionPaneItem();
                api.id_ = i;
                api.text_ = hd.name;
                api.left_icon_ = R.drawable.tvm_sort_icon_normal;
                api.left_icon_selected_ = R.drawable.tvm_sort_icon_hover;
                api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
                api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
                api.options_ = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX);
                items_.add(0, api);

                ActionOptionsPaneItems transponders = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX);
                transponders.add_item(language_manager_.GetString(LocaleStrings.IDS_VISIBILITY_ALL_CHANNELS), any_transponder_id);

                    for (int j = 0; j < hd.transponders.size(); j++) {
                        Transponder tr = hd.transponders.get(j);
                        transponders.add_item(tr.name, tr.id);
                    }

                transponders.set_selected_item_id(empty_selected_option_id_);
                this.set_options(i, transponders);
            }

            ActionPaneItem reset_search_api = new ActionPaneItem();
            reset_search_api.text_ = LocaleStrings.IDS_RESET_SEARCH;
            reset_search_api.id_ = visibility_reset_search_action_id_;
            reset_search_api.left_icon_ = R.drawable.tvm_reset_icon_normal;
            reset_search_api.left_icon_selected_ = R.drawable.tvm_reset_icon_hover;
            items_.add(0, reset_search_api);

            ActionPaneItem search_api = new ActionPaneItem();
            search_api.text_ = LocaleStrings.IDS_SEARCH_PROGRAMS_PAGE_TITLE;
            search_api.id_ = visibility_search_action_id_;
            search_api.left_icon_ = R.drawable.tvm_search_icon_normal;
            search_api.left_icon_selected_ = R.drawable.tvm_search_icon_hover;
            items_.add(0, search_api);

            ActionPaneItem check_all_api = new ActionPaneItem();
            check_all_api.text_ = language_manager_.GetString(LocaleStrings.IDS_VISIBILITY_UNCHECK_ALL);
            check_all_api.id_ = visibility_uncheck_all_pane_action_id_;
            check_all_api.left_icon_ = R.drawable.tvm_unchecked_icon_normal;
            check_all_api.left_icon_selected_ = R.drawable.tvm_unchecked_icon_hover;
            items_.add(0, check_all_api);

            ActionPaneItem uncheck_all_api = new ActionPaneItem();
            uncheck_all_api.text_ = language_manager_.GetString(LocaleStrings.IDS_VISIBILITY_CHECK_ALL);
            uncheck_all_api.id_ = visibility_check_all_pane_action_id_;
            uncheck_all_api.left_icon_ = R.drawable.tvm_checked_icon_normal;
            uncheck_all_api.left_icon_selected_ = R.drawable.tvm_checked_icon_hover;
            items_.add(0, uncheck_all_api);

        }
    }

	private enum CVActivityState {
		STATE_NONE,
		STATE_REQUESTING_ALL_CHANNELS,
		STATE_REQUESTING_GET_CHANNELS_VISIBILITY,
		STATE_REQUESTING_SET_CHANNELS_VISIBILITY,
		STATE_READY
	}

	private static final String TAG = "ChannelsVisibility";
	private CVActivityState state_ = CVActivityState.STATE_NONE;
	private ListView channelList;
    protected ScannedChannelsList headends_;
	private ChannelsVisibilityAdapter channelsAdapter;
	private boolean panic = false;
    private MyActionPane _options_menu = null;
    private String searchPhrase;
    private static int minimumSearchLength = 3;

	public ChannelsVisibilityActivity() {
		show_exit_dialog = false;
	}

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.channels_visibility_activity);

        TextView tv;
		if (Utils.usesRemoteControl(this)) {
            tv = (TextView)findViewById(R.id.head_bar);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_PAGE_TITLE));

            RelativeLayout rl = (RelativeLayout)findViewById(R.id.channels_visibility_category);
            tv = (TextView)rl.findViewById(R.id.category_text);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_CHANNEL_VISIBILITY));
            tv = (TextView)rl.findViewById(R.id.category_desc);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_CHANNEL_VISIBILITY_DESC));

        } else {
            //if it is a mobile phone, hide category and show visibility text at the top
            tv = (TextView)findViewById(R.id.head_bar);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_CHANNEL_VISIBILITY));

            RelativeLayout rl = (RelativeLayout)findViewById(R.id.channels_visibility_category);
            rl.setVisibility(View.GONE);
        }

		View activity = findViewById(R.id.channels_visibility_activity);
		channelList = (ListView) activity.findViewById(R.id.channel_list);

		state_ = CVActivityState.STATE_READY;

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                requestAllChannels();
            }
        });

	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() != KeyEvent.ACTION_DOWN) {
			return true;
		}

        if (_options_menu != null) {

            if (keyCode == KeyEvent.KEYCODE_MENU) {
                _options_menu.show();
                return true;
            }
            if (Common.backPressed(keyCode, event)) {
                _options_menu.show();
                return true;
            }

        }
		return false;
	}

	private void fillChannelList(ScannedChannelsList headends) {

        headends_ = headends;
        _options_menu = new MyActionPane(this, this);

        //set reset search invisible by default
        _options_menu.setItemVisibility(ActionPane.visibility_reset_search_action_id_, false);

        channelsAdapter = new ChannelsVisibilityAdapter(headends);
		channelList.setAdapter(channelsAdapter);
		channelList.setItemsCanFocus(true);
	}

	private SetChannelsVisibilityRequest makeSetChannelsVisibilityRequest(ArrayList<String> invisibleChannelsID) {
		SetChannelsVisibilityRequest invisibleChannels = new SetChannelsVisibilityRequest();
		InvisibleChannel invisibleChannel;
		for (String invisibleChannelID: invisibleChannelsID) {
			invisibleChannel = new InvisibleChannel();
			invisibleChannel.id = invisibleChannelID;
			invisibleChannels.add(invisibleChannel);
		}
		return invisibleChannels;
	}

    public void onActionPaneAction(int action_id) {
        if (action_id == ActionPane.exit_pane_action_id_) {
            finish();
        } else
        if (action_id == ActionPane.exit_and_save_pane_action_id_) {
            requestSetChannelsVisibility();
        } else
        if (action_id == ActionPane.visibility_check_all_pane_action_id_) {
            _options_menu.hide();
            activateProgressBar(true);

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    channelsAdapter.toggleAllChannels(true);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            channelsAdapter.notifyDataSetChanged();
                            activateProgressBar(false);
                        }
                    });
                }
            });
        } else
        if (action_id == ActionPane.visibility_uncheck_all_pane_action_id_) {

            _options_menu.hide();
            activateProgressBar(true);

            AsyncTask.execute(new Runnable() {
                @Override
                public void run() {
                    channelsAdapter.toggleAllChannels(false);

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            channelsAdapter.notifyDataSetChanged();
                            activateProgressBar(false);
                        }
                    });
                }
            });
        } else
        if (action_id == ActionPane.visibility_search_action_id_) {
            TextInputDialog tid = new TextInputDialog(_context,
                    "",
                    language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_OK),
                    language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL));

            tid.setOnResultListener(new TextInputDialog.OnResultListener() {
                @Override
                public void onResult(boolean result, String text) {
                    if (result) {
                        if (text.length() >= minimumSearchLength) {
                            searchPhrase = text;
                            _options_menu.setItemVisibility(ActionPane.visibility_reset_search_action_id_, true);

                            channelsAdapter.enableSearch(searchPhrase);
                            channelsAdapter.notifyDataSetChanged();
                        } else {
                            Toast.makeText(_context, String.format(language_manager_.GetString(LocaleStrings.IDS_SEARCH_KEYWORDS_ERROR_MSG), minimumSearchLength), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
            });

            tid.showDialog(TYPE_CLASS_TEXT, searchPhrase);
        } else
        if (action_id == ActionPane.visibility_reset_search_action_id_) {
            _options_menu.hide();

            searchPhrase = "";
            _options_menu.setItemVisibility(ActionPane.visibility_reset_search_action_id_, false);

            channelsAdapter.resetSearch();
            channelsAdapter.notifyDataSetInvalidated();
        }
    }

    public void onActionPaneOption(int option_id, String selected_option) {

        if (selected_option.equals(empty_selected_option_id_)) {
            channelsAdapter.setHeadendAndTransponderID(any_headend_id, any_transponder_id);
        } else {
            channelsAdapter.setHeadendAndTransponderID(headends_.get(option_id).id, selected_option);

            //make sure that all other transponders on other headends are not selected
            for (int i=0; i<headends_.size(); i++) {
                if (option_id != i) {
                    //reset selection
                    ActionOptionsPaneItems options = _options_menu.get_options(i);
                    if (options == null) {
                        continue;
                    }
                    options.set_selected_item_id(empty_selected_option_id_);
                }
            }
        }
        //setHeadendAndTransponderID resets search (if any)
        searchPhrase = "";
        _options_menu.setItemVisibility(ActionPane.visibility_reset_search_action_id_, false);

        _options_menu.hide();
        channelsAdapter.notifyDataSetInvalidated();
    }

    // region Server communication
	private void requestAllChannels() {
		Log.i(TAG, "requestAllChannels");
		if (data_provider_ != null && state_ == CVActivityState.STATE_READY)  {
			activateProgressBar(true);
			state_ = CVActivityState.STATE_REQUESTING_ALL_CHANNELS;
			data_provider_.GetScannedChannels(new ScannedChannelsRequest());
		}
	}

	private void processAllChannels() {
		Log.i(TAG, "processAllChannels");
		try {
			state_ = CVActivityState.STATE_READY;
			if (response_ != null && response_ instanceof ScannedChannelsList) {
				ScannedChannelsList channels = (ScannedChannelsList) response_;

				fillChannelList(channels);
				requestChannelsVisibility();
			}
		} catch (Exception e) {
		}
	}

	private void requestChannelsVisibility() {
		Log.i(TAG, "requestChannelsVisibility");
		if (data_provider_ != null && state_ == CVActivityState.STATE_READY)  {
			state_ = CVActivityState.STATE_REQUESTING_GET_CHANNELS_VISIBILITY;
			data_provider_.GetChannelsVisibility(new GetChannelsVisibilityRequest());
		}
	}

	private void processChannelsVisibility() {
		Log.i(TAG, "processChannelsVisibility");
		try {
			activateProgressBar(false);
			state_ = CVActivityState.STATE_READY;
			if (response_ != null && response_ instanceof InvisibleChannelsList) {
				channelsAdapter.setInvisibleChannels((InvisibleChannelsList) response_);
			}
		} catch (Exception e) {
		}
	}

	private boolean requestSetChannelsVisibility() {
		Log.i(TAG, "requestSetChannelsVisibility");
		if (channelsAdapter.getCount() != 0) {
			//check that not all channels are hidden
			if (channelsAdapter.getInvisibleChannels().size() == channelsAdapter.getCount()) {
				Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_VISIBILITY_ALL_INVISIBLE), Toast.LENGTH_SHORT).show();
				return false;
			}

			if (data_provider_ != null && state_ == CVActivityState.STATE_READY) {
				activateProgressBar(true);
				state_ = CVActivityState.STATE_REQUESTING_SET_CHANNELS_VISIBILITY;
				data_provider_.SetChannelsVisibility(
						makeSetChannelsVisibilityRequest(
								channelsAdapter.getInvisibleChannels()
						)
				);
			}
		} else {
            finish();
        }
		return true;
	}

	private void processSetChannelsVisibility() {
		Log.i(TAG, "processSetChannelsVisibility");
		try {
			activateProgressBar(false);
			state_ = CVActivityState.STATE_READY;
			if (response_ != null && response_ instanceof Boolean && (Boolean)response_) {
				onBackPressed();
			}
		} catch (Exception e) {
		}
	}

	public void DataProcessing(String command, Object result_data) {
		response_ = result_data;
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (state_ == CVActivityState.STATE_REQUESTING_ALL_CHANNELS) {
					processAllChannels();
				} else if (state_ == CVActivityState.STATE_REQUESTING_GET_CHANNELS_VISIBILITY) {
					processChannelsVisibility();
				} else if (state_ == CVActivityState.STATE_REQUESTING_SET_CHANNELS_VISIBILITY) {
					processSetChannelsVisibility();
				}
			}
		});
	}

	public void ErrorProcessing(String command, StatusCode error) {
		panic = true;
		http_error_ = error;
		Log.e(TAG, "server error: " + command + " [" + error.toString() + "]");
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ProcessDataProviderError();
			}
		});
	}

	public void ProcessDataProviderError() {
        boolean bexit = (state_ == CVActivityState.STATE_REQUESTING_ALL_CHANNELS);

		state_ = CVActivityState.STATE_READY;
		activateProgressBar(false);
		if (language_manager_ != null) {
			String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
			Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
		}

        if (bexit)
            finish();
	}
	// endregion Server communication
}
