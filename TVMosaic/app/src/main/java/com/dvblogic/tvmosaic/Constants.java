package com.dvblogic.tvmosaic;

import com.dvblogic.dvblink_common.Program;
import com.dvblogic.dvblink_common.VideoInfo;

import TVMosaic.TVMosaic.R;

public class Constants
{
	public static String	LanguageDirectory	= "Language";
	public static String	SettingsFileName	= "TVMosaic";

	public static String	TVFavoritesGuid		= "c02ecf21-3b0c-4774-9bd8-6897592a902a";


	public static boolean HasGenreColor(Program prg) {
		if (prg.IsMovie)
			return true;
		else if (prg.IsSports)
			return true;
		else if (prg.IsNews)
			return true;
		else if (prg.IsKids)
			return true;
		else if (prg.IsSpecial)
			return true;

		return false;
	}

	public static int GetColorFromGenre(Program prg) {
		if (prg.IsMovie)
			return R.color.genre_movie;
		else if (prg.IsSports)
			return R.color.genre_sports;
		else if (prg.IsNews)
			return R.color.genre_news;
		else if (prg.IsKids)
			return R.color.genre_kids;
		else if (prg.IsSpecial)
			return R.color.genre_special;

		return R.color.common_bkg;
	}
	public static boolean HasGenreColor(VideoInfo prg) {
		if (prg.IsMovie)
			return true;
		else if (prg.IsSports)
			return true;
		else if (prg.IsNews)
			return true;
		else if (prg.IsKids)
			return true;
		else if (prg.IsSpecial)
			return true;

		return false;
	}

	public static int GetColorFromGenre(VideoInfo prg) {
		if (prg.IsMovie)
			return R.color.genre_movie;
		else if (prg.IsSports)
			return R.color.genre_sports;
		else if (prg.IsNews)
			return R.color.genre_news;
		else if (prg.IsKids)
			return R.color.genre_kids;
		else if (prg.IsSpecial)
			return R.color.genre_special;

		return R.color.common_bkg;
	}
}
