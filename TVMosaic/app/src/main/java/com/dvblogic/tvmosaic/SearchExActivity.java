package com.dvblogic.tvmosaic;

import android.content.Intent;
import android.os.Bundle;
import TVMosaic.TVMosaic.R;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.dvblogic.dvblink_common.*;
import java.util.*;


public class SearchExActivity extends BaseActivity implements OnBitmapLoadFinished {

	private static final String TAG = "SearchEx";
	private static final String ONLY_IN_NAME_KEYWORD_MODIFIER = "#%s";
	private static final String EXACT_MATCH_KEYWORD_MODIFIER = "\"%s\"";
	private static final int PLAY_ACTIVITY_REQUEST_CODE = 1;
	public static final int REQUESTED_COUNT = 50;

	private View root;
	private EditText keywords;
	private CheckBox onlyInName;
	private CheckBox exactMatch;
	private ListView searchResults;
	private ToggleButton genresButton;
	private ToggleButton channelsButton;
	private DropdownMenu genresMenu;
	private DropdownMenu channelsMenu;

	private SearchActivityState state_ = SearchActivityState.STATE_NONE;
	private HashMap<String, Channel> channelByIDMap = new HashMap<>();
	private ProgramDetails programDetailsPane;

	private enum SearchActivityState {
		STATE_NONE,
		STATE_REQUESTING_CHANNELS_INFO,
		STATE_REQUESTING_SEARCH_RESULTS,
        STATE_REQUESTING_ADD_SCHEDULE,
		STATE_READY
	}

	private class SearchResultsAdapter extends BaseAdapter {
		private ArrayList<Pair<Program, Channel>> searchResults = new ArrayList<>();
		private LayoutInflater ltInflater;
		private int positionLastShowedProgramInfo = -1;

		private View.OnClickListener resultItemClickListener = new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showProgramInfo((int) view.getTag(), true);
			}
		};
		private View.OnFocusChangeListener resultItemFocusListener = new View.OnFocusChangeListener() {
			@Override
			public void onFocusChange(View view, boolean focus) {
				if (!focus) {
					return;
				}
				showProgramInfo((int) view.getTag(), false);
			}
		};
		private View.OnKeyListener resultItemKeyListener = new View.OnKeyListener() {
			@Override
			public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
				if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && Common.okPressed(keyEvent.getKeyCode())) {
					showProgramInfo((int) view.getTag(), true);
					return true;
				}
				return false;
			}
		};

		private void showProgramInfo(int position, boolean extend) {
			if (position == positionLastShowedProgramInfo && !extend) {
				return;
			}
			positionLastShowedProgramInfo = position;

			Pair<Program, Channel> programChannel = searchResults.get(position);
			Program program = programChannel.first;

            if (programDetailsPane.getVisibility() == View.VISIBLE)
			    programDetailsPane.setInfo(program, programChannel.second.Name, 0, false, false, language_manager_);

			if (extend) {
				startProgramActivity(program, programChannel.second);
			} else {
				setProgramImage(program);
			}
		}

		void startProgramActivity(Program program, Channel channel) {
			Intent i = new Intent(SearchExActivity.this, ProgramActivity.class);
			i.putExtra("channel_id", channel.ID);
			i.putExtra("long_channel_id", channel.DVBLinkID);
			i.putExtra("channel_name", channel.Name);
			i.putExtra("program_id", program.ID);
			i.putExtra("social_supported", false);
			i.putExtra("enable_create_profile", enableCreateProfile);
			i.putExtra("enable_like_dislike", false);
			startActivityForResult(i, PLAY_ACTIVITY_REQUEST_CODE);
		}

		public SearchResultsAdapter() {
			ltInflater = getLayoutInflater();
		}

		@Override
		public int getCount() {
			return searchResults.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = ltInflater.inflate(R.layout.search_ex_item, parent, false);
			}
			Pair<Program, Channel> programChannel = searchResults.get(position);

			TextView label;
			label = (TextView) view.findViewById(R.id.title);
			label.setText(programChannel.first.Title);

            if (!Utils.usesRemoteControl(_context)) {
                String str = ((programChannel.first.Subname == null) ? "" : programChannel.first.Subname);
                String ES = "";
                if (programChannel.first.SeasonNum != 0)
                    ES += String.format("S%02d", programChannel.first.SeasonNum);

                if (programChannel.first.EpisodeNum != 0) {
                    ES += String.format("E%02d", programChannel.first.EpisodeNum);
                }
                if (str.isEmpty()) {
                    str += ES;
                } else {
                    if (!ES.isEmpty())
                        str += "  |  " + ES;
                }

                label = (TextView) view.findViewById(R.id.subtitle);
                label.setText(str);
                if (!str.isEmpty())
                    label.setVisibility(View.VISIBLE);
                else
                    label.setVisibility(View.GONE);
            }

			label = (TextView) view.findViewById(R.id.start_time);
			label.setText(formatStartTime(programChannel.first.StartTime));

			label = (TextView) view.findViewById(R.id.start_date);
			label.setText(formatStartDate(
					programChannel.first.StartTime, programChannel.first.StartTime + programChannel.first.Duration
			));

			label = (TextView) view.findViewById(R.id.channel);
			label.setText(programChannel.second.Name);

			label = (TextView) view.findViewById(R.id.duration);
			label.setText(formatDuration(programChannel.first.Duration));

			view.setTag(position);
			view.setOnFocusChangeListener(resultItemFocusListener);
			view.setOnClickListener(resultItemClickListener);
			view.setOnKeyListener(resultItemKeyListener);

			return view;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return searchResults.get(position);
		}

		public void addProgram(Program program, Channel channel) {
			searchResults.add(new Pair<>(program, channel));
		}

		public void sort() {
			Collections.sort(searchResults, new Comparator<Pair<Program, Channel>>() {
				@Override
				public int compare(Pair<Program, Channel> o1, Pair<Program, Channel> o2) {
					return (int) (o1.first.StartTime - o2.first.StartTime);
				}
			});
		}

		public void limit(int length) {
			searchResults = new  ArrayList<>(searchResults.subList(0, Math.min(length, searchResults.size())));
		}
	}

	@Override
	public void loadFinished() {}

	private boolean enableCreateProfile = false;
	private ChannelIDList channels_to_update_ = new ChannelIDList();

	@Override
	protected void onCreate(Bundle bundle) {
	super.onCreate(bundle);
		this.show_exit_dialog = false;
		setContentView(R.layout.search_ex_activity);

        final int number_of_visible_rows = 4;
        final int row_height  = (int)this.getResources().getDimension(R.dimen.search_item_height);
        final int divider_width = (int)this.getResources().getDimension(R.dimen.search_divider_width);

		final int layout_height = Utils.usesRemoteControl(this) ? (row_height * number_of_visible_rows + divider_width * number_of_visible_rows) : ViewGroup.LayoutParams.MATCH_PARENT;
        final View lv = findViewById(R.id.search_results);

		if (!Utils.usesRemoteControl(this)) {
			RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) lv.getLayoutParams();
			params.addRule(RelativeLayout.ABOVE, R.id.status_line);
			lv.setLayoutParams(params);
		}

        lv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lv.getLayoutParams().height = layout_height;
                lv.requestLayout();
                lv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

		enableCreateProfile = this.getIntent().getExtras().getBoolean("enable_create_profile");

		root = findViewById(R.id.search_ex_layout);
		onlyInName = (CheckBox) root.findViewById(R.id.only_in_name);
		onlyInName.setText(language_manager_.GetString(LocaleStrings.IDS_SEARCH_ONLY_IN_NAME));
		exactMatch = (CheckBox) root.findViewById(R.id.exact_match);
		exactMatch.setText(language_manager_.GetString(LocaleStrings.IDS_SEARCH_EXACT_MATCH));
		keywords = (EditText) root.findViewById(R.id.keywords);
		keywords.setHint(language_manager_.GetString(LocaleStrings.IDS_SEARCH_KEYWORDS_HINT));

		keywords.setOnEditorActionListener(new TextView.OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {
				if (actionId == EditorInfo.IME_ACTION_NEXT || actionId == EditorInfo.IME_ACTION_PREVIOUS) {
                    Button btn = (Button)findViewById(R.id.button_search);
                    btn.requestFocus();
					return true;
				}
				return false;
			}
		});

        Button btn_search = (Button)findViewById(R.id.button_search);
        btn_search.setText(language_manager_.GetString(LocaleStrings.IDS_SEARCH_BUTTON));
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    search();
            }
        });

		Boolean can_record = settings_.getValue(SettingsManager.CAN_RECORD_KEY, false);

        Button btn_add = (Button)findViewById(R.id.button_add_schedule);
        if (can_record) {
            btn_add.setText(language_manager_.GetString(LocaleStrings.IDS_ADD_SCHEDULE_BUTTON));
            btn_add.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addSchedule();
                }
            });
        } else {
            btn_add.setVisibility(View.GONE);
            LinearLayout.LayoutParams p = (LinearLayout.LayoutParams)keywords.getLayoutParams();
            p.weight += 1;
            keywords.setLayoutParams(p);
        }

		searchResults = (ListView) root.findViewById(R.id.search_results);
		programDetailsPane = (ProgramDetails) root.findViewById(R.id.program_details_pane);
		if (!Utils.usesRemoteControl(this))
			programDetailsPane.setVisibility(View.GONE);

		genresButton = (ToggleButton) root.findViewById(R.id.genres_button);
		genresButton.setText(language_manager_.GetString(LocaleStrings.IDS_SEARCH_GENRES));
		genresButton.setTextOn(language_manager_.GetString(LocaleStrings.IDS_SEARCH_GENRES));
		genresButton.setTextOff(language_manager_.GetString(LocaleStrings.IDS_SEARCH_GENRES));
		channelsButton = (ToggleButton) root.findViewById(R.id.channels_button);
		channelsButton.setText(language_manager_.GetString(LocaleStrings.IDS_SEARCH_CHANNELS));
		channelsButton.setTextOn(language_manager_.GetString(LocaleStrings.IDS_SEARCH_CHANNELS));
		channelsButton.setTextOff(language_manager_.GetString(LocaleStrings.IDS_SEARCH_CHANNELS));

		Genres.init(language_manager_);
		ArrayList<Pair<String, String>> items = new ArrayList<>();
		String[] genreNameList = Genres.keywords();
		for (String genreName: genreNameList) {
			items.add(new Pair<>(genreName, Long.toString(Genres.fromString(genreName))));
		}
		genresMenu = new DropdownMenu(this, genresButton, items);

		state_ = SearchActivityState.STATE_READY;
		requestChannels();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (data != null) {
			String channels_xml = data.getStringExtra("need_channel_update");
			if (channels_xml != null) {
				ChannelIDList channels = (ChannelIDList) Serializer.deserialize(
						channels_xml,
						ChannelIDList.class
				);
				for (int i=0; i<channels.size(); i++) {
					if (!channels_to_update_.list().contains(channels.get(i)))
						channels_to_update_.add(channels.get(i));
				}
			}
		}

		if (requestCode == PLAY_ACTIVITY_REQUEST_CODE) {

			if (data == null) {
				return;
			}

			if (resultCode == ProgramActivity.return_play) {
				finishWithResult(resultCode, data);
			} else if (resultCode == ProgramActivity.RETURN_ADVISER_CREATE_PROFILE ||
					resultCode == ProgramActivity.RETURN_ADVISER_LIKE_THIS) {
				finishWithResult(resultCode, data);
			}
		}
	}

	protected void finishWithResult(int resultCode, Intent data) {
		if (channels_to_update_.size() > 0) {
			if (data == null)
				data = new Intent();

            data.removeExtra("need_channel_update");

			String channels_xml = Serializer.serialize(channels_to_update_);
			data.putExtra("need_channel_update", channels_xml);
		}
		setResult(resultCode, data);
		finish();
	}

    protected long getSearchGenres() {
        long searchGenresFlag = 0;
        ArrayList<String> searchGenres = genresMenu.getCheckedID();
        for (String checkedGenre: searchGenres) {
            searchGenresFlag |= Long.valueOf(checkedGenre);
        }
        return searchGenresFlag;
    }

    protected String getSearchKeywords() {
        String keywords = this.keywords.getText().toString().trim();

		int minimumSearchLength = 3;
        if (keywords.length() > 0 && keywords.length() < minimumSearchLength) {
            Toast.makeText(
                    this, String.format(language_manager_.GetString(LocaleStrings.IDS_SEARCH_KEYWORDS_ERROR_MSG), minimumSearchLength), Toast.LENGTH_SHORT
            ).show();

            return null;
        }

		if (keywords.length() > 0) {
			keywords = exactMatch.isChecked() ? String.format(EXACT_MATCH_KEYWORD_MODIFIER, keywords) : keywords;
	        keywords = onlyInName.isChecked() ? String.format(ONLY_IN_NAME_KEYWORD_MODIFIER, keywords) : keywords;
		}

        return keywords;
    }

    protected ChannelIDList getSearchChannels() {
        ArrayList<String> channelsID = channelsMenu.getCheckedID();
        ChannelIDList searchChannels = new ChannelIDList();
        for (String channelID: channelsID) {
            searchChannels.add(channelID);
        }
        return searchChannels;
    }

	private void search() {
		Log.i(TAG, "Search: " + keywords + " " + onlyInName.isChecked() + " " + exactMatch.isChecked());

        long searchGenresFlag = getSearchGenres();

        String keywords = getSearchKeywords();
        if (keywords == null)
            return;

        ChannelIDList searchChannels = getSearchChannels();

        if (keywords.length() == 0 && searchGenresFlag == 0) {
            Toast.makeText(
                    this, language_manager_.GetString(LocaleStrings.IDS_SEARCH_KEYWORDS_HINT), Toast.LENGTH_SHORT
            ).show();
            return;
        }

		requestSearch(keywords, searchChannels, searchGenresFlag);
	}

    protected void addSchedule() {
        long searchGenresFlag = getSearchGenres();

        String keywords = getSearchKeywords();
        if (keywords == null)
            return;

        ChannelIDList searchChannels = getSearchChannels();

        if (keywords.length() == 0 && searchGenresFlag == 0) {
            Toast.makeText(
                    this, language_manager_.GetString(LocaleStrings.IDS_SEARCH_KEYWORDS_HINT), Toast.LENGTH_SHORT
            ).show();
            return;
        }

        if (data_provider_ == null) {
            return;
        }
        activateProgressBar(true);

        String channel_id = ByPatternSchedule.channel_id_any;
        if (searchChannels.size() > 0)
            channel_id = searchChannels.get(0); //just take a first channel

        ByPatternSchedule bp_sch = new ByPatternSchedule(channel_id, keywords, searchGenresFlag, ByPatternSchedule.keep_all_recordings);
        Schedule sch = new Schedule(bp_sch);
        state_ = SearchActivityState.STATE_REQUESTING_ADD_SCHEDULE;
        data_provider_.AddSchedule(sch);
    }

	private void clearResults() {
		searchResults.setAdapter(null);
		programDetailsPane.clear();
		setProgramImage(null);
	}

	private void setProgramImage(Program program) {
        if (programDetailsPane.getVisibility() == View.VISIBLE) {
            programDetailsPane.findViewById(R.id.description_image).setVisibility(View.INVISIBLE);
            if (program == null) {
                return;
            }
            boolean loadBitmap = (program.Image != null && !program.Image.isEmpty());
            if (loadBitmap) {
                new com.dvblogic.tvmosaic.BitmapLoader(
                        this, this, program.Image,
                        (ImageView) programDetailsPane.findViewById(R.id.description_image), null
                ).start();
            }
        }
	}

	private void showSearchResult(ChannelIdWithProgramsList epgData) {
		SearchResultsAdapter searchResultsAdapter = new SearchResultsAdapter();
		for (ChannelIdWithPrograms resultItem: epgData.list()) {
			Channel channel = channelByIDMap.get(resultItem.ChannelId);
			if (channel == null) {
				continue;
			}
			for (Program program: resultItem.Programs.list()) {
				searchResultsAdapter.addProgram(program, channel);
			}
		}
		searchResultsAdapter.sort();
		searchResultsAdapter.limit(REQUESTED_COUNT);
		searchResults.setAdapter(searchResultsAdapter);
		searchResults.setItemsCanFocus(true);
	}

	private String formatStartDate(long startTime, long endTime) {
		long now = Utils.CurrentUtc();
		if (now >= startTime && now < endTime) {
			return language_manager_.GetString(LocaleStrings.IDS_NOW);
		}
		return (Utils.isToday(startTime) ?
				language_manager_.GetString(LocaleStrings.IDS_TODAY) :
				Utils.UtcToMediumDateString(_context, startTime));
	}

	private String formatStartTime(long startTime) {
		return Utils.UtcToShortTimeString(_context, startTime);
	}

	private String formatDuration(long duration) {
		return String.format(Locale.getDefault(), "%d %s",
				duration / 60, language_manager_.GetString(LocaleStrings.IDS_MINUTE)
		);
	}

	// region Server communication
	public void DataProcessing(String command, Object result_data) {
		response_ = result_data;
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (state_ == SearchActivityState.STATE_REQUESTING_SEARCH_RESULTS) {
					processSearchResponse();
				} else if (state_ == SearchActivityState.STATE_REQUESTING_CHANNELS_INFO) {
					processChannelsResponse();
                } else if (state_ == SearchActivityState.STATE_REQUESTING_ADD_SCHEDULE) {
                    processAddScheduleResponse();
                }


			}
		});
	}

	public void ErrorProcessing(String command, StatusCode error) {
		http_error_ = error;
		this.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				ProcessDataProviderError();
			}
		});
	}

	public void ProcessDataProviderError() {
		state_ = SearchActivityState.STATE_READY;
		activateProgressBar(false);
		if (language_manager_ != null) {
			String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
			Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
		}
	}

	private void requestSearch(String keywords, ChannelIDList channels, long genres) {
		Log.i(TAG, "requestSearch: " + keywords);
		if (data_provider_ == null) {
			return;
		}
		activateProgressBar(true);
		clearResults();
		state_ = SearchActivityState.STATE_REQUESTING_SEARCH_RESULTS;
		EpgSearcher epgSearcher = new EpgSearcher(channels, keywords, genres);
		epgSearcher.RequestedCount = REQUESTED_COUNT;
		data_provider_.SearchEpg(epgSearcher);
	}

	private void processSearchResponse() {
		state_ = SearchActivityState.STATE_READY;
		activateProgressBar(false);

		try {
			if (response_ != null) {
				ChannelIdWithProgramsList epgData = (ChannelIdWithProgramsList) response_;
				if (epgData.size() > 0) {
					showSearchResult(epgData);
					searchResults.requestFocus();
				} else {
					Toast.makeText(
							this, language_manager_.GetString(LocaleStrings.IDS_PROGRAM_NOT_FOUND_MSG),
							Toast.LENGTH_SHORT
					).show();
					keywords.requestFocus();
				}
			}
		} catch (Exception e) {
			Log.e(TAG, "processSearchResponse", e);
		}
	}

    private void processAddScheduleResponse() {
        state_ = SearchActivityState.STATE_READY;
        activateProgressBar(false);
        Toast.makeText(
                this, language_manager_.GetString(LocaleStrings.IDS_SEARCH_SCHEDULE_ADDED),
                Toast.LENGTH_SHORT
        ).show();
    }

	private void requestChannels() {
		if (data_provider_ == null) {
			return;
		}

		activateProgressBar(true);
		state_ = SearchActivityState.STATE_REQUESTING_CHANNELS_INFO;
		data_provider_.GetChannels(new ChannelsRequest());
	}

	private void processChannelsResponse() {
		try {
			activateProgressBar(false);
			if (response_ != null) {
				ChannelsList channels = (ChannelsList) response_;
				for (Channel channel: channels.list()) {
					channelByIDMap.put(channel.ID, channel);
				}

				ArrayList<Pair<String, String>> items = new ArrayList<>();
				for (Map.Entry<String, Channel> entry: channelByIDMap.entrySet()) {
					Channel channel = entry.getValue();
					items.add(new Pair<>(channel.Name, channel.ID));
				}
				channelsMenu = new DropdownMenu(this, channelsButton, items);
			}
		} catch (Exception e) {
			Log.e(TAG, "processChannelsResponse", e);
		}
	}

    @Override
    public void onBackPressed() {
        finishWithResult(RESULT_CANCELED, null);
    }

	// endregion
}
