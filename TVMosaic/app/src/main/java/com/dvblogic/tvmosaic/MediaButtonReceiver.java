package com.dvblogic.tvmosaic;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.view.KeyEvent;

/**
 * Created by Serg on 06-Oct-16.
 */
public class MediaButtonReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            KeyEvent key = (KeyEvent) intent.getParcelableExtra(Intent.EXTRA_KEY_EVENT);
            int keycode = key.getKeyCode();

            if (keycode == KeyEvent.KEYCODE_MEDIA_NEXT) {
                // next
            } else if (keycode == KeyEvent.KEYCODE_MEDIA_PREVIOUS) {
                // previous
            } else if (keycode == KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE) {
                // play or pause
            }
        }
    }
};
