package com.dvblogic.tvmosaic;

import com.dvblogic.dvblink_common.ChannelIdWithProgramsList;

public interface IBackgroundEPGUpdater {

    void getChannelsForEPGUpdate(long start_time, long end_time);
    void processEPGDataUpdate(ChannelIdWithProgramsList epg_data, int currently_updated_channel_idx, long start_time, long end_time);
    void updateStarted();
    void updateFinished();
}
