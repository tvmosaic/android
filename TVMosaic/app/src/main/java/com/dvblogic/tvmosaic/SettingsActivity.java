package com.dvblogic.tvmosaic;

import java.util.ArrayList;

import TVMosaic.TVMosaic.R;

import android.content.Intent;
import android.os.AsyncTask;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.DataProvider;
import com.dvblogic.dvblink_common.ForceEPGUpdateRequest;
import com.dvblogic.dvblink_common.ServerSettings;
import com.dvblogic.dvblink_common.Settings;
import com.dvblogic.dvblink_common.StatusCode;
import com.dvblogic.dvblink_common.SynchronousDataProviderCallback;
import com.dvblogic.dvblink_common.Utils;


public class SettingsActivity extends BaseSettingsActivity implements ActionOptionsPane.ActionOptionsPaneEventListener {
    private static final int SCAN_ACTIVITY_REQUEST_CODE = 1001;
    private static final int SERVER_SELECTION_ACTIVITY_REQUEST_CODE = 1002;
    private static final int ADVANCED_SETTINGS_ACTIVITY_REQUEST_CODE = 1003;
    private static final int PLUS_UPGRADE_ACTIVITY_REQUEST_CODE = 1004;

    private ArrayList<String> languages;
    private ActionOptionsPane _language_menu;
    boolean show_in_development_ = false;
    boolean supports_device_mgmt_ = false;

    @Override
    protected void PreCreate() {
        supports_device_mgmt_ = true;//settings_.getValue(SettingsManager.SUPPORTS_DEVICE_MGMT_KEY, false);
        show_in_development_ = settings_.getValue(SettingsManager.IN_DEVELOPMENT_FUNCTIONALITY_KEY, false);
    }

    @Override
    protected int getLayoutID() {
        return R.layout.settings_activity;
    }

    @Override
    protected void customizeList() {
        languages = language_manager_.GetLanguageList();
        customizeNetworkServerSettings();
        customizeTunerManagement();
        customizeGeneralSettings();
        customizeAdviserSettings();
        super.customizeList();
    }

    protected void customizeTunerManagement() {
        RelativeLayout layout;
        RelativeLayout category = (RelativeLayout) findViewById(R.id.server_management);
        category.setVisibility(View.GONE);
    }

    String GetFirstStartNameFromCurrentSetting() {
        long first_activity = settings_.GetValueOrDefault(Settings.START_FIRST_ACTIVITY_KEY, Settings.START_FIRST_DEFAULT);

        String ret_val = language_manager_.GetString(LocaleStrings.IDS_CHANNELS_AND_GUIDE_PAGE_TITLE);
        if (first_activity == Settings.START_FIRST_GUIDE)
            ret_val = language_manager_.GetString(LocaleStrings.IDS_CHANNELS_AND_GUIDE_PAGE_TITLE);
        else if (first_activity == Settings.START_FIRST_TVRECORDS)
            ret_val = language_manager_.GetString(LocaleStrings.IDS_RECORDED_TV_PAGE_TITLE);
        else if (first_activity == Settings.START_FIRST_MEDIA_LIBRARY)
            ret_val = language_manager_.GetString(LocaleStrings.IDS_MEDIA_LIBRARY_PAGE_TITLE);

        return ret_val;
    }

    private void customizeGeneralSettings() {
        TextView tv;
        RelativeLayout layout;
        RelativeLayout category = (RelativeLayout) findViewById(R.id.general_settings);
        layout = (RelativeLayout) category.findViewById(R.id.general_settings_category);
        customizeCategory(layout, LocaleStrings.IDS_SETTINGS_GENERAL, LocaleStrings.IDS_SETTINGS_GENERAL_DESC);

        View v = category.findViewById(R.id.plus_upgrade);
        if (v != null)
            v.setVisibility(View.GONE);

        if (settings_.getServerSettings().transcodingSupported) {
            customizeLink(
                    category, R.id.video_transcoding,
                    LocaleStrings.IDS_TRANSCODING, LocaleStrings.IDS_TRANSCODING_DESC,
                    TranscodingSettingsActivity.class
            );
        } else {
            v = category.findViewById(R.id.video_transcoding);
            if (v != null)
                v.setVisibility(View.GONE);
        }

//      first start activity selector
        layout = (RelativeLayout) category.findViewById(R.id.first_start_spinner);
        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_FIRST_START));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_FIRST_START_DESC));
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(GetFirstStartNameFromCurrentSetting());
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int first_activity = settings_.GetValueOrDefault(Settings.START_FIRST_ACTIVITY_KEY, Settings.START_FIRST_DEFAULT);
                switch (first_activity) {
                    case Settings.START_FIRST_GUIDE:
                        first_activity = Settings.START_FIRST_TVRECORDS;
                        break;
                    case Settings.START_FIRST_TVRECORDS:
                        first_activity = Settings.START_FIRST_MEDIA_LIBRARY;
                        break;
                    case Settings.START_FIRST_MEDIA_LIBRARY:
                        first_activity = Settings.START_FIRST_GUIDE;
                        break;
                }
                settings_.setValue(Settings.START_FIRST_ACTIVITY_KEY, first_activity);

                RelativeLayout layout = (RelativeLayout) v.findViewById(R.id.first_start_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                tv.setText(GetFirstStartNameFromCurrentSetting());
            }
        });

        layout = (RelativeLayout) category.findViewById(R.id.language_spinner);
        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_LANGUAGE));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_LANGUAGE_DESC));
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(settings_.getLanguage());
        layout.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                showLanguageMenu();
            }
        });

        customizeLink(
                category, R.id.advanced_settings_link,
                LocaleStrings.IDS_SETTINGS_ADVANCED, LocaleStrings.IDS_SETTINGS_ADVANCED_DESC,
                AdvancedSettingsActivity.class, ADVANCED_SETTINGS_ACTIVITY_REQUEST_CODE
        );

        v = category.findViewById(R.id.product_settings_link);
        if (v != null)
            v.setVisibility(View.GONE);

        v = category.findViewById(R.id.update_settings_link);
        if (v != null)
            v.setVisibility(View.GONE);

        customizeLink(
                category, R.id.about_link,
                LocaleStrings.IDS_ABOUT, LocaleStrings.IDS_ABOUT_DESC,
                AboutActivity.class
        );
    }

    private void customizeNetworkServerSettings() {
        RelativeLayout category = (RelativeLayout) findViewById(R.id.network_server_settings);
        RelativeLayout layout = (RelativeLayout) category.findViewById(R.id.network_server_settings_category);

        String desc = Utils.usesRemoteControl(this) ? LocaleStrings.IDS_SETTINGS_NETWORK_SERVER_DESC : LocaleStrings.IDS_SETTINGS_NETWORK_SERVER_TOUCH_DESC;
        customizeCategory(layout, LocaleStrings.IDS_SETTINGS_NETWORK_SERVER, desc);

        customizeLink(
                category, R.id.network_server_link,
                LocaleStrings.IDS_SETTINGS_NETWORK_SERVER_LINK, LocaleStrings.IDS_SETTINGS_NETWORK_SERVER_NO_SERVER,
                ServerSelectionActivity.class, SERVER_SELECTION_ACTIVITY_REQUEST_CODE
        );

        //if there is a currently selected server - display its data
        ServerSettings ss = settings_.getServerSettings();
        if (!ss.serverAddress.isEmpty()) {
            String current_server = String.format("%s (%s:%d)", ss.serverName, ss.serverAddress, ss.serverPort);

            RelativeLayout l = (RelativeLayout) category.findViewById(R.id.network_server_link);
            TextView tv = (TextView) l.findViewById(R.id.desc_text);
            tv.setText(current_server);
        }
        category.setVisibility(View.VISIBLE);
    }

    private void customizeAdviserSettings() {
        RelativeLayout category = (RelativeLayout) findViewById(R.id.adviser_settings);
        RelativeLayout layout = (RelativeLayout) category.findViewById(R.id.adviser_settings_category);

        if (show_in_development_) {
            customizeCategory(layout, LocaleStrings.IDS_SETTINGS_ADVISER, LocaleStrings.IDS_SETTINGS_ADVISER_DESC);
            customizeLink(
                    category, R.id.adviser_link,
                    LocaleStrings.IDS_SETTINGS_ADVISER_LINK, LocaleStrings.IDS_SETTINGS_ADVISER_LINK_DESC,
                    AdviserSettingsActivity.class
            );
        } else {
            category.setVisibility(View.GONE);
        }
    }

    private void onChangeLanguage(int position) {
        final String[] strLanguages = (String[]) languages.toArray(new String[languages.size()]);
        String current_language = strLanguages[position];
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.language_spinner);
        TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(current_language);
        settings_.setLanguage(current_language);
        refresh();
    }

    public void onActionPaneOption(int option_id, String selected_option) {
        onChangeLanguage(Integer.parseInt(selected_option));
        _language_menu.hide();
    }

    private void showLanguageMenu() {
        ActionOptionsPaneItems language_options = new ActionOptionsPaneItems(
                ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON
        );
        final String current_language = settings_.getLanguage();
        for (int idx = 0; idx < languages.size(); idx++) {
            String language = languages.get(idx);
            String language_id = String.valueOf(idx);
            language_options.add_item(language, language_id);
            if (current_language.equalsIgnoreCase(language)) {
                language_options.set_selected_item_id(language_id);
            }
        }
        _language_menu = new ActionOptionsPane(this, this, language_options, 0, null);
        _language_menu.show();
    }

    protected void refresh() {
        ScrollView scroll = (ScrollView) findViewById(R.id.settings_scrollview);
        int scroll_y = scroll.getScrollY();
        reloadLanguage();
        headBar.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_PAGE_TITLE));
        customizeList();
        scroll.scrollTo(0, scroll_y);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == SCAN_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            setResult(RESULT_OK, data);
            finish();
        }

        if (requestCode == SERVER_SELECTION_ACTIVITY_REQUEST_CODE  && resultCode == RESULT_OK) {
            //set result to OK to refresh Guide settings and exit
            setResult(RESULT_OK, data);
            finish();
        }

        if (requestCode == ADVANCED_SETTINGS_ACTIVITY_REQUEST_CODE) {
            customizeNetworkServerSettings();
        }

        if (requestCode == PLUS_UPGRADE_ACTIVITY_REQUEST_CODE) {
            customizeGeneralSettings();
        }
    }
}