package com.dvblogic.tvmosaic;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.dvblogic.dvblink_common.Utils;

import java.util.ArrayList;

import TVMosaic.TVMosaic.R;

public class TopLevelNavigationPane {

    public static final int exclude_flags_none_activity = 0;
    public static final int exclude_flags_guide_activity = 1;
    public static final int exclude_flags_tvrecords_activity = 2;
    public static final int exclude_flags_media_library_activity = 4;

    protected static final int guide_pane_action_id_ = 100;
    protected static final int recordings_pane_action_id_ = 101;
    protected static final int exit_pane_action_id_ = 104;
    protected static final int media_library_pane_action_id_ = 107;

    protected int exclude_activity_ = exclude_flags_none_activity;
    BaseActivity context_;
    TopLevelNavigationPaneImpl action_pane_ = null;

    public TopLevelNavigationPane(int exclude_activity, BaseActivity context) {
        context_ = context;
        exclude_activity_ = exclude_activity;
        action_pane_ = new TopLevelNavigationPaneImpl(context_, mylistener);
    }

    private ActionPane.ActionPaneEventListener mylistener = new ActionPane.ActionPaneEventListener() {
        public void onActionPaneAction(int action_id) {

            Class cls = null;

            if (action_id == guide_pane_action_id_)
                cls = GuideActivity.class;
            else if (action_id == recordings_pane_action_id_)
                cls = TVRecordsActivity.class;
            else if (action_id == media_library_pane_action_id_)
                cls = MediaLibraryActivity.class;
            else if (action_id == exit_pane_action_id_)
                context_.finish();

            if (cls != null) {
                Intent i = new Intent();
                i.setClass(context_, cls);
                context_.startActivity(i);
                context_.finish();
            }
        }

        public void onActionPaneOption(int option_id, String selected_option) {

        }
    };

    public class TopLevelNavigationPaneImpl extends ActionPane {

        public TopLevelNavigationPaneImpl(BaseActivity context, ActionPane.ActionPaneEventListener listener) {
            super(context, listener);
        }

        protected void fill_standard_pane_items() {
            //guide
            if ((exclude_activity_ & exclude_flags_guide_activity) == 0) {
                ActionPaneItem guide_api = new ActionPaneItem();
                guide_api.text_ = LocaleStrings.IDS_CHANNELS_AND_GUIDE_PAGE_TITLE;
                guide_api.id_ = guide_pane_action_id_;
                guide_api.left_icon_ = R.drawable.tvm_guide_icon_normal;
                guide_api.left_icon_selected_ = R.drawable.tvm_guide_icon_hover;
                guide_api.flags_ = action_pane_item_flag_do_not_hide_menu;
                items_.add(guide_api);
            }

            //recordings
            if ((exclude_activity_ & exclude_flags_tvrecords_activity) == 0 && context_.settings_.getValue(SettingsManager.CAN_RECORD_KEY, false)) {
                ActionPaneItem tvrecords_api = new ActionPaneItem();
                tvrecords_api.text_ = LocaleStrings.IDS_RECORDED_TV_PAGE_TITLE;
                tvrecords_api.id_ = recordings_pane_action_id_;
                tvrecords_api.left_icon_ = R.drawable.tvm_recordedtv_icon_normal;
                tvrecords_api.left_icon_selected_ = R.drawable.tvm_recordedtv_icon_hover;
                tvrecords_api.flags_ = action_pane_item_flag_do_not_hide_menu;
                items_.add(tvrecords_api);
            }

            //media library
            if ((exclude_activity_ & exclude_flags_media_library_activity) == 0) {
                ActionPaneItem media_library_api = new ActionPaneItem();
                media_library_api.text_ = LocaleStrings.IDS_MEDIA_LIBRARY_PAGE_TITLE;
                media_library_api.id_ = media_library_pane_action_id_;
                media_library_api.left_icon_ = R.drawable.tvm_listview_icon_normal;
                media_library_api.left_icon_selected_ = R.drawable.tvm_listview_icon_hover;
                media_library_api.flags_ = action_pane_item_flag_do_not_hide_menu;
                items_.add(media_library_api);
            }

            //exit
            ActionPaneItem exit_api = new ActionPaneItem();
            exit_api.text_ = LocaleStrings.IDS_EXIT;
            exit_api.id_ = exit_pane_action_id_;
            exit_api.left_icon_ = R.drawable.tvm_exit_icon_normal;
            exit_api.left_icon_selected_ = R.drawable.tvm_exit_icon_hover;
            exit_api.flags_ = action_pane_item_flag_do_not_hide_menu;
            items_.add(exit_api);
        }

        protected void add_extra_pane_items() {
        }
    }

    public void show() {
        action_pane_.show();
    }

    public void hide() {
        action_pane_.show();
    }
}
