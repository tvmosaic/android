package com.dvblogic.tvmosaic;

import java.lang.String;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;
import java.io.File;
import java.io.InputStream;

public abstract class LanguageSettings
{
	private static final String	CULTURE_NAME_FORMAT		= "%s";
	private static final String	ENGLISH_FILE_NAME		= "en.xml";
	private static final String	RUSSIAN_FILE_NAME		= "ru.xml";
	private static final String	CHINA_FILE_NAME			= "zh.xml";
	private static final String	CZECH_FILE_NAME			= "cs.xml";
	private static final String	DENMARK_FILE_NAME		= "da.xml";
	private static final String	GERMANY_FILE_NAME		= "de.xml";
	private static final String	SPAIN_FILE_NAME			= "es.xml";
	private static final String	FRANCE_FILE_NAME		= "fr.xml";
	private static final String	NETHERLANDS_FILE_NAME	= "nl.xml";
	private static final String	POLAND_FILE_NAME		= "pl.xml";
	private static final String	PORTUGAL_FILE_NAME		= "pt.xml";
	private static final String	SWEDEN_FILE_NAME		= "sv.xml";
	private static final String	UKRAIN_FILE_NAME		= "uk.xml";
	private static final String	SLOVEN_FILE_NAME		= "sl.xml";
	private static final String	ITALIAN_FILE_NAME		= "it.xml";

	private LanguageInfo		xml_language_;
	private ArrayList<String>	language_file_names_;
	private String				language_directrory_;
	private String				current_language_;

	public LanguageSettings() {
		language_directrory_ = "";
		current_language_ = "";
		xml_language_ = null;

		// Make language file list
		language_file_names_ = new ArrayList<String>();
		language_file_names_.add(ENGLISH_FILE_NAME);
		language_file_names_.add(RUSSIAN_FILE_NAME);
		language_file_names_.add(CHINA_FILE_NAME);
		language_file_names_.add(CZECH_FILE_NAME);
		language_file_names_.add(DENMARK_FILE_NAME);
		language_file_names_.add(GERMANY_FILE_NAME);
		language_file_names_.add(SPAIN_FILE_NAME);
		language_file_names_.add(FRANCE_FILE_NAME);
		language_file_names_.add(NETHERLANDS_FILE_NAME);
		language_file_names_.add(POLAND_FILE_NAME);
		language_file_names_.add(PORTUGAL_FILE_NAME);
		language_file_names_.add(SWEDEN_FILE_NAME);
		language_file_names_.add(UKRAIN_FILE_NAME);
		language_file_names_.add(SLOVEN_FILE_NAME);
		language_file_names_.add(ITALIAN_FILE_NAME);
	}

	public boolean LoadLanguage(String language_directory, String language_name) {
		if (language_directory.length() > 0 && !language_directory.endsWith(File.separator)) {
			language_directory += File.separator;
		}
		language_directrory_ = language_directory;

		if (SetCurrentLanguage(language_name)) {
			current_language_ = language_name;
			return true;
		}
		return false;
	}

	public ArrayList<String> GetLanguageList() {
		ArrayList<String> languages = new ArrayList<String>();

		for (String file_name : language_file_names_) {
			LanguageInfo info = GetLanguageParam(language_directrory_ + file_name);
			if (info != null) {
				if (!languages.contains(info.Name)) {
					languages.add(info.Name);
				}
			}
		}
		Collections.sort(languages);

		return languages;
	}

	public String GetCurrentLanguage() {
		return current_language_;
	}

	public void setFolder(String language_directory) {
		if (language_directory.length() > 0 && !language_directory.endsWith(File.separator)) {
			language_directory += File.separator;
		}
		language_directrory_ = language_directory;
	}

	public String getFolder() {
		return language_directrory_;
	}

	public boolean SetCurrentLanguage(String language_name) {
		boolean success = false;

		xml_language_ = null;

		try {
			String file_name = GetLanguageFileName(language_name);
			if (file_name != null) {
				String language_path = language_directrory_ + file_name;
				InputStream xml_stream = OpenLanguageFile(language_path);
				if (null != xml_stream) {
					xml_language_ = new LanguageInfo(xml_stream, language_path);
					if (xml_language_.IsValid) {
						current_language_ = language_name;
						success = true;
					}
				}
			}
		} catch (Exception e) {
			success = false;
		}

		return success;
	}

	public String GetString(String xpath) {
		String value = xpath;

		try {
			value = xml_language_.Strings.get(xpath);
			if (value == null)
				value = xpath;

		} catch (Exception e) {
			value = xpath;
		}

		return value;
	}

	public Locale GetLocale() {
		Locale locale = null;

		try {
			locale = new Locale(String.format(CULTURE_NAME_FORMAT, xml_language_.ID));
		} catch (Exception e) {
			locale = new Locale(String.format(CULTURE_NAME_FORMAT, current_language_));
		}

		return locale;
	}

	private String GetLanguageFileName(String language_name) {
		for (String file : language_file_names_) {
			String file_name = language_directrory_ + file;
			LanguageInfo info = GetLanguageParam(file_name);
			if (info != null) {
				if (language_name.equalsIgnoreCase(info.Name)) {
					return file;
				}
			}
		}
		return null;
	}

	private LanguageInfo GetLanguageParam(String language_path) {
		LanguageInfo info = null;
		try {
			InputStream is = OpenLanguageFile(language_path);
			info = new LanguageInfo(is, language_path);
			if (info.IsValid) {
				return info;
			} else {
				info = null;
			}
		} catch (Exception e) {
			// System.out.println(e.getMessage());
		}
		return info;
	}

	public String getID() {
		return xml_language_.ID;
	}

	protected abstract InputStream OpenLanguageFile(String language_path);

	private String langFileNameToISO(String langFileName) {
		return langFileName.substring(0, langFileName.lastIndexOf("."));
	}

	private String langISOToName(String langISO) {
		for (String file : language_file_names_) {
			String fileName = language_directrory_ + file;
			LanguageInfo info = GetLanguageParam(fileName);
			if (info != null) {
				if (langISO.equalsIgnoreCase(info.ID)) {
					return info.Name;
				}
			}
		}
		return null;
	}

	public String getSystemLanguage() {
		setFolder(Constants.LanguageDirectory);
		String systemLang = Locale.getDefault().getLanguage();
		for (String langFileName: language_file_names_) {
			String langISO = langFileNameToISO(langFileName);
			if (systemLang.equals(new Locale(langISO).getLanguage())) {
				return langISOToName(langISO);
			}
		}
		return null;
	}
}
