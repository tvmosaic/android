package com.dvblogic.tvmosaic;

import android.util.Log;
import android.widget.Toast;
import com.dvblogic.dvblink_common.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

class MediaLibraryProvider {

    private static final String TAG = "MediaLibraryProvider";

    private static final int STATE_NOT_READY = -1;
    private static final int STATE_READY = 1000;
    private static final int STATE_GET_ROOT_OBJECTS = 1001;
    private static final int STATE_GET_FIRST_LEVEL_OBJECTS = 1002;
    private static final int STATE_GET_SECOND_LEVEL_OBJECTS = 1003;
    private static final int STATE_GET_PB_ITEMS = 1004;
    private static final int STATE_SEARCH_OBJECTS = 1005;
    private static final int STATE_GET_PB_ITEM = 1006;
    private static final int STATE_GET_RESUME_INFO = 1007;
    private static final int STATE_SET_RESUME_INFO = 1008;

    public class MediaLibrarySource {
        String id;
        String name;
    }

    public class MediaLibraryCategory {
        Container category;
        public ArrayList<Container> subcategories = new ArrayList<>();
    }

    public class MediaLibrarySourceStructure {
        public ArrayList<Container> filters = new ArrayList<>();
        public ArrayList<MediaLibraryCategory> categories = new ArrayList<>();

        public void sort() {
            if (filters != null) {
                Collections.sort(filters, new Comparator<Container>() {
                    @Override
                    public int compare(Container item1, Container item2) {
                        // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                        return item1.Name.compareToIgnoreCase(item2.Name);
                    }
                });
            }
            if (categories != null) {
                for (MediaLibraryProvider.MediaLibraryCategory category : categories) {
                    Collections.sort(category.subcategories, new Comparator<Container>() {
                        @Override
                        public int compare(Container item1, Container item2) {
                            // -1 - less than, 1 - greater than, 0 - equal, all inversed for descending
                            return item1.Name.compareToIgnoreCase(item2.Name);
                        }
                    });
                }
            }
        }
    }

    private int state = STATE_NOT_READY;

    private BaseActivity activity;
    private Object serverResponse;
    private ScanChannelsDataProvider serverDataProvider_;
    private StatusCode httpError = StatusCode.STATUS_OK;
    private String serverAddress;

    private ContainerList first_level_containers_ = null;
    private MediaLibrarySourceStructure pending_source_structure_ = null;

    public MediaLibraryProvider(BaseActivity activity) {
        this.activity = activity;
        serverDataProvider_ = activity.data_provider_;
        serverAddress = activity.serverSettings.serverAddress;
        state = STATE_READY;
    }

    public void ErrorProcessing(String command, StatusCode error) {
        httpError = error;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.activateProgressBar(false);
                if (state == STATE_GET_RESUME_INFO) {
                    ResumeInfo ri = new ResumeInfo();
                    onResumeInfo(ri);
                } else
                if (state == STATE_SET_RESUME_INFO) {
                    onSetResumeInfo();
                } else
                if (activity.language_manager_ != null) {
                    String msg = String.format("%s", activity.language_manager_.ErrorToString(httpError));
                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                    //notify caller
                    onError();
                }
                state = STATE_READY;
            }
        });
    }

    public void DataProcessing(String command, Object result_data) {
        serverResponse = result_data;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state == STATE_GET_ROOT_OBJECTS) {
                    processRootObjects();
                } else if (state == STATE_GET_FIRST_LEVEL_OBJECTS) {
                    processFirstLevelObjects();
                } else if (state == STATE_GET_SECOND_LEVEL_OBJECTS) {
                    processSecondLevelObjects();
                } else if (state == STATE_GET_PB_ITEMS) {
                    processItems();
                } else if (state == STATE_SEARCH_OBJECTS) {
                    processSearchResults();
                } else if (state == STATE_GET_PB_ITEM) {
                    processItem();
                } else if (state == STATE_GET_RESUME_INFO) {
                    processResumeInfo();
                } else if (state == STATE_SET_RESUME_INFO) {
                    processSetResumeInfo();
                } else {
                    throw new RuntimeException("unknown state");
                }
                serverResponse = null;
            }
        });
    }

    public void requestSources() {

        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }

        state = STATE_GET_ROOT_OBJECTS;
        activity.activateProgressBar(true);

        serverDataProvider_.GetObjectAsXml(new ObjectRequester(ObjectRequester.OBJECT_ROOT_ID, serverAddress));
    }

    private void processRootObjects() {
        state = STATE_READY;
        activity.activateProgressBar(false);

        ArrayList<MediaLibrarySource> sources = new ArrayList<>();

        PlaybackItemContainer data = null;
        try {
            data = (PlaybackItemContainer) Serializer.deserialize((String) serverResponse, PlaybackItemContainer.class);
        } catch (Exception e) {data = null;}

        if (data != null) {
            for (Container container : data.Containers.list()) {
                if (container.SourceID.equalsIgnoreCase(ObjectRequester.DLRECORDER_SOURCE_ID)) {
                    //filter out the RecordedTV source
                    continue;
                }
                MediaLibrarySource src = new MediaLibrarySource();
                src.id = container.ObjectID;
                src.name = container.Name;
                sources.add(src);
            }
        }

        onSources(sources);
    }

    public void requestSourceStructure(String source_id) {
        //first we get level one containers
        state = STATE_GET_FIRST_LEVEL_OBJECTS;
        activity.activateProgressBar(true);

        first_level_containers_ = null;
        pending_source_structure_ = new MediaLibrarySourceStructure();

        ObjectRequester obj_req = new ObjectRequester(source_id, serverAddress);
        obj_req.ObjectType = ObjectType.OBJECT_CONTAINER;

        serverDataProvider_.GetObjectAsXml(obj_req);
    }

    private void processFirstLevelObjects() {
        state = STATE_READY;
        activity.activateProgressBar(false);

        PlaybackItemContainer data = null;
        try {
            data = (PlaybackItemContainer) Serializer.deserialize((String) serverResponse, PlaybackItemContainer.class);
        } catch (Exception e) {
            data = null;
        }

        if (data != null)
            first_level_containers_ = data.Containers;

        if (first_level_containers_ == null || first_level_containers_.size() == 0) {
            //empty source callback
            onSourceStructure(pending_source_structure_);
        } else {
            //get sub-containers of the first level containers one by one
            requestSecondLevelObjects(first_level_containers_.get(0).ObjectID);
        }
    }

    private void requestSecondLevelObjects(String object_id) {
        //first we get level one containers
        state = STATE_GET_SECOND_LEVEL_OBJECTS;
        activity.activateProgressBar(true);

        ObjectRequester obj_req = new ObjectRequester(object_id, serverAddress);
        obj_req.ObjectType = ObjectType.OBJECT_CONTAINER;

        serverDataProvider_.GetObjectAsXml(obj_req);
    }

    private void processSecondLevelObjects() {
        state = STATE_READY;
        activity.activateProgressBar(false);

        PlaybackItemContainer data = null;
        try {
            data = (PlaybackItemContainer) Serializer.deserialize((String) serverResponse, PlaybackItemContainer.class);
        } catch (Exception e) {
            data = null;
        }

        if (data != null) {
            if (data.Containers == null || data.Containers.size() == 0) {
                //this container is filter
                pending_source_structure_.filters.add(first_level_containers_.get(0));
            } else {
                //this container is category
                MediaLibraryCategory cat = new MediaLibraryCategory();
                cat.category = first_level_containers_.get(0);
                cat.subcategories = data.Containers.list();
                pending_source_structure_.categories.add(cat);
            }
        }

        first_level_containers_.remove(0);

        if (first_level_containers_.size() == 0) {
            onSourceStructure(pending_source_structure_);
        } else {
            //get sub-containers gor the next source
            requestSecondLevelObjects(first_level_containers_.get(0).ObjectID);
        }
    }

    public void requestItems(String object_id, int start, int count) {
        state = STATE_GET_PB_ITEMS;
        activity.activateProgressBar(true);

        ObjectRequester obj_req = new ObjectRequester(object_id, serverAddress);
        obj_req.ObjectType = ObjectType.OBJECT_ITEM;
        obj_req.StartPosition = start;
        obj_req.RequestedCount = count;

        serverDataProvider_.GetObjectAsXml(obj_req);
    }


    private void processItem() {
        state = STATE_READY;
        activity.activateProgressBar(false);

        PlaybackItemContainer data = null;
        try {
            data = (PlaybackItemContainer) Serializer.deserialize((String) serverResponse, PlaybackItemContainer.class);
        } catch (Exception e) {
            data = null;
        }

        onItem(data);
    }

    public void requestItem(String object_id) {
        state = STATE_GET_PB_ITEM;
        activity.activateProgressBar(true);

        ObjectRequester obj_req = new ObjectRequester(object_id, serverAddress);
        obj_req.ObjectType = ObjectType.OBJECT_ITEM;
        obj_req.IsChildrenRequest = false;

        serverDataProvider_.GetObjectAsXml(obj_req);
    }


    private void processItems() {
        state = STATE_READY;
        activity.activateProgressBar(false);

        PlaybackItemContainer data = null;
        try {
            data = (PlaybackItemContainer) Serializer.deserialize((String) serverResponse, PlaybackItemContainer.class);
        } catch (Exception e) {
            data = null;
        }

        onItems(data);
    }


    public void searchObjects(String object_id, String search_text) {
        state = STATE_SEARCH_OBJECTS;
        activity.activateProgressBar(true);

        ObjectSearcher obj_req = new ObjectSearcher(object_id, serverAddress, search_text);
        obj_req.ObjectType = ObjectType.OBJECT_ITEM;

        serverDataProvider_.SearchObjects(obj_req);
    }

    private void processSearchResults() {
        state = STATE_READY;
        activity.activateProgressBar(false);

        PlaybackItemContainer data = null;
        try {
            data = (PlaybackItemContainer) Serializer.deserialize((String) serverResponse, PlaybackItemContainer.class);
        } catch (Exception e) {
            data = null;
        }

        onSearchResults(data != null ? data.Items : null);
    }

    public void requestResumeInfo(String obj_id) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_GET_RESUME_INFO;
        serverDataProvider_.GetResumeInfo(new GetResumeInfoRequest(obj_id));
    }

    private void processResumeInfo() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);
            ResumeInfo resp = (ResumeInfo) serverResponse;
            onResumeInfo(resp);
        } catch (Exception exception) {
            Log.e(TAG, "processResumeInfo", exception);

            ResumeInfo ri = new ResumeInfo();
            onResumeInfo(ri);
        }
    }

    public void requestSetResumeInfo(String objectID, int pos) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_SET_RESUME_INFO;
        serverDataProvider_.SetResumeInfo(new SetResumeInfoRequest(objectID, pos));
    }

    private void processSetResumeInfo() {
        state = STATE_READY;
        activity.activateProgressBar(false);
        onSetResumeInfo();
    }

    protected void onSources(ArrayList<MediaLibrarySource> sources) {}
    protected void onSourceStructure(MediaLibrarySourceStructure source_structure) {}
    protected void onItems(PlaybackItemContainer items) {}
    protected void onItem(PlaybackItemContainer item) {}
    protected void onSearchResults(PlaybackItemList items) {}
    protected void onError() {}
    protected void onResumeInfo(ResumeInfo ri) {}
    protected void onSetResumeInfo() {}
}

