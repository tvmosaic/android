package com.dvblogic.tvmosaic;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class ObservableVertScrollView extends ScrollView
{

	private ScrollViewListener	scrollViewListener	= null;

	public ObservableVertScrollView(Context context) {
		super(context);
	}

	public ObservableVertScrollView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}

	public ObservableVertScrollView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
	}

	public void setScrollViewListener(ScrollViewListener scrollViewListener) {
		this.scrollViewListener = scrollViewListener;
	}

	@Override
	protected void onScrollChanged(int l, int t, int oldl, int oldt) {
		super.onScrollChanged(l, t, oldl, oldt);
		if (scrollViewListener != null) {
			scrollViewListener.onScrollChanged(this, l, t, oldl, oldt);
		}
	}

}
