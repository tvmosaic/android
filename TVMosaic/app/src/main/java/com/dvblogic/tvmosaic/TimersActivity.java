package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import TVMosaic.TVMosaic.R;

import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.*;
import com.dvblogic.dvblink_common.*;

import java.util.*;

import static com.dvblogic.dvblink_common.ObjectRequester.DLRECORDER_RECORDINGS_BY_DATE_ID;
import static com.dvblogic.tvmosaic.ActionOptionsPaneItems.empty_selected_option_id_;

public class TimersActivity extends BaseActivity implements OnBitmapLoadFinished
{
    private static final int state_ready      = 1;
    private static final int state_requesting_timers      = 2;
    private static final int state_requesting_channels      = 3;
    private static final int state_cancelling_timer      = 4;

    private static final String TAG = "TVRecordsActivity";

    private class ResultsAdapter extends BaseAdapter {
        private ArrayList<Recording> timers = new ArrayList<>();

        private LayoutInflater ltInflater;
        private int positionLastShowedProgramInfo = -1;
        private int position_to_remove_ = -1;

        private View.OnClickListener resultItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CancelTimer((int) view.getTag());
            }
        };
        private View.OnFocusChangeListener resultItemFocusListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (!focus) {
                    return;
                }
                showProgramInfo((int) view.getTag());
            }
        };
        private View.OnKeyListener resultItemKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && Common.okPressed(keyEvent.getKeyCode())) {
                    CancelTimer((int) view.getTag());
                    return true;
                }
                return false;
            }
        };

        protected void CancelTimer(int position) {

            position_to_remove_ = position;

            String dlg_text = language_manager_.GetString(LocaleStrings.IDS_TIMERS_INFO_DLG_CANCEL_TIMER_TEXT);
            String positive_btn_text = language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES);
            String negative_btn_text = language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO);

            Recording timer = timers.get(position_to_remove_);
            if (timer.Program.IsRepeatRecord) {
                dlg_text = language_manager_.GetString(LocaleStrings.IDS_TIMERS_INFO_DLG_CANCEL_WHAT_TEXT);
                positive_btn_text = language_manager_.GetString(LocaleStrings.IDS_TIMERS_CANCEL_ONE);
                negative_btn_text = language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL);
            }

            AlertDialog.Builder ad = new AlertDialog.Builder(_context)
                    .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                    .setMessage(dlg_text)
                    .setPositiveButton(positive_btn_text,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Recording timer = timers.get(position_to_remove_);
                                    requestCancelTimer(timer);
                                }
                            })
                    .setNegativeButton(negative_btn_text,
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            });

            if (timer.Program.IsRepeatRecord) {
                ad.setNeutralButton(language_manager_.GetString(LocaleStrings.IDS_TIMERS_CANCEL_SCHEDULE),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                Recording timer = timers.get(position_to_remove_);
                                requestCancelSchedule(timer);
                            }
                        });
            }

            ad.show();

        }

        private void showProgramInfo(int position) {
            if (position == positionLastShowedProgramInfo) {
                return;
            }
            positionLastShowedProgramInfo = position;

            Recording timer = timers.get(position);

            if (programDetailsPane.getVisibility() == View.VISIBLE) {
                String channel_name = channels.containsKey(timer.ChannelID) ? channels.get(timer.ChannelID).Name : "";
                programDetailsPane.setInfo(timer.Program, channel_name, 0, false, false, language_manager_);
                setProgramImage(timer);
            }
        }

        public ResultsAdapter() {
            ltInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return timers.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ltInflater.inflate(R.layout.search_ex_item, parent, false);
            }
            Recording timer = timers.get(position);

            int textColor = getResources().getColor(
                    timer.IsConflict ?
                            R.color.new_timer_record_conflict :
                            R.color.new_text_primary_active
            );

            TextView label;
            label = (TextView) view.findViewById(R.id.title);
            if (timer.Program.Title != null)
                label.setText(timer.Program.Title);
            label.setTextColor(textColor);

            if (!Utils.usesRemoteControl(_context)) {
                String str = ((timer.Program.Subname == null) ? "" : timer.Program.Subname);
                String ES = "";
                if (timer.Program.SeasonNum != 0)
                    ES += String.format("S%02d", timer.Program.SeasonNum);

                if (timer.Program.EpisodeNum != 0) {
                    ES += String.format("E%02d", timer.Program.EpisodeNum);
                }
                if (str.isEmpty()) {
                    str += ES;
                } else {
                    if (!ES.isEmpty())
                        str += "  |  " + ES;
                }

                label = (TextView) view.findViewById(R.id.subtitle);
                label.setText(str);
                label.setTextColor(textColor);
                if (!str.isEmpty())
                    label.setVisibility(View.VISIBLE);
                else
                    label.setVisibility(View.GONE);
            }

            label = (TextView) view.findViewById(R.id.start_time);
            label.setText(formatStartTime(timer));
            label.setTextColor(textColor);

            label = (TextView) view.findViewById(R.id.start_date);
            label.setText(formatStartDate(
                    timer.Program.StartTime, timer.Program.StartTime + timer.Program.Duration
            ));
            label.setTextColor(textColor);

            String channel_name = channels.containsKey(timer.ChannelID) ? channels.get(timer.ChannelID).Name : "";
            label = (TextView) view.findViewById(R.id.channel);
            label.setText(channel_name);
            label.setTextColor(textColor);

            label = (TextView) view.findViewById(R.id.duration);
            label.setText(formatDuration(timer.Program.Duration));
            label.setTextColor(textColor);

            view.setTag(position);
            view.setOnFocusChangeListener(resultItemFocusListener);
            view.setOnClickListener(resultItemClickListener);
            view.setOnKeyListener(resultItemKeyListener);

            return view;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return timers.get(position);
        }

        public void addTimer(Recording timer) {
            if (timer.Program.IsRecord)
                timers.add(timer);
        }

        public void Clear() {
            timers.clear();
            positionLastShowedProgramInfo = -1;
        }
    }

    private ListView results;
    private ResultsAdapter resultsAdapter;
    private ProgramDetails programDetailsPane;
    int state_ = state_ready;
    HashMap<String, Channel> channels = new HashMap<>();
    private ChannelIDList channels_to_refresh = new ChannelIDList();

    private String formatStartDate(long startTime, long endTime) {
        return (Utils.isToday(startTime) ?
                language_manager_.GetString(LocaleStrings.IDS_TODAY) :
                Utils.UtcToMediumDateString(_context, startTime));
    }

    private String formatStartTime(Recording timer) {
        long startTime= timer.Program.StartTime;
        long endTime = timer.Program.StartTime + timer.Program.Duration;

        if (timer.IsActive) {
            return language_manager_.GetString(LocaleStrings.IDS_RECORDING_IN_PROGRESS);
        }

        return Utils.UtcToShortTimeString(_context, startTime);
    }

    private String formatDuration(long duration) {
        return String.format(Locale.getDefault(), "%d %s",
                duration / 60, language_manager_.GetString(LocaleStrings.IDS_MINUTE)
        );
    }

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.timers_activity);

        final int number_of_visible_rows = 5;
        final int row_height  = (int)this.getResources().getDimension(R.dimen.search_item_height);
        final int divider_width = (int)this.getResources().getDimension(R.dimen.search_divider_width);

        final int layout_height = Utils.usesRemoteControl(this) ? (row_height * number_of_visible_rows + divider_width * number_of_visible_rows) : ViewGroup.LayoutParams.MATCH_PARENT;
        final View lv = findViewById(R.id.search_results);

        if (!Utils.usesRemoteControl(this)) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) lv.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, R.id.status_line);
            lv.setLayoutParams(params);
        }

        lv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lv.getLayoutParams().height = layout_height;
                lv.requestLayout();
                lv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(language_manager_.GetString(LocaleStrings.IDS_TIMERS));

        View root = findViewById(R.id.tvrecords_layout);
        results = (ListView) root.findViewById(R.id.search_results);
        programDetailsPane = (ProgramDetails) root.findViewById(R.id.program_details_pane);
        if (!Utils.usesRemoteControl(this))
            programDetailsPane.setVisibility(View.GONE);

        resultsAdapter = new ResultsAdapter();
        results.setAdapter(resultsAdapter);
        results.setItemsCanFocus(true);

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                requestChannels();
            }
        });
    }

    @Override
    public void onBackPressed() {
        if (channels_to_refresh.size() > 0) {
            Intent intent = getIntent();

            String channels_xml = Serializer.serialize(channels_to_refresh);
            intent.putExtra("need_channel_update", channels_xml);
            setResult(RESULT_OK, intent);
        }
        finish();
    }

    private void clearTimers() {
        programDetailsPane.clear();
        setProgramImage(null);
    }

    private void setProgramImage(Recording timer) {
        programDetailsPane.findViewById(R.id.description_image).setVisibility(View.INVISIBLE);
        if (timer == null) {
            return;
        }
        boolean loadBitmap = (timer.Program.Image != null && !timer.Program.Image.isEmpty());
        if (loadBitmap) {
            new com.dvblogic.tvmosaic.BitmapLoader(
                    this, this, timer.Program.Image,
                    (ImageView) programDetailsPane.findViewById(R.id.description_image), null
            ).start();
        }
    }

    protected void requestCancelTimer(Recording timer) {
        activateProgressBar(true);
        state_ = state_cancelling_timer;
        channels_to_refresh.add(timer.ChannelID);

        RecordingRemover rr = new RecordingRemover(timer.RecordingID);
        data_provider_.RemoveRecording(rr);
    }

    protected void requestCancelSchedule(Recording timer) {
        activateProgressBar(true);
        state_ = state_cancelling_timer;
        channels_to_refresh.add(timer.ChannelID);

        ScheduleRemover sr = new ScheduleRemover(timer.ScheduleID);
        data_provider_.RemoveSchedule(sr);
    }

    private void processCancelTimer() {
        activateProgressBar(false);
        state_ = state_ready;

        refresh();
    }

    protected void requestChannels() {
        activateProgressBar(true);
        state_ = state_requesting_channels;

        ChannelsRequest cr = new ChannelsRequest();
        data_provider_.GetChannels(cr);
    }

    private void processChannels() {
        Log.d(TAG, "processChannels()");
        ChannelsList cl = (ChannelsList)response_;

        channels.clear();
        for (int i=0; i<cl.size(); i++) {
            channels.put(cl.get(i).ID, cl.get(i));
        }

        state_ = state_ready;
        activateProgressBar(false);

        requestTimers();
    }

    protected void requestTimers() {
        activateProgressBar(true);
        state_ = state_requesting_timers;

        RecordingsRequest rr = new RecordingsRequest();
        data_provider_.GetRecordings(rr);
    }

    private void processTimers() {
        Log.d(TAG, "processTimers()");

        RecordingsList timers = (RecordingsList)response_;
        showTimers(timers);

        state_ = state_ready;
        activateProgressBar(false);
    }

    private void showTimers(RecordingsList timers) {

        resultsAdapter.Clear();
        for (Recording timer : timers.list()) {
            resultsAdapter.addTimer(timer);
        }

        if (timers.size() == 0)
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_TIMERS_NO_TIMERS_FOUND), Toast.LENGTH_SHORT).show();

        resultsAdapter.notifyDataSetChanged();
    }

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;

        if (state_ == state_requesting_timers) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processTimers();
                }
            });
        } else
        if (state_ == state_requesting_channels) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processChannels();
                }
            });
        } else
        if (state_ == state_cancelling_timer) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processCancelTimer();
                }
            });
        }
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        http_error_ = error;
        runOnUiThread(new Runnable() {
            public void run() {
                ProcessDataProviderError();
            }
        });
    }

    protected void ProcessDataProviderError() {
        try {
            String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

            state_ = state_ready;
            activateProgressBar(false);
        } catch (Exception e) {
        }
    }

    @Override
    public void loadFinished() {}

    public void refresh() {
        clearTimers();
        requestChannels();
    }

}
