package com.dvblogic.tvmosaic;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import com.dvblogic.dvblink_common.Settings;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class SplashActivity extends AppCompatActivity {
    private static final String TAG = "TVMosaic";
    public static final String PREF_NAME_FIRSTLAUNCH = "firstlaunch";

    protected boolean first_launch_ = false;
    SettingsManager settings_ = null;

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        first_launch_ = isFirstLaunch();

        settings_ = new SettingsManager(this);
        settings_.setValue("list_mode", false);

        if (first_launch_) {
            String systemLang = LanguageManager.getInstance(this).getSystemLanguage();
            if (systemLang != null) {
                settings_.setLanguage(systemLang);
            }
        }

        getIntent().setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        //start main activity
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startGuideActivity();
                finish();
            }
        }, 1 * 1000);
    }

    protected void startGuideActivity() {
        int first_activity = settings_.GetValueOrDefault(Settings.START_FIRST_ACTIVITY_KEY, Settings.START_FIRST_DEFAULT);
        Class cls = GuideActivity.class;
        if (first_activity == Settings.START_FIRST_GUIDE)
            cls = GuideActivity.class;
        else if (first_activity == Settings.START_FIRST_TVRECORDS)
            cls = TVRecordsActivity.class;
        else if (first_activity == Settings.START_FIRST_MEDIA_LIBRARY)
            cls = MediaLibraryActivity.class;

        Intent i = new Intent();
        i.setClass(getApplicationContext(), cls);
        i.putExtra("first_launch", first_launch_);
        startActivity(i);
    }

    private boolean isFirstLaunch() {
        SharedPreferences preferences = getPreferences(Activity.MODE_PRIVATE);
        if (preferences.getBoolean(PREF_NAME_FIRSTLAUNCH, true)) {
            preferences.edit().putBoolean(PREF_NAME_FIRSTLAUNCH, false).apply();
            return true;
        }
        return false;
    }

}
