package com.dvblogic.tvmosaic;

import java.io.IOException;
import java.io.InputStream;

import android.content.Context;

import com.dvblogic.dvblink_common.StatusCode;

public class LanguageManager extends LanguageSettings
{
	static Context	context_;
	
	private static LanguageManager instance;
	
	public static LanguageManager getInstance(Context context) {
		if (instance == null) {
			context_ = context;
			instance = new LanguageManager();
		}
		return instance;
	}

	@Override
	protected InputStream OpenLanguageFile(String language_path) {
		try {
			return context_.getAssets().open(language_path);
		} catch (IOException e) {
			//e.printStackTrace();
		} catch (Exception e) {
			//e.printStackTrace();
		}
		return null;
	}

	public String ErrorToString(StatusCode error_code) {
		if (error_code == StatusCode.STATUS_INVALID_DATA)
			return GetString(LocaleStrings.IDS_INVALID_DATA_MSG);
		else if (error_code == StatusCode.STATUS_MC_NOT_RUNNING)
			return GetString(LocaleStrings.IDS_GENERAL_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_NO_DEFAULT_RECORDER)
			return GetString(LocaleStrings.IDS_GENERAL_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_MCE_CONNECTION_ERROR)
			return GetString(LocaleStrings.IDS_GENERAL_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_CONNECTION_ERROR)
			return GetString(LocaleStrings.IDS_DVBLINK_CONNECTION_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_UNAUTHORISED)
			return GetString(LocaleStrings.IDS_UNAUTHORISED_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_INVALID_PARAM)
			return GetString(LocaleStrings.IDS_GENERAL_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_NOT_IMPLEMENTED)
			return GetString(LocaleStrings.IDS_GENERAL_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_ERROR)
			return GetString(LocaleStrings.IDS_GENERAL_ERROR_MSG);
		else if (error_code == StatusCode.STATUS_PRODUCT_NOT_ACTIVATED)
			return GetString(LocaleStrings.IDS_PRODUCT_NOT_ACTIVATED);
		else if (error_code == StatusCode.STATUS_NO_FREE_TUNER)
			return GetString(LocaleStrings.IDS_NO_FREE_TUNER);

		return GetString(LocaleStrings.IDS_GENERAL_ERROR_MSG);
	}
}
