package com.dvblogic.tvmosaic;


import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import TVMosaic.TVMosaic.R;


abstract public class BaseSettingsActivity extends BaseActivity {

    protected TextView headBar;

    abstract protected int getLayoutID();

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        PreCreate();

        show_exit_dialog = false;
        setContentView(getLayoutID());
        headBar = (TextView) findViewById(R.id.head_bar);
        headBar.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_PAGE_TITLE));
        customizeList();
    }

    protected void PreCreate() {
        //to be inhereted from
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return true;
        }
        if (keyCode == 58) {
            return false;
        }
        if (Common.backPressed(keyCode, event)) {
            onBackPressed();
            return true;
        }
        return false;
    }

    protected void customizeList() {
        View scroll = findViewById(R.id.settings_content);
        scroll.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return onKeyDown(keyCode, event);
            }
        });
    }

    protected void customizeCategory(RelativeLayout layout, String title, String desc) {
        TextView category_text = (TextView) layout.findViewById(R.id.category_text);
        TextView category_desc = (TextView) layout.findViewById(R.id.category_desc);
        category_text.setText(language_manager_.GetString(title));
        category_desc.setText(language_manager_.GetString(desc));
    }

    protected void customizeLink(RelativeLayout parent, int layoutID, String title, String desc, final Class activity) {
        customizeLink(parent, layoutID, title, desc, activity, 0);
    }

    protected void customizeLink(
            RelativeLayout parent, int layoutID, String title, String desc, final Class activity, final int requestCode
    ) {
        customizeLink(parent, layoutID, title, desc, new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(getBaseContext(), activity), requestCode);
            }
        });
    }

    protected void customizeLink(
            RelativeLayout parent, int layoutID, String title, String desc, View.OnClickListener listener
    ) {
        TextView tv;
        RelativeLayout layout = (RelativeLayout) parent.findViewById(layoutID);
        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(title));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(desc));
        layout.setOnClickListener(listener);
    }
}
