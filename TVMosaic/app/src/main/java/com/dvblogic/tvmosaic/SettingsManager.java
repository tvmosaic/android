package com.dvblogic.tvmosaic;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.dvblogic.dvblink_common.ServerSettings;
import com.dvblogic.dvblink_common.Settings;

public class SettingsManager extends Settings
{
	public static final String	CLIENT_ID_KEY						= "client_id";
	public static final String	CAN_RECORD_KEY						= "can_record";
	public static final String	SUPPORTS_DEVICE_MGMT_KEY			= "device_management";
	public static final String	IN_DEVELOPMENT_FUNCTIONALITY_KEY    = "in_development";
	public static final String	EPG_UPDATE_TIMESTAMP_KEY			= "epg_update_timestamp";
	public static final String	LAST_PLAYBACK_SRC_ID_KEY			= "last_playback_source";
	public static final String	LAST_PLAYBACK_CONTAINER_ID_KEY			= "last_playback_container";

	public static final long				EPG_UPDATE_TIMESTAMP_DEFAULT				    = 0;
	public static final String				LAST_PLAYBACK_SRC_ID_DEFAULT				    = "";
	public static final String				LAST_PLAYBACK_CONTAINER_ID_DEFAULT				    = "";

	SharedPreferences			preferences_;

	public SettingsManager(Activity parent) {
		preferences_ = parent.getSharedPreferences(Constants.SettingsFileName, Context.MODE_PRIVATE);
		LoadSettings();
	}

	public String getClientID() {
		return GetValueOrDefault(CLIENT_ID_KEY, "");
	}


	protected void LoadSettings() {
		// walk through all settings to check whether it exists in database
		if (!preferences_.contains(Settings.LANGUAGE_KEY))
			AddOrUpdateValue(Settings.LANGUAGE_KEY, Settings.LANGUAGE_DEFAULT);
		if (!preferences_.contains(Settings.AUDIO_TRACK_KEY))
			AddOrUpdateValue(Settings.AUDIO_TRACK_KEY, Settings.AUDIO_TRACK_DEFAULT);
		if (!preferences_.contains(Settings.BITRATE_KEY))
			AddOrUpdateValue(Settings.BITRATE_KEY, Settings.BITRATE_DEFAULT);
		if (!preferences_.contains(Settings.VIDEO_SCALING_KEY))
			AddOrUpdateValue(Settings.VIDEO_SCALING_KEY, Settings.VIDEO_SCALING_DEFAULT);
		if (!preferences_.contains(Settings.CH_SORT_MODE_KEY))
			AddOrUpdateValue(Settings.CH_SORT_MODE_KEY, Settings.CH_SORT_MODE_DEFAULT);
		if (!preferences_.contains(EPG_UPDATE_TIMESTAMP_KEY))
			AddOrUpdateValue(EPG_UPDATE_TIMESTAMP_KEY, EPG_UPDATE_TIMESTAMP_DEFAULT);
		if (!preferences_.contains(LAST_PLAYBACK_SRC_ID_KEY))
			AddOrUpdateValue(LAST_PLAYBACK_SRC_ID_KEY, LAST_PLAYBACK_SRC_ID_DEFAULT);
		if (!preferences_.contains(CLIENT_ID_KEY)) {
			java.util.UUID new_id = java.util.UUID.randomUUID();
			AddOrUpdateValue(CLIENT_ID_KEY, new_id.toString());
		}
		if (!preferences_.contains(Settings.TRANSCODING_ENABLED_KEY))
			AddOrUpdateValue(Settings.TRANSCODING_ENABLED_KEY, Settings.TRANSCODING_ENABLED_DEFAULT);
	}

	@Override
	protected void SaveSettings() {
	}

	@Override
	protected boolean AddOrUpdateValue(String key, Object value) {
		SharedPreferences.Editor editor = preferences_.edit();
		String svalue = value.toString();
		editor.putString(key, svalue);
		editor.apply();
		return true;
	}

	@SuppressWarnings("unchecked")
	@Override
	protected <T> T GetValueOrDefault(String key, T default_value) {
		T value = default_value;

		String svalue = this.preferences_.getString(key, "");
		if (svalue == "")
			return default_value;

		try {
            if (value instanceof String) {
                return (T) (Object) svalue;
            }
            if (value instanceof Boolean) {
                boolean ret = Boolean.parseBoolean(svalue);
                return (T) (Object) ret;
            }
            if (value instanceof Long) {
                long ret = Long.parseLong(svalue);
                return (T) (Object) ret;
            }
            if (value instanceof Integer) {
                int ret = Integer.parseInt(svalue);
                return (T) (Object) ret;
            }
            if (value instanceof Settings.EChannelSortMode) {
                Settings.EChannelSortMode e = Settings.EChannelSortMode.valueOf(svalue);
                return (T) (Object) e;
            }
            if (value instanceof Enum) {
                return (T) (Object) value;
            }
        } catch (Exception e) {
            value = default_value;
        }
		return value;
	}

}
