package com.dvblogic.tvmosaic;

import android.content.Context;
import android.graphics.Point;
import android.graphics.Typeface;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.dvblogic.dvblink_common.Utils;

import java.util.ArrayList;

import TVMosaic.TVMosaic.R;

public class ActionPane implements ActionOptionsPane.ActionOptionsPaneEventListener {

    public interface ActionPaneEventListener {
        void onActionPaneAction(int action_id);
        void onActionPaneOption(int option_id, String selected_option);
    }

    public static final int goto_pane_action_id_ = 99;

    public static final int schedules_pane_action_id_ = 102;
    public static final int settings_pane_action_id_ = 103;
    public static final int exit_pane_action_id_ = 104;
    public static final int timers_pane_action_id_ = 105;
    public static final int exit_and_save_pane_action_id_ = 106;

    public static final int guide_favorites_pane_action_id_ = 200;
    public static final int guide_sorting_pane_action_id_ = 201;
    public static final int guide_search_pane_action_id_ = 202;
    public static final int guide_refresh_pane_action_id_ = 203;
    public static final int guide_profiles_pane_action_id_ = 204;
    public static final int guide_recommendations_pane_action_id_ = 205;

    public static final int recordings_refresh_pane_action_id_ = 300;

    public static final int visibility_check_all_pane_action_id_ = 400;
    public static final int visibility_uncheck_all_pane_action_id_ = 401;
    public static final int visibility_search_action_id_ = 402;
    public static final int visibility_reset_search_action_id_ = 403;

    public static final int devices_scan_pane_action_id_ = 500;
    public static final int devices_settings_pane_action_id_ = 501;
    public static final int devices_reset_pane_action_id_ = 502;
    public static final int devices_delete_pane_action_id_ = 503;
    public static final int devices_rescan_pane_action_id_ = 504;

    public static final int epg_sources_settings_pane_action_id_ = 600;
    public static final int epg_sources_refresh_pane_action_id_ = 601;

    public static final int media_library_refresh_pane_action_id_ = 700;
    public static final int media_library_search_pane_action_id_ = 701;
    public static final int media_library_reset_search_pane_action_id_ = 702;

    public static final int user_id_ = 2000;

    public static final int action_pane_item_flag_do_not_hide_menu = 1;

    protected BaseActivity context_;
    protected LinearLayout layout_;
    PopupWindow popup_;
    ArrayList<ActionPaneItem> items_;
    ListView list_view_;
    ActionPaneEventListener event_listener_;
    Point dimensions_;
    ActionPane instance_;
    ActionPaneGroupAdapter adapter;

    final int empty_icon_id_ = -1;
    final int item_id_separator_ = -1;

    public class ActionPaneItem {

        ActionPaneItem() {
            id_ = item_id_separator_;
            left_icon_ = empty_icon_id_;
            left_icon_selected_ = empty_icon_id_;
            right_icon_ = empty_icon_id_;
            right_icon_selected_ = empty_icon_id_;
            options_ = null;
            flags_ = 0;
        }

        public int id_;
        public int left_icon_;
        public int left_icon_selected_;
        public String text_ = null;
        public String raw_text_ = null;
        public int right_icon_;
        public int right_icon_selected_;
        public ActionOptionsPaneItems options_;
        public boolean visible_ = true;
        public int flags_;
    }

    public class ActionPaneGroupAdapter extends BaseAdapter
    {
        private LayoutInflater layoutInflater;
        public ActionPaneGroupAdapter() {
            super();
            layoutInflater = (LayoutInflater)context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

        @SuppressWarnings("unchecked")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            ActionPaneItem api = items_.get(position);
            if (!api.visible_) {
                return layoutInflater.inflate(R.layout.null_item, null);
            }

            try {
                if (row == null || row.getId() == R.id.null_item) {
                    row = layoutInflater.inflate(R.layout.action_pane_row, list_view_, false);
                }

                ImageView left_icon = (ImageView) row.findViewById(R.id.action_pane_row_icon_left);
                ImageView right_icon = (ImageView) row.findViewById(R.id.action_pane_row_icon_right);
                TextView text = (TextView) row.findViewById(R.id.action_pane_row_icon_text);

                String translatedText = api.raw_text_ != null ? api.raw_text_ : context_.language_manager_.GetString(api.text_);
                text.setText(translatedText != null ? translatedText : api.text_);

                if (row.isSelected()) {
                    text.setTextColor(ContextCompat.getColor(context_, R.color.settings_text_selected));
                    text.setTextSize(TypedValue.COMPLEX_UNIT_PX, context_.getResources().getDimension(R.dimen.action_bar_text_size_selected));

                    if (api.left_icon_selected_ != empty_icon_id_)
                        left_icon.setImageResource(api.left_icon_selected_);

                    if (api.right_icon_selected_ != empty_icon_id_)
                        right_icon.setImageResource(api.right_icon_selected_);

                } else {
                    text.setTextColor(ContextCompat.getColor(context_, R.color.settings_text));
                    text.setTextSize(TypedValue.COMPLEX_UNIT_PX, context_.getResources().getDimension(R.dimen.action_bar_text_size));

                    if (api.left_icon_ != empty_icon_id_)
                        left_icon.setImageResource(api.left_icon_);

                    if (api.right_icon_ != empty_icon_id_)
                        right_icon.setImageResource(api.right_icon_);
                }

            } catch (Exception e) {
                Log.e("ActionPane", "getView", e);
            }

            return row;
        }

        @Override
        public boolean areAllItemsEnabled() {
            return false;
        }


        @Override
        public boolean isEnabled (int position) {

            if (items_.get(position).id_ == item_id_separator_ ||
                (items_.get(position).options_ != null && items_.get(position).options_.size() == 0) ||
                    !items_.get(position).visible_)
                return false;

            return true;
        }

        @Override
        public int getCount() {
            return items_.size();
        }

        @Override
        public Object getItem(int position) {
            return items_.get(position);
        }

        @SuppressWarnings("unchecked")
        @Override
        public long getItemId(int position) {
            ActionPaneItem api = (ActionPaneItem)getItem(position);
            return api.id_;
        }
    }

    public ActionPane(BaseActivity context, ActionPaneEventListener listener) {
        context_ = context;
        event_listener_ = listener;
        instance_ = this;

        create();
    }

    ActionOptionsPane aop;

    protected void create() {
        LayoutInflater layoutInflater = (LayoutInflater)context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout_ = (LinearLayout)layoutInflater.inflate(R.layout.action_pane, null);

        if (!Utils.usesRemoteControl(context_)) {
            int vert_padding = (int) context_.getResources().getDimension(R.dimen.action_bar_vert_padding_mobile);
            layout_.setPadding(layout_.getPaddingLeft(), vert_padding, layout_.getPaddingRight(), vert_padding);
        }

        popup_ = new PopupWindow(context_);
        popup_.setContentView(layout_);
        popup_.setFocusable(true);

        Display display = ((WindowManager) context_.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        dimensions_ = new Point();
        display.getSize(dimensions_);

        popup_.setHeight(dimensions_.y);
        dimensions_.x = (int)context_.getResources().getDimension(R.dimen.action_bar_width);
        popup_.setWidth(dimensions_.x);

        items_ = new ArrayList<ActionPaneItem>();

        fill_standard_pane_items();

        add_extra_pane_items();

        list_view_ = (ListView)layout_.findViewById(R.id.action_pane_items_list);
        adapter = new ActionPaneGroupAdapter();
        list_view_.setAdapter(adapter);

        list_view_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                ActionPaneItem api = (ActionPaneItem)items_.get(position);
                if (api.options_ == null) {
                    event_listener_.onActionPaneAction(api.id_);
                    if ((api.flags_ & action_pane_item_flag_do_not_hide_menu) == 0)
                        popup_.dismiss();

                } else {
                    aop = new ActionOptionsPane(context_, instance_, api.options_, api.id_, dimensions_);
                    aop.show();
                }
            }
        });

        list_view_.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                list_view_.invalidateViews();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        list_view_.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Common.backPressed(keyCode, event)) {
                    popup_.dismiss();
                    return true;
                }
                return false;
            }
        });

    }

    public void show() {
        popup_.showAtLocation(context_.getWindow().getDecorView(), Gravity.NO_GRAVITY, 0, 0);

    }

    public void onActionPaneOption(int option_id, String selected_option) {
        event_listener_.onActionPaneOption(option_id, selected_option);
    }

    public void hide() {
        hide_nested_menu();
        popup_.dismiss();
    }

    public void hide_nested_menu() {
        if (aop != null) {
            aop.hide();
        }
    }

    protected void fill_standard_pane_items() {
        //goto item
        ActionPaneItem goto_api = new ActionPaneItem();
        goto_api.text_ = LocaleStrings.IDS_TOP_LEVEL_NAVIGATE_TO;
        goto_api.id_ = goto_pane_action_id_;
        goto_api.left_icon_ = R.drawable.tvm_navigate_icon_normal;
        goto_api.left_icon_selected_ = R.drawable.tvm_navigate_icon_hover;
        goto_api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
        goto_api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
        goto_api.flags_ = action_pane_item_flag_do_not_hide_menu;
        items_.add(goto_api);
    }

    protected void add_extra_pane_items() {

        //separator
//        ActionPaneItem sep_api = new ActionPaneItem();
//        items_.add(0, sep_api);

        //add guide specific items from bottom to top

        //settings
        ActionPaneItem settings_api = new ActionPaneItem();
        settings_api.text_ = LocaleStrings.IDS_SETTINGS_PAGE_TITLE;
        settings_api.id_ = settings_pane_action_id_;
        settings_api.left_icon_ = R.drawable.tvm_settings_icon_normal;
        settings_api.left_icon_selected_ = R.drawable.tvm_settings_icon_hover;
        items_.add(0, settings_api);

        //refresh
        ActionPaneItem refresh_api = new ActionPaneItem();
        refresh_api.text_ = LocaleStrings.IDS_REFRESH;
        refresh_api.id_ = guide_refresh_pane_action_id_;
        refresh_api.left_icon_ = R.drawable.tvm_refresh_icon_normal;
        refresh_api.left_icon_selected_ = R.drawable.tvm_refresh_icon_hover;
        items_.add(0, refresh_api);

        //search
        ActionPaneItem search_api = new ActionPaneItem();
        search_api.text_ = LocaleStrings.IDS_SEARCH_PROGRAMS_PAGE_TITLE;
        search_api.id_ = guide_search_pane_action_id_;
        search_api.left_icon_ = R.drawable.tvm_search_icon_normal;
        search_api.left_icon_selected_ = R.drawable.tvm_search_icon_hover;
        items_.add(0, search_api);

        //Favorites
        ActionPaneItem favorites_api = new ActionPaneItem();
        favorites_api.id_ = guide_favorites_pane_action_id_;
        favorites_api.text_ = LocaleStrings.IDS_FAVORITES;
        favorites_api.left_icon_ = R.drawable.tvm_favorites_icon_normal;
        favorites_api.left_icon_selected_ = R.drawable.tvm_favorites_icon_hover;
        favorites_api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
        favorites_api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
        favorites_api.options_ = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX);
        //actual favorites will be filled in externally
        items_.add(0, favorites_api);

        //Recommendation List
        ActionPaneItem recommendations_api = new ActionPaneItem();
        recommendations_api.text_ = LocaleStrings.IDS_LIST_MODE;
        recommendations_api.id_ = guide_recommendations_pane_action_id_;
        recommendations_api.left_icon_ = R.drawable.tvm_listview_icon_normal;
        recommendations_api.left_icon_selected_ = R.drawable.tvm_listview_icon_hover;
        recommendations_api.visible_ = false;
        items_.add(0, recommendations_api);

        //Profiles
        ActionPaneItem profiles_api = new ActionPaneItem();
        profiles_api.id_ = guide_profiles_pane_action_id_;
        profiles_api.text_ = LocaleStrings.IDS_PROFILES;
        profiles_api.left_icon_ = R.drawable.tvm_profiles_icon_normal;
        profiles_api.left_icon_selected_ = R.drawable.tvm_profiles_icon_hover;
        profiles_api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
        profiles_api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
        profiles_api.options_ = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX);
        profiles_api.visible_ = false;
        //actual profiles will be filled in externally
        items_.add(0, profiles_api);

        //Sorting
        ActionPaneItem sorting_api = new ActionPaneItem();
        sorting_api.id_ = guide_sorting_pane_action_id_;
        sorting_api.text_ = LocaleStrings.IDS_SORTING;
        sorting_api.left_icon_ = R.drawable.tvm_sort_icon_normal;
        sorting_api.left_icon_selected_ = R.drawable.tvm_sort_icon_hover;
        sorting_api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
        sorting_api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
        sorting_api.options_ = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON);
        //actual favorites will be filled in externally
        items_.add(0, sorting_api);
    }

    public void set_options(int option_id, ActionOptionsPaneItems options) {

        for (int i=0; i<items_.size(); i++) {
            if (items_.get(i).id_ == option_id) {
                items_.get(i).options_ = options;
                break;
            }
        }
    }

    public ActionOptionsPaneItems get_options(int option_id) {
        for (int i=0; i<items_.size(); i++) {
            if (items_.get(i).id_ == option_id) {
                return items_.get(i).options_;
            }
        }
        return null;
    }

    public void update_nested_items () {
        if (aop != null) {
            ((BaseAdapter) aop.list_view_.getAdapter()).notifyDataSetChanged();
        }
    }

    public void setItemVisibility(int option_id, boolean visibility) {
        for (int i=0; i<items_.size(); i++) {
            if (items_.get(i).id_ == option_id) {
                items_.get(i).visible_ = visibility;
                break;
            }
        }
        adapter.notifyDataSetChanged();
    }

}
