package com.dvblogic.tvmosaic;

import android.content.Context;
import android.graphics.Point;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.util.TypedValue;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.dvblogic.dvblink_common.Utils;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.HashMap;

import TVMosaic.TVMosaic.R;

public class ActionOptionsPane implements SearchView.OnQueryTextListener {
private static final String TAG = "ActionOptionsPane";

    public interface ActionOptionsPaneEventListener {
        public void onActionPaneOption(int option_id, String selected_option);
    }

    protected BaseActivity context_;
    protected LinearLayout layout_;
    PopupWindow popup_;
    ListView list_view_;
    ActionOptionsPaneEventListener event_listener_;
    Point dimensions_;
    int option_id_;
    ActionOptionsPaneItems options_;
    HashMap<Integer, Integer> visible_options_map_ = new HashMap<Integer, Integer>();

    public class ActionPaneGroupAdapter extends BaseAdapter
    {
        public ActionPaneGroupAdapter() {
            super();
        }

        @SuppressWarnings("unchecked")
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View row = convertView;

            try {
                if (row == null) {
                    LayoutInflater layoutInflater = (LayoutInflater)context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    row = layoutInflater.inflate(R.layout.action_options_pane_row, list_view_, false);
                }

                ActionOptionsPaneItems.ActionOptionsPaneItem api = get_option_from_pos(position);

                ImageView icon = (ImageView) row.findViewById(R.id.action_options_pane_row_icon);

                boolean is_checked;
                if (options_.is_multiselect()) {
                    is_checked = options_.is_selected(api.id_);
                } else {
                    is_checked = !options_.get_selected_item_id().equals(ActionOptionsPaneItems.empty_selected_option_id_) && api.id_.equals(options_.get_selected_item_id());
                }

                TextView text = (TextView) row.findViewById(R.id.action_options_pane_row_text);
                text.setText(api.text_);

                if (row.isSelected()) {

                    if (is_checked)
                        icon.setImageResource(R.drawable.tvm_checked_icon_hover);
                    else
                        icon.setImageResource(R.drawable.tvm_unchecked_icon_hover);

                    text.setTextColor(ContextCompat.getColor(context_, R.color.settings_text_selected));
                    text.setTextSize(TypedValue.COMPLEX_UNIT_PX, context_.getResources().getDimension(R.dimen.action_bar_text_size_selected));
                } else {

                    if (is_checked)
                        icon.setImageResource(R.drawable.tvm_checked_icon_normal);
                    else
                        icon.setImageResource(R.drawable.tvm_unchecked_icon_normal);

                    text.setTextColor(ContextCompat.getColor(context_, R.color.settings_text));
                    text.setTextSize(TypedValue.COMPLEX_UNIT_PX, context_.getResources().getDimension(R.dimen.action_bar_text_size));
                }
            } catch (Exception e) {
            }
            return row;
        }

        public ActionOptionsPaneItems.ActionOptionsPaneItem get_option_from_pos(int pos) {
            ActionOptionsPaneItems.ActionOptionsPaneItem api = null;
            if (visible_options_map_.containsKey(pos)) {
                Integer idx = visible_options_map_.get(pos);
                if (idx < options_.size())
                    api = options_.get(idx);
            }

            return api;
        }

        @Override
        public int getCount() {
            return visible_options_map_.size();
        }

        @Override
        public Object getItem(int position) {
            return get_option_from_pos(position);
        }

        @SuppressWarnings("unchecked")
        @Override
        public long getItemId(int position) {
            return position;
        }
    }

    ActionPaneGroupAdapter adapter_ = null;

    public ActionOptionsPane(BaseActivity context, ActionOptionsPaneEventListener listener, ActionOptionsPaneItems options, int option_id, Point dimensions) {
        context_ = context;
        event_listener_ = listener;
        options_ = options;
        option_id_ = option_id;

        build_visible_options_map("");

        if (dimensions == null) {
            Display display = ((WindowManager) context_.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
            dimensions_ = new Point();
            display.getSize(dimensions_);
            dimensions_.x = (int) context_.getResources().getDimension(R.dimen.action_bar_width);
        } else {
            dimensions_ = dimensions;
        }

        create();
    }

    protected void create() {

        LayoutInflater layoutInflater = (LayoutInflater)context_.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        layout_ = (LinearLayout)layoutInflater.inflate(R.layout.action_options_pane, null);

        if (!Utils.usesRemoteControl(context_)) {
            int vert_padding = (int) context_.getResources().getDimension(R.dimen.action_bar_vert_padding_mobile);
            layout_.setPadding(layout_.getPaddingLeft(), vert_padding, layout_.getPaddingRight(), vert_padding);
        }

        popup_ = new PopupWindow(context_);
        popup_.setContentView(layout_);
        popup_.setFocusable(true);

        popup_.setHeight(dimensions_.y);
        popup_.setWidth(dimensions_.x);

        SearchView sv = (SearchView)layout_.findViewById(R.id.search_view);
        sv.setOnQueryTextListener(this);

        if (!Utils.usesRemoteControl(context_)) {
            sv.setIconified(false);
            sv.setIconifiedByDefault(false);
            sv.setOnCloseListener(new SearchView.OnCloseListener() {
                @Override
                public boolean onClose() {
                    return true; //returning true will stop search view from collapsing
                }
            });
        }

        //only show search if number of items is more than a certain amount
        if (options_.size() > 16)
            sv.setVisibility(View.VISIBLE);

        list_view_ = (ListView)layout_.findViewById(R.id.action_options_pane_items_list);

        adapter_ = new ActionPaneGroupAdapter();
        list_view_.setAdapter(adapter_);

        list_view_.setItemsCanFocus(true);

        list_view_.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String current_id = adapter_.get_option_from_pos(position).id_;
                String selected_id = options_.get_selected_item_id();

                if (options_.get_select_mode() == ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON) {
                    //there is always a selection
                    options_.set_selected_item_id(current_id);
                } else {
                    if (options_.is_multiselect()) {
                        options_.toggle_selection(current_id);
                        options_.set_selected_item_id(current_id);
                    } else {
                        //selection may become empty if currently selected item is selected again
                        if (selected_id.equals(ActionOptionsPaneItems.empty_selected_option_id_)) {
                            //if there is no selection currently - select new item
                            options_.set_selected_item_id(current_id);
                        } else {
                            if (selected_id.equals(current_id)) {
                                //reset selection
                                options_.set_selected_item_id(ActionOptionsPaneItems.empty_selected_option_id_);
                            } else {
                                //select a new item
                                options_.set_selected_item_id(current_id);
                            }
                        }
                    }
                }
                list_view_.invalidateViews();
                //TODO: animation?
                event_listener_.onActionPaneOption(option_id_, options_.get_selected_item_id());
            }
        });

        list_view_.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                list_view_.invalidateViews();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        list_view_.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (Common.backPressed(keyCode, event)) {
                    popup_.dismiss();
                    return true;
                }
                return false;
            }
        });

        list_view_.requestFocus();
    }

    public void show() {
        popup_.showAtLocation(context_.getWindow().getDecorView(), Gravity.NO_GRAVITY, 0, 0);

    }

    public void hide() { popup_.dismiss(); }

    public ArrayList<String> get_multiselected_items_id() {
        if (!options_.is_multiselect()) {
            throw new InvalidParameterException("required multiselect");
        }
        return options_.get_selected_items_id();
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        Log.d(TAG, "onQueryTextChange: " + newText);

        build_visible_options_map(newText);
        adapter_.notifyDataSetChanged();
        return true;
    }

    protected void build_visible_options_map(String filter) {
        visible_options_map_.clear();

        String lc_filter = filter.toLowerCase();
        int count = 0;
        for (int i=0; i<options_.size(); i++) {
            if (filter.isEmpty()) {
                visible_options_map_.put(i, i);
            } else {
                if (options_.get(i).text_.toLowerCase().contains(lc_filter)) {
                    visible_options_map_.put(count, i);
                    count += 1;
                }
            }
        }
    }

}
