package com.dvblogic.tvmosaic;

import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Point;
import android.text.format.DateFormat;
import android.view.Display;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import java.lang.reflect.Field;

import TVMosaic.TVMosaic.R;

public class CustomTimePickerDialog extends TimePickerDialog
{
    final int picker_color = Color.TRANSPARENT;

    Context _context;
    TimePickerDialog dlg;
    TimePicker _timePicker;

	public CustomTimePickerDialog(Context context, TimePickerDialog.OnTimeSetListener listener, int hour, int minute, boolean is24HourView) {
        super(context, listener, hour, minute, is24HourView);
        _context = context;
        dlg = this;

        try {
            Field field = TimePickerDialog.class.getDeclaredField("mTimePicker");
            field.setAccessible(true);
            _timePicker = (TimePicker)field.get(this);
        } catch (Exception s) {}

        try {
            int id = _context.getResources().getIdentifier("android:id/minute", null, null);
            final NumberPicker minutePicker = (NumberPicker) _timePicker.findViewById(id);
            if (minutePicker != null) {
                minutePicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus)
                            minutePicker.setBackgroundColor(_context.getResources().getColor(R.color.date_picker_focused));
                        else
                            minutePicker.setBackgroundColor(picker_color);
                    }
                });
            }
            id = _context.getResources().getIdentifier("android:id/hour", null, null);
            final NumberPicker hourPicker = (NumberPicker) _timePicker.findViewById(id);
            if (hourPicker != null) {
                hourPicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus)
                            hourPicker.setBackgroundColor(_context.getResources().getColor(R.color.date_picker_focused));
                        else
                            hourPicker.setBackgroundColor(picker_color);
                    }
                });
                hourPicker.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                            View btn = dlg.getButton(DialogInterface.BUTTON_POSITIVE);
                            if (btn != null)
                                btn.requestFocus();
                            return true;
                        }
                        return false;
                    }
                });
            }
            id = _context.getResources().getIdentifier("android:id/amPm", null, null);
            final NumberPicker amPicker = (NumberPicker) _timePicker.findViewById(id);
            if (amPicker != null) {
                amPicker.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus)
                            amPicker.setBackgroundColor(_context.getResources().getColor(R.color.date_picker_focused));
                        else
                            amPicker.setBackgroundColor(picker_color);
                    }
                });
            }

            NumberPicker picker = is24HourView ? minutePicker : amPicker;
            if (picker != null) {
                picker.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                            View btn = dlg.getButton(DialogInterface.BUTTON_NEGATIVE);
                            if (btn != null)
                                btn.requestFocus();
                            return true;
                        }
                        return false;
                    }
                });
            }

        } catch (Exception e) {
           e.printStackTrace();
        }
	}
}
