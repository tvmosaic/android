package com.dvblogic.tvmosaic;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.ChannelIDList;
import com.dvblogic.dvblink_common.Schedule;
import com.dvblogic.dvblink_common.SchedulesList;
import com.dvblogic.dvblink_common.SchedulesRequest;
import com.dvblogic.dvblink_common.Serializer;
import com.dvblogic.dvblink_common.StatusCode;
import com.dvblogic.dvblink_common.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import TVMosaic.TVMosaic.R;

public class SchedulesActivity extends BaseActivity
{
    final int schedule_props_activity_req_code      = 100;
    final int manual_schedule_activity_req_code     = 101;


    private static final int state_ready      = 1;
    private static final int state_requesting_schedules      = 2;

    private static final String TAG = "SchedulesActivity";
    private static final String addManualScheduleId = "81b63c2c-d389-11e7-9296-cec278b6b50a";

    private class ResultsAdapter extends BaseAdapter {
        private ArrayList<Schedule> schedules = new ArrayList<>();

        private LayoutInflater ltInflater;

        private View.OnClickListener resultItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ShowScheduleDetails((int) view.getTag());
            }
        };
        private View.OnKeyListener resultItemKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && Common.okPressed(keyEvent.getKeyCode())) {
                    ShowScheduleDetails((int) view.getTag());
                    return true;
                }
                return false;
            }
        };

        protected void ShowScheduleDetails(int position) {
            Schedule sch = schedules.get(position);
            if (sch.ScheduleID.equals(addManualScheduleId)) {
                //start add manual schedule activity
                startManualScheduleActivity();
            } else {
                showScheduleProps(sch);
            }
        }

        public ResultsAdapter() {
            ltInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return schedules.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ltInflater.inflate(R.layout.settings_link_layout, parent, false);
            }
            Schedule sch = schedules.get(position);

            String first_line  = "";
            String second_line  = "";

            if (sch.ScheduleID.equals(addManualScheduleId)) {
                first_line = language_manager_.GetString(LocaleStrings.IDS_ADD_SCHEDULE);
                second_line = language_manager_.GetString(LocaleStrings.IDS_MANUAL_SCHEDULE_DESC);
            } else {
                if (sch.isManual()) {
                    first_line = sch.Manual.Title;
                    second_line = getScheduleDescriptionString(sch);
                } else if (sch.isEpg()) {
                    first_line = sch.ByEpg.Program.Title;
                    second_line = getScheduleDescriptionString(sch);
                } else if (sch.isPatern()) {
                    if (!sch.ByPattern.KeyPhrase.isEmpty())
                        first_line = "*" + sch.ByPattern.KeyPhrase + "*";

                    if (sch.ByPattern.GenreMask != Genres.RDGC_ANY) {
                        if (!first_line.isEmpty())
                            first_line += ", ";

                        first_line += Genres.maskToString(sch.ByPattern.GenreMask);
                    }

                    second_line = getScheduleDescriptionString(sch);
                }
            }

            int textColor = getResources().getColor(R.color.new_text_primary_active);

            if (sch.isV2Schedule() && !sch.Active)
                textColor = getResources().getColor(R.color.new_text_disabled);

            TextView label;
            label = (TextView) view.findViewById(R.id.title_text);
            label.setText(first_line);
            label.setTextColor(textColor);

            label = (TextView) view.findViewById(R.id.desc_text);
            label.setText(second_line);
            label.setTextColor(textColor);

            view.setTag(position);
            view.setOnClickListener(resultItemClickListener);
            view.setOnKeyListener(resultItemKeyListener);

            return view;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return schedules.get(position);
        }

        public void addSchedule(Schedule sch) {
            schedules.add(sch);
        }

        public void Clear() {
            schedules.clear();
        }
    }

    private ListView results;
    private SchedulesActivity.ResultsAdapter resultsAdapter;
    int state_ = state_ready;
    private ChannelIDList channels_to_refresh = new ChannelIDList();

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.schedules_activity);

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(language_manager_.GetString(LocaleStrings.IDS_SCHEDULES));

        View root = findViewById(R.id.schedules_layout);
        results = (ListView) root.findViewById(R.id.schedule_list);

        resultsAdapter = new SchedulesActivity.ResultsAdapter();
        results.setAdapter(resultsAdapter);
        results.setItemsCanFocus(true);

        Genres.init(language_manager_);

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                requestSchedules();
            }
        });
    }

    @Override
    public void onBackPressed() {

        if (channels_to_refresh.size() > 0) {
            Intent intent = getIntent();

            String channels_xml = Serializer.serialize(channels_to_refresh);
            intent.putExtra("need_channel_update", channels_xml);
            setResult(RESULT_OK, intent);
        }

        finish();
    }

    protected boolean IsV2Schedules(SchedulesList sl) {
        boolean ret_val = true;

        for (int i=0; i<sl.size(); i++) {
            if (!sl.get(i).isV2Schedule()) {
                ret_val = false;
                break;
            }
        }

        return ret_val;
    }

    protected void requestSchedules() {
        activateProgressBar(true);
        state_ = state_requesting_schedules;

        SchedulesRequest sr = new SchedulesRequest();
        data_provider_.GetSchedules(sr);
    }

    private void processSchedules() {
        SchedulesList sl = (SchedulesList)response_;

        resultsAdapter.Clear();

        Schedule addManualScheduleItem = new Schedule();
        addManualScheduleItem.ScheduleID = addManualScheduleId;
        resultsAdapter.addSchedule(addManualScheduleItem);

        for (Schedule sch : sl.list()) {
            resultsAdapter.addSchedule(sch);
        }

        if (sl.size() == 0)
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_NO_SCHEDULES_FOUND), Toast.LENGTH_SHORT).show();

        if (!IsV2Schedules(sl))
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_NOT_V2), Toast.LENGTH_SHORT).show();

        resultsAdapter.notifyDataSetChanged();

        state_ = state_ready;
        activateProgressBar(false);
    }

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;

        if (state_ == state_requesting_schedules) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processSchedules();
                }
            });
        }
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        http_error_ = error;
        runOnUiThread(new Runnable() {
            public void run() {
                ProcessDataProviderError();
            }
        });
    }

    protected void ProcessDataProviderError() {
        try {
            String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

            state_ = state_ready;
            activateProgressBar(false);
        } catch (Exception e) {
        }
    }

    public void refresh() {
        requestSchedules();
    }

    protected void showScheduleProps(Schedule sch) {
        Intent i = new Intent();
        i.setClass(this, SchedulePropertiesActivity.class);
        i.putExtra("schedule_id", sch.ScheduleID);
        startActivityForResult(i, schedule_props_activity_req_code);
    }

    protected void startManualScheduleActivity() {
        Intent i = new Intent();
        i.setClass(this, ManualScheduleActivity.class);
        startActivityForResult(i, manual_schedule_activity_req_code);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {

            if (data != null) {
                String channels_xml = data.getStringExtra("need_channel_update");
                if (channels_xml != null) {
                    ChannelIDList channels = (ChannelIDList) Serializer.deserialize(
                            channels_xml,
                            ChannelIDList.class
                    );
                    for (int i=0; i<channels.size(); i++) {
                        if (!channels_to_refresh.list().contains(channels.get(i)))
                            channels_to_refresh.add(channels.get(i));
                    }
                }
            }

            refresh();
        }

    }

    protected String addActivePriorityTexts(Schedule sch, String ret_val) {
        if (sch.isV2Schedule()) {
            //active/inactive
            ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_ACTIVE) + ": " + SchedulePropertiesActivity.getActiveString(this, sch.Active);
            //priority
            ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_PRIORITY) + ": " + SchedulePropertiesActivity.getPriorityString(this, sch.Priority);
        }
        return ret_val;
    }

    protected String addWeekdayStartKeepStrings(Schedule sch, String ret_val) {
        if (sch.isV2Schedule()) {
            //weekdays
            ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_WEEKDAYS) + ": " + SchedulePropertiesActivity.getWeekdayRepeatString(this, sch.getDayMask());
            //starts after
            ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_STARTS_AFTER) + ": " + SchedulePropertiesActivity.getStartBeforeAfterString(this, sch.getStartAfter());
            //starts before
            ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_STARTS_BEFORE) + ": " + SchedulePropertiesActivity.getStartBeforeAfterString(this, sch.getStartBefore());
        }
        //keep recordings
        ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_KEEP_ITEMS) + ": " + SchedulePropertiesActivity.getKeepNoMoreString(this, sch.recordingsToKeep());

        return ret_val;
    }

    protected String addMarginsTexts(Schedule sch, String ret_val) {
        ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_START_MARGIN) + ": " + SchedulePropertiesActivity.getStartMarginString(this, sch.MargineBefore / 60);
        ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_END_MARGIN) + ": " + SchedulePropertiesActivity.getEndMarginString(this, sch.MargineAfter / 60);
        return ret_val;
    }

    protected String getScheduleDescriptionString(Schedule sch) {
        String ret_val = "";

        if (sch != null) {

            if (sch.isManual()) {
                ret_val = language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_MANUAL);

                ret_val = addActivePriorityTexts(sch, ret_val);

                //start - end date/time
                if (sch.isRepeat())
                    ret_val += ", " + Utils.UtcToShortTimeString(this, sch.Manual.StartTime) + " - " + Utils.UtcToShortTimeString(this, sch.Manual.StartTime + sch.Manual.Duration);
                else
                    ret_val += ", " + Utils.UtcToShortDateString(this, sch.Manual.StartTime) + " " + Utils.UtcToShortTimeString(this, sch.Manual.StartTime) + " - " +
                            Utils.UtcToShortDateString(this, sch.Manual.StartTime + sch.Manual.Duration) + " " + Utils.UtcToShortTimeString(this, sch.Manual.StartTime + sch.Manual.Duration);
                //start / end margins
                ret_val = addMarginsTexts(sch, ret_val);

                if (sch.isRepeat()) {
                    //weekdays
                    ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_REPEAT) + ": " + ManualScheduleActivity.getRepeatString(this, sch.getDayMask());
                    //keep recordings
                    ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_KEEP_ITEMS) + ": " + SchedulePropertiesActivity.getKeepNoMoreString(this, sch.recordingsToKeep());
                }
            } else
            if (sch.isEpg()) {
                ret_val = language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_EPG);
                ret_val = addActivePriorityTexts(sch, ret_val);

                //single / repeat recording
                ret_val += ", " + (sch.isRepeat() ? language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_REPEATING) : language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_RECORD_ONCE));
                //start / end margins
                ret_val = addMarginsTexts(sch, ret_val);

                if (sch.isRepeat()) {
                    //new only
                    ret_val += ", " + language_manager_.GetString(LocaleStrings.IDS_NEW_ONLY) + ": " + SchedulePropertiesActivity.getNewOnlyString(this, sch.ByEpg.NewOnly);
                    ret_val = addWeekdayStartKeepStrings(sch, ret_val);
                }
            } else {
                ret_val = language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_PATTERN);
                ret_val = addActivePriorityTexts(sch, ret_val);
                //start / end margins
                ret_val = addMarginsTexts(sch, ret_val);

                ret_val = addWeekdayStartKeepStrings(sch, ret_val);
            }
        }
        return ret_val;
    }


}
