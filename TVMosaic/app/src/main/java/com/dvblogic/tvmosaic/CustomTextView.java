package com.dvblogic.tvmosaic;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class CustomTextView extends TextView
{
	public CustomTextView(Context context) {
		super(context);

		setTypeface(CustomFont.getInstance(context).getTypeFace());
	}

	public CustomTextView(Context context, AttributeSet attrs) {
		super(context, attrs);

		set_font_attr(attrs);
	}

	public CustomTextView(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);

		set_font_attr(attrs);
	}

	protected void set_font_attr(AttributeSet attrs) {
		String text_style_attr = attrs.getAttributeValue("http://schemas.android.com/apk/res/android", "textStyle");
		if (text_style_attr != null) {
			try {
				if (Integer.decode(text_style_attr) == Typeface.BOLD_ITALIC) {
					setTypeface(CustomFont.getInstance(getContext()).getTypeFace_BoldItalic());
				} else if (Integer.decode(text_style_attr) == Typeface.BOLD) {
					setTypeface(CustomFont.getInstance(getContext()).getTypeFace_Bold());
				} else if (Integer.decode(text_style_attr) == Typeface.ITALIC) {
					setTypeface(CustomFont.getInstance(getContext()).getTypeFace_Italic());
				} else {
					setTypeface(CustomFont.getInstance(getContext()).getTypeFace());
				}
			} catch (Exception e) {
				setTypeface(CustomFont.getInstance(getContext()).getTypeFace());
			}
		}
	}
}
