package com.dvblogic.tvmosaic;

import java.util.*;

import android.app.*;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.*;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;

import android.support.v7.app.MediaRouteButton;

import TVMosaic.TVMosaic.R;

public class GuideActivity extends BaseActivity implements ScrollViewListener, OnTouchListener, OnClickListener, ActionPane.ActionPaneEventListener, IBackgroundEPGUpdater
{
    private static final String TAG = "GuideActivity";
    private static final String MENU_LIKE_THIS_OPTION_ID = "b293c547-13ff-45e4-bc85-c8d863ca0a3d";
    private static final int maximum_visible_channels_num = 500;
    private final long  epg_update_interval_sec          = 6*3600;

    //states
    public static int EGAS_NONE = 1000;
    public static int EGAS_REQUESTING_STREAM_CAPS = 1001;
    public static int EGAS_REQUESTING_CHANNELS = 1002;
    public static int EGAS_REQUESTING_FAVORITES = 1003;
    public static int EGAS_REQUESTING_DATE_EPG = 1004;
    public static int EGAS_REQUESTING_DIRECT_URL = 1005;
    public static int EGAS_REQUESTING_OOB_URL = 1006;
    public static int EGAS_READY = 1007;

    //activity request codes
    final int   program_activity_req_code       = 100;
    final int   play_activity_req_code          = 101;
    final long  search_activity_req_code        = 102;
    final int   settings_activity_req_code      = 103;
    final long  list_activity_req_code          = 104;
    final int   scan_activity_req_code          = 105;

    final int   sec_per_hour = 3600;
    final int   seconds_per_day = (60*60*24);
    final long  epg_loading_step = 12 * sec_per_hour;
    final long  guidebar_length = 3 * 24 * sec_per_hour; // 3 days
    final long  epg_start_offset = 30*60;
    final int number_of_visible_channels = 5;
    final int background_epg_update_timeout_sec_ = 5*60; //every 5 minutes
    int         layout_item_height              = 48;
    long        requested_start_time = 0;
    long        requested_end_time = 0;
    boolean     first_launch_ = false;

    private boolean tunerNotConnectedDialogShowed = false;
    private boolean needPlayLastPlayedChannel = false;
    private boolean needPlayFirstChannel = false;

    private GuideAdviserProvider guideAdviserProvider;
    private MappedChannelsResponse mappedChannels = null;
    private ArrayList<ProfileRecommendation> profiles = new ArrayList<ProfileRecommendation>();
    private String newSelectedProfile = "";
    private boolean likeThisMode = false;
    private String likeThisChannelID;
    private long likeThisStartTime;
    private long likeThisEndTime;
    boolean can_record_ = false;

    class ScrollerData
    {
        public ScrollerData() {
            reset();
        }

        public void reset() {
            active = false;
            start_pos = -1;
            scroll_pos = -1;
            stop_pos = -1;
        }

        public boolean  active;
        public int      start_pos;
        public int      scroll_pos;
        public int      stop_pos;
    }

    class ChannelRef
    {
        public ChannelRef() {
        }

        public ChannelItem  channel_item;
        public LinearLayout epg_container;
    }

    class ProgramDetailsLoader implements OnBitmapLoadFinished, IDataProvider
    {
        Activity activity;
        GuideBar bar = null;
        GuideBar.GuideBarItem item = null;
        String channel_id = null;
        String channel_name = null;
        Thread program_details_thread;
        protected Object server_response_;
        protected ScanChannelsDataProvider server_data_provider_;


        public ProgramDetailsLoader(Activity _activity) {
            activity = _activity;

            //initialize own server data provider
            try {
                server_data_provider_ = new ScanChannelsDataProvider(serverSettings, this);
                server_data_provider_.setLanguage(language_manager_.getID());
            } catch (Exception e) {
                server_data_provider_ = null;
            }
        }

        void release() {
            skipProgramDetailsRequest();
        }

        void set_program(String _channel_id, GuideBar _bar, GuideBar.GuideBarItem _item) {
            channel_id = _channel_id;
            bar = _bar;
            item = _item;
            channel_name = channel_display_map.get(channel_id).channel_item.channel_.Name;

            fill();

            if (item != null && !item.is_empty_)
                requestProgramDetails();
        }

        public void DataProcessing(String command, Object result_data) {
            server_response_ = result_data;
            skipTimeoutThread();
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    processProgramDetails();
                    server_response_ = null;
                }
            });

        }

        public void ErrorProcessing(String command, StatusCode error) {

        }


        @Override
        public void loadFinished() {
        }

        protected void skipProgramDetailsRequest() {
            if (program_details_thread != null && !program_details_thread.isInterrupted()) {
                Thread thread = program_details_thread;
                program_details_thread = null;
                thread.interrupt();
            }
        }

        protected void requestProgramDetails() {
            program_details_thread = new Thread(new Runnable() {
                public void run() {
                    try {
                        Thread.sleep(100);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                EpgSearcher es = new EpgSearcher(channel_id, item.prg_.ID);
                                server_data_provider_.SearchEpg(es);
                            }
                        });
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            });
            program_details_thread.start();
        }

        protected void processProgramDetails() {
            try {
                if (server_response_ != null) {
                    ChannelIdWithProgramsList epg_data = (ChannelIdWithProgramsList) server_response_;
                    Program program_ = getProgram(epg_data);
                    if (program_ != null) {
                        // thumbnail
                        ImageView iv = (ImageView) activity.findViewById(R.id.description_image);
                        boolean load_bitmap = (program_.Image != null && !program_.Image.isEmpty());
                        if (load_bitmap) {
                            new com.dvblogic.tvmosaic.BitmapLoader(activity, this, program_.Image,
                                    (ImageView) activity.findViewById(R.id.description_image),
                                    null).start();
                        }

                        ProgramDetails info = (ProgramDetails) activity.findViewById(R.id.program_details_pane);
                        info.setInfo(program_, channel_name, -1, isAdviserMode(), program_.IsRecordConflict, language_manager_);
                    } else {
                        System.out.println("ProgramActivity.processProgramDetails(): program_ == null");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        protected Program getProgram(ChannelIdWithProgramsList epg_data) {
            if (epg_data.size() == 0)
                return null;
            for (ChannelIdWithPrograms cp : epg_data.list()) {
                for (Program prg : cp.Programs.list()) {
                    if (prg.ID.equals(item.prg_.ID))
                        return prg;
                }
            }
            return null;
        }

        protected void fill() {
            ProgramDetails pi = (ProgramDetails) findViewById(R.id.program_details_pane);
            pi.findViewById(R.id.description_image).setVisibility(View.INVISIBLE);

            if (item != null) {
                if (item.is_empty_) {
                    pi.setInfo(item.prg_, channel_name);
                } else {
                    RecommendationProgram recommendationProgram = bar.recommendation_map.get(item.prg_.ID);
                    float score = (recommendationProgram != null) ? recommendationProgram.Score : 0.0f;
                    pi.setInfo(item.prg_,
                            channel_name,
                            score,
                            isAdviserMode(),
                            item.is_conflict,
                            language_manager_
                    );
                }
            } else {
                pi.clear("");
            }
        }

        void clear() {
            ProgramDetails pi = (ProgramDetails) findViewById(R.id.program_details_pane);
            pi.findViewById(R.id.description_image).setVisibility(View.INVISIBLE);
            pi.clear("");
        }

        void refresh() {
            fill();
        }
    }

    private class TVMSessionManagerListener implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionEnded(CastSession session, int error) {
            if (session == mCastSession) {
                mCastSession = null;
            }
        }

        @Override
        public void onSessionResumed(CastSession session, boolean wasSuspended) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarted(CastSession session, String sessionId) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarting(CastSession session) {
        }

        @Override
        public void onSessionStartFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionEnding(CastSession session) {
        }

        @Override
        public void onSessionResuming(CastSession session, String sessionId) {
        }

        @Override
        public void onSessionResumeFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionSuspended(CastSession session, int reason) {
        }
    }

    private static GuideActivity _activity;
    View search_icon = null;
    MotionEvent                         prev_motion_event_          = null;
    int                 state_;
    HashMap<String, ChannelRef>         channel_display_map = new HashMap<String, GuideActivity.ChannelRef>();
    ArrayList<String>                   channel_id_list_ = new ArrayList<String>();
    FavoritesList favorites_list                = null;
    ScrollerData                        scroller_data_;
    List<String>                        pending_epg_channels_       = null;
    ObservableScrollView        time_scroller               = null;
    ObservableScrollView        guide_scroller              = null;
    ObservableVertScrollView    channel_scroller            = null;
    LinearLayout                channel_container = null;
    EpgView                     epg_container = null;

    public Bitmap                       record_bmp_;
    public Bitmap                       record_series_bmp_;
    public Bitmap                       hd_bmp_;
    public Bitmap                       preimere_bmp_;
    public Bitmap                       repeat_bmp_;
    public Bitmap                       rec_bmp_;
    public Bitmap                       rec_series_bmp_;
    public Bitmap                       rec_conflict_bmp_;
    public Bitmap                       star_full_bmp_;
    public Bitmap                       star_half_bmp_;
    public Bitmap                       star_none_bmp_;
    public Bitmap                       warning_bmp_;
    public boolean                      social_supported_           = false;
    Thread                              scroll_watch_thread_        = null;
    boolean                             scroll_thread_exit_flag_    = false;
    Thread                              now_watch_thread_           = null;
    boolean                             now_thread_exit_flag_       = false;
    Settings.EChannelSortMode sort_mode = Settings.EChannelSortMode.CH_SORT_MODE_NAME;

    String selectedFavorite = null;
    boolean useProgramColors = true;
    int pickerTheme;

    public int colorItemFill;
    public int colorItemFillShaded;
    public int colorItemStroke;
    public int colorItemText;
    public int colorItemTextShaded;
    public int guideItemActivatedColor;

    int active_channel_index = 0;
    int active_cell_index = 0;
    ActionPane _options_menu;
    StatusLine _status_line;
    Animation channel_animation;
    ProgramDetailsLoader programDetailsLoader = null;
    Thread connection_timeout_thread;
    BackgroundEPGUpdater background_epg_updater_ = null;

    //cast controls and variables
    private CastContext mCastContext = null;
    private MediaRouteButton mMediaRouteButton = null;
    private CastSession mCastSession = null;
    private final SessionManagerListener<CastSession> mSessionManagerListener = new TVMSessionManagerListener();
    String pending_channel_id = null;

    // Date picket image viewer used for Show menu
    ImageView showMenu;

    public GuideActivity() {
        super();
        _activity = this;
        state_ = EGAS_NONE;
        scroller_data_ = new ScrollerData();
    }

    static public GuideActivity getInstance() {
        return _activity;
    }

    // region Events

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
        }
    };

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.guide_activity);

        //initialize cast context only on mobile devices and only if google apis are available
        if (!Utils.usesRemoteControl(this) && Common.isGoogleApiAvailable(this)) {
            try {
                mCastContext = CastContext.getSharedInstance(this);
            } catch (Exception e) {
                mCastContext = null;
            }
        }

        if (mCastContext != null) {
            mMediaRouteButton = (MediaRouteButton) findViewById(R.id.media_route_button);
            CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), mMediaRouteButton);

            ViewStub miniControllerStub = (ViewStub) findViewById(R.id.cast_mini_controller);
            miniControllerStub.inflate();
        }

        needPlayLastPlayedChannel = bundle == null;

        first_launch_ = getIntent().getBooleanExtra("first_launch", false);
        try {
            getSettings();

            colorItemFill = Common.getColorFromAttribute(this, R.attr.guideItemFillColor);
            colorItemFillShaded = Common.getColorFromAttribute(this, R.attr.guideItemShadedFillColor);
            colorItemStroke = Common.getColorFromAttribute(this, R.attr.screenBoundsColor);
            colorItemText = Common.getColorFromAttribute(this, R.attr.guideItemTextColor);
            colorItemTextShaded = Common.getColorFromAttribute(this, R.attr.guideItemShadedTextColor);
            guideItemActivatedColor  = Common.getColorFromAttribute(this, R.attr.guideItemActivatedColor);

            TypedValue typedvalueattr = new TypedValue();
            getTheme().resolveAttribute(R.attr.hdBmp, typedvalueattr, true);
//          hd_bmp_ = BitmapFactory.decodeResource(getResources(), typedvalueattr.resourceId);

            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inScaled = false;

            hd_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.tvm_icon_hd, options);
            preimere_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.tvm_icon_premiere, options);
            repeat_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.tvm_icon_repeat, options);
            rec_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.tvm_icon_rec, options);
            rec_conflict_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.tvm_icon_rec_conflict, options);
            rec_series_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.tvm_icon_rec_series, options);

            getTheme().resolveAttribute(R.attr.starFullBmp, typedvalueattr, true);
            star_full_bmp_ = BitmapFactory.decodeResource(getResources(), typedvalueattr.resourceId);
            getTheme().resolveAttribute(R.attr.starHalfBmp, typedvalueattr, true);
            star_half_bmp_ = BitmapFactory.decodeResource(getResources(), typedvalueattr.resourceId);
            getTheme().resolveAttribute(R.attr.starNoneBmp, typedvalueattr, true);
            star_none_bmp_ = BitmapFactory.decodeResource(getResources(), typedvalueattr.resourceId);

            record_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.flag_reconce_light);
            record_series_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.flag_recseries_light);

            warning_bmp_ = BitmapFactory.decodeResource(getResources(), R.drawable.flag_recconflict_light);

            pickerTheme = (themeId == R.style.AppTheme) ? TimePickerDialog.THEME_HOLO_DARK :TimePickerDialog.THEME_HOLO_LIGHT;
            pickerTheme = AlertDialog.THEME_TRADITIONAL;

            layout_item_height  = (int)this.getResources().getDimension(R.dimen.channel_item_height);

            if (!Utils.usesRemoteControl(this)) {
                findViewById(R.id.menu_layout).setVisibility(View.GONE);
            }

            startGuideMode();

            channel_animation = AnimationUtils.loadAnimation(_context, R.anim.channel_activate);

            background_epg_updater_ = new BackgroundEPGUpdater(serverSettings, language_manager_, this, this, background_epg_update_timeout_sec_);
            guideAdviserProvider = new GuideAdviserProvider();

            if (data_provider_ == null) {
                findViewById(R.id.first_setup).setVisibility(View.VISIBLE);

                if (checkAndResetFirstLaunch()) {
                    //this is a TVMosaic app (e.g. no local server, first start)
                    //start settings activity to select a network server
                    startSettingsActivity();
                }
            }

            getWindow().getDecorView().post(new Runnable() {
                public void run() {
                    refresh();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
        //Show menu on  date picker click
        showMenu = (ImageView) findViewById(R.id.date_picker);
        showMenu.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                _options_menu.show();
            }
        });

    }

    @Override
    protected void onResume() {
        if (mCastContext != null) {
            mCastContext.getSessionManager().addSessionManagerListener(
                    mSessionManagerListener, CastSession.class);
            if (mCastSession == null) {
                mCastSession = CastContext.getSharedInstance(this).getSessionManager()
                        .getCurrentCastSession();
            }
            if (mCastSession != null) {
                RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
                if (remoteMediaClient != null)
                    remoteMediaClient.addListener(new TVMRemoteMediaClientListener(mCastSession));
            }

        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mCastContext != null) {
            mCastContext.getSessionManager().removeSessionManagerListener(mSessionManagerListener, CastSession.class);
        }
        super.onPause();
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == play_activity_req_code) {
            View item = channel_container.getChildAt(active_channel_index);
            if (item != null) {
                int y_pos = item.getTop();
                channel_scroller.scrollTo(0, y_pos);
            }
            checkAndScrollToNow();
        }

        if (requestCode == program_activity_req_code || requestCode == search_activity_req_code) {
            if (resultCode == ProgramActivity.RETURN_ADVISER_CREATE_PROFILE) {
                ProfileCreator profile = new ProfileCreator();
                profile.DVBLinkID = data.getStringExtra("long_channel_id");
                profile.Name = data.getStringExtra("profile_name");
                profile.StartTime = data.getLongExtra("start_time", 0);
                profile.EndTime = data.getLongExtra("end_time", 0);
                guideAdviserProvider.requestCreateProfile(profile);
            } else if (resultCode == ProgramActivity.RETURN_ADVISER_LIKE_THIS) {
                RecommendLikeThis request = new RecommendLikeThis();
                request.DVBLinkChannelId = data.getStringExtra("long_channel_id");
                request.StartTime = data.getLongExtra("start_time", 0);
                request.EndTime = data.getLongExtra("end_time", 0);
                request.RecommendetionsFrom = request.StartTime;
                request.RecommendetionsTo = request.RecommendetionsFrom + seconds_per_day;

                likeThisMode = true;
                likeThisChannelID = request.DVBLinkChannelId;
                likeThisStartTime = request.StartTime;
                likeThisEndTime = request.EndTime;
                newSelectedProfile = "";

                clearRecommendations();
                guideAdviserProvider.requestLikeThis(request);
            }
        }

        if (requestCode == program_activity_req_code || requestCode == list_activity_req_code || requestCode == search_activity_req_code) {
            // stop channel playback on server
            if (resultCode == ProgramActivity.return_play && data != null) {
                String channel_id = data.getStringExtra("channel_id");
                if (channel_id != null) {
                    int cnt = channel_container.getChildCount();
                    for (int idx = 0; idx < cnt; idx++) {
                        ChannelItem ci = (ChannelItem) channel_container.getChildAt(idx);
                        if (ci.channel_.ID.equals(channel_id)) {
                            startChannelPlayActivity(ci.channel_.DVBLinkID);
                            break;
                        }
                    }
                }
            }

            if (data != null && data.hasExtra("need_update_recommendations")) {
                requestRecommendBySelectedProfile();
            }
        }

        if (requestCode == program_activity_req_code || requestCode == list_activity_req_code || requestCode == search_activity_req_code) {
            if (data != null && data.hasExtra("need_channel_update")) {
                String channels_xml = data.getStringExtra("need_channel_update");
                if (channels_xml != null) {
                    ChannelIDList channels = (ChannelIDList) Serializer.deserialize(
                            channels_xml,
                            ChannelIDList.class
                    );
                    long start_time = -1;
                    long end_time = -1;
                    for (int i=0; i<channels.size(); i++) {
                        LinearLayout ll = (LinearLayout) channel_display_map.get(channels.get(i)).epg_container;
                        if (ll != null) {
                            View vv = ll.getChildAt(0);
                            if (vv instanceof GuideBar) {
                                GuideBar bar = (GuideBar) vv;
                                if (start_time == -1)
                                    start_time = bar.GetBarStartTime();
                                else
                                    start_time = bar.GetBarStartTime() < start_time ? bar.GetBarStartTime() : start_time;
                                if (end_time == -1)
                                    end_time = bar.GetBarEndTime();
                                else
                                    end_time = bar.GetBarEndTime() > end_time ? bar.GetBarEndTime() : end_time;
                            }
                        }
                    }
                    if (start_time > 0 && end_time > 0)
                        background_epg_updater_.add_update(start_time, end_time, channels);
                }
            }
        }

        if (requestCode == settings_activity_req_code) {
            //reload settings
            ProcessSettingsChange();

            //re-create adviser provider
            guideAdviserProvider = new GuideAdviserProvider();
            //re-create background epg updater with new settings
            background_epg_updater_ = new BackgroundEPGUpdater(serverSettings, language_manager_, this, this, background_epg_update_timeout_sec_);

            refresh();
        }

        if (requestCode == list_activity_req_code) {
            if (data != null && data.hasExtra("need_update_recommendations")) {
                newSelectedProfile = data.getStringExtra("profile_id");
                likeThisMode = false;
                createMenu();
                changeSelectedProfileOnMenu(newSelectedProfile);
                clearRecommendations();
                requestRecommendBySelectedProfile();
            }
        }
    }

    private void changeSelectedProfileOnMenu(String profileID) {
        _options_menu.get_options(ActionPane.guide_profiles_pane_action_id_).set_selected_item_id(profileID);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            _options_menu.show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
//            super.onBackPressed();
        _options_menu.show();
    }

    @Override
    public void onActionPaneAction(int action_id) {

        if (action_id == ActionPane.goto_pane_action_id_) {
            TopLevelNavigationPane tln = new TopLevelNavigationPane(TopLevelNavigationPane.exclude_flags_guide_activity, this);
            tln.show();
        } else {
            _options_menu.hide();

            if (action_id == ActionPane.settings_pane_action_id_) {
                startSettingsActivity();
            } else if (action_id == ActionPane.guide_search_pane_action_id_) {
                startSearchExActivity();
            } else if (action_id == ActionPane.guide_refresh_pane_action_id_) {
                refresh();
            } else if (action_id == ActionPane.guide_recommendations_pane_action_id_) {
                if (isAdviserMode()) {
                    startRecommendationListEx();
                }
            }
        }
    }

    @Override
    public void onActionPaneOption(int option_id, String selected_option) {

        _options_menu.hide();

        if (option_id == ActionPane.guide_sorting_pane_action_id_) {
            sort_mode = Settings.EChannelSortMode.CH_SORT_MODE_NAME;
            if (selected_option.equals(Settings.EChannelSortMode.CH_SORT_MODE_NAME.toString())) {
                sort_mode = Settings.EChannelSortMode.CH_SORT_MODE_NAME;
            } else if (selected_option.equals(Settings.EChannelSortMode.CH_SORT_MODE_NUMBER.toString())) {
                sort_mode = Settings.EChannelSortMode.CH_SORT_MODE_NUMBER;
            }
            settings_.setChannelSortMode(sort_mode);

        } else if (option_id == ActionPane.guide_favorites_pane_action_id_) {
            selectedFavorite = selected_option;
            settings_.setValue("selected_favorite_id", selectedFavorite);

        } else if (option_id == ActionPane.guide_profiles_pane_action_id_) {
            boolean isLikeThisMode = likeThisMode;
            likeThisMode = false;
            clearRecommendations();
            if (isLikeThisMode) {
                if (profiles.size() > 0) {
                    ActionOptionsPaneItems actionOptionsPaneItems = _options_menu.get_options(
                            ActionPane.guide_profiles_pane_action_id_
                    );
                    actionOptionsPaneItems.remove_item(MENU_LIKE_THIS_OPTION_ID);
                    _options_menu.update_nested_items();
                } else {
                    _options_menu.hide_nested_menu();
                }

                if (selected_option.equals(ActionOptionsPaneItems.empty_selected_option_id_)) {
                    correctMenuForRecommendations();
                    return;
                }
            }

            newSelectedProfile = selected_option.equals(ActionOptionsPaneItems.empty_selected_option_id_) ?
                    "" : selected_option;
            if (!newSelectedProfile.isEmpty()) {
                requestRecommendBySelectedProfile();
            }

            correctMenuForRecommendations();
            return;
        }
        this.refresh();
    }


    @Override
    protected Dialog onCreateDialog(int id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.NewTunerNotConnectedDialog);
        View dialog = getLayoutInflater().inflate(R.layout.tuner_not_connected_dialog_layout, null);
        ((TextView) dialog.findViewById(R.id.title)).setText(
                language_manager_.GetString(LocaleStrings.IDS_TUNER_NOT_CONNECTED_TITLE)
        );
        ((TextView) dialog.findViewById(R.id.description)).setText(
                Html.fromHtml(language_manager_.GetString(LocaleStrings.IDS_TUNER_NOT_CONNECTED_DESCRIPTION))
        );
        return builder
                .setView(dialog)
                .setCancelable(false)
                .create();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        needPlayLastPlayedChannel = false;
        init_first_setup_dialog();
    }

    @Override
    public boolean onTouch(View v, MotionEvent e) {
        if (e.getAction() == MotionEvent.ACTION_UP) {
            if (prev_motion_event_ != null && e.getX() == prev_motion_event_.getX()
                    && e.getY() == prev_motion_event_.getY()) {
                prev_motion_event_ = null;

                if (v instanceof GuideBar) {
                    GuideBar guide_bar = (GuideBar) v;
                    GuideBar.GuideBarItem item = guide_bar.GetProgramForX((int) e.getX());

                    if (item != null) {
                        startProgramActivity(guide_bar.channel_id_, item, guide_bar.recommendation_map.get(item.prg_.ID));
                    }
                }
            }
            v.performClick();
        }
        if (e.getAction() == MotionEvent.ACTION_DOWN)
            prev_motion_event_ = e;

        return true;
    }

    @Override
    public void onDestroy() {
        clearAll();
        if (data_provider_ != null)
            data_provider_.Cancel();
        data_provider_ = null;
        language_manager_ = null;
        _options_menu.hide();

        super.onDestroy();
    }

    @Override
    protected void refresh() {
        findViewById(R.id.first_setup).setVisibility(View.GONE);
        background_epg_updater_.stop();

        clearProgramInfoPane();
        clearEPG();
        clearChannels();

        if (profiles != null) {
            profiles.clear();
            profiles = null;
        }
        if (mappedChannels != null) {
            mappedChannels.clear();
            mappedChannels = null;
        }
        clearRecommendations();
        likeThisMode = false;
        newSelectedProfile = "";

        if (pending_epg_channels_ != null)
            pending_epg_channels_.clear();
        pending_epg_channels_ = null;

        active_channel_index = -1;

        correctMenuForRecommendations();

        getSettings();
        setDateLabelText();

        requestStreamingCaps();
    }

    @Override
    public void onScrollChanged(Object scrollView, int x, int y, int oldx, int oldy) {
        if (scrollView == time_scroller) {
            guide_scroller.scrollTo(x, y);
            if (scroller_data_.start_pos == -1) {
                scroller_data_.start_pos = x;
                scroller_data_.active = true;
            }
            scroller_data_.scroll_pos = x;
            changeDate(oldx - x);
        } else if (scrollView == guide_scroller) {
            if (y != oldy) {
                if (active_channel_index == 0 && y != 0)
                    guide_scroller.scrollTo(x, 0);
            }
            time_scroller.scrollTo(x, y);
            changeDate(oldx - x);
        }
        Rect bar_rect = new Rect();
        Rect rect = new Rect();
        epg_container.getGlobalVisibleRect(rect);
        int cnt = epg_container.getChildCount();
        for (int idx = 0; idx < cnt; idx++) {
            ChannelItem item = (ChannelItem) channel_container.getChildAt(idx);
            LinearLayout ll = channel_display_map.get(item.channel_.ID).epg_container;
            View vv = ll.getChildAt(0);
            vv.getGlobalVisibleRect(bar_rect);
            if (Rect.intersects(rect, bar_rect))
                vv.invalidate();
        }
    }

    @Override
    public void onClick(View v) {
        boolean enable = false;
        if (state_ == EGAS_REQUESTING_STREAM_CAPS ||
                state_ == EGAS_REQUESTING_CHANNELS ||
                state_ == EGAS_READY)
            enable = true;

        if (!enable)
            return;

        if (v instanceof ChannelItem) {
            // only start playback from ready state
            ChannelItem ci = (ChannelItem) v;
            startChannelPlayActivity(ci.channel_.DVBLinkID);
        } else {
            int id = v.getId();
            switch (id) {
                /*case R.id.guide_bottom_button:
                    refresh();
                    break;*/

                case R.id.date_layout:
                    startDatePicker();
                    break;

                case R.id.date_picker:
                    startDatePicker();
                    break;
            }
        }
    }

    public void onDatePicker(int year, int month, int day) {

        if (state_ == EGAS_READY) {

            try {
                TextView tv = (TextView) findViewById(R.id.date_label);
                Date dt = (Date) tv.getTag();
                Calendar old_calendar = Calendar.getInstance();
                old_calendar.setTime(dt);
                old_calendar.set(Calendar.HOUR_OF_DAY, 0);
                old_calendar.set(Calendar.MINUTE, 0);
                old_calendar.set(Calendar.SECOND, 0);
                old_calendar.set(Calendar.MILLISECOND, 0);

                Calendar new_calendar = Calendar.getInstance();
                new_calendar.set(year, month, day, 0, 20, 0);

                long new_now = new_calendar.getTimeInMillis() / 1000;
                setDateLabelText(new_now);
                Pair<Long, Long> pair = TimeLineBuilder.getBounds();
                requestDateEpg(channel_id_list_, new_now - (1 * sec_per_hour), new_now + (23 * sec_per_hour));
                final int now_offs = TimeLineBuilder.GridTimeToPxWidth(this, pair.first, new_now);
                time_scroller.post(new Runnable() {
                    public void run() {
                        time_scroller.scrollTo(now_offs, time_scroller.getScrollY());
                    }
                });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // endregion

    // region Server communication

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;
        skipTimeoutThread();
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state_ == EGAS_REQUESTING_CHANNELS) {
                    processChannels();
                } else if (state_ == EGAS_REQUESTING_DATE_EPG) {
                    processDateEpg();
                } else if (state_ == EGAS_REQUESTING_STREAM_CAPS) {
                    processStreamCaps();
                } else if (state_ == EGAS_REQUESTING_DIRECT_URL) {
                    processDirectUrl();
                } else if (state_ == EGAS_REQUESTING_OOB_URL) {
                    processOobChannelUrl();
                } else if (state_ == EGAS_REQUESTING_FAVORITES) {
                    processFavorites();
                }
                response_ = null;
            }
        });
    }

    public void ErrorProcessing(String command, StatusCode error) {
        skipTimeoutThread();
        if (error == StatusCode.STATUS_UNAUTHORISED) {
            response_ = null;
            http_error_ = error;
            this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    processAuthorizationError();
                }
            });
        } else {
            if (state_ == EGAS_REQUESTING_STREAM_CAPS) {
                // this can happen when app is running against the old version of
                // the server
                // In this case just skip caps part and requestRoot channels
                response_ = null;
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        processStreamCaps();
                    }
                });
            } else
            if (state_ == EGAS_REQUESTING_OOB_URL) {
                //this can happen if server is either old or there is no oob url for the requested channel
                response_ = null;
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        processOobChannelUrl();
                    }
                });
            } else
            if (state_ == EGAS_REQUESTING_FAVORITES) {
                response_ = null;
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        state_ = EGAS_READY;
                        activateProgressBar(false);
                        checkTuner();
                    }
                });
                return;
            } else {
                http_error_ = error;
                this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        processDataProviderError();
                    }
                });
            }
        }
    }

    protected void requestStreamingCaps() {
        if (data_provider_ != null && state_ == EGAS_READY) {
            state_ = EGAS_REQUESTING_STREAM_CAPS;
            activateProgressBar(true);
            data_provider_.GetStreamingCapabilities();
            enableSortingSpinner();
        }
    }

    protected void processStreamCaps() {
        try {
            state_ = EGAS_READY;
            activateProgressBar(false);
            if (response_ != null && response_ instanceof ServerCapabilities) {
                ServerCapabilities streaming_caps = (ServerCapabilities) response_;
                social_supported_ = false;//streaming_caps.IsSocialSupported();
                settings_.setValue(Settings.TIMESHIFT_SUPPORTED_KEY, streaming_caps.SupportsTimeShift);

                can_record_ = streaming_caps.CanRecord;
                settings_.setValue(SettingsManager.CAN_RECORD_KEY, streaming_caps.CanRecord);

                //device management is only supported in TVMosaic Live
                boolean supports_device_mgmt = false;
                settings_.setValue(SettingsManager.SUPPORTS_DEVICE_MGMT_KEY, supports_device_mgmt);

                //is transcoding supported by server?
                ServerSettings ss = settings_.getServerSettings();
                ss.transcodingSupported = streaming_caps.IsH264Supported();
                settings_.setServerSettings(ss);


                if (mMediaRouteButton != null) {
                    //show chromecast button if transcoding is supported
                    if (ss.transcodingSupported)
                        mMediaRouteButton.setVisibility(View.VISIBLE);
                }

                settings_.setValue(SettingsManager.IN_DEVELOPMENT_FUNCTIONALITY_KEY, false);
            }
            requestFavorites();
        } catch (Exception e) {
        }
    }

    protected void requestChannels() {
        if (data_provider_ != null && state_ == EGAS_READY)  {
            activateProgressBar(true);
            state_ = EGAS_REQUESTING_CHANNELS;

            String fav = getSelectedFavorite() == null ? "" : selectedFavorite;

            data_provider_.GetChannels(new ChannelsRequest(fav));
        }
    }

    protected void processChannels() {

        boolean b_too_many_channels = false;

        try {
            activateProgressBar(false);
            state_ = EGAS_READY;

            if (response_ != null && response_ instanceof ChannelsList) {
                ChannelsList channels = (ChannelsList) response_;

                //check if there are a lot of channels and limit its number for UI not to hang
                if (channels.size() > maximum_visible_channels_num) {
                    b_too_many_channels = true;
                    channels.list().subList(maximum_visible_channels_num, channels.list().size()).clear();
                }

                sortChannels(channels, sort_mode);
                processChannels(channels);

                prepareTimelineAndScroll();
                eraseEPG(null);
            }
        } catch (Exception e) {
        }

        if (b_too_many_channels)
            Toast.makeText(this, String.format(language_manager_.GetString(LocaleStrings.IDS_TOO_MANY_CHANNELS_WARNING), maximum_visible_channels_num), Toast.LENGTH_LONG).show();

        guideAdviserProvider.requestMappedChannels();

    }

    protected void lastInitStep() {
        //if there are scanned channels - request epg for them
        //otherwise - show popup with setup instructions
        if (channel_id_list_.size() > 0) {
            if (Utils.usesRemoteControl(this))
                findViewById(R.id.program_details_pane).setVisibility(View.VISIBLE);

            applyFavorites();

            //set input focus on the channels container
            if (!channel_container.hasFocus())
                channel_container.requestFocus();
            else
                activateFirstVisibleChannelDown();

            //start background epg updater
            background_epg_updater_.start();

            checkAndPlayLastChannel();

        }

        enableSortingSpinner();

        createMenu();
    }

    protected Boolean checkAndResetFirstLaunch() {
        Boolean b = first_launch_;
        first_launch_ = false;

        return b;
    }

    protected void requestDateEpg(ArrayList<String> channels, long start_time, long end_time) {
        if (data_provider_ == null || channels == null || channels.isEmpty())
            return;
        if (state_ != EGAS_READY)
            return;
/*
        ChannelIDList id_list = new ChannelIDList();
        id_list.list().addAll(channels);

        Pair<Long, Long> p = getLoadingArea(start_time, end_time);

        if (p == null) {
            requested_start_time = start_time;
            requested_end_time = end_time;
        } else {
            requested_start_time = p.first;
            requested_end_time = p.second;
        }
        //-->

        pending_epg_channels_ = channels;
        EpgSearcher es = new EpgSearcher(id_list, true, requested_start_time, requested_end_time);

        activateProgressBar(true);
        state_ = EGAS_REQUESTING_DATE_EPG;
        data_provider_.SearchEpg(es);

        addLoadedArea(start_time, end_time);
        */
    }

    private void processDateEpg() {
        try {
            if (response_ != null && response_ instanceof ChannelIdWithProgramsList) {
                ChannelIdWithProgramsList epg_data = (ChannelIdWithProgramsList) response_;
                updateEpg(epg_data, -1, requested_start_time, requested_end_time);

                GuideBar bar = getActiveGuideBar();
                GuideBar.GuideBarItem item = bar.items_.get(active_cell_index);
                item.highlight(false);

                Rect drawingRect = new Rect();
                bar.getLocalVisibleRect(drawingRect);
                for (int idx=0; idx<bar.items_.size(); idx++) {
                    item = bar.items_.get(idx);
                    if (drawingRect.intersects(item.start_x_, drawingRect.centerY()-1, item.end_x_, drawingRect.centerY()+1)) {
                        active_cell_index = idx;
                        break;
                    }
                }
                fillItemDetails(bar, item);
            }
        } catch (Exception e) {
        }
        state_ = EGAS_READY;
        activateProgressBar(false);
    }

    protected void requestFavorites() {
        if (data_provider_ != null && state_ == EGAS_READY)  {
            activateProgressBar(true);
            state_ = EGAS_REQUESTING_FAVORITES;
            data_provider_.GetFavorites(new FavoritesRequest());
        }
    }

    protected void processFavorites() {
        try {
            activateProgressBar(false);
            state_ = EGAS_READY;

            favorites_list = null;
            if (response_ != null && response_ instanceof FavoritesList) {
                favorites_list = (FavoritesList) response_;
                if (favorites_list.list().isEmpty()) {
                    favorites_list = null;
                } else {
                    restoreSelectedFavorite();
                }
            }
        } catch (Exception e) {
        }
        checkTuner();
    }

    // endregion

    // region New activity

    void startChannelPlayActivity(String channel_id) {
        if (mCastSession != null) {
            //start playback on chromecast
            pending_channel_id = channel_id;

            ServerSettings ss = settings_.getServerSettings();
            if (ss.transcodingSupported) {
                //if transcoding is supported - this is the preffered way of streaming
                //Request direct url and start transcoded streaming
                requestDirectUrl(channel_id);
            } else {
                Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_CHROMCAST_NEEDS_TRANSCODING), Toast.LENGTH_LONG).show();
            }

        } else {
            //start local playback
            Intent i = new Intent();
            i.setClass(this, VLCPlayLiveActivity.class);
            i.putExtra("dvblink_channel_id", channel_id);
            startActivityForResult(i, play_activity_req_code);
        }
    }

    void startProgramActivity(String channel_id, GuideBar.GuideBarItem item, RecommendationProgram recommendationProgram) {
        Intent i = new Intent(GuideActivity.this, ProgramActivity.class);
        i.putExtra("channel_id", channel_id);
        String long_channel_id = channel_display_map.get(channel_id).channel_item.channel_.DVBLinkID;
        i.putExtra("long_channel_id", long_channel_id);
        i.putExtra("channel_name", channel_display_map.get(channel_id).channel_item.channel_.Name);
        i.putExtra("program_id", item.prg_.ID);
        i.putExtra("social_supported", false);
        i.putExtra("enable_create_profile", enableCreateProfile(long_channel_id));
        if (isAdviserMode()) {
            i.putExtra("adviser_mode", true);
            i.putExtra("profile_id", newSelectedProfile);
            float rating = 0;
            if (recommendationProgram != null) {
                if (recommendationProgram.Rating < 0) {
                    rating = -1;
                } else if (recommendationProgram.Rating > 0) {
                    rating = 1;
                }
            }

            i.putExtra("rating", rating);
            i.putExtra("score", (recommendationProgram != null) ? recommendationProgram.Score : 0);
            i.putExtra("enable_like_dislike", !likeThisMode);
        }
        startActivityForResult(i, program_activity_req_code);
    }

    void startSettingsActivity() {
        Intent i = new Intent();
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setClass(this, SettingsActivity.class);
        startActivityForResult(i, settings_activity_req_code);
    }

    void startSearchExActivity() {
        Intent i = new Intent();
        i.setClass(getBaseContext(), SearchExActivity.class);
        i.putExtra("enable_create_profile", profiles != null && !profiles.isEmpty());
        i.putExtra("enable_like_dislike", false);

        startActivityForResult(i, (int) search_activity_req_code);
    }
    // endregion

    protected void createMenu() {
        _options_menu = new ActionPane(this, this);

        //add sorting options
        ActionOptionsPaneItems sorting_options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON);
        sorting_options.add_item(language_manager_.GetString(LocaleStrings.IDS_SORT_BY_NAME), Settings.EChannelSortMode.CH_SORT_MODE_NAME.toString());
        sorting_options.add_item(language_manager_.GetString(LocaleStrings.IDS_SORT_BY_NUMBER), Settings.EChannelSortMode.CH_SORT_MODE_NUMBER.toString());
        Settings.EChannelSortMode e_sort_mode = settings_.getChannelSortMode();
        sorting_options.set_selected_item_id(e_sort_mode.toString());

        _options_menu.set_options(ActionPane.guide_sorting_pane_action_id_, sorting_options);

        //add favorites
        if (favorites_list != null && favorites_list.size() > 0) {
            ActionOptionsPaneItems favorites_options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX);

            for (int idx=0; idx<favorites_list.size(); idx++) {
                favorites_options.add_item(favorites_list.get(idx).Name, favorites_list.get(idx).ID);
            }

            favorites_options.set_selected_item_id(selectedFavorite);

            _options_menu.set_options(ActionPane.guide_favorites_pane_action_id_, favorites_options);
        }

        //add profiles
        boolean adviserProfilesExists = profiles != null && !profiles.isEmpty();
        if (adviserProfilesExists || likeThisMode) {
            ActionOptionsPaneItems profilesOptions = new ActionOptionsPaneItems(
                    ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX
            );
            if (likeThisMode) {
                profilesOptions.add_item(language_manager_.GetString(LocaleStrings.IDS_LIKE_THIS), MENU_LIKE_THIS_OPTION_ID);
            }
            for (ProfileRecommendation profileRecommendation: profiles) {
                profilesOptions.add_item(profileRecommendation.profile.ProfileName, profileRecommendation.profile.ProfileId);
            }
            String d = likeThisMode ? MENU_LIKE_THIS_OPTION_ID : newSelectedProfile;
            profilesOptions.set_selected_item_id(d);
            _options_menu.set_options(ActionPane.guide_profiles_pane_action_id_, profilesOptions);
        }

        correctMenuForRecommendations();
    }

    private void correctMenuForRecommendations() {
        boolean adviserProfilesExists = profiles != null && !profiles.isEmpty();
        boolean recommendationActive = !newSelectedProfile.isEmpty() || likeThisMode;
        _options_menu.setItemVisibility(ActionPane.guide_profiles_pane_action_id_, adviserProfilesExists || likeThisMode);
        _options_menu.setItemVisibility(ActionPane.guide_recommendations_pane_action_id_, recommendationActive);
        _options_menu.setItemVisibility(ActionPane.guide_search_pane_action_id_, !recommendationActive);

        if (_status_line != null) {
            String rec_status_text = "";
            if (likeThisMode) {
                rec_status_text = language_manager_.GetString(LocaleStrings.IDS_LIKE_THIS);
            } else if (recommendationActive) {
                for (int i = 0; i < profiles.size(); i++) {
                    if (profiles.get(i).profile.ProfileId.equals(newSelectedProfile)) {
                        rec_status_text = profiles.get(i).profile.ProfileName;
                    }
                }
            }

            final String text = language_manager_.GetString(LocaleStrings.IDS_PROFILE) + ": ";
            _status_line.setField(0, text, rec_status_text, StatusLine.status_icon_profile);
        }
    }

    protected void clearAll() {
        stopNowThread();
        stopScrollWatchThread();
        clearEPG();
        clearChannels();
        profiles = null;
        record_bmp_ = null;
        record_series_bmp_ = null;
        hd_bmp_ = null;
        preimere_bmp_ = null;
        repeat_bmp_ = null;
        star_full_bmp_ = null;
        star_half_bmp_ = null;
        star_none_bmp_ = null;
        warning_bmp_ = null;
        search_icon = null;
        prev_motion_event_ = null;
        channel_display_map.clear();
        channel_id_list_.clear();
        scroller_data_ = null;
        pending_epg_channels_ = null;
        time_scroller = null;
        guide_scroller = null;
        channel_scroller = null;
    }

    protected void init_first_setup_dialog() {
        View v = findViewById(R.id.first_setup);
        TextView tv = (TextView) v.findViewById(R.id.setup_text);
        tv.setText(Html.fromHtml(language_manager_.GetString(LocaleStrings.IDS_FIRST_SETUP)));
    }

    protected void enableSortingSpinner() {
        String sort_mode_text = "";
        if (channel_id_list_.size() > 0) {
            if (sort_mode == Settings.EChannelSortMode.CH_SORT_MODE_NAME) {
                sort_mode_text = language_manager_.GetString(LocaleStrings.IDS_SORT_BY_NAME);
            } else if (sort_mode == Settings.EChannelSortMode.CH_SORT_MODE_NUMBER) {
                sort_mode_text = language_manager_.GetString(LocaleStrings.IDS_SORT_BY_NUMBER);
            }
        }

        final String text = language_manager_.GetString(LocaleStrings.IDS_SORTING) + ": ";
        _status_line.setField(2, text, sort_mode_text, StatusLine.status_icon_sort);
    }

    protected void startGuideMode() {

        init_first_setup_dialog();

        final int layout_height = Utils.usesRemoteControl(this) ? layout_item_height * number_of_visible_channels : ViewGroup.LayoutParams.MATCH_PARENT;
        final LinearLayout ll = (LinearLayout) findViewById(R.id.list_layout);

        if (!Utils.usesRemoteControl(this)) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) ll.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, R.id.status_line);
            ll.setLayoutParams(params);
        }

        ll.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                ll.getLayoutParams().height = layout_height;
                ll.requestLayout();
                ll.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        createMenu();
        _status_line = new StatusLine(findViewById(R.id.status_line));

        setDateLabelText();

        epg_container = (EpgView) findViewById(R.id.epg_container);
        epg_container.setWillNotDraw(false);

        // set scrollers callback
        time_scroller = (ObservableScrollView) findViewById(R.id.time_scroller);
        time_scroller.setScrollViewListener(this);
        guide_scroller = (ObservableScrollView) findViewById(R.id.guide_scroller);
        guide_scroller.setScrollViewListener(this);

        channel_container = (LinearLayout) this.findViewById(R.id.channel_container);
        channel_scroller = (ObservableVertScrollView) findViewById(R.id.main_scroll_view);
        channel_scroller.setScrollViewListener(this);
        channel_scroller.setFocusable(false);
        channel_container.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (active_channel_index != -1) {
                    ChannelItem item = (ChannelItem) channel_container.getChildAt(active_channel_index);
                    if (item == null)
                        return;

                    if (hasFocus) {
                        item.setActivated(true);
                    } else {
                        item.setActivated(false);
                    }
                    fillItemDetails(active_channel_index);
                } else {
                    activateFirstVisibleChannelDown();
                }
            }
        });

        channel_container.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP)
                    return true;

                if (Common.backPressed(keyCode, event) || keyCode == KeyEvent.KEYCODE_MENU) {
                    _options_menu.show();
                    return true;
                }

                if (channel_id_list_.size() == 0)
                    return false;

                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT) {
                    LinearLayout ll = (LinearLayout)getChannelEpg(active_channel_index);
                    View vv = ll.getChildAt(0);
                    if (vv instanceof GuideBar) {
                        vv.requestFocus();
                        GuideBar bar = (GuideBar) vv;
                        Rect rect = new Rect();
                        bar.getLocalVisibleRect(rect);
                        for (int idx=0; idx<bar.items_.size(); idx++) {
                            GuideBar.GuideBarItem item = bar.items_.get(idx);
                            if (item.start_x_ >= rect.left || item.end_x_ > rect.left) {
                                active_cell_index = idx;
                                item.highlight(true);
                                fillItemDetails(bar, item);
                                break;
                            }
                        }
                    }
                    return false;
                } else if (keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                    if(active_channel_index == 0) {
//                        findViewById(R.id.date_layout).requestFocus();
                    } else {
                        int old_index = active_channel_index;
                        ChannelItem item;
                        while(true) {
                            active_channel_index--;
                            if (active_channel_index < 0) {
                                active_channel_index = 0;
                                return  true;
                            }
                            item = (ChannelItem) channel_container.getChildAt(active_channel_index);
                            if (item.getVisibility() == View.VISIBLE)
                                break;
                        }
                        channel_container.getChildAt(old_index).setActivated(false);
                        item.setActivated(true);

                        int dy = item.getHeight() + channel_container.getShowDividers();
                        Rect scrollBounds = new Rect();
                        channel_scroller.getHitRect(scrollBounds);
                        if (!item.getLocalVisibleRect(scrollBounds)) {
                            channel_scroller.scrollBy(0, -dy*3);
                        } else if (active_channel_index == 0) {
                            channel_scroller.scrollBy(0, 0);
                        }
                        fillItemDetails(active_channel_index);
                    }
                    return true;
                } else if (keyCode == KeyEvent.KEYCODE_DPAD_DOWN) {
                    if (active_channel_index < channel_container.getChildCount()-1) {
                        activateFirstVisibleChannelDown();
                    }
                    return true;
                } else if (Common.okPressed(keyCode)) {
                    ChannelItem ci = (ChannelItem) channel_container.getChildAt(active_channel_index);
                    if (state_ == EGAS_REQUESTING_STREAM_CAPS || state_ == EGAS_REQUESTING_CHANNELS || state_ == EGAS_READY) {
                        startChannelPlayActivity(ci.channel_.DVBLinkID);
                    }
                }
                return false;
            }
        });

        // reset activity state (restore it from Bundle in the future?)
        state_ = EGAS_READY;

        //refresh();

        if (scroll_watch_thread_ == null) {
            scroll_thread_exit_flag_ = false;
            scroll_watch_thread_ = new Thread(new Runnable() {
                public void run() {
                    scrollWatchThread();
                }
            });
            scroll_watch_thread_.start();
        }

        RelativeLayout rl = (RelativeLayout) findViewById(R.id.date_layout);
        rl.setOnClickListener(this);
        rl.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    if (active_channel_index != -1) {
                        View child = channel_container.getChildAt(active_channel_index);
                        if (child != null) {
                            child.setActivated(false);
                            child.invalidate();
                        }
                    }
                }
            }
        });

        rl.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                return false;
            }
        });

        ImageView image = (ImageView) findViewById(R.id.date_picker);
        image.setOnClickListener(this);
    }

    protected void activateFirstVisibleChannelDown() {
        int old_index = active_channel_index;

        ChannelItem item;
        while(true) {
            active_channel_index++;
            if (active_channel_index > channel_container.getChildCount()-1) {
                active_channel_index = channel_container.getChildCount()-1;
                return;
            }
            item = (ChannelItem) channel_container.getChildAt(active_channel_index);
            if (item.getVisibility() == View.VISIBLE)
                break;
        }

        if (old_index != -1)
            channel_container.getChildAt(old_index).setActivated(false);

        item.setActivated(true);

        int dy = item.getHeight() + channel_container.getShowDividers();
        Rect scrollBounds = new Rect();
        channel_scroller.getHitRect(scrollBounds);
        if (!item.getLocalVisibleRect(scrollBounds)) {
            channel_scroller.scrollBy(0, dy*3);
        } else if (active_channel_index == channel_container.getChildCount()-1) {
            channel_scroller.scrollBy(0, dy*3);
        }
        fillItemDetails(active_channel_index);
    }

    protected String getChannelId(String id) {
        for (Map.Entry<String,ChannelRef> entry: channel_display_map.entrySet()) {
            ChannelRef ref = entry.getValue();
            if (ref.channel_item.channel_.DVBLinkID.equals(id)) {
                return ref.channel_item.channel_.ID;
            }
        }
        return "";
    }

    private void playFirstChannel() {
        needPlayFirstChannel = true;
    }

    public void stopNowThread() {
        if (now_watch_thread_ != null) {
            now_thread_exit_flag_ = true;
            try {
                now_watch_thread_.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            now_watch_thread_ = null;
        }
    }

    public void stopScrollWatchThread() {
        if (scroll_watch_thread_ != null) {
            scroll_thread_exit_flag_ = true;
            try {
                scroll_watch_thread_.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            scroll_watch_thread_ = null;
        }
    }

    public void scrollWatchThread() {
        while (!scroll_thread_exit_flag_) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            this.runOnUiThread(new Runnable() {
                public void run() {
                    checkScrollData();
                }
            });
        }
    }

    public void nowWatchThread() {
        int sleep_value = 200;
        int update_interval_sec = 60;
        long cnow = Utils.CurrentUtc();
        long next_update_time = cnow + update_interval_sec;

        while (!now_thread_exit_flag_) {
            try {
                Thread.sleep(sleep_value);
                cnow = Utils.CurrentUtc();
                if (cnow > next_update_time) {
                    next_update_time = cnow + update_interval_sec;
                    this.runOnUiThread(new Runnable() {
                        public void run() {
                            updateNowPosition();
                        }
                    });

                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    protected void updateNowPosition() {
        // check if now is still within this window
        if (guide_scroller == null)
            return;
        Pair<Long, Long> pair = TimeLineBuilder.getBounds();
        int now_offs = TimeLineBuilder.GetNowOffset(this, pair.first);
        if (epg_container != null)
            epg_container.set_now_coord(now_offs);
    }

    protected void removeAndDisposeAllViews(ViewGroup view) {
        if (view == null)
            return;
        view.removeAllViews();
    }

    protected void clearChannels() {
        removeAndDisposeAllViews(channel_container);
        removeAndDisposeAllViews(epg_container);

        channel_display_map.clear();
        channel_id_list_.clear();
    }

    protected void clearEPG() {
        try {
            channel_display_map.clear();
            if (guide_scroller != null) {
                EpgView epg_view = (EpgView) guide_scroller.getChildAt(0);
                if (epg_view != null)
                    epg_view.removeAllViews();
            }

            // timeline
            TimeLine time_line = (TimeLine) this.findViewById(R.id.time_line);
            time_line.clear_timeline();

        } catch(Exception e) {

        }
    }

    private void checkAndPlayLastChannel() {
        Log.i(TAG, "checkAndPlayLastChannel");
        boolean playLastChannelOnStart = settings_.getValue(
                Settings.PLAY_LAST_CHANNEL_ON_START, Settings.PLAY_LAST_CHANNEL_ON_START_DEFAULT
        );
        if (!playLastChannelOnStart) {
            return;
        }
        if (!needPlayLastPlayedChannel && !needPlayFirstChannel) {
            return;
        }
        String lastChannelID;
        if (needPlayFirstChannel && channel_id_list_.size() > 0) {
            lastChannelID = channel_id_list_.get(0);
            settings_.setValue(Settings.PLAY_LAST_CHANNEL_ID, lastChannelID);
            needPlayFirstChannel = false;
        } else {
            lastChannelID = settings_.getValue(Settings.PLAY_LAST_CHANNEL_ID, "");
        }
        if (lastChannelID.length() > 0 && channel_id_list_.contains(lastChannelID)) {
            startChannelPlayActivity(lastChannelID);
        }
    }

    protected void processChannels(ChannelsList channels) {
        try {
            if (channels != null) {
                // process channels response
                boolean show_num = settings_.getValue(Settings.SHOW_CHANNEL_NUMBER_KEY, true);
                if (channel_container != null)
                    channel_container.removeAllViews();
                if (epg_container != null)
                    epg_container.removeAllViews();
                for (int idx = 0; idx < channels.size(); idx++) {
                    Channel ch = channels.get(idx);

                    ChannelRef channel_ref = new ChannelRef();

                    // insert channel in channel list
                    ChannelItem channel = (ChannelItem) getLayoutInflater().inflate(R.layout.channel_item, channel_container, false);
                    channel.setChannel(ch, show_num);
                    channel.setTag(idx);

                    channel_container.addView(channel);
                    channel_ref.channel_item = channel;
                    channel_container.requestLayout();

                    // insert placeholder for epg data into epg container
                    LinearLayout ll = new LinearLayout(this);
                    ll.setOrientation(LinearLayout.HORIZONTAL);
                    epg_container.addView(ll, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                            layout_item_height));

                    channel_ref.epg_container = ll;

                    // add channel to channel ref structure
                    channel_display_map.put(ch.ID, channel_ref);
                    channel_id_list_.add(ch.ID);

                    channel.setOnClickListener(this);
                }
                response_ = null;
            }
        } catch (Exception e) {
            String err = e.getMessage();
            System.out.println(err);
            Log.e(TAG, "processChannels", e);
        }
    }

    void getSettings() {
        useProgramColors = settings_.getValue(Settings.USE_PROGRAM_COLORS_KEY, true);
        sort_mode = settings_.getChannelSortMode();
    }

    void clearProgramInfoPane() {
        ProgramDetails info = (ProgramDetails) findViewById(R.id.program_details_pane);
        info.clear(null);
        info.setVisibility(View.INVISIBLE);
    }

    protected void changeDate(int dx) {
        if (dx == 0)
            return;
        Rect outRect = new Rect();
        guide_scroller.getDrawingRect(outRect);
        long time_left = TimeLineBuilder.PixToGridTime(this, outRect.left);
        setDateLabelText(time_left);
    }

    protected void checkScrollData() {
        if (scroller_data_  != null && scroller_data_.active) {
            if (scroller_data_.stop_pos != -1 && scroller_data_.stop_pos == scroller_data_.scroll_pos) {
                if (state_ == EGAS_READY) {
                    Rect outRect = new Rect();
                    guide_scroller.getDrawingRect(outRect);
                    if (scroller_data_.stop_pos < scroller_data_.start_pos) {
                        int tmp = scroller_data_.start_pos;
                        scroller_data_.start_pos = scroller_data_.stop_pos;
                        scroller_data_.stop_pos = tmp;
                    }
                    boolean need_load = true;
                    if (need_load) {
                        int start_x = outRect.left;
                        long start_time = TimeLineBuilder.PixToGridTime(this, start_x);
                        long end_time = TimeLineBuilder.PixToGridTime(this, outRect.right) + epg_loading_step;

                        background_epg_updater_.add_update(start_time, end_time, null);
                    }
                    scroller_data_.reset();
                }
            }
            scroller_data_.stop_pos = scroller_data_.scroll_pos;
        }
    }

    protected void processAuthorizationError() {
        activateProgressBar(false);
        state_ = EGAS_READY;

        if (language_manager_ != null) {
            String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
            Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        }

        //launch settings activity
        startSettingsActivity();
    }

    protected void processDataProviderError() {
        try {
            if (language_manager_ != null) {
                String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
                Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
            }
            state_ = EGAS_READY;
            activateProgressBar(false);

        } catch(Exception e) {
        }
    }

    protected HashMap<String, Program>  extractCurrentProgramsEx() {
        return extractCurrentProgramsEx(Utils.CurrentUtc());
    }

    protected HashMap<String, Program> extractCurrentProgramsEx(long current_time) {
        HashMap<String, Program> result = new HashMap<String, Program>();
        int cnt = epg_container.getChildCount();
        for (int idx = 0; idx < cnt; idx++) {
            LinearLayout ll = (LinearLayout) getChannelEpg(idx);
            View vv = ll.getChildAt(0);
            if (vv instanceof GuideBar) {
                GuideBar bar = (GuideBar) vv;
                GuideBar.GuideBarItem it = bar.getItemForTime(current_time);
                if (it != null && it.prg_.ID != null)
                    result.put(bar.channel_id_, it.prg_);
            }
        }
        return result;
    }

    protected void sortChannels(ChannelsList channels, Settings.EChannelSortMode mode) {
        Utils.sortChannels(channels, mode);
    }

    protected void prepareTimelineAndScroll() {
        // stop position update thread
        stopNowThread();

        long start_time = Utils.CurrentUtc();
        buildTimeLine(start_time - epg_start_offset, start_time+guidebar_length-epg_start_offset);

    }

    protected void eraseEPG(String channel_id) {
        Pair<Long, Long> pair = TimeLineBuilder.getBounds();
        if (channel_id == null) {
            for (Map.Entry<String, ChannelRef> entry : channel_display_map.entrySet()) {
                ChannelRef ch = entry.getValue();
                ch.epg_container.removeAllViews();
                // add a single empty epg item in each channel epg container
                // until real data arrives
                GuideBar guide_bar = new GuideBar(this, this, ch.channel_item.channel_.ID, null);
                long two_weeks = pair.first + guidebar_length;
                ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                        TimeLineBuilder.GridTimePxPosition(this, two_weeks),
                        ViewGroup.LayoutParams.WRAP_CONTENT);
                ch.epg_container.addView(guide_bar, params);
                guide_bar.setOnTouchListener(this);
                guide_bar.setOnKeyListener(guide_bar);
            }
        } else {
            channel_display_map.get(channel_id).epg_container.removeAllViews();
            channel_display_map.get(channel_id).epg_container.addView(
                    new GuideBar(this, this, channel_id, null),
                    new ViewGroup.LayoutParams(TimeLineBuilder.GridTimePxPosition(this, pair.first+guidebar_length),
                            ViewGroup.LayoutParams.MATCH_PARENT));
        }
    }

    public void requestEpg() {
        Rect outRect = new Rect();
        //we load here one screen back and epg_loading_step to the future from right side of the screen
        guide_scroller.getDrawingRect(outRect);
        int start_x = outRect.left;
        long start_time = TimeLineBuilder.PixToGridTime(this, start_x);

        int endx_x = outRect.right;
        long end_time = TimeLineBuilder.PixToGridTime(this, endx_x) + epg_loading_step;

        background_epg_updater_.add_update(start_time, end_time, null);
    }

    protected void buildTimeLine(long start_time, long end_time) {
        TimeLineBuilder.InitTimeline(this, start_time, end_time);

        TimeLine time_line = (TimeLine) this.findViewById(R.id.time_line);
        time_line.create_time_line(start_time, end_time);

        //scroll to now and start update thread
        Pair<Long, Long> pair = TimeLineBuilder.getBounds();
        final int now_offs = TimeLineBuilder.GetNowOffset(this, pair.first);
        epg_container.set_now_coord(now_offs);

        // start position update thread
        now_thread_exit_flag_ = false;
        now_watch_thread_ = new Thread(new Runnable() {
            public void run() {
                nowWatchThread();
            }
        });
        now_watch_thread_.start();
    }

    protected void updateEpgInt(ChannelIdWithPrograms ciwp) {
        // find channel reference structure
        if (channel_display_map.containsKey(ciwp.ChannelId)) {
            ChannelRef channel_ref = channel_display_map.get(ciwp.ChannelId);
            if (channel_ref.epg_container.getChildCount() != 0) {
                Object child = channel_ref.epg_container.getChildAt(0);
                if (child instanceof GuideBar) {
                    GuideBar guide_bar = (GuideBar) child;
                    guide_bar.addEpg(ciwp.Programs);
                } else {
                    channel_ref.epg_container.removeAllViews();
                    GuideBar guide_bar = new GuideBar(this, this, ciwp.ChannelId, ciwp.Programs);
                    ViewGroup.LayoutParams params = new ViewGroup.LayoutParams(
                            TimeLineBuilder.GridTimeToPxWidth(this, TimeLineBuilder.startTime(), TimeLineBuilder.endTime()),
                            ViewGroup.LayoutParams.WRAP_CONTENT);
                    channel_ref.epg_container.addView(guide_bar, params);
                    guide_bar.setOnTouchListener(this);
                }
            }

            //update program information pane
            if (active_channel_index >=0 && active_channel_index < channel_container.getChildCount()) {
                ChannelItem item = (ChannelItem) channel_container.getChildAt(active_channel_index);
                if (item.channel_.ID.equalsIgnoreCase(ciwp.ChannelId)) {
                    if (channel_container.hasFocus()) {
                        fillItemDetails(active_channel_index);
                    } else {
                        LinearLayout ll = (LinearLayout)getChannelEpg(active_channel_index);
                        View vv = ll.getChildAt(0);
                        if (vv.hasFocus()) {
                            GuideBar bar = (GuideBar) vv;
                            if (active_cell_index >=0 && active_cell_index < bar.items_.size()) {
                                GuideBar.GuideBarItem bar_item = bar.items_.get(active_cell_index);
                                fillItemDetails(bar, bar_item);
                            }
                        }

                    }
                }
            }
        }
    }

    public void updateEpg(ChannelIdWithProgramsList epg_data, int channel_idx, long start_time, long end_time) {

        Log.i("GuideActivity", "updateEpg (" + channel_idx + ")");

        try {
            if (channel_display_map != null) {
                if (channel_idx >= 0) {
                    ChannelIdWithPrograms ciwp = epg_data.get(channel_idx);
                    updateEpgInt(ciwp);
                } else {
                    for (int idx = 0; idx < epg_data.size(); idx++) {
                        ChannelIdWithPrograms ciwp = epg_data.get(idx);
                        updateEpgInt(ciwp);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public GuideBar getActiveGuideBar() {
        LinearLayout ll = (LinearLayout) getChannelEpg(active_channel_index);
        return (GuideBar) ll.getChildAt(0);
    }

    protected void startDatePicker() {

        DatePickerDialog.OnDateSetListener listener = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                try {
                    onDatePicker(year, monthOfYear, dayOfMonth);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

        };

        try {
            TextView tv = (TextView) findViewById(R.id.date_label);
            Date dt = (Date) tv.getTag();
            Calendar cal = Calendar.getInstance(language_manager_.GetLocale());
            cal.setTime(dt);

            String title = language_manager_.GetString(LocaleStrings.IDS_DATES_PAGE_TITLE);
            final CustomDatePickerDialog dlg = new CustomDatePickerDialog(this, listener,
                    cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

            dlg.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                    //if (keyCode == KeyEvent.KEYCODE_BACK) {
                    {
                        DatePicker dp = dlg.getDatePicker();
                        View focus = dp.getFocusedChild();
                        int count = dp.getChildCount();
                        for (int idx=0; idx<count; idx++) {
                            View child = dp.getChildAt(idx);
                            int id = child.getId();
                        }
                    }
                    return false;
                }
            });

            Pair<Long, Long> pair = TimeLineBuilder.getBounds();
            dlg.getDatePicker().setMinDate(pair.first * 1000);
            dlg.getDatePicker().setMaxDate((pair.first + guidebar_length) * 1000);
            dlg.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean useProgramColors() {
        return useProgramColors;
    }

    void setDateLabelText(long utc_sec) {
        TextView date_label = (TextView) findViewById(R.id.date_label);
        String strDate = Utils.UtcToMediumDateString(this, utc_sec);
        date_label.setText(strDate);
        date_label.setTag(utc_sec);
    }

    void setDateLabelText() {
        setDateLabelText(Utils.CurrentUtc());
    }

    public Favorite getSelectedFavorite() {
        Favorite favorite = null;
        if (favorites_list != null) {
            for (int fav_idx = 0; fav_idx < favorites_list.size(); fav_idx++) {
                if (favorites_list.get(fav_idx).ID.equals(selectedFavorite)) {
                    favorite = favorites_list.get(fav_idx);
                    break;
                }
            }
        }
        return favorite;
    }


    protected void enableFavoritesSpinner() {
        if (favorites_list == null) {
            _status_line.setField(1, null, null, StatusLine.status_icon_favorites);
        } else {

            Favorite favorite = getSelectedFavorite();

            if (favorite != null) {
                final String text = language_manager_.GetString(LocaleStrings.IDS_FAVORITES) + ": ";
                _status_line.setField(1, text, favorite.Name, StatusLine.status_icon_favorites);
            } else {
                _status_line.setField(1, null, null, StatusLine.status_icon_favorites);
            }
        }
    }

    protected void applyFavorites() {
        if (favorites_list == null)
            return;

        //find selected favorite (if it exists at all)
        Favorite favorite = getSelectedFavorite();

        if (favorite != null) {
            for (Map.Entry<String, ChannelRef> entry : channel_display_map.entrySet()) {
                String id = entry.getKey();
                boolean visible = favorite.contains(id);
                ChannelRef ref = (ChannelRef) entry.getValue();
                ref.channel_item.setVisibility(visible ? View.VISIBLE : View.GONE);
                ref.epg_container.setVisibility(visible ? View.VISIBLE : View.GONE);
                ref.epg_container.setFocusable(false);
            }
        } else {
            for (Map.Entry<String, ChannelRef> entry : channel_display_map.entrySet()) {
                ChannelRef ref = (ChannelRef) entry.getValue();
                ref.channel_item.setVisibility(View.VISIBLE);
                ref.epg_container.setVisibility(View.VISIBLE);
                ref.epg_container.setFocusable(true);
            }
        }

        enableFavoritesSpinner();
    }

    void restoreSelectedFavorite() {
        String default_favorite = language_manager_.GetString(LocaleStrings.IDS_NONE);

        selectedFavorite = settings_.getValue("selected_favorite_id", default_favorite);
    }

    public View getChannelEpg(int idx) {
        int cnt = channel_container.getChildCount();
        if(idx >= 0 && idx < cnt) {
            ChannelItem item = (ChannelItem) channel_container.getChildAt(idx);
            return channel_display_map.get(item.channel_.ID).epg_container;
        }
        return null;
    }

    void skipTimeoutThread() {
        if (connection_timeout_thread != null && !connection_timeout_thread.isInterrupted()) {
            Thread thread = connection_timeout_thread;
            connection_timeout_thread = null;
            thread.interrupt();
        }
    }

    protected void fillItemDetails(int ii) {
        LinearLayout ll = (LinearLayout)getChannelEpg(active_channel_index);
        View vv = ll.getChildAt(0);
        if (vv instanceof GuideBar) {
            GuideBar bar = (GuideBar) vv;
            long current_time = Utils.CurrentUtc();
            GuideBar.GuideBarItem item = bar.getItemForTime(current_time);
            fillItemDetails(bar, item);
        } else {
            clearItemDetails();
        }
    }

    protected void fillItemDetails(GuideBar bar, GuideBar.GuideBarItem item) {
        if (item != null) {
            if (Utils.usesRemoteControl(this)) {
                if (programDetailsLoader != null)
                    programDetailsLoader.clear();
                findViewById(R.id.program_details).setVisibility(View.VISIBLE);
                ChannelItem ci = (ChannelItem) channel_container.getChildAt(active_channel_index);
                if (programDetailsLoader == null)
                    programDetailsLoader = new ProgramDetailsLoader(this);

                programDetailsLoader.set_program(ci.channel_.ID, bar, item);
            }
        } else {
            clearItemDetails();
        }
    }

    protected void clearItemDetails() {
        if (programDetailsLoader != null)
            programDetailsLoader.clear();

        ProgramDetails dpi = (ProgramDetails) findViewById(R.id.program_details_pane);
        dpi.clear(language_manager_.GetString(LocaleStrings.IDS_NO_PROGINFO_AVAILABLE));
    }

    public void getChannelsForEPGUpdate(long start_time, long end_time) {

        long time_to_check = BackgroundEPGUpdater.invalid_time_value;
        long start_time_l;
        long end_time_l;

        if (start_time == BackgroundEPGUpdater.invalid_time_value || end_time == BackgroundEPGUpdater.invalid_time_value) {
            time_to_check = Utils.CurrentUtc();
            start_time_l = Math.max(TimeLineBuilder.startTime(), Utils.CurrentUtc() - 2 * epg_start_offset);
            end_time_l = start_time_l + epg_loading_step;
        } else {
            start_time_l = start_time;
            end_time_l = end_time;
        }

        int visible_channel_idx = -1;
        ChannelIDList channels = new ChannelIDList();
        for (int i=0; i<channel_container.getChildCount(); i++) {
            ChannelItem ch = (ChannelItem) channel_container.getChildAt(i);
            LinearLayout ll = (LinearLayout)channel_display_map.get(ch.channel_.ID).epg_container;

            int old_channels_size = channels.size();

            View vv = ll.getChildAt(0);
            if (vv instanceof GuideBar) {
                GuideBar bar = (GuideBar) vv;
                GuideBar.GuideBarItem item;
                //time to check
                if (time_to_check != BackgroundEPGUpdater.invalid_time_value) {
                    item = bar.getItemForTime(time_to_check);
                    if (item == null || item.is_empty_)
                        channels.add(ch.channel_.ID);
                } else {
                    //check both start and end time of the period
                    item = bar.getItemForTime(end_time_l);
                    if (item == null || item.is_empty_) {
                        channels.add(ch.channel_.ID);
                    } else {
                        item = bar.getItemForTime(start_time_l);
                        if (item == null || item.is_empty_)
                            channels.add(ch.channel_.ID);
                    }
                }
            }
            if (channels.size() != old_channels_size) {
                if (i == active_channel_index && channels.size() > 0)
                    visible_channel_idx = channels.size() - 1;
            }
        }

        //sort channels, so that the visible channel plus/minus 4 is at the top of the list
        HashMap<String, String> visible_channels = null;
        if (visible_channel_idx != -1) {

            visible_channels = new HashMap<>();
            for (int i = visible_channel_idx - 5; i<=visible_channel_idx + 5; i++) {
                if (i >= 0 && i < channels.size())
                    visible_channels.put(channels.get(i), channels.get(i));
            }
        }

        background_epg_updater_.getEpgForChannels(channels, visible_channels, start_time_l, end_time_l);
    }

    public void processEPGDataUpdate(ChannelIdWithProgramsList epg_data, int currently_updated_channel_idx, long start_time, long end_time) {
        updateEpg(epg_data, currently_updated_channel_idx, start_time, end_time);
    }

    public void updateStarted() {
        _status_line.showLoadIndicator();
    }

    public void updateFinished() {
        _status_line.hideLoadIndicator();
    }

    protected void checkAndScrollToNow() {

        if (guide_scroller != null && epg_container != null) {

            long now_offs = TimeLineBuilder.GridTimePxPosition(this, Utils.CurrentUtc());

            Rect r = new Rect();
            epg_container.getLocalVisibleRect(r);

            if (now_offs < r.left || now_offs > r.right) {
                //scroll the view
                long time_to_scroll = Utils.CurrentUtc() - epg_start_offset;
                int offs = TimeLineBuilder.GridTimePxPosition(this, time_to_scroll);
                if (offs >= 0 && offs < epg_container.getRight())
                    guide_scroller.scrollTo(offs, guide_scroller.getScrollY());
            }
        }
    }

    private void checkTuner() {
            requestChannels();
    }

    // region Adviser

    private class GuideAdviserProvider extends AdviserProvider {
        public GuideAdviserProvider() {
            super(_activity, serverSettings, language_manager_.getID());
        }

        @Override
        protected void onMappedChannels(MappedChannelsResponse mappedChannels) {
            Log.i(TAG, "processMappedChannels");

            //check if this is a valid class
            if (language_manager_ != null) {
                if (mappedChannels.size() > 0) {
                    GuideActivity.this.mappedChannels = mappedChannels;
                    requestProfiles();
                } else {
                    lastInitStep();
                }
            }
        }

        @Override
        protected void onProfiles(ProfilesResponse profilesResponse) {
            if (profiles == null) {
                profiles = new ArrayList<>();
            }
            profiles.clear();

            if (profilesResponse != null) {
                ProfileRecommendation recommendation;
                for (ProviderProfilesList providerProfiles: profilesResponse.list()) {
                    for (Profile profile: providerProfiles.list()) {
                        recommendation = new ProfileRecommendation();
                        recommendation.providerId = providerProfiles.ProviderId;
                        recommendation.profile = profile;
                        profiles.add(recommendation);
                    }
                }
                Collections.sort(profiles, new Comparator<ProfileRecommendation>() {
                    @Override
                    public int compare(ProfileRecommendation o1, ProfileRecommendation o2) {
                        return o1.profile.ProfileName.compareToIgnoreCase(o2.profile.ProfileName);
                    }
                });

                correctMenuForRecommendations();

                if (isCreatedProfile) {
                    isCreatedProfile = false;
                    clearRecommendations();
                    requestRecommendBySelectedProfile();
                }
            }

            lastInitStep();
        }

        @Override
        protected void onRecommendByProfiles(RecommendationsList recommendations) {
            for (Recommendation recommendation : recommendations.list()) {
                String long_channel_id = recommendation.DVBLinkID;
                String channel_id = getChannelId(long_channel_id);
                ChannelRef channel_ref = channel_display_map.get(channel_id);
                for (int idx = 0; idx < channel_ref.epg_container.getChildCount(); idx++) {
                    View child = channel_ref.epg_container.getChildAt(idx);
                    if (child instanceof GuideBar) {
                        GuideBar bar = (GuideBar) child;
                        if (bar.channel_id_.equals(channel_id)) {
                            bar.setRecommendations(recommendation);
                        }
                    }
                }
            }
            epg_container.invalidate();
        }

        private boolean isCreatedProfile = false;

        @Override
        protected void onCreateProfile(ProfileId profile) {
            newSelectedProfile = profile.ProfileId;
            isCreatedProfile = true;
            guideAdviserProvider.requestProfiles();
        }

        @Override
        protected void onLikeThis(RecommendationsList recommendations) {
            onRecommendByProfiles(recommendations);
            createMenu();
        }
    }

    private void requestRecommendBySelectedProfile() {
        Rect outRect = new Rect();
        guide_scroller.getDrawingRect(outRect);
        int startX = outRect.left;
        long startTime = TimeLineBuilder.PixToGridTime(this, startX);
        long endTime  = startTime  + (3 * seconds_per_day);

        guideAdviserProvider.requestRecommendByProfile(newSelectedProfile, startTime, endTime);
    }

    boolean enableCreateProfile(String longChannelID) {
        return isChannelMapped(longChannelID, null);
    }

    boolean isChannelMapped(String longChannelID, String providerID) {
        if (mappedChannels != null) {
            for (MappedChannelList channels : mappedChannels.list()) {
                if (providerID == null || channels.ProviderId.equals(providerID)) {
                    for (MappedChannel channel : channels.list()) {
                        if (channel.DVBLinkID.equals(longChannelID)) {
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    protected void clearRecommendations() {
        for (Map.Entry<String, ChannelRef> entry : channel_display_map.entrySet()) {
            ChannelRef channel_ref = entry.getValue();
            for (int idx = 0; idx < channel_ref.epg_container.getChildCount(); idx++) {
                View child = channel_ref.epg_container.getChildAt(idx);
                GuideBar bar = (GuideBar) child;
                bar.clearRecommendations();
            }
        }
    }

    public boolean isAdviserMode() {
        return (!newSelectedProfile.isEmpty() || likeThisMode);
    }

    protected void startRecommendationListEx() {
        Intent intent = new Intent(this, RecommendationListExActivity.class);
        ChannelsList channels = new ChannelsList();
        for (Map.Entry<String, ChannelRef> entry: channel_display_map.entrySet()) {
            channels.add(entry.getValue().channel_item.channel_);
        }

        intent.putExtra("channels", Serializer.serialize(channels));
        if (likeThisMode) {
            intent.putExtra("like_this_dvblink_channel_id", likeThisChannelID);
            intent.putExtra("like_this_start_time", likeThisStartTime);
            intent.putExtra("like_this_end_time", likeThisEndTime);
        } else {
            intent.putExtra("profile_id", newSelectedProfile);
        }

        startActivityForResult(intent, (int) list_activity_req_code);
    }

    protected void requestOobChannelUrl(String channel_id) {
        if (data_provider_ != null && state_ == EGAS_READY) {
            state_ = EGAS_REQUESTING_OOB_URL;
            activateProgressBar(true);
            data_provider_.GetOobChannelUrl(new OobChannelUrlRequest(channel_id, OobChannelUrlRequest.OobChannelUrlFormatChromcast));
        }
    }

    protected void processOobChannelUrl() {
        state_ = EGAS_READY;
        activateProgressBar(false);

        if (response_ != null) {
            String channel_url = null;
            String mime = null;
            try {
                if (response_ instanceof OobChannelUrl) {
                    OobChannelUrl oob = (OobChannelUrl) response_;
                    channel_url = oob.url;
                    mime = oob.mime;
                }
            } catch (Exception e) {
                channel_url = null;
                mime = null;
            }

            if (channel_url != null && mime != null)
                startChromecastChannel(pending_channel_id, channel_url, mime);
        } else {
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_NO_OOB_CHANNEL_URL), Toast.LENGTH_LONG).show();
        }
    }

    protected void startChromecastChannel(String channel_id, String channel_url, String mime) {
        if (channel_url != null && mCastSession != null) {
            RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {
                //here we use dvblink channel id as a channel id without lookup. works in TVMosaic, does not work in DVBLink
                if (channel_display_map.containsKey(channel_id)) {
                    ChannelRef cr = channel_display_map.get(channel_id);

                    String chnum_str = Utils.formatChannelNumber(cr.channel_item.channel_);
                    MediaMetadata mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
                    mediaMetadata.putString(MediaMetadata.KEY_TITLE, chnum_str.isEmpty() ? cr.channel_item.channel_.Name : chnum_str + " - " + cr.channel_item.channel_.Name);
                    if (cr.channel_item.channel_.ChannelLogo != null && !cr.channel_item.channel_.ChannelLogo.isEmpty())
                        mediaMetadata.addImage(new WebImage(Uri.parse(cr.channel_item.channel_.ChannelLogo)));

                    MediaInfo mi = new MediaInfo.Builder(channel_url)
                            .setContentType(mime)
                            .setStreamType(MediaInfo.STREAM_TYPE_LIVE)
                            .setMetadata(mediaMetadata).build();

                    remoteMediaClient.addListener(new TVMRemoteMediaClientListener(mCastSession));

                    remoteMediaClient.load(mi);
                }
            }
        }
    }

    protected void requestDirectUrl(String channel_id) {
        if (data_provider_ != null && state_ == EGAS_READY) {
            DVBLinkChannelIdList idlist = new DVBLinkChannelIdList();
            idlist.add(channel_id);
            StreamInfoRequest rs = new StreamInfoRequest(serverSettings.serverAddress, settings_.getClientID(),
                    idlist);
            state_ = EGAS_REQUESTING_DIRECT_URL;
            activateProgressBar(true);
            data_provider_.GetStreamInfo(rs);
        }
    }

    protected void processDirectUrl() {
        state_ = EGAS_READY;
        activateProgressBar(false);

        String direct_channel_url = null;
        try {
            if (response_ != null && response_ instanceof StreamInfoList) {
                StreamInfoList stream_info = (StreamInfoList) response_;
                if (stream_info.size() > 0) {
                    direct_channel_url = stream_info.get(0).Url;
                }
            }
        } catch (Exception e) {
        }

        long chromecast_bitrate = settings_.getValue(Settings.CHROMECAST_BITRATE_KBS_KEY, Settings.CHROMECAST_BITRATE_KBS_DEFAULT);
        if (direct_channel_url != null)
            direct_channel_url += "&transcoder=mp4&bitrate=" + chromecast_bitrate;

        startChromecastChannel(pending_channel_id, direct_channel_url, "video/mpeg");
    }

    // endregion
}

