package com.dvblogic.tvmosaic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

import TVMosaic.TVMosaic.R;


public class ActionPaneRow extends RelativeLayout {

    Paint paint_ = null;

    public ActionPaneRow(Context context) {
        super(context);
    }

    public ActionPaneRow(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ActionPaneRow(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (paint_ == null)
            paint_ = new Paint();

        paint_.setStyle(Paint.Style.FILL_AND_STROKE);

        if (isSelected()) {
            int top_bottom_offs = (int)getContext().getResources().getDimension(R.dimen.channel_contents_top_bottom_offs);
            int left_right_offs = 0;
            Rect rt = new Rect(left_right_offs, top_bottom_offs, getWidth() - left_right_offs - 1, getHeight() - top_bottom_offs - 1);
            paint_.setColor(ContextCompat.getColor(getContext(), R.color.settings_bkg_selected));
            canvas.drawRect(rt, paint_);
        }

    }

}
