package com.dvblogic.tvmosaic;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import TVMosaic.TVMosaic.R;
import android.widget.Toast;
import com.dvblogic.dvblink_common.Settings;
import com.dvblogic.dvblink_common.StatusCode;
import com.dvblogic.dvblink_common.Utils;

import java.util.ArrayList;

public class TranscodingSettingsActivity extends BaseSettingsActivity {
    private static final String TAG = "TranscodingSettingsActivity";

    private ArrayList<Integer> bitrates_;
    private ArrayList<Integer> chromecast_bitrates_;
    private ArrayList<Integer> scale_factors_;

    @Override
    protected void PreCreate() {
        bitrates_ = new ArrayList<>();
        bitrates_.add(384);
        bitrates_.add(512);
        bitrates_.add(768);
        bitrates_.add(1024);
        bitrates_.add(1536);
        bitrates_.add(2048);

        chromecast_bitrates_ = new ArrayList<>();
        chromecast_bitrates_.add(512);
        chromecast_bitrates_.add(1024);
        chromecast_bitrates_.add(2048);
        chromecast_bitrates_.add(4096);
        chromecast_bitrates_.add(8192);

        scale_factors_ = new ArrayList<>();
        scale_factors_.add(1);
        scale_factors_.add(2);
        scale_factors_.add(4);
    }

    protected <T> T getNextObjectFromList(ArrayList<T> list, T current_value) {
        T obj = null;

        int idx = list.indexOf(current_value);
        if (idx == -1 || idx == list.size() - 1)
            idx = 0;
        else
            idx += 1;

        obj = list.get(idx);

        return obj;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.transcoding_settings_activity;
    }

    @Override
    protected void customizeList() {
        customizeTranscodingSettings();
        headBar.setText(language_manager_.GetString(LocaleStrings.IDS_TRANSCODING));
        super.customizeList();
    }

    private String getOnOffFromBoolean(boolean isOn) {
        return language_manager_.GetString(isOn ? LocaleStrings.IDS_ON : LocaleStrings.IDS_OFF);
    }

    private void customizeTranscodingSettings() {
        RelativeLayout general_category = (RelativeLayout) findViewById(R.id.general_category);
        customizeCategory(general_category, LocaleStrings.IDS_TRANSCODER_GENERAL_SETTINGS, LocaleStrings.IDS_TRANSCODER_GENERAL_SETTINGS_DESC);

        // transcoding enabled
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.transcoding_enabled);
        TextView tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_TRANSCODING_ENABLED));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_TRANSCODING_ENABLED_DESC));

        boolean onoff = settings_.getValue(Settings.TRANSCODING_ENABLED_KEY, Settings.TRANSCODING_ENABLED_DEFAULT);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(getOnOffFromBoolean(onoff));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                boolean onoff = !settings_.getValue(Settings.TRANSCODING_ENABLED_KEY, Settings.TRANSCODING_ENABLED_DEFAULT);
                settings_.setValue(Settings.TRANSCODING_ENABLED_KEY, onoff);
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.transcoding_enabled);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);
                tv.setText(getOnOffFromBoolean(onoff));
            }
        });

        // bitrate
        layout = (RelativeLayout) findViewById(R.id.bitrate_spinner);
        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_BITRATE_TITLE));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_BITRATE_DESC));

        long bitrate = settings_.getValue(Settings.BITRATE_KEY, Settings.BITRATE_DEFAULT);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(String.valueOf(bitrate));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.bitrate_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);

                long bitrate = settings_.getValue(Settings.BITRATE_KEY, Settings.BITRATE_DEFAULT);
                bitrate = getNextObjectFromList(bitrates_, (int)bitrate);
                settings_.setValue(Settings.BITRATE_KEY, bitrate);

                tv.setText(String.valueOf(bitrate));
            }
        });

        // scale factor
        layout = (RelativeLayout) findViewById(R.id.resolution_spinner);
        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SCREEN_RESOLUTION));
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SCREEN_RESOLUTION_DESC));

        long scale_factor = settings_.getValue(Settings.VIDEO_SCALING_KEY, Settings.VIDEO_SCALING_DEFAULT);
        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(String.format("1 : %d", scale_factor));

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RelativeLayout layout = (RelativeLayout) findViewById(R.id.resolution_spinner);
                TextView tv = (TextView) layout.findViewById(R.id.spinner_label);

                long scale_factor = settings_.getValue(Settings.VIDEO_SCALING_KEY, Settings.VIDEO_SCALING_DEFAULT);
                scale_factor = getNextObjectFromList(scale_factors_, (int)scale_factor);
                settings_.setValue(Settings.VIDEO_SCALING_KEY, scale_factor);

                tv.setText(String.format("1 : %d", scale_factor));
            }
        });

        if (!Utils.usesRemoteControl(_context)) {
            RelativeLayout chromecast_category = (RelativeLayout) findViewById(R.id.chromecast_settings);
            customizeCategory(chromecast_category, LocaleStrings.IDS_CHRROMECAST_GENERAL_SETTINGS, LocaleStrings.IDS_CHRROMECAST_GENERAL_SETTINGS_DESC);

            // цчромецаст bitrate
            layout = (RelativeLayout) findViewById(R.id.chromecast_bitrate_spinner);
            tv = (TextView) layout.findViewById(R.id.title_text);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_BITRATE_TITLE));
            tv = (TextView) layout.findViewById(R.id.desc_text);
            tv.setText(language_manager_.GetString(LocaleStrings.IDS_BITRATE_DESC));

            long chromecast_bitrate = settings_.getValue(Settings.CHROMECAST_BITRATE_KBS_KEY, Settings.CHROMECAST_BITRATE_KBS_DEFAULT);
            tv = (TextView) layout.findViewById(R.id.spinner_label);
            tv.setText(String.valueOf(chromecast_bitrate));

            layout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    RelativeLayout layout = (RelativeLayout) findViewById(R.id.chromecast_bitrate_spinner);
                    TextView tv = (TextView) layout.findViewById(R.id.spinner_label);

                    long chromecast_bitrate = settings_.getValue(Settings.CHROMECAST_BITRATE_KBS_KEY, Settings.CHROMECAST_BITRATE_KBS_DEFAULT);
                    chromecast_bitrate = getNextObjectFromList(chromecast_bitrates_, (int) chromecast_bitrate);
                    settings_.setValue(Settings.CHROMECAST_BITRATE_KBS_KEY, chromecast_bitrate);

                    tv.setText(String.valueOf(chromecast_bitrate));
                }
            });
        } else {
            //hide those controls
            RelativeLayout chromecast_category = (RelativeLayout) findViewById(R.id.chromecast_settings);
            chromecast_category.setVisibility(View.GONE);
            layout = (RelativeLayout) findViewById(R.id.chromecast_bitrate_spinner);
            layout.setVisibility(View.GONE);
        }

    }

}
