package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;

import TVMosaic.TVMosaic.R;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.*;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.dvblogic.dvblink_common.*;

import java.util.*;


public class DropdownMenu {
    private class DropdownMenuItemsAdapter extends BaseAdapter {
        private LayoutInflater ltInflater;
        private ArrayList<Pair<String, String>> items = new ArrayList<>();
        private ArrayList<String> checkedItems = new ArrayList<>();
        DropdownMenu parent_;

        public DropdownMenuItemsAdapter(ArrayList<Pair<String, String>> items, DropdownMenu parent) {
            this.items = items;
            ltInflater = activity.getLayoutInflater();
            parent_ = parent;
        }

        @Override
        public int getCount() {
            return items.size();
        }

        private class checkBoxClickListener implements View.OnClickListener {
            private final int mIndex;

            private checkBoxClickListener(int index) {
                mIndex = index;
            }

            @Override
            public void onClick(View v) {
                parent_.processChildClick(mIndex);
            }
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ltInflater.inflate(R.layout.search_dropdown_item, parent, false);
            }
            Pair<String, String> item = items.get(position);
            CheckBox checkBox = (CheckBox) view.findViewById(R.id.checkbox);
            checkBox.setText(item.first);
            checkBox.setChecked(checkedItems.contains(item.second));
            checkBox.setTag(item.second);
            checkBox.setMaxLines(1);
            checkBox.setEllipsize(TextUtils.TruncateAt.END);
            checkBox.setOnClickListener(new checkBoxClickListener(position));
            return view;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return items.get(position);
        }

        public void toggleSelectedCheckbox(int position, boolean singleSelect) {
            Pair<String, String> item = items.get(position);
            String id = item.second;
//			if (checkedItems.contains(id)) {
//				if (!singleSelect) {
//                    checkedItems.remove(id);
//                }
//			} else {
//				checkedItems.add(id);
//			}
//			notifyDataSetChanged();
            toogleItem(id, singleSelect);
        }

        public void toogleItem(String id, boolean singleSelect) {
            if (checkedItems.contains(id)) {
                if (!singleSelect) {
                    checkedItems.remove(id);
                }
            } else {
                checkedItems.add(id);
            }
            notifyDataSetChanged();
        }

        public void clearChecked() {
            checkedItems.clear();
            notifyDataSetChanged();
        }

        public ArrayList<String> getCheckedItems() {
            return checkedItems;
        }
    }

    public interface OnItemToggle {
        void onItemsChanged(ArrayList<String> checkedItems, boolean self);
    }

    private BaseActivity activity;

    private CompoundButton toggleButton;
    private Rect toggleButtonRect = new Rect();;
    private int xPosition = -1;
    private Point displaySize = new Point();
    private PopupWindow popupWindow;
    private GridView dropdownList;
    private int menuWidth;
    private boolean singleSelect = false;

    private DropdownMenuItemsAdapter menuAdapter;

    private OnItemToggle onItemToggle;

    public class ArrayItemComparator implements Comparator<Pair<String, String>>
    {
        public int compare(Pair<String, String> left, Pair<String, String> right) {
            return left.first.compareTo(right.first);
        }
    }

    public DropdownMenu(BaseActivity activity, CompoundButton button, ArrayList<Pair<String, String>> items) {
        this(activity, button, items, false);
    }

    public DropdownMenu(BaseActivity activity, CompoundButton button, ArrayList<Pair<String, String>> items, final Boolean singleSelect) {
        toggleButton = button;
        this.singleSelect = singleSelect;
        this.activity = activity;

        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                toggleMenu(toggleButton);
            }
        });
        activity.getWindowManager().getDefaultDisplay().getSize(displaySize);

        LayoutInflater layoutInflater = (LayoutInflater) activity.getSystemService(
                Context.LAYOUT_INFLATER_SERVICE
        );
        LinearLayout layout = (LinearLayout) layoutInflater.inflate(R.layout.dropdown_window, null);

        dropdownList = (GridView) layout.findViewById(R.id.items);
        dropdownList.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean isFocus) {
                if (!isFocus) {
                    dropdownList.setSelection(-1);
                }
            }
        });

        dropdownList.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent event) {
                if (event.getAction() != KeyEvent.ACTION_DOWN) {
                    return false;
                }
                if (Common.okPressed(keyCode)) {
                    processChildClick(dropdownList.getSelectedItemPosition());
                    return true;
                }
                return false;
            }
        });

        Button clearAll = (Button) layout.findViewById(R.id.clear_all);
        if (Utils.usesRemoteControl(activity) && !singleSelect) {
            clearAll.setText(activity.language_manager_.GetString(LocaleStrings.IDS_SEARCH_CLEAR_ALL));
            clearAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    menuAdapter.clearChecked();
                    if (onItemToggle != null) {
                        onItemToggle.onItemsChanged(menuAdapter.getCheckedItems(), false);
                    }
                }
            });
            clearAll.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                    if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_DPAD_UP) {
                        toggleButton.setChecked(false);
                        return true;
                    }
                    return false;
                }
            });
        } else {
            layout.findViewById(R.id.clear_all_divider).setVisibility(View.GONE);
            clearAll.setVisibility(View.GONE);
        }

        menuWidth = (int) activity.getResources().getDimension(R.dimen.search_dropdown_width);
        int menuHeight = (int) activity.getResources().getDimension(R.dimen.search_dropdown_height);


        popupWindow = new PopupWindow(activity);
        popupWindow.setContentView(layout);
        popupWindow.setFocusable(true);

        popupWindow.setHeight(menuHeight);
        popupWindow.setWidth(menuWidth);

        popupWindow.setBackgroundDrawable(new BitmapDrawable(activity.getResources(), (Bitmap) null));
        popupWindow.setOnDismissListener(new PopupWindow.OnDismissListener() {
            @Override
            public void onDismiss() {
                toggleButton.setChecked(false);
            }
        });

        ArrayList<Pair<String, String>> sorted_items = items;
        Collections.sort(sorted_items, new ArrayItemComparator());
        setItems(sorted_items);
    }

    public void processChildClick(int position) {
        if (singleSelect) {
            menuAdapter.clearChecked();
        }
        menuAdapter.toggleSelectedCheckbox(position, singleSelect);

        if (onItemToggle != null) {
            onItemToggle.onItemsChanged(menuAdapter.getCheckedItems(), false);
        }
    }

    public void setItems(ArrayList<Pair<String, String>> items) {
        menuAdapter = new DropdownMenuItemsAdapter(items, this);
    }

    public void setOnItemToggle(OnItemToggle onItemToggle) {
        this.onItemToggle = onItemToggle;
    }

    private void toggleMenu(CompoundButton compoundButton) {
        if (compoundButton.isChecked()) {

            if (xPosition < 0) {
                toggleButton.getGlobalVisibleRect(toggleButtonRect);
                xPosition = (displaySize.x > toggleButtonRect.left + menuWidth) ?
                        toggleButtonRect.left : toggleButtonRect.right - menuWidth;
            }

            popupWindow.showAtLocation(
                    activity.getWindow().getDecorView(), Gravity.NO_GRAVITY, xPosition, toggleButtonRect.bottom
            );
            dropdownList.setAdapter(menuAdapter);
        } else {
            popupWindow.dismiss();
        }
    }

    public ArrayList<String> getCheckedID() {
        return menuAdapter.getCheckedItems();
    }

    public void toggleCheckbox(String id) {
        toggleCheckbox(id, false);
    }

    public void toggleCheckbox(String id, boolean fired) {
        menuAdapter.toogleItem(id, singleSelect);
        if (fired && onItemToggle != null) {
            onItemToggle.onItemsChanged(menuAdapter.getCheckedItems(), true);
        }
    }

    public void hide() {
        popupWindow.dismiss();
    }
}
