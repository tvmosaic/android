package com.dvblogic.tvmosaic;

import TVMosaic.TVMosaic.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.text.InputFilter;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.view.inputmethod.EditorInfo;
import android.widget.*;
import com.dvblogic.dvblink_common.MappedChannelsResponse;
import com.dvblogic.dvblink_common.Profile;
import com.dvblogic.dvblink_common.ProfilesResponse;
import com.dvblogic.dvblink_common.ProviderProfilesList;

import java.util.*;


public class AdviserSettingsActivity extends BaseActivity {

	public static final int RENAME_PROFILE_DIALOG = 1;
	public static final int REMOVE_PROFILE_DIALOG = 2;

    public class ProfileAdapter extends BaseAdapter {

		private LayoutInflater ltInflater;
		private Map<String, String> profiles = new TreeMap<>(new Comparator<String>() {
			@Override
			public int compare(String o1, String o2) {
				return o1.compareToIgnoreCase(o2);
			}
		});

        private ProfileClickListener profileClickListener = new ProfileClickListener();

		private class ProfileClickListener implements View.OnClickListener {
			@Override
			public void onClick(View view) {
				int position = (int) view.getTag();
				String name = getName(position);
				nextView(name, getID(name));
			}
		}

		ProfileAdapter() {
			ltInflater = getLayoutInflater();
		}

		@Override
		public int getCount() {
			return profiles.size();
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			View view = convertView;
			if (view == null) {
				view = ltInflater.inflate(R.layout.settings_link_layout, parent, false);
			}

			String name = getName(position);
			TextView label;
			label = (TextView) view.findViewById(R.id.title_text);
			label.setText(name);

			view.setOnClickListener(profileClickListener);
			view.setTag(position);
			return view;
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@Override
		public Object getItem(int position) {
			return getID(getName(position));
		}

		private String getID(String name) {
			return profiles.get(name);
		}

		private String getName(int position) {
			return (String) profiles.keySet().toArray()[position];
		}

		public void add(Profile profile) {
			profiles.put(profile.ProfileName, profile.ProfileId);
		}
	}

	private class SettingsAdviserProvider extends AdviserProvider {
		public SettingsAdviserProvider() {
			super(AdviserSettingsActivity.this, serverSettings, language_manager_.getID());
		}

		@Override
		protected void onMappedChannels(MappedChannelsResponse mappedChannels) {
			Log.i(TAG, "processMappedChannels");
			if (mappedChannels.size() > 0) {
				requestProfiles();
			}
		}

		@Override
		protected void onProfiles(ProfilesResponse profilesResponse) {
			profileIDList.clear();
			profileList.setAdapter(null);

			if (profilesResponse != null) {
				ProfileAdapter profileAdapter = new ProfileAdapter();

				boolean profilesExists = false;
				for (ProviderProfilesList providerProfiles: profilesResponse.list()) {
					for (Profile profile: providerProfiles.list()) {
						profilesExists = true;
						profileAdapter.add(profile);
					}
				}
				if (!profilesExists) {
					Toast.makeText(_context, "No profiles found", Toast.LENGTH_SHORT).show();
					finish();
				}
				profileList.setAdapter(profileAdapter);
				profileList.setItemsCanFocus(true);
			}
		}

		@Override
		protected void onRemoveProfile() {
			dismissDialog(REMOVE_PROFILE_DIALOG);
			setFlipperDirection(false);
			flipper.showPrevious();
			requestProfiles();
		}

		@Override
		protected void onRenameProfile() {
			dismissDialog(RENAME_PROFILE_DIALOG);
			setFlipperDirection(false);
			flipper.showPrevious();
			requestProfiles();
		}
	}

	private static final String TAG = "AdviserSettingsActivity";

    private SettingsAdviserProvider settingsAdviserProvider;
    private ListView profileList;
	ArrayList<String> profileIDList = new ArrayList<>();

	public AdviserSettingsActivity() {
		show_exit_dialog = false;
	}

    private String currentProfileID;
	private String currentProfileName;

    private EditText nameEditText;

    private ViewFlipper flipper;

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.adviser_settings_activity);

		TextView tv = (TextView)findViewById(R.id.head_bar);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_PAGE_TITLE));

		RelativeLayout rl = (RelativeLayout)findViewById(R.id.adviser_profile_category);
		tv = (TextView)rl.findViewById(R.id.category_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_LINK));
		tv = (TextView)rl.findViewById(R.id.category_desc);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_LINK_DESC));

		View activity = findViewById(R.id.adviser_settings_activity);
		profileList = (ListView) activity.findViewById(R.id.adviser_profile_list);

		View removeProfileLink = findViewById(R.id.remove_profile_link);
		tv = (TextView) removeProfileLink.findViewById(R.id.title_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_REMOVE_LINK));
		tv = (TextView) removeProfileLink.findViewById(R.id.desc_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_REMOVE_LINK_DESC));
		removeProfileLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showDialog(REMOVE_PROFILE_DIALOG);
			}
		});

		View renameProfileLink = findViewById(R.id.rename_profile_link);
		tv = (TextView) renameProfileLink.findViewById(R.id.title_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_RENAME_LINK));
		tv = (TextView) renameProfileLink.findViewById(R.id.desc_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_RENAME_LINK_DESC));
		renameProfileLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				showDialog(RENAME_PROFILE_DIALOG);
			}
		});

		flipper = (ViewFlipper) findViewById(R.id.adviser_settings_flipper);

		settingsAdviserProvider = new SettingsAdviserProvider();
		settingsAdviserProvider.requestMappedChannels();
	}

	public boolean onKeyDown(int keyCode, KeyEvent event) {
		if (event.getAction() != KeyEvent.ACTION_DOWN) {
			return true;
		}

		if (keyCode == 58) {
			return false;
		}

		if (Common.backPressed(keyCode, event)) {
			View view = flipper.getCurrentView();
			int viewID = view.getId();

			if (viewID == R.id.adviser_settings_edit_view) {
				setFlipperDirection(false);
				flipper.showPrevious();
			} else if (viewID == R.id.adviser_settings_main_view) {
				onBackPressed();
			}

			return true;
		}
		return false;
	}

	protected void setFlipperDirection(Boolean forward) {
		flipper.setInAnimation(null);
		flipper.setOutAnimation(null);
		flipper.setInAnimation(AnimationUtils.loadAnimation(_context, forward ? R.anim.scan_in : R.anim.scan_in_rev));
		flipper.setOutAnimation(AnimationUtils.loadAnimation(_context, forward ? R.anim.scan_out : R.anim.scan_out_rev));
	}

	@Override
	protected Dialog onCreateDialog(final int dialogID) {
		if (dialogID == REMOVE_PROFILE_DIALOG) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.NewSettingsProgressDialog);
			View renameProfileDialog = getLayoutInflater()
					.inflate(R.layout.adviser_remove_profile_dialog_layout, null);

			TextView description = (TextView) renameProfileDialog.findViewById(R.id.description);
			description.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_REMOVE_DIALOG_CONFIRM));

			Button yesButton = (Button) renameProfileDialog.findViewById(R.id.yes_button);
			yesButton.setText(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES));
			yesButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					settingsAdviserProvider.requestRemoveProfile(currentProfileID);
				}
			});
			Button noButton = (Button) renameProfileDialog.findViewById(R.id.no_button);
			noButton.setText(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO));
			noButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dismissDialog(dialogID);
				}
			});

			return builder
					.setView(renameProfileDialog)
					.setCancelable(true)
					.create();
		}
		if (dialogID == RENAME_PROFILE_DIALOG) {
			AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.NewSettingsProgressDialog);
			View renameProfileDialog = getLayoutInflater()
					.inflate(R.layout.adviser_rename_profile_dialog_layout, null);

			TextView description = (TextView) renameProfileDialog.findViewById(R.id.description);
			description.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_ADVISER_RENAME_DIALOG_DESC));

			nameEditText = (EditText) renameProfileDialog.findViewById(R.id.name);
            InputFilter[] filterArray = new InputFilter[1];
            filterArray[0] = new InputFilter.LengthFilter(AdviserProvider.PROFILE_NAME_MAX_LENGTH);
            nameEditText.setFilters(filterArray);
			nameEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
				@Override
				public boolean onEditorAction(TextView textView, int actionID, KeyEvent keyEvent) {
					if (actionID == EditorInfo.IME_ACTION_DONE) {
						renameProfile();
						return true;
					}
					return false;
				}
			});
			nameEditText.setOnKeyListener(new View.OnKeyListener() {
				@Override
				public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
					if (Common.okPressed(keyCode)) {
						renameProfile();
						return true;
					}
					return false;
				}
			});

			Button saveButton = (Button) renameProfileDialog.findViewById(R.id.save_button);
			saveButton.setText(language_manager_.GetString(LocaleStrings.IDS_SCAN_SAVE_BUTTON));
			saveButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					renameProfile();
				}
			});
			Button cancelButton = (Button) renameProfileDialog.findViewById(R.id.cancel_button);
			cancelButton.setText(language_manager_.GetString(LocaleStrings.IDS_SCAN_CANCEL_BUTTON));
			cancelButton.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					dismissDialog(dialogID);
				}
			});

			return builder
					.setView(renameProfileDialog)
					.setCancelable(true)
					.create();
		}
		return null;
	}

	@Override
	public void onPrepareDialog(int dialogID, Dialog dialog) {
		if (RENAME_PROFILE_DIALOG == dialogID) {
			nameEditText.setText(currentProfileName);
			nameEditText.requestFocus();
			nameEditText.selectAll();
		}
	}

	private void nextView(String name, String id) {
		currentProfileName = name;
		currentProfileID = id;
		setFlipperDirection(true);
		flipper.showNext();
	}

	private void renameProfile() {
		activateProgressBar(true);
		settingsAdviserProvider.requestRenameProfile(currentProfileID, nameEditText.getText().toString());
	}
}
