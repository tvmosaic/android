package com.dvblogic.tvmosaic;

import TVMosaic.TVMosaic.R;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

public class StatusLine 
{

	public static final int status_icon_none = -1;
	public static final int status_icon_favorites = 0;
	public static final int status_icon_profile = 1;
	public static final int status_icon_sort = 2;
	public static final int status_icon_disk_space = 3;

	View status_line;
	int[] fields = {R.id.field_1, R.id.field_2, R.id.field_3, R.id.field_4};
	int[] images = {R.drawable.tvm_icon_status_favorites, R.drawable.tvm_icon_status_profile, R.drawable.tvm_icon_status_sort, R.drawable.tvm_icon_status_sort};

	public StatusLine(View view) {
		status_line = view;
	}

	public void setField(int index, String header, String value, int icon_id) {
		if (header == null || header.isEmpty() || value == null || value.isEmpty()) {
			status_line.findViewById(fields[index]).setVisibility(View.INVISIBLE);
		} else {
			RelativeLayout v = (RelativeLayout)status_line.findViewById(fields[index]);

			TextView tv = (TextView) v.findViewById(R.id.field_text_value);
			tv.setText(value);

			tv = (TextView) v.findViewById(R.id.field_text_header);
			tv.setText(header);

			ImageView iv = (ImageView) v.findViewById(R.id.field_icon);
            if (icon_id == status_icon_none) {
                iv.setVisibility(View.GONE);
            } else {
                iv.setImageResource(images[icon_id]);
                iv.setVisibility(View.VISIBLE);
            }

			v.setVisibility(View.VISIBLE);
			v.requestLayout();
		}
		status_line.requestLayout();
	}

	public void showLoadIndicator() {
        ProgressBar spinner = (ProgressBar)status_line.findViewById(R.id.load_indicator);
        spinner.setVisibility(View.VISIBLE);
	}

    public void hideLoadIndicator() {
        ProgressBar spinner = (ProgressBar)status_line.findViewById(R.id.load_indicator);
        spinner.setVisibility(View.INVISIBLE);
    }
}
