package com.dvblogic.tvmosaic;

public interface ScrollViewListener
{
	void onScrollChanged(Object scrollView, int x, int y, int oldx, int oldy);
}
