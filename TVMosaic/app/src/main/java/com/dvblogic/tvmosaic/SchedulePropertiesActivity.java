package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.dvblogic.dvblink_common.ChannelIDList;
import com.dvblogic.dvblink_common.ManualSchedule;
import com.dvblogic.dvblink_common.RecordingsList;
import com.dvblogic.dvblink_common.RecordingsRequest;
import com.dvblogic.dvblink_common.Schedule;
import com.dvblogic.dvblink_common.ScheduleRemover;
import com.dvblogic.dvblink_common.ScheduleUpdater;
import com.dvblogic.dvblink_common.SchedulesList;
import com.dvblogic.dvblink_common.SchedulesRequest;
import com.dvblogic.dvblink_common.SendToTarget;
import com.dvblogic.dvblink_common.SendToTargetIDLIst;
import com.dvblogic.dvblink_common.SendToTargetsRequest;
import com.dvblogic.dvblink_common.SendToTargetsRespond;
import com.dvblogic.dvblink_common.Serializer;
import com.dvblogic.dvblink_common.StatusCode;
import com.dvblogic.dvblink_common.Utils;
import com.dvblogic.dvblink_common.XmlCommandResponse;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import TVMosaic.TVMosaic.R;

public class SchedulePropertiesActivity extends BaseActivity implements ActionOptionsPane.ActionOptionsPaneEventListener, ActionPane.ActionPaneEventListener
{
    private static final int state_ready      = 1;
    private static final int state_requesting_schedule_update      = 2;
    private static final int state_requesting_schedule_delete      = 3;
    private static final int state_requesting_schedules       = 4;
    private static final int state_requesting_timers       = 5;
    private static final int state_requesting_targets       = 6;

    private static final String TAG = "SchedulePropetiesActivity";

    boolean schedule_changed_ = false;
    int state_ = state_ready;
    Schedule schedule_ = null;
    String schedule_id_ = null;
    private ActionOptionsPane choice_menu_;
    private SendToTargetsRespond send_to_targets_ = null;
    private MyActionPane _options_menu = null;

    private class MyActionPane extends ActionPane {

        public MyActionPane(BaseActivity context, ActionPaneEventListener listener) {
            super(context, listener);
        }

        protected void fill_standard_pane_items() {

            ActionPaneItem exit_and_save_api = new ActionPaneItem();
            exit_and_save_api.text_ = language_manager_.GetString(LocaleStrings.IDS_SAVE);
            exit_and_save_api.id_ = exit_and_save_pane_action_id_;
            exit_and_save_api.left_icon_ = R.drawable.tvm_save_icon_normal;
            exit_and_save_api.left_icon_selected_ = R.drawable.tvm_save_icon_hover;
            items_.add(exit_and_save_api);

            ActionPaneItem exit_api = new ActionPaneItem();
            exit_api.text_ = language_manager_.GetString(LocaleStrings.IDS_DO_NOT_SAVE);
            exit_api.id_ = exit_pane_action_id_;
            exit_api.left_icon_ = R.drawable.tvm_cancel_icon_normal;
            exit_api.left_icon_selected_ = R.drawable.tvm_cancel_icon_hover;
            items_.add(exit_api);
        }

        protected void add_extra_pane_items() {
        }
    }


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setContentView(R.layout.schedule_properties_activity);

        schedule_changed_ = false;
        //set result to cancelled until there are changes
        setResult(RESULT_CANCELED);

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_PROPERTIES));

        schedule_id_ = this.getIntent().getStringExtra("schedule_id");
        _options_menu = new MyActionPane(this, this);

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                requestSchedules();
            }
        });
    }

    protected void customizeActivity() {
        //enable/disable properties, depending on the schedule type
        if (customizeProperty(R.id.active, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_ACTIVE),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_ACTIVE_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_active);
                    }
                },
                schedule_.isV2Schedule()
        )) {
            setPropertyCurrentValue(R.id.active, getActiveString(this, schedule_.Active));
        }

        if (customizeProperty(R.id.priority, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_PRIORITY),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_PRIORITY_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_priority);
                    }
                },
                schedule_.isV2Schedule()
        )) {
            setPropertyCurrentValue(R.id.priority, getPriorityString(this, schedule_.Priority));
        }

        if (customizeProperty(R.id.only_new_programs, language_manager_.GetString(LocaleStrings.IDS_NEW_ONLY),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_NEW_ONLY_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_new_only);
                    }
                },
                schedule_.isEpg() && schedule_.isRepeat()
        )) {
            setPropertyCurrentValue(R.id.only_new_programs, getNewOnlyString(this, schedule_.ByEpg.NewOnly));
        }

        if (customizeProperty(R.id.start_after, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_STARTS_AFTER),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_STARTS_AFTER_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_start_after);
                    }
                },
                (schedule_.isEpg() || schedule_.isPatern()) && schedule_.isRepeat() && schedule_.isV2Schedule()
        )) {
            setPropertyCurrentValue(R.id.start_after, getStartBeforeAfterString(this, schedule_.getStartAfter()));
        }

        if (customizeProperty(R.id.start_before, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_STARTS_BEFORE),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_STARTS_BEFORE_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_start_before);
                    }
                },
                (schedule_.isEpg() || schedule_.isPatern()) && schedule_.isRepeat() && schedule_.isV2Schedule()
        )) {
            setPropertyCurrentValue(R.id.start_before, getStartBeforeAfterString(this, schedule_.getStartBefore()));
        }

        if (customizeProperty(R.id.keep_no_more, language_manager_.GetString(LocaleStrings.IDS_KEEP_ITEMS),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_KEEP_ITEMS_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_to_keep);
                    }
                },
                schedule_.isRepeat()
        )){
            setPropertyCurrentValue(R.id.keep_no_more, getKeepNoMoreString(this, schedule_.recordingsToKeep()));
        }

        if (customizeProperty(R.id.start_time, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_START_MARGIN),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_START_MARGIN_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_start);
                    }
                },
                true
        )) {
            setPropertyCurrentValue(R.id.start_time, getStartMarginString(this, schedule_.MargineBefore / 60));
        }

        if (customizeProperty(R.id.stop_time, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_END_MARGIN),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_END_MARGIN_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showActionMenu(action_pane_mode_end);
                    }
                },
                true
        )) {
            setPropertyCurrentValue(R.id.stop_time, getEndMarginString(this, schedule_.MargineAfter / 60));
        }

        if (customizeProperty(R.id.daymask, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_WEEKDAYS),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_WEEKDAYS_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDayMaskOptionsMenu();
                    }
                },
                (schedule_.isEpg() || schedule_.isPatern()) && schedule_.isRepeat() && schedule_.isV2Schedule()
        )) {
            setPropertyCurrentValue(R.id.daymask, getWeekdayRepeatString(this, schedule_.getDayMask()));
        }

        if (customizeProperty(R.id.targets, language_manager_.GetString(LocaleStrings.IDS_SENDTO_TARGETS),
                language_manager_.GetString(LocaleStrings.IDS_SENDTO_TARGETS_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showTargetsOptionsMenu();
                    }
                },
                send_to_targets_ != null || (schedule_.Targets != null && schedule_.Targets.size() > 0)
        )) {
            setPropertyCurrentValue(R.id.targets, getSendToTargetsAsString());
        }

        customizeProperty(R.id.delete_schedule, language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_CANCEL),
                language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_CANCEL_DESC),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        AlertDialog.Builder ad = new AlertDialog.Builder(_context)
                                .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                                .setMessage(language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_CANCEL_WARN))
                                .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                                requestTimers();
                                            }
                                        })
                                .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int whichButton) {
                                            }
                                        });
                        ad.show();
                    }
                },
                true
        );
    }

    protected boolean customizeProperty(int layoutID, String title, String desc, View.OnClickListener listener, boolean show) {
        TextView tv;
        View lv = findViewById(R.id.schedule_properties);
        RelativeLayout layout = (RelativeLayout) lv.findViewById(layoutID);
        layout.setVisibility(show ? View.VISIBLE : View.GONE);

        tv = (TextView) layout.findViewById(R.id.title_text);
        tv.setText(title);
        tv = (TextView) layout.findViewById(R.id.desc_text);
        tv.setText(desc);

        layout.setOnClickListener(listener);

        return show;
    }

    protected void setPropertyCurrentValue(int layoutID, String current_value) {
        TextView tv;
        View lv = findViewById(R.id.schedule_properties);
        RelativeLayout layout = (RelativeLayout) lv.findViewById(layoutID);

        tv = (TextView) layout.findViewById(R.id.spinner_label);
        tv.setText(current_value);
    }

    protected String getSendToTargetsAsString() {
        String ret_val = "";
        if (schedule_.Targets != null) {
            for (int i = 0; i < schedule_.Targets.size(); i++) {
                String name = language_manager_.GetString(LocaleStrings.IDS_UNKNOWN_TARGET);
                if (send_to_targets_ != null) {
                    SendToTarget stt = send_to_targets_.getById(schedule_.Targets.get(i));
                    if (stt != null)
                        name = stt.name;
                }
                if (!ret_val.isEmpty())
                    ret_val += ", ";
                ret_val += name;
            }
        }
        return ret_val;
    }

    public static String getWeekdayRepeatString(BaseActivity activity, int day_mask) {
        String ret_val = "";
        if (day_mask == ManualSchedule.DAY_MASK_DAILY || day_mask == Schedule.dayMaskAnyDay) {
            ret_val = activity.language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_WEEKDAYS_ANYDAY);
        } else {
            ret_val = ManualScheduleActivity.buildRepeatDaysString(activity, day_mask);
        }
        return ret_val;
    }

    public static String getPriorityString(BaseActivity activity, int priority) {
        String ret_val = "";

        if (priority == Schedule.schedulePriorityLow)
            ret_val = activity.language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_PRIORITY_LOW);
        else if (priority == Schedule.schedulePriorityNormal)
            ret_val = activity.language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_PRIORITY_NORMAL);
        else if (priority == Schedule.schedulePriorityHigh)
            ret_val = activity.language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_PRIORITY_HIGH);

        return ret_val;
    }

    public static String getActiveString(BaseActivity activity, boolean active) {
        return (active ? activity.language_manager_.GetString(LocaleStrings.IDS_YES) : activity.language_manager_.GetString(LocaleStrings.IDS_NO));
    }

    public static String getNewOnlyString(BaseActivity activity, boolean new_only) {
        return new_only ? activity.language_manager_.GetString(LocaleStrings.IDS_YES) : activity.language_manager_.GetString(LocaleStrings.IDS_NO);
    }

    public static String getStartBeforeAfterString(BaseActivity activity, int sec) {
        String ret_val = "";

        if (sec == Schedule.startMarginAnyTime) {
            ret_val = activity.language_manager_.GetString(LocaleStrings.IDS_ANYTIME);
        } else {
            String str = String.format("%02d:%02d", sec / 3600, (sec % 3600) / 60);
            SimpleDateFormat df = new SimpleDateFormat("HH:mm", Locale.getDefault());
            long ms = 0;
            try {
                Date d = df.parse(str);
                ms = d.getTime();
            } catch (Exception e) {}
            ret_val = Utils.UtcToShortTimeString(activity, ms / 1000);
        }

        return ret_val;
    }

    protected String getRecordTimeString(boolean anytime, long start_time) {
        if (anytime) {
            return language_manager_.GetString(LocaleStrings.IDS_ANYTIME);
        } else {
            return language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_RECORD_TIME_AROUND) + " " + Utils.UtcToShortTimeString(this, start_time);
        }
    }

    public static String getKeepNoMoreString(BaseActivity activity, int num) {
        if (num == 0) {
            return activity.language_manager_.GetString(LocaleStrings.IDS_ALL);
        } else {
            return String.format("%d", num);
        }
    }

    public static  String getStartMarginString(BaseActivity activity, long margin_min) {
        if (margin_min <= 0) {
            return activity.language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_ON_TIME);
        } else {
            return String.format("-%d %s", margin_min, activity.language_manager_.GetString(LocaleStrings.IDS_MINUTE));
        }
    }

    public static String getEndMarginString(BaseActivity activity, long margin_min) {
        if (margin_min <= 0) {
            return activity.language_manager_.GetString(LocaleStrings.IDS_SCHEDULE_ON_TIME);
        } else {
            return String.format("+%d %s", margin_min, activity.language_manager_.GetString(LocaleStrings.IDS_MINUTE));
        }
    }


    @Override
    public void onBackPressed() {
        if (schedule_changed_)
            _options_menu.show();
        else
            finish();
    }

    protected void requestTimers() {
        activateProgressBar(true);
        state_ = state_requesting_timers;

        data_provider_.GetRecordings(new RecordingsRequest());
    }

    private void processTimers() {

        ChannelIDList channels_to_refresh = new ChannelIDList();

        RecordingsList rl = (RecordingsList)response_;
        for (int i=0; i<rl.size(); i++) {
            channels_to_refresh.add(rl.get(i).ChannelID);
        }

        Intent intent = getIntent();
        String channels_xml = Serializer.serialize(channels_to_refresh);
        intent.putExtra("need_channel_update", channels_xml);
        setResult(RESULT_OK, intent);

        requestScheduleCancel();
    }

    protected void requestScheduleCancel() {
        activateProgressBar(true);
        state_ = state_requesting_schedule_delete;

        ScheduleRemover sr = new ScheduleRemover(schedule_id_);
        data_provider_.RemoveSchedule(sr);
    }

    private void processScheduleCancel() {
        state_ = state_ready;
        activateProgressBar(false);

        finish();
    }

    protected void requestTargets() {
        activateProgressBar(true);
        state_ = state_requesting_targets;

        data_provider_.SendToGetTargets(new SendToTargetsRequest());
    }

    protected void processSendToTargets() {
        try {
            if (response_ != null && response_ instanceof XmlCommandResponse) {
                XmlCommandResponse cmd_response = (XmlCommandResponse) response_;
                if (cmd_response.IsSuccess()) {
                    send_to_targets_ = (SendToTargetsRespond) Serializer.deserialize(
                            cmd_response.Params, SendToTargetsRespond.class);
                }
            }
        } catch (Exception e) {
            send_to_targets_ = null;
        }

        if (send_to_targets_ != null && send_to_targets_.size() == 0)
            send_to_targets_ = null;

        activateProgressBar(false);
        state_ = state_ready;

        if (schedule_ != null)
            customizeActivity();
    }

    protected void requestSchedules() {
        activateProgressBar(true);
        state_ = state_requesting_schedules;

        SchedulesRequest sr = new SchedulesRequest();
        data_provider_.GetSchedules(sr);
    }

    private void processSchedules() {
        SchedulesList sl = (SchedulesList)response_;

        for (Schedule sch : sl.list()) {
            if (sch.ScheduleID.equals(schedule_id_)) {
                schedule_ = sch;
                break;
            }
        }

        state_ = state_ready;
        activateProgressBar(false);

        //request sendto targets
        requestTargets();

        if (schedule_ == null)
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_SCHEDULES_NO_SCHEDULES_FOUND), Toast.LENGTH_SHORT).show();
    }

    private void processScheduleUpdate() {
        state_ = state_ready;
        activateProgressBar(false);

        setResult(RESULT_OK);

        finish();
    }

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;

        if (state_ == state_requesting_schedule_update) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processScheduleUpdate();
                }
            });
        } else
        if (state_ == state_requesting_schedule_delete) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processScheduleCancel();
                }
            });
        } else
        if (state_ == state_requesting_timers) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processTimers();
                }
            });
        } else
        if (state_ == state_requesting_schedules) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processSchedules();
                }
            });
        }
        if (state_ == state_requesting_targets) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processSendToTargets();
                }
            });
        }
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        http_error_ = error;
        runOnUiThread(new Runnable() {
            public void run() {
                ProcessDataProviderError();
            }
        });
    }

    protected void ProcessDataProviderError() {
        try {
            if (state_ == state_requesting_targets) {
                //it's ok. apparently sendto is not supported on this platform
                send_to_targets_ = null;
                if (schedule_ != null)
                    customizeActivity();
            } else {
                String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
                Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
            }

            state_ = state_ready;
            activateProgressBar(false);
        } catch (Exception e) {
        }
    }

    static final int action_pane_mode_none = 0;
    static final int action_pane_mode_new_only = 1;
//    static final int action_pane_mode_record_time = 2;
    static final int action_pane_mode_to_keep = 3;
    static final int action_pane_mode_start = 4;
    static final int action_pane_mode_end = 5;
    static final int action_pane_mode_targets = 6;
    static final int action_pane_mode_active = 7;
    static final int action_pane_mode_priority = 8;
    static final int action_pane_mode_start_before = 9;
    static final int action_pane_mode_start_after = 10;
    static final int action_pane_mode_daymask = 11;

    protected int action_pane_mode_ = action_pane_mode_none;

    boolean action_pane_values_active[] = {true, false};

    int action_pane_values_priority[] = {Schedule.schedulePriorityLow, Schedule.schedulePriorityNormal, Schedule.schedulePriorityHigh};

    boolean action_pane_values_new_only[] = {true, false};

    boolean action_pane_values_after_before_time[] = {true, false};

    int action_pane_values_to_keep[] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};

    int action_pane_values_start[] = {0, 1, 2, 3, 4, 5, 10, 15};

    int action_pane_values_end[] = {0, 1, 2, 3, 4, 5, 10, 15, 30};

    protected void showActionMenu(int mode) {
        ActionOptionsPaneItems options = new ActionOptionsPaneItems(
                ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON
        );

        action_pane_mode_ = mode;
        int selected_item = 0;
        ArrayList<String> values = new ArrayList<String>();
        switch (mode) {
            case action_pane_mode_active:
                values.add(getActiveString(this, action_pane_values_active[0]));
                values.add(getActiveString(this, action_pane_values_active[1]));
                selected_item = schedule_.Active == action_pane_values_active[0] ? 0 : 1;
                break;
            case action_pane_mode_priority:
                for (int i=0; i<action_pane_values_priority.length; i++) {
                    values.add(getPriorityString(this, action_pane_values_priority[i]));
                    if (action_pane_values_priority[i] == schedule_.Priority) {
                        selected_item = i;
                    }
                }
                break;
            case action_pane_mode_new_only:
                values.add(getNewOnlyString(this, action_pane_values_new_only[0]));
                values.add(getNewOnlyString(this, action_pane_values_new_only[1]));
                selected_item = schedule_.ByEpg.NewOnly == action_pane_values_new_only[0] ? 0 : 1;
                break;
            case action_pane_mode_start_after:
                values.add(language_manager_.GetString(LocaleStrings.IDS_ANYTIME));
                values.add(language_manager_.GetString(LocaleStrings.IDS_SELECTED_TIME));
                selected_item = (schedule_.getStartAfter() == Schedule.startMarginAnyTime) == action_pane_values_after_before_time[0] ? 0 : 1;
                break;
            case action_pane_mode_start_before:
                values.add(language_manager_.GetString(LocaleStrings.IDS_ANYTIME));
                values.add(language_manager_.GetString(LocaleStrings.IDS_SELECTED_TIME));
                selected_item = (schedule_.getStartBefore() == Schedule.startMarginAnyTime) == action_pane_values_after_before_time[0] ? 0 : 1;
                break;
            case action_pane_mode_to_keep:
                for (int i=0; i<action_pane_values_to_keep.length; i++) {
                    values.add(getKeepNoMoreString(this, action_pane_values_to_keep[i]));
                    if (action_pane_values_to_keep[i] == schedule_.recordingsToKeep()) {
                        selected_item = i;
                    }
                }
                break;
            case action_pane_mode_start:
                for (int i=0; i<action_pane_values_start.length; i++) {
                    values.add(getStartMarginString(this, action_pane_values_start[i]));
                    if (action_pane_values_start[i] == schedule_.MargineBefore / 60) {
                        selected_item = i;
                    }
                }
                break;
            case action_pane_mode_end:
                for (int i=0; i<action_pane_values_end.length; i++) {
                    values.add(getEndMarginString(this, action_pane_values_end[i]));
                    if (action_pane_values_end[i] == schedule_.MargineAfter / 60) {
                        selected_item = i;
                    }
                }
                break;
            default:
                break;
        }

        for (int i=0; i<values.size(); i++) {
            String id = String.format("%d", i);
            options.add_item(values.get(i), id);
            if (selected_item == i) {
                options.set_selected_item_id(id);
            }
        }

        choice_menu_ = new ActionOptionsPane(this, this, options, 0, null);
        choice_menu_.show();
    }

    protected void showDayMaskOptionsMenu() {
        ActionOptionsPaneItems options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX, true);

        for (int i = 0; i < ManualScheduleActivity.repeat_masks.length; i++) {
            String id = String.valueOf(i);
            options.add_item(language_manager_.GetString(ManualScheduleActivity.repeat_mask_names[i]), id);
            if ((ManualScheduleActivity.repeat_masks[i] & schedule_.getDayMask()) != 0) {
                //all items are unchecked by default
                options.toggle_selection(id);
            }
        }

        action_pane_mode_ = action_pane_mode_daymask;
        choice_menu_ = new ActionOptionsPane(this, this, options, 0, null);
        choice_menu_.show();
    }

    protected void showTargetsOptionsMenu() {
        ActionOptionsPaneItems options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX, true);

        if (send_to_targets_ != null) {
            for (int i=0; i<send_to_targets_.size(); i++) {
                options.add_item(send_to_targets_.get(i).name, send_to_targets_.get(i).id);
            }
        }

        if (schedule_.Targets != null) {
            for (int i = 0; i < schedule_.Targets.size(); i++) {
                String id = schedule_.Targets.get(i);

                boolean bexists = false;
                if (send_to_targets_ != null) {
                    SendToTarget stt = send_to_targets_.getById(id);
                    bexists = stt != null;
                }

                if (!bexists) {
                    //this is non-existant target. Add it as is
                    options.add_item(language_manager_.GetString(LocaleStrings.IDS_UNKNOWN_TARGET), id);
                }

                options.toggle_selection(id);
            }
        }

        action_pane_mode_ = action_pane_mode_targets;
        choice_menu_ = new ActionOptionsPane(this, this, options, 0, null);
        choice_menu_.show();
    }

    protected void showTimePickerDialog() {
        int t = action_pane_mode_ == action_pane_mode_start_before ? schedule_.getStartBefore() : schedule_.getStartAfter();
        int hour = t / 3600;
        int minute = ( t % 3600) / 60;
        boolean b24 = DateFormat.is24HourFormat(_context);

        if (Utils.usesRemoteControl(_context)) {
            CustomTimePickerDialog start_time_picker = new CustomTimePickerDialog(_context, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                    ApplyAfterBeforeTimeSelection(selectedHour, selectedMinute);
                }
            }, hour, minute, DateFormat.is24HourFormat(_context));
            start_time_picker.setTitle(language_manager_.GetString(LocaleStrings.IDS_SELECT_TIME));
            start_time_picker.show();
        } else {
            final TimePicker timePicker = new TimePicker(_context);
            timePicker.setIs24HourView(b24);
            timePicker.setCurrentHour(hour);
            timePicker.setCurrentMinute(minute);

            final AlertDialog timePickerDialog = new AlertDialog.Builder(_context)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            ApplyAfterBeforeTimeSelection(timePicker.getCurrentHour(), timePicker.getCurrentMinute());
                        }
                    })
                    .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .setView(timePicker).show();
        }
    }

    protected void ApplyAfterBeforeTimeSelection(int hour, int minute) {
        int t = hour * 3600 + minute * 60;
        if (action_pane_mode_ == action_pane_mode_start_before) {
            schedule_.setStartBefore(t);
            setPropertyCurrentValue(R.id.start_before, getStartBeforeAfterString(this, schedule_.getStartBefore()));
        } else {
            schedule_.setStartAfter(t);
            setPropertyCurrentValue(R.id.start_after, getStartBeforeAfterString(this, schedule_.getStartAfter()));
        }
    }

    public void onActionPaneOption(int option_id, String selected_option) {

        //if we get here then schedule will be changed
        schedule_changed_ = true;

        if (action_pane_mode_ == action_pane_mode_targets) {
            ArrayList<String> selected_items = choice_menu_.get_multiselected_items_id();
            if (schedule_.Targets != null)
                schedule_.Targets.clear();
            else
                schedule_.Targets = new SendToTargetIDLIst();

            for (int i = 0; i < selected_items.size(); i++) {
                schedule_.Targets.add(selected_items.get(i));
            }
            setPropertyCurrentValue(R.id.targets, getSendToTargetsAsString());
            //if there are no targets and "ghost" targets - hide the menu
            boolean show = send_to_targets_ != null || (schedule_.Targets != null && schedule_.Targets.size() > 0);
            View lv = findViewById(R.id.schedule_properties);
            RelativeLayout layout = (RelativeLayout) lv.findViewById(R.id.targets);
            layout.setVisibility(show ? View.VISIBLE : View.GONE);
            if (!show)
                choice_menu_.hide();
        } else
        if (action_pane_mode_ == action_pane_mode_daymask) {
            ArrayList<String> selected_items = choice_menu_.get_multiselected_items_id();
            int daymask = Schedule.dayMaskAnyDay;
            for (int i=0; i<selected_items.size(); i++) {
                int idx = Integer.valueOf(selected_items.get(i));
                daymask |= ManualScheduleActivity.repeat_masks[idx];
            }
            schedule_.setDayMask(daymask);
            setPropertyCurrentValue(R.id.daymask, getWeekdayRepeatString(this, schedule_.getDayMask()));
        } else {
            choice_menu_.hide();

            int value_idx = Integer.parseInt(selected_option);
            switch (action_pane_mode_) {
                case action_pane_mode_active:
                    schedule_.Active = action_pane_values_active[value_idx];
                    setPropertyCurrentValue(R.id.active, getActiveString(this, schedule_.Active));
                    break;
                case action_pane_mode_priority:
                    schedule_.Priority = action_pane_values_priority[value_idx];
                    setPropertyCurrentValue(R.id.priority, getPriorityString(this, schedule_.Priority));
                    break;
                case action_pane_mode_new_only:
                    schedule_.ByEpg.NewOnly = action_pane_values_new_only[value_idx];
                    setPropertyCurrentValue(R.id.only_new_programs, getNewOnlyString(this, schedule_.ByEpg.NewOnly));
                    break;
                case action_pane_mode_start_before:
                case action_pane_mode_start_after:
                    if (action_pane_values_after_before_time[value_idx]) {
                        //any time
                        if (action_pane_mode_ == action_pane_mode_start_after) {
                            schedule_.setStartAfter(Schedule.startMarginAnyTime);
                            setPropertyCurrentValue(R.id.start_after, getStartBeforeAfterString(this, schedule_.getStartAfter()));
                        } else {
                            schedule_.setStartBefore(Schedule.startMarginAnyTime);
                            setPropertyCurrentValue(R.id.start_before, getStartBeforeAfterString(this, schedule_.getStartBefore()));
                        }
                    } else {
                        //select time
                        showTimePickerDialog();
                        return;
                    }
                    break;
                case action_pane_mode_to_keep:
                    schedule_.setRecordingsToKeep(action_pane_values_to_keep[value_idx]);
                    setPropertyCurrentValue(R.id.keep_no_more, getKeepNoMoreString(this, schedule_.recordingsToKeep()));
                    break;
                case action_pane_mode_start:
                    schedule_.MargineBefore = action_pane_values_start[value_idx] * 60;
                    setPropertyCurrentValue(R.id.start_time, getStartMarginString(this, schedule_.MargineBefore / 60));
                    break;
                case action_pane_mode_end:
                    schedule_.MargineAfter = action_pane_values_end[value_idx] * 60;
                    setPropertyCurrentValue(R.id.stop_time, getStartMarginString(this, schedule_.MargineAfter / 60));
                    break;
                default:
                    break;
            }
        }
    }

    public void onActionPaneAction(int action_id) {

        _options_menu.hide();

        if (action_id == ActionPane.exit_pane_action_id_) {
            finish();
        } else
        if (action_id == ActionPane.exit_and_save_pane_action_id_) {
            ApplyChanges();
        }
    }

    protected void ApplyChanges() {
        ScheduleUpdater su = new ScheduleUpdater();
        su.ScheduleID = schedule_.ScheduleID;
        su.Targets = new SendToTargetIDLIst();
        if (schedule_.Targets != null) {
            for (int i = 0; i < schedule_.Targets.size(); i++)
                su.Targets.add(schedule_.Targets.get(i));
        }

        su.Active = schedule_.Active;
        su.Priority = schedule_.Priority == null ? Schedule.schedulePriorityNormal : schedule_.Priority;

        su.NewOnly = (schedule_.isEpg() && schedule_.isRepeat()) ? schedule_.ByEpg.NewOnly : false;
        su.DayMask = ((schedule_.isEpg() || schedule_.isPatern()) && schedule_.isRepeat()) ? schedule_.getDayMask() : Schedule.dayMaskAnyDay;
        su.RecordingsToKeep = schedule_.isRepeat() ? schedule_.recordingsToKeep() : 0;
        su.MargineAfter = schedule_.MargineAfter;
        su.MargineBefore = schedule_.MargineBefore;

        su.StartAfter = schedule_.getStartAfter();
        su.StartBefore = schedule_.getStartBefore();
        //for compatibility
        su.RecordSeriesAnytime = schedule_.getStartAfter() == Schedule.startMarginAnyTime && schedule_.getStartBefore() == Schedule.startMarginAnyTime;

        activateProgressBar(true);
        state_ = state_requesting_schedule_update;
        data_provider_.UpdateSchedule(su);
    }

}
