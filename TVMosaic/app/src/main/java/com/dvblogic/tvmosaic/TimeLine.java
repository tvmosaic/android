package com.dvblogic.tvmosaic;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.view.View;

import com.dvblogic.dvblink_common.Utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import TVMosaic.TVMosaic.R;

public class TimeLine extends View {

    long timelime_step_seconds = 1800; // 30'

    Paint paint_ = new Paint();
    Rect bounds_rect_ = new Rect();
    Rect bar_rect_ = new Rect();
    long start_time_sec_ = -1;
    long end_time_sec_ = -1;
    int text_color_;
    float text_size_;

    public TimeLine(Context context) {
        super(context);
    }

    public TimeLine(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void clear_timeline() {
        start_time_sec_ = -1;
        end_time_sec_ = -1;
        invalidate();
    }

    public void create_time_line(long start_time_sec, long end_time_sec) {
        start_time_sec_ = start_time_sec;
        end_time_sec_ = end_time_sec;
        int width = TimeLineBuilder.GridTimeToPxWidth(getContext(), start_time_sec_, end_time_sec_);
        this.setMinimumWidth(width);
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        text_color_ = ContextCompat.getColor(getContext(), R.color.timeline_text);
        text_size_ = getContext().getResources().getDimensionPixelSize(R.dimen.timeline_item_text_size);

        setBackgroundColor(Color.TRANSPARENT);

        paint_.setAntiAlias(true);
        paint_.setTypeface(CustomFont.getInstance(getContext()).getTypeFace_Bold());
        paint_.setStyle(Paint.Style.FILL);
        paint_.setColor(text_color_);
        paint_.setTextSize(text_size_);

        bar_rect_.set(0, 0, getWidth(), getHeight());

        long step = timelime_step_seconds;
        long cur_start = start_time_sec_;
        long next_start;

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(cur_start * 1000);
        long minutes = calendar.get(Calendar.MINUTE);
        if (minutes < 30) {
            calendar.set(Calendar.MINUTE, 30);
            next_start = calendar.getTimeInMillis() / 1000;
        } else {
            calendar.set(Calendar.HOUR_OF_DAY, calendar.get(Calendar.HOUR_OF_DAY) + 1);
            calendar.set(Calendar.MINUTE, 0);
            next_start = calendar.getTimeInMillis() / 1000;
        }

        draw_time(canvas, cur_start);

        cur_start = next_start;
        while (cur_start < end_time_sec_) {
            draw_time(canvas, cur_start);
            cur_start += step;
        }
    }

    protected void draw_time(Canvas canvas, long tick_time_sec) {

        if (tick_time_sec <= 0)
            return;

        String tick_time_str = null;
        Date dt = new Date(Utils.UtcToLocal(tick_time_sec) * 1000);

        Calendar calendar = Calendar.getInstance(Locale.getDefault());
        calendar.setTime(dt);

        long minutes = calendar.get(Calendar.MINUTE);
        if (minutes == 0 || minutes == 30) {
            tick_time_str = Utils.UtcToShortTimeString(this.getContext(), tick_time_sec);
        }

        if (tick_time_str != null) {
            float text_spacing = paint_.getTextSize();
            int tick_offs = TimeLineBuilder.GridTimeToPxWidth(getContext(), start_time_sec_, tick_time_sec);

            paint_.getTextBounds(tick_time_str, 0, tick_time_str.length(), bounds_rect_);

            bounds_rect_.set(tick_offs - bounds_rect_.width()/2,
                    (int)(bar_rect_.top + (bar_rect_.height() - text_spacing) / 2),
                    tick_offs + bounds_rect_.width()/2,
                    (int)(bar_rect_.bottom - (bar_rect_.height() - text_spacing) / 2));

            //draw only fully visible items
            if (bounds_rect_.left >= bar_rect_.left && bounds_rect_.right <= bar_rect_.right)
                canvas.drawText(tick_time_str, bounds_rect_.left, bounds_rect_.bottom, paint_);
        }
    }

}
