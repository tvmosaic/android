package com.dvblogic.tvmosaic;

import android.util.Log;
import android.widget.Toast;
import com.dvblogic.dvblink_common.*;

import java.util.ArrayList;

class EPGSettingsProvider implements IDataProvider {

	private static final String TAG = "EPGSetProvider";

	private static int STATE_NOT_READY = -1;
	private static int STATE_READY = 1000;
	private static int STATE_GET_CHANNELS = 1001;
	private static int STATE_GET_EPG_SOURCES = 1002;
	private static int STATE_GET_EPG_CHANNELS = 1003;
	private static int STATE_GET_EPG_CHANNEL_CONFIG = 1004;
	private static int STATE_SET_EPG_CHANNEL_CONFIG = 1005;
	private static int STATE_MATCH_EPG_CHANNELS = 1006;

	private int state = STATE_NOT_READY;

	private BaseActivity activity;
	private Object serverResponse;
	private ScanChannelsDataProvider serverDataProvider_;
	private StatusCode httpError = StatusCode.STATUS_OK;

	private String epgSourceID = "";

	public EPGSettingsProvider(BaseActivity activity, ServerSettings serverSettings, String language) {
		this.activity = activity;
		try {
			serverDataProvider_ = new ScanChannelsDataProvider(serverSettings, this);
			serverDataProvider_.setLanguage(language);
			state = STATE_READY;
		} catch (Exception e) {
			serverDataProvider_ = null;
			Log.e(TAG, "Init", e);
		}
	}

	public void ErrorProcessing(String command, StatusCode error) {
		httpError = error;
		this.epgSourceID = "";
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				state = STATE_READY;
				activity.activateProgressBar(false);
				if (activity.language_manager_ != null) {
					String msg = String.format("%s", activity.language_manager_.ErrorToString(httpError));
					Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
				}
			}
		});
	}

	public void DataProcessing(String command, Object result_data) {
		serverResponse = result_data;
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (state == STATE_GET_EPG_SOURCES) {
					processEPGSources();
				} else if (state == STATE_GET_CHANNELS) {
					processChannels();
				} else if (state == STATE_GET_EPG_CHANNELS) {
					processEPGChannels();
				} else if (state == STATE_GET_EPG_CHANNEL_CONFIG) {
					processGetEPGChannelConfig();
				} else if (state == STATE_SET_EPG_CHANNEL_CONFIG) {
					processSetEPGChannelConfig();
				} else if (state == STATE_MATCH_EPG_CHANNELS) {
					processMatchEPGChannels();
				} else {
					throw new RuntimeException("unknown state");
				}
				serverResponse = null;
			}
		});
	}

	protected void requestChannels() {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		activity.activateProgressBar(true);
		state = STATE_GET_CHANNELS;
		serverDataProvider_.GetChannels(new ChannelsRequest());
	}

	private void processChannels() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			onChannels(
					serverResponse instanceof ChannelsList ?
							(ChannelsList) serverResponse : null
			);
		} catch (Exception exception) {
			Log.e(TAG, "processChannels", exception);
		}
	}

	protected void requestEPGSources() {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		state = STATE_GET_EPG_SOURCES;
		activity.activateProgressBar(true);
		serverDataProvider_.GetEPGSources();
	}

	private void processEPGSources() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if (serverResponse != null && serverResponse instanceof EPGSourceList) {
				onEPGSources((EPGSourceList) serverResponse);
			}
		} catch (Exception exception) {
			Log.e(TAG, "processEPGSources", exception);
		}
	}

	protected void requestEPGChannels(String epgSourceID) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}

		this.epgSourceID = epgSourceID;
		state = STATE_GET_EPG_CHANNELS;
		activity.activateProgressBar(true);
		GetEPGChannelsRequest request = new GetEPGChannelsRequest(epgSourceID);
		serverDataProvider_.GetEPGChannels(request);
	}

	private void processEPGChannels() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			onEPGChannels(this.epgSourceID,
					serverResponse instanceof EPGChannelList ?
							(EPGChannelList) serverResponse : null
			);
			this.epgSourceID = "";
		} catch (Exception exception) {
			Log.e(TAG, "processEPGChannels", exception);
		}
	}

	protected void requestEPGChannelConfig(ArrayList<String> channelsID) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		state = STATE_GET_EPG_CHANNEL_CONFIG;
		activity.activateProgressBar(true);
		GetEPGChannelConfigRequest getEPGChannelConfigRequest = new GetEPGChannelConfigRequest();
		ArrayList<EPGChannel> epgChannels = getEPGChannelConfigRequest.list();
		for (String channelID: channelsID) {
			EPGChannel epgChannel = new EPGChannel(channelID, null);
			epgChannels.add(epgChannel);
		}
		serverDataProvider_.GetEPGChannelConfig(getEPGChannelConfigRequest);
	}

	private void processGetEPGChannelConfig() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			onGetEPGChannelConfig(
					serverResponse instanceof EPGChannelConfigList ?
							(EPGChannelConfigList) serverResponse : null
			);
		} catch (Exception exception) {
			Log.e(TAG, "processGetEPGChannelConfig", exception);
		}
	}

	protected void requestSaveEPGChannelConfig(ArrayList<EPGChannelConfig> channels) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		state = STATE_SET_EPG_CHANNEL_CONFIG;
		activity.activateProgressBar(true);
		EPGChannelConfigList epgChannelConfigList = new EPGChannelConfigList();
		ArrayList<EPGChannelConfig> epgChannels = epgChannelConfigList.list();
		for (EPGChannelConfig channel: channels) {
			epgChannels.add(channel);
		}
		serverDataProvider_.SetEPGChannelConfig(epgChannelConfigList);
	}

	private void processSetEPGChannelConfig() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if (serverResponse != null) {
				onSetEPGChannelConfig();
			}
		} catch (Exception exception) {
			Log.e(TAG, "processSetEPGChannelConfig", exception);
		}
	}

	protected void requestMatchEPGChannels(String epgSourceId, ArrayList<String> channelsId) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		state = STATE_MATCH_EPG_CHANNELS;
		activity.activateProgressBar(true);
		EPGMatchChannelsRequest request = new EPGMatchChannelsRequest();
		ArrayList<EPGMatchChannelRequest> epgMatchChannel = request.channels.list();
		for (String channelId: channelsId) {
			epgMatchChannel.add(new EPGMatchChannelRequest(channelId));
		}
		request.epgSourceId = epgSourceId;
		serverDataProvider_.MatchEPGChannels(request);
	}

	private void processMatchEPGChannels() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if (serverResponse != null && serverResponse instanceof EPGMatchResults) {
				onMatchEPGChannels((EPGMatchResults) serverResponse);
			}
		} catch (Exception exception) {
			Log.e(TAG, "processMatchEPGChannels", exception);
		}
	}

	protected void onChannels(ChannelsList channels) {}
	protected void onEPGSources(EPGSourceList epgSources) {}
	protected void onEPGChannels(String epgSource, EPGChannelList epgChannels) {}
	protected void onGetEPGChannelConfig(EPGChannelConfigList epgChannelConfigList) {}
	protected void onSetEPGChannelConfig() {}
	protected void onMatchEPGChannels(EPGMatchResults epgMatchResults) {}
}