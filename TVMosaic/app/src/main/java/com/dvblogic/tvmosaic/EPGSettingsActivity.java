package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import TVMosaic.TVMosaic.R;
import android.util.Log;
import android.view.*;
import android.view.animation.AnimationUtils;
import android.widget.*;
import com.dvblogic.dvblink_common.*;

import java.util.*;

public class EPGSettingsActivity extends BaseActivity implements ActionPane.ActionPaneEventListener {

    final static int epg_source_settings_activity = 100;
    final static int epg_mapping_activity = 101;
    final static int epg_source_selection_activity = 102;

    private static final String source_selector_container_id = "6bc8272f-eb3a-4ff0-8f68-81d8f0aa7d6e";
    private static final String selected_epg_source_key = "selected_epg_source_key";

    private class MyActionPane extends ActionPane {

        public String epg_source_id_;

        public MyActionPane(BaseActivity context, ActionPaneEventListener listener) {
            super(context, listener);
        }

        protected void fill_standard_pane_items() {

        }

        protected void add_extra_pane_items() {

            ActionPaneItem settings_api = new ActionPaneItem();
            settings_api.text_ = language_manager_.GetString(LocaleStrings.IDS_EPG_SOURCE_SETTINGS);
            settings_api.id_ = epg_sources_settings_pane_action_id_;
            settings_api.left_icon_ = R.drawable.tvm_settings_icon_normal;
            settings_api.left_icon_selected_ = R.drawable.tvm_settings_icon_hover;
            items_.add(0, settings_api);

            ActionPaneItem rescan_api = new ActionPaneItem();
            rescan_api.text_ = language_manager_.GetString(LocaleStrings.IDS_EPG_SOURCE_REFRESH);
            rescan_api.id_ = epg_sources_refresh_pane_action_id_;
            rescan_api.left_icon_ = R.drawable.tvm_refresh_icon_normal;
            rescan_api.left_icon_selected_ = R.drawable.tvm_refresh_icon_hover;
            items_.add(0, rescan_api);

        }
    }

    private class EpgSourceClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {
            String tag = (String)view.getTag();
            int idx = Integer.parseInt(tag);
            epg_sources_pane_.epg_source_id_ = epg_sources_.get(idx).id;
            epg_sources_pane_.setItemVisibility(ActionPane.epg_sources_settings_pane_action_id_, epg_sources_.get(idx).has_settings);
            epg_sources_pane_.show();
        }
    }

    EpgSourceClickListener epgSourceClickListener = new EpgSourceClickListener();

    private enum ESActivityState {
        STATE_NONE,
        STATE_READY,
        STATE_REQUESTING_EPG_SOURCES,
        STATE_REQUESTING_EPG_SOURCE_SETTINGS,
        STATE_SETTINGS_EPG_SOURCE_SETTINGS
    }

    private LinearLayout epgSourcesList;
    private ESActivityState state_ = ESActivityState.STATE_NONE;
    private EPGSourceList epg_sources_ = null;
    private MyActionPane epg_sources_pane_;
    private String current_epg_source_id_;

    public EPGSettingsActivity() {
        show_exit_dialog = false;
    }


    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        setContentView(R.layout.epg_settings_activity);

        TextView tv = (TextView)findViewById(R.id.head_bar);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_PAGE_TITLE));

        RelativeLayout rl = (RelativeLayout)findViewById(R.id.epg_sources_category);
        tv = (TextView)rl.findViewById(R.id.category_text);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SOURCES));
        tv = (TextView)rl.findViewById(R.id.category_desc);
        tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SOURCES_DESC));

        View activity = findViewById(R.id.epg_settings_activity);
        epgSourcesList = (LinearLayout) activity.findViewById(R.id.epg_sources_list);

        epg_sources_pane_ = new MyActionPane(this, this);

        state_ = ESActivityState.STATE_READY;

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                requestEpgSources();
            }
        });
    }


    private void requestEpgSources() {
        if (data_provider_ != null && state_ == ESActivityState.STATE_READY)  {
            activateProgressBar(true);
            state_ = ESActivityState.STATE_REQUESTING_EPG_SOURCES;
            data_provider_.GetEPGSources();
        }
    }

    private void processEpgSources() {
        try {
            if (response_ != null && response_ instanceof EPGSourceList) {
                epg_sources_ = (EPGSourceList) response_;

                for (int i=0; i<epg_sources_.size(); i++) {
                    EPGSource es = epg_sources_.get(i);

                    View view = getLayoutInflater().inflate(R.layout.settings_spinner_layout, epgSourcesList, false);
                    //title
                    TextView label = (TextView) view.findViewById(R.id.title_text);
                    label.setText(es.name);

                    view.setOnClickListener(epgSourceClickListener);
                    view.setTag(String.format("%d", i));
                    epgSourcesList.addView(view);
                }

                RelativeLayout category = (RelativeLayout) findViewById(R.id.epg_mapping);
                if (getNonDefaultEpgSrcNum() > 0) {
                    category.setVisibility(View.VISIBLE);

                    TextView tv;
                    RelativeLayout layout;

                    layout = (RelativeLayout)findViewById(R.id.epg_mapping_category);
                    tv = (TextView)layout.findViewById(R.id.category_text);
                    tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MAPPING_CATEGORY));
                    tv = (TextView)layout.findViewById(R.id.category_desc);
                    tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MAPPING_CATEGORY_DESC));

                    layout = (RelativeLayout) category.findViewById(R.id.epg_mapping_link);
                    tv = (TextView) layout.findViewById(R.id.title_text);
                    tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MAPPING));
                    tv = (TextView) layout.findViewById(R.id.desc_text);
                    tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_MAPPING_DESC));
                    layout.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (getNonDefaultEpgSrcNum() == 1) {
                                for (int i=0; i<epg_sources_.size(); i++) {
                                    if (!epg_sources_.get(i).is_default) {
                                        startEPGMappingActivity(i);
                                        break;
                                    }
                                }
                            } else {
                                //launch source selector
                                ParamContainer epg_source_container = new ParamContainer();
                                epg_source_container.id = source_selector_container_id;
                                epg_source_container.name = language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SOURCE_SELECTION);
                                epg_source_container.desc = "";
                                SelectableParam sp = new SelectableParam();
                                sp.key = selected_epg_source_key;
                                sp.name = language_manager_.GetString(LocaleStrings.IDS_SETTINGS_EPG_SOURCE);
                                for (int i=0; i<epg_sources_.size(); i++)
                                    if (!epg_sources_.get(i).is_default)
                                        sp.addParamEntry(String.format("%d", i), epg_sources_.get(i).name);
                                epg_source_container.selectable_params.add(sp);

                                Intent intent = new Intent(_context, ParamContainerActivity.class);
                                String param = Serializer.serialize(epg_source_container);
                                intent.putExtra("container", param);
                                startActivityForResult(intent, epg_source_selection_activity);
                            }
                        }
                    });
                } else {
                    category.setVisibility(View.GONE);
                }

                //set focus on the first item in epg sources list
                epgSourcesList.requestFocus();
                if (epgSourcesList.getChildCount() > 0)
                    epgSourcesList.getChildAt(0).requestFocus();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        state_ = ESActivityState.STATE_READY;
        dismissProgressBar();
    }

    private int getNonDefaultEpgSrcNum() {
        int count = 0;

        for (int i=0; i<epg_sources_.size(); i++)
            if (!epg_sources_.get(i).is_default)
                count += 1;

        return count;
    }

    private void startEPGMappingActivity(int idx) {
        Intent intent = new Intent(getBaseContext(), EPGMappingActivity.class);
        String param = Serializer.serialize(epg_sources_.get(idx));
        intent.putExtra("epg_source", param);
        startActivityForResult(intent, epg_mapping_activity);
    }

    private void RequestEpgSourceSettings(String epg_source_id) {
        if (data_provider_ != null && state_ == ESActivityState.STATE_READY)  {
            activateProgressBar(true);
            state_ = ESActivityState.STATE_REQUESTING_EPG_SOURCE_SETTINGS;
            data_provider_.GetEPGSourceSettings(epg_source_id);
        }
    }

    private void processEpgSourceSettings() {
        try {
            if (response_ != null && response_ instanceof EPGSourceSettings) {
                EPGSourceSettings epg_source_settings = (EPGSourceSettings) response_;
                //start settings editor activity
                Intent intent = new Intent(this, ParamContainerActivity.class);
                String param = Serializer.serialize(epg_source_settings.settings);
                intent.putExtra("container", param);
                startActivityForResult(intent, epg_source_settings_activity);

            }
        } catch (Exception e) {
        }
        state_ = ESActivityState.STATE_READY;
        dismissProgressBar();
    }

    private void RequestSetEpgSourceSettings(String epg_source_id, ConciseParamMap parameters) {
        if (data_provider_ != null && state_ == ESActivityState.STATE_READY)  {
            activateProgressBar(true);
            state_ = ESActivityState.STATE_SETTINGS_EPG_SOURCE_SETTINGS;
            data_provider_.SetEPGSourceSettings(epg_source_id, parameters);
        }
    }

    private void processSetEpgSourceSettings() {
        state_ = ESActivityState.STATE_READY;
        dismissProgressBar();
    }

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state_ == ESActivityState.STATE_REQUESTING_EPG_SOURCES) {
                    processEpgSources();
                } else
                if (state_ == ESActivityState.STATE_REQUESTING_EPG_SOURCE_SETTINGS) {
                    processEpgSourceSettings();
                } else
                if (state_ == ESActivityState.STATE_SETTINGS_EPG_SOURCE_SETTINGS) {
                    processSetEpgSourceSettings();
                }
            }
        });
    }

    public void ErrorProcessing(String command, StatusCode error) {
        http_error_ = error;
        this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                ProcessDataProviderError();
            }
        });
    }

    public void ProcessDataProviderError() {
        state_ = ESActivityState.STATE_READY;
        activateProgressBar(false);
        if (language_manager_ != null) {
            String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
        }
    }

    public void onActionPaneAction(int action_id) {
        epg_sources_pane_.hide();

        if (action_id == ActionPane.epg_sources_settings_pane_action_id_) {
            current_epg_source_id_ = epg_sources_pane_.epg_source_id_;
            RequestEpgSourceSettings(current_epg_source_id_);
        } else
        if (action_id == ActionPane.epg_sources_refresh_pane_action_id_) {

                class EpgSourceUpdater implements Runnable {
                    String epg_source_id_;
                    EpgSourceUpdater(String s) { epg_source_id_ = s; }
                    public void run() {
                        ServerSettings ss = settings_.getServerSettings();
                        try {
                            SynchronousDataProviderCallback callback = new SynchronousDataProviderCallback();
                            AdviserDataProvider data_provider = new AdviserDataProvider(ss, callback);
                            data_provider.setSynchronousMode(true);
                            data_provider.RefreshEpgSource(epg_source_id_);
                            final String user_message;
                            if (callback.result_code_ == StatusCode.STATUS_OK)
                                user_message = language_manager_.GetString(LocaleStrings.IDS_UPDATE_EPG_STARTED);
                            else
                                user_message = language_manager_.GetString(LocaleStrings.IDS_STATUS_FAILED);
                            //inform user
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(_context, user_message, Toast.LENGTH_LONG).show();
                                }
                            });

                        } catch (Exception e) {
                        }
                    }
                }

            //start asynchronous task to start epg update
            AsyncTask.execute(new EpgSourceUpdater(epg_sources_pane_.epg_source_id_));

        }
    }

    public void onActionPaneOption(int option_id, String selected_option) {
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == epg_source_settings_activity) {
            if (resultCode == RESULT_OK) {
                String param_str = data.getExtras().getString("parameters");
                ConciseParamMap parameters = (ConciseParamMap) Serializer.deserialize(param_str, ConciseParamMap.class);
                RequestSetEpgSourceSettings(current_epg_source_id_, parameters);
            }
        } else
        if (requestCode == epg_source_selection_activity) {
            if (resultCode == RESULT_OK) {
                String param_str = data.getExtras().getString("parameters");
                ConciseParamMap parameters = (ConciseParamMap) Serializer.deserialize(param_str, ConciseParamMap.class);
                int idx = Integer.parseInt(parameters.parameters.get(selected_epg_source_key));
                startEPGMappingActivity(idx);
            }
        }
    }
}
