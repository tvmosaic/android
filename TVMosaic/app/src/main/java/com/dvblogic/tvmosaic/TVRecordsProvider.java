package com.dvblogic.tvmosaic;

import android.util.Log;
import android.widget.Toast;
import com.dvblogic.dvblink_common.*;

import java.util.ArrayList;
import java.util.HashMap;

import static com.dvblogic.dvblink_common.ObjectRequester.DLRECORDER_SOURCE_ID;

class TVRecordsProvider implements IDataProvider {

    private static final String TAG = "TVRecordsProvider";

    private static final int STATE_NOT_READY = -1;
    private static final int STATE_READY = 1000;
    private static final int STATE_GET_ROOT_OBJECT = 1001;
    private static final int STATE_GET_CATEGORY_GROUP = 1002;
    private static final int STATE_GET_CATEGORY = 1003;
    private static final int STATE_GET_OBJECT = 1004;
    private static final int STATE_REMOVE_OBJECT = 1005;
    private static final int STATE_STOP_RECORDING = 1006;
    private static final int STATE_RECORDING_SETTINGS = 1007;
    private static final int STATE_GET_TARGETS = 1008;
    private static final int STATE_DO_SENDTO_OBJECT = 1009;
    private static final int STATE_GET_RESUME_INFO = 1010;
    private static final int STATE_SET_RESUME_INFO = 1011;

    public class CategoryContainer {
        String id;
        String name;
        int type;

        public ArrayList<CategoryContainer> categoryContainer = new ArrayList<>();

        public CategoryContainer(String id, String name, int type) {
            this.id = id;
            this.name = name;
            this.type = type;
        }
    }

    private int state = STATE_NOT_READY;

    private BaseActivity activity;
    private Object serverResponse;
    private ScanChannelsDataProvider serverDataProvider_;
    private StatusCode httpError = StatusCode.STATUS_OK;
    private String serverAddress;

    private HashMap<String, CategoryContainer> categoryById = new HashMap<>();
    private ArrayList<String> requestedCategory = new ArrayList<>();

    protected ArrayList<CategoryContainer> getCategory() {
        return new ArrayList<>(categoryById.values());
    }

    public TVRecordsProvider(BaseActivity activity) {
        this.activity = activity;
        serverDataProvider_ = activity.data_provider_;
        serverAddress = activity.serverSettings.serverAddress;
        state = STATE_READY;
    }

    public void ErrorProcessing(String command, StatusCode error) {
        httpError = error;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                activity.activateProgressBar(false);
                if (state == STATE_GET_RESUME_INFO) {
                    ResumeInfo ri = new ResumeInfo();
                    onResumeInfo(ri);
                } else
                if (state == STATE_SET_RESUME_INFO) {
                    onSetResumeInfo();
                } else
                if (activity.language_manager_ != null) {
                    String msg = String.format("%s", activity.language_manager_.ErrorToString(httpError));
                    Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
                }
                state = STATE_READY;
            }
        });
    }

    public void DataProcessing(String command, Object result_data) {
        serverResponse = result_data;
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (state == STATE_GET_ROOT_OBJECT || state == STATE_GET_CATEGORY) {
                    processCategory();
                } else if (state == STATE_GET_CATEGORY_GROUP) {
                    processCategoryGroup();
                } else if (state == STATE_GET_OBJECT) {
                    processObject();
                } else if (state == STATE_REMOVE_OBJECT) {
                    processRemoveObject();
                } else if (state == STATE_RECORDING_SETTINGS) {
                    processRecordingSettings();
                } else if (state == STATE_GET_TARGETS) {
                    processTargets();
                } else if (state == STATE_DO_SENDTO_OBJECT) {
                    processDoSendToObject();
                } else if (state == STATE_GET_RESUME_INFO) {
                    processResumeInfo();
                } else if (state == STATE_SET_RESUME_INFO) {
                    processSetResumeInfo();
                } else if (state == STATE_STOP_RECORDING) {
                    processStopRecording();
                } else {
                    throw new RuntimeException("unknown state");
                }
                serverResponse = null;
            }
        });
    }

    public void requestRoot() {
        requestCategory("");
    }

    protected void requestCategory(String id) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);

        if (id.isEmpty()) {
            state = STATE_GET_ROOT_OBJECT;
            id = ObjectRequester.OBJECT_ROOT_ID;
        } else {
            state = STATE_GET_CATEGORY;
        }

        serverDataProvider_.GetObjectAsXml(new ObjectRequester(id, serverAddress));
    }

    public void requestObject(String id) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);

        state = STATE_GET_OBJECT;

        serverDataProvider_.GetObjectAsXml(new ObjectRequester(id, serverAddress));
    }

    private void processObject() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);

            PlaybackItemContainer data = (PlaybackItemContainer) Serializer.deserialize(
                    (String) serverResponse,
                    PlaybackItemContainer.class
            );
            onObject(data);

        } catch (Exception exception) {
            Log.e(TAG, "processObject", exception);
        }
    }

    private void processCategory() {
        Log.i(TAG, "processCategory");
        try {
            PlaybackItemContainer data = (PlaybackItemContainer) Serializer.deserialize(
                    (String) serverResponse,
                    PlaybackItemContainer.class
            );

            int oldState = state;
            state = STATE_READY;
            if (oldState == STATE_GET_ROOT_OBJECT) {
                for (Container container: data.Containers.list()) {
                    if (container.SourceID.equalsIgnoreCase(DLRECORDER_SOURCE_ID)) {
                        requestCategory(container.ObjectID);
                        return;
                    }
                }
            }

            if (oldState == STATE_GET_CATEGORY) {
                onRequestCategoryComplete(data);
                for (Container container: data.Containers.list()) {
                    if (container.Type == ContainerType.CONTAINER_CATEGORY_GROUP) {
                        categoryById.put(container.ObjectID, new CategoryContainer(container.ObjectID, container.Name, ContainerType.CONTAINER_CATEGORY_GROUP));
                        requestedCategory.add(container.ObjectID);
                    }
                }
                requestCategoryGroup();
            }
        } catch (Exception exception) {
            Log.e(TAG, "processCategory", exception);
        }
    }

    public void requestCategoryGroup() {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        state = STATE_GET_CATEGORY_GROUP;
        if (requestedCategory.size() < 1) {
            return;
        }
        String objectID = requestedCategory.get(0);
        serverDataProvider_.GetObjectAsXml(new ObjectRequester(objectID, serverAddress));
    }

    private void processCategoryGroup() {
        try {
            state = STATE_READY;
            PlaybackItemContainer data = (PlaybackItemContainer) Serializer.deserialize((String) serverResponse,
                    PlaybackItemContainer.class);

            if (data != null) {
                String parentID = "";
                for (Container container: data.Containers.list()) {
                    parentID = container.ParentID;
                    if (!categoryById.containsKey(parentID)) {
                        continue;
                    }
                    CategoryContainer parentCategory = categoryById.get(parentID);
                    parentCategory.categoryContainer.add(new CategoryContainer(container.ObjectID, container.Name, ContainerType.CONTAINER_CATEGORY_GROUP));
                }
            }
            if (requestedCategory.size() > 0) {
                String lastRequestObjectID = requestedCategory.get(0);
                requestedCategory.remove(lastRequestObjectID);
            }

            if (requestedCategory.size() > 0) {
                requestCategoryGroup();
            } else {
                activity.activateProgressBar(false);
                onRequestCategoryGroupComplete();
            }
        } catch (Exception exception) {
            Log.e(TAG, "processCategoryGroup", exception);
        }
    }

    public void requestRecordingSettings() {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_RECORDING_SETTINGS;
        serverDataProvider_.GetRecordingSettings(new RecordingSettingsRequest());
    }

    private void processRecordingSettings() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);
            RecordingSettingsResponse resp = (RecordingSettingsResponse) serverResponse;
            onRecordingSettings(resp);
        } catch (Exception exception) {
            Log.e(TAG, "processRecordingSettings", exception);
        }
    }

    public void requestResumeInfo(String obj_id) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_GET_RESUME_INFO;
        serverDataProvider_.GetResumeInfo(new GetResumeInfoRequest(obj_id));
    }

    private void processResumeInfo() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);
            ResumeInfo resp = (ResumeInfo) serverResponse;
            onResumeInfo(resp);
        } catch (Exception exception) {
            Log.e(TAG, "processResumeInfo", exception);

            ResumeInfo ri = new ResumeInfo();
            onResumeInfo(ri);
        }
    }

    public void AddSendToItem(String object_id, String target_id) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_DO_SENDTO_OBJECT;

        SendToAddRequest request = new SendToAddRequest();
        SendToAddItem item = new SendToAddItem();
        item.description = "";
        item.objectId = object_id;
        item.target = target_id;
        request.add(item);
        serverDataProvider_.SendToAddItem(request);
    }

    private void processDoSendToObject() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);
            onOnSendToObjectCompleted();
        } catch (Exception exception) {
            Log.e(TAG, "processDoSendToObject", exception);
        }
    }

    public void requestTargets() {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_GET_TARGETS;
        serverDataProvider_.SendToGetTargets(new SendToTargetsRequest());
    }

    private void processTargets() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);
            SendToTargetsRespond send_to_targets = null;
            try {
                if (serverResponse != null && serverResponse instanceof XmlCommandResponse) {
                    XmlCommandResponse cmd_response = (XmlCommandResponse) serverResponse;
                    if (cmd_response.IsSuccess()) {
                        send_to_targets = (SendToTargetsRespond) Serializer.deserialize(
                                cmd_response.Params, SendToTargetsRespond.class);
                    }
                }
            } catch (Exception e) {
                send_to_targets = null;
            }
            onTargets(send_to_targets);
        } catch (Exception exception) {
            Log.e(TAG, "processTargets", exception);
        }
    }

    public void requestRemoveObject(String objectID) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_REMOVE_OBJECT;
        serverDataProvider_.RemoveObject(new ObjectRemover(objectID));
    }

    private void processRemoveObject() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);
            onRemoveObject();
        } catch (Exception exception) {
            Log.e(TAG, "processRemoveObject", exception);
        }
    }

    public void requestStopRecording(String objectID) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_STOP_RECORDING;
        serverDataProvider_.StopRecording(new StopRecording(objectID));
    }

    private void processStopRecording() {
        try {
            state = STATE_READY;
            activity.activateProgressBar(false);
            onStopRecording();
        } catch (Exception exception) {
            Log.e(TAG, "processStopRecording", exception);
        }
    }

    public void requestSetResumeInfo(String objectID, int pos) {
        if (serverDataProvider_ == null || state != STATE_READY) {
            return;
        }
        activity.activateProgressBar(true);
        state = STATE_SET_RESUME_INFO;
        serverDataProvider_.SetResumeInfo(new SetResumeInfoRequest(objectID, pos));
    }

    private void processSetResumeInfo() {
        state = STATE_READY;
        activity.activateProgressBar(false);
        onSetResumeInfo();
    }

    protected void onObject(PlaybackItemContainer data) {}
    protected void onRequestCategoryComplete(PlaybackItemContainer data) {}
    protected void onRequestCategoryGroupComplete() {}
    protected void onRemoveObject() {}
    protected void onStopRecording() {}
    protected void onRecordingSettings(RecordingSettingsResponse resp) {}
    protected void onTargets(SendToTargetsRespond resp) {}
    protected void onOnSendToObjectCompleted() {}
    protected void onResumeInfo(ResumeInfo ri) {}
    protected void onSetResumeInfo() {}

}

