package com.dvblogic.tvmosaic;

import java.util.Locale;

import TVMosaic.TVMosaic.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.*;

public class ProgramActivity extends BaseActivity implements OnBitmapLoadFinished, View.OnFocusChangeListener
{
    private static final String TAG = "ProgramActivity";

    enum EProgramActivityState {
        EPAS_NONE,
        EPAS_REQUESTING_PROGRAM_INFO,
        EPAS_ADDING_SCHEDULE,
        EPAS_REQUESTING_TIMERS_SINGLE,
        EPAS_REQUESTING_TIMERS_SCHEDULE,
        EPAS_REMOVING_TIMER,
        EPAS_READY
    }

    public final static int return_play = 200;
    public final static int RETURN_ADVISER_LIKE_THIS = 201;
    public final static int RETURN_ADVISER_CREATE_PROFILE = 202;

    EProgramActivityState						state_;
    String										channel_id_;
    String										long_channel_id;
    String										program_id_;
    String										channel_name_;
    Program program_						    = null;
    boolean										can_record_     				= false;
    boolean 									load_bitmap 					= false;

    boolean										adviser_mode					= false;
    boolean										enable_like_dislike				= false;
    boolean										enable_create_profile			= false;
    float										score_							= 0;
    float										rating_							= 0;
    String										profile_id						= "";

    private View likeButton;
    private View dislikeButton;

    public ProgramActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle bundle) {

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(bundle);
        show_exit_dialog = false;

        try {
            initializeSettings();

            setContentView(R.layout.program_activity);

            if (!Utils.usesRemoteControl(this)) {
                View prg_container = findViewById(R.id.program_details_container);
                prg_container.setPadding(
                        (int) (prg_container.getPaddingLeft() * 0.3),
                        (int) (prg_container.getPaddingTop() * 0.3),
                        (int) (prg_container.getPaddingRight() * 0.3),
                        (int) (prg_container.getPaddingBottom() * 0.3)
                );
            }

            ClearProgramInfo();

            // get parameters
            channel_id_ = this.getIntent().getExtras().getString("channel_id");
            long_channel_id = this.getIntent().getExtras().getString("long_channel_id");
            program_id_ = this.getIntent().getExtras().getString("program_id");
            channel_name_ = this.getIntent().getExtras().getString("channel_name");
            enable_create_profile = this.getIntent().getExtras().getBoolean("enable_create_profile");
            if (this.getIntent().hasExtra("adviser_mode")) {
                adviser_mode = this.getIntent().getExtras().getBoolean("adviser_mode");
                profile_id = this.getIntent().getExtras().getString("profile_id");
                score_ = this.getIntent().getExtras().getFloat("score");
                rating_ = this.getIntent().getExtras().getFloat("rating");
                if (rating_ < 0)
                    rating_ = -1;
                else if (rating_ > 0)
                    rating_ = 1;
                enable_like_dislike = this.getIntent().getExtras().getBoolean("enable_like_dislike");
            }

            can_record_ = settings_.getValue(SettingsManager.CAN_RECORD_KEY, false);

            customizeLayout();

            // set activity result to cancelled (unless timers will be changed)
            setResult(RESULT_CANCELED);

            programAdviserProvider  = new ProgramAdviserProvider();

            state_ = EProgramActivityState.EPAS_READY;

            getWindow().getDecorView().post(new Runnable() {
                public void run() {
                    requestProgramDetails();
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void finishWithResult(int result) {
        Intent intent = getIntent();
        if (needUpdateRecommendations) {
            intent.putExtra("need_update_recommendations", true);
        }
        if (needChannelUpdate) {
            ChannelIDList channels = new ChannelIDList();
            channels.add(channel_id_);
            String channels_xml = Serializer.serialize(channels);
            intent.putExtra("need_channel_update", channels_xml);
        }
        setResult(result, intent);
        finish();
    }

    protected void customizeLayout() {

        findViewById(R.id.content_layout).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                    View next_button = findViewById(R.id.play_button);
                    if (next_button.isEnabled()) {
                        next_button.requestFocus();
                    } else {
                        //try record button
                        next_button = findViewById(R.id.record_button);
                        if (next_button.isEnabled()) {
                            next_button.requestFocus();
                        }
                    }

                    return true;
                } else
                if (Common.backPressed(keyCode, event)) {
                    finishWithResult(RESULT_CANCELED);
                    return true;
                }

                return false;
            }
        });


        View play_button = findViewById(R.id.play_button);
        ((TextView)play_button.findViewById(R.id.title)).setText(language_manager_.GetString(LocaleStrings.IDS_PLAY_BUTTON_TITLE));
        boolean enable = false;

        if (program_ != null && program_.ID != null) {
            long dt_now = Utils.CurrentUtc();
            enable = (dt_now >= program_.StartTime) && (dt_now < program_.StartTime + program_.Duration);
        }

        play_button.setEnabled(enable);
        play_button.setOnClickListener(playClickListener);

        if (enable)
            play_button.requestFocus();

        View record_button = findViewById(R.id.record_button);
        ((TextView)record_button.findViewById(R.id.title)).setText(language_manager_.GetString(LocaleStrings.IDS_RECORD_BUTTON_TITLE));
        ((ImageView) record_button.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_record);
        record_button.setOnClickListener(recordClickListener);

        if (can_record_) {
            if (program_ == null || (!program_.IsRecord && program_.IsRepeatRecord))
                record_button.setEnabled(false); //do not offer record button if this is record series, but this episode was cancelled
            else
                record_button.setEnabled(true);
        } else {
            record_button.setVisibility(View.GONE);
        }

        customizeAdviserLayout();
    }

    protected void customizeAdviserLayout() {
        if (enable_create_profile && !enable_like_dislike) {
            View likeThisButton = findViewById(R.id.like_this_button);
            ((TextView) likeThisButton.findViewById(R.id.title)).setText(
                    language_manager_.GetString(LocaleStrings.IDS_LIKE_THIS)
            );
            ((ImageView) likeThisButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_like_this);
            likeThisButton.setVisibility(View.VISIBLE);
            likeThisButton.setOnClickListener(likeThisClickListener);

            View followMeButton = findViewById(R.id.follow_me_button);
            ((TextView) followMeButton.findViewById(R.id.title)).setText(
                    language_manager_.GetString(LocaleStrings.IDS_ADVISER_CREATE_PROFILE)
            );
            ((ImageView) followMeButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_follow);
            followMeButton.setVisibility(View.VISIBLE);
            followMeButton.setOnClickListener(followMeClickListener);
        }

        if (enable_like_dislike) {
            likeButton = findViewById(R.id.like_button);
            ((TextView) likeButton.findViewById(R.id.title)).setText(
                    language_manager_.GetString(LocaleStrings.IDS_ADVISER_RATE_LIKE)
            );
//            ((ImageView) likeButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_like);
            likeButton.setVisibility(View.VISIBLE);
            likeButton.setOnClickListener(rateClickListener);
            dislikeButton = findViewById(R.id.dislike_button);
            ((TextView) dislikeButton.findViewById(R.id.title)).setText(
                    language_manager_.GetString(LocaleStrings.IDS_ADVISER_RATE_DISLIKE)
            );
//            ((ImageView) dislikeButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_dislike);
            dislikeButton.setVisibility(View.VISIBLE);
            dislikeButton.setOnClickListener(rateClickListener);
            highlightRateButton();
        }
    }

    OnClickListener playClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            finishWithResult(return_play);
        }

    };

    OnClickListener recordClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (program_ == null) {
                return;
            }

            if (!program_.IsRecord) {
                AlertDialog.Builder ad = new AlertDialog.Builder(_context)
                        .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                        .setMessage(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_WHAT_TO_RECORD))
                        .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_WHAT_TO_RECORD_THIS_EPISODE),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        RequestAddSchedule(false);
                                    }
                                })
                        .setNeutralButton(language_manager_.GetString(LocaleStrings.IDS_WHAT_TO_RECORD_SERIES),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        RequestAddSchedule(true);
                                    }
                                })
                        .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL),
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                });

                ad.show();
            } else {
                String dlg_text = language_manager_.GetString(LocaleStrings.IDS_TIMERS_INFO_DLG_CANCEL_TIMER_TEXT);
                String positive_btn_text = language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES);
                String negative_btn_text = language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO);

                if (program_.IsRepeatRecord) {
                    dlg_text = language_manager_.GetString(LocaleStrings.IDS_TIMERS_INFO_DLG_CANCEL_WHAT_TEXT);
                    positive_btn_text = language_manager_.GetString(LocaleStrings.IDS_TIMERS_CANCEL_ONE);
                    negative_btn_text = language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL);
                }

                AlertDialog.Builder ad = new AlertDialog.Builder(_context)
                        .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                        .setMessage(dlg_text)
                        .setPositiveButton(positive_btn_text,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                        requestCancelTimer(false);
                                    }
                                })
                        .setNegativeButton(negative_btn_text,
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int whichButton) {
                                    }
                                });

                if (program_.IsRepeatRecord) {
                    ad.setNeutralButton(language_manager_.GetString(LocaleStrings.IDS_TIMERS_CANCEL_SCHEDULE),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    requestCancelTimer(true);
                                }
                            });
                }

                ad.show();
            }
        }
    };

    protected void requestCancelTimer(boolean cancelSeries) {
        activateProgressBar(true);
        state_ = cancelSeries ? EProgramActivityState.EPAS_REQUESTING_TIMERS_SCHEDULE : EProgramActivityState.EPAS_REQUESTING_TIMERS_SINGLE;

        RecordingsRequest rr = new RecordingsRequest();
        data_provider_.GetRecordings(rr);
    }

    protected void RequestAddSchedule(boolean recordSeries) {
        ByEpgSchedule bes = new ByEpgSchedule(channel_id_, program_id_, recordSeries);
        Schedule sch = new Schedule(bes);

        activateProgressBar(true);
        state_ = EProgramActivityState.EPAS_ADDING_SCHEDULE;
        data_provider_.AddSchedule(sch);
    }

    OnClickListener likeThisClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (program_ == null) {
                return;
            }
            createProfileInt(true);
        }
    };

    OnClickListener followMeClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            if (program_ == null) {
                return;
            }

            //ask user if new recommendation profile has to be added
            AlertDialog.Builder ad = new AlertDialog.Builder(_context)
                    .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_INFO))
                    .setMessage(language_manager_.GetString(LocaleStrings.IDS_CREATE_NEW_REC_PROFILE_INFO))
                    .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_OK),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    createProfileInt(false);
                                }
                            })
                    .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_CANCEL),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            });

            ad.show();
        }
    };

    protected void createProfileInt(boolean isLikeThis) {
        Log.i(TAG, "createProfileClickListener " + isLikeThis);
        Intent intent = getIntent();
        intent.putExtra("long_channel_id", long_channel_id);
        intent.putExtra("start_time", program_.StartTime);
        intent.putExtra("end_time", program_.StartTime + program_.Duration);
        intent.putExtra("profile_name", program_.Title);

        setResult(isLikeThis ? RETURN_ADVISER_LIKE_THIS : RETURN_ADVISER_CREATE_PROFILE, intent);
        finish();
    }

    OnClickListener rateClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            boolean isLike = view.getId() == R.id.like_button;
            Log.i(TAG, "rateClickListener " + isLike);

            rating_ = isLike ? 1 : -1;

            RateProgram rateProgram = new RateProgram();
            rateProgram.DVBLinkChannelId = long_channel_id;
            rateProgram.ProfileId = profile_id;
            rateProgram.StartTime = program_.StartTime;
            rateProgram.EndTime = program_.StartTime + program_.Duration;
            rateProgram.Rating = rating_;

            programAdviserProvider.requestRateProgram(rateProgram);
        }
    };

    private boolean needUpdateRecommendations = false;
    private boolean needChannelUpdate = false;

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void ClearProgramInfo() {
        program_ = null;

        TextView tv = (TextView) findViewById(R.id.program_name);
        tv.setText("");

        ImageView hd = (ImageView) findViewById(R.id.hd_icon);
        hd.setVisibility(View.INVISIBLE);

        ImageView repeat_premiere = (ImageView) findViewById(R.id.premiere_repeat_icon);
        repeat_premiere.setVisibility(View.INVISIBLE);

        View rec_icon = findViewById(R.id.rec_icon);
        rec_icon.setVisibility(View.INVISIBLE);

        tv = (TextView) findViewById(R.id.program_desc_line_1);
        tv.setText("");

        tv = (TextView) findViewById(R.id.program_desc_line_2);
        tv.setText("");

        tv = (TextView) findViewById(R.id.program_desc_line_3);
        tv.setText("");

        tv = (TextView) findViewById(R.id.description_text);
        tv.setText("");

        ImageView iv = (ImageView) findViewById(R.id.description_image);
        iv.setVisibility(View.INVISIBLE);

    }

    public void DataProcessing(String command, Object result_data) {
        response_ = result_data;

        if (state_ == EProgramActivityState.EPAS_REQUESTING_PROGRAM_INFO) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processProgramDetails();
                }
            });
        }

        if (state_ == EProgramActivityState.EPAS_ADDING_SCHEDULE || state_ == EProgramActivityState.EPAS_REMOVING_TIMER) {
            runOnUiThread(new Runnable() {
                public void run() {
                    dismissProgressBar();
                    state_ = EProgramActivityState.EPAS_READY;

                    needChannelUpdate = true;
                    requestProgramDetails();
                }
            });
        }

        if (state_ == EProgramActivityState.EPAS_REQUESTING_TIMERS_SCHEDULE ||
                state_ == EProgramActivityState.EPAS_REQUESTING_TIMERS_SINGLE) {
            runOnUiThread(new Runnable() {
                public void run() {
                    processTimerList();
                }
            });
        }

    }

    public void ErrorProcessing(String command, StatusCode error) {
        http_error_ = error;
        runOnUiThread(new Runnable() {
            public void run() {
                ProcessDataProviderError();
            }
        });
        state_ = EProgramActivityState.EPAS_READY;
    }

    protected void processTimerList() {
        if (response_ != null) {
            RecordingsList rcs = (RecordingsList) response_;
            // find recording, matching our program
            for (Recording r : rcs.list()) {
                if (r.Program.ID.equals(program_id_) && r.ChannelID.equals(channel_id_)) {
                    if (state_ == EProgramActivityState.EPAS_REQUESTING_TIMERS_SINGLE) {
                        state_ = EProgramActivityState.EPAS_REMOVING_TIMER;
                        RecordingRemover rr = new RecordingRemover(r.RecordingID);
                        data_provider_.RemoveRecording(rr);
                    } else
                    if (state_ == EProgramActivityState.EPAS_REQUESTING_TIMERS_SCHEDULE) {
                        state_ = EProgramActivityState.EPAS_REMOVING_TIMER;
                        ScheduleRemover sr = new ScheduleRemover(r.ScheduleID);
                        data_provider_.RemoveSchedule(sr);
                    }

                    break;
                }
            }
        }

    }

    protected void requestProgramDetails() {
        activateProgressBar(true);

        state_ = EProgramActivityState.EPAS_REQUESTING_PROGRAM_INFO;
        EpgSearcher es = new EpgSearcher(channel_id_, program_id_);
        data_provider_.SearchEpg(es);
    }

    protected String AddLineToString(String valstr, String res_id, String str_to_add) {
        if (str_to_add != null && !str_to_add.equals("")) {
            str_to_add = str_to_add.replace("/", ", ");
            if (!valstr.equals(""))
                valstr += "\n";
            valstr += String.format("<i>%s:</i> %s", language_manager_.GetString(res_id), str_to_add);
        }
        return valstr;
    }

    protected Program getProgram(ChannelIdWithProgramsList epg_data) {
        if (epg_data.list() == null)
            return null;
        for (ChannelIdWithPrograms cp : epg_data.list()) {
            for (Program prg : cp.Programs.list()) {
                if (prg.ID.equals(program_id_))
                    return prg;
            }
        }
        return null;
    }

    @Override
    public void loadFinished() {
        setProgramDescription();
    }


    protected void processProgramDetails() {
        dismissProgressBar();
        state_ = EProgramActivityState.EPAS_READY;

        try {
            if (response_ != null) {
                ChannelIdWithProgramsList epg_data = (ChannelIdWithProgramsList) response_;
                program_ = getProgram(epg_data);
                if (program_ != null) {
                    // thumbnail
                    ImageView iv = (ImageView) findViewById(R.id.description_image);
                    load_bitmap = (program_.Image != null && !program_.Image.equals(""));
                    if (load_bitmap) {
                        new BitmapLoader(this, this, program_.Image, (ImageView) findViewById(R.id.description_image), null).start();
                    }

                    setProgramInfo();
                    setProgramDescription();

                    customizeLayout();

                } else {
                    setProgramInfo();

                    customizeLayout();

                }
                state_ = EProgramActivityState.EPAS_READY;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void setProgramInfo() {
        TextView tv = (TextView) findViewById(R.id.program_name);
        tv.setText(program_ == null || program_.Title == null ? language_manager_.GetString(LocaleStrings.IDS_NO_PROGINFO_AVAILABLE) : program_.Title);

        if (program_ == null)
            return;

        ImageView hd = (ImageView) findViewById(R.id.hd_icon);
        hd.setVisibility(program_.IsHdtv ? View.VISIBLE : View.INVISIBLE);

        ImageView repeat_premiere = (ImageView) findViewById(R.id.premiere_repeat_icon);
        if (program_.IsRepeat || program_.IsPremiere) {
            repeat_premiere.setImageResource(program_.IsRepeat ? R.drawable.tvm_icon_repeat : R.drawable.tvm_icon_premiere);
            repeat_premiere.setVisibility(View.VISIBLE);
        } else {
            repeat_premiere.setVisibility(View.INVISIBLE);
        }

        ImageView rec_icon = (ImageView) findViewById(R.id.rec_icon);
        if (program_.IsRecord) {

            if (program_.IsRecordConflict)
                rec_icon.setImageResource(R.drawable.tvm_icon_rec_conflict);
            else
            if (program_.IsRepeatRecord)
                rec_icon.setImageResource(R.drawable.tvm_icon_rec_series);
            else
                rec_icon.setImageResource(R.drawable.tvm_icon_rec);

            rec_icon.setVisibility(View.VISIBLE);
        } else {
            rec_icon.setVisibility(View.INVISIBLE);
        }

        // episode
        String str = ((program_.Subname == null) ? "" : program_.Subname);

        String ES = "";
        if (program_.SeasonNum != 0)
            ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_SEASON), program_.SeasonNum);
        if (program_.EpisodeNum != 0) {
            if (program_.SeasonNum != 0)
                ES += ", ";
            ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_EPISODE),
                    program_.EpisodeNum);
        }
        if (str.isEmpty()) {
            str += ES;
        } else {
            if (!ES.isEmpty())
                str += "  |  " + ES;
        }

        tv = (TextView) findViewById(R.id.program_desc_line_1);
        tv.setText(str);

        // channel & time
        String strChannel = channel_name_;

        String strDate = null;
        if (Utils.isToday(program_.StartTime)) {
            strDate = language_manager_.GetString(LocaleStrings.IDS_TODAY);
        } else {
            strDate = Utils.UtcToMediumDateString(_context, program_.StartTime);
        }

        String strStart = Utils.UtcToShortTimeString(_context, program_.StartTime);
        String strEnd = Utils.UtcToShortTimeString(_context, program_.StartTime + program_.Duration);

        int duration_minutes = (int) (program_.Duration / 60.0);
        String strDuration = String.format(Locale.getDefault(), "%s %d %s",
                language_manager_.GetString(LocaleStrings.IDS_DURATION), duration_minutes,
                language_manager_.GetString(LocaleStrings.IDS_MINUTE));

        str = String.format("%s  |  %s  |  %s - %s  |  %s", strChannel, strDate, strStart, strEnd, strDuration);

        tv = (TextView) findViewById(R.id.program_desc_line_2);
        tv.setText(str);

        //categories
        str = "";
        if (program_.Keywords != null && !program_.Keywords.equals("")) {
            str = program_.Keywords.replace("/", ", ");
        }
        tv = (TextView) findViewById(R.id.program_desc_line_3);
        tv.setText(str);

    }

    protected void setProgramDescription() {

        TextView tv = (TextView) findViewById(R.id.description_text);

        String desc_str = new String();
        if (program_.ShortDesc != null && !program_.ShortDesc.equals(""))
            desc_str += program_.ShortDesc;

        String ext_str = new String();
        ext_str += "\n";
        if (program_.Actors != null && !program_.Actors.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_ACTORS_HEADER, program_.Actors);
            ext_str += "\n";
        }
        if (program_.Guests != null && !program_.Guests.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_GUESTS_HEADER, program_.Guests);
            ext_str += "\n";
        }
        if (program_.Directors != null && !program_.Directors.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_DIRECTORS_HEADER, program_.Directors);
            ext_str += "\n";
        }
        if (program_.Producers != null && !program_.Producers.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_PRODUCERS_HEADER, program_.Producers);
            ext_str += "\n";
        }
        if (program_.Writers != null && !program_.Writers.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_WRITERS_HEADER, program_.Writers);
            ext_str += "\n";
        }
        if (program_.Year > 0) {
            String local = String.format(Locale.getDefault(), "%d", program_.Year);
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_YEAR_HEADER, local);
        }

        final String desc_str_f = desc_str.toString().replace("\n", "<br />");
        final String ext_str_f = ext_str.toString().replace("\n", "<br />");

        ImageView iv = (ImageView) findViewById(R.id.description_image);
        if (load_bitmap) {
            iv.setVisibility(View.VISIBLE);
            Display display = getWindowManager().getDefaultDisplay();
            Point pt = new Point();
            display.getSize(pt);
            iv.measure(pt.x, pt.y);

            if (iv.getMeasuredWidth() == 0 || iv.getMeasuredHeight() == 0) {
                Spanned s = Html.fromHtml(desc_str_f + ext_str_f);
                tv.setText(s);
            } else {
                float margin = getResources().getDimension(R.dimen.program_activity_thumb_margin);
                final int wd = (int) (iv.getMeasuredWidth() + margin);
                float ht = iv.getMeasuredHeight() + margin;

                float text_spacing = tv.getPaint().getFontSpacing();
                final int lines = (int) (ht / text_spacing);

                final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tv.getLayoutParams();
                params.setMargins(wd, 0, 0, 0);
                tv.setText(Html.fromHtml(desc_str_f + ext_str_f));
                tv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @SuppressWarnings("deprecation")
                    @Override
                    public void onGlobalLayout() {

                        TextView tv = (TextView) findViewById(R.id.description_text);
                        int linesCount = tv.getLayout().getLineCount();

                        // restore the margin
                        params.setMargins(0, 0, 0, 0);
                        SpannableString spanS = new SpannableString(Html.fromHtml(desc_str_f + ext_str_f));

                        if (linesCount <= lines) {
                            spanS.setSpan(new InfoLeadingMarginSpan2(lines, wd), 0, spanS.length(), 0);
                            tv.setText(spanS);
                        } else {
                            // find the breakpoint where to break the String.
                            int breakpoint = tv.getLayout().getLineEnd(lines - 1);

                            Spannable s1 = new SpannableStringBuilder(spanS, 0, breakpoint);
                            s1.setSpan(new InfoLeadingMarginSpan2(lines, wd), 0, s1.length(), 0);
                            Spannable s2 = new SpannableStringBuilder(System.getProperty("line.separator"));
                            Spannable s3 = new SpannableStringBuilder(spanS, breakpoint, spanS.length());
                            // It is needed to set a zero-margin span on for the text under the image to prevent the space on the right!
                            s3.setSpan(new InfoLeadingMarginSpan2(0, 0), 0, s3.length(), 0);
                            tv.setText(TextUtils.concat(s1, s2, s3));

                            // Align the text with the image by removing the rule that the text is to the right of the image
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tv.getLayoutParams();
                            int[] rules = params.getRules();
                            rules[RelativeLayout.RIGHT_OF] = 0;

                        }

                        // remove the GlobalLayoutListener
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            tv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            tv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }

                    }
                });
            }
        }
        else {
            iv.setVisibility(View.GONE);
            Spanned s = Html.fromHtml(desc_str_f + ext_str_f);
            tv.setText(s);
        }
    }

    protected void ProcessDataProviderError() {
        try {
            String msg = String.format("%s", language_manager_.ErrorToString(http_error_));
            Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();

            state_ = EProgramActivityState.EPAS_READY;
            activateProgressBar(false);
        } catch (Exception e) {
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    public void onFocusChange(View v, boolean hasFocus) {
        int i = v.getId();
    }

    private ProgramAdviserProvider programAdviserProvider;

    private class ProgramAdviserProvider extends AdviserProvider {
        public ProgramAdviserProvider() {
            super(ProgramActivity.this, serverSettings, language_manager_.getID());
        }
        @Override
        protected void onRateProgram() {
            needUpdateRecommendations = true;
            highlightRateButton();
        }
    }

    private void highlightRateButton() {
        ((TextView) likeButton.findViewById(R.id.title)).setTextColor(
                getResources().getColor(rating_ > 0 ? R.color.new_button_accent : R.color.white)
        );
        ((ImageView) likeButton.findViewById(R.id.icon)).setImageResource(
                rating_ > 0 ? R.drawable.tvm_icon_like_active : R.drawable.tvm_icon_like
        );
        ((TextView) dislikeButton.findViewById(R.id.title)).setTextColor(
                getResources().getColor(rating_ < 0 ? R.color.new_button_accent : R.color.white)
        );
        ((ImageView) dislikeButton.findViewById(R.id.icon)).setImageResource(
                rating_ < 0 ? R.drawable.tvm_icon_dislike_active : R.drawable.tvm_icon_dislike
        );
    }

    @Override
    public void onBackPressed() {
        finishWithResult(RESULT_CANCELED);
    }
}
