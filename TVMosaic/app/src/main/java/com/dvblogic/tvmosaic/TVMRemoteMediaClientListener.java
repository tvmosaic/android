package com.dvblogic.tvmosaic;

import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaQueueItem;
import com.google.android.gms.cast.MediaStatus;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;

public class TVMRemoteMediaClientListener implements RemoteMediaClient.Listener {

    private CastSession mCastSession = null;

	TVMRemoteMediaClientListener(CastSession cs) {
		mCastSession = cs;
	}

	@Override
	public void onStatusUpdated () {
		try {
            if (mCastSession != null) {
                RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
                int state = remoteMediaClient.getPlayerState();
                if (state == MediaStatus.PLAYER_STATE_PAUSED) {
                    //treat pause as stop for the live streams
                    MediaQueueItem mqi = remoteMediaClient.getCurrentItem();
                    if (mqi != null && mqi.getMedia().getStreamType() == MediaInfo.STREAM_TYPE_LIVE)
                        remoteMediaClient.queueRemoveItem(mqi.getItemId(), null);
                }
            }
		} catch(Exception e) {

		}
    }

	@Override
	public void onMetadataUpdated () {
    }

	@Override
	public void onQueueStatusUpdated () {
    }

	@Override
	public void onPreloadStatusUpdated () {
    }

	@Override
	public void onSendingRemoteMediaRequest () {
    }

	@Override
	public void onAdBreakStatusUpdated () {
    }
}
