package com.dvblogic.tvmosaic;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Locale;

import TVMosaic.TVMosaic.R;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.MediaRouteButton;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.dvblogic.dvblink_common.*;
import com.google.android.gms.cast.MediaInfo;
import com.google.android.gms.cast.MediaMetadata;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;
import com.google.android.gms.cast.framework.CastSession;
import com.google.android.gms.cast.framework.SessionManagerListener;
import com.google.android.gms.cast.framework.media.RemoteMediaClient;
import com.google.android.gms.common.images.WebImage;

// TODO: 01.06.2017 обновлять по таймеру состаяние записи, если она инпрогресс

public class TVRecordActivity extends BaseActivity implements OnBitmapLoadFinished, View.OnFocusChangeListener, ActionOptionsPane.ActionOptionsPaneEventListener
{
    private static final String TAG = "TVRecordActivity";

    static final int play_activity_request_code = 110;

    private class TVMSessionManagerListener implements SessionManagerListener<CastSession> {

        @Override
        public void onSessionEnded(CastSession session, int error) {
            if (session == mCastSession) {
                mCastSession = null;
            }
        }

        @Override
        public void onSessionResumed(CastSession session, boolean wasSuspended) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarted(CastSession session, String sessionId) {
            mCastSession = session;
        }

        @Override
        public void onSessionStarting(CastSession session) {
        }

        @Override
        public void onSessionStartFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionEnding(CastSession session) {
        }

        @Override
        public void onSessionResuming(CastSession session, String sessionId) {
        }

        @Override
        public void onSessionResumeFailed(CastSession session, int error) {
        }

        @Override
        public void onSessionSuspended(CastSession session, int reason) {
        }
    }

    private class TVRecordsProviderEx extends TVRecordsProvider {
        public TVRecordsProviderEx() {
            super(TVRecordActivity.this);
        }

        @Override
        protected void onRemoveObject() {
            result_ = RESULT_FIRST_USER;
            finishWithResult();
        }

        @Override
        protected void onStopRecording() {
            //set activity result to update this record
            result_ = RESULT_FIRST_USER;
            //update info for this activity
            updateRecord();
        }

        @Override
        protected void onObject(PlaybackItemContainer data) {
            for (Item resultItem: data.Items.list()) {
                if (resultItem.ObjectID.equals(recording_id_) && resultItem instanceof RecordedTV) {
                    showRecordedTV((RecordedTV)resultItem);
                    return;
                }
            }
        }

        @Override
        protected void onTargets(SendToTargetsRespond resp) {
            processTargetsResponse(resp);
        }

        @Override
        protected void onOnSendToObjectCompleted() {
            processSendToObjectCompleted();
        }

        @Override
        protected void onResumeInfo(ResumeInfo ri) {
            processResumeInfo(ri);
        }

        @Override
        protected void onSetResumeInfo() {
            int l=0;
        }
    }

    private ActionOptionsPane choice_menu_;
    private TVRecordsProviderEx provider;
    private RecordedTV recordedTV = null;
    private String recording_id_ = null;
    boolean load_bitmap  = false;
    private ChannelIDList channels_to_refresh = new ChannelIDList();
    int result_ = RESULT_CANCELED;

    //cast controls and variables
    private CastContext mCastContext = null;
    private CastSession mCastSession = null;
    private final SessionManagerListener<CastSession> mSessionManagerListener = new TVMSessionManagerListener();

    public TVRecordActivity() {
        super();
    }

    @Override
    protected void onCreate(Bundle bundle) {

        requestWindowFeature(Window.FEATURE_INDETERMINATE_PROGRESS);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        super.onCreate(bundle);
        show_exit_dialog = false;

        try {
            initializeSettings();

            String[] sizeLabels = {
                    language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE_BYTE),
                    language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE_PREFIX_K),
                    language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE_PREFIX_M),
                    language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE_PREFIX_G),
                    language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE_PREFIX_T),
                    language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE_PREFIX_P),
                    language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE_PREFIX_E)
            };
            this.sizeLabels = sizeLabels;

            setContentView(R.layout.tvrecord_activity);

            //initialize cast context only on mobile devices and only if google apis are available
            if (!Utils.usesRemoteControl(this) && Common.isGoogleApiAvailable(this)) {
                try {
                    mCastContext = CastContext.getSharedInstance(this);
                } catch (Exception e) {
                    mCastContext = null;
                }
            }

            if (!Utils.usesRemoteControl(this)) {
                View prg_container = findViewById(R.id.program_details_container);
                prg_container.setPadding(
                        (int) (prg_container.getPaddingLeft() * 0.3),
                        (int) (prg_container.getPaddingTop() * 0.3),
                        (int) (prg_container.getPaddingRight() * 0.3),
                        (int) (prg_container.getPaddingBottom() * 0.3)
                );
            }

            recording_id_ = this.getIntent().getExtras().getString("recording_id");
            ClearProgramInfo();

            setResult(RESULT_CANCELED);

            provider = new TVRecordsProviderEx();

            getWindow().getDecorView().post(new Runnable() {
                public void run() {
                    provider.requestObject(recording_id_);
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        if (mCastContext != null) {
            mCastContext.getSessionManager().addSessionManagerListener(
                    mSessionManagerListener, CastSession.class);
            if (mCastSession == null) {
                mCastSession = CastContext.getSharedInstance(this).getSessionManager()
                        .getCurrentCastSession();
            }
            if (mCastSession != null) {
                RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
                if (remoteMediaClient != null)
                    remoteMediaClient.addListener(new TVMRemoteMediaClientListener(mCastSession));
            }

        }
        super.onResume();
    }

    @Override
    protected void onPause() {
        if (mCastContext != null) {
            mCastContext.getSessionManager().removeSessionManagerListener(mSessionManagerListener, CastSession.class);
        }
        super.onPause();
    }

    @Override
    public void DataProcessing(String command, Object result_data) {
        provider.DataProcessing(command, result_data);
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        provider.DataProcessing(command, error);
    }

    private void showRecordedTV(RecordedTV recordedTV) {
        this.recordedTV = recordedTV;
        processProgramDetails();
    }

    private void updateRecord() {
        provider.requestObject(recording_id_);
    }

    private void finishWithResult() {
        if (channels_to_refresh.size() > 0) {
            Intent intent = getIntent();

            String channels_xml = Serializer.serialize(channels_to_refresh);
            intent.putExtra("need_channel_update", channels_xml);
            setResult(result_, intent);
        } else {
            setResult(result_);
        }
        finish();
    }

    protected void customizeLayout() {

        findViewById(R.id.content_layout).setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if (event.getAction() == KeyEvent.ACTION_UP)
                    return true;

                if (keyCode == KeyEvent.KEYCODE_DPAD_RIGHT || keyCode == KeyEvent.KEYCODE_DPAD_LEFT) {
                    View playButton = findViewById(R.id.play_button);
                    playButton.requestFocus();
                    return true;
                }

                return false;
            }
        });

        View playButton = findViewById(R.id.play_button);
        ((TextView) playButton.findViewById(R.id.title)).setText(
                language_manager_.GetString(LocaleStrings.IDS_PLAY_BUTTON_TITLE)
        );
        ((ImageView) playButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_play);
        playButton.setOnClickListener(playClickListener);
        playButton.setVisibility(View.VISIBLE);
        playButton.setEnabled(true);

        View removeButton = findViewById(R.id.remove_button);
        ((TextView) removeButton.findViewById(R.id.title)).setText(
                language_manager_.GetString(LocaleStrings.IDS_REMOVE)
        );
        ((ImageView) removeButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_remove);
        removeButton.setOnClickListener(removeClickListener);
        removeButton.setEnabled(recordedTV.State != RecordedTVState.RTVS_IN_PROGRESS);
        removeButton.setVisibility(recordedTV.State != RecordedTVState.RTVS_IN_PROGRESS ? View.VISIBLE : View.GONE);

        View stopButton = findViewById(R.id.stop_button);
        ((TextView) stopButton.findViewById(R.id.title)).setText(
                language_manager_.GetString(LocaleStrings.IDS_STOP)
        );
        ((ImageView) stopButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_stop);
        stopButton.setOnClickListener(stopClickListener);
        stopButton.setEnabled(recordedTV.State == RecordedTVState.RTVS_IN_PROGRESS);
        stopButton.setVisibility(recordedTV.State == RecordedTVState.RTVS_IN_PROGRESS ? View.VISIBLE : View.GONE);

        View sendtoButton = findViewById(R.id.sendto_button);
        ((TextView) sendtoButton.findViewById(R.id.title)).setText(
                language_manager_.GetString(LocaleStrings.IDS_SENDTO_BUTTON_TITLE)
        );
        ((ImageView) sendtoButton.findViewById(R.id.icon)).setImageResource(R.drawable.tvm_icon_sendto);
        sendtoButton.setOnClickListener(sendtoClickListener);
        sendtoButton.setEnabled(recordedTV.State != RecordedTVState.RTVS_IN_PROGRESS);
        sendtoButton.setVisibility(recordedTV.State != RecordedTVState.RTVS_IN_PROGRESS ? View.VISIBLE : View.GONE);

        playButton.requestFocus();
    }

    private void playCurrentRecord(int resume_pos) {
        if (recordedTV != null) {
            if (mCastSession != null) {
                startChromcastPlayback(resume_pos);
            } else {
                Intent i = new Intent();
                if (serverSettings.transcodingSupported && settings_.GetValueOrDefault(Settings.TRANSCODING_ENABLED_KEY, Settings.TRANSCODING_ENABLED_DEFAULT)) {
                    i.setClass(this, PlayTranscodedRecordingActivity.class);
                } else {
                    i.setClass(this, VLCPlayRecordingActivity.class);
                }
                String xml_recording = Serializer.serialize(recordedTV);
                i.putExtra("recording", xml_recording);
                i.putExtra("resume_position", resume_pos);
                startActivityForResult(i, play_activity_request_code);
            }
        }
    }

    OnClickListener playClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            //request resume position
            provider.requestResumeInfo(recordedTV.ObjectID);
        }
    };

    class ResumeOnClickListener implements DialogInterface.OnClickListener {
        int resume_pos_ = 0;

        public ResumeOnClickListener(int resume_pos) {
            resume_pos_ = resume_pos;
        }

        public void onClick(DialogInterface dialog, int which) {
            playCurrentRecord(resume_pos_);
        }
    }

    protected void processResumeInfo(ResumeInfo ri) {
        //do not ask about resume position for chromecast. We will not be able to update it back anyway
        if (mCastSession == null && ri.pos > 0) {
            new AlertDialog.Builder(_context)
                    .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_INFO))
                    .setMessage(
                            language_manager_.GetString(LocaleStrings.IDS_PLAY_RESUME_QUESTION))
                    .setOnCancelListener(new DialogInterface.OnCancelListener() {
                        public void onCancel(DialogInterface dialog) {
                            playCurrentRecord(0);
                        }
                    })
                    .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
                            new ResumeOnClickListener(ri.pos))
                    .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    playCurrentRecord(0);
                                }
                            }).show();
        } else {
            playCurrentRecord(0);
        }
    }

    OnClickListener removeClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {

            new AlertDialog.Builder(_context)
                    .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                    .setMessage(
                            language_manager_.GetString(LocaleStrings.IDS_RECORD_INFO_DLG_REMOVE_TEXT))
                    .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    provider.requestRemoveObject(recording_id_);
                                }
                            })
                    .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            }).show();
        }
    };

    OnClickListener sendtoClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            provider.requestTargets();
        }
    };

    OnClickListener stopClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            new AlertDialog.Builder(_context)
                    .setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION))
                    .setMessage(
                            language_manager_.GetString(LocaleStrings.IDS_RECORD_INFO_DLG_STOP_TEXT))
                    .setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    if (recordedTV != null)
                                        channels_to_refresh.add(recordedTV.ChannelId);

                                    provider.requestStopRecording(recording_id_);
                                }
                            })
                    .setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            }).show();
        }
    };

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    protected void ClearProgramInfo() {

        TextView tv = (TextView) findViewById(R.id.program_name);
        tv.setText("");

        ImageView hd = (ImageView) findViewById(R.id.hd_icon);
        hd.setVisibility(View.INVISIBLE);

        ImageView repeat_premiere = (ImageView) findViewById(R.id.premiere_repeat_icon);
        repeat_premiere.setVisibility(View.INVISIBLE);

        View genre_dot = findViewById(R.id.genre_dot);
        genre_dot.setVisibility(View.INVISIBLE);

        tv = (TextView) findViewById(R.id.program_desc_line_1);
        tv.setText("");

        tv = (TextView) findViewById(R.id.program_desc_line_2);
        tv.setText("");

        tv = (TextView) findViewById(R.id.program_desc_line_3);
        tv.setText("");

        tv = (TextView) findViewById(R.id.description_text);
        tv.setText("");

        ImageView iv = (ImageView) findViewById(R.id.description_image);
        iv.setVisibility(View.INVISIBLE);

        //hide buttons
        View playButton = findViewById(R.id.play_button);
        playButton.setVisibility(View.GONE);

        View removeButton = findViewById(R.id.remove_button);
        removeButton.setVisibility(View.GONE);

        View stopButton = findViewById(R.id.stop_button);
        stopButton.setVisibility(View.GONE);
    }

    protected String AddLineToString(String valstr, String res_id, String str_to_add) {
        if (str_to_add != null && !str_to_add.equals("")) {
            str_to_add = str_to_add.replace("/", ", ");
            if (!valstr.equals(""))
                valstr += "\n";
            valstr += String.format("<i>%s:</i> %s", language_manager_.GetString(res_id), str_to_add);
        }
        return valstr;
    }

    @Override
    public void loadFinished() {
        setProgramDescription();
    }

    protected void processProgramDetails() {
        if (recordedTV != null) {
            // thumbnail
            ImageView iv = (ImageView) findViewById(R.id.description_image);
            load_bitmap = (recordedTV.VideoInfo.Image != null && !recordedTV.VideoInfo.Image.equals(""));
            if (load_bitmap) {
                new BitmapLoader(this, this, recordedTV.VideoInfo.Image, (ImageView) findViewById(R.id.description_image), null).start();
            }

            setProgramInfo();
            setProgramDescription();

            customizeLayout();

        } else {
            ClearProgramInfo();
        }
    }

    protected void setProgramInfo() {
        TextView tv = (TextView) findViewById(R.id.program_name);
        tv.setText(recordedTV == null || recordedTV.VideoInfo.Title == null ? language_manager_.GetString(LocaleStrings.IDS_NO_PROGINFO_AVAILABLE) : recordedTV.VideoInfo.Title);

        if (recordedTV == null)
            return;

        ImageView hd = (ImageView) findViewById(R.id.hd_icon);
        hd.setVisibility(recordedTV.VideoInfo.IsHdtv ? View.VISIBLE : View.INVISIBLE);

        ImageView repeat_premiere = (ImageView) findViewById(R.id.premiere_repeat_icon);
        if (recordedTV.VideoInfo.IsRepeat || recordedTV.VideoInfo.IsPremiere) {
            repeat_premiere.setImageResource(recordedTV.VideoInfo.IsRepeat ? R.drawable.tvm_icon_repeat : R.drawable.tvm_icon_premiere);
            repeat_premiere.setVisibility(View.VISIBLE);
        } else {
            repeat_premiere.setVisibility(View.INVISIBLE);
        }

        View genre_dot = findViewById(R.id.genre_dot);
        if (Constants.HasGenreColor(recordedTV.VideoInfo)) {
            genre_dot.setVisibility(View.VISIBLE);
            GradientDrawable bgShape = (GradientDrawable)genre_dot.getBackground();
            bgShape.setColor(ContextCompat.getColor(_context, Constants.GetColorFromGenre(recordedTV.VideoInfo)));
        } else {
            genre_dot.setVisibility(View.INVISIBLE);
        }

        // episode
        String str = ((recordedTV.VideoInfo.Subname == null) ? "" : recordedTV.VideoInfo.Subname);

        String ES = "";
        if (recordedTV.VideoInfo.SeasonNum != 0)
            ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_SEASON), recordedTV.VideoInfo.SeasonNum);
        if (recordedTV.VideoInfo.EpisodeNum != 0) {
            if (recordedTV.VideoInfo.SeasonNum != 0)
                ES += ", ";
            ES += String.format("%s %d", language_manager_.GetString(LocaleStrings.IDS_EPISODE),
                    recordedTV.VideoInfo.EpisodeNum);
        }
        if (str.isEmpty()) {
            str += ES;
        } else {
            if (!ES.isEmpty())
                str += "  |  " + ES;
        }

        tv = (TextView) findViewById(R.id.program_desc_line_1);
        tv.setText(str);

        // channel & time
        String strChannel = recordedTV.ChannelName;

        String strDate = null;
        if (Utils.isToday(recordedTV.VideoInfo.StartTime)) {
            strDate = language_manager_.GetString(LocaleStrings.IDS_TODAY);
        } else {
            strDate = Utils.UtcToMediumDateString(_context, recordedTV.VideoInfo.StartTime);
        }

        String strStart = Utils.UtcToShortTimeString(_context, recordedTV.VideoInfo.StartTime);
        String strEnd = Utils.UtcToShortTimeString(_context, recordedTV.VideoInfo.StartTime + recordedTV.VideoInfo.Duration);

        int duration_minutes = (int) (recordedTV.VideoInfo.Duration / 60.0);
        String strDuration = String.format(Locale.getDefault(), "%s %d %s",
                language_manager_.GetString(LocaleStrings.IDS_DURATION), duration_minutes,
                language_manager_.GetString(LocaleStrings.IDS_MINUTE));

        str = String.format("%s  |  %s  |  %s - %s  |  %s", strChannel, strDate, strStart, strEnd, strDuration);
        if (recordedTV.Size > 0) {
            str += " | " + language_manager_.GetString(LocaleStrings.IDS_FILE_SIZE) + " " + formatSize(recordedTV.Size, sizeLabels);
        }

        tv = (TextView) findViewById(R.id.program_desc_line_2);
        tv.setText(str);

        //categories
        str = "";
        if (recordedTV.VideoInfo.Keywords != null && !recordedTV.VideoInfo.Keywords.equals("")) {
            str = recordedTV.VideoInfo.Keywords.replace("/", ", ");
        }
        tv = (TextView) findViewById(R.id.program_desc_line_3);
        tv.setText(str);

    }

    String[] sizeLabels;

    public static String formatSize(long bytes, String[] labels) {
        // labels => { B, K, M. G, T, P, E }
        if (labels.length != 7) {
            throw new RuntimeException("param error");
        }
        if (bytes < 1024) {
            return bytes + " " + labels[0];
        }
        int numberOfLeadingZeros = (63 - Long.numberOfLeadingZeros(bytes)) / 10;
        return String.format(
                Locale.getDefault(),
                "%.1f %sB",
                (double) bytes / (1L << (numberOfLeadingZeros * 10)),
                numberOfLeadingZeros < 1 ? "" : labels[numberOfLeadingZeros]
        );
    }


    protected void setProgramDescription() {

        TextView tv = (TextView) findViewById(R.id.description_text);

        String desc_str = new String();
        if (recordedTV.VideoInfo.ShortDesc != null && !recordedTV.VideoInfo.ShortDesc.equals(""))
            desc_str += recordedTV.VideoInfo.ShortDesc;

        String ext_str = new String();
        ext_str += "\n";
        if (recordedTV.VideoInfo.Actors != null && !recordedTV.VideoInfo.Actors.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_ACTORS_HEADER, recordedTV.VideoInfo.Actors);
            ext_str += "\n";
        }
        if (recordedTV.VideoInfo.Guests != null && !recordedTV.VideoInfo.Guests.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_GUESTS_HEADER, recordedTV.VideoInfo.Guests);
            ext_str += "\n";
        }
        if (recordedTV.VideoInfo.Directors != null && !recordedTV.VideoInfo.Directors.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_DIRECTORS_HEADER, recordedTV.VideoInfo.Directors);
            ext_str += "\n";
        }
        if (recordedTV.VideoInfo.Producers != null && !recordedTV.VideoInfo.Producers.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_PRODUCERS_HEADER, recordedTV.VideoInfo.Producers);
            ext_str += "\n";
        }
        if (recordedTV.VideoInfo.Writers != null && !recordedTV.VideoInfo.Writers.isEmpty()) {
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_WRITERS_HEADER, recordedTV.VideoInfo.Writers);
            ext_str += "\n";
        }
        if (recordedTV.VideoInfo.Year > 0) {
            String local = String.format(Locale.getDefault(), "%d", recordedTV.VideoInfo.Year);
            ext_str = AddLineToString(ext_str, LocaleStrings.IDS_YEAR_HEADER, local);
        }

        final String desc_str_f = desc_str.toString().replace("\n", "<br />");
        final String ext_str_f = ext_str.toString().replace("\n", "<br />");

        ImageView iv = (ImageView) findViewById(R.id.description_image);
        if (load_bitmap) {
            iv.setVisibility(View.VISIBLE);
            Display display = getWindowManager().getDefaultDisplay();
            Point pt = new Point();
            display.getSize(pt);
            iv.measure(pt.x, pt.y);

            if (iv.getMeasuredWidth() == 0 || iv.getMeasuredHeight() == 0) {
                Spanned s = Html.fromHtml(desc_str_f + ext_str_f);
                tv.setText(s);
            } else {
                float margin = getResources().getDimension(R.dimen.program_activity_thumb_margin);
                final int wd = (int) (iv.getMeasuredWidth() + margin);
                float ht = iv.getMeasuredHeight() + margin;

                float text_spacing = tv.getPaint().getFontSpacing();
                final int lines = (int) (ht / text_spacing);

                final RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tv.getLayoutParams();
                params.setMargins(wd, 0, 0, 0);
                tv.setText(Html.fromHtml(desc_str_f + ext_str_f));
                tv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {

                    @SuppressWarnings("deprecation")
                    @Override
                    public void onGlobalLayout() {

                        TextView tv = (TextView) findViewById(R.id.description_text);
                        int linesCount = tv.getLayout().getLineCount();

                        // restore the margin
                        params.setMargins(0, 0, 0, 0);
                        SpannableString spanS = new SpannableString(Html.fromHtml(desc_str_f + ext_str_f));

                        if (linesCount <= lines) {
                            spanS.setSpan(new InfoLeadingMarginSpan2(lines, wd), 0, spanS.length(), 0);
                            tv.setText(spanS);
                        } else {
                            // find the breakpoint where to break the String.
                            int breakpoint = tv.getLayout().getLineEnd(lines - 1);

                            Spannable s1 = new SpannableStringBuilder(spanS, 0, breakpoint);
                            s1.setSpan(new InfoLeadingMarginSpan2(lines, wd), 0, s1.length(), 0);
                            Spannable s2 = new SpannableStringBuilder(System.getProperty("line.separator"));
                            Spannable s3 = new SpannableStringBuilder(spanS, breakpoint, spanS.length());
                            // It is needed to set a zero-margin span on for the text under the image to prevent the space on the right!
                            s3.setSpan(new InfoLeadingMarginSpan2(0, 0), 0, s3.length(), 0);
                            tv.setText(TextUtils.concat(s1, s2, s3));

                            // Align the text with the image by removing the rule that the text is to the right of the image
                            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) tv.getLayoutParams();
                            int[] rules = params.getRules();
                            rules[RelativeLayout.RIGHT_OF] = 0;

                        }

                        // remove the GlobalLayoutListener
                        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            tv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                        } else {
                            tv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }

                    }
                });
            }
        }
        else {
            iv.setVisibility(View.GONE);
            Spanned s = Html.fromHtml(desc_str_f + ext_str_f);
            tv.setText(s);
        }
    }

    @Override
    public void finish() {
        super.finish();
    }

    public void onFocusChange(View v, boolean hasFocus) {
        int i = v.getId();
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (event.getAction() != KeyEvent.ACTION_DOWN) {
            return true;
        }
        if (Common.backPressed(keyCode, event)) {
            finishWithResult();
            return false;
        }
        return false;
    }

    protected void processTargetsResponse(SendToTargetsRespond resp) {

        ActionOptionsPaneItems options = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON);

        if (resp != null) {
            if (resp.size() > 0) {
                for (int i = 0; i < resp.size(); i++) {
                    options.add_item(resp.get(i).name, resp.get(i).id);
                }
                choice_menu_ = new ActionOptionsPane(TVRecordActivity.this, TVRecordActivity.this, options, 0, null);
                choice_menu_.show();
            } else {
                Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_SENDTO_NO_TARGETS), Toast.LENGTH_LONG).show();
            }
        }
    }

    public void onActionPaneOption(int option_id, String selected_option) {
        choice_menu_.hide();
        String target_id = selected_option;
        provider.AddSendToItem(recordedTV.ObjectID, target_id);
    }

    protected void processSendToObjectCompleted() {
        Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_SENDTO_BKG_SUBMITTED), Toast.LENGTH_LONG).show();
    }

    protected class ResumeRunnable implements Runnable {
        String vi_obj_id_;
        int resume_pos_;

        ResumeRunnable(String vi_obj_id, int resume_pos) {
            vi_obj_id_ = vi_obj_id;
            resume_pos_ = resume_pos;
        }

        public void run() {
            provider.requestSetResumeInfo(vi_obj_id_, resume_pos_);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (data != null) {
            int resume_position = data.getIntExtra("resume_position", 0);
            getWindow().getDecorView().post(new ResumeRunnable(recordedTV.ObjectID, resume_position));
        }

    }

    protected void startChromcastPlayback(int resume_pos) {
        ServerSettings ss = settings_.getServerSettings();
        if (ss.transcodingSupported) {
            RemoteMediaClient remoteMediaClient = mCastSession.getRemoteMediaClient();
            if (remoteMediaClient != null) {

                MediaMetadata mediaMetadata = new MediaMetadata(MediaMetadata.MEDIA_TYPE_MOVIE);
                mediaMetadata.putString(MediaMetadata.KEY_TITLE, recordedTV.VideoInfo.Title);
                if (recordedTV.VideoInfo.Subname != null && !recordedTV.VideoInfo.Subname.isEmpty())
                    mediaMetadata.putString(MediaMetadata.KEY_SUBTITLE, recordedTV.VideoInfo.Subname);
                if (recordedTV.VideoInfo.Image != null && !recordedTV.VideoInfo.Image.isEmpty())
                    mediaMetadata.addImage(new WebImage(Uri.parse(recordedTV.VideoInfo.Image)));
                if (recordedTV.VideoInfo.SeasonNum > 0)
                    mediaMetadata.putInt(MediaMetadata.KEY_SEASON_NUMBER, recordedTV.VideoInfo.SeasonNum);
                if (recordedTV.VideoInfo.EpisodeNum > 0)
                    mediaMetadata.putInt(MediaMetadata.KEY_EPISODE_NUMBER, recordedTV.VideoInfo.EpisodeNum);

                long chromecast_bitrate = settings_.getValue(Settings.CHROMECAST_BITRATE_KBS_KEY, Settings.CHROMECAST_BITRATE_KBS_DEFAULT);

                int pos = recordedTV.Url.indexOf('?');
                if (pos != -1) {
                    String url = new StringBuilder(recordedTV.Url).insert(pos, ".m3u8").toString();

                    url += "&transcoder=hls&client_id=" + settings_.getClientID() + "&bitrate=" + chromecast_bitrate;

                    MediaInfo mi = new MediaInfo.Builder(url)
                            .setContentType("video/mpeg")
                            .setStreamType(MediaInfo.STREAM_TYPE_BUFFERED)
                            .setMetadata(mediaMetadata).build();

                    remoteMediaClient.addListener(new TVMRemoteMediaClientListener(mCastSession));

                    remoteMediaClient.load(mi, true, (long) resume_pos * 1000);
                    //exit this activity to display recordings list and chromecast mediacontrol widgit
                    finishWithResult();
                }
            }
        } else {
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_CHROMCAST_NEEDS_TRANSCODING), Toast.LENGTH_LONG).show();
        }
    }


}
