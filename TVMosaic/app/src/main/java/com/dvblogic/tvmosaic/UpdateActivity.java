package com.dvblogic.tvmosaic;

import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.Html;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import TVMosaic.TVMosaic.R;
import android.widget.Toast;
import com.dvblogic.dvblink_common.*;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

public class UpdateActivity extends BaseActivity {

	private static final long REQUEST_UPDATE_STATUS_DELAY = 1000;
	private static final String TAG = "UpdateActivity";

	private class RequestUpdateStatusTimerTask extends TimerTask {
		@Override
		public void run() {
			provider.requestUpdateStatus();
		}
	}

	private class UpdateSettingsProvider implements IDataProvider {
		private static final String TAG = "UpdateSettingsProvider";

		private static final int STATE_NOT_READY = -1;
		private static final int STATE_READY = 1000;
		private static final int STATE_CHECK_UPDATE = 1001;
		private static final int STATE_GET_UPDATE_STATUS = 1002;
		private static final int STATE_START_UPDATE = 1003;

		private int state = STATE_NOT_READY;

		private BaseActivity activity;
		private Object serverResponse;
		private ScanChannelsDataProvider serverDataProvider_;
		private StatusCode httpError = StatusCode.STATUS_OK;

		public UpdateSettingsProvider(BaseActivity activity, ServerSettings serverSettings, String language) {
			this.activity = activity;
			try {
				serverDataProvider_ = new ScanChannelsDataProvider(serverSettings, this);
				serverDataProvider_.setLanguage(language);
				state = STATE_READY;
			} catch (Exception e) {
				serverDataProvider_ = null;
				Log.e(TAG, "Init", e);
			}
		}

		public void ErrorProcessing(String command, StatusCode error) {
			httpError = error;
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					state = STATE_READY;
					activity.activateProgressBar(false);
					if (activity.language_manager_ != null) {
						String msg = String.format("%s", activity.language_manager_.ErrorToString(httpError));
						Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
					}
				}
			});
		}

		public void DataProcessing(String command, Object result_data) {
			serverResponse = result_data;
			activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					if (state == STATE_CHECK_UPDATE) {
						processCheckUpdate();
					} else if (state == STATE_GET_UPDATE_STATUS) {
						processUpdateStatus();
					} else if (state == STATE_START_UPDATE) {
						processStartUpdate();
					} else {
						throw new RuntimeException("unknown state");
					}
					serverResponse = null;
				}
			});
		}

		protected void requestCheckUpdate() {
			if (serverDataProvider_ == null || state != STATE_READY) {
				return;
			}
			activity.activateProgressBar(true);
			state = STATE_CHECK_UPDATE;
			serverDataProvider_.CheckUpdate();
		}

		private void processCheckUpdate() {
			try {
				state = STATE_READY;
				activity.activateProgressBar(false);
				onCheckUpdate();
			} catch (Exception exception) {
				Log.e(TAG, "processCheckUpdate", exception);
			}
		}

		protected void requestUpdateStatus() {
			if (serverDataProvider_ == null || state != STATE_READY) {
				return;
			}
			activity.activateProgressBar(true);
			state = STATE_GET_UPDATE_STATUS;
			serverDataProvider_.GetUpdateStatus();
		}

		private void processUpdateStatus() {
			try {
				state = STATE_READY;
				onUpdateStatus(	serverResponse instanceof UpdaterGetStatusResponse ?
						(UpdaterGetStatusResponse) serverResponse : null);
			} catch (Exception exception) {
				Log.e(TAG, "processUpdateStatus", exception);
			}
		}

		protected void requestStartUpdate() {
			if (serverDataProvider_ == null || state != STATE_READY) {
				return;
			}
			activity.activateProgressBar(true);
			state = STATE_START_UPDATE;
			serverDataProvider_.StartUpdate();
		}

		private void processStartUpdate() {
			try {
				state = STATE_READY;
				activity.activateProgressBar(false);
				onStartUpdate();
			} catch (Exception exception) {
				Log.e(TAG, "processStartUpdate", exception);
			}
		}

		protected void onCheckUpdate() {
			this.requestUpdateStatus();
		}

		protected void onUpdateStatus(UpdaterGetStatusResponse statusResponse) {
			UpdaterGetStatusResponse.Status status = statusResponse.getStatus();
			if (status == UpdaterGetStatusResponse.Status.UP_TO_DATE) {
				destroyRequestUpdateStatusTimer();
				String extended = getComponentsDescription(statusResponse);
				setUpdateComment(commentUpToDate, extended);
				activity.activateProgressBar(false);
				updateLink.setVisibility(View.GONE);
				isFirstInProgress = true;
				return;
			}
			if (status == UpdaterGetStatusResponse.Status.UPDATE_IN_PROGRESS) {
				if (isFirstInProgress) {
					requestUpdateStatusTimer = new Timer();
					setUpdateComment(commentUpdateInProgress);
					activity.activateProgressBar(true);
					updateLink.setVisibility(View.GONE);
					isFirstInProgress = false;
				}
				scheduleRequestUpdateStatusTimer();
				return;
			}
			if (status == UpdaterGetStatusResponse.Status.NEEDS_UPDATE) {
				destroyRequestUpdateStatusTimer();
				String extended = getComponentsDescription(statusResponse);
				setUpdateComment(commentNeedsUpdate, extended);
				activity.activateProgressBar(false);
				updateLink.setVisibility(View.VISIBLE);
				isFirstInProgress = true;
			}
		}

		protected void onStartUpdate() {
			provider.requestUpdateStatus();
		}
	}

	private Timer requestUpdateStatusTimer;

	private Pair<String, String> commentCheckUpdates;
	private Pair<String, String> commentUpToDate;
	private Pair<String, String> commentUpdateInProgress;
	private Pair<String, String> commentNeedsUpdate;

	private boolean isFirstInProgress = true;

	private UpdateSettingsProvider provider;

	private TextView updateCommentTitle;
	private TextView updateCommentDescription;
	private RelativeLayout updateLink;

	public UpdateActivity() {
		show_exit_dialog = false;
	}

	private String getComponentsDescription(UpdaterGetStatusResponse statusResponse) {
		if (statusResponse.components.size() < 1) {
			return "";
		}
		String extended = "";
		for (UpdaterGetStatusResponse.Component component: statusResponse.components) {
            extended += component.name.substring(0, 1).toUpperCase() + component.name.substring(1);
			if (component.local.equalsIgnoreCase(component.remote)) {
				if (!component.local.equalsIgnoreCase(UpdaterGetStatusResponse.Component.UNKNOWN)) {
					extended += ": " + component.local;
				}
			} else {
				extended += ": ";
				extended += component.local.equalsIgnoreCase(UpdaterGetStatusResponse.Component.UNKNOWN) ?
						"?" : component.local;
				extended += "/";
				extended += component.remote.equalsIgnoreCase(UpdaterGetStatusResponse.Component.UNKNOWN) ?
						"?" : component.remote;
			}
            extended += ", ";
        }
		return extended.substring(0, extended.lastIndexOf(", "));
	}

	private void scheduleRequestUpdateStatusTimer() {
		requestUpdateStatusTimer.schedule(
				new RequestUpdateStatusTimerTask(), REQUEST_UPDATE_STATUS_DELAY
		);
	}

	@Override
	protected void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.update_activity);

		commentCheckUpdates = new Pair<>(
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_CHECK),
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_CHECK_DESC)
		);
		commentUpToDate = new Pair<>(
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_ACTUAL),
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_ACTUAL_DESC)
		);
		commentUpdateInProgress = new Pair<>(
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_IN_PROGRESS),
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_IN_PROGRESS_DESC)
		);
		commentNeedsUpdate = new Pair<>(
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_NEEDS_UPDATE),
				language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_NEEDS_UPDATE_DESC)
		);

		TextView tv = (TextView)findViewById(R.id.head_bar);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_PAGE_TITLE));

		RelativeLayout rl;

		rl = (RelativeLayout) findViewById(R.id.update_category);
		tv = (TextView)rl.findViewById(R.id.category_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_LINK));
		tv = (TextView)rl.findViewById(R.id.category_desc);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_DESC));

		rl = (RelativeLayout) findViewById(R.id.update_description);
		updateCommentTitle = (TextView) rl.findViewById(R.id.title_text);
		updateCommentDescription = (TextView) rl.findViewById(R.id.desc_text);

		updateLink = (RelativeLayout) findViewById(R.id.update_link);
		tv = (TextView) updateLink.findViewById(R.id.title_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_UPDATE));
		tv = (TextView) updateLink.findViewById(R.id.desc_text);
		tv.setText(language_manager_.GetString(LocaleStrings.IDS_SETTINGS_UPDATE_UPDATE_DESC));
		updateLink.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				provider.requestStartUpdate();
			}
		});

		setUpdateComment(commentCheckUpdates);
		provider = new UpdateSettingsProvider(this, serverSettings, language_manager_.getID());

		getWindow().getDecorView().post(new Runnable() {
			public void run() {
				provider.requestCheckUpdate();
			}
		});

	}

	@Override
	public void onDestroy() {
		try {
			destroyRequestUpdateStatusTimer();
		} catch (Exception e) {
			Log.e(TAG, "onDestroy", e);
		}
		super.onDestroy();
	}

	private void destroyRequestUpdateStatusTimer() {
		if (requestUpdateStatusTimer != null) {
			requestUpdateStatusTimer.cancel();
			requestUpdateStatusTimer = null;
		}
	}

	private void setUpdateComment(Pair<String, String> comment) {
		setUpdateComment(comment, "");
	}

	private void setUpdateComment(Pair<String, String> comment, String extended) {
		updateCommentTitle.setText(comment.first);
		updateCommentDescription.setText(
				String.format(Locale.getDefault(), extended.isEmpty() ? "%s" : "%s (%s)", comment.second, extended)
		);
	}
}
