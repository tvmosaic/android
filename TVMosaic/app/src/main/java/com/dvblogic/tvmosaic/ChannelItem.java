package com.dvblogic.tvmosaic;

import TVMosaic.TVMosaic.R;
import android.content.Context;
import android.graphics.*;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.dvblogic.dvblink_common.Channel;
import com.dvblogic.dvblink_common.Utils;

import java.io.InputStream;

public class ChannelItem extends RelativeLayout implements OnBitmapLoadFinished
{
	public Channel channel_;
	Paint	paint_ = null;
	Context context;
	Rect draw_rect = new Rect();

	public ChannelItem(Context context) {
		super(context);
		this.context = context;
	}

	public ChannelItem(Context context, AttributeSet attrs) {
		super(context, attrs);
		this.context = context;
    }

	public ChannelItem(Context context, AttributeSet attrs, int defStyle) {
		super(context, attrs, defStyle);
		this.context = context;
	}

	public void setChannel(Channel ch, boolean show_num) {
		channel_ = ch;
		TextView tv = (TextView) findViewById(R.id.name);
		tv.setText(ch.Name);
		if (show_num) {
            String chnum_str = Utils.formatChannelNumber(ch);
            if (!chnum_str.isEmpty()) {
                tv = (TextView) findViewById(R.id.number);
                tv.setText(chnum_str);
            }
		}
		drawLogo();
	}

	private void drawLogo() {
		if (channel_ == null || channel_.ChannelLogo == null || channel_.ChannelLogo.isEmpty()) {
			return;
		}
		ImageView logo = (ImageView) findViewById(R.id.logo);
		logo.setVisibility(VISIBLE);

		Glide.with(context)
				.load(channel_.ChannelLogo)
				.diskCacheStrategy(DiskCacheStrategy.RESULT)
				.into(logo);
	}

	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		ImageView bmImage;

		public DownloadImageTask(ImageView bmImage) {
			this.bmImage = bmImage;
		}

		protected Bitmap doInBackground(String... urls) {
			String urldisplay = urls[0];
			Bitmap mIcon11 = null;
			try {
				InputStream in = new java.net.URL(urldisplay).openStream();
				mIcon11 = BitmapFactory.decodeStream(in);
				in.close();
			} catch (Exception e) {
				Log.e("Error", e.getMessage());
				e.printStackTrace();
			}
			return mIcon11;
		}

		protected void onPostExecute(Bitmap result) {
			bmImage.setImageBitmap(result);
		}
	}




	@Override
	public void loadFinished() {}

	public boolean handleOnKey(View v, int keyCode, KeyEvent event) {
        return false;
    }

    protected void onFocusChanged(boolean gainFocus,int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction,previouslyFocusedRect);
    }

    @Override
    public void setActivated(boolean activated) {
        super.setActivated(activated);

		Typeface tp;
		float text_size;
		int text_color;
		if (activated) {
			tp = CustomFont.getInstance(getContext()).getTypeFace_Bold();
			text_color = ContextCompat.getColor(getContext(), R.color.channel_item_text_selected);
			text_size = getContext().getResources().getDimension(R.dimen.channel_item_text_size_selected);
		} else {
			tp = CustomFont.getInstance(getContext()).getTypeFace();
			text_color = ContextCompat.getColor(getContext(), R.color.channel_item_text);
            text_size = getContext().getResources().getDimension(R.dimen.channel_item_text_size);
		}

		TextView tv_name = (TextView) findViewById(R.id.name);
		TextView tv_number = (TextView) findViewById(R.id.number);

		tv_name.setTypeface(tp, Typeface.NORMAL);
		tv_name.setTextColor(text_color);
		tv_name.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);

		tv_number.setTypeface(tp, Typeface.NORMAL);
		tv_number.setTextColor(text_color);
		tv_number.setTextSize(TypedValue.COMPLEX_UNIT_PX, text_size);
	}

	@Override
	protected void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		if (paint_ == null)
			paint_ = new Paint();

		paint_.setStyle(Paint.Style.FILL_AND_STROKE);
		paint_.setAntiAlias(false);

		paint_.setColor(ContextCompat.getColor(getContext(), R.color.guide_separator));
		float sep_width = getContext().getResources().getDimension(R.dimen.separator_width);
		paint_.setStrokeWidth(sep_width);

		canvas.drawLine(0, 0, this.getWidth() - 1, 0, paint_); //top hor
		canvas.drawLine(this.getWidth() - 1, 0, this.getWidth() - 1, this.getHeight() - 1, paint_); //right ver
		canvas.drawLine(0, this.getHeight() - 1, this.getWidth() - 1, this.getHeight() - 1, paint_); //bottom hor

		if (isActivated()) {
			int top_bottom_offs = (int)getContext().getResources().getDimension(R.dimen.channel_contents_top_bottom_offs);
			int left_right_offs = (int)getContext().getResources().getDimension(R.dimen.channel_contents_left_right_offs);
			draw_rect.set(left_right_offs, top_bottom_offs, getWidth() - left_right_offs - 1, getHeight() - top_bottom_offs - 1);
			paint_.setColor(ContextCompat.getColor(getContext(), R.color.channel_item_bkg_selected));
			canvas.drawRect(draw_rect, paint_);

		}

	}

}



