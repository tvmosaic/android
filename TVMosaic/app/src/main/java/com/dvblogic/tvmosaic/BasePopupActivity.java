package com.dvblogic.tvmosaic;

import TVMosaic.TVMosaic.R;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.Gravity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import android.support.v7.app.AppCompatActivity;
import com.dvblogic.dvblink_common.AdviserDataProvider;
import com.dvblogic.dvblink_common.IDataProvider;
import com.dvblogic.dvblink_common.ServerSettings;
import com.dvblogic.dvblink_common.Settings;
import com.dvblogic.dvblink_common.StatusCode;

public class BasePopupActivity extends AppCompatActivity implements IDataProvider
{

	protected LanguageManager		language_manager_;
	protected SettingsManager		settings_;
	protected AdviserDataProvider data_provider_;
	protected Object				response_;
	protected StatusCode http_error_;
	protected ServerSettings serverSettings;
	protected int					themeId	= R.style.AppTheme;
	boolean no_network = false;
	Context _context = this;
    protected ProgressDialog progressBar = null;

	@Override
	protected void onCreate(Bundle bundle) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_PROGRESS);

		super.onCreate(bundle);

		lockOrientation();

		no_network = getIntent().hasExtra("no_network");
		
		try {
			initializeSettings();
			setTheme(themeId);

            setProgressBarIndeterminate(true);
            getSupportActionBar().hide();

        } catch (Exception e) {
			e.printStackTrace();
		}
	}

	protected void lockOrientation() {
		//fix orientation to LANDSCAPE always
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_SENSOR_LANDSCAPE);
	}

	
	public boolean ProcessBackButton() {
		return false;
	}

	public void ProcessSettingsChange() {
	}

	@Override
	public void DataProcessing(String command, Object result_data) {
	}

	@Override
	public void ErrorProcessing(String command, StatusCode error) {
	}
	
	protected void initializeSettings() {

		settings_ = new SettingsManager(this);

		language_manager_ = LanguageManager.getInstance(this);
		language_manager_.LoadLanguage(Constants.LanguageDirectory, settings_.getLanguage());
		
		//Locale.setDefault(language_manager_.GetLocale());


		serverSettings = settings_.getServerSettings();	
		if (serverSettings.serverAddress.isEmpty())
			no_network = true;
		
		data_provider_ = null;
		if (!no_network) {
			try {
				data_provider_ = new AdviserDataProvider(serverSettings, this);
			} catch (Exception e) {
				Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_SERVER_NAME_INVALID_MSG),
						Toast.LENGTH_SHORT).show();
				System.out.println(e.getMessage());
				data_provider_ = null;
			}
		}
	}

	protected void refresh() {
	}


	protected int getDropDownWidth(TextView tv, String[] str, ImageView iv) {
		Display display = getWindowManager().getDefaultDisplay();
		return Common._calcDropDownWidth(tv, str, iv, display);
	}

	protected void activateProgressBar(boolean activate) {
		//setProgressBarIndeterminateVisibility(activate);
		setProgressBarVisibility(activate);
		if (progressBar == null) {
			progressBar = new ProgressDialog(_context);
			try {
				progressBar.show();
			} catch (WindowManager.BadTokenException e) {

			}
			progressBar.setCancelable(true);
			progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
			progressBar.setIndeterminate(true);
			progressBar.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
			progressBar.setContentView(R.layout.progress_dialog);
		}
		if (activate) {
			progressBar.show();
		} else {
			progressBar.hide();
		}
	}
}
