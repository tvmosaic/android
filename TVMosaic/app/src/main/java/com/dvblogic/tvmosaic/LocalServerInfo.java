package com.dvblogic.tvmosaic;

import android.content.Context;

import com.dvblogic.dvblink_common.DataProvider;
import com.dvblogic.dvblink_common.IDataProvider;
import com.dvblogic.dvblink_common.ServerSettings;
import com.dvblogic.dvblink_common.ServerVersionRequest;
import com.dvblogic.dvblink_common.StatusCode;
import com.dvblogic.dvblink_common.SynchronousDataProviderCallback;

public class LocalServerInfo {

    public boolean isRunning(Context context) {
        boolean ret_val = false;

        ServerSettings ss = new ServerSettings();
        ss.serverAddress = this.getAddress();
        ss.serverPort = this.getPort();
        try {
            SynchronousDataProviderCallback callback = new SynchronousDataProviderCallback();
            DataProvider data_provider = new DataProvider(ss, callback);
            data_provider.setSynchronousMode(true);
            data_provider.GetVersion(new ServerVersionRequest());
            ret_val = callback.result_ != null && callback.result_code_ == StatusCode.STATUS_OK;
        } catch (Exception e) {
            ret_val = false;
        }

        return ret_val;//Utils.usesRemoteControl(context);
    }

    public String getName() {
        return "TVMosaic";
    }

    public String getID() {
        return "93f2327d-0095-4d9a-a864-81b353b72396";
    }

    public String getAddress() {
        return "127.0.0.1";
    }

    public int getPort() {
        return 9270;
    }


}
