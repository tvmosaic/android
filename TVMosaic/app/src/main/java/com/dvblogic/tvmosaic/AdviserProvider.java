package com.dvblogic.tvmosaic;

import android.util.Log;
import android.widget.Toast;
import com.dvblogic.dvblink_common.*;

class AdviserProvider implements IDataProvider {

	public static final int PROFILE_NAME_MAX_LENGTH = 30;

	private static final String TAG = "AdviserProvider";

	private static int STATE_READY = 1000;
	private static int STATE_CREATE_PROFILE = 1001;
	private static int STATE_RENAME_PROFILE = 1002;
	private static int STATE_REMOVE_PROFILE = 1003;
	private static int STATE_REQUESTING_PROFILES = 1004;
	private static int STATE_RECOMMED_BY_PROFILE = 1005;
	private static int STATE_REQUEST_MAPPED_CHANNELS = 1006;
	private static int STATE_RATE_PROGRAM = 1007;
	private static int STATE_LIKE_THIS = 1008;

	private int state = STATE_READY;

	private BaseActivity activity;
	protected Object serverResponse;
	protected ScanChannelsDataProvider serverDataProvider_;
	private StatusCode httpError = StatusCode.STATUS_OK;

	public AdviserProvider(BaseActivity activity, ServerSettings serverSettings, String language) {
		this.activity = activity;
		try {
			serverDataProvider_ = new ScanChannelsDataProvider(serverSettings, this);
			serverDataProvider_.setLanguage(language);
			state = STATE_READY;
		} catch (Exception e) {
			serverDataProvider_ = null;
		}
	}

	public void ErrorProcessing(String command, StatusCode error) {
		httpError = error;
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				activity.activateProgressBar(false);
				//do not show error toast if we have requested mapped channels. It is ok - error means that no adviser is installed
				if (state != STATE_REQUEST_MAPPED_CHANNELS) {
					if (activity.language_manager_ != null) {
						String msg = String.format("%s", activity.language_manager_.ErrorToString(httpError));
						Toast.makeText(activity, msg, Toast.LENGTH_SHORT).show();
					}
				}
				state = STATE_READY;
			}
		});
	}

	public void DataProcessing(String command, Object result_data) {
		serverResponse = result_data;
		activity.runOnUiThread(new Runnable() {
			@Override
			public void run() {
				if (state == STATE_REQUESTING_PROFILES) {
					processProfiles();
				} else if (state == STATE_REQUEST_MAPPED_CHANNELS) {
					processMappedChannels();
				} else if (state == STATE_REMOVE_PROFILE) {
					processRemoveProfile();
				} else if (state == STATE_RENAME_PROFILE) {
					processRenameProfile();
				} else if (state == STATE_RECOMMED_BY_PROFILE) {
					processRecommendByProfiles();
				} else if (state == STATE_RATE_PROGRAM) {
					processRateProgram();
				} else if (state == STATE_CREATE_PROFILE) {
					processCreateProfile();
				} else if (state == STATE_LIKE_THIS) {
					processLikeThis();
				} else {
					throw new RuntimeException("unknown state");
				}
				serverResponse = null;
			}
		});
	}

	public void requestMappedChannels() {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		state = STATE_REQUEST_MAPPED_CHANNELS;
		MappedChannelsRequest request = new MappedChannelsRequest();
		activity.activateProgressBar(true);
		serverDataProvider_.GetMappedChannels(request);
	}

	private void processMappedChannels() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if (serverResponse != null && serverResponse instanceof MappedChannelsResponse) {
				MappedChannelsResponse mappedChannels = (MappedChannelsResponse) serverResponse;
				onMappedChannels(mappedChannels);
			}
		} catch (Exception exception) {
			Log.e(TAG, "processMappedChannels", exception);
		}
	}

	protected void requestProfiles() {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}

		state = STATE_REQUESTING_PROFILES;
		activity.activateProgressBar(true);
		ProfilesRequest request = new ProfilesRequest();
		serverDataProvider_.GetProfiles(request);
	}

	protected void processProfiles() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			onProfiles(
					serverResponse instanceof ProfilesResponse ?
							(ProfilesResponse) serverResponse : null
			);
		} catch (Exception exception) {
			Log.e(TAG, "processProfiles", exception);
		}
	}

	protected void requestRemoveProfile(String profileID) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		state = STATE_REMOVE_PROFILE;
		activity.activateProgressBar(true);
		ProfileDelete profileDelete = new ProfileDelete();
		profileDelete.ProfileId = profileID;
		serverDataProvider_.RemoveProfile(profileDelete);
	}

	protected void processRemoveProfile() {
		state = STATE_READY;
		activity.activateProgressBar(false);
		onRemoveProfile();
	}

	public void requestRenameProfile(String profileID, String profileName) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		ProfileRename pr = new ProfileRename();
		pr.ProfileId = profileID;
		pr.ProfileName = profileName;
		state = STATE_RENAME_PROFILE;
		activity.activateProgressBar(true);
		serverDataProvider_.RenameProfile(pr);
	}

	protected void processRenameProfile() {
		state = STATE_READY;
		activity.activateProgressBar(false);
		onRenameProfile();
	}

	public void requestRecommendByProfile(String profileID, long start, long end) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		RecommendByProfiles request = new RecommendByProfiles();
		request.StartTime = start;
		request.EndTime = end;
		request.ExtendedEpg = false;
		request.Profiles = new ProfileIDList();
		request.Profiles.add(profileID);

		state = STATE_RECOMMED_BY_PROFILE;
		activity.activateProgressBar(true);
		serverDataProvider_.Recommend(request);
	}

	protected void processRecommendByProfiles() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if ((serverResponse != null) && (serverResponse instanceof RecommendationsList)) {
				onRecommendByProfiles((RecommendationsList) serverResponse);
			}
		} catch (Exception exception) {
			Log.e(TAG, "processRecommendByProfiles", exception);
		}
	}

	public void requestRateProgram(RateProgram request) {
		if (serverDataProvider_ == null || state != STATE_READY) {
			return;
		}
		state = STATE_RATE_PROGRAM;
		activity.activateProgressBar(true);
		serverDataProvider_.RateProgram(request);
	}

	protected void processRateProgram() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if (serverResponse != null) {
				onRateProgram();
			}
		} catch (Exception exception) {
			Log.e(TAG, "processRateProgram", exception);
		}
	}

	public void requestCreateProfile(ProfileCreator request) {
		if (serverDataProvider_ == null && state != STATE_READY) {
			return;
		}
		state = STATE_CREATE_PROFILE;
		activity.activateProgressBar(true);
		request.Name = request.Name.substring(0, Math.min(request.Name.length(), PROFILE_NAME_MAX_LENGTH)).trim();
		serverDataProvider_.CreateProfile(request);
	}

	protected void processCreateProfile() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if (serverResponse != null && serverResponse instanceof ProfileId) {
				onCreateProfile((ProfileId) serverResponse);
			}
		} catch (Exception exception) {
			Log.e(TAG, "processCreateProfile", exception);
		}
	}

	public void requestLikeThis(RecommendLikeThis request) {
		if (serverDataProvider_ == null && state != STATE_READY) {
			return;
		}
		state = STATE_LIKE_THIS;
		activity.activateProgressBar(true);
		serverDataProvider_.Recommend(request);
	}

	protected void processLikeThis() {
		try {
			state = STATE_READY;
			activity.activateProgressBar(false);
			if (serverResponse != null && serverResponse instanceof RecommendationsList) {
				onLikeThis((RecommendationsList) serverResponse);
			}
		} catch (Exception exception) {
			Log.e(TAG, "processLikeThis", exception);
		}
	}

	protected void onMappedChannels(MappedChannelsResponse mappedChannels) {}
	protected void onProfiles(ProfilesResponse profiles) {}
	protected void onRemoveProfile() {}
	protected void onRenameProfile() {}
	protected void onRecommendByProfiles(RecommendationsList recommendations) {}
	protected void onRateProgram() {}
	protected void onCreateProfile(ProfileId profile) {}
	protected void onLikeThis(RecommendationsList recommendations) {}

}