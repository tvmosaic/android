package com.dvblogic.tvmosaic;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.Html;
import android.view.ContextThemeWrapper;
import android.view.Display;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import android.support.v7.app.AppCompatActivity;
import com.dvblogic.dvblink_common.IDataProvider;
import com.dvblogic.dvblink_common.ScanChannelsDataProvider;
import com.dvblogic.dvblink_common.ServerSettings;
import com.dvblogic.dvblink_common.Settings;
import com.dvblogic.dvblink_common.StatusCode;
import com.dvblogic.dvblink_common.Utils;

import TVMosaic.TVMosaic.R;

public class BaseActivity extends AppCompatActivity implements IDataProvider
{

	protected LanguageManager		language_manager_;
	protected SettingsManager		settings_;
	protected ScanChannelsDataProvider data_provider_;
	protected Object				response_;
	protected StatusCode http_error_;
	protected ServerSettings serverSettings;
	protected int					themeId	= R.style.AppTheme;
	boolean no_network = false;
	protected boolean show_exit_dialog = true;
	protected boolean narrow_mode = false;
	protected boolean hide_progress_bar = false;

	protected int active_zone = -1;
	protected int active_subzone = -1;
	Context _context;
	protected ProgressDialog progressBar = null;
	
	@Override
	protected void onCreate(Bundle bundle) {
		
		_context = this;

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        requestWindowFeature(Window.FEATURE_PROGRESS);

		super.onCreate(bundle);

		/*
		String narrow = getResources().getString(R.string.narrow_layout);
		narrow_mode = narrow != null && narrow.equalsIgnoreCase("true");
		
		String progress = getResources().getString(R.string.hide_progress_bar);
		hide_progress_bar = progress != null && progress.equalsIgnoreCase("true");
		*/
		no_network = getIntent().hasExtra("no_network");
		
		try {
			initializeSettings();
			setTheme(themeId);

			getSupportActionBar().hide();

			setProgressBarIndeterminate(true);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

    @Override
    protected void onDestroy() {
        if (progressBar != null)
            progressBar.dismiss();

        super.onDestroy();
    }

    protected void activateProgressBar_int(boolean fullScreen) {
        setProgressBarVisibility(true);
        if (progressBar == null) {
            progressBar = new ProgressDialog(_context);
            try {
                progressBar.show();
            } catch (WindowManager.BadTokenException e) {

            }
            progressBar.setCancelable(false);
            progressBar.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progressBar.setIndeterminate(true);
            progressBar.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

            progressBar.setContentView(R.layout.progress_dialog);

            if (fullScreen && !Utils.usesRemoteControl(this)) {
                progressBar.getWindow().getDecorView().setSystemUiVisibility(
                        View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                                | View.SYSTEM_UI_FLAG_FULLSCREEN
                                | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
            }
        }
        progressBar.show();
    }

    protected void deActivateProgressBar_int() {
        setProgressBarVisibility(false);

        if (progressBar != null) {
            progressBar.hide();
        }
    }

    protected void dismissProgressBar_int() {
        setProgressBarVisibility(false);

        if (progressBar != null) {
            progressBar.dismiss();
            progressBar = null;
        }
    }

	protected void activateProgressBar(boolean activate) {
        if (activate)
            activateProgressBar_int(false);
        else
            deActivateProgressBar_int();
	}

    protected void activateProgressBarFullScreen(boolean activate) {
        if (activate)
            activateProgressBar_int(true);
        else
            deActivateProgressBar_int();
    }

    protected void dismissProgressBar() {
        dismissProgressBar_int();
    }

	@Override
	public void onBackPressed() {
		if (show_exit_dialog) {
			//int theme = themeId;//android.R.style.Theme_Dialog;
			int theme = (themeId == R.style.AppTheme) 
					? android.R.style.Theme_Holo_Dialog_NoActionBar 
					: android.R.style.Theme_Holo_Light_Dialog_NoActionBar;
			AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(new ContextThemeWrapper(this, theme));
			alertDialogBuilder.setTitle(language_manager_.GetString(LocaleStrings.IDS_DLG_TITLE_ATTENTION));
			alertDialogBuilder.setMessage(language_manager_.GetString(LocaleStrings.IDS_EXIT_DVBLINK_CONFIRM_DLG));
			alertDialogBuilder.setPositiveButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_YES),
				new DialogInterface.OnClickListener() {
				
				@Override
				public void onClick(DialogInterface arg0, int arg1) {
					finish();
				}
			});
			alertDialogBuilder.setNegativeButton(language_manager_.GetString(LocaleStrings.IDS_DLG_BUTTON_NO),
				new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface arg0, int arg1) {

				}
			});

			AlertDialog alertDialog = alertDialogBuilder.create();
			alertDialog.show();
		} else {
			finish();
		}
		//moveTaskToBack(true);	
	}
	
	public boolean ProcessBackButton() {
		//initializeSettings();
		return false;
	}

	public void ProcessSettingsChange() {
		initializeSettings();
	}

	@Override
	public void DataProcessing(String command, Object result_data) {
	}

	@Override
	public void ErrorProcessing(String command, StatusCode error) {
	}

	private void applyDebugConnection() {
		// TODO DEBUG
		//serverSettings.serverAddress = "10.0.0.48";
		//serverSettings.serverAddress = "10.0.0.7";

		// serverSettings.serverAddress = "192.168.1.147";
		// serverSettings.serverPort = 8002;

		// serverSettings.serverAddress = "dvblogic-dev.dyndns.org";
		// serverSettings.serverPort = 9200;

//		serverSettings.serverAddress = "192.168.1.5";
//		serverSettings.serverPort = 8100;
	}

	protected void initializeSettings() {

		settings_ = new SettingsManager(this);
		themeId = R.style.AppTheme_Ligth;

		language_manager_ = LanguageManager.getInstance(this);
		language_manager_.LoadLanguage(Constants.LanguageDirectory, settings_.getLanguage());

		serverSettings = settings_.getServerSettings();

		applyDebugConnection();

		if (serverSettings.serverAddress.isEmpty()) {
			no_network = true;
		} else {
            no_network = false;
        }

		data_provider_ = null;
		if (!no_network) {
			try {
				data_provider_ = new ScanChannelsDataProvider(serverSettings, this);
				data_provider_.setLanguage(language_manager_.getID());
			} catch (Exception e) {
				Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_SERVER_NAME_INVALID_MSG),
						Toast.LENGTH_SHORT).show();
				System.out.println(e.getMessage());
				data_provider_ = null;
			}
		}
	}

	public void reloadLanguage() {
        language_manager_.LoadLanguage(Constants.LanguageDirectory, settings_.getLanguage());
        if (data_provider_ != null)
            data_provider_.setLanguage(language_manager_.getID());
	}

	protected void refresh() {
	}

	protected int getDropDownWidth(TextView tv, String[] str, ImageView iv) {
		Display display = getWindowManager().getDefaultDisplay();
		return Common._calcDropDownWidth(tv, str, iv, display);
	}
	
	public boolean darkTheme() {
		return themeId == R.style.AppTheme; 
	}

	void addEmptyListMessageView(ListView parentView, String message) {
		ViewGroup vg = (ViewGroup) parentView.getParent();
		LayoutInflater li = getLayoutInflater();
		View v = li.inflate(R.layout.first_setup, vg, false);

		TextView tv = (TextView) v.findViewById(R.id.setup_text);
		tv.setText(Html.fromHtml(message));
		vg.addView(v);
		parentView.setEmptyView(v);
	}

}
