package com.dvblogic.tvmosaic;

import android.content.Context;
import android.os.Build;
import android.provider.Settings;

import java.security.Key;
import org.apache.commons.codec.binary.Hex;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class LocalStringEncrypter {
	
private static final String ALGORITHM = "AES";
//private static final byte[] keyValue =
//    new byte[] { 'T', 'h', 'i', 's', 'I', 's', 'A', 'S', 'e', 'c', 'r', 'e', 't', 'K', 'e', 'y' };

 public static String encrypt(Context ctx, String valueToEnc) {
     try {
         Key key = generateKey(getLocalID(ctx));
         Cipher c = Cipher.getInstance(ALGORITHM);
         c.init(Cipher.ENCRYPT_MODE, key);
         byte[] encValue = c.doFinal(valueToEnc.getBytes());
         String encryptedValue = String.valueOf(Hex.encodeHex(encValue));
         return encryptedValue;
     } catch (Exception e) {
         e.printStackTrace();
         return valueToEnc;
     }
}

public static String decrypt(Context ctx, String encryptedValue) {
    try {
        Key key = generateKey(getLocalID(ctx));
        Cipher c = Cipher.getInstance(ALGORITHM);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decordedValue = Hex.decodeHex(encryptedValue.toCharArray());
        byte[] decValue = c.doFinal(decordedValue);
        String decryptedValue = new String(decValue);
        return decryptedValue;
    } catch (Exception e) {
        e.printStackTrace();
        return encryptedValue;
    }
}

private static Key generateKey(String seed) throws Exception {
    Key key = new SecretKeySpec(seed.getBytes(), ALGORITHM);
    return key;
}

private static String getLocalID(Context ctx) {
    String serial = Build.SERIAL;
    String android_id = Settings.Secure.getString(ctx.getContentResolver(),
            Settings.Secure.ANDROID_ID);

    return serial == null ? "serial" : serial + android_id == null ? "android_id" : android_id;
}

}