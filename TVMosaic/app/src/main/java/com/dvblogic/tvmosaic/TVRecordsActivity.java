package com.dvblogic.tvmosaic;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import TVMosaic.TVMosaic.R;

import android.os.IBinder;
import android.support.v7.app.MediaRouteButton;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.widget.*;
import com.dvblogic.dvblink_common.*;
import com.google.android.gms.cast.framework.CastButtonFactory;
import com.google.android.gms.cast.framework.CastContext;

import java.util.*;

import static com.dvblogic.dvblink_common.ObjectRequester.DLRECORDER_RECORDINGS_BY_DATE_ID;
import static com.dvblogic.tvmosaic.ActionOptionsPaneItems.empty_selected_option_id_;

public class TVRecordsActivity extends BaseActivity implements ActionPane.ActionPaneEventListener, OnBitmapLoadFinished
{
    private static final int PLAY_ACTIVITY_REQUEST_CODE = 100;
    final int timers_activity_req_code      = 101;
    final int schedules_activity_req_code      = 102;

    private static final String TAG = "TVRecordsActivity";

    private class ResultsAdapter extends BaseAdapter {
        private ArrayList<RecordedTV> searchResults = new ArrayList<>();

        private LayoutInflater ltInflater;
        private int positionLastShowedProgramInfo = -1;

        private View.OnClickListener resultItemClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showProgramInfo((int) view.getTag(), true);
            }
        };
        private View.OnFocusChangeListener resultItemFocusListener = new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean focus) {
                if (!focus) {
                    return;
                }
                showProgramInfo((int) view.getTag(), false);
            }
        };
        private View.OnKeyListener resultItemKeyListener = new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int keyCode, KeyEvent keyEvent) {
                if (keyEvent.getAction() == KeyEvent.ACTION_DOWN && Common.okPressed(keyEvent.getKeyCode())) {
                    showProgramInfo((int) view.getTag(), true);
                    return true;
                }
                return false;
            }
        };

        private void showProgramInfo(int position, boolean extend) {
            if (position == positionLastShowedProgramInfo && !extend) {
                return;
            }
            positionLastShowedProgramInfo = position;

            RecordedTV record = searchResults.get(position);

            if (programDetailsPane.getVisibility() == View.VISIBLE)
                programDetailsPane.setInfo(record.VideoInfo, record.ChannelName, 0, false, false, language_manager_);

            if (extend) {
                startProgramActivity(record);
            } else {
                setProgramImage(record);
            }
        }

        void startProgramActivity(RecordedTV record) {
            Intent intent = new Intent(TVRecordsActivity.this, TVRecordActivity.class);
            intent.putExtra("recording_id", record.ObjectID);
            startActivityForResult(intent, PLAY_ACTIVITY_REQUEST_CODE);
        }

        public ResultsAdapter() {
            ltInflater = getLayoutInflater();
        }

        @Override
        public int getCount() {
            return searchResults.size();
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view = convertView;
            if (view == null) {
                view = ltInflater.inflate(R.layout.search_ex_item, parent, false);
            }
            RecordedTV records = searchResults.get(position);

            int textColor = getResources().getColor(
                    records.State == RecordedTVState.RTVS_ERROR ?
                            R.color.new_record_item_error :
                            R.color.new_text_primary_active
            );

            if (records.State == RecordedTVState.RTVS_IN_PROGRESS)
                textColor = getResources().getColor(R.color.new_record_item_in_progress);

            TextView label;
            label = (TextView) view.findViewById(R.id.title);
            if (records.VideoInfo.Title != null) {
                String text = records.VideoInfo.Title;
                label.setText(text);
            }
            label.setTextColor(textColor);

            if (!Utils.usesRemoteControl(_context)) {
                String str = ((records.VideoInfo.Subname == null) ? "" : records.VideoInfo.Subname);
                String ES = "";
                if (records.VideoInfo.SeasonNum != 0)
                    ES += String.format("S%02d", records.VideoInfo.SeasonNum);

                if (records.VideoInfo.EpisodeNum != 0) {
                    ES += String.format("E%02d", records.VideoInfo.EpisodeNum);
                }
                if (str.isEmpty()) {
                    str += ES;
                } else {
                    if (!ES.isEmpty())
                        str += "  |  " + ES;
                }

                label = (TextView) view.findViewById(R.id.subtitle);
                label.setText(str);
                label.setTextColor(textColor);
                if (!str.isEmpty())
                    label.setVisibility(View.VISIBLE);
                else
                    label.setVisibility(View.GONE);
            }

            label = (TextView) view.findViewById(R.id.start_time);
            label.setText(formatStartTime(records));
            label.setTextColor(textColor);

            label = (TextView) view.findViewById(R.id.start_date);
            label.setText(formatStartDate(
                    records.VideoInfo.StartTime, records.VideoInfo.StartTime + records.VideoInfo.Duration
            ));
            label.setTextColor(textColor);

            label = (TextView) view.findViewById(R.id.channel);
            label.setText(records.ChannelName);
            label.setTextColor(textColor);

            label = (TextView) view.findViewById(R.id.duration);
            label.setText(formatDuration(records.VideoInfo.Duration));
            label.setTextColor(textColor);

            view.setTag(position);
            view.setOnFocusChangeListener(resultItemFocusListener);
            view.setOnClickListener(resultItemClickListener);
            view.setOnKeyListener(resultItemKeyListener);

            return view;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public Object getItem(int position) {
            return searchResults.get(position);
        }

        public void addPrograms(RecordedTV records) {
            searchResults.add(records);
        }
    }

    private class MyActionPane extends ActionPane {

        public MyActionPane(BaseActivity context, ActionPaneEventListener listener) {
            super(context, listener);
        }

        void uncheck_all_except_this(int option_id, String selected_option) {
            for (int i=0, l=items_.size(); i<l; ++i) {

                ActionOptionsPaneItems sorting_options = this.get_options(i);
                if (sorting_options == null) {
                    continue;
                }

                if (option_id == i) {
                    //make sure that option is selected
                    sorting_options.set_selected_item_id(selected_option);
                } else {
                    //reset selection
                    sorting_options.set_selected_item_id(empty_selected_option_id_);
                }
            }
        }

        protected void add_extra_pane_items() {

            //separator
//            ActionPaneItem sep_api = new ActionPaneItem();
//            items_.add(0, sep_api);

            //refresh
            ActionPaneItem refresh_api = new ActionPaneItem();
            refresh_api.text_ = LocaleStrings.IDS_REFRESH;
            refresh_api.id_ = recordings_refresh_pane_action_id_;
            refresh_api.left_icon_ = R.drawable.tvm_refresh_icon_normal;
            refresh_api.left_icon_selected_ = R.drawable.tvm_refresh_icon_hover;
            items_.add(0, refresh_api);

            ActionPaneItem timers_api = new ActionPaneItem();
            timers_api.text_ = language_manager_.GetString(LocaleStrings.IDS_TIMERS);
            timers_api.id_ = timers_pane_action_id_;
            timers_api.left_icon_ = R.drawable.tvm_timers_icon_normal;
            timers_api.left_icon_selected_ = R.drawable.tvm_timers_icon_hover;
            items_.add(0, timers_api);

            ActionPaneItem schedules_api = new ActionPaneItem();
            schedules_api.text_ = language_manager_.GetString(LocaleStrings.IDS_SCHEDULES);
            schedules_api.id_ = schedules_pane_action_id_;
            schedules_api.left_icon_ = R.drawable.tvm_schedules_icon_normal;
            schedules_api.left_icon_selected_ = R.drawable.tvm_schedules_icon_hover;
            items_.add(0, schedules_api);

            int apiId = 0;

            Collections.reverse(categoryContainer);
            for (TVRecordsProvider.CategoryContainer t: categoryContainer) {
                ActionPaneItem api = new ActionPaneItem();
                api.id_ = ++apiId;
                api.text_ = t.name;
                api.left_icon_ = R.drawable.tvm_sort_icon_normal;
                api.left_icon_selected_ = R.drawable.tvm_sort_icon_hover;
                api.right_icon_ = R.drawable.tvm_dropdown_icon_normal;
                api.right_icon_selected_ = R.drawable.tvm_dropdown_icon_hover;
                api.options_ = new ActionOptionsPaneItems(ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX);
                items_.add(0, api);

                ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode select_mode = t.type == ContainerType.CONTAINER_CATEGORY_SORT ? ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_RADIO_BUTTON : ActionOptionsPaneItems.ActionOptionsPaneItemsSelectMode.AOPI_SELECT_MODE_CHECK_BOX;
                ActionOptionsPaneItems sorting_options = new ActionOptionsPaneItems(select_mode);
                for (TVRecordsProvider.CategoryContainer nestedCategory: t.categoryContainer) {
                    sorting_options.add_item(nestedCategory.name, nestedCategory.id);
                }
                sorting_options.set_selected_item_id(empty_selected_option_id_);

                this.set_options(apiId, sorting_options);
            }
        }

        public void selectDefaultItem() {

            if (items_.size() < 1) {
                return;
            }

            for (int i=0; i<items_.size(); i++) {
                ActionPaneItem item = items_.get(i);
                ActionOptionsPaneItems options = item.options_;

                if (options == null || options.size() < 1) {
                    continue;
                }

                for (int j=0; j<options.size(); j++) {
                    String firstItemID = options.get(j).id_;
                    if (firstItemID.contains(DLRECORDER_RECORDINGS_BY_DATE_ID)) {
                        options.set_selected_item_id(firstItemID);
                        event_listener_.onActionPaneOption(item.id_, firstItemID);
                    }
                }
            }
        }

        public void selectItem(String lastSortID) {
            for (ActionPaneItem item: items_) {
                ActionOptionsPaneItems options = item.options_;
                if (options == null) {
                    continue;
                }
                for (int optionIndex = 0; optionIndex < options.size(); ++optionIndex) {
                    ActionOptionsPaneItems.ActionOptionsPaneItem option = options.get(optionIndex);
                    if (option.id_.equals(lastSortID)) {
                        options.set_selected_item_id(lastSortID);
                        event_listener_.onActionPaneOption(item.id_, lastSortID);
                        return;
                    }
                }
            }
            selectDefaultItem();
        }
    }

    private class TVRecordsProviderEx extends TVRecordsProvider {
        public TVRecordsProviderEx() {
            super(TVRecordsActivity.this);
        }

        @Override
        protected void onObject(PlaybackItemContainer data) {
            showResult(data);
        }

        @Override
        protected void onRecordingSettings(RecordingSettingsResponse resp) {
            processRecordingSettings(resp);
        }

        @Override
        protected void onRequestCategoryComplete(PlaybackItemContainer data) {
            categoryContainer = new ArrayList<>();
            CategoryContainer sorting = new CategoryContainer("41fba2aa-6e23-11e7-907b-a6006ad3dba0",
                    language_manager_.GetString(LocaleStrings.IDS_ALL_RECORDINGS_SORTED_BY), ContainerType.CONTAINER_CATEGORY_SORT);

            categoryContainer.add(sorting);
            for (Container container: data.Containers.list()) {
                if (container.Type == ContainerType.CONTAINER_CATEGORY_SORT) {
                    sorting.categoryContainer.add(new CategoryContainer(container.ObjectID, container.Name, ContainerType.CONTAINER_CATEGORY_SORT));
                }
            }
        }

        @Override
        protected void onRequestCategoryGroupComplete() {
            categoryContainer.addAll(getCategory());
            createMenu();
        }
    }

    private ListView results;
    private ProgramDetails programDetailsPane;
    private MyActionPane _options_menu;
    private TVRecordsProviderEx provider;
    private String selectedOption = "";
    private ArrayList<TVRecordsProvider.CategoryContainer> categoryContainer = new ArrayList<>();
    private ChannelIDList channels_to_update_ = new ChannelIDList();
    private StatusLine _status_line;

    // Date picket image viewer used for Show menu
    ImageView showMenu;

    //cast controls and variables
    private CastContext mCastContext = null;
    private MediaRouteButton mMediaRouteButton = null;

    @Override
    public void loadFinished() {}

    private String formatStartDate(long startTime, long endTime) {
        return (Utils.isToday(startTime) ?
                language_manager_.GetString(LocaleStrings.IDS_TODAY) :
                Utils.UtcToMediumDateString(_context, startTime));
    }

    private String formatStartTime(RecordedTV recording) {
        long startTime= recording.VideoInfo.StartTime;
        long endTime = recording.VideoInfo.StartTime + recording.VideoInfo.Duration;

        if (recording.State == RecordedTVState.RTVS_IN_PROGRESS) {
            return language_manager_.GetString(LocaleStrings.IDS_RECORDING_IN_PROGRESS);
        }

        return Utils.UtcToShortTimeString(_context, startTime);
    }

    private String formatDuration(long duration) {
        return String.format(Locale.getDefault(), "%d %s",
                duration / 60, language_manager_.GetString(LocaleStrings.IDS_MINUTE)
        );
    }

    public TVRecordsActivity() {
        show_exit_dialog = false;
    }

    private ServiceConnection mServiceConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName name) {
        }
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
        }
    };

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getIntent().setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);

        setContentView(R.layout.tvrecords_activity);

        //initialize cast context only on mobile devices and only if google apis are available
        if (!Utils.usesRemoteControl(this) && Common.isGoogleApiAvailable(this)) {
            try {
                mCastContext = CastContext.getSharedInstance(this);
            } catch (Exception e) {
                mCastContext = null;
            }
        }

        if (mCastContext != null) {
            mMediaRouteButton = (MediaRouteButton) findViewById(R.id.media_route_button);
            CastButtonFactory.setUpMediaRouteButton(getApplicationContext(), mMediaRouteButton);

            ServerSettings ss = settings_.getServerSettings();
            if (ss.transcodingSupported)
                mMediaRouteButton.setVisibility(View.VISIBLE);

            ViewStub miniControllerStub = (ViewStub) findViewById(R.id.cast_mini_controller);
            miniControllerStub.inflate();
        }

        int number_of_visible_rows = 5;
        int row_height  = (int)this.getResources().getDimension(R.dimen.search_item_height);
        int divider_width = (int)this.getResources().getDimension(R.dimen.search_divider_width);

        final int layout_height = Utils.usesRemoteControl(this) ? (row_height * number_of_visible_rows + divider_width * number_of_visible_rows) : ViewGroup.LayoutParams.MATCH_PARENT;
        final View lv = findViewById(R.id.search_results);

        if (!Utils.usesRemoteControl(this)) {
            RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) lv.getLayoutParams();
            params.addRule(RelativeLayout.ABOVE, R.id.status_line);
            lv.setLayoutParams(params);
        }

        lv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                lv.getLayoutParams().height = layout_height;
                lv.requestLayout();
                lv.getViewTreeObserver().removeOnGlobalLayoutListener(this);
            }
        });

        _status_line = new StatusLine(findViewById(R.id.status_line));

        CustomTextView v = (CustomTextView) findViewById(R.id.head_bar);
        v.setText(language_manager_.GetString(LocaleStrings.IDS_RECORDED_TV_PAGE_TITLE));

        View root = findViewById(R.id.tvrecords_layout);
        results = (ListView) root.findViewById(R.id.search_results);
        programDetailsPane = (ProgramDetails) root.findViewById(R.id.program_details_pane);
        if (!Utils.usesRemoteControl(this))
            programDetailsPane.setVisibility(View.GONE);

        createMenu();

        provider = new TVRecordsProviderEx();

        //Show menu on  date picker click
        showMenu = (ImageView) findViewById(R.id.date_picker);
        showMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                _options_menu.show();
            }
        });

        getWindow().getDecorView().post(new Runnable() {
            public void run() {
                refresh();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void DataProcessing(String command, Object result_data) {
        provider.DataProcessing(command, result_data);
    }

    @Override
    public void ErrorProcessing(String command, StatusCode error) {
        provider.DataProcessing(command, error);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event){
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            _options_menu.show();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {
        _options_menu.show();
    }

    protected void restoreLastSort() {
        String lastSortID = settings_.getTVRecordsSort();
        if (lastSortID.isEmpty()) {
            _options_menu.selectDefaultItem();
            return;
        }
        _options_menu.selectItem(lastSortID);
    }

    protected void createMenu() {
        _options_menu = new MyActionPane(this, this);
        restoreLastSort();
    }

    public void onActionPaneAction(int action_id) {
        if (action_id == ActionPane.recordings_refresh_pane_action_id_) {
            _options_menu.hide();
            refresh();
        } else
        if (action_id == ActionPane.goto_pane_action_id_) {
            TopLevelNavigationPane tln = new TopLevelNavigationPane(TopLevelNavigationPane.exclude_flags_tvrecords_activity, this);
            tln.show();
        } else
        if (ActionPane.timers_pane_action_id_ == action_id) {
            Intent i = new Intent();
            i.setClass(this, TimersActivity.class);
            startActivityForResult(i, timers_activity_req_code);
        } else
        if (ActionPane.schedules_pane_action_id_ == action_id) {
            Intent i = new Intent();
            i.setClass(this, SchedulesActivity.class);
            startActivityForResult(i, schedules_activity_req_code);
        }

    }

    public void onActionPaneOption(int option_id, String selected_option) {
        clearResults();

        if (!selected_option.equals(empty_selected_option_id_)) {
            _options_menu.uncheck_all_except_this(option_id, selected_option);
            settings_.setTVRecordsSort(selected_option);
            this.selectedOption = selected_option;
            provider.requestObject(selected_option);
        } else {
            _options_menu.selectDefaultItem();
        }
    }

    private void clearResults() {
        results.setAdapter(null);
        programDetailsPane.clear();
        setProgramImage(null);
    }

    private void setProgramImage(RecordedTV record) {
        if (programDetailsPane.getVisibility() == View.VISIBLE) {
            programDetailsPane.findViewById(R.id.description_image).setVisibility(View.INVISIBLE);
            if (record == null) {
                return;
            }
            boolean loadBitmap = (record.Thumbnail != null && !record.Thumbnail.isEmpty());
            if (loadBitmap) {
                new com.dvblogic.tvmosaic.BitmapLoader(
                        this, this, record.Thumbnail,
                        (ImageView) programDetailsPane.findViewById(R.id.description_image), null
                ).start();
            }
        }
    }

    private void showResult(PlaybackItemContainer data) {
        ResultsAdapter resultsAdapter = new ResultsAdapter();
        for (Item resultItem: data.Items.list()) {
            if (resultItem instanceof RecordedTV)
                resultsAdapter.addPrograms((RecordedTV)resultItem);
        }
        if (data.Items.size() == 0)
            Toast.makeText(this, language_manager_.GetString(LocaleStrings.IDS_RECORDINGS_NOT_FOUND), Toast.LENGTH_SHORT).show();

        results.setAdapter(resultsAdapter);
        results.setItemsCanFocus(true);
    }

    protected void processRecordingSettings(RecordingSettingsResponse resp) {

        final double mb = 1024.0;
        final double gb = 1024 * mb;
        final double tb = 1024 * gb;
        String avail_unit;
        String total_unit;

        double avail = resp.AvailSpace;
        double total = resp.TotalSpace;
        if ((resp.AvailSpace / gb) >= 1000) {
            avail = resp.AvailSpace / (double)tb;
            avail_unit = language_manager_.GetString(LocaleStrings.IDS_SIZE_TB_STRING);
        } else {
            avail = resp.AvailSpace / (double)gb;
            avail_unit = language_manager_.GetString(LocaleStrings.IDS_SIZE_GB_STRING);
        }
        if ((resp.TotalSpace / gb) >= 1000) {
            total = resp.TotalSpace / (double)tb;
            total_unit = language_manager_.GetString(LocaleStrings.IDS_SIZE_TB_STRING);
        } else {
            total = resp.TotalSpace / (double)gb;
            total_unit = language_manager_.GetString(LocaleStrings.IDS_SIZE_GB_STRING);
        }

        String disk_str = language_manager_.GetString(LocaleStrings.IDS_DISK_SPACE) + ": ";
        _status_line.setField(0, disk_str, String.format("%.1f %s / %.1f %s", avail, avail_unit, total, total_unit), StatusLine.status_icon_disk_space);

        //request recording objects
        if (selectedOption.isEmpty())
            provider.requestRoot();
        else
            provider.requestObject(selectedOption);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_FIRST_USER) {
            refresh();
        }

        if (data != null) {
            String channels_xml = data.getStringExtra("need_channel_update");
            if (channels_xml != null) {
                ChannelIDList channels = (ChannelIDList) Serializer.deserialize(
                        channels_xml,
                        ChannelIDList.class
                );
                for (int i=0; i<channels.size(); i++) {
                    if (!channels_to_update_.list().contains(channels.get(i)))
                        channels_to_update_.add(channels.get(i));
                }
            }
        }

    }

    public void refresh() {
        provider.requestRecordingSettings();
    }
}
