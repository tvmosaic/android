# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\work\Android\sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

-dontshrink
-verbose

-keep class com.dvblogic.tvmosaic.VLCPlayLiveActivity
-keep class com.dvblogic.tvmosaic.PlayTranscodedRecordingActivity
-keep class com.dvblogic.tvmosaic.VLCPlayRecordingActivity
-keep class com.dvblogic.dvblink_common.ByEpgScheduleConverter
-keep class com.dvblogic.dvblink_common.ByEpgSchedule
-keep class com.dvblogic.tvmosaic.CastOptionsProvider { *; }

-keep class **.R$* { *; }

-keep class com.thoughtworks.** { *; }
-dontwarn com.thoughtworks.**

-keep class org.apache.** { *; }
-dontwarn org.apache.**

-keep class org.videolan.libvlc.** { *; }

-keep class com.actionbarsherlock.** { *; }
-dontwarn com.actionbarsherlock.**

-keep class com.android.vending.billing.**
-dontwarn com.amazon.**
-keep class com.amazon.** {*;}
-keepattributes *Annotation*

